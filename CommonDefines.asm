;Only put defines and macros there!

;When changing these, change also the asm/patches/sram_table.asm
!PerilousPathsCleared = $1F2B|!addr
!Collectibles = $1F2C|!addr
!CheckpointsSMWCCoins = $1F49|!addr
!EventsUnlocked = $1FA9|!addr
!DeathCounter = $1FB8|!addr
!PlayTimeCounter = $1FBA|!addr
!SMWCCoinTotalCount = $1FBE|!addr
!Cheats = $1FC0|!addr
!SwitchesUnlocked = $1FCE|!addr
!EventsUnlockedTotalCount = $1FD5|!addr
!TNKTries = $7FB5F9
!TNKMode = $7FB5FA
!ResetOW = $7FB5FB


;SRAM offsets - look at sram_table.asm for what these values should be.
!SMWCCoinsOffset = $008D
!EventsUnlockedOffset = !SMWCCoinsOffset+$0060
!SMWCCoinTotalCountOffset = !SMWCCoinsOffset+$0060+$000F+$0006
!EventsUnlockedTotalCountOffset = !SMWCCoinsOffset+$008C
!TNKTriesOffset = !SMWCCoinsOffset+$008D+$00F9
!TNKModeOffset = !SMWCCoinsOffset+$008D+$00FA
!ResetOWOffset = !SMWCCoinsOffset+$008D+$00FB

!FileSize = $008D+$008D+$0100


;Values
!MaxProgress = 108+252  ;16-bit (signed for multiplication), exits+SMWCCoins
!ProgressMultiplier = $47  ;8-bit signed, !ProgressMultiplier*!MaxProgress needs to equal 25600 ($6400) or a bit below it


macro SaveSMWCCoins()  ;needs translevel in X
LDA !CheckpointsSMWCCoins,x  ;backup previous collected SMWC coins state
PHA  ;use stack so it doesn't clobber scratch ram

LDA $7FC060  ;load conditional Map16 flags address 1
AND #$07  ;preserve bits 0, 1 and 2
STA !CheckpointsSMWCCoins,x  ;store to respective level's collected SMWC coins address (permanent).

EOR 1,s                     ;\
AND !CheckpointsSMWCCoins,x ;/ keep only bits of newly collected SMWC coins
TAX
LDA.l PopCountTable,x  ;now count them
CLC : ADC !SMWCCoinTotalCount
STA !SMWCCoinTotalCount
LDA #$00
ADC !SMWCCoinTotalCount+1
STA !SMWCCoinTotalCount+1

PLA
endmacro

macro PopCountTable()  ;since we can't share tables between sprites/blocks/uberasm/patches
PopCountTable:
	db 0,1,1,2,1,2,2,3
endmacro

macro SaveTempSMWCCoins()  ;needs translevel in X
LDA $7FC060  ;load conditional Map16 flags address 1
AND #$07  ;preserve bits 0, 1 and 2
ASL #3
ORA !CheckpointsSMWCCoins,x
STA !CheckpointsSMWCCoins,x  ;store to respective level's collected SMWC coins address (permanent).
endmacro

macro ExitsPerLevel()
;NUMBER OF EXITS PER LEVEL
;Here you will determine how many exits every level has. Values above $02 display in the OW indicator as if it was $02.
	db $00,$00,$01,$02,$01,$01,$01,$02		;levels 000-007
	db $01,$02,$01,$02,$01,$01,$01,$02		;levels 008-00F
	db $01,$01,$02,$01,$01,$02,$01,$01		;levels 010-017
	db $01,$01,$02,$01,$01,$01,$01,$01		;levels 018-01F
	db $02,$01,$01,$01,$01				;levels 020-024
	db     $01,$02,$03,$02,$01,$00,$00		;levels 101-107
	db $00,$00,$00,$00,$00,$00,$00,$02		;levels 108-10F
	db $01,$01,$01,$02,$02,$01,$01,$01		;levels 110-117
	db $02,$02,$00,$01,$02,$01,$01,$02		;levels 118-11F
	db $01,$01,$01,$01,$01,$01,$02,$02		;levels 120-127
	db $01,$01,$01,$01,$01,$02,$02,$01		;levels 128-12F
	db $01,$01,$01,$01,$01,$01,$01,$01		;levels 130-137
	db $01,$01,$01,$01				;levels 138-13B
endmacro

macro GetEvent()
;You have to implement GetEventFail:
;input:
;A - exit number indexed from 0
;X - translevel number
;output:
;X - which bit of event byte (from left to right, use "reverse AND" table)
;Y - which event byte
CLC : ADC.l $05D608|!bank,x ;A = ExitNumber + EventNumber
CMP #$78
BCS GetEventFail
PHA			;use stack so it doesn't clobber scratch ram
LSR #3			;divide by 2 three times
TAY			;transfer to Y
PLA			;load event number from stack
AND #$07		;preserve bits 0, 1 and 2
TAX			;transfer to X
endmacro

macro ReverseAND()
db $80,$40,$20,$10,$08,$04,$02,$01
endmacro
