;Oriental submap ASM.
;Includes:
;> Midpoint setups for warp pipe levels.

init:
main:
;a hack so that you can't move right from the pipe at the bottom
LDA $1F1F
CMP #$07
BNE .notOnPipe
LDA $1F21
CMP #$13
BNE .notOnPipe
LDA #$01
TRB $16  ;prevent moving right
.notOnPipe

LDA #$40			;load bit value
TSB $1EA2+($0106-$DC)|!addr	;set to level 106's OW level setting flags.

REP #$20			;16-bit A
LDA $1F17|!addr			;load OW player's X-pos
CMP #$0090			;compare to value
BCC +				;if lower, clear midpoint flags.

SEP #$20			;8-bit A
LDA #$40			;load bit value
TSB $1EA2+($0108-$DC)|!addr	;set to level 108's OW level setting flags.
LDA #$40			;load value
STA !CheckpointsSMWCCoins+($0108-$DC)		;store to level 107's multi midpoint value.
RTL


+
SEP #$20			;8-bit A
LDA #$40			;load bit value
TRB $1EA2+($0108-$DC)|!addr	;clear from level 108's OW level setting flags.
RTL