;Factory map ASM.
;Includes:
;> Midpoint setups for warp pipe levels.

init:
main:
LDA #$40			;load bit value
TSB $1EA2+($010E-$DC)|!addr	;set to level 10E's OW level setting flags.
TSB $1EA2+($010B-$DC)|!addr	;set to level 10B's OW level setting flags.
TRB $1EA2+$24|!addr		;clear from level 24's OW level setting flags.
LDA #$40			;load midway point number (<<6)
STA !CheckpointsSMWCCoins+($010B-$DC)		;store to level 10B's midway point number (and reset SMWC coins which there should be none).

REP #$20			;16-bit A
LDA $1F17|!addr			;load OW player's X-pos
CMP #$01B8			;compare to value
BCS .check0			;if higher or equal, set midway number for level 10E to 0.
CMP #$0158			;compare to value
BCS .check1			;if higher or equal, set midway number for level 10E to 1.
SEP #$20			;8-bit A
LDA #$80			;load midway point number (<<6)
BRA .setmid			;branch ahead

.check0
SEP #$20			;8-bit A
LDA #$00			;load midway point number (<<6)
BRA .setmid			;branch ahead

.check1
SEP #$20			;8-bit A
LDA #$40			;load midway point number (<<6)

.setmid
STA !CheckpointsSMWCCoins+($010E-$DC)		;store to respective midpoint number (and reset SMWC coins which there should be none).
RTL				;return.