;Sky submap ASM.
;Includes:
;> HDMA effects (layer 3 parallax, sky gradient).
;> custom sprites. A custom sprite system isn't a good choice for this hack. we don't want to kill vanilla ones.

!medic_XPos = $0F5E|!addr
!medic_YPos = $0F5F|!addr
!medic_Timer = $0F60|!addr
!medic_Direction = $0F61|!addr

!WarpTimer = $0F08|!addr	;1 byte, from the vanilla status bar area
!Flaggity = $0F09|!addr		;same as above

init:
jsr outsideinit
jmp main

Layer3HDMA:
db $58,$10,$20,$20,$1C,$14,$00

SubMainHDMA:
	db $58,$14,$03
	db $6F,$10,$07
	db $14,$04,$13
	db $00

Table1:
db $3B,$28,$91
db $01,$29,$91
db $06,$29,$92
db $06,$29,$93
db $01,$2A,$93
db $06,$2A,$94
db $04,$2A,$95
db $03,$2B,$95
db $05,$2B,$96
db $03,$2C,$97
db $01,$2C,$98
db $02,$2D,$98
db $01,$2D,$99
db $01,$2E,$99
db $03,$2E,$9A
db $03,$2F,$9B
db $03,$30,$9C
db $01,$30,$9D
db $02,$31,$9D
db $02,$31,$9E
db $01,$32,$9E
db $02,$32,$9F
db $0A,$33,$9F
db $09,$34,$9F
db $0A,$35,$9F
db $08,$36,$9F
db $08,$37,$9F
db $08,$38,$9F
db $08,$39,$9F
db $07,$3A,$9F
db $07,$3B,$9F
db $07,$3C,$9F
db $1A,$3D,$9F
db $00

Table2:
db $40,$4B
db $0C,$4C
db $0C,$4D
db $05,$4E
db $03,$4F
db $04,$50
db $03,$51
db $04,$52
db $04,$53
db $03,$54
db $06,$55
db $0A,$56
db $08,$57
db $0A,$58
db $09,$59
db $08,$5A
db $07,$5B
db $08,$5C
db $07,$5D
db $08,$5E
db $1D,$5F
db $00

outsideinit:
LDA #$40			;load bit value
TSB $1EA2+($010A-$DC)|!addr	;set to level 10A's OW level setting flags.
TSB $1EA2+($010C-$DC)|!addr	;set to level 10C's OW level setting flags.
LDA #$40			;load bit value
TRB $1EA2+($010D-$DC)|!addr	;clear from level 10D's OW level setting flags.

LDA #$F0
STA $1A
STA $1E

LDY #$06
LDX #$12
-
LDA Layer3HDMA,y
STA !hdmaparallaxtbls,x
DEX #3
DEY
BPL -

LDA #$76
STA !medic_XPos
LDA #$60
STA !medic_YPos
LDA #$28
STA !medic_Timer
STZ !medic_Direction

REP #$20
LDA #$0000
STA !hdmaparallaxtbls+$1
STA !hdmaparallaxtbls+$10

LDA.w #Table1
STA $00
LDA.w #Table2
STA $03
SEP #$20
LDA.b #Table1>>16
STA $02
STA $05
JSL HDMA_Color2

REP #$20
LDA #$2C01
STA $4350
LDA.w #SubMainHDMA
STA $4352
LDA #$1102
STA $4360
LDA.w #!hdmaparallaxtbls
STA $4362
SEP #$20
LDA.b #SubMainHDMA>>16
STA $4354
LDA.b #!hdmaparallaxtbls>>16
STA $4364

LDA #$60
TSB $0D9F|!addr
RTS

main:
LDA !WarpTimer
BEQ ++

STZ $15 : STZ $16
STZ $17 : STZ $18
STZ $4219 : STZ $4218
STZ $0DA2 : STZ $0DA3
STZ $0DA6 : STZ $0DA7	;all of this, to assure the player can't input anything that could mess up the intended order of events...

CMP #$1A
BNE +

LDA #$08	;forcefully move up
STA $16

+
DEC !WarpTimer
BRA +++

++
JSR WarpIf

+++
LDA $1DE8
CMP #$05
BNE +

JSR outsideinit

+
;a hack so that you can't move up from the pipe in the middle
LDA $1F1F
CMP #$07
BNE .notOnPipe
LDA $1F21
CMP #$1C
BNE .notOnPipe
LDA #$08
TRB $16  ;prevent moving up
.notOnPipe

REP #$20
LDA !hdmaparallaxtbls+$D
DEC
STA !hdmaparallaxtbls+$D
LSR
STA !hdmaparallaxtbls+$A
LSR
STA !hdmaparallaxtbls+$7
LSR
STA !hdmaparallaxtbls+$4
SEP #$20

LDA $0100
CMP #$0D
BCC .nodraw

JSR Graphics

.nodraw
	LDA $1F19|!addr
	CMP #$B8
	BCS +
	LDA $1F17|!addr
	CMP #$40
	BCC +
	CMP #$B0
	BCS +
	RTL

+
LDA !medic_Timer
BEQ .incdir

DEC !medic_Timer
BRA ++

.incdir
INC !medic_Direction
LDA #$28
STA !medic_Timer
++

LDA $13
AND #$07
BNE .ret

LDA !medic_Timer
CMP #$07
BCC .ret

LDA !medic_Direction
AND #$01
BEQ +

INC !medic_YPos
RTL

+
DEC !medic_YPos
.ret
RTL

Graphics:
LDA !medic_XPos

LDY #$C0
LDX #$0B
Loop:
LDA BlimpTiles,x
STA $0202|!addr,y
LDA #$36
STA $0203|!addr,y
LDA !medic_XPos
CLC : ADC XOffset,x
STA $0200|!addr,y
LDA !medic_YPos
CLC : ADC YOffset,x
STA $0201|!addr,y
PHY
TYA
LSR #2
TAY
LDA TileSize,x
STA $0420|!addr,y
PLY
INY #4
DEX
BPL Loop
RTS

BlimpTiles:
db $A0,$A2,$A4,$A6
db $C0,$C2,$C4,$C6
db $A8,$B8,$C8,$D8

XOffset:
db $00,$10,$20,$30
db $00,$10,$20,$30
db $10,$18,$20,$28	

YOffset:
db $00,$00,$00,$00
db $10,$10,$10,$10
db $20,$20,$20,$20

TileSize:
db $02,$02,$02,$02
db $02,$02,$02,$02
db $00,$00,$00,$00

WarpIf:
REP #$20
LDA $1F19|!addr
CMP #$01FF
BEQ SetWarpFlag
SEP #$20
RTS

SetWarpFlag:
SEP #$20
LDA #$02
STA !Flaggity
RTS