;Fire-Ice map ASM.
;Includes:
;> Midpoint setups for warp pipe levels.

init:
main:
LDA #$40			;load bit value
TSB $1EA2+($010B-$DC)|!addr	;set to level 10B's OW level setting flags.
TSB $1EA2+($010D-$DC)|!addr	;set to level 10D's OW level setting flags.
TRB $1EA2+($010E-$DC)|!addr	;clear from level 10E's OW level setting flags. 
STZ !CheckpointsSMWCCoins+($010B-$DC)		;reset level 10B's midway point number.
RTL
