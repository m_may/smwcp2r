;Carnival submap ASM.
;Includes:
;> custom sprites. A custom sprite system isn't a good choice for this hack. we don't want to kill vanilla ones.
;> Midpoint setups for warp pipe levels.

!LightColorIndex = $0F20|!addr		;at least 8 bytes long
!LightTile = $E2			;tile number

init:
LDA #$40			;load bit value
TSB $1EA2+($0109-$DC)|!addr	;set to level 109's OW level setting flags.
LDA #$40			;load bit value
TRB $1EA2+($010B-$DC)|!addr	;clear from level 10B's OW level setting flags.
TRB $1EA2+($010C-$DC)|!addr	;clear from level 10C's OW level setting flags.

LDA #$17
STA $212C
STA $212E
LDA #$07
STA $212D
STA $212F
RTL

main:
LDA $1B87|!addr		;load continue/save prompt/exchanger trigger
CMP #$02		;compare to value
BCS .NoDraw		;don't process any sprite codes if value is equal or higher than this.

JSR lightos		;draw lights on the map

.NoDraw
RTL			;return.

lightos:
JSR .Graphics
RTS

.Graphics
LDY #$C4		;OAM index

LDX #$07		;loop count
-
LDA $13			;load frame counter
AND #$1F		;preserve bits 0, 1, 2, 3 and 4
BNE ++			;if any of them are set, skip ahead.

JSL $01ACF9|!bank	;get a random number
AND #$03		;preserve bits 0 and 1
CMP !LightColorIndex,x	;compare to value
BNE +			;if not equal, no need to change the value.

INC			;increment A by one
AND #$03		;preserve bits 0 and 1 again

+
STA !LightColorIndex,x	;store result to respective light index.

++
LDA .Xpos,x		;load X-pos from table according to index
STA $0200|!addr,y	;store to OAM.
LDA .Ypos,x		;load Y-pos from table according to index
STA $0201|!addr,y	;store to OAM.
LDA #!LightTile		;load tile number
STA $0202|!addr,y	;store to OAM.
PHX			;preserve loop count
LDA !LightColorIndex,x	;load color index from RAM
AND #$03		;preserve bits 0 and 1
TAX			;transfer to X
LDA .properties,x	;load palette/properties from table according to index
STA $0203|!addr,y	;store to OAM.
PLX			;restore loop count

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to X
LDA #$00		;load tile size
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL -			;loop while it's positive.

RTS			;return.


.Xpos
db $C1,$D7,$CC,$CC		;ferris wheel
db $84,$94			;big circus tent
db $77,$89			;game tent

.Ypos
db $33,$33,$29,$3E		;ferris wheel
db $99,$99			;big circus tent
db $4D,$4D			;game tent

.properties
db $2C,$2E,$28,$2A