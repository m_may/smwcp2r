;Dream submap ASM.
;Includes:
;HDMA gradient

init:
	REP   #$20 ; 16-bit A

	; Set transfer modes.
	LDA   #$3202
	STA   $4330 ; Channel 3
	LDA   #$3200
	STA   $4340 ; Channel 4

	; Point to HDMA tables.
	LDA   #Gradient1_RedGreenTable
	STA   $4332
	LDA   #Gradient1_BlueTable
	STA   $1FFE ;fahrenheit hdma mirror

	SEP   #$20 ; 8-bit A

	; Store program bank to $43x4.
	PHK
	PLA
	STA   $4334 ; Channel 3
	STA   $4344 ; Channel 4

	; Enable channels 3 and 4.
	LDA.b #%00011000
	TSB   $0D9F

	RTL;

; --- HDMA Tables below this line ---
Gradient1_RedGreenTable:
db $1F,$3E,$5B
db $01,$3E,$5A
db $08,$3F,$5A
db $05,$3F,$59
db $02,$3E,$59
db $07,$3E,$58
db $07,$3E,$57
db $07,$3E,$56
db $0D,$3E,$55
db $0D,$3E,$56
db $0E,$3E,$57
db $0E,$3E,$58
db $02,$3E,$59
db $0D,$3F,$59
db $0F,$3F,$5A
db $0E,$3F,$5B
db $04,$3E,$5B
db $02,$3D,$5B
db $02,$3D,$5A
db $04,$3C,$5A
db $01,$3B,$5A
db $03,$3B,$59
db $05,$3A,$59
db $04,$39,$58
db $04,$38,$58
db $01,$38,$57
db $05,$37,$57
db $02,$36,$57
db $03,$36,$56
db $12,$35,$56
db $00

Gradient1_BlueTable:
db $24,$9F
db $08,$9E
db $08,$9D
db $08,$9C
db $08,$9B
db $0C,$9A
db $11,$99
db $12,$98
db $13,$97
db $05,$98
db $05,$99
db $04,$9A
db $05,$9B
db $04,$9C
db $04,$9D
db $04,$9E
db $3B,$9F
db $00
