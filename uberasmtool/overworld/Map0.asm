;Main map ASM.
;Includes:
;> Midpoint setups for warp pipe levels.
;> Sky custom exit warp handler.

!WarpTimer = $0F08|!addr	;1 byte, from the vanilla status bar area
!Flaggity = $0F09|!addr		;same as above

init:
main:
LDA !WarpTimer
BEQ ++

STZ $15 : STZ $16
STZ $17 : STZ $18
STZ $4219 : STZ $4218
STZ $0DA2 : STZ $0DA3
STZ $0DA6 : STZ $0DA7	;all of this, to assure the player can't input anything that could mess up the intended order of events...

CMP #$1A
BNE +

LDA #$04	;forcefully move down
STA $16

+
DEC !WarpTimer
BRA +++

++
JSR WarpIf

+++
LDA #$40			;load bit value
TRB $1EA2+($0106-$DC)|!addr	;clear from level 106's OW level setting flags.
TRB $1EA2+($0109-$DC)|!addr	;clear from level 109's OW level setting flags.
TRB $1EA2+($010A-$DC)|!addr	;clear from level 10A's OW level setting flags.
LDA #$40			;load bit value
TSB $1EA2+($0108-$DC)|!addr	;set to level 108's OW level setting flags.
TSB $1EA2+($010B-$DC)|!addr	;set to level 10B's OW level setting flags.
LDA #$80			;load value
STA !CheckpointsSMWCCoins+($010B-$DC)		;store to level 10B's midway point number.

REP #$20			;16-bit A
LDA $1F19|!addr			;load OW player's Y-pos
CMP #$0070			;compare to value
BCC +				;if lower, branch.

SEP #$20			;8-bit A
LDA #$80			;load value
BRA ++				;branch ahead

+
SEP #$20			;8-bit A
LDA #$00			;load value

++
STA !CheckpointsSMWCCoins+($0108-$DC)		;store to level 107's multi midpoint value.

REP #$20			;16-bit A
LDA $1F17|!addr			;load OW player's X-pos
CMP #$0110			;compare to value
BCS +				;if higher, set midpoint flags.
SEP #$20			;8-bit A

LDA #$40			;load bit value
TRB $1EA2+($0107-$DC)|!addr	;clear from level 107's OW level setting flags.
LDA #$40			;load bit value
TRB $1EA2+($011A-$DC)|!addr	;clear from level 11A's OW level setting flags.
RTL

+
SEP #$20			;8-bit A

LDA #$40			;load bit value
TSB $1EA2+($0107-$DC)|!addr	;set to level 107's OW level setting flags.
LDA #$40			;load bit value
TSB $1EA2+($011A-$DC)|!addr	;set to level 11A's OW level setting flags.
RETURN:
RTL

WarpIf:
REP #$20
LDA $1F17|!addr
CMP #$01A8
BCC nowarp
LDA $1F19|!addr
CMP #$0004
BEQ SetWarpFlag

nowarp:
SEP #$20
RTS

SetWarpFlag:
SEP #$20
LDA #$01
STA !Flaggity
RTS