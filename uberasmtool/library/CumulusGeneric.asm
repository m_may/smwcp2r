;Generic codes for Cumulus Chapel (HDMA table, etc.)
;relevant labels found here:

;PrepTableAndGrad (call from 'load' or 'init')
;Logic (call from 'init' and 'main')
;SetCoords (call from 'nmi')

!messageboxparallax = $7F890B		;change according to NoMesBosWin patch
!L3MsgYPos = $0108			;Y-pos (relative to screen) of the message on the layer 3 tilemap
!ManualMsg = $87			;free RAM: should be set to #$00 or #$01 depending on which message (1 or 2) you want to display.
					;(uberASM main code can't read values that change during an active message box period for some reason)
PrepTableAndGrad:
;Prepare the layer 3 parallax table.
LDA #$77			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$1E
STA !hdmaparallaxtbls+5
LDA #$18
STA !hdmaparallaxtbls+10
STA !hdmaparallaxtbls+15
REP #$20
LDA #$0000
STA !hdmaparallaxtbls+20
STA !hdmaparallaxtbls+1		;zero X-pos for the aurora
LDA $24				;use lm's default Y-pos for every part
STA !hdmaparallaxtbls+3
STA !hdmaparallaxtbls+8
STA !hdmaparallaxtbls+13
STA !hdmaparallaxtbls+18
SEP #$20

JSR setupcoords			;update x coordinates for clouds

;Set the hdma.
	REP   #$20 ; 16-bit A

	; Set transfer modes.
	LDA   #$3202
	STA   $4330 ; Channel 3
	LDA   #$3200
	STA   $4340 ; Channel 4
	LDA #$1103			;mode 3 on $2111 (a.k.a. affect layer 3 XY-wise)
	STA $4350

prochdma:
	; Point to HDMA tables.
	LDA.w #Table1
	STA   $4332
	LDA.w #Table2
	STA   $1FFE
	LDA.w #!hdmaparallaxtbls	;address of table (fahrenheit hdma mirror)
	STA $4352

	SEP   #$20 ; 8-bit A

	; Store bank to $43x4.
	LDA.b #Table1>>16
	STA   $4334 ; Channel 3
	STA   $4344 ; Channel 4
	LDA.b #!hdmaparallaxtbls>>16	;bank of table
	STA $4354

	; Enable channels 3, 4 and 5.
	LDA.b #%00111000
	TSB   $0D9F
	RTL

SetCoords:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$03
ORA $9D
ORA $13D4|!addr
ORA $1426|!addr
ORA $1B89|!addr
BNE +

JSR setupcoords

+
REP #$20
LDX $1B89|!addr			;load message box pointer
CPX #$03			;compare to value (check if on message being displayed state)
BNE prochdma			;if not equal, use regular HDMA table.

LDA.w #!messageboxparallax
STA $4352
SEP #$20
LDA.b #!messageboxparallax>>16
STA $4354
RTL

setupcoords:
REP #$20
LDA !hdmaparallaxtbls+16	;the clouds that should move faster - the bottom ones
INC
STA !hdmaparallaxtbls+16
LSR
STA !hdmaparallaxtbls+6		;the clouds that should move slower - the top ones
CLC
ADC !hdmaparallaxtbls+16
LSR
STA !hdmaparallaxtbls+11	;the clouds that should move with intermediate speed - the middle ones
SEP #$20
RTS

Logic:
JSR MsgTblCalc			;call handling of message HDMA table code

RTL				;return.

MsgTblCalc:
LDA.l !hdmaparallaxtbls		;load first scanline count from layer 3 HDMA table
LDX #$00			;table index

.loopcmp
CMP #$A7			;compare to max tolerated scanlines
BCS .endloop			;if higher, end the loop then subtract value from the result
INX #5				;increment scanline index five times
CLC				;clear carry
ADC.l !hdmaparallaxtbls,x	;add next scanline count value
BCS .endloop			;if carry is set, we're over #$0100 so we gotta take this in account
BRA .loopcmp			;loop back and compare again

.endloop
SEC				;set carry
SBC #$A7			;subtract value - we get how much we should then subtract from the actual offending scanline count
STA $00				;store to scratch RAM. we have the offending scanline count index in X, and the exceeding value here.
LDA !hdmaparallaxtbls,x		;load offending scanline count value
SEC				;set carry
SBC $00				;subtract value from exceeding amount
STA $00				;store over to scratch RAM.

LDA #$00			;load value
STA.l !messageboxparallax+10,x	;store to HDMA table (make it the last byte).
STA.l !messageboxparallax+6,x	;store to HDMA table.
STA.l !messageboxparallax+7,x	;store to HDMA table.
INC				;increment A by one
STA.l !messageboxparallax+5,x	;store to HDMA table.

LDA !ManualMsg			;load manual message number flag
STA.l !messageboxparallax+7,x	;store to HDMA table.

+
REP #$20			;16-bit A
LDA #!L3MsgYPos			;load Y-pos for layer 3 on this entry
STA !messageboxparallax+8,x	;store to message HDMA table.

LDA !hdmaparallaxtbls+3,x	;load actual value from effect HDMA table
STA !messageboxparallax+3,x	;copy over to message HDMA table.
LDA !hdmaparallaxtbls+1,x	;load actual value from effect HDMA table
STA !messageboxparallax+1,x	;copy over to message HDMA table.
SEP #$20			;8-bit A
LDA $00				;load subtracted scanline count from scratch RAM
STA !messageboxparallax,x	;store to message HDMA table.
DEX				;decrement X by one

.setuploop
LDA !hdmaparallaxtbls,x		;load value from effect table, indexed
STA !messageboxparallax,x	;store to message HDMA table, indexed.
DEX				;decrement X by one
BPL .setuploop			;loop while it's positive.
RTS				;return.

Table1:
db $0B,$20,$40
db $0F,$20,$41
db $07,$21,$41
db $15,$21,$42
db $16,$21,$43
db $16,$22,$44
db $03,$22,$45
db $02,$23,$46
db $02,$23,$47
db $01,$24,$47
db $02,$24,$48
db $01,$25,$48
db $02,$25,$49
db $01,$25,$4A
db $02,$26,$4A
db $02,$26,$4B
db $02,$27,$4C
db $0C,$26,$4B
db $09,$26,$4A
db $03,$25,$4A
db $0C,$25,$49
db $07,$25,$48
db $04,$24,$48
db $0C,$24,$47
db $06,$24,$46
db $06,$23,$46
db $0C,$23,$45
db $03,$23,$44
db $08,$22,$44
db $0C,$22,$43
db $00

Table2:
db $04,$80
db $07,$81
db $07,$82
db $07,$83
db $07,$84
db $08,$85
db $07,$86
db $07,$87
db $07,$88
db $07,$89
db $07,$8A
db $07,$8B
db $07,$8C
db $07,$8D
db $04,$8E
db $03,$8F
db $03,$90
db $03,$91
db $02,$92
db $03,$93
db $0B,$94
db $0A,$93
db $0A,$92
db $0A,$91
db $09,$90
db $0A,$8F
db $0A,$8E
db $0A,$8D
db $0A,$8C
db $0A,$8B
db $0A,$8A
db $00