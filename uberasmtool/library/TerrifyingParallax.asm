
init:
LDA #$00			;load value
STA !hdmaparallaxtbls+15	;store to parallax table.
LDA #$5E			;load value
STA !hdmaparallaxtbls		;store to parallax table.
LDA #$48
STA !hdmaparallaxtbls+3		;store to parallax table.
LDA #$0A
STA !hdmaparallaxtbls+6		;store to parallax table.
LDA #$17
STA !hdmaparallaxtbls+9		;store to parallax table.
STA !hdmaparallaxtbls+12	;store to parallax table.

nmi:
REP #$20
LDA $1462
LSR
STA !hdmaparallaxtbls+13
CLC
ADC $1462
LSR #2
STA !hdmaparallaxtbls+10
LSR
STA !hdmaparallaxtbls+7
LSR
STA !hdmaparallaxtbls+4
CLC
ADC $1462
LSR #5
STA !hdmaparallaxtbls+1
SEP #$20
RTL