;HDMA gradient handler (Two channels - 3 and 4)
;Inputs:
;$00 (24-bit) = table 1 location
;$03 (24-bit) = table 2 location

Color2:
REP #$20
LDA #$3200
STA $4330
LDA #$3202
STA $4340
LDA $00
STA $1FFE		; hdma mirror
LDA $03
STA $4332
SEP #$20

LDA $02
STA $4344
LDA $05
STA $4334

LDA #$18
BRA FinishHDMA

;HDMA gradient handler (Three channels - 3, 4 and 5)
;Inputs:
;$00 (24-bit) = red color table location
;$03 (24-bit) = green color table location
;$06 (24-bit) = blue color table location

Color3:
REP #$20		;\
LDA #$3200		; | Use Mode 0 on register 2132
STA $4330		; | 4330 = Mode, 4331 = Register for Channel 3
STA $4340		; | 4340 = Mode, 4341 = Register for Channel 4
STA $4350		; | 4350 = Mode, 4351 = Register for Channel 5
LDA $00			; | Address of red HDMA table
STA $4332		; | 4332 = Low-Byte of table, 4333 = High-Byte of table for Channel 3
LDX $02			; | Address of red HDMA table, get bank byte
STX $4334		; | 4334 = Bank-Byte of table for Channel 3
LDA $03			; | Address of green HDMA table
STA $1FFE		; | fahrenheit hdma mirror [4342 = Low-Byte of table, 4343 = High-Byte of table for Channel 4]
LDX $05			; | Address of green HDMA table, get bank byte
STX $4344		; | 4344 = Bank-Byte of table for Channel 4
LDA $06			; | Address of blue HDMA table
STA $4352		; | 4352 = Low-Byte of table, 4353 = High-Byte of table for Channel 5
LDX $08			; | Address of blue HDMA table, get bank byte
STX $4354		; | 4354 = Bank-Byte of table for Channel 5
SEP #$20		;/
LDA #$38
BRA FinishHDMA

;HDMA brightness handler (channel 3)
;Inputs:
;$00 (24-bit) = brightness table location

Brightness:
REP #$20		;\
STZ $4330		; | 4330 = Mode, 4331 = Register
LDA $00			; | Address of HDMA table
STA $4332		; | 4332 = Low-Byte of table, 4333 = High-Byte of table
LDX $02			; | Address of HDMA table, get bank byte
STX $4334		; | 4334 = Bank-Byte of table
SEP #$20		;/
LDA #$08

FinishHDMA:
TSB $0D9F|!addr
RTL

;Blazing Brush HDMA code (shared by a couple sublevels)
BlazingBrush:
LDA #$17		 	;\ Everything
STA $212C			;| is on main screen
STA $212E
STZ $212D			;| Nothing is on sub screen
STZ $212F
LDA #$27			;| Affect layer 1,2,3
STA $40				;/ and back enable

LDA.b #.Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #.Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #.Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL Color2			;execute HDMA (A goes 8-bit after this routine)
RTL			;return.

.Table1:
	db $80,$2E,$80
	db $22,$2E,$80
	db $04,$2F,$80
	db $04,$30,$80
	db $03,$31,$80
	db $03,$32,$80
	db $03,$33,$80
	db $03,$34,$80
	db $03,$35,$80
	db $04,$36,$80
	db $04,$37,$80
	db $17,$38,$80
	db $08,$39,$80
	db $00

.Table2:
	db $80,$40
	db $20,$40
	db $04,$41
	db $04,$42
	db $03,$43
	db $03,$44
	db $03,$45
	db $03,$46
	db $03,$47
	db $04,$48
	db $04,$49
	db $0E,$4A
	db $02,$4B
	db $01,$4C
	db $01,$4D
	db $01,$4E
	db $01,$4F
	db $01,$50
	db $01,$51
	db $01,$52
	db $01,$53
	db $01,$54
	db $01,$55
	db $02,$56
	db $04,$57
	db $00

;Playground Bound HDMA code (shared by a couple sublevels)
PlaygroundBound:
LDA.b #.Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #.Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #.Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL Color2			;execute HDMA (A goes 8-bit after this routine)
RTL			;return.

.Table1:
db $11,$20,$40
db $04,$21,$40
db $1D,$21,$41
db $0D,$22,$41
db $14,$22,$42
db $16,$23,$42
db $0C,$23,$43
db $1D,$24,$43
db $05,$24,$44
db $05,$24,$45
db $05,$24,$46
db $06,$24,$47
db $05,$24,$48
db $05,$24,$49
db $05,$24,$4A
db $01,$24,$4B
db $04,$23,$4B
db $25,$23,$4C
db $00

.Table2:
db $06,$80
db $0A,$81
db $0A,$82
db $0A,$83
db $0A,$84
db $0A,$85
db $0B,$86
db $0A,$87
db $0A,$88
db $0A,$89
db $0A,$8A
db $0B,$8B
db $0A,$8C
db $0A,$8D
db $0A,$8E
db $0A,$8F
db $09,$90
db $0A,$91
db $0A,$92
db $25,$93
db $00

;Fringe Forest HDMA code (shared by a couple sublevels)
FringeForest:
LDA.b #.Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #.Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #.Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL Color2			;execute HDMA (A goes 8-bit after this routine)
RTL			;return.

.Table1:
db $0A,$54,$9C
db $0C,$55,$9D
db $0B,$56,$9D
db $0B,$57,$9D
db $0C,$58,$9D
db $08,$59,$9D
db $03,$59,$9E
db $0C,$5A,$9E
db $0B,$5B,$9E
db $0B,$5C,$9E
db $0C,$5D,$9E
db $02,$5E,$9E
db $01,$5E,$9F
db $02,$5F,$9F
db $01,$5A,$9E
db $01,$53,$9C
db $05,$4D,$9B
db $03,$4E,$9B
db $03,$4E,$9C
db $06,$4F,$9C
db $06,$50,$9C
db $06,$51,$9C
db $06,$52,$9C
db $03,$53,$9C
db $03,$53,$9D
db $06,$54,$9D
db $06,$55,$9D
db $06,$56,$9D
db $06,$57,$9D
db $02,$58,$9D
db $03,$58,$9E
db $06,$59,$9E
db $07,$5A,$9E
db $08,$5B,$9E
db $07,$5C,$9E
db $02,$5D,$9E
db $05,$5D,$9F
db $00

.Table2:
db $0A,$28
db $08,$29
db $07,$2A
db $08,$2B
db $07,$2C
db $08,$2D
db $07,$2E
db $07,$2F
db $08,$30
db $07,$31
db $08,$32
db $07,$33
db $07,$34
db $08,$35
db $07,$36
db $01,$37
db $01,$39
db $01,$3C
db $01,$3F
db $01,$36
db $01,$2D
db $04,$24
db $07,$25
db $06,$26
db $07,$27
db $06,$28
db $07,$29
db $06,$2A
db $06,$2B
db $07,$2C
db $06,$2D
db $07,$2E
db $06,$2F
db $0D,$30
db $11,$31
db $00