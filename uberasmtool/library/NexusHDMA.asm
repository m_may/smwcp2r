init:
REP #$20
LDA #$3200
STA $4330
LDA #.BlueTable
STA $4332
LDY.b #.BlueTable>>16
STY $4334
SEP #$20
LDA #$08
TSB $0D9F|!addr
RTL

.BlueTable:          ; 
   db $0D : db $89   ; 
   db $0E : db $88   ; 
   db $0D : db $87   ; 
   db $0D : db $86   ; 
   db $0E : db $85   ; 
   db $0E : db $84   ; 
   db $0D : db $83   ; 
   db $0D : db $82   ; 
   db $0D : db $81   ; 
   db $0D : db $82   ; 
   db $0D : db $83   ; 
   db $0E : db $84   ; 
   db $0E : db $85   ; 
   db $0D : db $86   ; 
   db $0D : db $87   ; 
   db $0E : db $88   ; 
   db $0A : db $89   ; 
   db $00            ; 
