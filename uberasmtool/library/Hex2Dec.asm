;Convert hex value to a decimal value (up to 999), 16-bit version by Blind Devil
;Inputs:
;A (16-bit) = value to convert
;Outputs:
;A (8-bit) = ones digit
;X (8-bit) = tens digit
;Y (8-bit) = hundreds digit

run:
REP #$20		;16-bit A (just in case it's not)

run2:
LDX #$00		;load value into X (tens)
TXY			;copy over to Y (hundreds)

-
CMP #$0064		;compare to value
BCC lessthan100		;if lower, don't increase hundreds digit.
SEC			;set carry
SBC #$0064		;subtract value
INY			;increment hundreds digit by one
BRA -			;loop back.

lessthan100:
CMP #$000A		;compare to value
BCC LoopNoMore		;if lower, don't increase tens digit.
SEC			;set carry
SBC #$000A		;subtract value
INX			;increment tens digit by one
BRA lessthan100		;loop back.

LoopNoMore:
SEP #$20		;8-bit A
RTL			;return.

;And here we have a code for the play time clock, not in a separate file because
;a library file can't read another library file, and located here as well because
;this same code is used in two different places, and having an exact copy wastes
;space.
;Outputs:
;$00 (8-bit) = minutes' ones
;$01 (8-bit) = minutes' tens
;$02 (8-bit) = hours' ones
;$03 (8-bit) = hours' tens
LeGameClock:
REP #$20		;16-bit A
LDA !PlayTimeCounter+1	;load minutes of play time
AND #$00FF		;preserve low byte only
JSL run2		;convert number to decimal
STA $00			;store ones to scratch RAM.
STX $01			;store tens to scratch RAM.

REP #$20		;16-bit A
LDA !PlayTimeCounter+2	;load hours of play time
AND #$00FF		;preserve low byte only
JSL run2		;convert number to decimal
STA $02			;store ones to scratch RAM.
STX $03			;store tens to scratch RAM.
RTL			;return.