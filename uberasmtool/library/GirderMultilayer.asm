;Multilayer "Girder Grassland" BG - includes parallax (channels 5 and 6), sprite tile OAM drawing and manual message box display handling.
;channel 5: layer 3 parallax
;channel 6: layer 2 parallax
;relevant labels found here:

;PrepareTable (call from 'load' or 'init')
;Logic (call from 'init' and 'main')
;HDMAOAM (call from 'init' and 'nmi')

;Note that this doesn't handle gradients - so use the 2-channel gradient code from the other library file.

!messageboxparallax = $7F890B		;change according to NoMesBosWin patch
!L3MsgYPos = $0080			;Y-pos (relative to screen) of the message on the layer 3 tilemap
!ManualMsg = $87			;free RAM: should be set to #$00 or #$01 depending on which message (1 or 2) you want to display.
					;(uberASM main code can't read values that change during an active message box period for some reason)

!DontUseSprites = 1			;did this because the sprite tiles flicker on certain coordinates, as well as make the screen flicker due to processing cost
					;(tbh I also think these sprites are unnecessary, layers 2 and 3 for the bg effect look cool enough)

;call the below label in 'init'. it prepares the RAM table with some constant values.
PrepareTable:
LDA #$00
STA.l !hdmaparallaxtbls+25
STA.l !hdmaparallaxtbls+41
STA.l !hdmaparallaxtbls+30

LDA #$5F
STA.l !hdmaparallaxtbls+29
STA.l !hdmaparallaxtbls+20
STA.l !hdmaparallaxtbls+36

LDA #$80
STA.l !hdmaparallaxtbls+26
LSR #2
STA.l !hdmaparallaxtbls+5
LSR
STA.l !hdmaparallaxtbls+10
LDA #$53
STA.l !hdmaparallaxtbls+31
RTL

;Call the below label in init/main: it's the HDMA effect logic.
Logic:
REP #$20

;horizontal positions update
LDA $1E				;load layer 2 X-pos, current frame
STA.l !hdmaparallaxtbls+21	;store to HDMA table.
LSR				;divide A by 2
STA.l !hdmaparallaxtbls+37	;store to HDMA table (L2).
LSR				;divide A by 2
STA.l !hdmaparallaxtbls+16	;store to HDMA table.
STA.l !hdmaparallaxtbls+32	;store to HDMA table (L2).
sta.l !hdmaparallaxtbls+27	;store to HDMA table (L2).
LSR				;divide A by 2
STA.l !hdmaparallaxtbls+11	;store to HDMA table.
LSR				;divide A by 2
STA.l !hdmaparallaxtbls+6	;store to HDMA table.
LSR				;divide A by 2
STA.l !hdmaparallaxtbls+1	;store to HDMA table.

;vertical positions update
LDA $20				;load layer 2 Y-pos, current frame
CLC				;clear carry
ADC #$0040			;add value
STA.l !hdmaparallaxtbls+23	;store to HDMA table.
LSR				;divide A by 2
sta.l !hdmaparallaxtbls+39
STA $00				;store to scratch RAM.
SEP #$20			;8-bit A
ADC #$27			;add value
STA $03				;store to scratch RAM.
REP #$20			;16-bit A
LDA $00				;load value from scratch RAM
LSR				;divide by 2
STA.l !hdmaparallaxtbls+18	;store to HDMA table.
STA.l !hdmaparallaxtbls+34
STA $00				;store to scratch RAM.
SEP #$20			;8-bit A
CLC				;clear carry
ADC $03				;add value from scratch RAM
EOR #$FF			;flip all bits
STA.l !hdmaparallaxtbls+15	;store to HDMA table.

LDA $00
clc
adc #$77
eor #$FF
sta.l !hdmaparallaxtbls+31
lda.l !hdmaparallaxtbls+29
adc #$00
sta.l !hdmaparallaxtbls+26

REP #$20			;16-bit A
LDA $00				;load value from scratch RAM
LSR				;divide by 2
STA.l !hdmaparallaxtbls+13	;store to HDMA table.
STA $00				;store to scratch RAM.
LSR				;divide by 2
STA.l !hdmaparallaxtbls+8	;store to HDMA table.
STA.l !hdmaparallaxtbls+3	;store to HDMA table.
SEP #$20			;8-bit A
ADC #$80			;add value with carry
EOR #$FF			;flip all bits
STA.l !hdmaparallaxtbls		;store to HDMA table.

JSR MsgTblCalc			;jump to handling of message HDMA table

RTL				;return.

MsgTblCalc:
LDA.l !hdmaparallaxtbls		;load first scanline count from layer 3 HDMA table
LDX #$00			;table index

.loopcmp
CMP #$A7			;compare to max tolerated scanlines
BCS .endloop			;if higher, end the loop then subtract value from the result
INX #5				;increment scanline index five times
CLC				;clear carry
ADC.l !hdmaparallaxtbls,x	;add next scanline count value
BCS .endloop			;if carry is set, we're over #$0100 so we gotta take this in account
BRA .loopcmp			;loop back and compare again

.endloop
SEC				;set carry
SBC #$A7			;subtract value - we get how much we should then subtract from the actual offending scanline count
STA $00				;store to scratch RAM. we have the offending scanline count index in X, and the exceeding value here.
LDA !hdmaparallaxtbls,x		;load offending scanline count value
SEC				;set carry
SBC $00				;subtract value from exceeding amount
STA $00				;store over to scratch RAM.

LDA #$00			;load value
STA.l !messageboxparallax+10,x	;store to HDMA table (make it the last byte).
STA.l !messageboxparallax+6,x	;store to HDMA table.
STA.l !messageboxparallax+7,x	;store to HDMA table.
INC				;increment A by one
STA.l !messageboxparallax+5,x	;store to HDMA table.

LDA !ManualMsg			;load manual message number flag
STA.l !messageboxparallax+7,x	;store to HDMA table.

+
REP #$20			;16-bit A
LDA #!L3MsgYPos			;load Y-pos for layer 3 on this entry
STA !messageboxparallax+8,x	;store to message HDMA table.

LDA !hdmaparallaxtbls+3,x	;load actual value from effect HDMA table
STA !messageboxparallax+3,x	;copy over to message HDMA table.
LDA !hdmaparallaxtbls+1,x	;load actual value from effect HDMA table
STA !messageboxparallax+1,x	;copy over to message HDMA table.
SEP #$20			;8-bit A
LDA $00				;load subtracted scanline count from scratch RAM
STA !messageboxparallax,x	;store to message HDMA table.
DEX				;decrement X by one

.setuploop
LDA !hdmaparallaxtbls,x		;load value from effect table, indexed
STA !messageboxparallax,x	;store to message HDMA table, indexed.
DEX				;decrement X by one
BPL .setuploop			;loop while it's positive.
RTS				;return.

;Call the below label in init/nmi: it's the HDMA handling and OAM drawing code.
HDMAOAM:
PHB				;preserve data bank
PHK				;push program bank into stack
PLB				;pull program bank as new data bank
JSR coolbeans			;call the damn code
PLB				;restore previous data bank
RTL				;return.

coolbeans:
REP #$20			;16-bit A
LDA.w #!hdmaparallaxtbls
STA $00
LDX.b #!hdmaparallaxtbls>>16
STX $02

LDX $1B89|!addr			;load message box pointer
CPX #$03			;compare to value (check if on message being displayed state)
BNE .prochdma			;if not equal, use regular HDMA table.

LDA.w #!messageboxparallax
STA $00
LDX.b #!messageboxparallax>>16
STX $02

.prochdma
	LDA #$1103
	STA $4350
	LDA #$0F03
	STA $4360
	LDA $00
	STA $4352
	LDA.w #!hdmaparallaxtbls+26
	STA $4362
	SEP #$20
	LDA $02
	STA $4354
	LDA.b #!hdmaparallaxtbls>>16
	STA $4364

	LDA #$60			;\ Enable HDMA
	TSB $0D9F|!addr			;/ on channels 5 and 6

if !DontUseSprites
	RTS
else
LDA $1426|!addr
BEQ +

REP #$20
LDA !hdmaparallaxtbls+21
STA $0A
LDA !hdmaparallaxtbls+23
SEC
SBC #$0040
BRA ++

+
REP #$20
LDA $1E
STA $0A
LDA $20

++
STA $0C

	REP #$10
	LDY.w #$00FC
	LDX.w #$0000
-	LDA.w .SpTbl+0,x
	CMP.w #$7FFF
	BEQ +++
	SEC : SBC $0A
	AND.w #$01FF
	CMP.w #$0100
	BCC +
	CMP.w #$01F1		;TODO: Number different if 8x8
	BCC ++
+	STA.b $00
	LDA.w .SpTbl+2,x
	SEC : SBC.b $0C
	AND.w #$01FF
	CMP.w #$00E0
	BCC .use
	CMP.w #$01F1		;TODO: Number different if 8x8
	BCC ++
.use	SEP #%00100000
	STA.w $0301|!addr,y
	LDA.b $00
	STA.w $0300|!addr,y
	LDA.w .SpTbl+4,x
	STA.w $0302|!addr,y
	LDA.w .SpTbl+5,x
	STA.w $0303|!addr,y
	PHY
	TYA
	LSR #2
	TAY
	LDA.w .SpTbl+6,x
	ORA.b $01
	STA.w $0460|!addr,y
	PLY
	REP #%00100000
	DEY #4
++	INX #7
	BRA -
+++
	SEP #$30
	RTS

.SpTbl	dw $0078
	dw $0157
	db $86
	db %00001111
	db %10
	
	dw $0078
	dw $0167
	db $8A
	db %00001111
	db %10
	
	dw $00F8
	dw $014F
	db $C0
	db %00001111
	db %10
	
	dw $00F8
	dw $0157
	db $8A
	db %00001111
	db %10
	
	dw $00F8
	dw $0167
	db $E0
	db %00001111
	db %10
	
	dw $0178
	dw $0157
	db $86
	db %00001111
	db %10
	
	dw $0178
	dw $0167
	db $8A
	db %00001111
	db %10
	
	dw $01F8
	dw $014F
	db $C0
	db %00001111
	db %10

	dw $01F8
	dw $0157
	db $8A
	db %00001111
	db %10
	
	dw $01F8
	dw $0167
	db $E0
	db %00001111
	db %10
	
	dw $7FFF
endif