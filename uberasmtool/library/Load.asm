;Codes for 'load' label.
;Relevant labels:
;> CDM16intoCT

;CDM16intoCT: for levels that use HDMA effects or too many ExAnimations,
;SMWC coins must be the ExAnimation variation ones. That's because changing Map16 directly causes flickers.
;This sets the proper custom trigger flags (D, E and F) on or off depending on the coins collected.
;No inputs/outputs. Just call from load and you're golden.
CDM16intoCT:
LDA $7FC0FD			;load custom trigger address 2
AND #$1F			;preserve all bits except 5, 6 and 7
STA $7FC0FD			;store result back.

LDA $7FC060			;load CDM16 flags address 1
AND #$07			;preserve bits 0, 1 and 2
STA $00				;store to scratch RAM.

LDX $13BF|!addr			;load translevel number into X
LDA !CheckpointsSMWCCoins,x	;load SMWC coins collected from level according to index
AND #$3F				;not interested in midpoints - shouldn't be needed, just in case
ORA $00				;OR with the relevant CDM16 flags
PHA				;preserve value into stack
PHA				;preserve value into stack... again lol

AND #$09			;check if coin 1 was either temporarily or permanently collected
BEQ +				;if not collected, skip ahead.

LDA $7FC0FD			;load custom trigger address 2
ORA #$80			;set bit 7 (custom trigger F)
STA $7FC0FD			;store result back.

+
PLA				;restore value from stack
AND #$12			;check if coin 2 was either temporarily or permanently collected
BEQ +				;if not collected, skip ahead.

LDA $7FC0FD			;load custom trigger address 2
ORA #$40			;set bit 6 (custom trigger E)
STA $7FC0FD			;store result back.

+
PLA				;restore value from stack
AND #$24			;check if coin 3 was either temporarily or permanently collected
BEQ +				;if not collected, skip ahead.

LDA $7FC0FD			;load custom trigger address 2
ORA #$20			;set bit 5 (custom trigger D)
STA $7FC0FD			;store result back.

+
RTL