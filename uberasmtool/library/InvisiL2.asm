;Mimics a layer 3 tide, but applies interaction to layer 2 without using objects.
;Also has no current. And yeah, remember to set layer 2 Y scroll to constant.

Water3:
REP #$30
LDX #$00D0		;spot to start using interaction (higher value = lower water level)
BRA CODE_00A04A

Water2:
REP #$30
LDX #$0100		;spot to start using interaction (higher value = lower water level)
BRA CODE_00A04A

Water:
REP #$30
LDX #$0180		;spot to start using interaction (higher value = lower water level)

CODE_00A04A:
LDY #$0058
LDA #$0000		;#$0505 for lava acts-like

CODE_00A050:
STA.l $7EE300,x
INX
INX
DEY
BNE CODE_00A050
TXA
CLC
ADC #$0100
TAX
CPX #$1F00
BCC CODE_00A04A
SEP #$30
LDA #$80
TSB $5B
RTL