; needs 8-bit A
is100ed:
LDA !EventsUnlockedTotalCount
REP #$20
AND #$00FF
CLC : ADC !SMWCCoinTotalCount
CMP.w #!MaxProgress
SEP #$20
RTL

; needs 8-bit A, but the input is 16-bit progress unit count in A (as calculated by is100ed routine)
; mode 7 warning: don't use it while not in v/f-blank (unlikely but still)
calculatePercent:
sta $211B
xba
sta $211B
lda.b #!ProgressMultiplier
sta $211C
lda $2135
RTL
