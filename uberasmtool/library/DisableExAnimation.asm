;Zero out exanimation table thingy
run:
PHB
LDA #$7F  ;change data bank to $7F
PHA
PLB
REP #$20  ;16-bit A
STZ $C0C0
STZ $C0C7
STZ $C0CE
STZ $C0D5
STZ $C0DC
STZ $C0E3
STZ $C0EA
STZ $C0F1
LDX #$FF
STX $C00A
PLB
RTL
