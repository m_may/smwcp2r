run:
LDA $185C|!addr
BEQ .ret

LDA $1461|!addr
CMP #$50
BCS .death

INC $13D3|!addr
STZ $13D4|!addr

LDA #$09
STA $7D
STZ $7B

STZ $15
STZ $16
STZ $17
STZ $18

LDA #$3E
STA $13E0|!addr

INC $1461|!addr

LDA $14
LSR #2
AND #$01
STA $76

.ret
RTL

.death
LDA #$70
STA $97		;warp mario to the bottomless pit
RTL

prephdma:
   LDA #$17    ;\  BG1, BG2, BG3, OBJ on main screen (TM)
   STA $212C   ;|
   STA $212E   ;/  BG1, BG2, BG3, OBJ on main screen should use windowing (TMW)
   STZ $212D   ;\  0 on sub screen (TS)
   STZ $212F   ;/  0 on sub screen should use windowing. (TSW)
   LDA #$B7    ; BG1, BG2, BG3, OBJ, Backdrop, Substract for color math
   STA $40     ;/  mirror of $2131

LDA.b #Table1>>16	;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
LDA.w #Table3			;load address of second table
STA $06				;store to scratch RAM.
RTL

Table1:
   db $06 : db $27   ; 
   db $0C : db $26   ; 
   db $0C : db $25   ; 
   db $0D : db $24   ; 
   db $0B : db $23   ; 
   db $0C : db $22   ; 
   db $0D : db $21   ; 
   db $46 : db $20   ;
   db $0C : db $21   ; 
   db $0C : db $22   ; 
   db $0B : db $23   ; 
   db $0D : db $24   ; 
   db $0C : db $25   ; 
   db $0C : db $26   ; 
   db $03 : db $27   ; 
   db $00            ; 

Table2:
   db $0A : db $4A   ; 
   db $08 : db $49   ; 
   db $09 : db $48   ; 
   db $09 : db $47   ; 
   db $09 : db $46   ; 
   db $09 : db $45   ; 
   db $08 : db $44   ; 
   db $09 : db $43   ; 
   db $09 : db $42   ; 
   db $08 : db $41   ; 
   db $33 : db $40   ; 
   db $08 : db $41   ; 
   db $09 : db $42   ; 
   db $09 : db $43   ; 
   db $08 : db $44   ; 
   db $09 : db $45   ; 
   db $09 : db $46   ; 
   db $09 : db $47   ; 
   db $09 : db $48   ; 
   db $08 : db $49   ; 
   db $07 : db $4A   ; 
   db $00

Table3:
   db $04 : db $97   ; 
   db $05 : db $96   ; 
   db $05 : db $95   ; 
   db $05 : db $94   ; 
   db $04 : db $93   ; 
   db $05 : db $92   ; 
   db $05 : db $91   ; 
   db $05 : db $90   ; 
   db $05 : db $8F   ; 
   db $04 : db $8E   ; 
   db $05 : db $8D   ; 
   db $05 : db $8C   ; 
   db $04 : db $8B   ; 
   db $05 : db $8A   ; 
   db $05 : db $89   ; 
   db $05 : db $88   ; 
   db $05 : db $87   ; 
   db $04 : db $86   ; 
   db $05 : db $85   ; 
   db $05 : db $84   ; 
   db $05 : db $83   ; 
   db $04 : db $82   ; 
   db $05 : db $81   ; 
   db $09 : db $80   ; 
   db $05 : db $81   ; 
   db $04 : db $82   ; 
   db $05 : db $83   ; 
   db $05 : db $84   ; 
   db $05 : db $85   ; 
   db $04 : db $86   ; 
   db $05 : db $87   ; 
   db $05 : db $88   ; 
   db $05 : db $89   ; 
   db $05 : db $8A   ; 
   db $04 : db $8B   ; 
   db $05 : db $8C   ; 
   db $05 : db $8D   ; 
   db $04 : db $8E   ; 
   db $05 : db $8F   ; 
   db $05 : db $90   ; 
   db $05 : db $91   ; 
   db $05 : db $92   ; 
   db $04 : db $93   ; 
   db $05 : db $94   ; 
   db $05 : db $95   ; 
   db $06 : db $96   ; 
   db $00