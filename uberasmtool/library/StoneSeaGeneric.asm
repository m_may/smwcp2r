;Generic codes for Stone Sea Sanctuary (HDMA table, etc.)

!waveindex = $0F5E|!addr

mainwave:
PHB
PHK
PLB

LDA $14
LSR #3
AND #$07
STA !waveindex

LDA $010B|!addr
CMP #$1F
BEQ L1Y
CMP #$E9
BEQ L1Y

LDA $24
SEC
SBC #$40		;SUBTRACT THE AMOUNT OF VALUE THAT SHOULDN'T AFFECT THE SCANLINE
STA $00

LDA #$61		;CHANGE THIS TO THE SCANLINE COUNT IT'S SUPPOSED TO BE
SEC
SBC $00
STA $00
BRA sec

L1Y:
LDA #$7F		;load scanline count
SEC			;set carry
SBC $1C			;subtract layer 1 Y-pos, current frame, low byte, from it
STA $00			;store to scratch RAM.
BMI zeroone

sec:
LDA #$63
BRA st

zeroone:
LDA.b #$E2
SEC
SBC $1C

st:
STA $01

+
REP #$20
LDA !waveindex
ASL
TAY

STZ $0E
LDX #$00		;starting index for loop

waveloop:
PHY

CPX #$00		;first entry of table varies depending on layer 1 Y-pos
BEQ first

SEP #$20
LDA #$08		;default scanline count value

LDY $0E
BNE +
CPX #$03		;second entry
BNE +

second:
INC $0E
LDA $01			;second scanline count value
BRA ssec

first:
SEP #$20
LDA $00			;this holds the dynamic scanline count set before the loop
BMI second

ssec:
STA !hdmaparallaxtbls,x

REP #$20
LDA $1C
BRA ++

+
STA !hdmaparallaxtbls,x
REP #$20

PLY
LDA $1C
CLC
ADC Index,y

PHA

INY #2
TYA
AND #$000F
TAY

PLA

PHY	;anticrash

++
STA !hdmaparallaxtbls+1,x
PLY
INX #3
CPX #$60
BNE waveloop

SEP #$20

LDA #$00
STA !hdmaparallaxtbls,x		;finalize table

PLB
RTL

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond

;WaveScanlines:
;db $7F,$63

Table1:
db $04,$26,$47
db $01,$26,$48
db $0C,$27,$48
db $04,$27,$49
db $0C,$28,$49
db $05,$28,$4A
db $08,$29,$4A
db $04,$29,$4B
db $05,$2A,$4B
db $04,$2A,$4C
db $03,$2B,$4C
db $04,$2B,$4D
db $01,$2C,$4D
db $04,$2C,$4E
db $01,$2D,$4E
db $03,$2D,$4F
db $03,$2E,$50
db $01,$2F,$50
db $02,$2F,$51
db $01,$30,$51
db $03,$30,$52
db $01,$31,$52
db $03,$31,$53
db $02,$32,$53
db $03,$32,$54
db $01,$33,$54
db $04,$33,$55
db $02,$34,$55
db $03,$34,$56
db $03,$35,$56
db $04,$35,$57
db $03,$36,$57
db $03,$36,$58
db $04,$37,$58
db $03,$37,$59
db $03,$38,$59
db $03,$38,$5A
db $04,$39,$5A
db $02,$39,$5B
db $05,$3A,$5B
db $53,$3B,$5C
db $00

Table2:
db $0A,$8D
db $24,$8E
db $0C,$8F
db $07,$90
db $05,$91
db $06,$92
db $05,$93
db $05,$94
db $06,$95
db $07,$96
db $06,$97
db $08,$98
db $08,$99
db $08,$9A
db $08,$9B
db $57,$9C
db $00