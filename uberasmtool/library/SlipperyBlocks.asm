	!Freeram_MarioSlip = $1461|!addr
;^[1 byte] freeram determines slippery or not. Needed so that
;when reading the slippery flag is true and not always assume its
;0 every frame (a clear-itself and then do something RAM address)

	!Freeram_SpriteSlip = $7FACC1
;^[12 (normal) or 22 (SA-1) bytes] same as !Freeram_MarioSlip but for
;sprites.

	!Freeram_SpriteSlipFlag = $7FACCD
;^[12 (normal) or 22 (SA-1) bytes] the actual slippery flag for each
;sprite (determines if the sprite should treat the surface slippery;or not).

	!slipperyness = $FF
;^How much slippery, #$80 = half slippery and #$FF is full slippery.

main:
LDA $1696|!addr		;load disable screen barrier flag (used for goal animations)
BNE Return		;if set, return.

.SlipperyCode
..Mario
	LDA #$00                ;\If freeram slippery flag is already off, leave level
	CMP !Freeram_MarioSlip  ;|as friction for mario.
	BEQ ...Friction         ;/
	STA !Freeram_MarioSlip  ;>Clear the freeram (this should turn off when the player gets off the slippery surface)
	LDA #!slipperyness      ;>Load slippery value to turn on slippery for mario...
...Friction
	STA $86                 ;>Set slippery status.
	;^Note: Make sure that ONLY this code clears !Freeram_MarioSlip, else your slippery blocks
	;don't work.

..Sprite:
	LDX.b #!sprite_slots-1
...Loop
	LDA #$00
	CMP !Freeram_SpriteSlip,x
	BEQ ....Friction
	STA !Freeram_SpriteSlip,x
	LDA #!slipperyness
....Friction
	STA !Freeram_SpriteSlipFlag,x
	;^Note: Same as for mario, nothing other than this code clears !Freeram_SpriteSlip every frame.
....NextSlot
	DEX
	BPL ...Loop
Return:
	RTL