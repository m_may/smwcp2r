table "../msgtable.txt"

;Upload message tilemap into VRAM (channel 2)

;Destroys A, X and Y
;Inputs:
;X (8-bit) =
; bit 0 = has to be 0
; bit 1 = 0 - full, 1 - cropped
; bit 2 = 0 - message 1, 1 - message 2
; bit 3 = 0 - normal destination location, 1 - under it
; bits 4-7 = have to be 0
;A (16-bit) = label of message

DMA:
STA $4322        ;store to source low/high byte register.
LDY.b #Data>>16  ;load bank of messages
STY $4324        ;store to source bank byte register.

LDY #$00		;$00 = increment on low
STY $2115
LDA #$1800		;$00 = transfer 8-bit values, DMA to: $2118
STA $4320

.Finish
LDA.l .dest,x    ;load VRAM destination
STA $2116        ;store value in A to our VRAM destination which will be written to.

TXA
AND #$0002
TAX
LDA.l .length,x  ;load amount of bytes to transfer (based on if full or cropped)

.Finish2
STA $4325        ;store to bytes to transfer register.
LDY #$04         ;load value (DMA on channel 2)
STY $420B        ;store to DMA channel enable register.
RTL

!baseDest = $58A0

.dest
dw !baseDest,!baseDest+$20,!baseDest+$400,!baseDest+$420
dw !baseDest+$220,!baseDest+$240,!baseDest+$620,!baseDest+$640
.length
dw $0100,$00C0


;Destroys A, X and Y
;Inputs:
;X = same as with the routine above
;A (16-bit) = label of Props

.FillProp
STA $4322        ;store to source low/high byte register.
LDY.b #Data>>16  ;load bank of props
STY $4324        ;store to source bank byte register.

LDY #$80         ;$80 = increment on high
STY $2115
LDA #$1908       ;$08 = transfer single value, DMA to: $2119 (VRAM high bytes)
STA $4320
BRA DMA_Finish

.DefaultPropFill
LDX #$00
LDA.w #Props_Normal
JSL DMA_FillProp
LDX #$04
BRA DMA_Finish


;Destroys A and Y
;Inputs:
;A (16-bit) = label of Tile

.FillTileVertMargins
STA $4322        ;store to source low/high byte register.
LDY.b #Data>>16  ;load bank of messages
STY $4324        ;store to source bank byte register.

LDY #$00         ;$00 = increment on low
STY $2115
LDA #$1808       ;$08 = transfer single value, DMA to: $2118 (VRAM high bytes)
STA $4320

LDA #!baseDest   ;load VRAM destination
STA $2116        ;store value in A to our VRAM destination which will be written to.
LDA #$0020         ;load amount of bytes to transfer
JSL DMA_Finish2
LDA #!baseDest+$E0 ;load VRAM destination
STA $2116        ;store value in A to our VRAM destination which will be written to.
LDA #$0020         ;load amount of bytes to transfer
JSL DMA_Finish2
LDA #!baseDest+$400 ;load VRAM destination
STA $2116        ;store value in A to our VRAM destination which will be written to.
LDA #$0020         ;load amount of bytes to transfer
JSL DMA_Finish2
LDA #!baseDest+$4E0 ;load VRAM destination
STA $2116        ;store value in A to our VRAM destination which will be written to.
LDA #$0020         ;load amount of bytes to transfer
JMP DMA_Finish2


Data:

Props:
.Pal0
db $21

.Pal1
db $25

.Pal2
db $29

.Pal3
db $2D

.Pal4
db $31

.Pal5
.Accent
db $35

.Pal6
.Bedazzling
db $39

.Pal7
.Normal
db $3D

Tile:
.Opaque
db $1F

Aerotastic31:
db "                                "
db "          -ATTENTION-           "
db " Jeptaks, if you spot an enemy  "
db " in our territory, aim with the "
db " arrow, wait for the signal and "
db " teach'em a lesson!             "
db "                                "
db "                                "

Artillery11C: ;cropped (in-level order reversed)
.1
db "            -NOTICE-            "
db "                                "
db "        TRESPASSERS WILL        "
db "        BE SHOT ON SIGHT.       "
db "  SURVIVORS WILL BE PROSECUTED. "
db "                                "
.2
db " Norveg Industries is building  "
db " the next level of relaxation!  "
db " With a breathtaking view, and  "
db " over 24 swimming pools,        "
db " Skypoint Hotel opens soon!     "
db "                                "

Asteroid:
.1
db "                                "
db "         -DOC'S NOTES-          "
db "         Project Fluxus         "
db " Early today, we discovered an  "
db " interdimensional rift beneath  "
db " the oasis. That sure beats     "
db " analyzing tar samples, eh?     "
db "                                "
.2
db "                                "
db " This dimension is fascinating  "
db " - the fabric of reality itself "
db " ",$35,$36,$62,"n flux there, ever-shifting "
db " and evolving. And yet...       "
db " shouldn't the end result be    "
db " just a bit more... randomized? "
db "                                "
.3
db "                                "
db " Hmm",$60,$61,"there is definitely some  "
db " force guiding the flux, impos- "
db " ing a concrete, if still fluid "
db " shape on the flow. Now imagine "
db " if we could find a way to      "
db " control this guiding force",$60,$61,"   "
db "                                "
.4
db "                                "
db " Hoo-ha! Preliminary efforts to "
db " influence the force guiding    "
db " the flux were a huge success!  "
db " Refine this technology, and we "
db " could do quite literally       "
db " ANYTHING we can dream of.      "
db "                                "
.5
db "                                "
db " Well, nertz. Norveg has        "
db " prioritized company expansion  "
db " over all new projects. But why "
db " settle for owning the world    "
db " when you can harness the power "
db " of reality itself?             "
db "                                "
.6
db "                                "
db " So, I convinced the boss to    "
db " leave a skeleton crew there to "
db " work on our device, led by our "
db " master dreamer himself. If     "
db " anyone can feel at home in a   "
db " place like that, it is... him. "
db "                                "

awakeningDepression: ;cropped
.1
db " Oh, hello! It's nice to see a  "
db " new face around here. Friends  "
db " help keep the darkness away,   "
db " you know? Feel free to look    "
db " around and meet the others,    "
db " but please watch your step!    "
.2
db " So you're the new guy everyone "
db " is talking about, eh? I hear   "
db " you move around a lot, but I   "
db " hope you'll stay here with us  "
db " for a bit. I'd love to hear    "
db " some stories of your travels.  "
.3
db " I've never been outside of     "
db " this place. I wish I could see "
db " the world like you, Mario.     "
db " Even with my friends, it can   "
db " get pretty lonely down         "
db " here sometimes...              "
.4
db " Mario... you really shouldn't  " ;accent "really"
db " be down here with us. This...  "
db " is not a good place to be.     "
db " You're our friend. We'll help  "
db " you out. Don't worry about us. "
db " Just please",$60,$61,"don't forget us. "
.5
db " Please, ring the bells.        "
db "                                "
db "                                "
db " We'll... We'll be fine.        "
db "                                "
db "                                "

Beach1A:
.1
db "                                "
db " Welcome to the future sight of "
db " FantaSea Shores, the ultimate  "
db " vacationing resort brought to  "
db " you by Norveg Industries: The  "
db " global robotic engineering     "
db " company for all your needs!    "
db "                                "
.2
db "                                "
db "   -NOTICE TO ALL EMPLOYEES-    "
db " Beware of big, bouncy beach    "
db " balls! They have bumped a      "
db " bunch of our boisterous        "
db " breadwinners into the broad,   "
db " blue sea.                      "
db "                                "

Bedazzling1B0: ;TNK (in-level order reversed)
.1
db "                                "
db " You may say I'm a dreamer...   "
db " But I'm not the only one.      "
db " I hope some day you'll join us "
db " And the world will be as...    "
db " ahem, sorry, my mouth got--er, "
db " I got ahead of myself there.   "
db "                                "
.2
db "                                "
db " Look inside the eye of your    "
db " mind. Don't you know you might "
db " find another place to play?    "
db " You say that you'd never been, "
db " but all things that you've     "
db " seen will slowly fade away.    "
db "                                "

BlackMarket:
db "                                "
db " Welcome to the Black Market!   "
db "                                "
db " Today is a special day:        "
db " all wares for free!            "
db "                                "
db " Absolutely no refunds though!  "
db "                                "


CoasterFA:
db "                                "
db " Light up all of the switches   "
db " and win a prize!               "
db "                                "
db " Try as much as you like for as "
db " long as you want.              "
db "                                "
db "                                "

Cumulus11B: ;cropped
db "  PILOT ACCESS CODE ACCEPTED.   "
db "                                "
db "   CHAPEL TELEPORTATION WILL    "
db "   COMMENCE IN A FEW MOMENTS.   "
db "      BE WARY OF SPIRITS.       "
db "                                "

Cumulus17A: ;cropped
db " The creature's wing power is   "
db " draining. He is being consumed "
db " by the chapel itself.          "
db " Will you take him on a final   "
db " flight?                        "
db "                                "

Digsite102: ;cropped
.1
db " Oh, hey Mario. Us              "
db " archeologists just found this  "
db " pyramid, and we're real busy   "
db " digging it out. Feel free to   "
db " go in and explore, though.     "
db "                                "
.2
db " This place surely is creepy... "
db " everyone here is too scared to "
db " go past the first room. The    "
db " walls say there are mummies... "
db " evil cat mummies!              "
db "                                "
.3
db " The quicksand inside is        "
db " violent! We almost lost a guy  "
db " to it yesterday. He was        "
db " goofing around on those odd    "
db " floating blocks. But why do    "
db " they float on quicksand?       "
.4
db " We just discovered two paths   "
db " in this pyramid! Feel free to  "
db " explore wherever you want. Be  "
db " careful, though! It feels like "
db " something powerful is lurking  "
db " in the path to the right...    "

Digsite19F: ;cropped
.1
db " What was that noise? Don't     "
db " tell me you pressed the button "
db " up there, did you? AAAARGH!    "
db " Thanks a lot, Mario. Years of  "
db " work down the drain.           "
db "                                "
.2
db " Don't worry about that guy.    "
db " He's being overdramatic. That  "
db " was only the sound of the room "
db " resetting itself. Not sure why "
db " it does that, though...        "
db "                                "

Fantastic115: ;cropped
.1
db " Welcome to the carniva",$7D,$7E," main  "
db " attraction - the greatest      "
db " bouncy castle ever made! It's  "
db " a feature that is sure to make "
db " everybody JUMP for joy!        "
db "                                "
.2
db " (We at Norveg Industries are   "
db " sorry for the quality of the   "
db " previous pun. The writer has   "
db " suffered a most horrible and   "
db " deserved fate.)                "
db "                                "

Norveg: ;cropped
.1
db " Think you have me cornered,    "
db " do you?                        "
db " Hehe...                        "
db "                                "
db " You have fallen right into my  "
db " trap!                          "
.2
db " Don't you know that you should "
db " never underestimate an         "
db " entrepreneur?!                 "
db "                                "
db " HAHAHAHAHA!!                   "
db "                                "
.3
db " You really wanna know what     "
db " happens when I get my hands    "
db " dirty?                         "
db "                                "
db " FINE!! I'll show you           "
db " the real world, kid!           "
.4
db " NOOOOOOOOOOOO!!!               "
db "                                "
db " My... my empire...             "
db "                                "
db "                                "
db "                                "

Onsen:
.1
db "                                "
db " NOTE TO ALL EMPLOYEES: Due to  "
db " high levels of Greenium con-   "
db " tamination, bathing in the hot "
db " springs on the mountain peak   "
db " is STRICTLY prohibited. Yes,   "
db " that means you, too, Ted.      "
db "                                "
.2
db "                                "
db " Welcome to the Ya Ma Ma onsen  "
db " resort and rare earth mineral  "
db " processing plant! Please enjoy "
db " the hot springs, but don't     "
db " stay in too long! Don't let    "
db " yer feetsies overheatsies!     "
db "                                "

Opulent146: ;TNK
.1
db "                                "
db " You may have discovered the    "
db " curse of the oasis... but you  "
db " won't figure the way out of    "
db " this one! All you will do is   "
db " run around in circles!         "
db "                                "
db "                                "
.2
db "                                "
db " So, you've figured the way     "
db " out, Mario? Well, whatever you "
db " do, don't touch that Rainbow   "
db " Block. It will only lead to    "
db " super fun times.          -TNK "
db "                                "
db "                                "

Petroleum12D:
db "                                "
db "        -NOTE TO STAFF-         "
db " Do not attempt to swim in the  "
db " oil. If you fall in, you will  "
db " slowly sink, much like the     "
db " accurately named quicksand.    "
db "                                "
db "                                "

Rusted1DB:
db "                                "
db " Hee hee hee hee hee...         "
db " Welcome, mortal...             "
db " Norveg abandoned us... but     "
db " you, too, shall join our ranks "
db " of the forgotten!              "
db "                                "
db "                                "

Rusted1DF:
db "                                "
db "          -SECTOR 0A-           "
db "                                "
db "  SHUT DOWN DUE TO HAZARDOUS    "
db "      WORKING ENVIRONMENT.      "
db "  DO NOT ENTER FOR ANY REASON!  "
db "                                "
db "                                "

Stormy:
db "                                "
db " I went out for gas, will be    "
db " back soon. Remember, if you    "
db " see any intruders trespassing  "
db " in MY castle, shoot them down  "
db " immediately!                   "
db "            -Captain Hindenbird "
db "                                "

