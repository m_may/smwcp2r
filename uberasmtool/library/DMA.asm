;DMA handlers (all to be called from 'nmi' ... Disk Poppy note - NO, THE VRAM ONES WILL CORRUPT SCRATCH RAM IF NMI FIRES DURING SOME CODE)

;Relevant labels:
;CGRAM
;VRAM/UseNoIndex (to be called with with 16-bit A)

;Upload entire palette ($0703) into CGRAM (channel 2)
;No inputs/outputs.
CGRAM:
REP #$20
LDX #$00                ;\Set Address for CG-RAM Write to 00
STX $2121               ;/Address for CG-RAM Write
LDA #$2200              ;\One register write once, to 2122
STA $4320               ;/
LDA #$0703		;\Address of palette to upload
STA $4322               ;/ 
if !sa1
	LDX #$00		;\Bank to grab palette from
else
	LDX #$7E		;\Bank to grab palette from
endif
STX $4324               ;/ 
LDA #$0200              ;\Upload the whole palette, 200 bytes
STA $4325               ;/
LDX #$04		;\Start DMA
STX $420B		;/
SEP #$20
RTL

;Upload tilemap/GFX into VRAM (channel 2)

;Inputs:
;$00 (16-bit) = amount of bytes to transfer
;$07 (24-bit) = source data to upload to VRAM
;$0A (16-bit) = VRAM destination

UseNoIndex:
VRAM:
LDX #$80		;$80 = increment on high
STX $2115

LDA #$1801		;$01 = transfer 16-bit values, DMA to: $2118 (VRAM)
.Finish
STA $4320

LDA $0A			;load VRAM destination
STA $2116		;store value in A to our VRAM destination which will be written to.

LDA $07			;load address of GFX (source) to upload
STA $4322		;store to source low/high byte register.

LDX $09			;load bank of GFX (source) to upload
STX $4324               ;store to source bank byte register.

LDA $00			;load amount of bytes to transfer from scratch RAM
STA $4325               ;store to bytes to transfer register.
LDX #$04		;load value (DMA on channel 2)
STX $420B		;store to DMA channel enable register.
SEP #$20		;8-bit A
RTL			;return.

.Low
LDX #$00     ;$00 = increment on low
STX $2115
LDA #$1800   ;$00 = transfer 8-bit values, DMA to: $2118 (VRAM)
BRA .Finish

.SingleValue
..Low
LDX #$00     ;$00 = increment on low
STX $2115
LDA #$1808   ;$08 = transfer single value, DMA to: $2118 (VRAM)
BRA .Finish

..High
LDX #$80     ;$80 = increment on high
STX $2115
LDA #$1908   ;$08 = transfer single value, DMA to: $2119 (VRAM)
BRA .Finish
