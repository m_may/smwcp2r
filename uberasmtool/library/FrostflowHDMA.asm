InitGradient:
	REP   #$20 ; 16-bit A

	; Set transfer modes.
	LDA   #$3202
	STA   $4330 ; Channel 3
	LDA   #$3200
	STA   $4340 ; Channel 4

	; Point to HDMA tables.
	LDA.w #RedGreenTable
	STA   $4332
	LDA.w #BlueTable
	STA   $1FFE ; fahrenheit hdma mirror

	SEP   #$20 ; 8-bit A

	; Store bank of one of the tables to $43x4.
	LDA.b #RedGreenTable>>16
	STA   $4334 ; Channel 3
	STA   $4344 ; Channel 4

	; Enable channels 3 and 4.
	LDA   #%00011000
	TSB   $0D9F
	RTL

RedGreenTable:
db $12,$31,$57
db $03,$32,$57
db $03,$32,$58
db $06,$33,$58
db $01,$34,$58
db $05,$34,$59
db $06,$35,$59
db $06,$36,$5A
db $04,$37,$5A
db $01,$37,$5B
db $06,$38,$5B
db $02,$39,$5B
db $03,$39,$5C
db $05,$3A,$5C
db $01,$3B,$5C
db $04,$3B,$5D
db $05,$3C,$5D
db $04,$3D,$5E
db $0D,$3E,$5E
db $0D,$3E,$5D
db $07,$3E,$5C
db $09,$3F,$5C
db $10,$3F,$5B
db $53,$3F,$5A
db $00

BlueTable:
db $2E,$9D
db $2B,$9E
db $05,$9D
db $06,$9C
db $06,$9B
db $06,$9A
db $06,$99
db $07,$98
db $07,$97
db $07,$96
db $08,$95
db $4D,$94
db $00
