
init:
REP #$20
LDA #$0F02			;mode 2 on $210F (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL

nmi:
LDA #$1F
STA !hdmaparallaxtbls
STA !hdmaparallaxtbls+12
LDA #$10
STA !hdmaparallaxtbls+3
STA !hdmaparallaxtbls+9
LDA #$80
STA !hdmaparallaxtbls+6

REP #$20
LDA $1462|!addr
LSR
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+13	;top/bottom = variable
LSR #2
STA !hdmaparallaxtbls+7		;middle = slow

LDA $22
LSR
CLC
ADC $22
LSR
STA !hdmaparallaxtbls+4
STA !hdmaparallaxtbls+10	;midtop/midbottom = variable 3
SEP #$20

LDA #$00
STA !hdmaparallaxtbls+15
RTL