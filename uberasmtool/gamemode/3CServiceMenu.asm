;Custom Game Mode: File Service Menu
;by Disk Poppy

;TODO: Fancy ass menu where you can manage/swap/copy/delete save files and backups, including an option to choose a backup for the file conversion routine

table "../lg1texttable.txt"

!numOptions = $08

!msgDontUpload = $00
!msgFileEmpty = $01
!msgNotChanged = $02
!msgChanged = $03
!msgNotConverted = $04
!msgConverted = $05
!msgDone = $06
!msgBadVersion = $07

!MessageTilemapPtr = $58  ;2 bytes
!MessageTilemapSize = $5A
!Message = $7C

!MessageArg1 = $60  ;2 bytes - has to be direct page
!MessageArg2 = $62  ;2 bytes - same

!MessageArgTilemapLength = $03
!MessageArg1Tilemap = $7FC800
!MessageArg2Tilemap = !MessageArg1Tilemap+!MessageArgTilemapLength

nmi:
;palette animation
LDA $13
AND #$03
BNE +
LDA $13
LSR
LSR
PHA
JSR UploadColors
PLA
EOR #$20
JSR UploadColors
+

;handle messages
LDX !Message
BEQ .ret
.displayMessage
STZ $2115
LDA.b #Data>>16
STA $4324

LDA MessageTilemapPtrSizes-1,x
STA !MessageTilemapSize

TXA
ASL
TAX

REP #$20
LDA MessageTilemapPtrTbl-2,x
STA !MessageTilemapPtr

LDA #$5200
STA $2116
LDA #$1808   ;$08 = transfer single value, DMA to: $2118 (VRAM)
STA $4320
LDA.w #Tile_Empty
STA $4322
LDA #$0200
STA $4325
LDY #$04
STY $420B

LDY #$00
STY $4326
STY $4320    ;$00 = transfer 8-bit values,
;LDY #$00
-
LDA (!MessageTilemapPtr),y
INY
INY
STA $4322
LDA (!MessageTilemapPtr),y
INY
INY
STA $4324
LDA (!MessageTilemapPtr),y
INY
INY
STA $2116
LDX #$04
STX $420B
CPY !MessageTilemapSize
BCC -
SEP #$20
STZ !Message
.ret
RTL

UploadColors:
JSR GetColorIndex
STX $2121
STZ $2122
STZ $2122
INC
JSR GetColorIndex
STX $2121
AND #$3E
TAX
LDA ServiceMenuPalette_Pal2,x
STA $2122
LDA ServiceMenuPalette_Pal2+1,x
STA $2122
RTS

GetColorIndex:
PHA       ;76543210
ASL
AND #$70  ;z543zzzz
PHA
LDA $02,s
AND #$07  ;zzzzz210
ORA $01,s ;z543z210
CLC : ADC #$21
TAX
PLA
PLA
RTS

Data:
Tile:
.Empty
db $FC

MessageTilemapPtrTbl:
dw msgFileEmpty
dw msgNotChanged
dw msgChanged
dw msgNotConverted
dw msgConverted
dw msgDone
dw msgBadVersion

MessageTilemapPtrSizes:
db datasize(msgFileEmpty)
db datasize(msgNotChanged)
db datasize(msgChanged)
db datasize(msgNotConverted)
db datasize(msgConverted)
db datasize(msgDone)
db datasize(msgBadVersion)

msgFileEmpty:
dl msgFileEmptyTilemap : db datasize(msgFileEmptyTilemap) : dw $5282
msgNotChanged:
dl msgNotChangedTilemap : db datasize(msgNotChangedTilemap) : dw $5282
dl !MessageArg1Tilemap : db !MessageArgTilemapLength : dw $52A2
msgChanged:
dl msgChangedTilemap : db datasize(msgChangedTilemap) : dw $5282
dl !MessageArg1Tilemap : db !MessageArgTilemapLength : dw $5286
dl msgChangedTilemap2 : db datasize(msgChangedTilemap2) : dw $52A2
dl !MessageArg2Tilemap : db !MessageArgTilemapLength : dw $52A9
msgNotConverted:
dl msgNotConvertedTilemap : db datasize(msgNotConvertedTilemap) : dw $5282
dl !MessageArg1Tilemap : db !MessageArgTilemapLength : dw $5295
msgConverted:
dl msgConvertedTilemap : db datasize(msgConvertedTilemap) : dw $5282
dl msgConvertedTilemap2 : db datasize(msgConvertedTilemap2) : dw $52A2
dl !MessageArg1Tilemap : db !MessageArgTilemapLength : dw $52A3
dl !MessageArg2Tilemap : db !MessageArgTilemapLength : dw $52AB
msgDone:
dl msgDoneTilemap : db datasize(msgDoneTilemap) : dw $5282
msgBadVersion:
dl msgBadVersionTilemap : db datasize(msgBadVersionTilemap) : dw $5282
msgEnd:

msgFileEmptyTilemap:
db "FILE EMPTY"
msgNotChangedTilemap:
db "NO CHANGES WERE MADE"
msgChangedTilemap:
db "WAS"
msgChangedTilemap2:
db "IS NOW"
msgNotConvertedTilemap:
db "UNABLE TO CONVERT V"
msgConvertedTilemap:
db "CONVERTED FROM"
msgConvertedTilemap2:
db "V    TO V"
msgDoneTilemap:
db "DONE"
msgBadVersionTilemap:
db "BAD VERSION"
msgTilemapEnd:


init:
JSR LoadAndCheckFile
JSR CheckIfCurrentVersion
LDA.l $700000+!ResetOWOffset,x
SEP #$10
TAX
LDA TickPosTbl,x
STA $0205|!addr
RTL

main:
;handle bg scroll
LDA $1B92|!addr  ;cursor pos
ASL
ASL
CMP $20
BEQ +
BCC .dec
INC $20
BRA +
.dec
DEC $20
+

;handle cursor/selecting/pressing buttons
LDA $18
AND #$30
BNE ReturnToTitle
LDA $16
AND #$90
BNE RunOption
LDA $16
AND #$20
LSR
LSR
LSR
ORA $16
AND #$0C
BEQ .ret
LSR
LSR
TAX
LDA $1B92|!addr  ;cursor pos
ADC.l $009AC8-1|!bank,x  ;pressing down/select will move the cursor 1 down, pressing up or both will move 1 up
BPL +
LDA #!numOptions-1  ;wrap around from top to bottom
BRA .endWrapAround
+
CMP #!numOptions
BCC .endWrapAround
LDA #$00  ;wrap around from bottom to top
.endWrapAround
STA $1B92|!addr  ;cursor pos
TAY
LDA CursorPosTbl,y
STA $0201|!addr
LDA #$06
STA $1DFC|!addr  ;fireball sfx
.ret
RTL

ReturnToTitle:
STZ $62   ;needed so the fade doesn't mess up - look at NoLayer3.asm
INC $13D4|!addr  ;needed so the music starts playing - look at Nolayer3.asm
LDA #$02  ;fade to title screen
STA $0100|!addr
RTL

TickPosTbl:
db $F0
db $30
db $38

CursorPosTbl:
db $18
db $20
db $28
db $30
db $38
db $40
db $48
db $50
CursorPosTblEnd:

assert datasize(CursorPosTbl) == !numOptions

RunOption:
LDA $1B92|!addr
ASL
TAX
JMP (Options,x)

Options:
dw ReturnToTitle
dw ExitsConsistencyCheck
dw SMWCCoinsConsistencyCheck
dw ResetPlayerPos
dw ResetOW
dw MakeBackup
dw RestoreBackup
dw ConvertFile
OptionsEnd:

assert datasize(Options) == !numOptions*2

NotChanged:
LDA #$29
STA $1DFC|!addr
JSR ConvertMsgArg1
LDA #!msgNotChanged
STA !Message
RTL

ExitsConsistencyCheck:
JSR LoadAndCheckFile
JSR CheckIfCurrentVersion
REP #$21
LDA.l $700000+!EventsUnlockedTotalCountOffset,x
AND #$00FF
STA !MessageArg1
TXA
ADC.w #!EventsUnlockedOffset
STA $0D
PHX
SEP #$30
LDA #$70
STA $0F
JSR EventsUnlockedTotalCalc
TXA
REP #$10
PLX
STA.l $700000+!EventsUnlockedTotalCountOffset,x
REP #$20
AND #$00FF
CmpArgs:
SEP #$10
STA !MessageArg2
CMP !MessageArg1
SEP #$20
BEQ NotChanged
LDA #$04
STA $1DFC|!addr
JSR ConvertMsgArg1WIDE
JSR ConvertMsgArg2WIDE
LDA #!msgChanged
STA !Message
RTL

SMWCCoinsConsistencyCheck:
JSR LoadAndCheckFile
JSR CheckIfCurrentVersion
REP #$21
LDA.l $700000+!SMWCCoinTotalCountOffset,x
STA !MessageArg1
TXA
ADC.w #!SMWCCoinsOffset ;offset of SMWC Coins table in SRAM, refer to sram_table.asm
STA $0D
PHX  ;needs to push because X goes 8-bit
SEP #$30
LDA #$70
STA $0F
JSR BitCoinCalc  ;A is now 16-bit
REP #$10
PLX
STA.l $700000+!SMWCCoinTotalCountOffset,x
BRA CmpArgs

ResetPlayerPos:
JSR LoadAndCheckFile
JSR CheckIfCurrentVersion
LDA #$01
STA $1DFC
;LDA #$01
BRA OWCommon

ResetOW:
JSR LoadAndCheckFile
JSR CheckIfCurrentVersion
LDA #$01
STA $1DFC|!addr
INC ;LDA #$02
OWCommon:
CMP.l $700000+!ResetOWOffset,x
BNE +
LDA #$00
+
STA.l $700000+!ResetOWOffset,x
SEP #$10
TAX
LDA TickPosTbl,x
STA $0205|!addr
;LDA #!msgDone
;STA !Message
RTL

MakeBackup:
JSR LoadAndCheckFile
PHB
DEX
DEX
TXY
REP #$20
LDA #!FileSize+2
MVN $71,$70
Done:
SEP #$30
PLB
LDA #$29
STA $1DFC|!addr
LDA #!msgDone
STA !Message
RTL

RestoreBackup:
JSR LoadAndCheckBackup
PHB
DEX
DEX
TXY
REP #$20
LDA #!FileSize+2
MVN $70,$71
BRA Done

ConvertFile:
JSR LoadAndCheckFile
LDA.l $700000+!SRAMVersionComplementSRAMOffset,x
EOR #$FF
CMP.l $700000+!SRAMVersionSRAMOffset,x
BNE ConvertFileFromUnknownTo0
STA !MessageArg1
CMP #0
BEQ ConvertFileFromVersion0To1
;DEC
;BEQ ConvertFileFromVersion1To2
.notConverted
SEP #$10
LDA #$2A
STA $1DFC|!addr
JSR ConvertMsgArg1
LDA #!msgNotConverted
STA !Message
RTL

ConvertFileFromUnknownTo0:
LDA #0
STA !MessageArg2
STA $700000+!SRAMVersionSRAMOffset,x
EOR #$FF
STA $700000+!SRAMVersionComplementSRAMOffset,x

SEP #$10
LDA #$29
STA $1DFC|!addr

LDA.b #'X'
STA !MessageArg1Tilemap
STA !MessageArg1Tilemap+1
STA !MessageArg1Tilemap+2
JSR ConvertMsgArg2

LDA #!msgConverted
STA !Message
RTL

;needs 8-bit A, 16-bit X/Y
ConvertFileFromVersion0To1:
PHB
STX $09
LDA #$70
STA $0F
DEX
DEX
TXY
REP #$20
LDA.w #!FileSize+2-1
MVN $71,$70  ;DB register is now $71
;clear
LDA $09
CLC : ADC.w #$008D+$0060+$000F+$000A
STA $0D ;new misc stuff
LDA #$0000
LDY.w #$0114-2
-
STA [$0D],y
DEY
BPL -
;clear end
LDA $09
TAX
CLC : ADC.w #$008D
STA $0D ;old checkpoints / new checkpoints and smwc coins combined
CLC : ADC.w #$0060
STA $0B ;old smwc coins
SEP #$20
LDA #1  ;version 1
STA.b !MessageArg2
STA.l $700000+!SRAMVersionSRAMOffset,x
EOR #$FF
STA.l $700000+!SRAMVersionComplementSRAMOffset,x
LDA $70008C,x  ;events active total count
STA.l $70008C+$8D,x  ;events unlocked total count
SEP #$10
LDY #$5F
-
LDA ($0D),y ;checkpoints
AND #$03
LSR
ROR
ROR
ORA ($0B),y ;smwc coins
STA [$0D],y ;combined
DEY
BPL -
REP #$21
LDA $09
ADC #$0060
STA $0B ;old events active
CLC : ADC.w #$008D
STA $0D ;new events unlocked
SEP #$20
LDY #$0E
-
LDA ($0B),y ;old events active
STA [$0D],y ;new events unlocked
DEY
BPL -
REP #$21
LDA $09
ADC #$008D+$00C0
STA $0B ;old misc stuff
LDA $0D
CLC : ADC.w #$000F
STA $0D ;new misc stuff
LDY #$08
-
LDA ($0B),y ;old misc stuff
STA [$0D],y ;new misc stuff
DEY
DEY
BPL -
LDA $09
CLC : ADC.w #$0085
STA $0B ;old switches active
CLC : ADC.w #$008D
STA $0D ;new switches unlocked
LDY #$02
-
LDA ($0B),y ;old switches active
STA [$0D],y ;new switches unlocked
DEY
DEY
BPL -
SEP #$20
PLB
LDA #$29
STA $1DFC|!addr
JSR ConvertMsgArg1
JSR ConvertMsgArg2
LDA #!msgConverted
STA !Message
RTL

;X/Y HAVE TO BE 8-BIT
ConvertMsgArg1:
REP #$20
LDA !MessageArg1
AND #$00FF
BRA ConvertMsgArg1ANY
ConvertMsgArg1WIDE:
REP #$20
LDA !MessageArg1
ConvertMsgArg1ANY:
JSL Hex2Dec_run2
STA !MessageArg1Tilemap+2
TYA
STA !MessageArg1Tilemap
TXA
STA !MessageArg1Tilemap+1
RTS

;X/Y HAVE TO BE 8-BIT
ConvertMsgArg2:
REP #$20
LDA !MessageArg2
AND #$00FF
BRA ConvertMsgArg2ANY
ConvertMsgArg2WIDE:
REP #$20
LDA !MessageArg2
ConvertMsgArg2ANY:
JSL Hex2Dec_run2
STA !MessageArg2Tilemap+2
TYA
STA !MessageArg2Tilemap
TXA
STA !MessageArg2Tilemap+1
RTS


;loads SRAM location in Y, sets carry when file full, clears carry when file empty
;copied and modified from sram_plus.asm
GetSRAMLocation:
LDA $010A|!addr   ; file number
ASL
TAX               ; Multiply the save file by two.
REP #$30
LDA $009CCB,x     ; sram location
TAX
RTS

LoadAndCheckBackup:
JSR GetSRAMLocation
LDA $70FFFE,x
BRA CheckFileOrBackup

LoadAndCheckFile:
JSR GetSRAMLocation
LDA $6FFFFE,x
CheckFileOrBackup:
CMP #$BEEF        ; Check if the first two bytes are $BEEF.
BNE .fileEmpty
SEP #$20
RTS
.fileEmpty
PLA ; destroy JSR
SEP #$30
LDA #$2A
STA $1DFC
LDA #!msgFileEmpty
STA !Message
RTL

;equal/zero - current version
CheckIfCurrentVersion:
LDA.l $700000+!SRAMVersionComplementSRAMOffset,x
EOR #$FF
CMP.l $700000+!SRAMVersionSRAMOffset,x
BNE .fileBadVersion
CMP #!CurrentSRAMVersion
BNE .fileBadVersion
RTS
.fileBadVersion:
SEP #$10
PLA : PLA ;destroy JSR
LDA #$2A
STA $1DFC
LDA #!msgBadVersion
STA !Message
RTL

;Code that counts bits, used to keep track of all SMWC coins collected - originally by Blind Devil, replaced by Disk Poppy
;This counts all permanent (collected in a beaten level) SMWC coins, converting them from
;per-level bitflags to a numerical result.
;Outputs:
;A (16-bit) - number of permanent SMWC coins. Code returns with 16-bit A.
BitCoinCalc:
REP #$20
STZ $01
LDY #$5F		;loop count (number of translevels)
-
LDA [$0D],y	;load coins collected in level according to index
AND #$0007		;we'll count 3 bits per level (remember, these are the permanent ones)
TAX		;possible values range from #%00000000-#%00000111.
LDA PopCountTable,x
AND #$00FF
CLC : ADC $01
STA $01
DEY			;decrement Y by one
BPL -			;loop while it's positive.
LDA $01			;load counted bits
RTS			;return.

%PopCountTable()

EventsUnlockedTotalCalc:
LDX #$00
LDY #$0E
-
LDA [$0D],y
.countbit
LSR
BCC +
INX
+
BNE .countbit
DEY
BPL -
RTS
