;Custom Game Mode: Prepare Game Over screen (set up GFX, tilemaps, palettes)
;(the timer is set in the hijack)
;by Disk Poppy

table "../lg1texttable.txt"

!decompBuffer = $7EAD00

main:
JSL $7F8000  ;move all sprite tiles offscreen

STZ $4200  ;disable nmi
LDA #$80   ;load value
STA $2100  ;store to register to force blank.

;Disable previous HDMA
STZ $0D9F|!addr
STZ $420C

STZ $0D9B|!addr
LDA #$09
STA $3E  ;bg mode 1

JSL DisableExAnimation_run

REP #$20
STZ $1A
STZ $1C
STZ $1E
STZ $20
STZ $22
STZ $24

;disable windowing
STZ $41
STZ $42

;LDA #$0046
;STA $0701               ;set fixed color
STZ $0701

;Enable Game Over HDMA
LDA.w #GreenRedTable
STA $00
LDX.b #GreenRedTable>>16
STX $02
LDA.w #NexusHDMA_init_BlueTable
STA $03
LDX.b #NexusHDMA_init_BlueTable>>16
STX $05
JSL HDMA_Color2  ;A goes 8-bit after that

;Palette DMA
STZ $2121               ;Set Address for CG-RAM Write to 00

REP #$20
LDA #$2200              ;\One register write once, to 2122
STA $4320               ;/
LDA.w #Pal              ;\Address of palette to upload
STA $4322               ;/
LDX.b #Pal>>16          ;\Bank of palette
STX $4324               ;/ 
LDA.w #Pal_end-Pal      ;\Bytes to upload
STA $4325               ;/
LDX #$04                ;\Start DMA
STX $420B               ;/

;Layer 3 GFX DMA (LG1)
;REP #$20		;16-bit A
LDA.w #!decompBuffer
STA $00
LDX.b #!decompBuffer>>16
STX $02
LDA.w #$0028  ;ExGFX File
JSL $0FF900|!bank  ;Lunar Magic's decompression routine

REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #!decompBuffer	;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$4000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

;Layer 3 GFX DMA (LG2)
REP #$20		;16-bit A
LDA.w #!decompBuffer
STA $00
LDX.b #!decompBuffer>>16
STX $02
LDA.w #$0029  ;ExGFX File
JSL $0FF900|!bank  ;Lunar Magic's decompression routine

REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #!decompBuffer	;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$4400		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

;Layer 3 Tilemap DMA
REP #$20
LDA #$0400		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #Data>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #Tile_Empty  ;load absolute address of tile
STA $07			;store to scratch RAM.
LDA #$5000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_Low  ;execute DMA (A goes 8-bit after this routine)

REP #$20		;16-bit A
LDA.w #Props_Normal  ;load absolute address of properties
STA $07			;store to scratch RAM.
LDA #$0200		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_High  ;execute DMA (A goes 8-bit after this routine)
REP #$20		;16-bit A
LDA.w #Props_Flipped  ;load absolute address of properties
STA $07			;store to scratch RAM.
LDA #$5200		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_High  ;execute DMA (A goes 8-bit after this routine)

REP #$20		;16-bit A
LDA.w #Tilemap
STA $07			;store to scratch RAM.
LDA.w #Tilemap_end-Tilemap  ;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDA #$51CB		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_Low  ;execute DMA (A goes 8-bit after this routine)
REP #$20		;16-bit A
LDA #$524B		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_Low  ;execute DMA (A goes 8-bit after this routine)

;set main screen and subscreen
LDA #$04
STA $212C
STA $212E
STZ $212D
STZ $212F

;disable color math except for backdrop
LDA #$20
STA $40

;enable nmi
LDA #$81
STA $4200

INC $0100|!addr  ;increment game mode by one
RTL

Data:
GreenRedTable:
   db $5E : db $42,$26
   db $0D : db $42,$25
   db $07 : db $43,$25
   db $07 : db $43,$24
   db $08 : db $44,$24
   db $07 : db $44,$25
   db $08 : db $45,$25
   db $08 : db $45,$26
   db $07 : db $46,$26
   db $08 : db $46,$27
   db $07 : db $47,$27
   db $08 : db $47,$28
   db $08 : db $48,$28
   db $01 : db $48,$29
   db $00

Pal:
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
dw $0000,$1085,$0020,$1CC9,$0000,$0000,$0000,$0000
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
dw $0000,$3143,$0050,$7FFF,$0000,$0000,$0000,$0000
.end
Props:
.Normal
db $38
.Flipped
db $A8
Tile:
.Empty
db $FC
Tilemap:
db "GAME OVER"
.end
