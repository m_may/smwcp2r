;Custom Game Mode: Prepare Stats screen (set up GFX, tilemaps, palettes and counters)
;originally by Blind Devil
;modified by Disk Poppy

table "../lg1texttable.txt"

!decompress = 1  ;note - may crash on zsnes (i hope not)

!StatsScreenSong = $00		;formerly the new forest map theme, salvaged for the menu

!TimeCtLength = $05 ;(has to be the same as in gamemode 33)
!ExitCtLength = $03
!SMWCCtLength = $03
!DeathCtLength = $03
!ProgressCtLength = $03
!SaveSlotLength = $01

!TimeCtTilemap = $7FC800  ;ram for the play time clock (has to be the same as in gamemode 33)
!ExitCtTilemap = !TimeCtTilemap+!TimeCtLength   ;ram for the exits counter
!SMWCCtTilemap = !ExitCtTilemap+!ExitCtLength   ;ram for the SMWC coin counter
!DeathCtTilemap = !SMWCCtTilemap+!SMWCCtLength  ;ram for the death counter
!ProgressCtTilemap = !DeathCtTilemap+!DeathCtLength
!SaveSlotTilemap = !ProgressCtTilemap+!ProgressCtLength   ;ram for the current save slot

!TimeCtCoord = $5313  ;VRAM for the play time clock (has to be the same as in gamemode 33)

!YSwitchX = $7C			;yellow switch block X-pos
!GSwitchX = $90			;green switch block X-pos
!RSwitchX = $A4			;red switch block X-pos
!BSwitchX = $B8			;blue switch block X-pos

!SwitchY = $7B			;all switch blocks Y-pos

!SwBlank = $E4			;empty switch block tile
!SwFilled = $E6			;filled switch block tile

!YSwitchProp = $30		;yellow switch block palette/properties
!GSwitchProp = $3A		;green switch block palette/properties
!RSwitchProp = $34		;red switch block palette/properties
!BSwitchProp = $32		;blue switch block palette/properties

!Layer1TilemapVRAM = $2000
!Layer2TilemapVRAM = $3000
!Layer3TilemapVRAM = $5000

main:
JSL $7F8000		;move all sprite tiles offscreen

;make the relevant changes to layer 3 ram counters here
JSR WriteCounters	;let's write ram to vram buffer, shall we?
JSR WriteSwTiles

STZ $4200  ;disable nmi (originally just for decompression, else bad glitches would happen)
LDA #$80		;load value
STA $2100		;store to register to force blank.

STZ $0D9F|!addr		;disable all hdma.
STZ $420C		;actually disable all hdma.

LDA #$01
STA $0D9B|!addr  ;make the game not think you are on overworld so no overworld (Ex)Animations

JSL DisableExAnimation_run  ;16-bit A

;Palette DMA
LDX #$00
STX $2121               ;Set Address for CG-RAM Write to 00
LDA #$2200              ;\One register write once, to 2122
STA $4320               ;/
LDA.w #Pal              ;\Address of palette to upload
STA $4322               ;/
LDX.b #Pal>>16          ;\Bank of palette
STX $4324               ;/ 
LDA.w #Pal_end-Pal      ;\Bytes to upload
STA $4325               ;/
LDX #$04                ;\Start DMA
STX $420B               ;/
;SEP #$20

STZ $1A  ;reset layer 1 X-pos.
STZ $1C  ;reset layer 1 Y-pos.

;Layer 1 Tilemap clean DMA
;REP #$20		;16-bit A
LDA #$0750		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #Layer1TL>>16		;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA #Layer1TL		;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA.w #!Layer1TilemapVRAM	;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_Low  ;execute DMA (A goes 8-bit after this routine)
REP #$20		;16-bit A
INC $07			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_High  ;execute DMA (A goes 8-bit after this routine)

;Layer 1 GFX DMA
REP #$20		;16-bit A
LDA #$06C0		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #statsgfx>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #statsgfx		;load absolute address of tilemap table
STA $07			;store to scratch RAM.
STZ $0A			;reset scratch RAM ($0000 is our VRAM destination).
JSL DMA_UseNoIndex	;execute DMA (A goes 8-bit after this routine)

;Layer 2 Tilemap DMA
REP #$20			;16-bit A
LDA #$0800			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #statsl2tl_data>>16	;load bank byte of tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #statsl2tl_data		;load absolute address of tilemap table
STA $07				;store to scratch RAM.
LDA.w #!Layer2TilemapVRAM		;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA.w #!Layer2TilemapVRAM+$400	;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA.w #!Layer2TilemapVRAM+$800	;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA.w #!Layer2TilemapVRAM+$C00	;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)

;Layer 2 GFX DMA
;BD's note: would use decompression method but it often crashes on zsnes, and I really, REALLY want the game to work in it, too
if !decompress
	!decompBuffer = $7EAD00
	REP #$20		;16-bit A
	LDA.w #!decompBuffer
	STA $00
	LDX.b #!decompBuffer>>16
	STX $02
	LDA.w #$009F  ;ExGFX File
	JSL $0FF900|!bank  ;Lunar Magic's decompression routine

	REP #$20		;16-bit A
	LDA #$1000		;load amount of bytes to transfer
	STA $00			;store to scratch RAM.
	LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
	STX $09			;store X to scratch RAM.
	LDA.w #!decompBuffer	;load absolute address of tilemap table
	STA $07			;store to scratch RAM.
	LDA #$0800		;load VRAM destination
	STA $0A			;store to scratch RAM.
	JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)
else
	REP #$20		;16-bit A
	LDA #$1000		;load amount of bytes to transfer
	STA $00			;store to scratch RAM.
	LDX.b #ExGFX9F>>16	;load bank byte of tilemap table into X
	STX $09			;store X to scratch RAM.
	LDA.w #ExGFX9F		;load absolute address of tilemap table
	STA $07			;store to scratch RAM.
	LDA #$0800		;load VRAM destination
	STA $0A			;store to scratch RAM.
	JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)
endif

;Layer 3 Tilemap clean DMA
REP #$20			;16-bit A
LDA #$0750		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #Layer3>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #Layer3		;load VRAM destination
STA $07			;store to scratch RAM.
LDA.w #!Layer3TilemapVRAM	;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_Low  ;execute DMA (A goes 8-bit after this routine)
REP #$20		;16-bit A
INC $07			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_High  ;execute DMA (A goes 8-bit after this routine)

;Layer 3 Tilemap upload stripes
REP #$30
LDY #$0000
-
LDA Layer3Pointers+3,y
AND #$00FF
STA $00
LDA Layer3Pointers,y
STA $07
LDA Layer3Pointers+1,y
STA $08
LDA Layer3Pointers+4,y
STA $0A
SEP #$10
JSL DMA_VRAM_Low
REP #$31
TYA
ADC #$0006
TAY
CPY.w #datasize(Layer3Pointers)
BCC -
SEP #$30

;Layer 1 Tilemap DMA
REP #$20		;16-bit A
LDA.w #Layer1TL_end-Layer1TL  ;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #Layer1TL>>16		;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA #Layer1TL		;load VRAM destination
STA $07			;store to scratch RAM.
LDA.w #!Layer1TilemapVRAM+$48  ;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

if !StatsScreenSong
LDA #!StatsScreenSong	;load song number
STA $1DFB|!addr		;store to address to play it.
endif

;set main screen and subscreen
LDA #$17
STA $212C
STA $212E
STZ $212D
STZ $212F

;disable color math
STZ $40

LDA #$81
STA $4200  ;enable nmi

INC $0100|!addr		;increment game mode by one
RTL			;return.

Pal:
db $86,$59,$00,$00,$d0,$7a,$dd,$7f,$93,$73,$00,$00,$bb,$0a,$ff,$03
db $93,$73,$00,$00,$fb,$0c,$eb,$2f,$93,$73,$00,$00,$dd,$7f,$7f,$2d
db $86,$59,$23,$10,$43,$18,$64,$20,$84,$28,$a4,$30,$a4,$34,$c4,$3c
db $e5,$44,$05,$4d,$25,$55,$45,$5d,$65,$65,$66,$69,$86,$71,$a6,$79
db $86,$59,$dd,$7f,$ff,$7f,$a5,$30,$e7,$40,$08,$4d,$6b,$5d,$10,$5a
db $73,$5e,$d6,$62,$39,$67,$7b,$6f,$de,$7b,$ef,$41,$6b,$49,$7f,$3b
.end

Layer1TL:
db $FF,$11,$00,$09,$01,$09,$02,$09,$03,$09,$04,$09,$05,$09,$06,$09	;start of the stats text line ($00A0)
db $07,$09,$08,$09,$09,$09,$0A,$09,$0B,$09,$0C,$09,$0D,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$0E,$09,$0F,$09,$10,$09,$11,$09,$12,$09,$13,$09,$14,$09
db $15,$09,$16,$09,$17,$09,$18,$09,$19,$09,$1A,$09,$1B,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$1C,$09,$1D,$09,$1E,$09,$1F,$09,$20,$09,$21,$09,$22,$09
db $23,$09,$24,$09,$25,$09,$FF,$11,$26,$09,$27,$09,$28,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$29,$09,$2A,$09,$2B,$09,$2C,$09,$2D,$09,$2E,$09,$2F,$09
db $30,$09,$31,$09,$32,$09,$FF,$11,$33,$09,$34,$09,$35,$09		;end of the stats text line ($DE bytes to write total)
.end

Layer3:
db $FC,$20

; format : dl <src> : db <length> : dw <vram dest>
; higher get uploaded first
Layer3Pointers:
dl MarioTilemap : db datasize(MarioTilemap) : dw $510C
dl ExitsTilemap : db datasize(ExitsTilemap) : dw $5189
dl SMWCTilemap : db datasize(SMWCTilemap) : dw $51C6
dl SwitchesTilemap : db datasize(SwitchesTilemap) : dw $5206
dl DeathsTilemap : db datasize(DeathsTilemap) : dw $524A
dl ProgressTilemap : db datasize(ProgressTilemap) : dw $52C9
dl TimeTilemap : db datasize(TimeTilemap) : dw $5308
dl !TimeCtTilemap : db !TimeCtLength : dw !TimeCtCoord
dl !ExitCtTilemap : db !ExitCtLength : dw $5190
dl !SMWCCtTilemap : db !SMWCCtLength : dw $51D2
dl !DeathCtTilemap : db !DeathCtLength : dw $5252
dl !ProgressCtTilemap : db !ProgressCtLength : dw $52D3
dl !SaveSlotTilemap : db !SaveSlotLength : dw $5112
Layer3PointersEnd:

MarioTilemap:
db "MARIO"
ExitsTilemap:
db "EXITS:   0/108"
SMWCTilemap:
db "SMWC COINS:   0/252"
SwitchesTilemap:
db "SWITCHES:"
DeathsTilemap:
db "DEATHS:"
ProgressTilemap:
db "PROGRESS:   0%"
TimeTilemap:
db "PLAY TIME:"
TilemapsEnd:

WriteCounters:
LDA #$FC
STA !ExitCtTilemap
STA !ExitCtTilemap+1
STA !DeathCtTilemap
STA !DeathCtTilemap+1
STA !SMWCCtTilemap
STA !SMWCCtTilemap+1
STA !ProgressCtTilemap
STA !ProgressCtTilemap+1
LDA.b #':'
STA !TimeCtTilemap+2

LDA $010A|!addr		;load current save file number
CLC			;clear carry
ADC #$0A		;add value
STA !SaveSlotTilemap	;store to address.

REP #$20		;16-bit A
LDA !EventsUnlockedTotalCount  ;load amount of events unlocked (exits count)
AND #$00FF		;preserve low byte only
JSL Hex2Dec_run2		;convert to decimal digits (A goes 8-bit after that)
STA !ExitCtTilemap+2	;store ones to address.
CPY #$00		;check if hundreds is equal zero
BEQ +			;if yes, don't draw digit (keep the blank tile).
TYA			;transfer hundreds to A
STA !ExitCtTilemap	;store hundreds to address.
BRA ++			;and in this case, always write tens.

+
CPX #$00		;check if tens is equal zero
BEQ +++			;if yes, don't draw digit (keep the blank tile).

++
TXA			;transfer tens to A
STA !ExitCtTilemap+1	;store tens to address.

+++
REP #$20		;16-bit A
LDA !DeathCounter	;load death count
JSL Hex2Dec_run2		;convert to decimal digits (A goes 8-bit after that)

STA !DeathCtTilemap+2	;store ones to address.

CPY #$00		;check if hundreds is equal zero
BEQ NoHundreds		;if yes, don't draw digit (keep the blank tile).

TYA			;transfer hundreds to A
STA !DeathCtTilemap	;store hundreds to address.
BRA WriteTens		;and in this case, always write tens.

NoHundreds:
CPX #$00		;check if tens is equal zero
BEQ NoTens		;if yes, don't draw digit (keep the blank tile).

WriteTens:
TXA			;transfer tens to A
STA !DeathCtTilemap+1	;store tens to address.

NoTens:
JSL Hex2Dec_LeGameClock	;get converted hours and minutes of play time

LDA $03			;load hours' tens
STA !TimeCtTilemap	;store to address.
LDA $02			;load hours' ones
STA !TimeCtTilemap+1	;store to address.
LDA $01			;load hours' tens
STA !TimeCtTilemap+3	;store to address.
LDA $00			;load hours' ones
STA !TimeCtTilemap+4	;store to address.

REP #$20
LDA !SMWCCoinTotalCount
JSL Hex2Dec_run2		;convert to decimal digits (A goes 8-bit after that)

STA !SMWCCtTilemap+2	;store ones to address.

CPY #$00		;check if hundreds is equal zero
BEQ NoHundreds2		;if yes, don't draw digit (keep the blank tile).

TYA			;transfer hundreds to A
STA !SMWCCtTilemap	;store hundreds to address.
BRA WriteTens2		;and in this case, always write tens.

NoHundreds2:
CPX #$00		;check if tens is equal zero
BEQ NoTens2		;if yes, don't draw digit (keep the blank tile).

WriteTens2:
TXA			;transfer tens to A
STA !SMWCCtTilemap+1	;store tens to address.

NoTens2:
JSL ProgressCheck_is100ed
BCS Write100Percent
JSL ProgressCheck_calculatePercent
;smaller hex2dec (since the progress percent doesn't even technically reach 100 under normal circumstances)
	ldy.b #$00
-
	cmp.b #$0A
	bcc +
	sbc.b #$0A
	iny	
	bra -
	+

STA !ProgressCtTilemap+2	;store ones to address.

CPY #$00		;check if tens is equal zero
BEQ NoTens3		;if yes, don't draw digit (keep the blank tile).

WriteTens3:
TYA			;transfer tens to A
STA !ProgressCtTilemap+1	;store tens to address.

NoTens3:
RTS			;return.

Write100Percent:
LDA #$01
STA !ProgressCtTilemap	;store ones to address.
LDA #$00
STA !ProgressCtTilemap+1	;store ones to address.
STA !ProgressCtTilemap+2	;store ones to address.
RTS

WriteSwTiles:
LDA #!SwBlank		;load tile number for blank block
STA $00			;store to scratch RAM.
STA $01			;store to scratch RAM.
STA $02			;store to scratch RAM.
STA $03			;store to scratch RAM.

LDX #!SwFilled		;load tile number for filled block into X

LDA $1F28		;load yellow switch flag
BEQ +			;if not set, skip ahead.
STX $00			;store X to scratch RAM.

+
LDA $1F27		;load green switch flag
BEQ +			;if not set, skip ahead.
STX $01			;store X to scratch RAM.

+
LDA $1F2A		;load red switch flag
BEQ +			;if not set, skip ahead.
STX $02			;store X to scratch RAM.

+
LDA $1F29		;load blue switch flag
BEQ +			;if not set, skip ahead.
STX $03			;store X to scratch RAM.

+
LDA #!YSwitchX		;load yellow block's X-pos
STA $0340		;store to OAM.
LDA #!GSwitchX		;load green block's X-pos
STA $0344		;store to OAM.
LDA #!RSwitchX		;load red block's X-pos
STA $0348		;store to OAM.
LDA #!BSwitchX		;load blue block's X-pos
STA $034C		;store to OAM.
LDA #!SwitchY		;load blocks' Y-pos
STA $0341		;store to OAM.
STA $0345		;store to OAM.
STA $0349		;store to OAM.
STA $034D		;store to OAM.

LDA $00			;load yellow block tilemap
STA $0342		;store to OAM.
LDA $01			;load green block tilemap
STA $0346		;store to OAM.
LDA $02			;load red block tilemap
STA $034A		;store to OAM.
LDA $03			;load blue block tilemap
STA $034E		;store to OAM.

LDA #!YSwitchProp	;load yellow block properties
STA $0343		;store to OAM.
LDA #!GSwitchProp	;load green block properties
STA $0347		;store to OAM.
LDA #!RSwitchProp	;load red block properties
STA $034B		;store to OAM.
LDA #!BSwitchProp	;load blue block properties
STA $034F		;store to OAM.

LDA #$AA		;load tile size for all tiles
STA $0414		;store to OAM.

RTS			;return.
