init:
main:
LDA $0100|!addr
CMP #$0C
BEQ ZeroXY

LDA $13D4
BEQ +

LDA #$00
STA $7FB000

+
LDA $62				;load number 2 on titlescreen flag
BEQ +				;if equal zero, return (so it doesn't mess up the intro logo with layer 1 garbage).
LDA #$13			;load value (display layer 1, layer 2 and sprites only)
STA $212C : STA $212D		;store to main/subscreen registers.

ZeroXY:
STZ $2111
STZ $2111
STZ $2112
STZ $2112
+
RTL				;return.