init:
REP #$20
LDA #$1B20
STA $143C|!addr
SEP #$20
RTL

main:
REP #$20
DEC $143C|!addr
BEQ .endMode
LDA $143C|!addr
CMP #$0D90
BNE +
LDX #$19
STX $1DFB|!addr ;play I Smell a Rat
+
AND #$03E8
SEP #$20
BEQ .sniff
LDA #$0A
STA $0202|!addr
LDA $0203|!addr ;deedle ball properties
AND #$C0        ;only y/x-flip
ASL
ROL
ROL
TAX
LDA $0200|!addr
CLC : ADC .xspeed,x
STA $0200|!addr
BNE +
LDA #$40
TRB $0203|!addr
BRA .endBounceX
+
CMP #$F0
BNE +
LDA #$40
TSB $0203|!addr
+
.endBounceX
LDA $0201|!addr
CLC : ADC .yspeed,x
STA $0201|!addr
BNE +
LDA #$80
TRB $0203|!addr
BRA .endBounceY
+
CMP #$D0
BNE +
LDA #$80
TSB $0203|!addr
+
.endBounceY
RTL

.sniff
LDA #$4A
STA $0202|!addr
RTL

.endMode
SEP #$20
LDA #$00
STA !TNKMode
STA.l $700002+!TNKModeOffset
STA.l $702002+!TNKModeOffset
STA.l $704002+!TNKModeOffset
STZ $62   ;needed so the fade doesn't mess up - look at NoLayer3.asm
INC $13D4|!addr  ;needed so the music starts playing - look at Nolayer3.asm
LDA #$02
STA $0100|!addr ;game mode - prepare title screen
LDA #$FF
STA $1DFB|!addr ;make song fade out.
RTL

.xspeed
db $01,$FF,$01,$FF

.yspeed
db $01,$01,$FF,$FF
