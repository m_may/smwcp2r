;OW game mode: Press Select to enter Stats Screen by Blind Devil
;Includes:
;nmi: check the overworld pointer process and the move OW screen flag, and if select is pressed, go to game mode 30 and save the game
;...don't get why it breaks on main... something related to how the (hijacked) gamemode pointer works?
;also handles main map/sky custom warps depending on map Y-pos and the respective flag value set by OW codes.
;main: indicators

!WarpTimer = $0F08|!addr	;1 byte, from the vanilla status bar area
!Flaggity = $0F09|!addr		;same as above

nmi:
JSL Graphics

LDA $0100|!addr	;gamemode
CMP #$0E
BNE .ret

LDA $13D4		;load free map camera moving flag
BNE cwarp		;if set, skip ahead.
LDA $13D9		;load overworld pointers
CMP #$03		;check if player is standing still on a tile
BNE cwarp		;if not, skip ahead.

LDA $16			;load controller data 1, first frame only
AND #$20		;check if select is pressed
BEQ cwarp		;if not, skip ahead.

JSL $009BC9|!bank	;save game

LDA #$30		;load game mode number
STA $0100		;store and go to respective game mode.

.ret:
RTL

cwarp:
LDA !Flaggity
BEQ .ret

LDX #$0B
STX $0100|!addr

STZ !Flaggity

LDX #$20
STX !WarpTimer

CMP #$02
BEQ .mainmapwarp

REP #$20
LDA #$0018
STA $1F17|!addr
LDA #$0208		;has to end with 8
STA $1F19|!addr
SEP #$20

LDA #$03
STA $1F11|!addr		;go to sky map
RTL

.disableflag
STZ !Flaggity
.ret
RTL

.mainmapwarp
REP #$20
LDA #$01D8
STA $1F17|!addr
LDA #$0008		;has to end with 8
STA $1F19|!addr
SEP #$20

STZ $1F11|!addr		;go to main map
RTL


;Below we find NMI stuff meant to be run during OW-related gamemodes.
;Dealing with OAM kinda sucks, and SMW erases our sprites if we only use gamemode E for them.

;Tilemaps:
!FrameL = $4C		;frame where coins/exits appear, left tile (16x16)
!FrameR = $44		;frame where coins/exits appear, right tile (16x16)
!FrameProp = $34	;palette/properties for frame

!Outline1 = $6C		;outlined smwc coin, frames 1-3 (8x8)
!Outline2 = $6D 	;outlined smwc coin, frames 2-4 (8x8)
!Coin = $6E		;collected smwc coin (8x8)
!CoinProp = $30		;palette/properties for coin

!ExitN = $7D		;exit not found (8x8)
!ExitY = $6F		;exit found (8x8)
!ExitProp = $32		;palette/properties for exit

;OAM slot defines. They aren't all used, but serve as a guide.
;hint: tab is used
	!OAM03 = $70		;OAM slot for coin 1
!OAM04 = $74		;OAM slot for coin 2
!OAM05 = $78		;OAM slot for coin 3

	!OAM06 = $40		;OAM slot for exit 1
!OAM07 = $44		;OAM slot for exit 2

	!OAM08 = $84		;OAM slot for coins frame 1
!OAM09 = $88		;OAM slot for coins frame 2
!OAM10 = $8C		;OAM slot for coins frame 3
!OAM11 = $90		;OAM slot for exits frame 1
!OAM12 = $94		;OAM slot for exits frame 2
!OAM13 = $98		;OAM slot for exits frame 3

FramesXDisp:
db $C0,$D0,$E0		;top frame (SMWC coins)
db $D4,$E4,$E4		;bottom frame (exits, used when the level has 1 exit)
db $C0,$D0,$E0		;top frame (SMWC coins)
db $CA,$DA,$E0		;bottom frame (exits, used when the level has 2 exits)

FramesYDisp:
db $2B,$2B,$2B
db $3E,$3E,$3E

FramesTiles:
db !FrameL,!FrameR,!FrameR
db !FrameL,!FrameR,!FrameR

CoinTiles:
db !Outline1,!Outline2,!Coin

CoinXDisp:
db $D1,$DB,$E5

!CoinYDisp = $2F

ExitXDisp:
db $E5,$DB

!ExitYDisp = $42

OutlineProps:
db $00,$00,$C0,$40

;other used ram addresses:
;> $0F63-$0F65: tile number for each SMWC coin
;> $0F66-$0F68: tile flip/properties for each SMWC coin
;> $0F69-$0F6A: tile number for each exit indicator
;> $18C5: flags for displaying indicators and world name (format: -----WSI | W: world name. S: secret exit present. I: indicators.)
;> $0F2F: base X-pos of indicators
;> $18C7: SMWC coin flags/exits of current level (format: eetttppp | e: exits. t: temporary coins (blink). p: permanent coins (full).)
;> $18C8: world name display timer

Graphics:
PHB			;preserve data bank
PHK			;push program bank
PLB			;pull as new data bank

LDA $0F2F|!addr		;load base X-disp value for indicators
CMP #$30		;compare to value
BCS +			;if higher or equal, skip ahead.

JSR DrawFrames		;draw frames where indicators will be displayed
JSR DrawCoins		;draw SMWC coins indicator
JSR DrawExits		;draw exits indicator

+
;world name thing go here

PLB			;restore original data bank
RTL			;return.

;The frames that show up below indicators themselves.
DrawFrames:
LDY #!OAM08		;get OAM slot number into Y
LDX #$05		;loop count (draw 6 tiles)

STZ $00			;reset scratch RAM.
LDA $18C5|!addr		;load flags
AND #$02		;check if secret exit flag is set
BEQ .loop		;if not, skip ahead.

LDA #$06		;load value
STA $00			;store to scratch RAM.

.loop
PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $00			;add value from scratch RAM
TAX
LDA FramesXDisp,x	;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.
PLX			;restore loop count

LDA FramesYDisp,x	;load Y-disp for tile according to index
STA $0201|!addr,y	;store to OAM.

LDA FramesTiles,x	;load tile number from table according to index
STA $0202|!addr,y	;store to OAM.

LDA #!FrameProp		;load palette/properties value
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$02		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.

;The SMWC coin indicators.
DrawCoins:
LDY #!OAM03		;get OAM slot number into Y
LDX #$02		;loop count (draw 3 tiles)

.loop
LDA CoinXDisp,x		;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.

LDA #!CoinYDisp		;load Y-disp for tile
STA $0201|!addr,y	;store to OAM.

LDA $0F63|!addr,x	;load tile number from RAM according to index
STA $0202|!addr,y	;store to OAM.

LDA #!CoinProp		;load palette/properties value (palette only)
ORA.l $0F66|!addr,x	;set flips from RAM according to index
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$00		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.

;The exit indicators.
DrawExits:
LDY #!OAM06		;get OAM slot number into Y
LDX #$01		;loop count (draw 2 tiles)

STZ $00			;reset scratch RAM.
LDA $18C5|!addr		;load flags
AND #$02		;check if secret exit flag is set
BNE .loop		;if it is, skip ahead.

LDA #$0B		;load value
STA $00			;store to scratch RAM.

.loop
LDA ExitXDisp,x		;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
ADC $00			;add (no) secret exit X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.

LDA #!ExitYDisp		;load Y-disp for tile
STA $0201|!addr,y	;store to OAM.

LDA $0F69|!addr,x	;load tile number from RAM according to index
STA $0202|!addr,y	;store to OAM.

LDA #!ExitProp		;load palette/properties value
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$00		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.



;GFX handlers for indicators in the overworld.
main:
LDA $0100|!addr		;gamemode
CMP #$0E
BNE .ret
JSR ManageDisplayInd	;manage displaying indicators
JSR SMWCCoinInd		;handle SMWC coin tile indicators
JSR ExitInd		;handle exit tile indicators
.ret
RTL			;return.

SMWCCoinInd:
LDA $18C7|!addr		;load coins and exits flags
AND #$3F		;preserve all coin bits

LDX #$00		;loop count

.pcoinloop
PHA			;preserve bitflags
AND #$01		;preserve bit 0 only
CMP #$01		;check if bit 0 is set
BNE ..pnotset		;if not set, display it as empty.

LDA #!Coin		;load coin tilemap
STA $0F63|!addr,x	;store to indexed address.
STZ $0F66|!addr,x	;reset indexed address used for flips.
BRA ..pender		;branch ahead

..pnotset
PLA			;restore bitflags into A
PHA			;preserve them again
AND #$08		;preserve bit 3 only
CMP #$08		;check if bit 3 is set
BNE ..emptycoin		;if not, draw coin outline.

LDA $13			;load frame counter
LSR #3			;divide by 2 three times
AND #$01		;preserve bit 0 only
PHX			;preserve loop count
TAX			;transfer A to X
LDA CoinTiles+1,x	;load tile number from table according to index
PLX			;restore loop count
STA $0F63|!addr,x	;store to indexed address.
STZ $0F66|!addr,x	;reset indexed address used for flips.
BRA ..pender		;branch ahead

..emptycoin
LDA $13			;load frame counter
LSR #3			;divide by 2 three times
AND #$03		;preserve bits 0 and 1
STA $00			;store to scratch RAM.
PHX			;preserve loop count
TAX			;transfer A to X
LDA OutlineProps,x	;load flip properties table according to index
PLX			;restore loop count
STA $0F66|!addr,x	;store to indexed address used for flips.

PHX			;preserve loop count
LDA $00			;load value from scratch RAM
AND #$01		;preserve bit 0 only
TAX			;transfer to X
LDA CoinTiles,x		;load tile number from table according to index
PLX			;restore loop count
STA $0F63|!addr,x	;store to indexed address.

..pender
PLA			;restore bitflags
LSR			;shift bits to the right
INX			;increment X by one
CPX #$03		;compare X to value
BNE .pcoinloop		;keep looping if not equal.
RTS			;return.

ExitInd:
LDA $18C7|!addr		;load coins and exits flags
AND #$C0		;preserve all exit bits

LDX #$00		;loop count

.exitloop
PHA			;preserve bitflags
AND #$40		;preserve bit 6 only
BEQ ..noexit		;if not set, no exit found so draw the indicator off.

LDA #!ExitY		;load tile number
BRA ..eender		;branch ahead

..noexit
LDA #!ExitN		;load tile number

..eender
STA $0F69|!addr,x	;store to indexed address.

PLA			;restore bitflags
LSR			;shift bits to the right
INX			;increment X by one
CPX #$02		;compare X to value
BNE .exitloop		;loop if not equal.

RTS			;return.

ManageDisplayInd:
LDA $13D9|!addr		;load pointer to processes running on the overworld
CMP #$03		;compare to value
BEQ .FlagOnNSetLvl	;if equal, display the indicators and update level and flags for it
CMP #$05		;compare to value
BEQ .FlagOn		;if equal, display the indicators.
CMP #$08		;compare to value
BEQ .FlagOn		;if equal, display the indicators.

.FlagOff
LDA #$01		;load indicators flag value
TRB $18C5|!addr		;clear it from address.
BRA MoveInd		;branch to move our indicators

.FlagOnNSetLvl
JSL OWStuff_GetLvlFromCoord	;get current level number from coordinates
TAX				;transfer to X
LDA !CheckpointsSMWCCoins,x	;load SMWC coin flags from respective level
AND #$3F		;preserve coin bits (temp and permament)
STA $18C7|!addr			;store to mirror.

JSR CheckOnExits		;check on events and exits for levels
BRA MoveInd			;branch ahead

.FlagOn
LDA #$01		;load indicators flag value
TSB $18C5|!addr		;set it to address.

MoveInd:
LDA $18C5|!addr		;load flags
AND #$01		;check if display indicators flag is set
BEQ MoveBack		;if not, move them back.

LDA $0F2F|!addr		;load base X-pos for indicators
BEQ .done		;if equal, stop decrementing.
CMP #$03		;compare to value
BCC .spd1		;if lower, move 1 pixel every 2 frames.
CMP #$06		;compare to value
BCC .spd2		;if lower, move 1 pixel per frame.

DEC $0F2F|!addr		;decrement base X-pos for indicators by one...
DEC $0F2F|!addr		;...twice (move 3 pixels per frame)
BRA .spd2		;branch ahead

.spd1
LDA $13			;load frame counter
AND #$01		;preserve bit 0 only
BNE .done		;if it's set, skip ahead.

.spd2
DEC $0F2F|!addr		;decrement base X-pos for indicators by one

.done
RTS			;return.

MoveBack:
LDA $0F2F|!addr		;load base X-pos for indicators
CMP #$34		;compare to value
BCS .done		;if higher, stop incrementing.
CMP #$02		;compare to value
BCC .spd1		;if lower, move 1 pixel every 2 frames.
CMP #$04		;compare to value
BCC .spd2		;if lower, move 1 pixel per frame.

INC $0F2F|!addr		;increment base X-pos for indicators by one...
INC $0F2F|!addr		;...twice (move 3 pixels per frame)
BRA .spd2		;branch ahead

.spd1
LDA $13			;load frame counter
AND #$01		;preserve bit 0 only
BNE .done		;if it's set, skip ahead.

.spd2
INC $0F2F|!addr		;increment base X-pos for indicators by one

.done
RTS			;return.

CheckOnExits:
LDY #$00		;load value into Y (defines normal or secret exit for check)

LDA #$02		;load value for secret exit present flag
TRB $18C5|!addr		;clear it from flags.

LDA ExitsPerLevel,x	;load amount of exits the level has
BEQ .noexits		;if none, no reason to display anything.
DEC			;decrement A by one
BEQ .oneexit		;if equal #$01, level has one exit, so branch there.

.twoexits
INY			;increment Y by one
LDA #$02		;load value for secret exit present flag
TSB $18C5|!addr		;set it to flags.

.oneexit
JSR CheckBeaten		;check if the respective event was beaten (carry set = yes. carry clear = no.)
DEY			;decrement Y by one
BPL .oneexit		;loop while it's positive.
RTS			;return.

.noexits
LDA #$01		;load indicators flag value
TRB $18C5|!addr		;clear it from address. we don't want to display anything for a level with no exits or events.
RTS			;return.

CheckBeaten:
PHX			;preserve x
PHY			;preserve Y
TYA			;transfer Y to A
%GetEvent()  ;branches to GetEventFail in case it fails
LDA !EventsUnlocked,y  ;load event unlocked flags according to index
AND ReverseAND,x  ;check if respective indexed event is unlocked
BEQ .notunlocked		;if not unlocked, skip ahead.

PLX			;restore Y to X
LDA ReverseAND,x  ;load respective bit value from table according to index
TSB $18C7|!addr		;set them to mirror.
TXY			;transfer back to Y

PLX			;restore X
RTS			;return.

.notunlocked
PLX			;restore Y to X
LDA ReverseAND,x	;load respective bit value from table according to index
TRB $18C7|!addr		;clear them from mirror.
TXY			;transfer back to Y

PLX			;restore X
RTS			;return.

GetEventFail:
LDA #$2A
STA $1DFC|!addr
BRA CheckBeaten_notunlocked

ExitsPerLevel:
%ExitsPerLevel()

ReverseAND:
%ReverseAND()
