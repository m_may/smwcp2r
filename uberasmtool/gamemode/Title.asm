!titleram1 = $62		;trigger the number 2 in titlescreen
!titleram2 = $0F5E|!base2	;trigger the copyright text in titlescreen

!titlepointer = $0F5F|!base2	;title animation pointer
!titletimer = $0F60|!base2	;title animation timer

init:
LDA $0100|!addr
CMP #$05
BNE +
JMP TitleInit
+
CMP #$06
BNE +
JMP TitleInit
+
CMP #$08
BNE +
JMP SFXOnFileSelectOpen
+
RTL

TitleInit:
LDA #$01			;load value (display layer 1 only)
STA $212C : STA $212D	;store to main/subscreen registers.

LDA #$20			;load amount of frames
STA !titletimer		;store to title animation timer.

JSR NoInputsAllowed	;call routine to disable controller inputs
RTL			;return.

SFXOnFileSelectOpen:
STZ $2111
STZ $2111
STZ $2112
STZ $2112

LDA !titleram1		;load number 2 on titlescreen flag
CMP #$02			;compare to value
BEQ .return		;if equal, return.

LDA #$15			;load SFX value
STA $1DFC|!addr		;store to address to play it.

STZ $1DF5|!addr		;reset time to dismiss intro message.
INC !titleram1		;increment address by one
.return
RTL


main:
LDA $0100|!base2		;load game mode
CMP #$07			;compare to value
BNE +			;if not equal, branch.
JMP TitleMain		;go to titlescreen main code
+
RTL


TitleMain:
LDA !titletimer			;load titlescreen animation timer
BEQ +				;if equal zero, don't decrement it anymore.

DEC !titletimer			;decrement timer by one

+
LDA !titlepointer		;load titlescreen animation pointer
CMP #$08			;compare to value
BCS .ret			;if higher, return. (failsafe - avoid crashes)

XBA				;flip high/low bytes of A
JSR NoInputsAllowed		;call routine to disable controller inputs
XBA				;flip high/low bytes of A
JSL $0086DF|!bankB		;call 2-byte pointer subroutine
dw .title0		;$00 - wait
dw .title1		;$01 - move layer 1 upwards
dw .title2		;$02 - wait/execute thunder
dw .title3		;$03 - wait/revert thunder, show number 2, play sfx
dw .title2		;$04 - wait/execute thunder
dw .title5		;$05 - wait/revert thunder, show layer 2, play sfx, activate HDMA gradient
dw .title2		;$06 - wait/execute thunder
dw .title7		;$07 - wait/revert thunder, show layer 2, play sfx, activate HDMA gradient
dw .ret			;$08 - return (once again failsafe)

.title0
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

LDA #$2C			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one

.ret
RTL				;return.

.title1
LDA !titletimer			;load titlescreen animation timer
BNE +				;if not equal zero, move layer 1 upwards.

LDA #$10			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTL				;return.

+
LDA $13				;load frame counter
AND #$03			;preserve bits 0 and 1
BEQ .ret			;if none are set, return.

REP #$20			;16-bit A
INC $1464|!base2		;increment layer 1 Y-pos, next frame, by one
SEP #$20			;8-bit A
RTL				;return.

.title2
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

JSR WhiteFlash			;make screen flash
LDA #$0C			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTL				;return.

.title3
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

REP #$20			;16-bit A
LDA #$1111			;load value (display layer 1 and sprites only)
STA $212C			;store to main/subscreen registers.
STZ $0701|!base2		;reset back area color to black
SEP #$20			;8-bit A

INC !titleram1			;increment address - display number 2

LDA #$18			;load SFX value
STA $1DFC|!base2		;store to address to play it.

LDA #$40			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTL				;return.

.title5
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

.dopostthunder2
REP #$20			;16-bit A
LDA #$1717			;load value (display all layers and sprites)
STA $212C			;store to main/subscreen registers.
STZ $0701|!base2		;reset back area color to black
JSR TitleHDMA			;activate HDMA gradient for titlescreen (A goes 8-bit after this)

LDA #$18			;load SFX value
STA $1DFC|!base2		;store to address to play it.

LDA #$18			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one

.ret2
RTL				;return.

.title7
LDA !titletimer			;load titlescreen animation timer
BNE .ret2			;if not equal zero, return.

INC !titleram2			;increment address - display copyright text
BRA .dopostthunder2		;restore layers and HDMA after the thunder, then finish the animation

NoInputsAllowed:
REP #$20			;16-bit A
STZ $15			;disable controller data 1.
STZ $17			;disable controller data 2.
SEP #$20			;8-bit A
RTS			;return.

WhiteFlash:
REP #$20			;16-bit A
LDA #$77BD			;load color value
STA $0701|!base2		;store to back area color.
STZ $212C			;hide all layers.
SEP #$20			;8-bit A
STZ $0D9F|!base2		;disable all HDMA.
RTS				;return.

TitleHDMA:
LDA #.THDMA_Red			;load address value of red HDMA table
STA $00				;store to scratch RAM.
LDA #.THDMA_Green		;load address value of red HDMA table
STA $03				;store to scratch RAM.
LDA #.THDMA_Blue		;load address value of red HDMA table
STA $06				;store to scratch RAM.
SEP #$20			;8-bit A
LDA.b #.THDMA_Red>>16
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
JSL HDMA_Color3			;call HDMA (3 channels) handling routine
RTS				;return.

.THDMA_Red
db $60,$26
db $03,$27
db $03,$28
db $03,$29
db $03,$2A
db $03,$2B
db $03,$2C
db $03,$2D
db $03,$2E
db $03,$2F
db $03,$30
db $04,$31
db $03,$32
db $03,$33
db $03,$34
db $03,$35
db $03,$36
db $04,$37
db $03,$38
db $03,$39
db $03,$3A
db $03,$3B
db $04,$3C
db $03,$3D
db $03,$3E
db $34,$3F
db $00

.THDMA_Green
db $74,$45
db $17,$44
db $18,$43
db $3C,$42
db $00

.THDMA_Blue
db $68,$87
db $10,$86
db $11,$85
db $13,$84
db $43,$83
db $00
