;Goal/Death handling by Blind Devil
;Whenever $1696 is non-zero (except when it's #$FF), goal code will be run,
;and there will be animations, exit triggering, SMWC coin saving and whatnot.
;$1696 is the flag used to disable screen barrier before anyone asks.

;And if the player is dying, the respective (local) death song will play, and
;the death counter will be increased.
;Previously: And! Whenever the player beats Perilous Paths, $1F2B will be reset.
;WELL, NOT ANYMORE. This address is used as a bitflag holder for rooms cleared.

;NOTE: THERE IS A BUILT-IN HIJACK TO THE KEYHOLE CODE SO IT ALSO SAVES SMWC COINS!
;Disk Poppy note: No more - look at asm/patches/BeatLevel.asm

;Death Songs defines:
!W1 = $0A
!W2 = $0B
!W3 = $0C
!W4 = $0D
!W5 = $0E
!W6 = $0F
!W7 = $10
!W8 = $11
!W9 = $78
!W4b = $79	;because I forgot (not really, I'm just lazy - and yeah I know reef and desert are properly separate now lol)

main:
LDA $71			;load player animation pointer
CMP #$09		;check if player is dying
BNE nodead		;if not, skip ahead.

;Some random star/p-switch timer checks, no longer used because $9D gets set anyways

;LDA $1490|!addr
;BEQ +
;LDA #$FF
;STA $1490|!addr
;+
;LDA $14AD|!addr
;BEQ +
;LDA #$FF
;STA $14AD|!addr
;+
;LDA $14AE|!addr
;BEQ +
;LDA #$FF
;STA $14AE|!addr
;+

LDA $1496|!addr		;load (death) animation timer
CMP #$2D		;compare to value
BCS nodead		;if higher, skip ahead.
CMP #$2B		;compare to value
BCC nodead		;if lower, skip ahead.

LDA #$00		;load value
STA $7FB000		;store to AMK music mirror to allow local songs to be played again.
DEC $1496|!addr		;decrement player animation timer...
DEC $1496|!addr		;...twice

LDX $13BF|!addr		;load translevel number into X
LDA.l Death_Table,x	;load death song number from table according to index
STA $1DFB|!addr		;store to address to play it.

LDA $13CE|!addr		;load level's midpoint flag
BEQ +			;if not obtained, skip ahead.

LDA $1EA2|!addr,x	;load OW level setting flags from current level according to index
ORA #$40		;set the midway point flag
STA $1EA2|!addr,x	;store result back.

+
REP #$20		;16-bit A
LDA $010B|!addr
CMP #$0085		;if in sublevel 85 (erehps fake death)...
BEQ +			;don't increment it
SEP #$20
JSL ProgressCheck_is100ed	;if 100 percented the game
REP #$20
BCS +			;don't increment it
LDA !DeathCounter	;load death counter
CMP.w #999		;compare to value
BEQ +			;if equal, don't increment it.
INC			;increment A by one
STA !DeathCounter	;store result back.
+
SEP #$20		;8-bit A

JSL $009BC9|!bank	;save game

RTL			;return.

nodead:
LDA $1696|!addr		;load disable screen barrier flag
BEQ .ret		;if zero, return.
CMP #$0C		;compare again
BCS .ret		;if higher or equal, return (failsafe, avoid crashes).

JSR DisableInputs	;disable all controller inputs.

;LDX $13BF|!addr		;load translevel number into X
;CPX #$55		;compare to value (perilous paths level number)
;BNE +			;if not equal, don't erase $1F2B.

;STZ !PerilousPathsCleared  ;reset bitflag holder for perilous paths' rooms cleared.

;+
JSL $0086DF|!bank	;call 2-byte pointer subroutine
dw .ret			;$00: return
dw .settimer		;$01: set timer for action timer/trigger next action
dw .wait		;$02: wait
dw .peace		;$03: peace sign
dw .walkaway		;$04: walk away
dw .beatlvl		;$05: beat the level (normal exit)
dw .settimer		;$06: set timer for action timer/trigger next action
dw .wait		;$07: wait
dw .peace		;$08: peace sign
dw .walkaway		;$09: walk away
dw .beatsecret		;$0A: beat the level (secret exit)
dw .beatboss		;$0B: beat the level (normal exit, boss version, player doesn't walk)

.settimer
STZ $13D4|!addr		;reset pause flag, just to be sure.
STZ $86			;disable slippery level flag.

LDA #$6D		;load value					;<< RELEVANT NEXT ACTION
STA $14AC|!addr		;store to unused fourth-frame decrementer.

INC $1696|!addr		;increment screen barrier flag
.ret
RTL			;return.

.wait
LDA $14AC|!addr		;load fourth-frame decrementer
BEQ .next		;if equal zero, do next action.

;idle,walk right,face left/look up
CMP #$43
BCC .wlkrightslow
CMP #$44
BEQ .wlkright
CMP #$64
BCC .lookup
CMP #$67
BCC .faceleft
CMP #$6C
BCC .wlkright
RTL			;return.

.wlkrightslow
STZ $1412|!addr		;disable vertical scroll.

LDX $187A|!addr		;load riding yoshi flag
BEQ .noyoshi1		;if not riding, skip ahead.

LDY $18DF|!addr		;load yoshi index
DEY			;decrement by one
LDA $76			;load player direction
EOR #$01		;flip bit 0
STA $157C,y		;store to sprite direction.

.noyoshi1
LDA #$05		;load value
STA $7B			;store to player's X speed.
RTL			;return.

.wlkright
LDA #$01
STA $15
RTL			;return.

.lookup
LDX $187A|!addr		;load riding yoshi flag
BEQ .noyoshi2		;if not riding, skip ahead.

LDY $18DF|!addr		;load yoshi index
DEY			;decrement by one
LDA $76			;load player direction
EOR #$01		;flip bit 0
STA $157C,y		;store to sprite direction.

.noyoshi2
LDA #$08
STA $15
RTL			;return.

.faceleft
LDA #$02
STA $15
RTL			;return.

.next
LDA #$18		;load value
STA $14AC|!addr		;store to unused fourth-frame decrementer.

LDX #$01		;load value into X
STX $76			;store to player direction. force them to face right.

INC $1696|!addr		;increment screen barrier flag

+
RTL			;return.

.peace
LDA $14AC|!addr		;load fourth-frame decrementer
BEQ +			;if equal zero, run next action.

LDA #$26		;load pose value

LDX $187A|!addr		;load riding yoshi flag into X
BEQ .storepose		;if not riding, skip ahead.

LDA #$14		;load pose value

.storepose
STA $13E0|!addr		;store to player pose address.
RTL			;return.

+
LDA #$20		;load value
STA $14AC|!addr		;store to unused fourth-frame decrementer.
INC $1696|!addr		;increment screen barrier flag
RTL			;return.

.walkaway
LDA $14AC|!addr		;load fourth-frame decrementer
BNE +			;if not equal zero, make Mario walk to the right.

INC $1696|!addr		;increment screen barrier flag
RTL			;return.

+
LDA #$08		;load value
STA $7B			;store to player's X speed.
RTL			;return.

.beatlvl
LDA #$01		;load value to set normal exit

.beat
LDY #$0B
PEA $84CE		;so that the RTS takes us to RTL
JML $00C9FE|!bank	;ends in RTS

.beatsecret
LDA #$02		;load value to set secret exit
BRA .beat		;branch to end level

.beatboss
LDA $71			;load player animation pointer
CMP #$09		;check if dying
BEQ .novictoryforya	;if yes, return. no victory for ya.

LDA $14AC|!addr		;load fourth-frame decrementer
BEQ .setdec		;if equal zero, set it to some value.
DEC			;decrement A by one
BEQ .beatlvl		;if equal #$01, beat the level and warp back to overworld.
CMP #$60		;compare to value
BCS .setsong		;if higher or equal, just set the song.

LDA #$26		;load pose value
STA $13E0|!addr		;store to player pose address.

.novictoryforya
RTL

.setdec
LDA #$90		;load value
STA $14AC|!addr		;store to victory timer.

.setsong
LDA #$03		;load song to play
STA $1DFB|!addr		;play it lol
RTL			;return.

DisableInputs:
LDX #$03		;load amount of frames
STX $1497|!addr		;store to make the player immune to other sprites.

STZ $15
STZ $16
STZ $17
STZ $18

LDX $187A|!addr		;load riding Yoshi flag
BEQ +			;if not riding, skip ahead.

LDX $18DF|!addr		;load index of Yoshi, plus one, into X
DEX			;decrement X by one
BMI +			;if negative, skip ahead.

STZ $157C,x		;reset sprite direction - face right.

+
RTS

Death_Table:
	db $00										; level 0
	db !W1,!W1,!W1,!W1,!W1,!W1,!W1,!W1,!W1						; World 1, levels 001-009
	db !W2,!W2,!W2,!W2,!W2,!W2,!W2,!W2						; World 2, levels 00A-011
	db !W3,!W3,!W3,!W3,!W3,!W3,!W3							; World 3, levels 012-018
	db !W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4b,!W4b,!W4b,!W4b,!W4b,!W4b,!W4b,!W4b,!W4b     ; Worlds 4 and 5, levels 019-024 + 101-104
	db !W7										; World 8, level 105
	db $00,$00,$00,$00,$00,$00,$00,$00,$00						; World ?, levels 106-10E
	db !W5,!W5,!W5,!W5,!W5,!W5,!W5							; World 6, levels 10F-115
	db !W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6				; World 7, levels 116-121
	db !W7,!W7,!W7,!W7,!W7,!W7,!W7,!W7,!W7						; World 8, levels 122-12A
	db !W8,!W8,!W8,!W8,!W8,!W8,!W8,!W8						; World 9, levels 12B-132
	db !W9,!W9,!W9,!W9,!W9,!W9,!W9,!W9,!W9,!W9					; World S, levels 133-13B
