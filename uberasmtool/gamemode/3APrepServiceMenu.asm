;Custom Game Mode: Prepare File Service Menu (set up tilemap)
;By Disk Poppy

table "../lg1texttable.txt"

!Message = $7C  ;has to be the same as in 3CServiceMenu.asm

!SaveSlotTilemapLength = $01
!SaveSlotTilemap = $0F5F|!addr   ;ram for the current save slot

!decompBuffer = $7EAD00

main:
STZ !Message  ;not in 3C gamemode's init, as init seems to run after nmi

STZ $1B92|!addr  ;cursor pos

JSL $7F8000  ;move all sprite tiles offscreen
REP #$20
;cursor
LDA #$1810
STA $0200|!addr
LDA #$343D
STA $0202|!addr
;tick
LDA #$F0D3
STA $0204|!addr
LDA #$3ACA
STA $0206|!addr
SEP #$20
LDA #$0F
TRB $0400|!addr

LDA $010A|!addr  ;current save file number
CLC : ADC #$0A
STA !SaveSlotTilemap

STZ $4200  ;disable nmi
LDA #$80   ;load value
STA $2100  ;store to register to force blank.

;Disable previous HDMA
;STZ $0D9F|!addr
STZ $420C

JSL DisableExAnimation_run ;16-bit A

STZ $1A
STZ $1C
STZ $1E
STZ $20
STZ $22
STZ $24

STZ $0701  ;set fixed color

;Enable Service Menu DMA
LDA #$3200  ;mode 0, register $2132
STA $4330
LDA.w #GreenTable
STA $4332
LDX.b #GreenTable>>16
STX $4334
LDX #$08
STX $0D9F|!addr

;Palette DMA clear to black
LDX.b #ServiceMenuPalette_Pal2>>16 ;\Bank of palette
STX $4324                          ;/

LDX #$20
STX $2121
LDA #$220A              ;\One register write twice, to $2122, fixed
STA $4320               ;/
LDA.w #ServiceMenuPalette_PalBlack
STA $4322
LDA.w #$00C0            ;\Bytes to upload
STA $4325               ;/
LDX #$04                ;\Start DMA
STX $420B               ;/

;Palette DMA
LDX #$02                ;\One register write twice
STX $4320               ;/

LDA.w #ServiceMenuPalette_Pal2-8
STA $07
LDX #$29
STX $0A
LDX #$05
-
SEP #$20
LDA $0A
STA $2121               ;Set Address for CG-RAM Write
CLC : ADC #$10
STA $0A
REP #$20
LDA #$0008              ;\Bytes to upload
STA $4325               ;/
CLC : ADC $07           ;\Address of palette to upload
STA $4322               ;/
STA $07
LDY #$04                ;\Start DMA
STY $420B               ;/
DEX
BPL -

;Layer 2 Tilemap DMA
;REP #$20		;16-bit A
LDA.w #!decompBuffer
STA $00
LDX.b #!decompBuffer>>16
STX $02
LDA.w #$0405  ;ExGFX File
JSL $0FF900|!bank  ;Lunar Magic's decompression routine

REP #$20			;16-bit A
LDA #$0800			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #!decompBuffer		;load absolute address of tilemap table
STA $07				;store to scratch RAM.
LDA.w #$3800		;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

;Layer 2 GFX DMA
REP #$20		;16-bit A
LDA.w #!decompBuffer
STA $00
LDX.b #!decompBuffer>>16
STX $02
LDA.w #$0403  ;ExGFX File
JSL $0FF900|!bank  ;Lunar Magic's decompression routine

REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #!decompBuffer	;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$0800		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

;Layer 3 Tilemap DMA
REP #$20
LDA #$0400		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #Data>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #Tile_Empty  ;load absolute address of tile
STA $07			;store to scratch RAM.
LDA #$5000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_Low  ;execute DMA (A goes 8-bit after this routine)

REP #$20		;16-bit A
LDA.w #Props_Normal  ;load absolute address of properties
STA $07			;store to scratch RAM.
JSL DMA_VRAM_SingleValue_High  ;execute DMA (A goes 8-bit after this routine)

;Layer 3 Tilemap upload stripes
REP #$30
LDY #$0000
-
LDA Layer3Pointers+3,y
AND #$00FF
STA $00
LDA Layer3Pointers,y
STA $07
LDA Layer3Pointers+1,y
STA $08
LDA Layer3Pointers+4,y
STA $0A
SEP #$10
JSL DMA_VRAM_Low
REP #$31
TYA
ADC #$0006
TAY
CPY.w #datasize(Layer3Pointers)
BCC -
SEP #$30

LDA #$FF         ;load value
STA $1DFB|!addr  ;store to music address to make the tile screen song fade out.

;set main screen and subscreen
LDA #$16
STA $212C
STA $212E
STZ $212D
STZ $212F

;enable color math for backdrop and layer 2
LDA #$22
STA $40

;enable nmi
LDA #$81
STA $4200

INC $0100|!addr  ;increment game mode by one
RTL

Data:

Props:
.Normal
db $30
Tile:
.Empty
db $FC

; format : dl <src> : db <length> : dw <vram dest>
; higher get uploaded first
Layer3Pointers:
dl MarioTilemap : db datasize(MarioTilemap) : dw $502C
dl ReturnTilemap : db datasize(ReturnTilemap) : dw $5063
dl ExitsCheckTilemap : db datasize(ExitsCheckTilemap) : dw $5083
dl SMWCCoinsCheckTilemap : db datasize(SMWCCoinsCheckTilemap) : dw $50A3
dl ResetPlayerTilemap : db datasize(ResetPlayerTilemap) : dw $50C3
dl ResetOWTilemap : db datasize(ResetOWTilemap) : dw $50E3
dl MakeBackupTilemap : db datasize(MakeBackupTilemap) : dw $5103
dl RestoreBackupTilemap : db datasize(RestoreBackupTilemap) : dw $5123
dl ConvertTilemap : db datasize(ConvertTilemap) : dw $5143
dl !SaveSlotTilemap : db !SaveSlotTilemapLength : dw $5032
Layer3PointersEnd:

MarioTilemap:
db "MARIO"
ReturnTilemap:
db "RETURN TO TITLE"
ExitsCheckTilemap:
db "EXITS CHECK"
SMWCCoinsCheckTilemap:
db "SMWC COINS CHECK"
ResetPlayerTilemap:
db "RESET PLAYER POS IN OW"
ResetOWTilemap:
db "RESET OVERWORLD"
MakeBackupTilemap:
db "MAKE BACKUP"
RestoreBackupTilemap:
db "RESTORE BACKUP"
ConvertTilemap:
db "CONVERT FILE"
TilemapsEnd:

GreenTable:
db $13,$49
db $13,$48
db $13,$47
db $13,$46
db $13,$45
db $13,$44
db $13,$43
db $02,$4C
db $03,$54
db $03,$53
db $03,$52
db $03,$51
db $03,$50
db $03,$4F
db $03,$4E
db $03,$4D
db $03,$4C
db $03,$4B
db $03,$4A
db $03,$49
db $03,$48
db $03,$47
db $03,$46
db $03,$45
db $03,$44
db $03,$43
db $03,$42
db $03,$41
db $01,$40
db $00
