;Custom Game Mode: Prepare TNK mode (set up sprite)
;By Disk Poppy

!decompBuffer = $7EAD00

main:
LDA #$FF
STA $1DFB|!addr ;make song fade out.

JSL $7F8000  ;move all sprite tiles offscreen
REP #$20
;deedleball sprite
LDA #$6680
STA $0200|!addr
LDA #$350A
STA $0202|!addr
SEP #$20
LDA #$03
TRB $0400|!addr
LDA #$02
TSB $0400|!addr

STZ $4200  ;disable nmi
LDA #$80   ;load value
STA $2100  ;store to register to force blank.

;Disable previous HDMA
STZ $0D9F|!addr
STZ $420C

JSL DisableExAnimation_run ;16-bit A

;Palette DMA
LDX #$A0
STX $2121
LDA #$2202              ;\One register write twice, to $2122
STA $4320               ;/
LDA.w #Pal
STA $4322
LDX.b #Pal>>16          ;\Bank of palette
STX $4324               ;/
LDA.w #$0020            ;\Bytes to upload
STA $4325               ;/
LDX #$04
STX $420B

;Sprite GFX DMA
REP #$20		;16-bit A
LDA.w #!decompBuffer
STA $00
LDX.b #!decompBuffer>>16
STX $02
LDA.w #$01D0  ;ExGFX File
JSL $0FF900|!bank  ;Lunar Magic's decompression routine

REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #!decompBuffer>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #!decompBuffer	;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$7000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_VRAM		;execute DMA (A goes 8-bit after this routine)

;set main screen and subscreen
LDA #$10
STA $212C
STA $212E
STZ $212D
STZ $212F

;enable nmi
LDA #$81
STA $4200

INC $0100|!addr  ;increment game mode by one
RTL

Pal:
dw $0000,$7BFD,$0000,$4544,$5DE8,$6A8C,$7EAF,$18CD
dw $2D9A,$3A7D,$4B1E,$575E,$2534,$7394,$77DA,$77DA
