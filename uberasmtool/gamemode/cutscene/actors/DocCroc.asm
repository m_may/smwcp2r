!docCrocState = !actorVar1,x
!docCrocXPos = !actorVar2,x
!docCrocYPos = !actorVar3,x
!docCrocTimer = !actorVar4,x

!docCrocArmsBehindHead = $1FEF
!docCrocNice = $1FEE
!docCrocPropellorFrame = $1FED
!docCrocFlyVSpeed = $1FEC

DocCrocSetToWalking:
	LDX !actorSlot+!DocCroc
	LDA #$00
	STA !docCrocState
	RTL
	
DocCrocSetToFacingCamera:
	LDX !actorSlot+!DocCroc
	LDA #$01
	STA !docCrocState
	RTL

DocCrocSetToPointing:
	LDX !actorSlot+!DocCroc
	LDA #$02
	STA !docCrocState
	RTL
	
DocCrocSetToHat:
	LDX !actorSlot+!DocCroc
	LDA #$03
	STA !docCrocState
	LDA #$00
	STA !docCrocTimer
	LDA #$6A
	STA !docCrocPropellorFrame
	RTL
	
DocCrocSetToFlying:
	LDX !actorSlot+!DocCroc
	LDA #$04
	STA !docCrocState
	LDA #$00
	STA !docCrocFlyVSpeed
	RTL
	
DocCrocSetToFacingAwayFromCamera:
	LDX !actorSlot+!DocCroc
	LDA #$05
	STA !docCrocState
	RTL
	
DocCrocSetToStandingArmsOut:
	LDX !actorSlot+!DocCroc
	LDA #$07
	STA !docCrocState
	RTL
	
DocCrocSetXPos:
	LDX !actorSlot+!DocCroc
	STA !docCrocXPos
	RTL
	
DocCrocSetToStanding:
	LDX !actorSlot+!DocCroc
	LDA #$06
	STA !docCrocState
	RTL
	
	
DocCrocSetYPos:
	LDX !actorSlot+!DocCroc
	STA !docCrocYPos
	RTL
	
DocCrocSetArmsBehindHead:
	LDA #$01
	STA !docCrocArmsBehindHead
	STZ !docCrocNice
	RTL
	
DocCrocSetArmsNormal:
	STZ !docCrocArmsBehindHead
	STZ !docCrocNice
	RTL
	

DocCrocINIT:
	LDA #$3F
	STA !docCrocYPos
	RTL

DocCrocMAIN:
	STZ $0D
	PHK
	PLB

	PHX				; Jump to the Doc's current routine.
	LDA !docCrocState		;
	ASL
	TAX
	LDA DocCrocStateTable,x		;
	STA $00				;
	LDA DocCrocStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL

DocCrocNMI:
	LDA !actorVar3,x
	BNE +
	LDA.b #!layer1MapAddr>>11<<2	; \
	STA $2107			; /
	LDA #$01
	STA !actorVar3,x
	+
	RTL
	

DocCrocStateTable:
dw DocCrocWalk, DocCrocFaceCamera, DocCrocPoint, DocCrocHat, DocCrocFly, DocCrocFaceAway, DocCrocStand, DocCrocStandArmsOut

DocCrocSideMouthTiles:
db $2E, $48, $4A, $48

DocCrocFrontMouthTiles:
db $2E, $4C, $4E, $4C

GetDocCrocSideMouthTile:
	LDA !textIsAppearing
	BNE .openMouth
	LDA #$2E
	RTS
.openMouth
	PHX
	LDA $13
	AND #$0C
	LSR #2
	TAX
	LDA DocCrocSideMouthTiles,x
	PLX
	RTS

GetDocCrocFrontMouthTile:
	LDA !textIsAppearing
	BNE .openMouth
	LDA #$2E
	RTS
.openMouth
	PHX
	LDA $13
	AND #$0C
	LSR #2
	TAX
	LDA DocCrocFrontMouthTiles,x
	PLX
	RTS

DocCrocDrawTile:
	PHA
	LDA !cutsceneNumber
	CMP #$0C
	BNE +
	LDA $02
	CLC
	ADC #$80
	STA $02
+
	PLA
	JSL DrawTile
	RTS

DocCrocWalk:
{
	LDA $13
	AND #$01
	BEQ +
	REP #$20
	DEC $1A
	SEP #$20
	+
	
	LDA $13
	AND #$03
	BNE +
	REP #$20
	DEC $1E
	SEP #$20
	+
	
	LDA $13
	AND #$10
	LSR #4
	STA $0D		; Bob y offset
	
	; $00 = x pos
	; $01 = y pos
	; $02-$03 = tile
	; $04 = yxppccct
	; $05 = index (generally just an STX, INX STX, etc.)
	
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$00 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$02 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA $13 : AND #$18 : LSR #1 : CLC : ADC #$60 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA $13 : AND #$18 : LSR #1 : CLC : ADC #$62 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	RTS
}
	
DocCrocFaceCamera:
{
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocFrontMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$04 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$06 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$24 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$26 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	RTS
}

DocCrocPoint:
{
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$00 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$02 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$0E : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$22 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	RTS
}

	
CrocHatY:
db $02, $01, $00, $FF, $FE, $FD, $FC, $FC, $FB, $FB, $FA, $FA, $F9, $F9, $F9, $FA
db $FA, $FA, $FB, $FB, $FC, $FC, $FB, $FB

DocCrocHat:
{
	LDA !docCrocTimer
	CMP #$10
	BCS .drawExtrasAbove
	JSR DocCrocStand
	JSR .drawDressingExtras
	BRA +
.drawExtrasAbove
	JSR .drawDressingExtras
	JSR DocCrocStand
+


;	LDA !docCrocTimer
;	INC
;	STA !docCrocTimer
;	CMP #23
;	BNE +
;	LDA !docCrocState
;	INC
;	STA !docCrocState
;	LDA #$00
;	STA !docCrocTimer
;+

	RTS
	
.drawDressingExtras

	LDA !docCrocXPos
	CLC
	ADC #$09
	STA $00
	
	PHX
	LDA !docCrocTimer
	CMP #$17
	BCS +
	INC
	STA !docCrocTimer
	DEC
+
	TAX
	LDA CrocHatY,x
	SEC
	SBC #$08
	PLX
	CLC
	ADC !docCrocYPos
	STA $01
	
	LDA !docCrocPropellorFrame
	STA $02
	
	LDA #$3A
	STA $03
	
	;INC $04
	
	LDA #$02
	JSR DocCrocDrawTile
	
	;INC $04
	
	RTS
}	
	

DocCrocFly:
	LDA $13
	AND #$07
	BNE +
	INC !docCrocFlyVSpeed
	LDA !docCrocFlyVSpeed
	CMP #$04
	BCC +
	LDA #$04
	STA !docCrocFlyVSpeed
+
	
	LDA !docCrocYPos
	SEC
	SBC !docCrocFlyVSpeed
	STA !docCrocYPos
	BPL +
	CMP #$90
	BCS +
	LDA #$00
	STA !actorExists,x
	RTS
+
	
	LDA !docCrocPropellorFrame
	INC
	INC
	CMP #$70
	BCC +
	LDA #$68
+
	STA !docCrocPropellorFrame
	STA $02
	
	LDA !docCrocYPos
	CLC
	ADC #$F3
	STA $01
	
	LDA !docCrocXPos
	CLC
	ADC #$09
	STA $00
	
	LDA #$3A
	STA $03
	
	LDA #$02
	
	JSR DocCrocDrawTile
	
	
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$44 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$46 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$64 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$66 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Feet left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$4C : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Feet right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$4E : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
RTS

DocCrocFaceAway:

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$40 : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$42 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$60 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$62 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	
	RTS
	
DocCrocStand:

	LDA !docCrocArmsBehindHead
	BNE .armsBehindHead
	
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$00 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$02 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$20 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$22 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	RTS
	
.armsBehindHead
	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$08 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA $13 : AND #$10 : LSR #3 : CLC : ADC #$0A : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$28 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA $13 : AND #$10 : LSR #3 : CLC : ADC #$2A : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	RTS
	
DocCrocStandArmsOut:

	; Draw the mouth tile (if necessary)
	; Needs to be first for priority
	LDA !docCrocXPos : CLC : ADC #$08 : STA $00
	LDA !docCrocYPos : CLC : ADC #$08 : CLC : ADC $0D :  STA $01
	JSR GetDocCrocSideMouthTile : STA $02
	LDA #$3A : STA $03
	LDA #$02 : JSR DocCrocDrawTile

	; Position for the top left tile
	LDA !docCrocXPos : STA $00
	LDA !docCrocYPos : CLC : ADC $0D : STA $01
	
	
	; First, draw the top left tile.
	LDA #$04 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Top right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$06 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom left
	LDA $00 : SEC : SBC #$10 : STA $00
	LDA $01 : CLC : ADC #$10 : STA $01
	LDA #$24 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	
	; Bottom right
	LDA $00 : CLC : ADC #$10 : STA $00
	LDA #$26 : STA $02
	LDA #$02 : JSR DocCrocDrawTile
	RTS

	; Returns the correct body tile in A (for normal, arms behind head, nice, pointing)