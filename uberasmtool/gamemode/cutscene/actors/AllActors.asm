!cutsceneNumber = #$00

!marioNextState = $1715			; If positive, this is the state Mario will go to upon finishing his current one (if it's one that has a timer or other finishing condition).
!marioTargetXPos = $1716		; When Mario is variably walking left or right, this is the point at which he'll stop.	
!marioJumpBase = $1717			; The height Mario originally started jumping at.
!marioAboveHead = $1718			; The tile to draw above Mario's head.  If 0, don't draw anything.
!marioFlipX = $1719			; If set, flip Mario x-wise.
!marioRunning = $171A			; If set, Mario runs instead of walking
!marioMovementSpeed = $171B		
!marioJumpCount = $171C

!docCrocArmsBehindHead = $171D
!docCrocNice = $171E
!docCrocPropellorFrame = $171F
!docCrocFlyVSpeed = $1720

; Truntec is complex...
!truntecLight1XPos = $1721
!truntecLight2XPos = $1722
!truntecLight1YPos = $1723
!truntecLight2YPos = $1724
!truntecBeam1XPos = $1725
!truntecBeam2XPos = $1726
!truntecStateTime = $1727
!truntecLightStartX = $1728		; The starting position of the scanner lights.
!truntecStartMovingLightsBack = $1729	
!truntecEyeMode = $172A			; 0 = normal, 1 = hourglass, 2 = '!'
!truntecEyeIndex = $172B		; The index of the hourglass animation frame

!dryadTargetX = $172C			; The target X position of the dryad when in the moving state.
!dryadTargetY = $172D			; The target Y position of the dryad when in the moving state.
!dryadTopLeftBodyTile = $172E		; The top left body tile.
!dryadTopRightBodyTile = $172F		;
!dryadBotLeftBodyTile = $1730		;
!dryadBotRightBodyTile = $1731		;
!dryadMouthTile = $1732			; The top left mouth tile.
!dryadTopLeftPlantTile = $1733		; The top left plant base tile.
!dryadStopMoving = $1734		; 
!dryadBaseMouth = $1735			; The base tile for the dryad's mouth
!dryadFaceMario = $1736			; If clear, the dryad will spin while swinging around the room.  Otherwise she'll face Mario.


!barkerSpinningCane = $1737		; Set if the barker duck is spinning his cane
!barkerTopLeftTile = $1738
!barkerTopRightTile = $1739
!barkerBottomLeftTile = $173A
!barkerBottomRightTile = $173B
!barkerExtraCaneTile = $173C
;!barkerArmState = $1738			; 0 = normal, 1 = point to Mario, 2 = holding up, 3 = behind (holding Luigi on cane)
;!barkerAnimState = $1739		; 0 = standing, 1 = throwing Luigi, 2 = grabbing Luigi, 3 = jumping offscreen, 4 = running away
;!barkerFeetState = $173A		; 0 = standing, 1 = jumping up, 2 = jumping down, 3 = running
!barkerXFlip = $173D			; Set to #$80 if the barker if flipped horizontally
;!barkerCaneState = $173C		; Which cane frame to use.  Overwritten if !barkerSpinningCane is set
!barkerDrawGoomba = $173F		; If set, the barker draws the goomba.
;!barkerHeadFacingBack = $173F		; If set, the barker's head faces the opposite direction his body does.
!barkerLuigiDrawState = $1740		; 00 = don't draw Luigi, 01 = draw scared Luigi in barrel, 02 = draw Luigi hanging from cane
!barkerScrollScreen = $1741		; If set, the screen scrolls
!barkerLookBackRunning = $1742

!marioSetToFallThroughGround = $1743	; If set, Mario will fall through the ground.  Always set it before turning on gravity.

!dryadBobCounter = $1744


!DocCroc = $00
!MadamMau1 = $01
!Hindenbird = $03
!Luigi = $04
!Fish = $05
!Toad = $06
!Mario = $07

incsrc "DocCroc.asm"
incsrc "MadamMau1.asm"
incsrc "Hindenbird.asm"
incsrc "Luigi.asm"
incsrc "Fish.asm"
incsrc "Toad.asm"
incsrc "Mario.asm"
