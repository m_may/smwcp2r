MadamMau1INIT:
	RTL

MadamMau1MAIN:
	PHK
	PLB
	
	LDA #$78		; \
	STA $00			; / X pos
	
	LDA #$3F		; \ Y pos
	STA $01			; /
	
	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$42		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$0C		; |
	LSR			; | Otherwise, animate the mouth.
	CLC			; |
	ADC #$40		; /
	
+
	STA $02
	
	LDA #$3C		; \
	STA $03			; / Property
	
	;STX $04
	LDA #$02
	JSL DrawTile


	LDA #$4F		; \ Y pos
	STA $01			; /
	
	LDA #$0C		; \
	STA $02			; / Tile
	
	;INC $04			; Index
	
	LDA #$02		; Size
	
	JSL DrawTile		;


	LDA #$37		; \ Y pos
	STA $01			; /

	LDA #$7C		; \ X pos
	STA $00			; /

	LDA #$3C		; \
	STA $02			; / Tile

	;INC $04			; Index

	LDA #$00		; Size

	JSL DrawTile		;


	LDA #$57		; \ Y pos
	STA $01			; /

	LDA #$86		; \ X pos
	STA $00			; /
	
	LDA #$2C		; \
	STA $02			; / Tile
	
	;INC $04			; Index
	
	LDA #$00		; Size
	
	JSL DrawTile		;


	RTL
MadamMau1NMI:
	RTL