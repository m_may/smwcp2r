!toadXPos = !actorVar1,x
!toadYPos = !actorVar2,x
!toadState = !actorVar3,x
!toadTimer = !actorVar4,x
!toadSad = !actorVar5,x		; Set the highest bit if Toad should be using his sad face.  Every other bit counts for the sprites' properties.

ToadSetToUnSad:
	LDX !actorSlot+!Toad
	LDA #$00
	STA !toadSad
	RTL

ToadINIT:
	LDA #$00
	STA !toadTimer
	STA !toadState
	LDA #$40
	STA !toadYPos
	LDA #$F0
	STA !toadXPos
	LDA #$80
	STA !toadSad
	RTL
	

ToadStateTable:
dw ToadRunningToLeft, ToadBouncingBack, ToadStunned, ToadShaking, ToadStanding

ToadMAIN:
	PHK
	PLB
	
	STZ $05
	;STX $04
	PHX				; Jump to the Doc's current routine.
	LDA !toadState			;
	ASL
	TAX
	LDA ToadStateTable,x	;
	STA $00				;
	LDA ToadStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
ToadBouncingBack:
	LDA #$03
	STA $05
	
	LDA !toadXPos
	INC
	STA !toadXPos

	STZ $00
	STZ $01
	LDA !toadTimer
	SEC
	SBC #17
	BPL +
	;INC $00
	EOR #$FF
	INC A
	+
	STA $4202
	STA $4203
	NOP #3
	REP #$20
	LDA $4216
	LSR
	LSR
	LSR
	PHA
	;LDA $00
	;BEQ +
	;PLA
	;EOR #$FFFF
	;INC
	;BRA ++
;+
	PLA
;++
	CLC
	ADC.w #26
	SEP #$20
	STA !toadYPos
	
	LDA !toadTimer
	INC
	STA !toadTimer

	;LDA !toadTimer
	CMP #$08
	BCC +
	LDA !toadYPos
	CMP #$40
	BCC +
	LDA #$40
	STA !toadYPos
	LDA !toadState
	INC
	STA !toadState
	LDA #$01
	STA $2140
	LDA #39
	STA !toadTimer
+
	LDA !toadTimer
	CMP #$17
	BNE +
	
	JSL LuigiSetToFacingRight
	+
	LDA #$48
	PHA
	LDA #$28
	PHA
	JMP ToadStandardDraw
	
ToadStunned:
	LDA #$03
	STA $05
	LDA !toadTimer
	DEC
	STA !toadTimer
	BNE +
	LDA #40
	STA !toadTimer
	LDA !toadState
	INC
	STA !toadState
+
	LDA #$4A
	PHA
	LDA #$2A
	PHA
	JMP ToadStandardDraw
	
ToadShaking:
	LDA #$03
	STA $05
	LDA !toadTimer
	DEC
	STA !toadTimer
	BNE +
	LDA #50
	STA !toadTimer
	LDA !toadState
	INC
	;LDA #$00
	STA !toadState
	;STA !toadTimer
+
	LDA $13
	AND #$04
	LSR
	STA $00
	CLC
	ADC #$4C
	PHA
	LDA #$2C
	CLC
	ADC $00
	PHA
	JMP ToadStandardDraw
	
ToadStanding:
	STZ !forcePause
	LDA #$06
	PHA
	LDA #$2A
	PHA
	JMP ToadStandardDraw
	
	
	
		
ToadRunningToLeft:
	LDA #$01
	STA !forcePause
	
	;LDA $13
	;AND #$01
	;STA $00
	
	LDA !toadXPos
	;DEC
	SEC
	SBC #$02
	STA !toadXPos
	CMP #$9A
	BCS +
	LDA !toadState
	INC
	STA !toadState
	LDA #$03
	STA $2140
	+
	
	LDA $13
	AND #$04
	BEQ +
	INC $05
+
	LSR
	CLC
	ADC #$02
	PHA
	
	LDA #$2A
	PHA
	
ToadStandardDraw:		; Draw Toad.  Push the body then head tiles onto the stack first.  The mouth will be drawn automatically.

	LDA !toadState
	CMP #$04
	BNE .noDrawMouth
				; Draw Toad's mouth.
	LDA !textIsAppearing
	BEQ +
	LDA $13
	AND #$04
	LSR
	LSR
+
	CLC
	ADC #$40
	STA $02
	LDA !toadSad
	BPL +
	LDA $02
	CLC
	ADC #$10
	STA $02	
+
	LDA !toadXPos
	STA $00
	LDA !toadSad
	AND #$40
	BEQ +
	LDA $00
	CLC
	ADC #$08
	STA $00
+
	LDA !toadYPos
	CLC
	ADC #$10
	STA $01
	LDA !toadSad
	AND #$7F
	EOR #$31
	STA $03
	LDA #$00
	JSL DrawTile
	

.noDrawMouth
	LDA !toadXPos
	STA $00
	LDA !toadYPos
	CLC
	ADC $05
	STA $01
	PLA
	STA $02
	LDA !toadSad
	AND #$7F
	EOR #$31
	STA $03
	LDA #$02
	JSL DrawTile
	
	LDA $01
	CLC
	ADC #$10
	STA $01
	PLA
	STA $02
	LDA #$02
	JSL DrawTile
	RTS
	
	
ToadNMI:
	;LDA !hindenBirdNMIInit
	;BNE +
	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
	;STA $2107				; /
	;LDA #$01
	;STA !hindenBirdNMIInit
	+
RTL
