!marioXPos = $E4,x
!marioYPos = $D8,x
!marioState = !actorVar3,x
!marioTimer = !actorVar4,x
!marioProps = !actorVar5,x
!marioPose = $13E0
		

MarioBodyFrames:
;   w1, w2,  w3,  w4,  jmp, fall,spct,hurt,really?,!, mad, sad
db $80, $82, $84, $82, $E4, $E0, $80, $8E, $80, $86, $86, $80, $00, $00, $00, $00
db $E8, $CA, $C8, $CA, $EA, $EA

MarioHeadFrames:
db $A0, $A0, $A0, $A0, $C4, $C0, $A6, $AC, $CE, $AE, $AE, $A2, $00, $00, $00, $00
db $A8, $A8, $A8, $A8, $A8, $A8


MarioSetWaitStateToNothing:
	LDA #$00
	STA !marioNextState
	RTL
	
MarioSetWaitStateToFreeze:
	LDA #$05
	STA !marioNextState
	RTL
	
MarioSetToConfused:
	LDA #$08
	STA !marioPose
	LDX !actorSlot+!Mario
	LDA #$20
	STA !marioTimer
	JSL MarioAddEllipsis
	JSL MarioSetToFrozen
	RTL
	
MarioSetToAngry:
	LDA #$09
	STA !marioPose
	JSL MarioAddExclamationMark
	JSL MarioSetToFrozen
	RTL
	
MarioSetWaitStateToWalkOffscreenRight:
	LDA #$01
	STA !marioNextState
	RTL
	
MarioSetTargetX:
	STA !marioTargetXPos
	RTL

MarioDoStateSwitchCheck:
	LDA !marioNextState
	BMI +
	LDX !actorSlot+!Mario
	STA !marioState
	LDA #$80
	STA !marioNextState
	
	LDA #$00
	STA !marioTimer
	
	+
	LDA !marioState
	RTS

MarioINIT:
	LDA #$40
	STA !marioYPos
	LDA #$40
	STA !marioProps
	LDA #$02
	STA !marioMovementSpeed
	RTL
	
MarioSetPoseToSuspicion:
	LDX !actorSlot+!Mario
	LDA #$06
	STA !marioPose
	RTL
	
MarioSetPoseToStanding:
	LDX !actorSlot+!Mario
	STZ !marioPose
	RTL
	
	
MarioSetToFacingRight:
	LDX !actorSlot+!Mario
	LDA #$40
	STA !marioProps
	RTL
	
MarioSetToFacingLeft:
	LDX !actorSlot+!Mario
	LDA #$00
	STA !marioProps
	RTL
	
MarioSetToWalkingRight:
	LDX !actorSlot+!Mario
	LDA #$01
	STA !marioState
	RTL
	
MarioSetXPos:
	LDX !actorSlot+!Mario
	STA !marioXPos
	RTL
	
MarioSetYPos:
	LDX !actorSlot+!Mario
	STA !marioYPos
	RTL
	
MarioSetToVarWalkRight:
	LDX !actorSlot+!Mario
	LDA #$03
	STA !marioState
	RTL
	
MarioSetPoseToHurt:
	LDA #$07
	STA !marioPose
	RTL
	
MarioSetToFrozen:
	LDX !actorSlot+!Mario
	LDA #$05
	STA !marioState
	RTL
	
MarioResumeDoubleJumping:
	LDX !actorSlot+!Mario
	LDA #$02
	STA !marioState
	RTL
	
MarioAddQuestionMark:
	LDA #$AA
	STA !marioAboveHead
	RTL
	
MarioAddEllipsis:
	LDA #$BA
	STA !marioAboveHead
	LDX !actorSlot+!Mario
	LDA #$00
	STA !marioTimer
	RTL
	
MarioAddExclamationMark:
	LDA #$AB
	STA !marioAboveHead
	RTL
	
MarioRemoveAboveHeadSymbol:
	STZ !marioAboveHead
	RTL
	
MarioSetXSpeed:
	LDX !actorSlot+!Mario
	STA $B6,x
	RTL
	
MarioSetYSpeed:
	LDX !actorSlot+!Mario
	STA $AA,x
	RTL
	
MarioSetToGravityJumping:
	LDX !actorSlot+!Mario
	
	LDA !marioSetToFallThroughGround
	BEQ +
	LDA #$FF
	BRA ++
+
	LDA !marioYPos
++	STA !marioJumpBase
	LDA #$06
	STA !marioState
	RTL
	
MarioSetPoseToSad:
	LDA #$0B
	STA !marioPose
	RTL
	
MarioSetToRunning:
	LDA #$01
	STA !marioRunning
	LDA #$04
	STA !marioMovementSpeed
	RTL
	
MarioSetMovementSpeed:
	STA !marioMovementSpeed
	RTL
	
MarioSetWaitStateToVarMoveRight:
	LDA #$03
	STA !marioNextState
	RTL
	
MarioSendFlyingOffScreen:
	LDX !actorSlot+!Mario
	LDA #$07
	STA !marioState
	RTL
	
MarioSetToFallThroughGround:
	LDA #$01
	STA !marioSetToFallThroughGround
	RTL

MarioSetToRobotReveal:
	LDX !actorSlot+!Mario
	LDA #$08
	STA !marioState
	RTL


MarioStateTable:
; Just stand there | Walk offscreen to the right | Do two quick jumps |  Move right to a spot |  Move left to a spot  | Frozen in place|Jump w/ gravity, fly left from Frank
dw MarioNothing,     MarioMoveRight,              MarioSmallDoubleJump, MarioMoveVariablyRight, MarioMoveVariablyLeft, MarioFreeze, MarioGravityJump, MarioFlyingOffScreenLeft, MarioRobotReveal

MarioMAIN:
	PHK
	PLB
	
	
	LDA !marioTimer
	INC
	STA !marioTimer
	
	PHX				; Jump to Mario's current routine.
	LDA !marioState			;
	ASL				;
	TAX				;
	LDA MarioStateTable,x		;
	STA $00				;
	LDA MarioStateTable+1,x		;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
MarioFlyingOffScreenLeft:
	STA $7FFFE0
	LDA !marioXPos
	BMI +
	SEC
	SBC #$04
	STA !marioXPos
	BRA .draw
+
	LDA #$00
	STA !marioState
	
.draw
	LDA #$07
	STA !marioPose
	BRA MarioStandardDraw
	
MarioMoveRight:
	LDA !cutsceneNumber
	BNE +
	PHX
	LDX !actorSlot+!Luigi
	LDA !luigiXPos
	STA $01
	LDX !actorSlot+!Toad
	LDA !toadXPos
	STA $02
	PLX
	
	LDA !marioXPos
	STA $00
	
	CMP $02
	BCS .flipToad
	CMP $01
	BCS .flipLuigi
	BRA +
.flipToad
	LDX !actorSlot+!Toad
	LDA #$40
	STA !toadSad
	BRA +
	
.flipLuigi
	JSL LuigiSetToFacingRight
+
	LDX !actorSlot+!Mario
	
	LDA $13
	AND #$03
	BNE ++
	LDA !marioPose
	INC
	CMP #$03
	BNE +
	LDA #$00
	STA !marioPose
+
	STA !marioPose
++

	LDA !marioXPos
	INC
	INC
	STA !marioXPos
	CMP #$F0
	BCC MarioStandardDraw
	LDA #$00
	STA !actorExists,x

	
MarioNothing:
	STZ !marioPose

MarioFreeze:
	LDA !marioTimer
	DEC
	STA !marioTimer
				; Don't do anything.
				
MarioStandardDraw:
	STZ $09
	STX $06
	LDX !marioPose
	LDA MarioBodyFrames,x
	PHA
	LDA MarioHeadFrames,x
	PHA
	LDX $06

	LDA !marioXPos		; \
	STA $00			; / X pos
	LDA !marioYPos		; \ Y pos
	STA $01			; /
	
	LDA !marioPose
	CMP #$02
	BEQ .moveDown1
	CMP #$12
	BEQ .moveDown1
	BRA .dontMoveDown
.moveDown1
	INC $01
	LDA $01
	STA $09
.dontMoveDown
	
	PLA
	CMP #$82
	BNE +
	INC $01
+
	STA $02
	
	LDA #$31		; \
	EOR !marioProps		; |
	STA $03			; / Property
	
	;INC $04
	
	LDA #$02
	JSL DrawTile
	

	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
	PLA			; \
	STA $02			; /

	LDA #$02		; Size
	JSL DrawTile		;
	
	LDA !marioAboveHead
	BEQ .nothingAboveHead
	CMP #$BA
	BEQ .tententen
	
	STA $02
	LDA !marioYPos
	SEC
	SBC #$08
	STA $01
	LDA !marioXPos
	CLC
	ADC #$05
	STA $00
	
	LDA #$31
	STA $03
	
	LDA #$00
	JSL DrawTile
.nothingAboveHead


	
	
	LDA !marioRunning
	BEQ .dontDrawExtraHandTile

	LDA !marioXPos
	;SEC
	;SBC #$04
	STA $00
	
	LDA !marioYPos
	CLC
	ADC #$10
	CLC
	ADC $09
	STA $01
	
	LDA #$CC
	STA $02
	
	LDA $00
	SEC
	SBC #$08
	STA $00
	
	LDA $03
	ORA #$40
	STA $03
	
	LDA #$00
	JSL DrawTile
	
.dontDrawExtraHandTile

	RTS
	
.tententen
	LDA !marioXPos
	DEC
	STA $00
	
	LDA !marioYPos
	SEC
	SBC #$0A
	STA $01
	
	LDA #$BA
	STA $02
	
	LDA #$31
	STA $03
	
	PHX
	
	LDA !marioTimer
	LSR
	LSR
	LSR
	LSR
	LSR
	AND #$03
	TAX

-
	CPX #$00
	BEQ +
	LDA #$00
	JSL DrawTile
	DEX
	LDA $00
	CLC
	ADC #$05
	STA $00
BRA -
+
	PLX
	
	RTS
	

MarioSetToSmallSingleJumping:
	LDA #$01
	BRA MarioSetToSmallJumping

MarioSetToSmallDoubleJumping:
	LDA #$02
	
	; Load A with the number of double jumps to preform.
MarioSetToSmallJumping:
	STA !marioJumpCount
	LDX !actorSlot+!Mario
	LDA #$00
	STA !marioTimer
	LDA #$02
	STA !marioState
	
	LDA !marioYPos
	STA !marioJumpBase
	RTL

MarioSmallDoubleJump:
	LDA #$04
	STA !marioPose
	
	LDA !marioTimer
	SEC
	SBC #10
	BPL +
	;INC $00
	EOR #$FF
	INC A
	+
	STA $4202
	STA $4203
	NOP #3
	REP #$20
	LDA $4216
	LSR
	LSR
	PHA
	;LDA $00
	;BEQ +
	;PLA
	;EOR #$FFFF
	;INC
	;BRA ++
;+
	PLA
;++
	CLC
	ADC.w #26
	SEP #$20
	STA !marioYPos
	
	;LDA !marioTimer
	;INC
	;STA !marioTimer
	
	CMP #$08
	BCC +
	LDA !marioYPos
	CMP !marioJumpBase
	BCC +
	LDA !marioJumpBase
	STA !marioYPos
	DEC !marioJumpCount
	LDA #$00
	STA !marioTimer
	LDA !marioJumpCount
	BNE +
	;LDA #$00
	;STA !marioState
	;JSL MarioSetToWalkingRight
	
	JSR MarioDoStateSwitchCheck
	
	STA !marioPose
	
+
	JSR MarioStandardDraw
	RTS
	
MarioNMI:
	RTL

	
MarioMoveVariablyRight:
	LDA !marioXPos
	STA $00
	

	LDX !actorSlot+!Mario
	
	LDA !marioRunning
	BEQ .handleWalkingAnimation
	
	LDA $13
	AND #$01
	BNE ++
	LDA !marioPose
	CMP #$10
	BCS +
	LDA #$0F
+
	INC
	CMP #$13
	BNE +
	LDA #$10
+
	STA !marioPose
	BRA .doneWithWalkingAnimation
	
.handleWalkingAnimation
	LDA $13
	AND #$03
	BNE ++
	LDA !marioPose
	INC
	CMP #$04
	BCC +
	LDA #$00
+
	STA !marioPose
++
.doneWithWalkingAnimation

	LDA !marioXPos
	CLC
	ADC !marioMovementSpeed
	STA !marioXPos
	CMP !marioTargetXPos
	BCC +
	JSR MarioDoStateSwitchCheck
+
	JSR MarioStandardDraw
	RTS

	
MarioMoveVariablyLeft:
	LDA !marioXPos
	STA $00
	

	LDX !actorSlot+!Mario
	
	LDA $13
	AND #$03
	BNE ++
	LDA !marioPose
	INC
	CMP #$03
	BNE +
	LDA #$00
	STA !marioPose
+
	STA !marioPose
++

	LDA !marioXPos
	DEC
	DEC
	STA !marioXPos
	CMP !marioTargetXPos
	BCS +
	JSR MarioDoStateSwitchCheck
+
	JSR MarioStandardDraw
	RTS
	
	
	
MarioGravityJump:
	LDA !marioYPos
	CMP #$80
	BCC .move
	CMP #$E0
	BCC .noMove
.move

	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position if it's falling.
	
	LDA !marioYPos
	CMP !marioJumpBase
	BCC +
	LDA !marioJumpBase
	STA !marioYPos
	JSR MarioDoStateSwitchCheck
+
	LDA !marioPose
	CMP #$07
	BEQ +
	LDA #$04
	STA !marioPose
	LDA $AA,x
	BMI +
	INC !marioPose
+
.noMove
	JSR MarioStandardDraw
	RTS
	
	
	
	
MarioRobotReveal:
	lda $13
	and #$02
	beq +
	lda !marioYPos
	inc
	sta !marioYPos
	+
	jsr MarioDoStateSwitchCheck
	jmp MarioNothing