!fishXPos = !actorVar1,x
!fishYPos = !actorVar2,x
!fishState = !actorVar3,x
!fishTimer = !actorVar4,x

FishINIT:
	LDA #$00
	STA !fishTimer
	STA !fishState
	LDA #$40
	STA !fishYPos
	LDA #$8C
	STA !fishXPos
	RTL
	

FishStateTable:
dw FishJumping, FishLookingUp, FishDressing, FishWalkingAway

FishMAIN:
	PHK
	PLB
	
	;STX $04
	PHX				; Jump to the Doc's current routine.
	LDA !fishState			;
	ASL
	TAX
	LDA FishStateTable,x	;
	STA $00				;
	LDA FishStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
FishJumping:
	;LDA $13
	;AND #$01
	;BNE +
	LDA !fishXPos
	DEC
	STA !fishXPos
;+

	;LDA !fishTimer
	;LSR
	;SEC
	;SBC #$04
	;STA $4202
	;STA $4203
	;NOP
	;NOP
	;NOP
	;NOP
	;LDA $4216
	;CLC
	;ADC #$30
	;STA !fishYPos
	LDA !fishTimer
	SEC
	SBC #$08
	STA $4202
	STA $4203
	NOP #4
	LDA $4216
	LSR
	LSR
	CLC
	ADC #50
	STA !fishYPos
	
	LDA !fishTimer
	INC
	STA !fishTimer

	LDA !fishYPos
	CMP #$50
	BCC +
	LDA #$50
	STA !fishYPos
	LDA !fishState
	INC
	STA !fishState
	LDA #$00
	STA !fishTimer
+
	
	LDA #$60
	JMP FishDraw

FishLookingUp:
	;LDA $13
	;AND #$10
	;LSR
	;LSR
	;LSR
	;CLC
	;ADC #$40
	;STA $02
	;LDA !fishXPos
	;SEC
	;SBC #$04
	;STA $00
	;LDA !fishYPos
	;SEC
	;SBC #$07
	;STA $01
	;LDA #$33
	;STA $03
	;LDA #$02
	;JSL DrawTile
	;INC $04
	


	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$6A		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$08		; |
	LSR			; | Otherwise, animate the mouth.
	LSR			; |
	CLC			; |
	ADC #$6A		; /
	
+
	JMP FishDraw
	
FishSuitcaseX:
db $05, $06, $07, $08, $09, $0A, $0B, $0B, $0C, $0C, $0C, $0D, $0D, $0D, $0D, $0D
db $0C, $0C, $0B, $0B, $0A, $09, $08, $07

FishHatY:
db $02, $01, $00, $FF, $FE, $FD, $FC, $FC, $FB, $FB, $FA, $FA, $F9, $F9, $F9, $FA
db $FA, $FA, $FB, $FB, $FC, $FC, $FB, $FB

FishSetToDressing:
	LDA #$01
	STA !forcePause

	LDX !actorSlot+!Fish
	LDA #$02
	STA !fishState
	RTL
	
FishDressing:
	LDA !fishTimer
	CMP #$10
	BCS .drawExtrasUnder
	JSR .drawFish
	JSR .drawDressingExtras
	BRA +
.drawExtrasUnder
	JSR .drawDressingExtras
	JSR .drawFish
+


	LDA !fishTimer
	INC
	STA !fishTimer
	CMP #23
	BNE +
	LDA !fishState
	INC
	STA !fishState
	LDA #$00
	STA !fishTimer
+

	RTS
	
	
.drawFish
	LDA !fishTimer
	CMP #$04
	BCC .d1
	CMP #$08
	BCC .d2
.d1
	LDA #$62
	BRA +
.d2
	LDA #$64
+
	JSR FishDraw
	;INC $04
	RTS
	
.drawDressingExtras

	PHX
	LDA !fishTimer
	TAX
	LDA FishSuitcaseX,x
	SEC
	SBC #$02
	PLX
	CLC
	ADC !fishXPos
	STA $00
	
	LDA #$07
	CLC
	ADC !fishYPos
	STA $01
	
	LDA #$56
	STA $02
	
	LDA #$33
	STA $03
	
	LDA #$00
	JSL DrawTile
	
	LDA !fishXPos
	CLC
	ADC #$04
	STA $00
	
	PHX
	LDA !fishTimer
	TAX
	LDA FishHatY,x
	SEC
	SBC #$08
	PLX
	CLC
	ADC !fishYPos
	STA $01
	
	LDA #$44
	STA $02
	
	;INC $04
	
	LDA #$02
	JSL DrawTile
	
	;INC $04
	
	RTS
	
FishSuitcaseTiles:
db $46, $56, $46, $56
FishSuitcaseProps:
db $33, $33, $73, $73
FishYOffsets:
db $00, $01, $00, $01
FishXOffsets:
db $00, $02, $00, $FE
FishWalkingFrames:
db $66, $68, $66, $68
	
FishWalkingAway:
	
	LDA #$01
	STA !forcePause
	
	LDA !fishXPos
	INC
	STA !fishXPos
	CMP #$90
	BCC +
	PHX
	JSL LuigiSetToFacingRight
	PLX
	+
	
	LDA !fishXPos
	CMP #$F0
	BCC +
	PHX
	LDA #$00
	STA !actorExists,x
	STA !forcePause
	JSL LuigiSetToFacingLeft
	PLX
	RTS
	+
	
	LDA $13
	AND #$18
	LSR
	LSR
	LSR
	
	PHX
	TAX
	TAY
	
	LDA FishXOffsets,x
	PLX
	CLC
	ADC !fishXPos
	CLC
	ADC #$05
	STA $00
	
	LDA !fishYPos
	CLC
	ADC #$07
	STA $01
	
	PHX
	TYX
	
	LDA FishSuitcaseTiles,x
	STA $02
	
	LDA FishSuitcaseProps,x
	STA $03
	
	PLX
	
	LDA #$00
	JSL DrawTile
;	INC $04
	
	
	
	LDA !fishXPos
	CLC
	ADC #$04
	STA $00
	
	
	PHX
	TYX
	
	LDA FishYOffsets,x
	PLX
	CLC
	ADC !fishYPos
	SEC
	SBC #$0D
	STA $01
	
	LDA #$44
	STA $02
	
	LDA #$33
	STA $03
	
	LDA #$02
	JSL DrawTile
	;INC $04
	
	
	
	PHX
	TYX
	LDA FishWalkingFrames,x
	PLX
	
	JSR FishDraw
	;INC $04
	
	RTS
	
	
FishDraw:
	PHA
	LDA !fishXPos
	STA $00
	LDA !fishYPos
	STA $01
	PLA
	STA $02
	LDA #$33
	STA $03
	LDA #$02
	JSL DrawTile
	;INC $04
	RTS
	
FishNMI:
	;LDA !hindenBirdNMIInit
	;BNE +
	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
	;STA $2107				; /
	;LDA #$01
	;STA !hindenBirdNMIInit
	+
RTL
