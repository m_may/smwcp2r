!hindenBirdFloatTarget = !actorVar1,x		; If non-zero, float down to this height.
!hindenBirdXPos = !actorVar2,x
!hindenBirdYPos = !actorVar3,x
!hindenBirdState = !actorVar4,x
!hindenBirdTimer = !actorVar5,x

HindenbirdSetToFloating:
	LDX !actorSlot+!Hindenbird
	LDA #$00
	STA !hindenBirdState
	RTL
	
HindenbirdSetToFlyingOff:
	LDX !actorSlot+!Hindenbird
	LDA #$01
	STA !hindenBirdState
	DEC
	STA !hindenBirdTimer
	RTL


HindenbirdINIT:
	;LDA #$2D
	;STA !hindenBirdYPos
	;LDA #$78
	;STA !hindenBirdXPos
	RTL
	
HindenbirdSetXPosition:
	LDX !actorSlot+!Hindenbird
	STA !hindenBirdXPos
	RTL
	
HindenbirdSetYPosition:
	LDX !actorSlot+!Hindenbird
	STA !hindenBirdYPos
	RTL
	
HindenbirdFloatDown:
	LDX !actorSlot+!Hindenbird
	STA !hindenBirdFloatTarget
	RTL

SineBob:                
db $00, $01, $01, $02, $03, $03, $04, $04, $05, $05, $06, $06, $06, $07, $07, $07 
db $07, $07, $07, $07, $06, $06, $06, $05, $05, $04, $04, $03, $03, $02, $01, $01 
db $00, $FF, $FF, $FE, $FD, $FD, $FC, $FC, $FB, $FB, $FA, $FA, $FA, $F9, $F9, $F9 
db $F9, $F9, $F9, $F9, $FA, $FA, $FA, $FB, $FB, $FC, $FC, $FD, $FD, $FE, $FF, $FF 

HindenbirdStateTable:
dw HindenbirdFloat, HindenbirdFlyOff

HindenbirdMAIN:
	PHK
	PLB

	PHX				; Jump to the Doc's current routine.
	LDA !hindenBirdState		;
	ASL
	TAX
	LDA HindenbirdStateTable,x	;
	STA $00				;
	LDA HindenbirdStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL

HindenbirdFloat:
	LDA !hindenBirdFloatTarget
	BEQ +
	LDA $13
	AND #$01
	BEQ +
	LDA !hindenBirdYPos
	BMI .moveDown
	CMP !hindenBirdFloatTarget
	BCC .moveDown
	BRA +
.moveDown
	LDA !hindenBirdYPos
	INC
	STA !hindenBirdYPos
+

	LDA !hindenBirdXPos	; \
	STA $00			; / X pos
	PHX
	LDA $13
	LSR
	AND #$3F
	TAX
	LDA SineBob,x
	PLX
	CLC
	ADC !hindenBirdYPos	; \ Y pos
	STA $01			; /
	
	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$80		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$04		; |
	LSR			; | Otherwise, animate the mouth.
	CLC			; |
	ADC #$80		; /
	
+
	STA $02
	
	LDA #$3E		; \
	STA $03			; / Property
	
	;STX $04
	LDA #$02
	JSL DrawTile
	

	
	LDA $01
	CLC
	ADC #$08
	STA $01
	
	LDA $02			; \ Tile
	CLC			; |
	ADC #$10		; |
	STA $02			; /
	
	;INC $04			; Index
	LDA #$02		; Size
	JSL DrawTile		;
	
	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
	LDA $13
	LSR
	SEC
	SBC #$08
	AND #$3F
	BMI +
	CMP #$B0
	BCS +
	LDA #$B2
	BRA ++
+
	LDA #$32
++
	STA $02
	
	;INC $04			; Index
	
	LDA #$02		; Size
	
	JSL DrawTile		;
	
	
	LDA $00
	CLC
	ADC #$08
	STA $00
	
	LDA $01
	CLC
	ADC #$03
	STA $01
	
	LDA $13
	AND #$02
	CLC
	ADC #$FA
	STA $02
	
	;INC $04
	LDA #$00
	JSL DrawTile
	
	INC $02
	;INC $04
	LDA $01
	CLC
	ADC #$08
	STA $01
	LDA #$00
	JSL DrawTile
	RTS
	
HindenbirdFlyOff:
	LDA !hindenBirdTimer
	SEC
	SBC #13
	BPL +
	EOR #$FF
	INC
+
	STA $4202
	STA $4203
	NOP #3
	REP #$20
	LDA $4216
	STA $4204
	SEP #$20
	LDA #13
	STA $4206
	NOP #8
	LDA $4214
	EOR #$FF
	INC
	CLC
	ADC #60
	STA !hindenBirdYPos
	
	LDA !hindenBirdTimer
	INC
	STA !hindenBirdTimer
	CMP #$34
	BCC +
	LDA !hindenBirdTimer
	DEC
	STA !hindenBirdTimer
+

	CMP #10
	BCC +
	JMP .drawUp
+
	
	
{	
	LDA !hindenBirdXPos
	STA $00
	
	LDA !hindenBirdYPos
	STA $01
	
	LDA #$84
	STA $02
	
	LDA #$3E
	STA $03
	
	;STX $04
	
	LDA #$02
	JSL DrawTile
}
{	
	LDA $01
	CLC
	ADC #$08
	STA $01
	
	LDA $02
	CLC
	ADC #$10
	STA $02
	
	;INC $04
	
	LDA #$02
	JSL DrawTile
}
{	
	LDA $13
	AND #$02
	BEQ +
	LDA #$06
+
	CLC
	ADC #$B4
	STA $02
	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
	;INC $04
	LDA #$02
	JSL DrawTile
}	
{
	LDA $00
	SEC
	SBC #$08
	STA $00
	
	LDA $01
	SEC
	SBC #$08
	STA $01

	LDA #$88
	STA $02
	
	;INC $04
	LDA #$00
	JSL DrawTile
}
{	
	LDA $00
	CLC
	ADC #$18
	STA $00
	
	LDA #$89
	STA $02
	
	;INC $04
	LDA #$00
	JSL DrawTile
}	
	RTS

.drawUp
{			; Head, top left
	LDA !hindenBirdXPos
	STA $00
	
	LDA !hindenBirdYPos
	STA $01
	
	LDA #$86
	STA $02
	
	LDA #$3E
	STA $03
	
	;STX $04
	
	LDA #$00
	JSL DrawTile
}
{			; Head, top right
	LDA $00
	CLC
	ADC #$08
	STA $00
	
	INC $02
	
	;INC $04
	
	LDA #$00
	JSL DrawTile
}
{			; Head, bottom right
	LDA $13
	AND #$02
	CLC
	ADC #$10
	CLC
	ADC $02
	STA $02
	
	LDA $01
	CLC
	ADC #$08
	STA $01
	
	;INC $04
	
	LDA #$00
	JSL DrawTile
}
{			; Head, bottom left
	DEC $02
	
	LDA $00
	SEC
	SBC #$08
	STA $00
	
	;INC $04
	
	LDA #$00
	JSL DrawTile
}

{
	LDA $02
	CLC
	ADC #$10
	STA $02
	
	LDA $01
	CLC
	ADC #$08
	STA $01
	
	;INC $04
	
	LDA #$02
	JSL DrawTile
}
{
	LDA $02
	CLC
	ADC #$20
	STA $02
	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
;	INC $04
	
	LDA #$02
	JSL DrawTile
}
	RTS
	
	
	
HindenbirdNMI:
	;LDA !hindenBirdNMIInit
	;BNE +
	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
	;STA $2107				; /
	;LDA #$01
	;STA !hindenBirdNMIInit
	+
RTL
