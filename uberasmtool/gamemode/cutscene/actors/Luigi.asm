!luigiXPos = !actorVar1,x
!luigiYPos = !actorVar2,x
!luigiState = !actorVar3,x
!luigiHolding = !actorVar4,x
!luigiProperties = !actorVar5,x

LuigiINIT:
	LDA #$40
	STA !luigiYPos
	RTL
	
LuigiSetToHolding:
	LDX !actorSlot+!Luigi
	LDA #$01
	STA !luigiHolding
	RTL
	
LuigiSetToNotHolding:
	LDX !actorSlot+!Luigi
	LDA #$00
	STA !luigiHolding
	RTL
	
LuigiSetX:
	LDX !actorSlot+!Luigi
	STA !luigiXPos
	RTL
	
LuigiSetY:
	LDX !actorSlot+!Luigi
	STA !luigiYPos
	RTL
	
;LuigiSetXTo90:
;	LDX !actorSlot+!Luigi
;	LDA #$90
;	STA !luigiXPos
;	RTL
	
;LuigiSetXToF0:
;	LDX !actorSlot+!Luigi
;	LDA #$F0
;	STA !luigiXPos
;	RTL
	
LuigiSetToStanding:
	LDX !actorSlot+!Luigi
	LDA #$01
	STA !luigiState
	LDA #$90
	STA !luigiXPos
	RTL
	
LuigiSetToWalkingLeft:
	LDX !actorSlot+!Luigi
	LDA #$00
	STA !luigiState
	LDA #$90
	STA !luigiXPos
	LDA #$F0
	STA !luigiXPos
	RTL
	
LuigiSetToWalkingRight:
	LDX !actorSlot+!Luigi
	LDA #$02
	STA !luigiState
	LDA #$00
	STA !luigiXPos
	RTL
	
LuigiSetToFacingRight:
	LDX !actorSlot+!Luigi
	LDA #$40
	STA !luigiProperties
	RTL
	
LuigiSetToFacingLeft:
	LDX !actorSlot+!Luigi
	LDA #$00
	STA !luigiProperties
	RTL
	

LuigiStateTable:
dw LuigiRunInFromRight, LuigiStandStill, LuigiRunInFromLeft

LuigiMAIN:
	PHK
	PLB
	
	PHX				; Jump to the Doc's current routine.
	LDA !luigiState			;
	ASL
	TAX
	LDA LuigiStateTable,x	;
	STA $00				;
	LDA LuigiStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
LuigiDrawTile:
	;PHA
	;LDA !cutsceneNumber
	;BEQ +
	;LDA $02
	;SEC
	;SBC #$80
	;STA $02
	;+
	;PLA
	JSL DrawTile
	RTS
	
LuigiRunInFromLeft:
	LDA !luigiXPos
	CMP #$63
	BCC .moveRight
	LDA #$01
	STA !luigiState
	BRA .draw
	
.moveRight
	LDA !luigiXPos
	INC
	INC
	STA !luigiXPos
	
.draw

	;LDA !luigiXPos
	LSR
	LSR
	AND #$03
	ASL
	PHA
	JMP LuigiStandardDraw
	
	

LuigiRunInFromRight:
	LDA !luigiXPos
	CMP #$90
	BCS .moveLeft
	LDA #$01
	STA !luigiState
	BRA .draw
	
.moveLeft
	LDA !luigiXPos
	DEC
	STA !luigiXPos
	
.draw

	;LDA !luigiXPos
	LSR
	LSR
	AND #$03
	ASL
	PHA
	
LuigiStandardDraw:		; Note: push the body tile onto the stack first
	
	LDA !luigiHolding
	BEQ .noDrawFish
	
	LDA !luigiXPos
	SEC
	SBC #$08
	STA $00
	
	LDA !luigiYPos
	CLC
	ADC #$08
	STA $01
	
	PLA
	CMP #$04
	BNE +
	INC $01
+
	PHA
	
	LDA #$6E
	STA $02
	
	LDA #$B3		; \
	EOR !luigiProperties	; |
	STA $03			; / Property
	
	;STX $04
	LDA #$02
	JSR LuigiDrawTile
	
	
.noDrawFish
	

	;STX $04
	;LDA !luigiHolding
	;BEQ +
	;INC $04
;+
	
	LDA !luigiXPos		; \
	STA $00			; / X pos
	LDA !luigiYPos		; \ Y pos
	STA $01			; /
	PLA
	CMP #$04
	BNE +
	INC $01
+
	PHA

	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$20		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$04		; |
	LSR			; | Otherwise, animate the mouth.
	CLC			; |
	ADC #$20		; /
	
+
	STA $02
	
	LDA #$32		; \
	LDY !cutsceneNumber
	BNE +
	INC
	+
	EOR !luigiProperties	; |
	STA $03			; / Property
	
	;INC $04
	
	LDA #$02
	JSR LuigiDrawTile
	

	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
	PLA			; \
	STA $02			; /
	LDA !luigiHolding
	BEQ +
	LDA $02
	CLC
	ADC #$08
	STA $02
+
	
	;INC $04			; Index
	LDA #$02		; Size
	JSR LuigiDrawTile		;

	RTS
	
LuigiStandStill:
	LDA #$00
	PHA
	JMP LuigiStandardDraw

	
LuigiNMI:
	;LDA !hindenBirdNMIInit
	;BNE +
	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
	;STA $2107				; /
	;LDA #$01
	;STA !hindenBirdNMIInit
	+
RTL
