; NOTE: $1600+ is used for various cutscene controls.
; $1700+ is used for the individual actors and the RAM they need.

; More defines there
incsrc "../asm/cutscene/shared.asm"

!currentFontPointer = $45   ; The pointer to the current font (updated automatically)
!currentWidthsPointer = $47 ; The pointer to the current widths (updated automatically)
!trackingWidth = $49        ; How wide the space between each character is.  Should not be larger than 8 - the widest character.


;!buffer = $000000
!messagePtr = $16A0
!timeToNextLetter = $16A3
!messagePos = $16A4
!waitingForButton = $16A6  
!noWaitOn = $16A7
!letterX = $16A8
!VRAMUploadAddr = $16AA ; Current VRAM address to upload current tile(s) to.
!doMAIN = $16AC
!textNumber = $16AE
!uploadTwo = $16AF      ; If this value is below 2, then this is the number of tiles to upload - 1.  Otherwise, it's 1 tile.

!currentFontPointer = $45   ; The pointer to the current font (updated automatically)
!currentWidthsPointer = $47 ; The pointer to the current widths (updated automatically)

!waitingForButton = $16A6

!VRAMX = $16B0              ; The x position of the next empty tile.
!VRAMY = $16B1              ; The y position of the next empty tile.
!currentLetter = $16B2      ; Only used for adding delays to certain characters ("." "," "!" etc.)
!doNotUploadTiles = $16B3   ; Set if the tiles should not be updated.
!checkForWordWrap = $16B4   ; Set after every space.
!scrollTextUp = $16B6       ; Timer for scrolling the text up.  While !0, the text will scroll.
!clearTextPosition = $16B8  ; Used to clear the text, in conjunction with !fadeTextOut.
!textSpeed = $16B9          ; The speed at which the text scrolls.
!textIsAppearing = $16BA    ; Set if the text is appearing, clear otherwise.
!NMIUploadFlag = $16BB      ; Set if, during NMI, we should upload what's in the buffer at $7EAD00
!NMIUploadDest = $16BC      ; The upload destination of buffer uploads.
!NMIUploadSize = $16BE      ; The upload size of buffer uploads.

!currentFont = $16C0

!tile1 = $7EC810            ; Address of the first tile
!tile2 = $7EC830            ; Address of the second tile.
!textTilemapExGFX = $0200   ; The ExGFX number of the layer 3 tilemap used for the text
!borderExGFX = $0201        ; The ExGFX number of the border graphics
!buttonSpriteExGFX = $0202  ; The ExGFX number of the button press and Mario graphics.
!textTilemapSize = $1000
!borderExGFXSize = $0060
!letterHeight = $10         ; How high each letter's graphic is.
!backgroundColor = $16C1    ; 3 bytes; sets the background color to R G B (5 bits for each)

!layer1MapAddr = $5000      ; Note: This is enough to store two tilemaps.  Since it's only possible to upload one tilemap at a time,
!layer2MapAddr = $6000      ; Alternate between which tilemap to use for each scene (upload one, upload the other, switch to them both).
!layer3MapAddr = $4000      ; Covers both the top (window) and bottom (text) portions (top is right and bottom is left).

!layer1ChrAddr = $8000
!layer2ChrAddr = $8000
!layer12ChrAddr = !layer2ChrAddr<<4|!layer2ChrAddr
!layer34ChrAddr = $0000     ;

!spaceWidth = $07       ; The width of a space.

!forcePause = $16C5     ; If set, no "press A to continue" prompts will appear until cleared.

!inManualWait = $16C6       ; Set if we used a "wait" command ($F0+)

!currentSpeaker = $16C7     ; Set to the index of the current speaker.  If negative, then anyone can speak.

!nopingTheFuckOut = $16C8

!enableColorMath = $16C9

!wordBegin = $16CD  ; Set to 1 when a word begins. Reset to 0 when ends.

!soundTimer = $16CE


    ; Codes:
    ; $F0 - $FF: Wait for that many frames ($F0 = 1 frame, $FF = 16 frames).
    ; $EF: wait for button press.
    ; $EE: Possible break (use after dashes and such; indicates that a word can break here without a space).
    ; $ED: Space.
    ; $EC: New line
    ; $EB: Clear all text.
    ; $EA $XX: Set the text speed to $XX.
    ; $E9 $XX $YY $ZZ: JSL to $ZZXXYY.
    ; $E8 $XX $YY $ZZ $WW: JSL to $ZZXXYY with $WW in A.
    ; $E7 $XX: Set font to XX
    ; $E0: Stop.

!CCButton = $EF
!CCZeroSpace = $EE
!CCNewLine = $EC
!CCClearText = $EB
!CCTextSpeed = $EA
!CCSetFont = $E7


macro VWFASM(label)
    db $E9
    dl <label>
endmacro

macro VWFASMArg(label, arg)
    db $E8
    dl <label>
    db <arg>
endmacro


SpecialEffectTable:
dl VWFEnd, $000000, $000000, $000000, $000000, $000000, $000000, VWFSetFont
dl VWFExecuteCodeArg, VWFExecuteCode, VWFSetTextSpeed, VWFClearText, VWFLineBreak, VWFSpace, VWFWordBreak, VWFCheckButton
dl VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait
dl VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait, VWFSetWait

;UNUSED
;SpecialEffectWidths:
;db $01, $01, $01, $01, $01, $01, $01, $01
;db $05, $04, $02, $01, $01, $01, $01, $01
;db $05, $04, $02, $01, $01, $01, $01, $01
;db $05, $04, $02, $01, $01, $01, $01, $01

incsrc "gamemode/cutscene/fonts.asm"
incsrc "gamemode/cutscene/actor.asm"


init:
 JSR Prepare
 JSR RealInit
 JSR RealMain
RTL

main:
 JSR Prepare
 JSR RealMain
RTL

nmi:
	LDA $0DAE           ; \
	AND #$0F            ; | Turn force blank off.
	STA $2100           ; /

	LDA !inCutscene     ; \
	BNE +               ; / If we're not in a cutscene, get out of here.
	RTL
	+

	LDA !NMIUploadFlag
	BEQ +
	STZ !NMIUploadFlag
	REP #$10
	LDX !NMIUploadDest
	LDY !NMIUploadSize
	JSL UploadFromBuffer
	SEP #$30
	+

	LDA !backgroundColor
	ORA #$20
	STA $2132

	LDA !backgroundColor+1
	ORA #$40
	STA $2132

	LDA !backgroundColor+2
	ORA #$80
	STA $2132
    
	LDA #$01
	JSL RunActorCode

	LDA #$0A            ; \
	STA $4209           ; | Set an IRQ at scanline #$0A (which will modify the windowing).
	STZ $420A           ; /

	LDA !doNotUploadTiles       ; Only upload if the tiles have been updated.
	BNE .noUpload
	;LDA !timeToNextLetter
	;BNE .noUpload

	LDX !currentLetter      ; \
	CPX #$48            ; |
	BEQ .addWait            ; |
	CPX #$49            ; |
	BEQ .addWait            ; |
	CPX #$3E            ; | Add time to certain characters.
	BEQ .addWait            ; |
	CPX #$3F            ; |
	BNE .noWait         ; |
 .addWait                ; |
	LDA #$08            ;   \
	STA $4202           ;   |
	LDA !textSpeed          ;   |
	INC             ;   |
	STA $4203           ;   |
	INC !doNotUploadTiles       ;   | Multiply by the text speed.
	NOP             ;   |
	LDA $4216           ;   |
	STA !timeToNextLetter       ;   /
 .noWait                 ; /

	JSR CalculateVRAMAddress    ; Calculate the address to upload the tile(s) to.
	LDA !VRAMUploadAddr     ; \ Store to the VRAM address.
	STA $2116           ; /         
	LDX #$01            ; \ 2 registers write once.
	STX $4300           ; /
	LDX #$18            ; \ Write to $2118
	STX $4301           ; / 
	LDA.w #!tile1           ; \
	STA $4302           ; |
	LDX.b #!tile1>>16       ; | Source is the address of the first tile.
	STX $4304           ; /     
	LDA #$0020          ; \
	LDX !uploadTwo          ; |
	BEQ +               ; | Upload either 32 or 64 bytes, depending on need.
	ASL             ; | Also increase the VRAM position if we're uploading two.
	INC !VRAMX          ; |
	INC !VRAMX          ; |
	+               ; |
	STA $4305           ; /
	SEP #$20            ; \     
	LDA #$01            ; | Begin DMA.
	STA $420B           ; /
 .noUpload

	LDA !fadeTextOut
	BEQ .skipOverFadeStuff
	CMP #$01
	BEQ .doNotDecreaseFade
	DEC !fadeTextOut
	BRA .skipOverFadeStuff
	.doNotDecreaseFade

	LDA.w !clearTextPosition    ; \
	REP #$20            ; |
	AND #$00FF          ; | Get the address to clear.
	ASL #9              ; |
	STA $2116           ; / 
	LDX #$09            ; \ 
	STX $4300           ; / 
	LDX #$18            ; \ Write to $2118
	STX $4301           ; / 
	LDA.w #blankTileGFX     ; \
	STA $4302           ; |
	LDX.b #blankTileGFX>>16     ; | Source is the address of the first tile.
	STX $4304           ; /     
	LDA #$0400          ; \ 
	STA $4305           ; /         
	LDX #$01            ; \ Begin DMA.
	STX $420B           ; /

	SEP #$10            ; \
	LDA.w #!tile2           ; |
	STA $00             ; | Clear out tile2.
	LDA.w #!tile2>>8        ; |
	STA $01             ; |
	JSR ClearTile           ; /

	REP #$20            ; \
	LDA.w #!tile1           ; |
	STA $00             ; |
	LDA.w #!tile1>>8        ; | Clear out tile1.
	STA $01             ; |
	JSR ClearTile           ; /

	SEP #$20            ;
	DEC !clearTextPosition      ;
	;LDA !clearTextPosition     ;
	;CMP #!layer3MapAddr>>9-1   ;
	BPL +               ;
	STZ !fadeTextOut        ;
	STZ !VRAMX          ;
	STZ !VRAMY          ;   
	LDA #$20            ;
	STA !letterX            ;
	STZ !textYPos           ;


+

	.skipOverFadeStuff

	; Now we "switch" the screen.
	LDA $1A             ; \
	STA $210D           ; | Layer 1 X
	LDA $1B             ; |
	STA $210D           ; /
	LDA $1C             ; \
	CLC             ; |
	ADC $1888           ; |
	STA $210E           ; | Layer 1 Y
	LDA $1D             ; |
	STA $210E           ; /
	LDA $1E             ; \
	STA $210F           ; | Layer 2 X
	LDA $1F             ; |
	STA $210F           ; /
	LDA $20             ; \
	STA $2110           ; | Layer 2 Y 
	LDA $21             ; |
	STA $2110           ; /
	STZ $2111           ; \
	LDA #$01            ; | Layer 3 X
	STA $2111           ; /
	LDA #$FB            ; \
	STA $2112           ; | Layer 3 Y
	STZ $2112           ; /

	STZ $420C           ; No HDMA

	LDA #$33            ; \ Enable inversion window 1 for layers 1 and 2.
	STA $2123           ; /
	STZ $2124           ; Disable window 1 for layers 3 and 4.
	LDA #$33            ; \
	STA $2125           ; / Enable inversion window 1 for OBJ and Color Window

	LDA #$FF            ;
	STA $2126           ; Clip the entire screen
	STZ $2127           ; (will be turned off a few scanlines later)

	LDA #$17            ; \ Layer 3 on the main screen
	STA $212C           ; /
	lda #$13
	STA $212E           ; No windowing on the main screen.

	LDA #$13            ; \ Layers 1, 2, and OBJ on the sub screen.
	STA $212D           ; /
	STA $212F           ; Clip them, too.
    
	LDA #$22            ; \ Prevent color math inside color windows
	STA $2130           ; / and add the subscreen instead of fixed color.

	LDA #$20            ; \ Enable color math on the backdrop.
	STA $2131           ; / 

	lda !enableColorMath
	beq +

	lda $40
	sta $2131

	lda $44
	sta $2130

	+

	STZ $11             ; We have two IRQs, so $11 is used to distinguish whose code to run.

	RTL


; Sets the current VRAM address to upload.
; A should be 8-bit. $00-$01 is destroyed.
CalculateVRAMAddress:
{
	LDA !VRAMY
	CMP #$30
	BCC +
	STZ !VRAMY
	+
	SEP #$20
	LDA !VRAMX
	REP #$20
	AND #$00FF
	ASL #3
	STA $00
	LDA !VRAMY
	AND #$00FF
	ASL #7
	CLC
	ADC $00
	;ORA #!layer34ChrAddr>>1
	AND #$3FF0

	STA !VRAMUploadAddr
	RTS
}

Prepare:
	JSL $7F8000  ; Clear out all sprite tiles.

	LDX #$20
	-
	STZ $03FF,x
	DEX
	BNE -

	LDA !currentFont
	AND #$FF
	ASL
	TAX
	REP #$20
	LDA fonttable,x
	STA !currentFontPointer
	LDA widthstable,x
	STA !currentWidthsPointer
	SEP #$20


	LDA !waitingForButton  ; \ If we're waiting on a button, we should basically be doing nothing.
	BNE +                  ; /
	LDA !scrollTextUp
	BEQ +
	INC !textYPos
	DEC !scrollTextUp
	+

	LDA #$01               ; \ Never upload tiles unless specifically told to.
	STA !doNotUploadTiles  ; /
	RTS

RealInit:
{
	STZ $4200           ; Disable NMI for now.

	LDA #$80            ; \ Force blank on.
	STA $2100           ; /

	STZ !nopingTheFuckOut

	; Set fade status to fade in from 0 to F
	LDA #%10000000
	STA !cutsceneFadeStatus
	LDA #$00
	STA $0DAE

	LDA #$01
	STA !trackingWidth

	LDX #$00        ; \
	TXA             ; |
-	STA !actorRAM,x ; | Clear out all actor RAM
	DEX             ; |
	BNE -           ; /

	LDA.b #strings1     ; \
	STA $03             ; |
	LDA.b #strings1>>8  ; |
	STA $04             ; |
	LDA.b #strings1>>16 ; |
	STA $05             ; /

	LDA !textNumber     ; \
	ASL                 ; |
	CLC                 ; |
	ADC !textNumber     ; |
	TAY                 ; |
	LDA [$03],y         ; |
	STA.w !messagePtr   ; | Set the message pointer to the correct place.
	INY                 ; |
	LDA [$03],y         ; |
	STA.w !messagePtr+1 ; |
	INY                 ; |
	LDA [$03],y         ; |
	STA.w !messagePtr+2 ; /

	LDA #$01            ; \
	STA !doMAIN         ; |
	STA !inCutscene     ; /



	;LDA #$13           ; \
	;STZ $212E          ; | Set window mask layer settings.
	;STZ $212F          ; /

	LDA #$09            ; \ Mode 1, 8x8 tiles, layer 3 has priority.
	STA $2105           ; /

	LDA.b #!layer1MapAddr>>11<<2 ; \
	STA $2107                    ; /
	LDA.b #!layer2MapAddr>>11<<2 ; \
	STA $2108                    ; /
	LDA.b #!layer3MapAddr>>11<<2+1 ; \ Layer 3 gets an "extra" tilemap to the right, which holds the top screen's border.
	STA $2109                      ; /

	LDA.b #!layer12ChrAddr>>13 ; \ GFX index for BG1 and 2.
	STA $210B                  ; / 
	LDA.b #!layer34ChrAddr>>13 ; \ GFX index for BG3 and 4.
	STA $210C                  ; / 

	STZ $1A
	STZ $1B
	LDA #$C1
	STA $1C
	STZ $1D
	STZ $1E
	STZ $1F
	STZ $20
	STZ $21             ; Reset BG scroll values

	REP #$20            ; 209947
	LDA.w #!layer12ChrAddr>>1 ; \ Dest. is the address of the graphics data.
	STA $2116                 ; /
	LDX #$09            ; \
	STX $4300           ; /
	LDX #$18            ; \ Write to $2118
	STX $4301           ; /
	LDA.w #blankTileGFX     ; \
	STA $4302               ; |
	LDX.b #blankTileGFX>>16 ; | Source is the address of the first tile.
	STX $4304               ; /
	LDA #$0000          ; \ Completely clear out VRAM.
	STA $4305           ; /
	LDX #$01            ; \ Begin DMA.
	STX $420B           ; /

	REP #$30                       ; \
	LDA #!textTilemapExGFX         ; |
	LDX #!layer3MapAddr>>1         ; | Upload the layer 3 tilemap
	LDY #!textTilemapSize          ; |
	JSL DecompressAndUploadGFXFile ; /
	SEP #$10


	REP #$10        ; \
	LDX.w #$4000    ; |
	LDA #$21FF      ; |
	-	            ; |
	STA $7EC800,x   ; | Create an empty tilemap to DMA in a moment.
	DEX             ; |
	DEX             ; |
	BPL -           ; /
	SEP #$10        ; 209945

	LDA.w #!layer1MapAddr>>1 ; \ Dest. is the address of the tilemap data.
	STA $2116                ; / 
	LDX #$01            ; \ 
	STX $4300           ; / 
	LDX #$18            ; \ Write to $2118
	STX $4301           ; / 
	LDA.w #$7EC800      ; \
	STA $4302           ; |
	LDX.b #$7EC800>>16  ; | Source is the address of the first tile.
	STX $4304           ; /     
	LDA.w #$4000        ; \ 
	STA $4305           ; /     
	LDX #$01            ; \ Begin DMA.
	STX $420B           ; /

	LDA.w #!layer2MapAddr>>1 ; \ Dest. is the address of the tilemap data.
	STA $2116                ; / 
	LDX #$01            ; \ 
	STX $4300           ; / 
	LDX #$18            ; \ Write to $2118
	STX $4301           ; / 
	LDA.w #$7EC800      ; \
	STA $4302           ; |
	LDX.b #$7EC800>>16  ; | Source is the address of the first tile.
	STX $4304           ; /     
	LDA.w #$4000        ; \ 
	STA $4305           ; /     
	LDX #$01            ; \ Begin DMA.
	STX $420B           ; /



	REP #$30                       ; \
	LDA #!borderExGFX              ; |
	LDX #!layer34ChrAddr>>1+$1F80  ; | Upload the layer 3 border graphics.
	LDY #!borderExGFXSize          ; |
	JSL DecompressAndUploadGFXFile ; /

	LDA #!buttonSpriteExGFX        ; \
	LDX #$7800                     ; |
	LDY #$1000                     ; | Upload the button press graphics.
	JSL DecompressAndUploadGFXFile ; /

	SEP #$30

	LDA #$20
	STA !letterX

	LDX #$00
	LDA #$00
	-               ; Clear out the actor RAM
	STA !actorRAM,x
	DEX
	BNE -

	JSL CSINIT1

	SEP #$30

	REP #$20            ; \
	LDA.w #!tile2       ; |
	STA $00             ; | Clear out tile2.
	LDA.w #!tile2>>8    ; |
	STA $01             ; |
	JSR ClearTile       ; /

	REP #$20            ; \
	LDA.w #!tile1       ; |
	STA $00             ; |
	LDA.w #!tile1>>8    ; | Clear out tile1.
	STA $01             ; |
	JSR ClearTile       ; /


	LDA #$A1            ; \ Turn NMI and IRQ on.
	STA $4200           ; /
	RTS
}

RealMain:
{
	LDA #$A1            ; \ Turn NMI and IRQ on (as IRQ is turned off when changing music track)
	STA $4200           ; /

	; Handle fading in and out.

	LDA !cutsceneFadeStatus
	BPL .doneFading

	AND #$01
	BEQ .fadingIn
	LDA !toLoad
	CMP #2
	BEQ .fadingOutNoMosaic
	BRA .fadingOutMosaic

	.fadingIn
	LDA $0DB0      ; \
	SEC : SBC #$10 ; |
	STA $0DB0      ; | mosaic
	ORA #$03       ; |
	STA $2106      ; /

	LDA $0DAE
	INC
	STA $0DAE
	CMP #$0F
	BNE .doneFading
	STZ !cutsceneFadeStatus
	BRA .doneFading

	.fadingOutMosaic
	LDA $0DB0      ; \
	CLC : ADC #$10 ; |
	STA $0DB0      ; | mosaic
	ORA #$03       ; |
	STA $2106      ; /

	.fadingOutNoMosaic
	LDA $0DAE
	DEC
	STA $0DAE
	;CMP #$00
	BNE .doneFading
	STZ !cutsceneFadeStatus	

	.doneFading
	LDA !soundTimer
	BEQ +
	DEC !soundTimer
	+

	LDA !nopingTheFuckOut
	BNE ++
	LDA $15
	AND #$10
	BEQ +
	STA !nopingTheFuckOut
	++
	; STZ !waitingForButton
	; STA !forcePause
	; JSR VWFClearText
	JMP VWFEnd
	+

	LDA #$00
	JSL RunActorCode

	LDA !waitingForButton
	BNE VWFCheckButton
	LDA !timeToNextLetter
	BNE DecWaitTimer
	STZ !inManualWait
	LDA !fadeTextOut
	BEQ FetchCode    ; Don't run any code if we're fading out.
	CMP #$01
	BNE +
	STZ !scrollTextUp
	STZ !textYPos
	+
	JMP VWFFinish
	FetchCode:
	REP #$20
	LDA !messagePtr
	STA $00
	SEP #$20
	LDA.w !messagePtr+2
	STA $02
	LDA [$00]
	CMP #$E0
	BCC Letter
	SEC
	SBC #$E0
	STA $04
	PHA
	ASL
	CLC
	ADC $04
	TAX
	REP #$20
	LDA SpecialEffectTable,x
	STA $04
	SEP #$20
	LDA SpecialEffectTable+2,x
	STA $06
	PLA
	JML [$0004]

Letter:
	JSR MakeSound
NotControlCode:
	JSR WordWrap
	LDY !waitingForButton
	BNE +
	JSR DecodeLetter

	LDA !noWaitOn
	BNE FetchCode
	+
	JMP VWFFinish

SetButtonWait:
	LDA #$01
	STA !waitingForButton
	RTS

VWFSpace:
	STZ !wordBegin
	LDA #$51
	BRA NotControlCode

DecWaitTimer:
	DEC !timeToNextLetter
	JMP VWFFinish

VWFCheckButton:
	STZ !wordBegin
	LDA !forcePause
	BNE .textIsPaused
	LDA #$02
	STA !textIsAppearing
	LDA #$D8
	STA $03FC
	LDA #$CA
	STA $03FD
	LDA $13
	AND #$10
	LSR #3
	CLC
	ADC #$EC
	STA $03FE
	LDA #$39
	STA $03FF
	LDA #$80
	STA $041F
	LDA $15
	; CMP #$80
	; BNE +
	BPL +
	LDA !waitingForButton       ; This never set for user-induced button waits
	BNE ++
	JSR IncreaseMessagePosition
	++
	STZ !waitingForButton
	+
	JMP VWFFinish

	.textIsPaused
	STZ !textIsAppearing
	RTS

VWFSetWait:
	STZ !wordBegin
	AND #$0F            ; Get the nibble of the byte
	ASL                 ; And multiply it by 2.
	STA $4202           ; \
	LDA !textSpeed      ; |
	INC                 ; | Multiply by the text speed
	STA $4203           ; |
	LDA #$01            ; |
	STA !inManualWait   ; |
	NOP                 ; |
	;NOP #4             ; |
	LDA $4216           ; /
	STA !timeToNextLetter
	JSR IncreaseMessagePosition
	JMP VWFFinish


VWFWordBreak:
	STZ !wordBegin
	JSR IncreaseMessagePosition
	JMP VWFFinish

VWFLineBreak:
	STZ !wordBegin
	JSR InsertNewLine
	JSR IncreaseMessagePosition
	JMP VWFFinish

VWFClearText:
	STZ !wordBegin
	LDA.b #$0E
	STA !clearTextPosition
	LDA #$10
	STA !fadeTextOut
	JSR IncreaseMessagePosition
	JSR InsertNewLine
	JMP VWFFinish
	VWFSetTextSpeed:
	REP #$20
	INC $00
	SEP #$20
	LDA [$00]
	STA !textSpeed
	JSR IncreaseMessagePosition
	JSR IncreaseMessagePosition
	JMP VWFFinish

VWFSetFont:
	REP #$20
	INC $00
	SEP #$20
	LDA [$00]
	STA !currentFont
	JSR IncreaseMessagePosition
	JSR IncreaseMessagePosition
	JMP VWFFinish


	VWFExecuteCodeArg:
	LDA #$01
	STA $0D
	BRA VWFExecuteCodeSkip

	VWFExecuteCode:
	STZ $0D
	VWFExecuteCodeSkip:
	PHP
	REP #$20
	INC $00
	LDA [$00]
	STA $04
	INC $00
	INC $00
	SEP #$20
	LDA [$00]
	STA $06
	PHB
	LDA $06
	PHA
	PLB
	PHK
	PEA.w .Ret1-1
	LDA $0D
	BEQ +
	REP #$20        ; \
	INC $00         ; |
	INC !messagePtr     ; | Get the argument
	SEP #$20        ; |
	LDA [$00]       ; /
	+
	JML [$0004]
	.Ret1
	PLB

	PLP
	REP #$20
	INC !messagePtr
	INC !messagePtr
	INC !messagePtr
	INC !messagePtr
	SEP #$20
	JMP FetchCode

	VWFEnd:

	LDA $0DAE
	BEQ .fadeOutComplete

	; Handle the actual fading out.
	; Note that since we never increase the message pointer we run this code "forever".
	LDA #%10000001
	STA !cutsceneFadeStatus
	JMP VWFFinish


.fadeOutComplete
	STZ $0109           ; Level to go to after fade to overworld (none)

	STZ !inCutscene

	LDA !toLoad
	CMP #2
	BEQ .toOverworld

	;INC $141A

	;JSL $84DC09
	LDA #$11
	STA $0100
	JMP VWFFinish

.toOverworld
	STZ !toLoad ; It wouldn't be cleared since we skip loading the level

	STZ $0DAE
	STZ $0DAF

	LDA #$00    ; \ No clue what this does...!
	STA $7fc009 ; / But it's necessary to get the graphics to load correctly.

	LDA #$0C
	STA $0100

VWFFinish:
	LDA !textIsAppearing
	CMP #$02
	BNE +
	STZ !textIsAppearing 
	BRA +++
+
	LDA !fadeTextOut
	BEQ + 
.textIsNotAppearing 
	STZ !textIsAppearing 
	BRA ++
+	                     ; Set the !textIsAppearing address.
	LDA !timeToNextLetter
	CMP !textSpeed
	BEQ .textIsAppearing
	BCS .textIsNotAppearing
.textIsAppearing
	LDA #$01
	STA !textIsAppearing
++

+++
	LDA !inManualWait
	BEQ +
	STZ !textIsAppearing
+
	RTS
}

InsertNewLine:
{
	LDA #$20 
	STA !letterX
	STZ !VRAMX 
	INC !VRAMY
	INC !VRAMY
	INC !VRAMY
	LDA !VRAMY
	CMP #$10
	BCC +
	LDA !textYPos
	BNE ++
	INC
	JSR SetButtonWait
	++
	LDA #$10
	CLC
	ADC !scrollTextUp
	STA !scrollTextUp
	+
	REP #$20            ; \
	LDA.w #!tile2       ; |
	STA $00             ; | Clear out tile2.
	LDA.w #!tile2>>8    ; |
	STA $01             ; |
	JSR ClearTile       ; /

	REP #$20            ; \
	LDA.w #!tile1       ; |
	STA $00             ; |
	LDA.w #!tile1>>8    ; | Clear out tile1.
	STA $01             ; |
	JSR ClearTile       ; /
	RTS
}

IncreaseMessagePosition:
	REP #$20
	INC !messagePtr
	SEP #$20
	RTS

; Checks for word wrap, and inserts new lines if necessary.
WordWrap:
{
	PHA          ; Save the current letter
	LDA !letterX ; \ Save the current x position.
	PHA          ; /
	LDY #$00

.loop
	LDA [$00],y     ; Get the next letter in the word

	CMP #$E7
	BEQ .skipCode2
	CMP #$E8
	BEQ .skipCode5
	CMP #$E9        ; \ If it's a JSL code, skip 4 bytes and continue
	BEQ .skipCode4  ; /
	CMP #$EA
	BEQ .skipCode2
	CMP #$EB
	BEQ .exitLoop
	CMP #$EC        ; \ If it's a new line... ... ...exit.
	BEQ .exitLoop   ; /
	CMP #$ED        ; \ If it's a clear, exit
	BEQ .exitLoop   ; /
	CMP #$EE        ; \ Not quite a space but exit anyway.
	BEQ .exitLoop   ; /
	CMP #$EF        ; \ If it's a button request, exit
	BEQ .exitLoop   ; /
	CMP #$E0        ; \ If it's an end code, exit.
	BEQ .exitLoop   ; /
	BCS .notChar    ; If it's any control code, skip it and go to the next character.
	TAX

	PHY
	TXY
	LDA !letterX                  ; \
	CLC                           ; | Increase the current x position
	ADC (!currentWidthsPointer),y ; | by this letter
	PLY                           ; |
	;INC                          ; | ;plus 1 to account for tracking.
	CLC                           ; |
	ADC !trackingWidth            ; |
	STA !letterX                  ; /
	CMP #$E0            ; \ If we've passed #$E0, then exit.
	BCS .tooFar         ; /
.notChar
	INY
	BRA .loop

.tooFar
	JSR InsertNewLine

	PLA
	PLA
	RTS
.exitLoop
	PLA
	STA !letterX
	PLA
	RTS
.skipCode5
	INY
.skipCode4
	INY
.skipCode3
	INY
.skipCode2
	INY
.skipCode1
	INY
	BRA .loop
}

MakeSound:
{
	PHA

	LDX !currentSpeaker
	BMI .return

	LDA !actorType,x
	TAX
	LDA #$1D
	STA $09
	LDA ActorVoicePropertiesTable,x
	AND #$01
	BNE +
	LDA #$F9
	BRA .channelChosen
	+
	LDA #$FC
	.channelChosen
	STA $08

	LDA ActorVoicePropertiesTable,x
	AND #$02
	BEQ .letterSound

	.wordSound
	LDA !wordBegin
	BNE .return
	LDA ActorVoiceTable,x
	STA [$08]
	LDA #1
	STA !wordBegin
	BRA .return

	.letterSound
	LDA !soundTimer
	BNE .return
	LDA ActorVoiceTable,x
	STA [$08]
	LDA #$02
	STA !soundTimer

	.return
	PLA
	RTS
}

DecodeLetter:
{
	PHA                    ; Save the current letter.
	STZ !doNotUploadTiles  ; We're decoding a letter, so upload stuff.

	LDA !textSpeed
	;INC
	STA !timeToNextLetter
	LDA !uploadTwo
	BEQ +            ; If uploadTwo is set, then we need to copy the contents of tile2 to tile1 and clear tile2.
	REP #$20         ; \
	LDA.w #!tile2    ; |
	STA $00          ; | 
	LDA.w #!tile2>>8 ; |
	STA $01          ; | Setup
	LDA.w #!tile1    ; |
	STA $03          ; |
	LDA.w #!tile1>>8 ; |
	STA $04          ; /
	JSR CopyTile     ; Copy tile2 to tile1.
	REP #$20    
	JSR ClearTile    ; Clear tile1 $00 still contains the address of tile2.
	STZ !uploadTwo   ; Turn this off.
	+

	PLA
	STA $07     ; Save the letter to $07.
	CMP #$ED
	BNE +
	LDA #$51    ; If this is a space, pretend it's character #$51
	STA $07
	+
	INC         ; We decode backwards, so start at the next letter.
	STA $4202
	LDA #!letterHeight
	STA $4203
	NOP #3
	REP #$30
	LDA $4216
	DEC         ; Last byte of the letter.
	TAY
	ASL
	TAX         ; Y contains 1bpp index, X contains 2bpp index.

	LDA !currentFontPointer
	STA $00
	SEP #$20
	PHB
	PLA
	STA $02
	;LDA.w #font
	;STA $00
	;LDA.w #font>>8
	;STA $01
	            ; $00 contains the address of the current letter's 1bpp graphics.

	;SEP #$20
	LDX.w #!letterHeight<<1-2 ; The number of bytes to decode
.loop
	XBA             ; \
	LDA #$00        ; | Clear out the high byte (I freaking love XBA).
	XBA             ; /
	LDA [$00],y     ; Get the byte.
	STA $04         ; Store to temp.

	PHX             ; But push X for now; we need it for a counter.

	LDA !letterX    ; Get the current x position.
	AND #$07        ; ...within the current tile.
	TAX             ; Put it into X; we need to shift the new tile this many times.
	LDA $04         ; Get the GFX byte back.
	REP #$20
	AND #$00FF
	XBA             ; Put the GFX byte into the high byte of A.

.shiftLoop
	DEX
	BMI .shiftLoopExit
	LSR               ; Shift it until it's out of the way of the first letter.
	BRA .shiftLoop
.shiftLoopExit
	PLX               ; Get x back (the 2bpp index).
	XBA
	PHA               ; Save both the left and right parts (but in reverse order).
	SEP #$20
	ORA !tile1,x      ; \ Store the GFX byte to the GFX buffer.
	STA !tile1,x      ; /
	PLA
	PLA               ; Get the right part.
	STA !tile2,x      ; And store it.

	LDA #$00 
	STA $7EC811,x     ; Store 0 to the next byte of the GFX buffer (for 2bpp graphics).
	STA $7EC831,x 

	DEY
	DEX
	DEX
	BPL .loop
	            ; We have the decoded letter in !tile1 / !tile2.
	SEP #$10
	LDX $07
	LDA !letterX
	AND #$07
	CLC
	PHY
	TXY
	ADC (!currentWidthsPointer),y
	PLY
	CMP #$07
	BCC .notNeedNewTile

	LDA #$01
	STA !uploadTwo

.notNeedNewTile
	LDA !letterX
	;INC
	CLC
	ADC !trackingWidth
	CLC             ; Increase the width.
	PHY
	TXY
	ADC (!currentWidthsPointer),y
	PLY
	STA !letterX
	JSR IncreaseMessagePosition

	CPX #$ED
	BNE +
	LDA #$01
	STA !checkForWordWrap
+
.space
	STX !currentLetter
	STZ !doNotUploadTiles
	RTS
}

!GFXBuffer = $7EAD00
; Ripped from genericuploader.asm.  
; 16-bit A should be the ExGFX number to decompress.
; 16-bit X should be the VRAM address to upload to (divided by 2).
; 16-bit Y should be the size of the upload.
DecompressAndUploadGFXFile:
	REP #%00010000      ; 16-bit X/Y
	PHY         ; back up Y
	PHX         ; back up X
	JSL DecompressGFXFile
	REP #%00010000      ; 16-bit X/Y
	PLX
	PLY
	JSL UploadFromBuffer
	RTL
; Decompresses the GFX file in 16-bit A to $7EAD00
{
print pc," : DecompressGFXFile"
DecompressGFXFile:
	CMP.w #$0032        ; \ < 32, use code
	BCC .GFX00to31      ; / for GFX 00-31
	CMP.w #$0080        ; \ still < 80,
	BCC .UploadReturn   ; / return
	CMP.w #$0100        ; \ > 100, use code
	BCS .ExGFX100toFFF  ; / for 100-FFF
 .GFX80toFF  AND.w #$007F        ; reset most significant bit
	STA.b $8A       ; \
	ASL A           ;  | multiply by 3 using
	CLC         ;  | shift-add method
	ADC.b $8A       ; /
	TAY         ; -> Y
	LDA.l $0FF94F       ; \
	STA.b $06       ;  | $0FF94F-$0FF950 contains the pointer
	LDA.l $0FF950       ;  | for the ExGFX table for 80-FF
	STA.b $07       ; /
	BRA .FinishDecomp   ; branch to next step

 .UploadReturn   ;PLX
	;PLY
	RTL

 .GFX00to31  TAX         ; ExGFX file -> X
	LDA.l $00B992,x     ; \
	STA $8A         ;  | copy ExGFX
	LDA.l $00B9C4,x     ;  | pointer to
	STA $8B         ;  | $8A-$8C
	LDA.l $00B9F6,x     ;  |
	STA $8C         ; /
	BRA .FinishDecomp2  ; branch to next step

 .ExGFX100toFFF  ;SEC            ; \ subtract #$100
	SBC.w #$0100        ; / SEC commented out because the BCS that branches here would only branch if the carry flag were already set
	STA.b $8A       ; \
	ASL A           ;  | multiply result
	CLC         ;  | by 3 to get
	ADC.b $8A       ;  | index
	TAY         ; /
	LDA.l $0FF873       ; \
	STA.b $06       ;  | $0FF873-$0FF875 contans the pointer
	LDA.l $0FF874       ;  | for the ExGFX table for 100-FFF
	STA.b $07       ; /
 .FinishDecomp   LDA.b [$06],y       ; \ Get low byte.
	STA.b $8A       ; / and high byte
	INC.b $06       ; Increase pointer position by 1.
	BNE .NoCrossBank    ; \
	SEP #%00100000      ;  | allow bank crossing
	INC $08         ;  | (not sure if this is necessary here)
	REP #%00100000      ; /
 .NoCrossBank    LDA.b [$06],y       ; \ Get the high byte (again)
	STA.b $8B       ; / and bank byte.
 .FinishDecomp2  LDA.w #!GFXBuffer   ; \ GFX buffer low
	STA.b $00       ; / and high byte
	SEP #%00100000      ; 8-bit A
	LDA.b #!GFXBuffer>>16   ; \ GFX buffer
	STA.b $02       ; / bank byte
	PHK         ; \
	PEA.w .Ret1-1       ;  | local JSL to LZ2 routine
	PEA $84CE       ;  | (afterwards A/X/Y all 8-bit)
	JML $00B8DC     ; /
 .Ret1       
	RTL

; Uploads the graphics at $7EAD00 with the 16-bit address in X and the 16-bit size in Y.    
UploadFromBuffer:
	REP #%00010000      ; 16-bit X/Y
	;PLX            ; \ restore X, X is the
	STX.w $2116     ; / VRAM destination
	LDA.b #%00000001    ; \ control
	STA.w $4300     ; / register
	LDA.b #$18      ; \ dma to
	STA.w $4301     ; / [21]18
	LDX.w #!GFXBuffer   ; \
	STX.w $4302     ;  | source
	LDA.b #!GFXBuffer>>16   ;  | of data
	STA.w $4304     ; /
	;PLY            ; Restore Y, Y is the
	STY.w $4305     ; / size of the upload
	LDA.b #%00000001    ; \ start DMA
	STA.w $420B     ; / transfer
	REP #%00100000      ; 16-bit A
	RTL
}

;UNUSED
;blankTileData:
;dw $21FF

blankTileGFX:
dw $0000

; $00 should contain the address of the tile to clear.
; A should be 16 bits wide (will be 8-bits on exit).
ClearTile:
{
	LDY #$1E
	LDA #$0000
	.loop
	STA [$00],y
	DEY
	DEY
	BPL .loop
	SEP #$20
	RTS
}

; $00 should contain the source address.
; $03 should contain the target address.
; A should be 16 bits wide (will be 8-bits on exit).    
CopyTile:
{   
	LDY #$1E
	.loop
	LDA [$00],y
	STA [$03],y
	DEY
	DEY
	BPL .loop
	SEP #$20
	RTS
}

reset bytes

strings1:
dl CS1T1, CS1T2, CS1T3, CS1T4, CS1T5, CS1T6, CS1T7, CS1T8, CS1T9, CS1TA, CS1TB, CS1TC, CS1TD, CS1TE, CS1TF, CS1T10

cleartable
table "gamemode/cutscene/charTable.txt"

incsrc "gamemode/cutscene/scriptLibrary.asm"
incsrc "gamemode/cutscene/Cutscene1.asm"	; Intro cutscene

print bytes," bytes of text/event data."