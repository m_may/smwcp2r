; "Base class" actor for SMWCP2 cutscenes.
; Actors have three routines associated with them:
; 1. A routine that runs on initialization.
; 2. A routine that runs every frame.
; 3. A routine that runs during NMI.
; It also has a byte detailing whether or not its slot is being used,
; as well as 6 misc bytes each actor can use for RAM (accessed by !actorRAM+10,x through !actorRAM+15,x or the !actorVar defines).

; To draw a tile, call DrawTile.  By default, each sprite has 8 tiles to it.
; More can be used, but they must be managed themselves.

!actorRAM = $7FA000		; Should be 0x100 bytes minimum.

!actorType = !actorRAM+$00
!actorTableIndex = !actorRAM+$10

!actorExists = !actorRAM+$90

!actorVar1 = !actorRAM+$A0
!actorVar2 = !actorRAM+$B0
!actorVar3 = !actorRAM+$C0
!actorVar4 = !actorRAM+$D0
!actorVar5 = !actorRAM+$E0
!actorVar6 = !actorRAM+$F0

!actorTableIndex = $1745
!actorSlot = $1780  ; 22 bytes

reset bytes
incsrc "actors/AllActors.asm"
print bytes," bytes of actor data."

ActorInitTable:
dl DocCrocINIT, MadamMau1INIT, MadamMau1INIT, HindenbirdINIT, LuigiINIT
dl FishINIT, ToadINIT, MarioINIT

ActorMainTable:
dl DocCrocMAIN, MadamMau1MAIN, MadamMau1MAIN, HindenbirdMAIN, LuigiMAIN
dl FishMAIN, ToadMAIN, MarioMAIN

ActorNmiTable:
dl DocCrocNMI, MadamMau1NMI, MadamMau1NMI, HindenbirdNMI, LuigiNMI
dl FishNMI, ToadNMI, MarioNMI

ActorVoiceTable:
db $0F, $2E, $2E, $28, $2E
db $0E, $2E, $2E

; bit 0:
;    0 - use $1DF9
;    1 - use $1DFC
; bit 1:
;    0 - sound on letter
;    1 - sound on word
ActorVoicePropertiesTable:
db $00, $00, $00, $03, $00
db $02, $00, $00

; This routine creates a new actor
; Arguments: A - actor type index
; Returns: A - the actor's ID in (0 - 0xF)
NewActor:
{
	PHB
	PHK
	PLB
	LDX #$0F

	STA $00

.loop	
	LDA !actorExists,x
	BEQ .endLoop
	DEX
	BMI .noneFound
	BRA .loop
.endLoop
	LDA $00
	STA !actorType,x
	ASL            ; \ multiply A by 3
	CLC : ADC $00  ; /
	STA !actorTableIndex,x

	LDA #$01
	STA !actorExists,x
	LDA #$00
	STA !actorVar1,x
	STA !actorVar2,x
	STA !actorVar3,x
	STA !actorVar4,x
	STA !actorVar5,x
	STA !actorVar6,x

	TXA
	LDX $00
	STA !actorSlot,x

	PLB
	RTL
.noneFound
	LDA #$00
	PLB
	RTL
}

DeleteActor:
	TAX
	LDA !actorSlot,x
	TAX
	LDA #$00
	STA !actorExists,x
	RTL

; $00 = x pos
; $01 = y pos
; $02 = tile
; $03 = yxppccct
; $0E = index (shouldn't need to worry about normally; just don't let any actor code touch $0E and you'll be okay)
; A = size (0 = 8x8, 2 = 16x16)
DrawTile:
{
	PHY
	PHX
	PHA
	LDA $0E
	ASL #2
	TAY
	LDA $00
	STA $0200,y
	LDA $01
	CLC
	ADC $1888
	CMP #$F0
	BMI .prematureEnd			; Don't draw things that are above the screen.
	STA $0201,y
	LDA $02
	STA $0202,y
	LDA $03
	STA $0203,y

	LDA $0E		; Table index into Y
	LSR #2		;
	TAY		;

	LDA $0E		; Shift count into X
	AND #$03	;
	;EOR #$03
	TAX		;
			
	PLA		;
	AND #$02	;
	BEQ ++
-
	DEX		;
	BMI +		; Shift until the bit is in the correct place.
	ASL		;
	ASL		;
	BRA -		;
+
	ORA $0400,y	; OR it into the table.
	STA $0400,y	;
++
	PLX
	PLY
	INC $0E
	RTL		;
	
.prematureEnd
	PLA
	PLX
	PLY
	RTL
}

RunActorCode:
{
	PHA
	CMP #$00
	BNE .skipGroundShake

	            ; ...while we're at it, zero out the APU ports too.
	STZ $2140
	STZ $2141
	STZ $2142
	STZ $2143

	LDA $1887       ; \ If shake ground timer is set 
	BEQ .skipGroundShake    ;  | 
	DEC $1887       ;  | Decrement timer 
	AND #$03        ;  | 
	TAX         ;  | 
	LDA $00A1CE,x       ;  | 
	STA $1888       ;  | $1888-$1889 = Amount to shift level 
	LDA $00A1D2,x       ;  | 
	STA $1889       ;  /
 .skipGroundShake
	PLA

	STZ $0E
	STA $0F
	LDX #$0F
 .loop
	LDA !actorExists,x
	BNE .run
 .back
	DEX
	BMI .quitLongJump
	BRA .loop
 .quitLongJump
	JMP .quit
 .run
	STX $15E9
	;TXA
	;ASL
	;ASL
	;STA $0E                    ; $0E is the actor's dword index to the OAM (each actor gets 4 tiles).
	LDA $0F
	BNE .runNMI

	LDA !actorExists,x
	CMP #$02
	BEQ .noRunINIT
	PHP
	PHY
	LDY !actorTableIndex,x
	LDA ActorInitTable+0,y
	STA $00
	LDA ActorInitTable+1,y
	STA $01
	LDA ActorInitTable+2,y
	STA $02
	PLY
	PHB
	;LDA $02
	PHA
	PLB
	PHK
	PEA.w .ret1-1

	JML [$0000]
 .ret1
	PLB
	PLP
	LDA #$02
	STA !actorExists,x

 .noRunINIT
	LDA !textIsAppearing
	PHA
	LDA !currentSpeaker
	BMI +
	CPX !currentSpeaker
	BEQ +
	STZ !textIsAppearing
+
	PHP
	PHY
	LDY !actorTableIndex,x
	LDA ActorMainTable+0,y
	STA $00
	LDA ActorMainTable+1,y
	STA $01
	LDA ActorMainTable+2,y
	STA $02
	PLY
	PHB
	;LDA $02
	PHA
	PLB
	PHK
	PEA.w .ret2-1
	JML [$0000]
 .ret2
	PLB
	PLP
	PLA
	STA !textIsAppearing

	BRA .back

 .runNMI
	PHY
	LDY !actorTableIndex,x
	LDA ActorNmiTable+0,y
	STA $00
	LDA ActorNmiTable+1,y
	STA $01
	LDA ActorNmiTable+2,y
	STA $02
	PLY
	PHK
	PEA.w .ret3-1
	JML [$0000]
 .ret3
	JMP .back

 .quit
	RTL
}
