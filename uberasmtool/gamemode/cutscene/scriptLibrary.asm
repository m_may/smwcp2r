; Remember to set things like the VRAM address, CGRAM address, etc. before starting.
macro StandardDMA(register, source, size, transferMode)
	REP #$20
	LDX <transferMode>		; \ 2 registers write once.
	STX $4300			; /
	LDX.b <register>		; \ Write to $2118
	STX $4301			; / 
	LDA.w #<source>		; \
	STA $4302			; |
	LDX.b #<source>>>16		; | Source is the address of the first tile.
	STX $4304			; / 	
	LDA <size>			; \
	STA $4305			; /
	SEP #$20			; \		
	LDA #$01			; |	Begin DMA.
	STA $420B			; /
endmacro

WaitAndUploadFromBuffer:
{
	STX !NMIUploadDest
	STY !NMIUploadSize
	SEP #$30
	LDA #$01
	STA !NMIUploadFlag
	WaitForNMI:
 -
	WAI         ; \
	LDA $4212       ; | Wait for NMI, when the graphics will be uploaded.
	BPL -           ; /
	SEP #$30
	RTL
}

QuickActorCodeRun:              ; Call this from within scripts during certain circumstances.
{
	; If A is 0, then we run INIT and MAIN.
	; Otherwise, we run NMI.
	; Note that no actor code should touch $0F.
	JSL $7F8000
	LDA !textIsAppearing
	PHA
	LDA #$00
	STA !textIsAppearing            ; No, NOT STZ.  The LDA #$00 is important.
	JSL RunActorCode
	PLA
	STA !textIsAppearing
	RTL
}

TurnForcePauseOn:
	LDA #$01
	STA !forcePause
	RTL

;UNUSED
;TurnForcePauseOff:
;	STZ !forcePause
;	RTL

SetSpeakerToNull:
	LDA #$FF
	STA !currentSpeaker
	RTL

SetSpeaker:
	PHX
	TAX
	LDA !actorSlot,x
	STA !currentSpeaker
	PLX
	RTL
