!CS1GardenTilemapExGFX      = $0203
!CS1GardenTilemapExGFXSize  = $0800

!CS1GardenChrExGFX          = $0204
!CS1GardenChrExGFXSize      = $1000

!CS1DesertTilemapExGFX		= $0206
!CS1DesertTilemapExGFXSize	= $0800

!CS1DocCrocExGFX            = $0205
!CS1DocCrocExGFXSize        = $1000

!CS1HindenBirdExGFX	        = $01DB
!CS1HindenBirdExGFXSize	    = $1000

!CS1GirderTilemapExGFX      = $0207
!CS1GirderTilemapExGFXSize  = $0800

!CS1PierTilemapExGFX        = $0208
!CS1PierTilemapExGFXSize    = $0800

!CS1HouseTilemapExGFX       = $0209
!CS1HouseTilemapExGFXSize   = $0800

!CS1FishTilemapExGFX        = $020A
!CS1FishTilemapExGFXSize    = $0800

!CS1FishChr1ExGFX           = $020B
!CS1FishChr1ExGFXSize       = $1000

!CS1FishChr2ExGFX           = $020C
!CS1FishChr2ExGFXSize       = $1000

!CS1SeaBackTilemapExGFX     = $020F
!CS1SeaBackTilemapExGFXSize = $0800

!CS1DesertBackTilemapExGFX  = $0210
!CS1DesertBackTilemapExGFXSize  = $0800

!CS1BackgroundChrExGFX      = $0211
!CS1BackgroundChrExGFXSize  = $1000

!CS1SkyForeTilemapExGFX     = $0212
!CS1SkyForeTilemapExGFXSize = $0800

!CS1LuigiChrExGFX           = $020E
!CS1LuigiChrExGFXSize       = $1000

Cutscene1Palette:
incbin Cutscene1.rawpal

	; Control codes:
	; $F0 - $FF: Wait for that many frames ($F0 = 1 frame, $FF = 16 frames).  Multiplied by text speed.
	; $EF: Wait for button press.
	; $EE: Possible break (use after dashes and such; indicates that a word can break here without a space).
	; $ED: Space.
	; $EC: New line
	; $EB: Clear all text.
	; $EA $XX: Set the text speed to $XX.
	; $E9 $XX $YY $ZZ: JSL to $ZZXXYY.
	; $E8 $XX $YY $ZZ $WW: JSL to $ZZXXYY with $WW in A.
	; $E7 $XX: Set font to XX
	; $E0: Stop.

CSINIT1:
	LDA #$19   ; \ Change music to "I Smell a Rat"
	STA $1DFB  ; /

	STZ $2121
	%StandardDMA(#$2122, Cutscene1Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30				; \
	LDA #!CS1GardenTilemapExGFX		; |
	LDX #!layer1MapAddr>>1			; | Upload garden tilemap
	LDY #!CS1GardenTilemapExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	

	LDA #!CS1GardenChrExGFX			; \
	LDX #!layer1ChrAddr>>1			; | Upload foreground GFX
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1BackgroundChrExGFX		; \
	LDX #!layer1ChrAddr+$1000>>1		; | Upload background GFX
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	LDA #!CS1DocCrocExGFX			; \
	LDX #$C000>>1				; | Upload Doc Croc and Madam Mau GFX (SP1)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1HindenBirdExGFX		; \
	LDX #$C000>>1|$800			; | Upload Hindenbird GFX (SP2)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1LuigiChrExGFX			; \
	LDX #$C000>>1|$1000			; | Upload Luigi/Fish GFX (SP3)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1SeaBackTilemapExGFX		; \
	LDX #!layer2MapAddr>>1			; | Upload the sea background
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CS1FishChr1ExGFX			; \
	LDX #!layer1ChrAddr+$2000>>1		; | 
	LDY #!CS1FishChr1ExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; | Upload fish GFX
	LDA #!CS1FishChr2ExGFX			; |
	LDX #!layer1ChrAddr+$3000>>1		; | 
	LDY #!CS1FishChr2ExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /

	SEP #$30
	
	JSL CS1SetBlueBackgroundColor
	
	LDA #$20
	STA $20
	STZ $21
	
	LDA #!DocCroc : JSL NewActor
	
	LDA #$70 : JSL DocCrocSetXPos
	
	RTL

UploadCS1DesertForeTilemap:
	REP #$30
	LDA #!CS1DesertTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer1MapAddr+$800>>1
	LDY #!CS1DesertTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	SEP #$30
	RTL
	
UploadCS1DesertBackTilemap:
	REP #$30
	LDA #!CS1DesertBackTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer2MapAddr+$800>>1
	LDY #!CS1DesertBackTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	SEP #$30
	RTL
	
CS1SetDesertBackgroundColor:
	LDA #$1F
	STA !backgroundColor
	LDA #$1B
	STA !backgroundColor+1
	LDA #$18
	STA !backgroundColor+2
	RTL
	
CS1SetBlueBackgroundColor:
	LDA #$0C			; \
	STA !backgroundColor		; |
	LDA #$19			; |
	STA !backgroundColor+1		; |
	LDA #$1E			; |
	STA !backgroundColor+2		; /
	RTL
	
CS1SetSceneToSlot1:
	JSL WaitForNMI
	LDA.b #!layer1MapAddr>>11<<2		; \
	STA $2107				; | Using slot 1
	LDA.b #!layer2MapAddr>>11<<2		; |
	STA $2108				; |
	RTL					; /	
	
CS1SetSceneToSlot2:
	JSL WaitForNMI
	LDA.b #!layer1MapAddr+$800>>11<<2	; \
	STA $2107				; | Using slot 1
	LDA.b #!layer2MapAddr+$800>>11<<2	; |
	STA $2108				; |
	RTL					; /	
	

UploadCS1GirderForeTilemap:
	REP #$30
	LDA #!CS1GirderTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer1MapAddr>>1
	LDY #!CS1GirderTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	SEP #$30
	RTL
	

UploadCS1PierTilemap:
	REP #$30
	LDA #!CS1PierTilemapExGFX
	JSL DecompressGFXFile
	;SEP #$20
	REP #$30
	LDA #$0040
	STA $20
	LDA $1E
	CLC
	ADC #$0080
	STA $1E
	
	LDX #!layer1MapAddr>>1
	LDY #!CS1PierTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	SEP #$30
	RTL
	
CS1FishPalette:
db $86, $0C, $09, $15, $89, $15, $EA, $19, $8F, $19, $8B, $2D, $ED, $35, $4D, $26 
db $AE, $26, $F1, $1D, $74, $2A, $D5, $2A, $71, $4A, $F3, $4E, $58, $5F, $DC, $73

UploadCS1FishTilemap:
	LDA #$05
	STA !backgroundColor
	LDA #$03
	STA !backgroundColor+1
	LDA #$02
	STA !backgroundColor+2

	REP #$30
	LDA #$00D9
	STA $1A
	LDA #$00F4
	STA $1C
	
	LDA #$00D0
	STA $20
	
	LDA #!CS1FishTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer1MapAddr>>1
	LDY #!CS1FishTilemapExGFXSize
	JSL WaitAndUploadFromBuffer

	SEP #$30
	LDA #$30
	STA $2121
	%StandardDMA(#$2122, CS1FishPalette, #$0020, #$00)
	RTL
	
UploadCS1HouseTilemap:
	REP #$30
	LDA #$00E0
	STA $1A
	LDA #$00C0
	STA $1C
	LDA #!CS1HouseTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer1MapAddr>>1
	LDY #!CS1HouseTilemapExGFXSize
	JSL WaitAndUploadFromBuffer

	RTL
	
CS1SetBG2VToA0:
	REP #$20
	LDA #$00A0
	STA $20
	SEP #$20
	RTL
	
CS1SetBG2VTo20:
	REP #$20
	LDA #$0020
	STA $20
	SEP #$20
	RTL
	
;CS1SetBG2VTo30:
;	REP #$20
;	LDA #$0030
;	STA $20
;	SEP #$20
;	RTL
	
;CS1SetBG2VTo40IncH:
;	REP #$20
;	LDA #$0040
;	STA $20
;	LDA $1E
;	CLC
;	ADC #$0080
;	STA $1E
;	SEP #$20
;	RTL
	
UploadCS1SkyForeTilemap:
	REP #$30
	LDA #!CS1SkyForeTilemapExGFX
	JSL DecompressGFXFile

	REP #$30
	LDA #$0030
	STA $20
	
	LDX #!layer1MapAddr>>1
	LDY #!CS1SkyForeTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	REP #$30
	SEP #$30
	RTL
	
UploadCS1SeaBackTilemap:
	REP #$30
	LDA #!CS1SeaBackTilemapExGFX
	JSL DecompressGFXFile
	SEP #$20
	REP #$10
	LDX #!layer2MapAddr>>1
	LDY #!CS1SeaBackTilemapExGFXSize
	JSL WaitAndUploadFromBuffer
	REP #$30
	SEP #$30
	RTL
	
	
	


CS1T1:
%VWFASM(DocCrocSetToFacingCamera)
%VWFASMArg(SetSpeaker, !DocCroc)
db "Heeeeeeey kids! It's yer old pal Doc Croc, here to tell you about some "
db "exciting news! Norveg Industries is going global, and to kick things off, we're "
db "expanding our production into territories all across the continent--", $EE, "including the Mushroom Kingdom!", $EF, $EB

CS1T2:
%VWFASM(DocCrocSetToWalking)
db "This expansion means all sorts of new and exciting opportunities for our customers, and "
db "for all our friends living near our newest branches of operation. From our company's "
db "inception, our founder has recognized the importance of working together with the "
db "community to ensure a better and brighter future for us all.", $EC, $EC

CS1T3:
%VWFASM(DocCrocSetToPointing)
db "Isn't that right, Madam Mau?"
db $EF, $EB

CS1T4:

%VWFASM(UploadCS1DesertForeTilemap)	; \ Upload the foreground, wait 1 frame.
db $F0					; /

%VWFASM(UploadCS1DesertBackTilemap)	; \ Upload the background, wait 1 frame.
db $F0					; /


%VWFASMArg(DeleteActor, !DocCroc)
%VWFASMArg(NewActor, !MadamMau1)
%VWFASM(QuickActorCodeRun)

%VWFASM(CS1SetBG2VToA0)
%VWFASM(CS1SetSceneToSlot2)		; Switch scenes.
%VWFASM(CS1SetDesertBackgroundColor)


db $F0
%VWFASMArg(SetSpeaker, !MadamMau1)
db $EA, $01, "A speck of light in the darkness, swelling to a brief, blinding flare, all-", $EE, "engulfing, "
db "all-", $EE, "consuming. Then, just as quickly, it fades away, back into the same blackness "
db "whence it came, leaving naught but a memory, until that, too, is consumed by the void. "
db "This cycle, ever-", $EE, "repeated, is the sum of all human endeavors."
db $EE, $EF, $EB

CS1T5:
%VWFASM(UploadCS1GirderForeTilemap)	; \ Upload the foreground, wait 1 frame.
db $F0					; /

%VWFASM(UploadCS1SeaBackTilemap)	; \ Upload the background, wait 1 frame.
db $F0					; /


%VWFASMArg(DeleteActor, !MadamMau1)	; \
%VWFASMArg(NewActor, !DocCroc)		; | Update the actors
%VWFASMArg(DocCrocSetXPos, #$70)	; |
%VWFASM(DocCrocSetToFacingCamera)	; |
%VWFASM(QuickActorCodeRun)		; /

%VWFASM(CS1SetBG2VTo20)
%VWFASM(CS1SetBlueBackgroundColor)
%VWFASM(CS1SetSceneToSlot1)		; Switch scenes.



%VWFASMArg(SetSpeaker, !DocCroc)
db $F0
db $EA, $00, "You said it, Madam! ", $EF
%VWFASM(DocCrocSetToWalking)
db "And at our company, we strive to build on that proud tradition of hard "
db "work and ingenuity to shine our light of progress in the darkness!", $EF, $EB

CS1T6:
db "At Norveg Industries, we view our customers as more than just consumers of our goods and "
db "services--", $EE, "we think of them as our friends and valued partners. That's why we've partnered with "
db "one of our longest-", $EE, "standing and most loyal clients, the Koopa Troop, to provide us "
db "with some of our most competent security personnel to ensure the safety of both our "
db "workers and the general public.", $EF, $EB, "At Norveg Industries, your safety is our number one "
db "concern--", $EE
%VWFASM(DocCrocSetToPointing)
db "just ask our Director of Security and Public Safety, Captain Hindenbird.", $EF, $EB

CS1T7:

%VWFASMArg(DeleteActor, !DocCroc)
%VWFASMArg(NewActor, !Hindenbird)
%VWFASMArg(HindenbirdSetXPosition, #$78)
%VWFASMArg(HindenbirdSetYPosition, #$2D)

%VWFASM(QuickActorCodeRun)

;%VWFASM(CS1SetBG2VTo30)
%VWFASM(UploadCS1SkyForeTilemap)




db $F0
%VWFASMArg(SetSpeaker, !Hindenbird)
db "With my Jeptak scouts, I can spot a security threat at 100 kilometers, and my "
db "stealth bombers can then swoop in and wipe it straight off the map before they even know "
db "they've been spotted.", $EC
db "Boom", $F4, $EC
db "  boom", $F4, $EC
%VWFASM(HindenbirdSetToFlyingOff)
db "   acka-lacka-lacka-", $EC
db "          BOOM!", $EF, $EB

CS1T8:
%VWFASMArg(DeleteActor, !Hindenbird)
%VWFASMArg(NewActor, !DocCroc)
%VWFASMArg(DocCrocSetXPos, #$70)
%VWFASM(DocCrocSetToFacingCamera)

%VWFASM(QuickActorCodeRun)

;%VWFASM(CS1SetBG2VTo40IncH)
%VWFASM(UploadCS1PierTilemap)


db $F0
%VWFASMArg(SetSpeaker, !DocCroc)
db "Wow, cap, that's sure some passionate devotion to public welfare!", $EF, $EB

CS1T9:
%VWFASM(DocCrocSetToWalking)
db "Well, folks, these sure are exciting times ahead of us. Be on the lookout "
db "for our products and facilities in an area near you in the very near future. This is one "
db "opportunity you won't want to miss--", $EE
%VWFASM(DocCrocSetToFacingCamera)
db "at Norveg Industries, we belive in striking while the iron "
db "is hot, and with you around, you can be sure the future is positively ", !CCSetFont, $02, "smoking", !CCSetFont, $00, "! See ya!", $EF, $EB

CS1TA:
%VWFASMArg(DeleteActor, !DocCroc)
%VWFASMArg(NewActor, !Luigi)
%VWFASMArg(SetSpeaker, !Luigi)
%VWFASM(LuigiSetToStanding)
%VWFASMArg(LuigiSetX, $F0)
%VWFASM(LuigiSetToHolding)
%VWFASMArg(NewActor, !Mario)
%VWFASMArg(MarioSetXPos, $4C)
%VWFASM(MarioSetPoseToSuspicion)
%VWFASM(QuickActorCodeRun)
%VWFASM(UploadCS1HouseTilemap)
db $FF, $FF
%VWFASM(LuigiSetToWalkingLeft)
db "Hey, Mario! I was"
%VWFASM(MarioSetPoseToStanding)
db " just fishing down by the lake, and look what I caught! ", $EF, $EB
%VWFASMArg(DeleteActor, !Luigi)
%VWFASMArg(DeleteActor, !Mario)
%VWFASM($7F8000)
%VWFASM(QuickActorCodeRun)
%VWFASM(UploadCS1FishTilemap)
db $FF, $FF
db "Ugly little thing, isn't he?", $EF, $EB

CS1TB:
%VWFASMArg(NewActor, !Luigi)
%VWFASM(LuigiSetToHolding)
%VWFASM(LuigiSetToStanding)
%VWFASMArg(LuigiSetX, $90)
%VWFASMArg(NewActor, !Mario)
%VWFASMArg(MarioSetXPos, $4C)
%VWFASM(MarioSetPoseToStanding)
%VWFASM(QuickActorCodeRun)
%VWFASM(UploadCS1HouseTilemap)
%VWFASMArg(SetSpeaker, !Fish)
db "Oh, I say, sir!", $EF, $EB

CS1TC:
%VWFASMArg(NewActor, !Fish)
%VWFASM(LuigiSetToNotHolding)
db "I admit, you fooled me fair and square with your artificial Lumbricinan ruse, but "
db "there's no need to be personally insulting about it. Humility is the essence of good "
db "sportsmanship, after all."
db $EF, $EB

CS1TD:
%VWFASM(FishSetToDressing)
%VWFASMArg(SetSpeaker, !Luigi)
db "Sorry!", $EF, $EB

CS1TE:
db "Well, that was awkward. I wonder if it has anything to do with that factory "
db "that just opened up on the eastern lakeshore_ ", $F7, $EA, $03, "<Sverry Industries>", $EA, $00, ", or "
db "something like that.", $EF, $EB

CS1TF:
%VWFASMArg(SetSpeaker, !Toad)
db "Mario! Mario!"
%VWFASM(TurnForcePauseOn)
%VWFASMArg(NewActor, !Toad)
db $EF, $EB

CS1T10:
db "There's a massive construction boom going on all around the Mushroom Kingdom! "
db "Someone called, uh, ", $EA, $03, "<Dan-Mark Industries>", $EA, $00, " or something is buying up land all over the place!", $EF, $EB

CS1T11:
db "They even got my favorite golf course! Massive girders are shooting up everywhere! "
db "Half the countryside looks like one big Richard Serra exhibition!", $EF, $EB

CS1T12:
db "They've opened up branches in all the surrounding countries, and now they're "
db "expanding into the Mushroom Kingdom, too? This sounds decidedly non-hunky-dory. "
db "Maybe you'd better pay this <Doc Croc> character a visit and explain that we're not "
db "too comfortable with this.", $EF, $EB

CS1T13:
%VWFASM(LuigiSetToFacingLeft)
%VWFASMArg(SetSpeaker, !Luigi)
db "Yeah, I'm sure this is all just a simple misunderstanding which we can solve quickly and "
db "easily, without any trouble. Just some good old-fashioned chatting between businesses and "
db "concerned citizens to reach a fair solution for everyone.", $EF, $EB

CS1T14:
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToWalkOffscreenRight)
%VWFASM(ToadSetToUnSad)
db "Good luck, bro!", $F8, $EC, $EB

CS1T15:
%VWFASMArg(SetSpeaker, !Toad)
db "Fracture a fibula!", $EF, $EB, $E0
