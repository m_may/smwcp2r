font1:
incbin "vwffont.bin"
font2:
incbin "vwffonttruntec.bin"
font3:
incbin "vwffontitalic.bin"

fonttable:
dw font1, font2, font3

widthstable:
dw widths1, widths2, widths1

widths1:
;   A    B    C    D    E    F    G    H
db $05, $05, $05, $05, $04, $04, $05, $05
;   I    J    K    L    M    N    O    P
db $01, $04, $05, $04, $07, $05, $05, $05
;   Q    R    S    T    U    V    W    X
db $05, $05, $05, $05, $05, $05, $07, $05
;   Y    Z    a    b    c    d    e    f
db $05, $05, $05, $05, $05, $05, $05, $03
;   g    h    i    j    k    l    m    n
db $05, $05, $01, $03, $05, $02, $07, $05
;   o    p    q    r    s    t    u    v
db $05, $05, $05, $04, $05, $04, $05, $05
;   w    x    y    z    0    1    2    3
db $07, $05, $05, $04, $05, $03, $05, $05
;   4    5    6    7    8    9    !    ?
db $07, $05, $05, $04, $03, $05, $01, $05
;   "<  >"    :    ;    ~    (    )    '
db $05, $05, $01, $02, $05, $03, $03, $02
;   .    ,    =    -    +    /    *    %
db $01, $02, $05, $05, $05, $03, $05, $08
;  ...   [ ]  [  TM ]  note
db $07, $07, $07, $07, $06, $07, $07, $07

widths2:
;   A    B    C    D    E    F    G    H
db $07, $07, $07, $07, $07, $07, $07, $07   ; 00
;   I    J    K    L    M    N    O    P
db $07, $07, $07, $07, $07, $07, $07, $07   ; 08
;   Q    R    S    T    U    V    W    X    
db $07, $07, $07, $07, $07, $07, $07, $07   ; 10
;   Y    Z    a    b    c    d    e    f
db $07, $07, $07, $07, $07, $07, $07, $07   ; 18
;   g    h    i    j    k    l    m    n
db $07, $07, $07, $07, $07, $07, $07, $07   ; 20
;   o    p    q    r    s    t    u    v
db $07, $07, $07, $07, $07, $07, $07, $07   ; 28
;   w    x    y    z    0    1    2    3
db $07, $07, $07, $07, $07, $07, $07, $07   ; 30
;   4    5    6    7    8    9    !    ?
db $07, $07, $07, $07, $07, $07, $07, $07   ; 38
;   "<  >"    :    ;    ~    (    )    '
db $07, $07, $07, $07, $07, $07, $07, $07   ; 40
;   .    ,    =    -    +    /    *    %
db $07, $07, $07, $07, $07, $07, $07, $07   ; 48
;  ...   [ ]  [  TM ]  note
db $07, $07, $07, $07, $06, $07, $07, $07   ; 50
