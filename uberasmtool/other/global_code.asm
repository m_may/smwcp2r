; Note that since global code is a single file, all code below should return with RTS.
load:
init:
RTS

;handle our play time counter
;it could be in a library file to be called from various gamemode uberasm files, but I think it's more flexible this way
main:
PlayTimeHandler:
LDA $0100|!base2		;load current game mode
CMP #$0E			;check if on overworld
BEQ RollItBuddy			;if yes, update the timer.
CMP #$14			;check if on level
BEQ RollItBuddy			;if yes, update the timer.
CMP #$33			;check if on stats screen
BEQ RollItBuddy			;if yes, update the timer.

WeVe100edDaGame:
RTS				;return.

RollItBuddy:
JSL ProgressCheck_is100ed
BCS WeVe100edDaGame  ;don't update the clock when having 100 percented the game

LDA !PlayTimeCounter+3		;load play time's frames
INC				;increment it by one
CMP #$3C			;compare to value
BCC NotASecond			;if it didn't match one second, skip ahead.

LDA !PlayTimeCounter		;load play time's seconds
INC				;increment by one
CMP #$3C			;compare to value
BCC NotAMinute			;if it didn't match one minute, skip ahead.

LDA !PlayTimeCounter+1		;load play time's minutes
INC				;increment by one
CMP #$3C			;compare to value
BCC NotAnHour			;if it didn't match one hour, skip ahead.

LDA !PlayTimeCounter+2		;load play time's hours
CMP #$63			;compare to value
BEQ StopClock			;if equal, we won't do anything anymore. it's the end of time.
INC				;increment by one
STA !PlayTimeCounter+2		;store to play time's hours.

LDA #$00			;load value

NotAnHour:
STA !PlayTimeCounter+1		;store to play time's minutes.

LDA #$00			;load value

NotAMinute:
STA !PlayTimeCounter		;store to play time's seconds.

LDA #$00			;load value

NotASecond:
STA !PlayTimeCounter+3		;store to play time's frames.

StopClock:
RTS				;return.
