;init - HDMA to CGRAM
;main - ???

!BlackMarketLevelNumber = $1FF

!DuckTimer = $0F5E|!addr

ColorTable:
db $40 : dw $4747,$7EEB
db $0B : dw $4747,$7EEC
db $0B : dw $4747,$7F0C
db $0B : dw $4747,$7F0D
db $0B : dw $4747,$7F2D
db $0B : dw $4747,$7F2E
db $0B : dw $4747,$7F2F
db $0B : dw $4747,$7F4F
db $0B : dw $4747,$7F6F
db $0B : dw $4747,$7F70
db $01 : dw $4747,$7F90
db $00

init:
REP #$20
LDA #$2103  ;mode 3 transfer to $2121
STA $4330
LDA #ColorTable
STA $4332
SEP #$20
LDA #ColorTable>>16
STA $4334
LDA #$08
TSB $0D9F  ;Enable HDMA channel 3

STZ !DuckTimer
RTL

main:
LDA $73
BEQ .noduck
LDA !DuckTimer
CMP #$FF
BEQ Warp
INC !DuckTimer
RTL
.noduck
STZ !DuckTimer
RTL

Warp:
STZ !DuckTimer
ChangeExit:
LDX $97 ;note: will break on horizontal/EXLEVELs
LDA.b #!BlackMarketLevelNumber
STA $19B8|!addr,x
LDA.b #%00000100|(!BlackMarketLevelNumber>>8)
STA $19D8|!addr,x
EnterUpFacingPipe:  ;based on code from $00F3FF
LDA #$20
LDX $187A|!addr  ;if on yoshi
BEQ +
LDA #$30
+
STA $88  ;frames until warp
INC $9D  ;lock animation and sprites = on
STZ $76  ;player direction = left
LDA #$03
STA $89  ;pipe action = enter an up-facing vertical pipe
LDA #$08
STA $1499|!addr  ;player face screen timer
LDA #$02
STA $1419|!addr  ;yoshi face screen
LDA #$06
STA $71  ;player action = enter/exit a vertical pipe
;disable inputs
STZ $15
STZ $16
STZ $17
STZ $18
;sfx
LDA #$04
STA $1DF9|!addr
RTL
