;Lava Lift Lair ASM (level 122).
;Includes:
;load: SMWC coin custom trigger init
;init/nmi: 2-channel HDMA gradient, layer 3 parallax init and processing
;nmi: layer 3 parallax processing
;main: incrementing autoscroll indexes

!scansub = $0F5E|!addr
!autoscroll_index1 = $0F60|!addr
!autoscroll_index2 = $0F62|!addr
!autoscroll_index3 = $0F64|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL

main:
LDA $71
ORA $9D
ORA $13D4
BNE ++

LDA $14
AND #$07
BNE +

REP #$20
INC !autoscroll_index1
SEP #$20

+
LDA $14
AND #$0F
CMP #$07
BNE +

REP #$20
INC !autoscroll_index2
SEP #$20

+
LDA $14
AND #$1F
BNE ++

REP #$20
INC !autoscroll_index3
SEP #$20

++
RTL

nmi:
LDA $24
SEC
SBC #$C0
STA !scansub		;amount of scanlines to subtract from first entry

LDA #$7F
SEC
SBC !scansub
STA !hdmaparallaxtbls
LDA #$0F
STA !hdmaparallaxtbls+3
LDA #$10
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+12
STA !hdmaparallaxtbls+15
LDA #$08
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+18

REP #$20
LDA $1466|!addr
LSR
STA $00
LSR
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4
LDA $00
CLC
ADC !autoscroll_index3
STA !hdmaparallaxtbls+7
LDA $00
CLC
ADC !autoscroll_index2
STA !hdmaparallaxtbls+10
LDA !autoscroll_index3
LSR
CLC
ADC !autoscroll_index2
ADC $00
STA !hdmaparallaxtbls+13
LDA $00
CLC
ADC !autoscroll_index1
STA !hdmaparallaxtbls+16
LDA !autoscroll_index2
LSR
CLC
ADC $00
ADC !autoscroll_index1
STA !hdmaparallaxtbls+19
SEP #$20

LDA #$00
STA !hdmaparallaxtbls+21
RTL

Table1:
db $02,$2A,$51
db $03,$2B,$51
db $01,$2B,$52
db $03,$2C,$52
db $01,$2C,$53
db $03,$2D,$53
db $02,$2E,$54
db $01,$2F,$54
db $02,$2F,$55
db $02,$30,$55
db $03,$31,$56
db $02,$32,$57
db $01,$33,$57
db $02,$33,$58
db $82,$34,$58,$34,$59
db $02,$35,$59
db $01,$35,$5A
db $03,$36,$5A
db $03,$37,$5B
db $01,$38,$5B
db $03,$38,$5C
db $04,$39,$5C
db $03,$39,$5D
db $07,$3A,$5D
db $04,$3A,$5E
db $05,$3B,$5E
db $08,$3B,$5F
db $13,$3C,$5F
db $19,$3D,$5F
db $08,$3E,$5F
db $05,$3E,$5E
db $04,$3E,$5D
db $03,$3E,$5C
db $03,$3E,$5B
db $03,$3E,$5A
db $03,$3E,$59
db $03,$3E,$58
db $02,$3E,$57
db $03,$3E,$56
db $02,$3F,$55
db $02,$3F,$54
db $03,$3F,$53
db $02,$3F,$52
db $02,$3F,$51
db $03,$3F,$50
db $02,$3F,$4F
db $02,$3F,$4E
db $02,$3F,$4D
db $03,$3F,$4C
db $02,$3F,$4B
db $02,$3F,$4A
db $03,$3F,$49
db $02,$3F,$48
db $03,$3F,$47
db $03,$3F,$46
db $03,$3F,$45
db $03,$3F,$44
db $03,$3F,$43
db $04,$3F,$42
db $04,$3F,$41
db $0E,$3F,$40
db $00

Table2:
db $04,$8D
db $06,$8E
db $05,$8F
db $04,$90
db $04,$91
db $05,$92
db $06,$93
db $1C,$94
db $0B,$93
db $0A,$92
db $08,$91
db $07,$90
db $08,$8F
db $07,$8E
db $08,$8D
db $08,$8C
db $07,$8B
db $07,$8A
db $07,$89
db $06,$88
db $06,$87
db $06,$86
db $07,$85
db $07,$84
db $07,$83
db $08,$82
db $09,$81
db $12,$80
db $00
