;Dangling Danger ASM (level 11D).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $02,$2D,$50
db $0E,$2D,$4F
db $0A,$2D,$4E
db $0E,$2D,$4D
db $04,$2E,$4D
db $05,$2E,$4C
db $0A,$2F,$4C
db $06,$30,$4C
db $05,$30,$4D
db $19,$31,$4D
db $03,$32,$4D
db $17,$32,$4E
db $08,$32,$4F
db $0C,$33,$4F
db $17,$33,$50
db $0D,$33,$51
db $2F,$34,$51
db $00

Table2:
db $08,$98
db $14,$99
db $11,$9A
db $4B,$9B
db $28,$9A
db $40,$99
db $00