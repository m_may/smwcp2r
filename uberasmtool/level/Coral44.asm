;Coral Corridor ASM (level 44).
;Includes:
;load: SMWC coin trigger initializer
;init: translucent layer 3 handler, 2-channel HDMA gradient, wave HDMA init and processing
;main: wave HDMA processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

Table1:
db $05,$22,$4E
db $07,$23,$4E
db $01,$23,$4F
db $0B,$24,$4F
db $06,$25,$4F
db $07,$25,$50
db $0A,$26,$50
db $01,$27,$50
db $08,$27,$51
db $05,$28,$51
db $02,$28,$52
db $06,$29,$52
db $01,$2A,$52
db $03,$2A,$53
db $04,$2B,$53
db $01,$2B,$54
db $04,$2C,$54
db $02,$2D,$54
db $01,$2D,$55
db $03,$2E,$55
db $01,$2F,$55
db $04,$2F,$56
db $05,$30,$56
db $05,$31,$57
db $04,$32,$57
db $02,$32,$58
db $07,$33,$58
db $02,$34,$58
db $04,$34,$59
db $08,$35,$59
db $08,$36,$5A
db $07,$37,$5A
db $02,$37,$5B
db $0A,$38,$5B
db $05,$39,$5B
db $06,$39,$5C
db $0B,$3A,$5C
db $02,$3B,$5C
db $09,$3B,$5D
db $0A,$3C,$5D
db $0A,$3D,$5E
db $06,$3E,$5E
db $03,$3E,$5F
db $05,$3F,$5F
db $00

Table2:
db $80,$9E
db $10,$9E
db $50,$9F
db $00

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond