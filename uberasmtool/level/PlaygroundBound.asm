;Playground Bound ASM.
;Includes:
;load: SMWC coin trigger initializer
;init/nmi: 2-channel HDMA gradient (library), enable and process layer 3 parallax
;nmi: process layer 3 parallax


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL HDMA_PlaygroundBound	;call HDMA from library for this level

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
LDA $1468|!addr
SEC
SBC #$A8
STA $0F5E|!addr

LDA #$63
SEC
SBC $0F5E|!addr
STA !hdmaparallaxtbls
STA !hdmaparallaxtbls+3

REP #$20
LDA $1466|!addr
LSR #2
STA !hdmaparallaxtbls+4
LSR #2
STA !hdmaparallaxtbls+1
SEP #$20

LDA #$00
STA !hdmaparallaxtbls+6
RTL