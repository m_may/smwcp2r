;Cumulus Chapel ASM (levels 11B and 179).
;Includes:
;load: SMWC coin custom trigger init, parallax HDMA init
;init: 2-channel HDMA gradient, parallax HDMA processing, message box text DMA
;init and nmi: parallax HDMA, and layer 3 message/parallax HDMA positioning.
;main: parallax logic, winged yoshi, make yoshi blue
;nmi: parallax HDMA processing

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $87				;reset address (corresponds to !ManualMsg in CumulusGeneric.asm, library file)

REP #$20			;16-bit A
LDX #$0A ;temp
LDA.w #Messages_Props_Normal ;temp
JSL Messages_DMA_FillProp ;temp
LDX #$0A
LDA.w #Messages_Cumulus11B
JSL Messages_DMA
SEP #$20

JSL CumulusGeneric_PrepTableAndGrad
BRA main			;branch ahead - part of nmi code isn't relevant here

nmi:
JSL CumulusGeneric_SetCoords

LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
RTL

main:
LDA #$02
STA $141E

LDX #$0B

-
LDA $9E,x
CMP #$35
BNE .redo

LDA $15F6,x
AND #$F1
ORA #$06
STA $15F6,x
BRA done

.redo
DEX
BPL -

done:
JML CumulusGeneric_Logic
