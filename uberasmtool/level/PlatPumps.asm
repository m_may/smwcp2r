;Plateau Pumps ASM.
;Includes:
;init: 2-channel HDMA gradient, message box text DMA
;init and nmi: calls multilayer background handling - parallax HDMA, layer 3 message/parallax HDMA positioning.
;main: parallax logic

!WindowTable = $7F8900

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL GirderMultilayer_PrepareTable
JSL GirderMultilayer_Logic

REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.
BRA +				;branch ahead - part of nmi code isn't relevant here

nmi:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
JML GirderMultilayer_HDMAOAM

main:
STZ $87				;reset address (corresponds to !ManualMsg in GirderMultilayer.asm, library file)

LDA $95				;load player's X-pos within the level, next frame, high byte
CMP #$05			;compare to value
BCC +				;if lower, skip ahead.

LDA #$01			;load value
STA $87				;store to address.

+
JML GirderMultilayer_Logic

Table1:
db $80,$2A,$56
db $1B,$2A,$56
db $45,$24,$54
db $00

Table2:
db $80,$9F
db $1B,$9F
db $45,$89
db $00

MsgDMA1:
db $FE,$28,$13,$29,$47,$29,$48,$29,$52,$29,$FE,$28,$40,$29,$51,$29
db $44,$29,$40,$29,$FE,$28,$48,$29,$52,$29,$FE,$28,$52,$29,$44,$29
db $42,$29,$54,$29,$51,$29,$44,$29,$43,$29,$FE,$28,$41,$29,$58,$29
db $FE,$28,$4E,$29,$4D,$29,$44,$29,$FE,$28,$4E,$29,$45,$29,$FE,$28
db $FE,$28,$4E,$29,$54,$29,$51,$29,$FE,$28,$56,$29,$4E,$29,$4D,$29
db $43,$29,$44,$29,$51,$29,$45,$29,$54,$29,$4B,$29,$FE,$28,$44,$29
db $4C,$29,$4F,$29,$4B,$29,$4E,$29,$58,$29,$44,$29,$44,$29,$52,$29
db $FE,$28,$40,$29,$53,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$0D,$29,$4E,$29,$51,$29,$55,$29,$44,$29,$46,$29,$FE,$28
db $08,$29,$4D,$29,$43,$29,$54,$29,$52,$29,$53,$29,$51,$29,$48,$29
db $44,$29,$52,$29,$1D,$29,$FE,$28,$0B,$29,$40,$29,$4A,$29,$48,$29
db $53,$29,$54,$29,$1B,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$0F,$29,$4B,$29,$44,$29,$40,$29,$52,$29,$44,$29,$FE,$28
db $41,$29,$44,$29,$FE,$28,$51,$29,$44,$29,$52,$29,$4F,$29,$44,$29
db $42,$29,$53,$29,$45,$29,$54,$29,$4B,$29,$FE,$28,$53,$29,$4E,$29
db $FE,$28,$47,$29,$48,$29,$4C,$29,$1D,$29,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$40,$29,$4D,$29,$43,$29,$FE,$28,$43,$3D,$4E,$3D,$4D,$3D
db $5D,$3D,$53,$3D,$FE,$28,$53,$3D,$47,$3D,$51,$3D,$4E,$3D,$56,$3D
db $FE,$28,$52,$3D,$53,$3D,$54,$3D,$45,$3D,$45,$3D,$FE,$28,$40,$3D
db $53,$3D,$FE,$3C,$47,$3D,$48,$3D,$4C,$3D,$1B,$29,$FE,$28,$FE,$28

MsgDMA2:
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$1C,$29,$FE,$28,$0B,$29,$00,$29,$15,$29,$00,$29,$FE,$28
db $00,$29,$0B,$29,$04,$29,$11,$29,$13,$29,$FE,$28,$1C,$29,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$12,$29,$53,$29,$44,$29,$44,$29,$51,$29,$FE,$28,$42,$29
db $4B,$29,$44,$29,$40,$29,$51,$29,$FE,$28,$4E,$29,$45,$29,$FE,$28
db $4B,$29,$40,$29,$55,$29,$40,$29,$FE,$28,$4F,$29,$4E,$29,$4E,$29
db $4B,$29,$52,$29,$1B,$29,$FE,$28,$16,$29,$44,$29,$FE,$28,$FE,$28
db $FE,$28,$4D,$29,$44,$29,$44,$29,$43,$29,$FE,$28,$53,$29,$47,$29
db $44,$29,$4C,$29,$FE,$28,$53,$29,$4E,$29,$FE,$28,$4C,$29,$4E,$29
db $4B,$29,$43,$29,$FE,$28,$4C,$29,$44,$29,$53,$29,$40,$29,$4B,$29
db $FE,$28,$45,$29,$4E,$29,$51,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$4E,$29,$54,$29,$51,$29,$FE,$28,$0C,$29,$54,$29,$52,$29
db $47,$29,$51,$29,$4E,$29,$4E,$29,$4C,$29,$FE,$28,$0A,$29,$48,$29
db $4D,$29,$46,$29,$43,$29,$4E,$29,$4C,$29,$FE,$28,$44,$29,$57,$29
db $4F,$29,$40,$29,$4D,$29,$52,$29,$48,$29,$4E,$29,$4d,$29,$1B,$29