;Crystalline Citadel ASM (levels 75 and 78).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 over layers 1, 2 and sprites
;init and main: layer 1 waves processing
;main: shaking layer 1 + sound effect, rising layer 3 tides

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls	;address of table
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4364
LDA #$40			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 6

mainwave:
REP #$20
LDA $24
SEC
SBC #$0040
STA $04			;this holds how much layer 3 has raised
SEP #$20

LDA $14
LSR #3
AND #$07
STA !waveindex

+
LDA #$7F		;load scanline count
SEC			;set carry
SBC $04			;subtract value from scratch RAM (how much layer 3 has raised)
STA $00			;store to scratch RAM.
BMI zeroone

sec:
LDA #$49
BRA st

zeroone:
LDA.b #$C8
SEC
SBC $04

st:
STA $01

+
REP #$20
LDA !waveindex
ASL
TAY

STZ $0E
LDX #$00		;starting index for loop

waveloop:
PHY

CPX #$00		;first entry of table varies depending on layer 1 Y-pos
BEQ first

SEP #$20
LDA #$08		;default scanline count value

LDY $0E
BNE +
CPX #$03		;second entry
BNE +

second:
INC $0E
LDA $01			;second scanline count value
BRA ssec

first:
SEP #$20
LDA $00			;this holds the dynamic scanline count set before the loop
BMI second

ssec:
STA !hdmaparallaxtbls,x

REP #$20
LDA $1C
CLC
ADC $1888|!addr
BRA ++

+
STA !hdmaparallaxtbls,x
REP #$20

PLY
LDA $1C
CLC
ADC $1888|!addr
ADC Index,y

PHA

INY #2
TYA
AND #$000F
TAY

PLA

PHY	;anticrash

++
STA !hdmaparallaxtbls+1,x
PLY
INX #3
CPX #$60
BNE waveloop

SEP #$20

LDA #$00
STA !hdmaparallaxtbls,x		;finalize table

Return:
RTL

main:
LDA $71				;load player animation pointer
ORA $9D				;OR with sprites/animation locked flag
ORA $13D4|!addr			;OR with game paused flag
BNE Return			;if any are set, return.

LDA $1887|!addr			;load shake ground timer
BNE +				;if not equal zero, skip ahead.

LDA #$09			;load value
STA $1887|!addr			;store to shake ground timer.
LDA #$1A			;load SFX value
STA $1DFC|!addr			;store to address to play it.

+
LDA $14A8|!addr			;load decrementer
BNE +				;if not equal zero, don't rise layer 3.

REP #$20			;16-bit A
LDA $24				;load layer 3 Y-pos 
CMP #$0108			;compare to value
BEQ .nomorerise			;if equal, stop rising.
INC $24				;increment layer 3 Y-pos by one - rise layer 3

.nomorerise
SEP #$20			;8-bit A
LDA #$12			;load value
STA $14A8|!addr			;store to decrementer.

+
JMP mainwave			;handle layer 1 waves

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond