;Terrifying Timbers ASM (levels 10F, AA and AB).
;Includes:
;load: SMWC coin custom trigger init
;init/nmi: 2-channel HDMA gradient, layer 2 parallax setup and processing
;main: change on/off status on jumping on level AA
;nmi: parallax processing (except level AB where there's no parallax, BG simply moves slower)


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

LDA $010B
CMP #$AB
BEQ nmi_slowlayer2

JSL TerrifyingParallax_init

REP #$20
LDA #$0F02			;mode 2 on $210F (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
BRA nmi_parallax

nmi:
LDA $010B
CMP #$AB
BEQ .slowlayer2

.parallax
JML TerrifyingParallax_nmi

.slowlayer2
REP #$20
LDA $1462
LSR #5
STA $1466
SEP #$20
RTL

main:
LDA $71
ORA $9D
ORA $13D4
BNE .return

LDA $010B
CMP #$AA
BNE .return

lda $7C
beq .not_inc_ram
	inc
	sta $7C
	cmp.b #$04
	bne +
		stz $7C
	+
.not_inc_ram

lda $75
bne .return

LDA $79
bne .check_ground

LDA $7D
BMI +
	stz $7C
	lda #$01
	sta $79
	RTL

+
inc $7C
inc $79
LDA $14AF
EOR #$01
STA $14AF
LDA #$0B
STA $1DF9
RTL

.check_ground
LDA $77
AND #$04
BEQ .return
stz $79
.return
RTL

Table1:
db $30,$20,$40
db $12,$21,$40
db $09,$22,$40
db $09,$23,$40
db $09,$24,$40
db $09,$25,$40
db $08,$26,$40
db $09,$27,$40
db $09,$28,$40
db $09,$29,$40
db $09,$2A,$40
db $07,$2B,$40
db $09,$2B,$41
db $11,$2C,$41
db $11,$2D,$41
db $10,$2E,$41
db $0B,$2F,$41
db $01,$20,$41
db $00

Table2:
db $04,$80
db $07,$81
db $08,$82
db $07,$83
db $07,$84
db $07,$85
db $08,$86
db $07,$87
db $08,$88
db $0A,$89
db $0A,$8A
db $0A,$8B
db $0A,$8C
db $0A,$8D
db $0A,$8E
db $0A,$8F
db $0A,$90
db $0A,$91
db $0A,$92
db $0B,$93
db $0A,$94
db $0A,$95
db $0B,$96
db $0A,$97
db $08,$98
db $01,$80
db $00