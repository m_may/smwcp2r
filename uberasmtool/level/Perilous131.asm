;Perilous Paths ASM (level 131).
;Includes:
;init: set up rooms cleared bitflags and set up respective custom triggers, message box text DMA, save SMWC coins collected instantly (at temp)

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA $0F33|!addr		;load timer's ones (lol i've used this as the bitflag holder for rooms cleared... coolbeans we can set thru lm)
BEQ .setnothing		;if zero, return.

TSB !PerilousPathsCleared	  ;set bits to door custom block flags.

.setnothing
LDA $7FC0FD		;load custom trigger address 2
AND #$F0		;preserve bits 4, 5, 6 and 7
ORA !PerilousPathsCleared	  ;set custom block flags
STA $7FC0FD		;store result back.

STZ $0F33|!addr		;reset timer's ones.

LDX $13BF|!addr		;load translevel number into X
%SaveTempSMWCCoins()
RTL			;return.

MsgDMA1:
incbin "msgbin/131-1msg2msg.bin":0-200