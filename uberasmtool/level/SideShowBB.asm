;Sideshow Showdown ASM (level BC).
;Includes:
;load: force screen upwards (it glitches randomly somehow so this attempts to fix)
;init: init race
;main: handle race

load:
	LDA #$38
	STA $1C
	STA $1464
	RTL

init:
	LDA $7F9A83
	ORA #$01
	STA $7F9A83
	LDA #$FF
	STA $1DEF		;freeram used for start of race timer
RTL				;return.

main:
	LDA $9D
	ORA $13D4
	BEQ +
	RTL

+
	LDA $1DEF	; freeram used for start of race timer
	CMP #$01
	BEQ StartBB
	CMP #$02
	BEQ StartSoundBB
	CMP #$03
	BEQ EndDrumBB
	CMP #$A0
	BEQ ReadySoundBB
	BRA DecTimeBB
StartSoundBB:
	LDA #$29
	STA $1DFC
	LDA #$40
	STA $7B
	BRA DecTimeBB
EndDrumBB:
	LDA #$12
	STA $1DFC
	BRA DecTimeBB
ReadySoundBB:
	LDA #$11
	STA $1DFC
DecTimeBB:
	DEC $1DEF
StartBB:

	LDA $13D4
	BNE DoneBB
	LDA $7B
	BMI DoneBB
	LDA $13C8
	BNE DecRAMBB
	LDA $16
	AND #$01
	BNE MoveBB
	RTL
DecRAMBB:
	DEC $13C8
	LDA $16
	AND #$01
	BNE BoostBB
	RTL
BoostBB:
	LDA $7B
	CLC
	ADC #$18
	CLC
	CMP #$7F
	BCC NoSlowBB
	LDA #$7F
NoSlowBB:
	STA $7B
MoveBB:
	LDA #$0A
	STA $13C8

DoneBB:
	RTL