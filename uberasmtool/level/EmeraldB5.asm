;Emerald Escapade ASM (level B5).
;Includes:
;init: message box text DMA
;main: violent quicksand handling, shake layer 1/LOWER CEILING NOW!

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return.

main:
JSL quicksink_run		;run violent quicksand code

LDA $9D
ORA $13D4|!addr
BNE +

LDA $14
AND #$0F
BNE +

REP #$20
LDA $1468|!addr
BEQ .stoplowering
DEC $1468|!addr
SEP #$20

LDA #$18
STA $1887|!addr

+
.stoplowering
SEP #$20
RTL

MsgDMA1:
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$0E,$21,$4E,$21,$0E,$21,$4E,$21,$0E,$21
db $4E,$21,$0E,$21,$4E,$21,$0E,$21,$4E,$21,$0E,$21,$4E,$21,$0E,$21
db $4E,$21,$0E,$21,$4E,$21,$4E,$21,$4E,$21,$4E,$21,$4E,$21,$4E,$21
db $4E,$21,$4E,$21,$1B,$21,$1B,$21,$1B,$21,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$13,$21,$47,$21,$40,$21,$4D,$21,$4A,$21,$FD,$20,$58,$21
db $4E,$21,$54,$21,$1D,$21,$FD,$20,$0C,$21,$40,$21,$51,$21,$48,$21
db $4E,$21,$1D,$21,$FD,$20,$45,$21,$4E,$21,$51,$21,$FD,$20,$47,$21
db $48,$21,$53,$21,$53,$21,$48,$21,$4D,$21,$46,$21,$FD,$20,$FD,$20
db $FD,$20,$53,$21,$47,$21,$44,$21,$FD,$20,$52,$21,$56,$21,$48,$21
db $53,$21,$42,$21,$47,$21,$1B,$21,$FD,$20,$0D,$21,$4E,$21,$56,$21
db $FD,$20,$58,$21,$4E,$21,$54,$21,$FD,$20,$56,$21,$48,$21,$4B,$21
db $4B,$21,$FD,$20,$4D,$21,$44,$21,$55,$21,$44,$21,$51,$21,$FD,$20
db $FD,$20,$4C,$21,$40,$21,$4A,$21,$44,$21,$FD,$20,$48,$21,$53,$21
db $FD,$20,$4E,$21,$54,$21,$53,$21,$FD,$20,$40,$21,$4B,$21,$48,$21
db $55,$21,$44,$21,$1B,$21,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$0B,$21
db $0E,$21,$16,$21,$04,$21,$11,$21,$FD,$20,$02,$21,$04,$21,$08,$21
db $0B,$21,$08,$21,$0D,$21,$06,$21,$FD,$20,$0D,$21,$0E,$21,$16,$21
db $1A,$21,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20
db $FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20,$FD,$20