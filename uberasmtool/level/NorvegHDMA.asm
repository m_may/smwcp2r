;Norveg (141) ASM.
;Includes
;init: messages 1-2 DMA, initialize death timer and layer 2 scroll HDMA table to be used by Norveg sprite
;nmi: messages 3-4 DMA
;main: check if Norveg is dead

	;these must be the same as in pixi/sprites/norveg.asm
		!TimerForVictoryAfterBossDeath = $0F5E|!addr
		!UploadMessages34Flag = $0F5F|!addr


		!HDMA_table_size	= $0080		; size of HDMA table

		; these overwrite unused parts of the level data
	if !sa1 = 1
		!SprSize = $16
		!BG2X			= $40D580
		!BG2Y			= $40D582
		!BG2dir			= $40D584
		!HDMA_table		= $40D586
	else
		!SprSize = $0C
		!BG2X			= $7ED580
		!BG2Y			= $7ED582
		!BG2dir			= $7ED584
		!HDMA_table		= $7ED586
	endif

		!HDMA			= $0D9F|!addr


init:
		STZ !TimerForVictoryAfterBossDeath
		STZ !UploadMessages34Flag

		LDA #$17		; BG1 + BG2 + BG3 + sprites on main
		STA $212C
		STA $212E				
		STZ $212D	; nothing on sub
		STZ $212F

		REP #$20
		LDX #$02
		LDA.w #Messages_Norveg_1
		JSL Messages_DMA
		LDX #$06
		LDA.w #Messages_Norveg_2
		JSL Messages_DMA

		LDA #$0001
		STA !HDMA_table+0 : STA !HDMA_table+!HDMA_table_size+0
		LDA #$0100
		STA !HDMA_table+1 : STA !HDMA_table+!HDMA_table_size+1
		LDA #$0000
		STA !HDMA_table+5 : STA !HDMA_table+!HDMA_table_size+5

		LDA #$0F03 : STA $4340
		LDA.w #!HDMA_table
		STA $4342
		STA $1FFE
		LDY.b #!HDMA_table>>16 : STY $4344
		LDY #$10 : STY !HDMA
		JML NexusHDMA_init

nmi:
LDA !UploadMessages34Flag
BEQ .return
REP #$20
LDX #$02
LDA.w #Messages_Norveg_3
JSL Messages_DMA
LDX #$06
LDA.w #Messages_Norveg_4
JSL Messages_DMA
SEP #$20
STZ !UploadMessages34Flag
.return
RTL

main:
LDA !TimerForVictoryAfterBossDeath
BEQ .return
DEC
STA !TimerForVictoryAfterBossDeath
BNE .return
	LDA #$0B
	STA $1696|!addr ;uberasm victory flag by blind devil
.return
RTL
