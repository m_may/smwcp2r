;FTS ASM (levels 85 and 8C).
;Includes:
;load/init: disable vertical scroll, wave HDMA init and processing (layer 3 for 85 and layer 2 for 8C), preserve powerup
;main: violent quicksand handling for level 85, no pause/movements allowed, hide HUD, warp when dying
;on level 85, teleport when Deedler is killed on 8C, wave HDMA processing, invincible mario

!preservedPowerup = $0F5E|!addr

load:
init:
REP #$20
LDA $010B|!addr
CMP #$008C
BEQ +

LDX $19
STX !preservedPowerup

LDX #$00
STX $1411|!addr

LDA #$0088
STA $1462|!addr
STA $1466|!addr
STA $1A
STA $1E

LDA #$1102
BRA ++

+
LDA #$0F02

++
STA $4330		;Use Mode 02 on register 2111 (or 210F)
   LDA.w #!hdmatable    ; | Address of HDMA table
   STA $4332            ; | 4332 = Low-Byte of table, 4333 = High-Byte of table
   SEP #$20             ; | 
   LDA.b #!hdmatable>>16 ; | Address of HDMA table, get bank byte
   STA $4334            ; | 4334 = Bank-Byte of table
   LDA #$08             ; | 
   TSB $0D9F|!addr      ; | Enable HDMA channel 3

BRA justdohdmadamn

main:
LDA #$03
STA $1497|!addr			;invincible mario because bob-ombs can sometimes be so evil and kill him earlier than expected

LDA #$02
STA $13D3|!addr			;no pausing... because the hud displays regardless of forcing not to display so NO PAUSING ALTOGETHER, FUCK IT

REP #$20
STZ $0F00|!addr			;clear all status bar counter display bitflags (so it never shows up)
STZ $0F02|!addr			;clear all status bar timer display bitflags
SEP #$20

LDA $010B|!addr
CMP #$8C
BNE .not8C

LDX #$0B
-
LDA $14C8,x
CMP #$08
BEQ .stillalive
DEX
BPL -

BRA WARP

.stillalive
BRA justdohdmadamn

.not8C
LDA $71
CMP #$09
BNE ++

REP #$20
LDA #$0770
STA $96
SEP #$20

+
LDA $1496|!addr
CMP #$02
BCS +

WARP:
LDA #$06
STA $71

LDA #$00
STA $89
STA $88
STA !hdmatable
STA $1DFB|!addr
STA $0DDA|!addr
STA $1496|!addr

LDA !preservedPowerup
STA $19
+
RTL

++
JSL quicksink_run		;run violent quicksand code

STZ $15
STZ $16
STZ $17
STZ $18

justdohdmadamn:
REP #$10
LDX #$0000
TXY

.Loop
LDA #$02
STA !hdmatable,x		;scanline count, always 2

REP #$20		;16-bit AXY
TYA
CLC
ADC $14
AND #$001F
PHY			;preserve entry loop count
TAY			;get wavetable index into Y now

LDA WaveTable,y
AND #$00FF
CLC
ADC $22
STA !hdmatable+1,x
SEP #$20		;8-bit A, 16-bit XY
INX #3			;increment X three times (we've got 3 bytes per entry - scanline count, X-pos low and X-pos high)
PLY			;restore entry loop count
INY			;increment by one
CPY #$0070		;compare to value
BNE .Loop		;loop if not equal.

LDA #$00
STA !hdmatable+3,x	;terminate table

rep #$20
txa
sta $7f8888
sep #$20

SEP #$10		;8-bit XY
RTL

WaveTable:
db $00,$01,$02,$03,$04,$05,$06,$07
db $08,$09,$0A,$0B,$0C,$0D,$0E,$0F
db $0F,$0E,$0D,$0C,$0B,$0A,$09,$08
db $07,$06,$05,$04,$03,$02,$01,$00