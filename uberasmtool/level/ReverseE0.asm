;Reverse Universe ASM (levels E0 and E1).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;main: inverted D-pad

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

main:
LDA $15				;load controller data 1
AND #$03			;preserve left/right D-pad bits
BEQ .DontInvertHorz		;if none are pressed, skip ahead.
LDA $15				;load controller data 1 again
EOR #$03			;flip bits
STA $15				;store back.

.DontInvertHorz
LDA $15				;load controller data 1
AND #$0C			;preserve up/down D-pad bits
BEQ .DontInvertVert		;if none are pressed, return.
LDA $15				;load controller data 1 again
EOR #$0C			;flip bits
STA $15				;store back.

.DontInvertVert
RTL

Table1:
db $05,$20,$43
db $03,$20,$44
db $05,$21,$44
db $07,$21,$45
db $01,$21,$46
db $05,$22,$46
db $05,$22,$47
db $01,$23,$47
db $05,$23,$48
db $03,$23,$49
db $01,$24,$49
db $0B,$24,$4A
db $06,$25,$4A
db $11,$25,$4B
db $16,$26,$4B
db $07,$26,$4C
db $13,$27,$4C
db $04,$28,$4C
db $07,$28,$4D
db $0B,$29,$4D
db $04,$2A,$4D
db $06,$2A,$4E
db $0B,$2B,$4E
db $04,$2C,$4E
db $06,$2C,$4F
db $0B,$2D,$4F
db $04,$2E,$4F
db $06,$2E,$50
db $0B,$2F,$50
db $04,$30,$50
db $06,$30,$51
db $06,$31,$51
db $00

Table2:
db $14,$89
db $1B,$8A
db $06,$89
db $07,$88
db $07,$87
db $09,$86
db $0A,$85
db $0B,$84
db $0D,$83
db $20,$82
db $36,$81
db $1C,$80
db $00