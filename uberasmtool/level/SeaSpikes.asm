;Seaside Spikes ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 handler, 2-channel HDMA gradient, wave HDMA init and processing, layer 2 "water tide" interaction
;main: wave HDMA processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15

JSL InvisiL2_Water		;make part of layer 2 BG act like water as if there was a layer 3 tide, but without current

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

Table1:
db $0C,$22,$42
db $06,$22,$43
db $0D,$23,$43
db $0A,$23,$44
db $09,$24,$44
db $0D,$24,$45
db $06,$25,$45
db $10,$25,$46
db $03,$26,$46
db $13,$26,$47
db $01,$26,$48
db $12,$27,$48
db $04,$27,$49
db $0F,$28,$49
db $08,$28,$4A
db $0B,$29,$4A
db $0B,$29,$4B
db $08,$2A,$4B
db $0E,$2A,$4C
db $05,$2B,$4C
db $12,$2B,$4D
db $01,$2C,$4D
db $03,$2C,$4E
db $00

Table2:
db $05,$82
db $12,$83
db $12,$84
db $13,$85
db $12,$86
db $12,$87
db $12,$88
db $12,$89
db $12,$8A
db $12,$8B
db $12,$8C
db $12,$8D
db $12,$8E
db $02,$8F
db $00

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond