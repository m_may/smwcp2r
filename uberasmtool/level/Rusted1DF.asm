;Rusted Retribution ASM (level 1DF).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Pal6
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_Rusted1DF
JSL Messages_DMA
SEP #$20
RTL
