;Crumbling Catacombs ASM (level 8A).
;Includes:
;main: map16 specific shattering effects

main:
	LDA $71
	ORA $9D
	BNE ReturnReturn8A
	LDA $13D4
	BEQ RunCode_8A
	ReturnReturn8A:
	RTL

	RunCode_8A:
	JSL Shatter101_Mid88
	LDA $18C5
	CMP #$01
	BEQ Frame2_8A
	CMP #$02
	BEQ Frame3_8A
	CMP #$03
	BEQ Return8A
	LDA $D2
	CMP #$0A
	BNE Return8A
	REP #$20
	LDA $D3
	CMP #$0160
	BNE Return8A
	LDA #$0130
	STA $98
	LDA #$0A20
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	LDA #$09
	STA $1DFC
	LDA #$14
	STA $1887
	Loop1_8A:
	JSL $80BEB0
	LDA $98
	CLC
	ADC #$10
	STA $98
	CMP #$40
	BNE ShatterSkip1_8A
	JSL Shatter101_main
	ShatterSkip1_8A:
	CMP #$60
	BNE Loop1_8A
	LDA #$01
	STA $18C5
	RTL
	Frame2_8A:
	REP #$20
	LDA #$00C0
	STA $98
	LDA #$0A30
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	Loop2_8A:
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$40
	BNE ShatterSkip2_8A
	JSL Shatter101_main
	ShatterSkip2_8A:
	CMP #$60
	BNE Loop2_8A
	LDA #$02
	STA $18C5
	RTL
	Return8A:
	SEP #$20
	RTL
	Frame3_8A:
	REP #$20
	LDA #$0070
	STA $98
	LDA #$0A30
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	Loop3_8A:
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$60
	BNE Loop3_8A
	LDA #$03
	STA $18C5
	RTL