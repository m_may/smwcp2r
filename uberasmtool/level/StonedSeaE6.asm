;Stone Sea Sanctuary ASM (level E6).
;Includes:
;init: 2-channel HDMA gradient

init:
LDA.b #StoneSeaGeneric_Table1>>16	;load bank of one of the tables
STA $02					;store to scratch RAM.
STA $05					;store to scratch RAM.
REP #$20				;16-bit A
LDA.w #StoneSeaGeneric_Table1		;load address of first table
STA $00					;store to scratch RAM.
LDA.w #StoneSeaGeneric_Table2		;load address of second table
STA $03					;store to scratch RAM.
JML HDMA_Color2				;execute HDMA (A goes 8-bit after this routine) and return.