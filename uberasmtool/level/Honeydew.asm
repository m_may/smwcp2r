;levels 138 and 1C9
;load: SMWC coin trigger initializer
;init: 3-channel HDMA gradient, layer 2 parallax HDMA and table init
;nmi: layer 2 parallax HDMA table updates
;main: perfect slippery blocks handler

!hdmaparallaxtbl_begin = $0F5E|!addr  ;address of the beginning of the table

!hdmaparallax_begin_scanlines = $70

load:
JML Load_CDM16intoCT

init:
LDA.b #BlueTable>>16
STA $02
STA $05
REP #$20
LDA.w #RedGreenTable
STA $00
LDA.w #BlueTable
STA $03
JSL HDMA_Color2

LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
STA !hdmaparallaxtbls+15
STA !hdmaparallaxtbls+18
LDA #$18
STA !hdmaparallaxtbls+21
STA !hdmaparallaxtbls+24

REP #$20
LDA #$0F02  ;mode 2 on $210F (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls  ;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16  ;bank of table
STA $4354
LDA #$20         ;\ Enable HDMA
TSB $0D9F|!addr  ;/ on channel 6
RTL


nmi:
;calculate the amount of scanlines in the first entry and whether it's over #$7F
LDA $1468|!addr  ;layer 2 y position, next frame
EOR #$FF
CLC : ADC #$20
CMP #!hdmaparallax_begin_scanlines+1
BCS .begin0

.begin1
STA !hdmaparallaxtbls+3
REP #$20
LDA.w #!hdmaparallaxtbls+3
STA !hdmaparallaxtbl_begin
BRA .nextlines

.begin0
SEC : SBC #!hdmaparallax_begin_scanlines
STA !hdmaparallaxtbls
LDA #!hdmaparallax_begin_scanlines
STA !hdmaparallaxtbls+3
REP #$20
LDA.w #!hdmaparallaxtbls
STA !hdmaparallaxtbl_begin

.nextlines
LDA $1462|!addr  ;layer 1 x position, next frame
LSR
STA !hdmaparallaxtbls+10  ;8/16
LSR 
STA !hdmaparallaxtbls+1  ;4/16
LSR
CLC : ADC $1462|!addr
LSR
STA !hdmaparallaxtbls+13  ;9/16

LDA $1462|!addr
CLC : ADC !hdmaparallaxtbls+10
LSR
STA !hdmaparallaxtbls+22  ;12/16
LSR
STA !hdmaparallaxtbls+7  ;6/16
CLC : ADC $1462|!addr
LSR
STA !hdmaparallaxtbls+19  ;11/16

LDA $1462|!addr
CLC : ADC !hdmaparallaxtbls+1
LSR
STA !hdmaparallaxtbls+16  ;10/16
LSR
STA !hdmaparallaxtbls+4  ;5/16

LDA $1462|!addr
CLC : ADC !hdmaparallaxtbls+22
LSR
STA !hdmaparallaxtbls+25 

SEP #$20

LDA #$00
STA !hdmaparallaxtbls+27

LDA !hdmaparallaxtbl_begin  ;address of table
STA $4352

RTL


RedGreenTable:
db $01,$3E,$57
db $04,$3E,$58
db $04,$3E,$59
db $03,$3E,$5A
db $03,$3E,$5B
db $04,$3E,$5C
db $04,$3E,$5D
db $04,$3E,$5E
db $09,$3E,$5F
db $08,$3D,$5F
db $05,$3C,$5F
db $08,$3B,$5F
db $32,$3A,$5F
db $05,$3A,$5E
db $02,$3A,$5D
db $03,$39,$5D
db $04,$39,$5C
db $04,$39,$5B
db $04,$39,$5A
db $06,$39,$59
db $0C,$39,$58
db $05,$3A,$58
db $04,$3B,$58
db $06,$3C,$58
db $05,$3D,$58
db $07,$3E,$58
db $19,$3F,$58
db $01,$3F,$57
db $00

BlueTable:
db $25,$96
db $0B,$97
db $07,$98
db $09,$99
db $08,$9A
db $06,$9B
db $05,$9C
db $08,$9D
db $08,$9E
db $50,$9F
db $05,$9E
db $03,$9D
db $04,$9C
db $03,$9B
db $04,$9A
db $04,$99
db $05,$98
db $11,$97
db $00

main:
JML SlipperyBlocks_main		;call slippery blocks handling routine and return.