;Balloon Bonanza ASM (levels 113, E7 and E8).
;Includes:
;load: SMWC coin custom trigger init
;init/nmi: 2-channel HDMA gradient (different one for level E8), layer 2 parallax setup and processing
;nmi: parallax processing


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.

LDA $010B|!addr			;load level number, low byte
CMP #$E8			;compare to value
BNE .dusk			;if not equal, use brighter gradient variation.


REP #$20			;16-bit A
LDA.w #Table3			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table4			;load address of second table
BRA +				;branch ahead

.dusk
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table

+
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

JSL TerrifyingParallax_init


REP #$20
LDA #$0F02			;mode 2 on $210F (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
JML TerrifyingParallax_nmi

Table1:
db $0B,$2A,$45
db $04,$2B,$45
db $01,$2B,$46
db $04,$2C,$46
db $03,$2D,$46
db $03,$2E,$46
db $02,$2F,$46
db $02,$30,$46
db $03,$31,$46
db $01,$31,$47
db $04,$32,$47
db $04,$33,$47
db $04,$34,$47
db $03,$35,$47
db $01,$35,$48
db $04,$36,$48
db $05,$37,$48
db $04,$38,$48
db $03,$39,$48
db $02,$39,$49
db $01,$39,$4A
db $02,$3A,$4A
db $02,$3A,$4B
db $03,$3A,$4C
db $01,$3B,$4C
db $04,$3B,$4D
db $0C,$3B,$4E
db $04,$3B,$4F
db $03,$3B,$50
db $03,$3B,$51
db $02,$3B,$52
db $03,$3B,$53
db $02,$3B,$54
db $03,$3B,$55
db $02,$3B,$56
db $04,$3B,$57
db $02,$3B,$58
db $05,$3C,$58
db $62,$3C,$59
db $00

Table2:
db $0B,$8D
db $08,$8E
db $06,$8F
db $05,$90
db $02,$91
db $09,$90
db $04,$8F
db $04,$8E
db $04,$8D
db $03,$8C
db $03,$8B
db $03,$8A
db $03,$89
db $06,$88
db $1E,$89
db $08,$8A
db $09,$8B
db $6A,$8C
db $00

Table3:
db $08,$24,$43
db $06,$24,$44
db $0E,$25,$44
db $0A,$26,$44
db $03,$27,$44
db $05,$27,$45
db $06,$28,$45
db $05,$29,$45
db $05,$2A,$45
db $04,$2B,$45
db $01,$2B,$46
db $03,$2C,$46
db $04,$2D,$46
db $02,$2E,$46
db $03,$2F,$46
db $03,$30,$46
db $04,$31,$46
db $05,$32,$46
db $02,$33,$46
db $03,$33,$47
db $05,$34,$47
db $05,$35,$47
db $05,$36,$47
db $06,$37,$47
db $06,$38,$47
db $09,$38,$48
db $5C,$39,$48
db $00

Table4:
db $11,$89
db $10,$8A
db $0C,$8B
db $09,$8C
db $08,$8D
db $07,$8E
db $06,$8F
db $06,$90
db $06,$91
db $09,$90
db $06,$8F
db $05,$8E
db $05,$8D
db $04,$8C
db $04,$8B
db $07,$8A
db $61,$89
db $00