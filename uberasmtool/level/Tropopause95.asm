;Tropopause Trail ASM (level 95).
;Includes:
;load: SMWC coin custom trigger init, parallax HDMA init
;init: 2-channel HDMA gradient, parallax HDMA processing
;nmi: parallax HDMA processing


load:
JSL Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

LDA #$77			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$1E
STA !hdmaparallaxtbls+3
LDA #$18
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
LDA #$00
STA !hdmaparallaxtbls+12
STA !hdmaparallaxtbls+1		;zero X-pos low byte - for the sun/aurora
STA !hdmaparallaxtbls+2		;zero X-pos high byte - for the sun/aurora
BRA instantsetupcoords		;set up the parallax coordinates before processing the actual hdma

init:
REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;Enable HDMA on channel 5
TSB $0D9F|!addr

LDA $1B				;load layer 1 X-pos current frame, high byte
BNE +				;if not zero, skip ahead.
LDA $1A				;load layer 1 X-pos current frame, low byte
BNE +				;if not zero, skip ahead.

STZ $1411|!addr			;disable horizontal scroll.
STZ $1412|!addr			;disable vertical scroll.
+

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

nmi:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$03
ORA $9D
ORA $13D4|!addr
BNE +

instantsetupcoords:
REP #$20
LDA !hdmaparallaxtbls+10	;the clouds that should move faster - the bottom ones
INC
STA !hdmaparallaxtbls+10
LSR
STA !hdmaparallaxtbls+4		;the clouds that should move slower - the top ones
CLC
ADC !hdmaparallaxtbls+10
LSR
STA !hdmaparallaxtbls+7		;the clouds that should move with intermediate speed - the middle ones
SEP #$20

+
RTL				;return.

Table1:
db $05,$32,$42
db $01,$33,$42
db $06,$33,$43
db $05,$33,$44
db $06,$33,$45
db $06,$34,$46
db $06,$34,$47
db $05,$34,$48
db $01,$34,$49
db $05,$35,$49
db $06,$35,$4A
db $05,$35,$4B
db $01,$35,$4C
db $05,$36,$4C
db $06,$36,$4D
db $05,$36,$4E
db $02,$36,$4F
db $04,$37,$4F
db $06,$37,$50
db $06,$37,$51
db $02,$37,$52
db $03,$38,$52
db $06,$38,$53
db $06,$38,$54
db $03,$38,$55
db $02,$39,$55
db $04,$39,$56
db $05,$38,$55
db $01,$38,$54
db $04,$37,$54
db $01,$37,$53
db $04,$36,$53
db $02,$36,$52
db $03,$35,$52
db $02,$35,$51
db $03,$34,$51
db $03,$34,$50
db $02,$33,$50
db $03,$33,$4F
db $02,$32,$4F
db $04,$32,$4E
db $01,$31,$4E
db $05,$31,$4D
db $05,$30,$4C
db $05,$2F,$4B
db $01,$2F,$4A
db $04,$2E,$4A
db $01,$2E,$49
db $04,$2D,$49
db $02,$2D,$48
db $03,$2C,$48
db $02,$2C,$47
db $03,$2B,$47
db $03,$2B,$46
db $02,$2A,$46
db $04,$2A,$45
db $01,$29,$45
db $04,$29,$44
db $01,$28,$44
db $05,$28,$43
db $05,$27,$42
db $05,$26,$41
db $01,$26,$40
db $00

Table2:
db $80,$9E
db $02,$9E
db $5C,$9D
db $02,$9C
db $00