;Stratus Skyworks ASM.
;Includes:
;load: SMWC coin trigger initializer
;init: message box text DMA, 2-channel HDMA gradient
;main: spawn cluster rain, layer 2 autoscroll slow

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $1413|!addr			;disable layer 2 H-scroll.

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA
REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

main:
LDA #$10			;load sprite number to spawn
JSL clusterspawn_run		;spawn cluster raindropdries

LDA $14
AND #$03
ORA $71
ORA $9D
ORA $13D4
BNE ret

REP #$20
INC $1466|!addr
SEP #$20

ret:
RTL

Table1:
db $06,$28,$48
db $0D,$28,$49
db $0E,$29,$49
db $0D,$29,$4A
db $0E,$2A,$4A
db $0E,$2A,$4B
db $0E,$2B,$4B
db $0F,$2B,$4C
db $0E,$2C,$4C
db $0E,$2C,$4D
db $0F,$2D,$4D
db $0E,$2D,$4E
db $0E,$2E,$4E
db $09,$2E,$4F
db $0F,$2F,$4F
db $07,$2F,$51
db $11,$31,$51
db $02,$31,$52
db $00

Table2:
db $16,$8A
db $15,$8B
db $15,$8C
db $14,$8D
db $15,$8E
db $14,$8F
db $14,$91
db $15,$92
db $19,$93
db $1A,$94
db $07,$95
db $00

MsgDMA1:
incbin "msgbin/11F-1msg.bin"

MsgDMA2:
incbin "msgbin/11F-2msg.bin"