;Smoldering Shrine ASM (levels 128, 157, 159 and 15A).
;Includes:
;load: SMWC coin custom trigger init
;init/nmi: 2-channel HDMA gradient, layer 3 parallax init and processing
;nmi: layer 3 parallax processing
;main: incrementing autoscroll indexes

!scansub = $0F5E|!addr
!autoscroll_index1 = $0F60|!addr
!autoscroll_index2 = $0F62|!addr
!autoscroll_index3 = $0F64|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL

main:
LDA $71
ORA $9D
ORA $13D4
BNE ++

LDA $14
AND #$07
BNE +

REP #$20
INC !autoscroll_index1
SEP #$20

+
LDA $14
AND #$0F
CMP #$07
BNE +

REP #$20
INC !autoscroll_index2
SEP #$20

+
LDA $14
AND #$1F
BNE ++

REP #$20
INC !autoscroll_index3
SEP #$20

++
RTL

nmi:
LDA $24
SEC
SBC #$C0
STA !scansub		;amount of scanlines to subtract from first entry

LDA #$7F
SEC
SBC !scansub
STA !hdmaparallaxtbls
LDA #$0F
STA !hdmaparallaxtbls+3
LDA #$10
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+12
STA !hdmaparallaxtbls+15
LDA #$08
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+18

REP #$20
LDA $1466|!addr
LSR
STA $00
LSR
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4
LDA $00
CLC
ADC !autoscroll_index3
STA !hdmaparallaxtbls+7
LDA $00
CLC
ADC !autoscroll_index2
STA !hdmaparallaxtbls+10
LDA !autoscroll_index3
LSR
CLC
ADC !autoscroll_index2
ADC $00
STA !hdmaparallaxtbls+13
LDA $00
CLC
ADC !autoscroll_index1
STA !hdmaparallaxtbls+16
LDA !autoscroll_index2
LSR
CLC
ADC $00
ADC !autoscroll_index1
STA !hdmaparallaxtbls+19
SEP #$20

LDA #$00
STA !hdmaparallaxtbls+21
RTL

Table1:
db $1E,$2B,$44
db $07,$2C,$44
db $01,$2C,$45
db $07,$2D,$45
db $07,$2E,$45
db $05,$2F,$45
db $02,$2F,$46
db $08,$30,$46
db $07,$31,$46
db $03,$32,$46
db $04,$32,$47
db $07,$33,$47
db $08,$34,$47
db $01,$35,$47
db $06,$35,$48
db $07,$36,$48
db $07,$37,$48
db $08,$38,$49
db $07,$39,$49
db $06,$3A,$49
db $56,$3A,$4A
db $00


Table2:
db $23,$88
db $23,$87
db $24,$86
db $76,$85
db $00


