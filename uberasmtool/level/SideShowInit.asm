;Sideshow Showdown ASM (levels 111, C2 and C4).
;Includes:
;init: initialize game RAM addresses for level 111, 2-channel HDMA gradient, store gradient table to RAM
;init/nmi: scroll gradient with layer 3

init:
LDA $010B|!addr
CMP #$11
BNE noinitram

STZ $19

LDA #$00

LDX #$14
-
STA $7F9A81,x
DEX
BPL -

LDA #25
STA $7F9A70

noinitram:
JSL SideshowHDMA_init

nmi:
JML SideshowHDMA_nmi