;Fantastic Elastic ASM (level DD).
;Includes:
;init: set up all mode 7, 2-channel HDMA gradient
;main: handle mode 7 rotation and HDMA positioning

!ExGFXFile = $2DD

!Rotation = $0DC3|!addr
!Table1RAM = !hdmatable
!Table2RAM = !hdmatable+$80

; when changing these addresses remember to remove JSL $04DAAD
!GFXBuffer = $7F6000 ;0x2000 bytes
!GFXBuffer2 = $7F4000 ;0x4000 bytes

FillTile:
db $00
FerrisTilemap:
incbin "ferris-comp-tilemap.bin"

init:
	; Fill tilemap with blank tile.
	REP #$20
	LDX #$00            ; \ Increase on $2118 write.
	STX $2115           ; /
	STZ $2116           ; Set where to write in VRAM - address $0000 for mode 7 tilemap
	LDX #$08            ;\
	STX $4300           ;/ Set mode to 1 reg write once, fixed A-bus address.
	LDX #$18            ;\ 
	STX $4301           ;/ Writing to $2118.
	LDA.w #FillTile       ;\  Adress where our data is.
	STA $4302                  ; | 
	LDX.b #FillTile>>16   ; | Bank where our data is.
	STX $4304                  ;/
	LDA #$4000          ;\ How many bytes to transfer.
	STA $4305           ;/
	LDX #$01            ;\ Start DMA transfer on channel 0.
	STX $420B           ;/

	; Transfer tilemap into VRAM.
	LDX #$00            ;\
	STX $4300           ;/ Set mode to 1 reg write once, increment A-bus address.
	LDA.w #FerrisTilemap       ;\  Adress where our data is.
	STA $4302                  ; | 
	;LDX.b #FerrisTilemap>>16   ; | Bank where our data is.
	;STX $4304                  ;/
	STZ $00
.m7tilemapuploadloop
	LDA $00			; \ Set where to write in VRAM...
	STA $2116		; / ...row by row.
	LDA #$0010          ;\ Size of our data.
	STA $4305           ;/
	LDX #$01            ;\ Start DMA transfer on channel 0.
	STX $420B           ;/
	LDA $00
	CLC : ADC #$0080
	STA $00
	CMP #$0800
	BCC .m7tilemapuploadloop
	SEP #$20

	; Decompress ExGFX
	LDA.b #!GFXBuffer>>16
	STA $02
	REP #$20
	LDA.w #!GFXBuffer
	STA $00
	LDA.w #!ExGFXFile
	JSL $0FF900|!bank   ; Decompress ExGFX routine
	SEP #$20

	; Transfer the gfx into VRAM.
	PHK
	LDA.b #!GFXBuffer2>>16
	PHA
	PLB
	REP #$10
	LDX #$0000
	LDY #$0000
.m7copygfxloop
	LDA !GFXBuffer,x
	STA $00
	AND #$0F
	STA.w !GFXBuffer2,y
	INY
	LDA $00
	LSR #4
	STA.w !GFXBuffer2,y
	INY
	INX
	CPX #$2000
	BNE .m7copygfxloop
	SEP #$10
	PLB

	LDA #$80            ; \ Increase on $2119 write.
	STA $2115           ; /
	STZ $4300           ; Set mode to 1 reg write once.
	LDA #$19            ;\ 
	STA $4301           ;/ Writing to $2119.
	REP #$20
	LDA.w #!GFXBuffer2       ;\  Adress where our data is.
	STA $4302                ; | 
	LDX.b #!GFXBuffer2>>16   ; | Bank where our data is.
	STX $4304                ;/
	STZ $2116           ; Set where to write in VRAM - has to be $0000
	LDA #$4000          ;\ Size of our data.
	STA $4305           ;/
	LDX #$01            ;\ Start DMA transfer on channel 0.
	STX $420B           ;/
	SEP #$20

	JSL $04DAAD         ; Reload overworld layer 2 data, because we used the RAM addresses to decompress the GFX

	LDA #$07			;\ Set BG Mode to 7.
	STA $3E				;/

	stz $211a
	REP #$20
	LDA #$0080 : STA $2A	; \ Set effect center
	LDA #$0080 : STA $2C	; /
	LDA #$0100
	STA $36			; Set angle
	LDA #$0C0C : STA $38	; Set size
	LDA #$0008 : STA $3A	; \ Set layer position
	LDA #$FF98 : STA $3C	; /
	SEP #$20

	LDX.b #T1End-Table1-1

.loop1
	LDA Table1,x
	STA !Table1RAM,x
	DEX
	CPX #$FF
	BNE .loop1

	LDX.b #T2End-Table2-2

.loop2
	LDA Table2,x
	STA !Table2RAM,x
	DEX
	CPX #$FF
	BNE .loop2


LDA.b #!Table1RAM>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #!Table1RAM		;load address of first table
STA $00				;store to scratch RAM.
LDA.w #!Table2RAM		;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
RTL

main:
LDA $71
CMP #$09
BEQ ++

LDA $77
AND #$04
BEQ +

REP #$20
LDA $96
CMP #$01F3
BCC +
CMP #$0338
BCS +

SEP #$20
LDA #$80
STA $1404	;force scroll at player

+
REP #$20
LDA $96
CMP #$01F2
BCS +++

++
SEP #$20
STZ $1412	;no vertical scroll
REP #$20

+++
LDA $1C
LSR #3
STA $00
SEP #$20

LDA #$5E
SEC
SBC $00
CLC
ADC #$09
STA !Table1RAM
STA !Table2RAM

	LDA $13D4
	ORA $9D
	BNE nope

	stz $149f
	REP #$20
	LDA #$FFC0
	STA $2A
	LDA #$FFC0
	STA $2C
	SEP #$20
	REP #$20
	LDA !Rotation
	STA $36
	SEP #$20
nope:
	RTL

Table1:
db $09,$20,$40
db $1C,$20,$40
db $01,$20,$41
db $35,$21,$41
db $03,$21,$42
db $1D,$22,$42
db $03,$23,$42
db $03,$24,$42
db $02,$25,$42
db $02,$26,$42
db $07,$27,$42
db $07,$28,$42
db $01,$28,$41
db $09,$29,$41
db $06,$2A,$41
db $01,$2B,$41
db $01,$2B,$42
db $03,$2C,$42
db $02,$2D,$43
db $02,$2E,$43
db $01,$2E,$44
db $01,$32,$4C
db $01,$32,$54
db $01,$2A,$4B
db $01,$24,$43
db $02,$24,$44
db $04,$25,$44
db $02,$25,$45
db $04,$26,$45
db $01,$26,$46
db $04,$27,$46
db $01,$27,$47
db $05,$28,$47
db $06,$29,$48
db $05,$2A,$49
db $01,$2B,$49
db $04,$2B,$4A
db $02,$2C,$4A
db $03,$2C,$4B
db $03,$2D,$4B
db $09,$2D,$4C
db $00
T1End:

Table2:
db $09,$80
db $09,$80
db $12,$81
db $12,$82
db $12,$83
db $12,$84
db $12,$85
db $16,$86
db $04,$87
db $03,$86
db $04,$85
db $05,$84
db $05,$83
db $06,$82
db $05,$81
db $02,$82
db $01,$83
db $02,$84
db $01,$85
db $02,$86
db $01,$87
db $02,$88
db $01,$93
db $01,$9B
db $01,$94
db $04,$8C
db $08,$8D
db $07,$8E
db $07,$8F
db $08,$91
db $07,$92
db $07,$93
db $09,$94
db $00
T2End: