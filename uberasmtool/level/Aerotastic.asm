;Aerotastic Assault ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: initialize Jeptak generator timer, message box text DMA, 2-channel HDMA gradient
;main: Jeptak generator

!JeptakNum = $31	;sprite number for the damn pigeon

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Pal4
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_Aerotastic31
JSL Messages_DMA
SEP #$20

STZ $0F5E|!addr		;timer - must be the same as the one in jeptak code

ret:
RTL

main:
LDA $9D
ORA $13D4|!addr
BNE ret

LDA $0F5E|!addr
BEQ ret
CMP #$01
BNE normdecnospawn

;spawn sprite
	LDX #$0B
	-
		LDA $14C8,x
		BEQ +
			DEX
		BPL -
		SEC
		BRA .no_slot
	+
	LDA #!JeptakNum
	STA $9E,x
	JSL $07F7D2|!bank

		LDA $9E,x
		STA $7FAB9E,x
		
		JSL $0187A7|!bank			; this sucker kills $00-$02
				
		REP #$20
		STZ $02
		STZ $00
		SEP #$20
		
		LDA #$08
		STA $7FAB10,x
	+
	
	LDA #$01
	STA $14C8,x
	
	TXY

	LDA $94					;
	STA.w $E4,y				; | store x position + x offset (low byte)
	LDA $95					; |
	STA $14E0,y				; /
		
	LDA $96					;
	STA.w $E4,y				; | store y position + x offset (low byte)
	LDA $97					; |
	STA $14E0,y				; /
	
	CLC
	
.no_slot:
	TXY

normdecnospawn:
LDA $14
AND #$01
BNE ret

DEC $0F5E|!addr
RTL

Table1:
db $0A,$2A,$4E
db $05,$2B,$4E
db $05,$2B,$4F
db $09,$2C,$4F
db $06,$2D,$4F
db $04,$2D,$50
db $0A,$2E,$50
db $03,$2F,$50
db $07,$2F,$51
db $09,$30,$51
db $01,$30,$52
db $0A,$31,$52
db $04,$32,$52
db $06,$32,$53
db $08,$33,$53
db $03,$33,$54
db $0A,$34,$54
db $01,$34,$55
db $0C,$35,$55
db $05,$35,$56
db $09,$36,$56
db $0A,$36,$57
db $06,$37,$57
db $0D,$37,$58
db $04,$38,$58
db $10,$38,$59
db $03,$39,$59
db $23,$39,$5A
db $00

Table2:
db $80,$9D
db $60,$9D
db $00