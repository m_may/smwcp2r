;Fringe Forest ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;init and nmi: parallax HDMA
;main: parallax logic

!parallax_autoscroll = $0F5E|!addr	;16-bit incrementing address for layer 3 clouds

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$00			;load value
STA !hdmaparallaxtbls+6		;store to parallax table 1 (layer 3).
LDA #$78			;load value
STA !hdmaparallaxtbls		;store to parallax table 1 (layer 3).
STA !hdmaparallaxtbls+3		;store to parallax table 1 (layer 3).

JSL HDMA_FringeForest

nmi:
REP #$20			;kinda Variable 3 H-scroll (and also handles some parallax)
LDA !parallax_autoscroll
STA !hdmaparallaxtbls+1

LDA $1A
LSR
CLC
ADC $1A
LSR #3
STA $1E
STA $1466|!addr
STA !hdmaparallaxtbls+4

LDA #$00BA			;no V-scroll (fixed Y-pos)
STA $20
STA $1468|!addr
SEP #$20

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return.

main:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$07
ORA $9D
ORA $13D4|!addr
BNE +

REP #$20
INC !parallax_autoscroll
SEP #$20

+
RTL			;return.
