;Crocodile Crossing ASM (level 1D).
;Includes:
;init: 2-channel HDMA gradient, custom layer 3 scrolling
;nmi: custom layer 3 scrolling

init:
JSL nmi				;run nmi code during init as well

REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

nmi:
REP #$20
LDA $1466|!addr
LSR #2
CLC
ADC $1466|!addr
STA $22
SEP #$20

+
RTL				;return.

Table1:
db $0A,$27,$4F
db $04,$28,$50
db $03,$29,$51
db $01,$29,$52
db $02,$2A,$52
db $01,$2A,$53
db $02,$2B,$53
db $03,$2C,$54
db $02,$2D,$55
db $02,$2E,$56
db $82,$2F,$56,$2F,$57
db $02,$30,$57
db $01,$30,$58
db $02,$31,$58
db $02,$32,$59
db $82,$33,$59,$33,$5A
db $03,$34,$5A
db $03,$35,$5B
db $03,$36,$5B
db $06,$37,$5C
db $0A,$38,$5C
db $04,$38,$5D
db $02,$37,$5D
db $04,$37,$5E
db $18,$37,$5F
db $02,$38,$5F
db $02,$39,$5F
db $03,$3A,$5F
db $02,$3B,$5F
db $03,$3C,$5F
db $02,$3D,$5F
db $03,$3E,$5F
db $04,$3F,$5F
db $04,$3F,$5E
db $06,$3F,$5D
db $5D,$3F,$5C
db $00

Table2:
db $0D,$9A
db $06,$9B
db $06,$9C
db $06,$9D
db $09,$9E
db $27,$9F
db $03,$9E
db $03,$9D
db $02,$9C
db $03,$9B
db $02,$9A
db $02,$99
db $02,$98
db $0A,$97
db $76,$96
db $00

MsgDMA1:
db $1F,$39,$13,$39,$47,$39,$48,$39,$52,$39,$1F,$39,$48,$39,$52,$39
db $4B,$39,$40,$39,$4D,$39,$43,$39,$5D,$39,$52,$39,$1F,$39,$46,$39
db $4E,$39,$53,$39,$1F,$39,$52,$39,$4E,$39,$4C,$39,$44,$39,$1F,$39
db $51,$39,$44,$39,$40,$39,$4B,$39,$4B,$39,$58,$39,$1F,$39,$1F,$39
db $1F,$39,$41,$3D,$48,$3D,$53,$3D,$44,$3D,$1C,$3D,$47,$3D,$40,$3D
db $4F,$3D,$4F,$3D,$58,$3D,$1F,$3D,$40,$3D,$4B,$3D,$4B,$3D,$48,$3D
db $46,$3D,$40,$3D,$53,$3D,$4E,$3D,$51,$3D,$52,$3D,$1A,$39,$1F,$39
db $01,$39,$44,$39,$53,$39,$53,$39,$44,$39,$51,$39,$1F,$39,$1F,$39
db $1F,$39,$41,$39,$44,$39,$1F,$39,$42,$39,$40,$39,$51,$39,$44,$39
db $45,$39,$54,$39,$4B,$39,$1D,$39,$1F,$39,$44,$39,$52,$39,$4F,$39
db $44,$39,$42,$39,$48,$39,$40,$39,$4B,$39,$4B,$39,$58,$39,$1F,$39
db $48,$39,$45,$39,$1F,$39,$58,$39,$4E,$39,$54,$39,$1F,$39,$1F,$39
db $1F,$39,$52,$39,$44,$39,$44,$39,$1F,$39,$40,$39,$1F,$39,$5B,$3D
db $1A,$3D,$5C,$3D,$1F,$3D,$52,$3D,$48,$3D,$46,$3D,$4D,$3D,$1F,$39
db $40,$39,$51,$39,$4E,$39,$54,$39,$4D,$39,$43,$39,$1B,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39

MsgDMA2:
db $1F,$39,$13,$39,$47,$39,$44,$39,$1F,$39,$56,$39,$40,$39,$53,$39
db $44,$39,$51,$39,$1F,$39,$48,$39,$4D,$39,$1F,$39,$53,$39,$47,$39
db $44,$39,$1F,$39,$4D,$39,$44,$39,$57,$39,$53,$39,$1F,$39,$40,$39
db $51,$39,$44,$39,$40,$39,$1F,$39,$48,$39,$52,$39,$1F,$39,$1F,$39
db $1F,$39,$4C,$3D,$40,$3D,$51,$3D,$52,$3D,$47,$3D,$58,$3D,$1F,$3D
db $40,$3D,$4D,$3D,$43,$3D,$1F,$3D,$43,$3D,$40,$3D,$4D,$3D,$46,$3D
db $44,$3D,$51,$3D,$4E,$3D,$54,$3D,$52,$3D,$1F,$3D,$53,$3D,$4E,$3D
db $1F,$3D,$52,$3D,$56,$3D,$48,$3D,$4C,$3D,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$48,$3D,$4D,$3D,$1B,$39,$1F,$39,$18,$39,$4E,$39,$54,$39
db $1F,$39,$56,$39,$48,$39,$4B,$39,$4B,$39,$1F,$39,$47,$39,$40,$39
db $55,$39,$44,$39,$1F,$39,$53,$39,$4E,$39,$1F,$39,$45,$39,$48,$39
db $4D,$39,$43,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$40,$39,$4D,$39,$4E,$39,$53,$39,$47,$39,$44,$39,$51,$39
db $1F,$39,$56,$39,$40,$39,$58,$39,$1F,$39,$53,$39,$4E,$39,$1F,$39
db $42,$39,$51,$39,$4E,$39,$52,$39,$52,$39,$1F,$39,$48,$39,$53,$39
db $1B,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39
db $1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39,$1F,$39