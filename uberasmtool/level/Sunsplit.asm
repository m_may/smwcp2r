;Sunset Split ASM (level 20).
;Includes:
;load: SMWC coin trigger initializer
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $04,$34,$42
db $07,$34,$43
db $01,$34,$44
db $08,$35,$44
db $0A,$35,$45
db $01,$35,$46
db $08,$36,$46
db $07,$36,$47
db $01,$36,$48
db $05,$37,$48
db $05,$37,$49
db $02,$37,$4A
db $02,$38,$4A
db $03,$38,$4B
db $03,$38,$4C
db $03,$39,$4D
db $02,$39,$4E
db $02,$39,$4F
db $01,$3A,$4F
db $04,$3A,$50
db $03,$3A,$51
db $01,$3A,$52
db $03,$3B,$52
db $03,$3B,$53
db $05,$3B,$54
db $01,$3B,$55
db $03,$3C,$55
db $05,$3C,$56
db $05,$3C,$57
db $03,$3C,$58
db $03,$3D,$58
db $06,$3D,$59
db $06,$3D,$5A
db $06,$3D,$5B
db $06,$3E,$5C
db $06,$3E,$5D
db $0A,$3E,$5E
db $04,$3E,$5F
db $42,$3D,$5F
db $00

Table2:
db $24,$81
db $17,$82
db $0A,$83
db $0A,$84
db $0B,$85
db $0D,$86
db $10,$87
db $10,$88
db $15,$89
db $44,$8A
db $00