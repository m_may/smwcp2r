;Koura Crevice ASM (level 16).
;Includes:
;init: reset certain addresses, set up layer 2 BG to move slower than layer 3
;main: yoshi handling
;nmi: set up layer 2 BG to move slower than layer 3

	!Flag = $0F5E|!addr
	!YoshiHeadTile = $68	; The tile of Yoshi's head or whatever should be displayed
				; on the bottom-left corner of the screen.
	!Address = $0F5F|!addr
	!DisableRAM = $0F60|!addr

init:
STZ !Flag
STZ !Address
INC $1B9B|!addr

nmi:
REP #$20
LDA $22
LSR
CLC
ADC $22
LSR
STA $1E
STA $1466|!addr
LDA $24
LSR
CLC
ADC $24
LSR
STA $20
STA $1468|!addr
SEP #$20
RTL

main:
	LDA !Flag
	BEQ .OffYoshi

	lda #$04
	sta !DisableRAM

	LDA #$7F
	STA $78

	LDA $17
	AND #$80
	ORA $15
	STA $15

	LDA $18
	AND #$80
	ORA $16
	STA $16

	LDA #$80
	TRB $17
	TRB $18

.OffYoshi
	RTL

.FindOAM
	slot_loop:
		LDA $0301|!addr,X
		CMP #$F0
		BEQ +
			INX #4
			CPX #$0100
			BCC slot_loop	; not sure why this was bcs
	+
	RTS