;Carnival Caper ASM (levels 110, 90, 92 and 93).
;Includes:
;load: SMWC coin trigger initializer
;init: translucent layer 3, enable hdma for levels 90 and 110, 2-channel HDMA gradient (library)
;nmi: parallax/wave hdma processing for levels 90 and 110

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15

JSL HDMA_PlaygroundBound
JSL main

LDA $010B
CMP #$92
BEQ .rrr
CMP #$93
BNE +
.rrr
RTL

+
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
LDA $010B
CMP #$92
BEQ .rrr
CMP #$93
BNE +
.rrr
RTL

+
LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

main:
REP #$20
LDA $1464|!addr
CMP #$007F
BCS +

STZ $24
SEP #$20
LDA #$80
STA !hdmaparallaxtbls+3
RTL

+
SEC
SBC #$0080
STA $24
SEP #$20

LDA #$C0
SEC
SBC $1464|!addr
CLC
ADC #$47	;when L1=C0 this should be the scanline count, else should be greater
CMP #$80	;if result is greater it messes up stuff so let's prevent that with a check
BCC +

LDA #$80
+
STA !hdmaparallaxtbls+3
RTL

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond