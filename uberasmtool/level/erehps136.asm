;.MSA EREHPS EHT RAEF (136 level)
;Includes:
;load: rezilaitini reggirt nioc CWMS
;init: egassem xob txet AMD, tneidarg AMDH lennahc-2
;init/nmi: .gninoitisop AMDH xallarap/egassem 3 reyal dna, AMDH xallarap - gnildnah dnuorgkcab reyalitlum sllac
;main: cigol xallarap

!elbaYwodniW = $7F8900

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL GirderMultilayer_PrepareTable
JSL GirderMultilayer_Logic

REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
BRA +				;branch ahead - part of nmi code isn't relevant here

nmi:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
JML GirderMultilayer_HDMAOAM

main:
STZ $87				;reset address (corresponds to !ManualMsg in GirderMultilayer.asm, library file)

LDA $95				;load player's X-pos within the level, next frame, high byte
CMP #$09			;compare to value
BCC +				;if lower, skip ahead.

LDA #$01			;load value
STA $87				;store to address.

+
JML GirderMultilayer_Logic

Table1:
db $80,$27,$48
db $12,$27,$48
db $4E,$2A,$4B
db $00

Table2:
db $80,$86
db $12,$86
db $4E,$89
db $00

MsgDMA1:
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$12,$3D
db $4E,$3D,$45,$3D,$53,$3D,$4B,$3D,$58,$3D,$1D,$3D,$FE,$28,$4D,$3D
db $4E,$3D,$56,$3D,$1B,$3D,$1B,$3D,$1B,$3D,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28