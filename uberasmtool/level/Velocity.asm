;rip easter egg because rip steel samba
!ripinrip = 1

;BD's Amazing Musical Easter Egg for Velocity Valves!
;I like the new song so much but people prefers Steel Samba for some reason, BUT!
;I have superpowers (not really) so if the player holds B during the map transition,
;Torchkas' song will be played on Velocity Valves instead.
;Also:
;load: SMWC coin trigger initializer

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

if !ripinrip == 0

init:
LDA $7FB000		;load song previously being played
CMP #$06		;check if it was the p-switch theme
BNE +			;if not, skip ahead.

LDA $0F5E|!addr		;load backup of backup of song played when the level was first loaded
BRA ++			;branch ahead

+
LDA $141A|!addr		;load levels entered counter
BNE ret			;if not zero, return.

LDA $15			;load controller data 1
ORA $0DA2|!addr		;OR with player 1 controller data 1 (I guess only this one gets read)
AND #$80		;check if B or A are being held
BEQ SteelSambaDefault	;if none are, play the default song.

LDA #$15		;else play torchkas' song
BRA ++			;branch ahead

SteelSambaDefault:
LDA #$46		;load song number
++
STA $1DFB|!addr		;store to address to play it.
STA $7FB008		;store to amk address (backup song).
STA $0DDA|!addr		;store to smw music backup address
STA $0F5E|!addr		;store to backup of backup. we need it for level 77 in case a p-switch is pressed before the level is entered.

LDA #$00		;load value
STA $7FB000		;store to amk address (current song). force song to play.

ret:
RTL			;return.

endif