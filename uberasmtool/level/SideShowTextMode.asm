;Sideshow Showdown ASM (level C1).
;Includes:
;load/init: setup screen positions, hide all sprites
;main: freeze the player, prevent pausing, press B to warp to screen exit

GamePlayed:
db $01,$02,$04,$08,$10,$00

load:
init:
LDA $95				;load player's X-pos, high byte
TAX				;transfer to X
STA $7F9A9F			;store to game index RAM.
STA $1B				;store to layer 1 X-pos current frame, low byte.
STA $1463|!addr			;store to layer 1 X-pos next frame, high byte.
STA $1F				;store to layer 2 X-pos current frame, low byte.
STA $1467|!addr			;store to layer 2 X-pos next frame, high byte.
STZ $1A				;reset layer 1 X-pos current frame, low byte.
STZ $1462|!addr			;reset layer 1 X-pos next frame, high byte.
STZ $1E				;reset layer 2 X-pos current frame, low byte.
STZ $1466|!addr			;reset layer 2 X-pos next frame, high byte.
LDA #$07			;load value
STA $212C|!addr			;store to mainscreen.
STA $212D|!addr			;store to subscreen.
STA $212E|!addr			;store to mainscreen window.
STA $212F|!addr			;store to subscreen window.

LDA $7F9A83			;load games played bitflags
ORA GamePlayed,x		;set respective bit according to index
STA $7F9A83			;store restult back.
RTL				;return.

main:
LDA #$04			;load value
STA $13D3|!addr			;store to pause flipping timer.
STA $18BD|!addr			;store to player stunned timer.

LDA $16				;load controller data 1, first frame only
ORA $18				;OR with controller data 2, first frame only
AND #$80			;check if either B or A were pressed
BEQ ret				;if none were pressed, return.

LDA #$29			;load SFX number
STA $1DFC|!addr			;store to address to play it.

LDA #$06
STA $71
STZ $89
STZ $88				;why comment this infamous piece of code lol

ret:
RTL				;return.