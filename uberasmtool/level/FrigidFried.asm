;Frigid-Fried Frenzy ASM.
;Includes
;load: SMWC coin custom trigger init
;init/nmi: layer 2 parallax init and processing
;nmi: layer 2 parallax processing
;main: slippery blocks handling

!scansub = $0F5E|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
REP #$20
LDA #$0F02			;mode 2 on $2111 (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL

nmi:
LDA $1468|!addr
SEC
SBC #$90
STA !scansub		;amount of scanlines to subtract from first entry

LDA #$7F
SEC
SBC !scansub
STA !hdmaparallaxtbls
LDA #$40
STA !hdmaparallaxtbls+3
LDA #$04
STA !hdmaparallaxtbls+6
LDA #$12
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12

REP #$20
LDA $1466|!addr
LSR
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4
CLC
ADC $1466|!addr
LSR
STA !hdmaparallaxtbls+7
CLC
ADC $1466|!addr
LSR
STA !hdmaparallaxtbls+10
CLC
ADC $1466|!addr
LSR
STA !hdmaparallaxtbls+13
SEP #$20

LDA #$00
STA !hdmaparallaxtbls+15
RTL

main:
JML SlipperyBlocks_main		;call slippery blocks handling routine and return.