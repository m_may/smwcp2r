;awakening ASM (Phase 3: Confusion, Phase 4: Depression, Phase 5: Anger).
;Includes:
;init: Reset thunderbell ring timer. Init messages for Depression.
;main: Handle thunberbell ring timer and warp after ringing. Swap messages for Depression.

	!Msg1LoadingZone1 = $0400
	!Msg2LoadingZone = $0600
	!Msg1LoadingZone2 = $0700

	!Msg1Num = $0F5E|!addr
	!Msg2Num = $0F5F|!addr

init:
STZ $0F42
	LDA $010B
	CMP #$4F  ;check if Depression
	BNE .return

	REP #$20
	STZ !Msg1Num
	JSL Messages_DMA_DefaultPropFill
	LDA.w #Messages_Tile_Opaque
	JSL Messages_DMA_FillTileVertMargins
	SEP #$20

	.return
RTL

main:
LDA $0F42
BEQ .return
CMP #$01
BEQ .warp
DEC $0F42
.return
	REP #$20
	LDA $010B
	CMP #$014F  ;check if Depression
	BNE .reallyreturn

	LDA $94  ;player x position
	CMP.w #!Msg1LoadingZone1
	BCC .msg1num1
	.msg1num2or3
	CMP.w #!Msg1LoadingZone2
	BCC .msg1num2
	.msg1num3
	LDX #$06
	BRA .msg1
	.msg1num2
	LDX #$04
	BRA .msg1
	.msg1num1
	LDX #$02

	.msg1
	CPX !Msg1Num
	BEQ .noupdatemsg1
	STX !Msg1Num
	LDA Msg1Pointers-2,x
	LDX #$02
	JSL Messages_DMA
	.noupdatemsg1

	LDA $94  ;player x position
	CMP.w #!Msg2LoadingZone
	BCC .msg2num1
	.msg2num2
	LDX #$04
	BRA .msg2
	.msg2num1
	LDX #$02

	.msg2
	CPX !Msg2Num
	BEQ .noupdatemsg2
	STX !Msg2Num
	LDA Msg2Pointers-2,x
	LDX #$06
	JSL Messages_DMA
	.noupdatemsg2

	.reallyreturn
	SEP #$20
RTL

.warp
LDA #$06
STA $71
STZ $89
STZ $88
RTL

	Msg1Pointers:
	dw Messages_awakeningDepression_1, Messages_awakeningDepression_3, Messages_awakeningDepression_5
	Msg2Pointers:
	dw Messages_awakeningDepression_2, Messages_awakeningDepression_4
