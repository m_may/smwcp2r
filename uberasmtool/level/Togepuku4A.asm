;Togepuku Tussle/Stone Sea Sanctuary ASM (levels 4A and EE).
;Includes:
;init: wave HDMA init and processing, 2-channel HDMA gradient
;main: wave HDMA processing


init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E03			;mode 3 on $210E (a.k.a. affect layer 1 Y-wise and layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
LDX #$00			;\  Init of
LDY #$00			; | X and Y. Y will be the loop counter, X the index for writing the table to the RAM

LDA $010B|!addr
CMP #$EE
BNE +

LDA #$12			;Set scanline height
STA !hdmaparallaxtbls,x

REP #$20
LDA $1C
STA !hdmaparallaxtbls+1,x
LDA $1E
STA !hdmaparallaxtbls+3,x
SEP #$20

INX #5				; | Increase X five times

+
LDA $14				; | Speed of Waves
LSR #2				; | Slowing down A
STA $00				;/  Save for later use

.Loop
LDA #$08			;\  Set scanline height
STA !hdmaparallaxtbls,x		; | for each wave
TYA				; | Transfer Y to A
ADC $00				; | Add in frame counter
AND #$07			; | only the lower half of the byte
PHY				; | Push Y, so that the loop counter isn't lost.
TAY				;/  Transfer A to Y

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1C				; | Add value from the wave table to layer y position (low byte).
STA !hdmaparallaxtbls+1,x	; | X position low byte
LDA $1D				; | Load layer y position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+2,x	;/  X position high byte

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1E				; | Add value from the wave table to layer x position (low byte).
STA !hdmaparallaxtbls+3,x	; | X position low byte
LDA $1F				; | Load layer x position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+4,x	;/  X position high byte

PLY				;\  Pull Y (original loop counter)
CPY #$1A			; | Compare with #$1C scanlines
BPL .End			; | If bigger, end HDMA
INX #5				; | Increase X five times, so that in the next loop, it writes the new table data at the end of the old one...
INY				; | Increase Y
BRA .Loop			;/  Do the loop

.End
LDA #$00			;\  End HDMA by writing
STA !hdmaparallaxtbls+5,x	; | #$00 here
RTL				;return.

Table1:
db $27,$51,$97
db $10,$50,$97
db $0B,$50,$96
db $0D,$4F,$96
db $08,$4E,$95
db $08,$4E,$94
db $08,$4D,$94
db $08,$4C,$93
db $08,$4B,$92
db $08,$4B,$91
db $08,$4A,$91
db $08,$49,$90
db $08,$49,$8F
db $08,$48,$8E
db $08,$47,$8D
db $08,$46,$8D
db $08,$46,$8C
db $08,$46,$8B
db $20,$45,$8A
db $01,$46,$8B
db $00

Table2:
db $37,$28
db $20,$27
db $20,$26
db $18,$25
db $20,$24
db $31,$23
db $00

Wave_Table:
db $00,$01,$02,$03,$03,$02,$01,$00