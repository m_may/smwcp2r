;Crumbling Catacombs ASM (level 86).
;Includes:
;main: map16 specific shattering effects

main:
	LDA $18C5
	AND #$01	
	CMP #$01
	BEQ Floor2_86
	REP #$20
	LDA $D3
	CMP #$0100
	BNE Floor2_86
	LDA $D1	
	CMP #$04B0
	BCC Floor2_86
	CMP #$0510
	BCS Floor2_86
	LDA #$00A0
	STA $98
	LDA #$0520
	STA $9A
	SEP #$20
	LDA $18C5
	ORA #$01
	STA $18C5
	LDA #$01
	STA $14AF
	JSL Shatter101_main
	LDA #$02
	STA $9C
	JSL $80BEB0
	REP #$20
	LDA #$00C0
	STA $98
	LDA #$04C0
	STA $9A
	SEP #$20
	JSL Shatter101_main

	Floor2_86:
	SEP #$20
	LDA $18C5
	AND #$02	
	CMP #$02
	BEQ Return86
	REP #$20
	LDA $D3
	CMP #$0040
	BNE Return86
	LDA $D1
	CMP #$0100
	BCC Return86
	CMP #$0120
	BCS Return86

	LDA #$0060
	STA $98
	LDA #$0100
	STA $9A
	SEP #$20
	LDA $18C5
	ORA #$02
	STA $18C5
	LDA #$02
	STA $9C
	JSL $80BEB0
	REP #$20
	LDA #$0110
	STA $9A
	SEP #$20
	JSL Shatter101_main
	JSL $80BEB0
	REP #$20
	LDA #$0120
	STA $9A
	SEP #$20
	JSL $80BEB0
	Return86:
	SEP #$20
	RTL