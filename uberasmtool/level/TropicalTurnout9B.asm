;Tropical Turnout ASM (levels 9B and 9C).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 handler, 2-color HDMA gradient, wave HDMA init and processing for level 9A, layer 3 parallax
;nmi: wave/parallax HDMA processing

!hdmaparallaxtbls2 = !hdmaparallaxtbls+100	;easy lazy way
!waveindex = $0F5E|!addr			;index for wave scroll
!parallax_autoscroll = $0F60|!addr		;index for clouds (parallax) scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15
STA !hdmaparallaxtbls2+9

LDA #$70
STA !hdmaparallaxtbls2
LDA #$46
STA !hdmaparallaxtbls2+3
STA !hdmaparallaxtbls2+6

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
STA $4364

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4360
LDA.w #!hdmaparallaxtbls2	;address of table
STA $4362
SEP #$20

LDA $010B|!addr
CMP #$9B
BNE +

LDA #$60			;Enable HDMA on channels 5, 6
BRA ++

+
LDA #$40			;Enable HDMA on channels 6

++
TSB $0D9F|!addr

nmi:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$07
ORA $9D
ORA $13D4|!addr
BNE +

REP #$20
INC !parallax_autoscroll
+

REP #$20

+
REP #$20
LDA $22
STA !hdmaparallaxtbls2+7
LDA !parallax_autoscroll
STA !hdmaparallaxtbls2+1
LDA $1E
STA !hdmaparallaxtbls2+4
SEP #$20

LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond

Table1:
db $0D,$29,$4E
db $11,$2B,$4E
db $10,$2C,$4F
db $10,$2E,$4F
db $10,$30,$4F
db $10,$31,$50
db $04,$33,$50
db $01,$35,$50
db $02,$36,$51
db $02,$38,$51
db $02,$3A,$56
db $02,$3D,$5A
db $02,$3F,$5E
db $03,$3F,$5F
db $01,$3B,$59
db $01,$31,$53
db $01,$29,$4D
db $03,$27,$4C
db $05,$28,$4D
db $05,$29,$4D
db $08,$2A,$4E
db $02,$2B,$4F
db $05,$2C,$4F
db $04,$2D,$50
db $04,$2E,$50
db $08,$2F,$51
db $05,$30,$52
db $05,$31,$53
db $06,$32,$54
db $04,$33,$54
db $06,$34,$55
db $05,$35,$55
db $04,$36,$56
db $06,$37,$57
db $05,$38,$57
db $05,$39,$58
db $05,$3A,$59
db $09,$3B,$59
db $00

Table2:
db $2E,$97
db $34,$98
db $05,$99
db $04,$98
db $02,$97
db $02,$9B
db $01,$9F
db $01,$9E
db $01,$9A
db $01,$96
db $08,$95
db $0D,$96
db $07,$97
db $10,$98
db $05,$99
db $0F,$9A
db $0B,$9B
db $0A,$9C
db $0F,$9D
db $09,$9E
db $00
