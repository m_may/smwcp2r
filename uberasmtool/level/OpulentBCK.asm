;Opulent Oasis ASM (levels 103 and 145 - no parallax version).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 over layers 1, 2 and sprites, waves HDMA init/processing, layer 2 "water tide" interaction
;init/main: waves HDMA init/processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

JSR mainwave			;execute wave processing

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls+80	;address of table
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4364
LDA #$40			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 6

JSL InvisiL2_Water2		;make part of layer 2 BG act like water as if there was a layer 3 tide, but without current
RTL				;return

main:
JSR mainwave			;execute wave processing
RTL

mainwave:
LDA $14
LSR #3
AND #$07
STA !waveindex

LDA #$7F		;load scanline count
SEC			;set carry
SBC $1C			;subtract layer 1 Y-pos, current frame, low byte, from it
STA $00			;store to scratch RAM.
BMI zeroone

LDA #$80
BRA st

zeroone:
LDA.b #$FF
SEC
SBC $1C

st:
STA $01

+
REP #$20
LDA !waveindex
ASL
TAY

STZ $0E
LDX #$00		;starting index for loop

waveloop:
PHY

CPX #$00		;first entry of table varies depending on layer 1 Y-pos
BEQ first

SEP #$20
LDA #$08		;default scanline count value

LDY $0E
BNE +
CPX #$03		;second entry
BNE +

second:
INC $0E
LDA $01			;second scanline count value
BRA ssec

first:
SEP #$20
LDA $00			;this holds the dynamic scanline count set before the loop
BMI second

ssec:
STA !hdmaparallaxtbls+80,x

REP #$20
LDA $1C
BRA ++

+
STA !hdmaparallaxtbls+80,x
REP #$20

PLY
LDA $1C
CLC
ADC Index,y

PHA

INY #2
TYA
AND #$000F
TAY

PLA

PHY	;anticrash

++
STA !hdmaparallaxtbls+81,x
PLY
INX #3
CPX #$60
BNE waveloop

SEP #$20

LDA #$00
STA !hdmaparallaxtbls+80,x		;finalize table
RTS

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond