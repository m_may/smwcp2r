;Bedazzling Bastion ASM (levels 1B0 and 1B1).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Bedazzling
JSL Messages_DMA_FillProp
LDX #$04
JSL Messages_DMA_Finish
LDX #$00
LDA.w #Messages_Bedazzling1B0_1
JSL Messages_DMA
LDX #$04
LDA.w #Messages_Bedazzling1B0_2
JSL Messages_DMA
SEP #$20
RTL
