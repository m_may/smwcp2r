;Stormy ASM.
;Includes:
;load: SMWC coin trigger initializer
;init: message box text DMA, disable layer 2 H-scroll
;main: spawn cluster rain, layer 2 autoscroll fast

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $1413|!addr			;disable layer 2 H-scroll.

REP #$20
LDA $010B|!addr
CMP #$0121
BNE .endmessage
LDX #$00
LDA.w #Messages_Props_Normal
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_Stormy
JSL Messages_DMA
.endmessage
SEP #$20

main:
LDA #$10			;load sprite number to spawn
JSL clusterspawn_run		;spawn cluster raindropdries

LDA $14
AND #$00
ORA $71
ORA $9D
ORA $13D4
BNE ret

REP #$20
INC $1466|!addr
SEP #$20

ret:
RTL

