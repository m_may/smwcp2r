;Sideshow Showdown ASM (level C3).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return.

MsgDMA1:
db $FE,$28,$FE,$28,$13,$29,$47,$29,$44,$29,$FE,$28,$4F,$29,$4E,$29
db $56,$29,$44,$29,$51,$29,$FE,$28,$4E,$29,$45,$29,$FE,$28,$53,$29
db $47,$29,$44,$29,$FE,$28,$52,$29,$56,$29,$48,$29,$53,$29,$42,$29
db $47,$29,$FE,$28,$58,$29,$4E,$29,$54,$29,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$F2,$29,$F2,$69,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$F6,$29,$F6,$69,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$4F,$29,$54,$29,$52,$29,$47,$29,$44,$29,$43,$29
db $FE,$28,$47,$29,$40,$29,$52,$29,$FE,$28,$53,$29,$54,$29,$51,$29
db $4D,$29,$44,$29,$43,$29,$FE,$28,$F3,$29,$F3,$69,$FE,$28,$48,$29
db $4D,$29,$53,$29,$4E,$29,$FE,$28,$F7,$29,$F7,$69,$1B,$29,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$00,$29,$56,$29,$56,$29,$1B,$29,$1B,$29,$1B,$29,$FE,$28
db $4E,$29,$54,$29,$51,$29,$FE,$28,$45,$29,$54,$29,$4D,$29,$53,$29
db $48,$29,$4C,$29,$44,$29,$FE,$28,$48,$29,$52,$29,$FE,$28,$4E,$29
db $55,$29,$44,$29,$51,$29,$1B,$29,$1B,$29,$1B,$29,$FE,$28,$FE,$28