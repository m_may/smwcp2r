;Emerald Escapade ASM (levels 23 and B4).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient (23)
;main: violent quicksand handling

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA $010B|!addr
CMP #$23
BEQ +
RTL

+
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

main:
JML quicksink_run		;run violent quicksand code and return

Table1:
db $2D,$21,$41
db $29,$21,$42
db $11,$22,$43
db $0B,$22,$44
db $0C,$22,$45
db $12,$22,$46
db $02,$23,$46
db $4E,$23,$47
db $00

Table2:
db $1C,$82
db $33,$83
db $0F,$84
db $0B,$85
db $07,$86
db $08,$87
db $08,$88
db $09,$89
db $0B,$8A
db $0A,$8B
db $0C,$8C
db $0B,$8D
db $0A,$8E
db $21,$8F
db $00