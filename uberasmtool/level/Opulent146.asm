;Opulent Oasis ASM (level 146). CURSED
;Includes:
;init: message box text DMA
;init/main: parallax HDMA init/processing
;main: no L/R scrolling, no scrolling left, no side exit enabled (because custom sprite 4D does it in its save progress code)


init:
REP #$20			;16-bit A
JSL Messages_DMA_DefaultPropFill
LDX #$00
LDA.w #Messages_Opulent146_1
JSL Messages_DMA
LDX #$04
LDA.w #Messages_Opulent146_2
JSL Messages_DMA
;SEP #$20

JSL ProcOffsets			;process offsets for use before we set the effect on

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return

main:
STZ $1B96|!addr			;no side exit enabled.

LDA $9D				;load sprites/animation locked flag
ORA $13D4|!addr			;OR with pause flag
BNE ProcOffsets			;if any are set, skip ahead.

LDA $18				;load controller data 2, first frame only
AND #$30			;check if L or R are pressed
BEQ +				;if not pressed, skip ahead.

LDA #$2A			;load SFX value
STA $1DFC|!addr			;store to address to play it.

+
STZ $1401|!addr			;reset L/R time to scroll the screen.

LDA #$01			;load value
STA $1411|!addr			;store to horizontal scroll flag.

LDA $7B				;load player's X speed
BMI noHscroll			;if negative, disable vertical scroll.
LDA $7E				;load player's X-pos within the screen
CMP #$69			;compare to value
BCS +				;if higher or equal, skip ahead.

noHscroll:
STZ $1411|!addr			;disable horizontal scroll.

+
LDA $0F5E|!addr			;load manual coordinates for layer 2 X-pos
CLC				;clear carry
ADC $17BD|!addr			;add amount of pixels the screen has moved horizontally
STA $0F5E|!addr			;store result back.
LDA $0F5F|!addr			;load manual coordinates for layer 2 X-pos, high byte
ADC #$00			;add carry if set
STA $0F5F|!addr			;store result back.

ProcOffsets:
REP #$20			;16-bit A
LDA $0F5E|!addr			;load manual layer 2 X-pos
LSR				;divide by 2
STA !hdmaparallaxtbls+34	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $20				;load layer 2 Y-pos, current frame
SEC				;set carry
SBC #$0060			;subtract value
STA $00				;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $00				;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $00				;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $00				;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $00				;load layer 2 Y-pos
CLC				;clear carry
ADC #$0060			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
RTL				;return.

Tbl1:
db $7F,$3D,$01
Tbl1End:
