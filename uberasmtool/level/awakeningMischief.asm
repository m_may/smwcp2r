;awakening ASM (Phase 2: Mischief).
;Includes:
;main: Horizontal Level wrap-around, on/off (reverse) gravity handler, bg scrolling, kill player if on water (swimming on lava lol)

!reversed		= $1B7F|!addr

main:
; scroll bg
STZ $1414|!addr ;/ V-Scroll = none 
LDA $13 
AND #$01 
BNE .endbg 

LDA !reversed
BNE .scrolldown
.scrollup
LDA $1468|!addr
SEC
SBC #$01
STA $1468|!addr
LDA $1469|!addr
SBC #$00
STA $1469|!addr
BRA .endbg
.scrolldown
LDA $1468|!addr
CLC
ADC #$01
STA $1468|!addr
LDA $1469|!addr
ADC #$00
STA $1469|!addr
.endbg

OnOffGravity:
LDA $14AF|!addr  ; on/off state
STA !reversed

; warp stuff
LDA $9D
BNE .noWrap
JSR WrapMario
JSR WrapSprites
.noWrap
RTL

;; Wrap-around ASM Below ------------------

!topEdge = $00A0	; where the "top" wrap point is
!botEdge = $01A0	; where the "bottom" wrap point is

!dist = !botEdge-!topEdge

WrapMario:
	LDA $13E0|!addr		; don't wrap if dead
	CMP #$3E
	BEQ .noWrap
	REP #$20
	LDA $96
	CMP #!botEdge
	BMI .checkAbove
	SEC : SBC #!dist
	STA $96
	BRA .noWrap
  .checkAbove
	CMP #!topEdge
	BPL .noWrap
	CLC : ADC #!dist
	STA $96
  .noWrap
	SEP #$20
	RTS


WrapSprites:
	LDX #!sprite_slots-1
  .loop
	LDA !14C8,x
	BEQ .skip
	CMP #$02
	BEQ .skip
	LDA !14D4,x
	XBA
	LDA !D8,x
	REP #$20
	CMP #!botEdge
	BMI .checkAbove
	SEC : SBC #!dist
	SEP #$20
	STA !D8,x
	XBA
	STA !14D4,x
	BRA .skip
  .checkAbove
	CMP #!topEdge
	BPL .skip
	CLC : ADC #!dist
	SEP #$20
	STA !D8,x
	XBA
	STA !14D4,x
  .skip
	SEP #$20
	DEX
	BPL .loop
	RTS