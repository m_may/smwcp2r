;Handle Switch Conditional Map16 and ExAnimation

load:
LDA #$00
LDX !SwitchesUnlocked
BEQ +
ORA #$01
+
LDX (!SwitchesUnlocked)+1
BEQ +
ORA #$02
+
LDX (!SwitchesUnlocked)+2
BEQ +
ORA #$04
+
LDX (!SwitchesUnlocked)+3
BEQ +
ORA #$08
+
STA $7FC062

;fallthrough

main:
LDA #$00
LDX $1F27|!addr
BEQ +
ORA #$01
+
LDX $1F28|!addr
BEQ +
ORA #$02
+
LDX $1F29|!addr
BEQ +
ORA #$04
+
LDX $1F2A|!addr
BEQ +
ORA #$08
+
STA $7FC0FC
RTL
