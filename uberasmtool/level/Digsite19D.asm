;Digsite Dangers ASM (level 19D).
;Includes:
;init: HDMA
;main: violent quicksand handling

!L2Timer = $0F5E|!addr

init:
JSL quicksink_prephdma		;prepare HDMA effects and stuff for level from library code
JML HDMA_Color3			;execute HDMA (A goes 8-bit after this routine) and return.

main:
JML quicksink_run		;run violent quicksand code and return