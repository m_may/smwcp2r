;Pon-Pon Palace ASM.
;Includes:
;init: reset B button flag
;main: force B button to be pressed when bounced from taiko drums

init:
	STZ $0F5E|!addr
	RTL

main:
	LDA $77
	AND #$04
	BNE init

	LDA $0F5E|!addr
	BEQ init

	LDA $7D
	BPL init

	LDA #$80
	TSB $15
	RTL