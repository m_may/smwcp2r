;Terrifying Timbers ASM (level A9).
;Includes:
;load: set on/off status to off
;init: subtraction color math for all layers
;main: change custom trigger 0 according to spotlight switch ($1486)


load:
LDA #$01
STA $14AF
RTL

init:
LDA #$17
STA $212C
STZ $212D
LDA #$A7
STA $40
RTL

main:
LDA $7FC0FC		;load custom trigger flags address 1

LDX $14AF		;load on/off flag
BEQ disable		;if not activated, disable custom trigger 0.

ORA #$01		;set bit 0
BRA store		;branch ahead

disable:
AND #$FE		;preserve all bits except 0

store:
STA $7FC0FC		;store result of A back.
RTL			;return.