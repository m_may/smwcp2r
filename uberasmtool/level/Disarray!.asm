;Dynamic Disarray ASM (levels 70 and 117).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $01,$31,$46
db $02,$32,$46
db $06,$32,$47
db $01,$33,$47
db $06,$33,$48
db $02,$33,$49
db $05,$34,$49
db $03,$34,$4A
db $04,$35,$4A
db $05,$35,$4B
db $01,$36,$4B
db $07,$36,$4C
db $07,$37,$4D
db $01,$37,$4E
db $05,$38,$4E
db $04,$38,$4F
db $03,$39,$4F
db $05,$39,$50
db $02,$3A,$50
db $03,$3A,$51
db $04,$3B,$51
db $05,$3C,$52
db $01,$3D,$52
db $03,$3D,$53
db $23,$3E,$53
db $18,$3E,$54
db $09,$3E,$55
db $09,$3E,$56
db $09,$3E,$57
db $09,$3E,$58
db $09,$3E,$59
db $08,$3E,$5A
db $09,$3E,$5B
db $09,$3E,$5C
db $08,$3E,$5D
db $00

Table2:
db $46,$9F
db $16,$9E
db $03,$9D
db $04,$9C
db $04,$9B
db $04,$9A
db $04,$99
db $04,$98
db $04,$97
db $04,$96
db $04,$95
db $05,$94
db $04,$93
db $03,$92
db $16,$93
db $16,$94
db $16,$95
db $13,$96
db $00