;Stone Sea Sanctuary ASM (level EF).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient, translucent layer 3 over layers 1, 2 and sprites
;init and main: layer 1 waves processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $1414|!addr			;set BG Y-scroll in LM to none.

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #StoneSeaGeneric_Table1>>16	;load bank of one of the tables
STA $02					;store to scratch RAM.
STA $05					;store to scratch RAM.
REP #$20				;16-bit A
LDA.w #StoneSeaGeneric_Table1		;load address of first table
STA $00					;store to scratch RAM.
LDA.w #StoneSeaGeneric_Table2		;load address of second table
STA $03					;store to scratch RAM.
JSL HDMA_Color2				;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA #$1002			;mode 2 on $2110 (a.k.a. affect layer 2 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
LDA.w #Fixed			;address of table
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA.b #Fixed>>16		;bank of table
STA $4364
LDA #$60			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channels 5 and 6

main:
JSL StoneSeaGeneric_mainwave		;process waves for HDMA

LDA $1468|!addr
SEC
SBC #$80
CLC
ADC #$40
STA $24

LDA $13D4|!addr		;load game paused flag
ORA $9D			;OR with sprites/animation frozen flag
BNE .ret		;if result isn't zero, return. don't move layer 2.

LDA $14AD|!addr		;load respective POW timer
BNE .moveit		;if not equal zero, move layer down/up.

REP #$20		;16-bit A
LDA $1468|!addr		;load layer 2 Y-pos, next frame
CMP #$0080		;compare with high height value
BCC .ret16		;if equal or higher/equal or lower, stop moving.

SEP #$20
LDA $14
AND #$01
BNE .ret
REP #$20

DEC $1468|!addr		;increment/decrement layer 2 Y-pos, next frame, by one every frame.

.ret16
SEP #$20		;8-bit A
.ret
RTL			;return.

.moveit
REP #$20		;16-bit A
LDA $1468|!addr		;load layer 2 Y-pos, next frame
CMP #$00C0		;compare with low height value
BCS .ret16		;if equal or lower/equal or higher, stop moving.

SEP #$20
LDA $14
AND #$01
BNE .ret
REP #$20

INC $1468|!addr		;decrement/increment layer 2 Y-pos, next frame, by one every frame.
SEP #$20		;8-bit A
+
RTL			;return.

Fixed:
db $80 : dw $0080
db $00