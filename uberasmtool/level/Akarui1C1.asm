;Akarui Avenue (sublevel 1C1) ASM.	
;Includes:
;init: 3-channel HDMA gradient

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
LDA.w #Table3			;load address of third table
STA $06				;store to scratch RAM.
JML HDMA_Color3			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $34,$20
db $05,$21
db $0F,$22
db $0B,$21
db $06,$20
db $05,$21
db $17,$20
db $6A,$21
db $00

Table2:
db $72,$40
db $01,$41
db $04,$40
db $0B,$41
db $0F,$42
db $07,$43
db $07,$44
db $0E,$45
db $15,$46
db $1D,$47
db $00

Table3:
db $1B,$82
db $07,$83
db $05,$84
db $08,$85
db $16,$86
db $07,$85
db $0D,$84
db $12,$83
db $02,$84
db $02,$85
db $03,$86
db $01,$87
db $02,$88
db $07,$89
db $2B,$8A
db $18,$8B
db $19,$8C
db $07,$8D
db $00