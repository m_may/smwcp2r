;Black Market (level 1FF) asm
;load - make it so already collected items don't reappear
;init - upload message

load:
LDA !Cheats
EOR #$FF
STA $7FC061 ;Conditional Direct Map16 Flags 8-F
RTL

init:
REP #$20
LDX #$00
LDA.w #Messages_Props_Pal3
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_BlackMarket
JSL Messages_DMA
SEP #$20
RTL
