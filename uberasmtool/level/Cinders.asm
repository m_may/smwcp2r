;Shivering Cinders ASM.
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: parallax X offset processing, 2-channel HDMA gradient
;main: parallax X offset processing, layer 2 parallax logic, perfect slippery blocks handler, time limit exanimation handling (workaround for message palettes)
;nmi: manual BG Y-pos updater

load:
STZ $1414|!addr			;set BG Y-scroll in LM to none.
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax
JSL FireIceParallax_HDMA	;execute HDMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

main:
JSL FireIceParallax_Logic	;handle layer 2 parallax logic
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax

STZ $0F31|!addr			;reset timer's hundreds.

REP #$20			;16-bit A
LDA $1C				;load layer 1 Y-pos, current frame
CMP #$0090			;compare to value
BCS +				;if higher, skip ahead.

INC $0F31|!addr			;increment timer's hundreds by one

+
SEP #$20			;8-bit A

JML SlipperyBlocks_main		;call slippery blocks handling routine and return.

nmi:
JML FireIceParallax_NMIBGUpd

Table1:
db $06,$20,$40
db $0A,$21,$40
db $06,$22,$40
db $05,$22,$41
db $0A,$23,$41
db $0B,$24,$41
db $0B,$25,$41
db $05,$26,$41
db $05,$26,$42
db $0B,$27,$42
db $0A,$28,$42
db $06,$29,$42
db $06,$29,$43
db $05,$29,$44
db $05,$29,$45
db $04,$29,$46
db $01,$2A,$46
db $05,$2A,$47
db $04,$2A,$48
db $04,$2A,$49
db $04,$2A,$4A
db $04,$2A,$4B
db $04,$2A,$4C
db $07,$29,$4C
db $08,$28,$4C
db $43,$27,$4C
db $00

Table2:
db $05,$80
db $08,$81
db $09,$82
db $09,$83
db $08,$84
db $09,$85
db $09,$86
db $08,$87
db $09,$88
db $08,$89
db $09,$8A
db $26,$8B
db $0A,$8C
db $05,$8B
db $05,$8A
db $07,$89
db $44,$88
db $00