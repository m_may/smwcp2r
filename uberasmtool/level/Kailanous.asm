;Callous Cliffs/Carnival-Sky Warp Pipe level ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient (alternative gradient for level 10C)

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.

LDA $010B
CMP #$0C
BEQ +

REP #$20			;16-bit A
LDA.w #Table3			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table4			;load address of second table
BRA ++				;branch ahead

+
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
++
STA $03				;store to scratch RAM.

JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $01,$2D,$46
db $04,$2E,$46
db $02,$2E,$47
db $02,$2F,$47
db $04,$2F,$48
db $0A,$2F,$49
db $0A,$2E,$4A
db $0A,$2D,$4B
db $01,$2D,$4C
db $08,$2C,$4C
db $08,$2C,$4D
db $01,$2B,$4D
db $08,$2B,$4E
db $07,$2B,$4F
db $07,$2B,$51
db $06,$2B,$52
db $02,$2B,$53
db $03,$2C,$53
db $06,$2C,$54
db $08,$2C,$55
db $0A,$2D,$56
db $05,$2D,$57
db $07,$2E,$57
db $07,$2E,$58
db $0A,$2F,$58
db $03,$2F,$59
db $0C,$31,$59
db $0C,$32,$59
db $0C,$33,$5A
db $0C,$34,$5A
db $06,$35,$5A
db $06,$35,$5B
db $0D,$36,$5B
db $01,$37,$5B
db $00

Table2:
db $03,$8B
db $03,$8C
db $03,$8D
db $02,$8E
db $03,$8F
db $07,$91
db $07,$92
db $07,$93
db $07,$94
db $06,$95
db $07,$96
db $07,$97
db $07,$98
db $06,$99
db $07,$9A
db $06,$9B
db $07,$9C
db $0A,$9D
db $0F,$9E
db $30,$9F
db $23,$9E
db $15,$9D
db $00

Table3:
db $08,$2F,$43
db $0B,$2F,$44
db $0B,$2F,$45
db $0B,$2F,$46
db $0A,$2F,$47
db $0B,$2F,$48
db $04,$2F,$49
db $07,$30,$49
db $0B,$30,$4A
db $0B,$30,$4B
db $0A,$30,$4C
db $1B,$30,$4D
db $30,$2F,$4D
db $2C,$2E,$4D
db $00

Table4:
db $05,$83
db $06,$84
db $05,$85
db $06,$86
db $06,$87
db $06,$88
db $06,$89
db $06,$8A
db $06,$8B
db $06,$8C
db $06,$8D
db $05,$8E
db $06,$8F
db $06,$90
db $06,$91
db $06,$92
db $06,$93
db $06,$94
db $20,$95
db $24,$96
db $24,$97
db $0F,$98
db $00