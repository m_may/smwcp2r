;Crumbling Catacombs ASM (level 101).
;Includes:
;init/main: parallax HDMA init/processing
;main: spawn cluster sandstorm, map16 specific shattering effects


init:
JSL ProcOffsets			;process offsets for use before we set the effect on

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return

main:
LDA #$0B			;load sprite number to spawn
JSL clusterspawn_run		;spawn cluster sandstorm

JSR Crumble			;make stuff break

ProcOffsets:
REP #$20			;16-bit A
LDA $1E				;load layer 2 X-pos current frame
STA !hdmaparallaxtbls+34	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $20				;load layer 2 Y-pos, current frame
SEC				;set carry
SBC #$0060			;subtract value
STA $00				;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $00				;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $00				;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $00				;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $00				;load layer 2 Y-pos
CLC				;clear carry
ADC #$0060			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
RTL				;return.

Tbl1:
db $7F,$3D,$01
Tbl1End:

Crumble:
	LDA $18C5
	CMP #$01
	BEQ Return101
	REP #$20
	LDA $D3
	STA $60	
	CMP #$0160
	BNE Return101
	LDA $D1	
	CMP #$0160
	BCC Return101
	CMP #$0190
	BCC Tile101
	Return101:
	SEP #$20
	RTS
	Tile101:	
	LDA #$0180
	STA $98
	LDA #$0160
	STA $9A
	SEP #$20
	LDA #$01
	STA $18C5
	JSL Shatter101_main
	TileLoop101:
	LDA #$02
	STA $9C
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$A0
	BNE TileLoop101
	JSL Shatter101_main
	RTS