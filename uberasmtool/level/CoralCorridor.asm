;Coral Corridor ASM (level 1C).
;Includes:
;load: SMWC coin trigger initializer
;init: wave HDMA init and processing, 2-channel HDMA gradient
;main: wave HDMA processing
;nmi: custom layer 3 scrolling


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL nmi				;run nmi code during init as well

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E03			;mode 3 on $210E (a.k.a. affect layer 1 Y-wise and layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
LDX #$00			;\  Init of
LDY #$00			; | X and Y. Y will be the loop counter, X the index for writing the table to the RAM
LDA $14				; | Speed of Waves
LSR #2				; | Slowing down A
STA $00				;/  Save for later use

.Loop
LDA #$08			;\  Set scanline height
STA !hdmaparallaxtbls,x		; | for each wave
TYA				; | Transfer Y to A
ADC $00				; | Add in frame counter
AND #$07			; | only the lower half of the byte
PHY				; | Push Y, so that the loop counter isn't lost.
TAY				;/  Transfer A to Y

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1C				; | Add value from the wave table to layer y position (low byte).
STA !hdmaparallaxtbls+1,x	; | X position low byte
LDA $1D				; | Load layer y position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+2,x	;/  X position high byte

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1E				; | Add value from the wave table to layer x position (low byte).
STA !hdmaparallaxtbls+3,x	; | X position low byte
LDA $1F				; | Load layer x position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+4,x	;/  X position high byte

PLY				;\  Pull Y (original loop counter)
CPY #$1A			; | Compare with #$1C scanlines
BPL .End			; | If bigger, end HDMA
INX #5				; | Increase X five times, so that in the next loop, it writes the new table data at the end of the old one...
INY				; | Increase Y
BRA .Loop			;/  Do the loop

.End
LDA #$00			;\  End HDMA by writing
STA !hdmaparallaxtbls+5,x	; | #$00 here
RTL				;return.

nmi:
REP #$20
LDA $1466|!addr
LSR #2
CLC
ADC $1466|!addr
STA $22
SEP #$20

+
RTL				;return.

Table1:
db $03,$32,$5D
db $06,$31,$5D
db $0B,$31,$5C
db $03,$30,$5C
db $0F,$30,$5B
db $0F,$2F,$5A
db $04,$2F,$59
db $0B,$2E,$59
db $06,$2E,$58
db $07,$2D,$58
db $09,$2D,$57
db $02,$2C,$57
db $0A,$2C,$56
db $01,$2C,$55
db $04,$2B,$55
db $03,$2B,$54
db $01,$2A,$54
db $04,$2A,$53
db $02,$2A,$52
db $02,$29,$52
db $02,$29,$51
db $01,$29,$50
db $02,$28,$50
db $03,$28,$4F
db $01,$27,$4F
db $04,$27,$4E
db $02,$27,$4D
db $03,$26,$4D
db $06,$26,$4C
db $03,$26,$4B
db $08,$25,$4B
db $10,$25,$4A
db $07,$25,$49
db $06,$24,$49
db $29,$24,$48
db $00

Table2:
db $23,$94
db $40,$95
db $03,$96
db $0A,$95
db $07,$94
db $06,$93
db $07,$92
db $0B,$91
db $19,$90
db $38,$8F
db $00

Wave_Table:
db $00,$01,$02,$03,$03,$02,$01,$00