;Onsen Overhang ASM.
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: message box text DMA except for sublevel 34, 2-channel HDMA gradient, parallax X offset processing
;nmi: very slow BG V-scroll relative to layer 1
;main: parallax X offset processing, water kills the player on sublevel 4E

load:
JSL Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

LDA $1464|!addr			;load layer 1 Y-pos, next frame
STA $1C				;store to layer 1 Y-pos, current frame.
STZ $1414|!addr			;set BG Y-scroll in LM to none.
STZ $1E
STZ $1466|!addr
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax
;fallthrough
nmi:
JML OrientParallax_NMIBGUpd	;update layer 2 Y-pos manually

init:
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_HDMA		;execute HDMA

LDA $010B|!addr			;load level number, low byte
CMP #$34			;compare to value
BEQ skip			;if equal, skip ahead.

REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Pal4
JSL Messages_DMA_FillProp
LDX #$04
JSL Messages_DMA_Finish
LDX #$00
LDA.w #Messages_Onsen_1
JSL Messages_DMA
LDX #$04
LDA.w #Messages_Onsen_2
JSL Messages_DMA

LDY #$80         ;$80 = increment on high
STY $2115
LDA #$1908       ;$08 = transfer single value, DMA to: $2119 (VRAM high bytes)
STA $4320
LDA #Messages_Props_Pal5
STA $4322        ;store to source low/high byte register.
LDA.w #$58A0+$6D
STA $2116        ;store value to our VRAM destination which will be written to.
LDA #$004A
STA $4325        ;store to bytes to transfer register.
LDY #$04         ;load value (DMA on channel 2)
STY $420B        ;store to DMA channel enable register.

LDA.w #$58A0+$0400+$92
STA $2116        ;store value to our VRAM destination which will be written to.
LDA #$001F
STA $4325        ;store to bytes to transfer register.
LDY #$04         ;load value (DMA on channel 2)
STY $420B        ;store to DMA channel enable register.
SEP #$20

LDA $010B|!addr			;load level number, low byte
CMP #$14			;compare to value
BNE +				;if not equal, skip ahead.

REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
BRA ++				;branch ahead

+
REP #$20			;16-bit A
LDA.w #Table5			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table6			;load address of second table
BRA ++				;branch ahead

skip:
REP #$20			;16-bit A
LDA.w #Table3			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table4			;load address of second table
BRA ++				;branch ahead

++
STA $03				;store to scratch RAM.
LDX.b #Table1>>16		;load bank of one of the tables
STX $02				;store to scratch RAM.
STX $05				;store to scratch RAM.
REP #$20			;16-bit A
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
SEP #$20			;8-bit A

main:
LDA $010B|!addr			;load sublevel number low byte
CMP #$4E			;compare to value
BNE +				;if not equal, skip ahead.

LDA $75				;load player is in water flag
BEQ +				;if not in water, skip ahead.

STZ $19				;remove player's powerup
JSL $00F5B7|!bank		;hurt the player (and it's a death).

+
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax
JML OrientParallax_Logic	;handle layer 2 parallax logic and return.


Table1:
db $08,$2B,$42
db $02,$2C,$42
db $04,$2D,$43
db $03,$2E,$43
db $05,$2E,$44
db $03,$2F,$44
db $05,$2F,$45
db $03,$30,$45
db $04,$30,$46
db $04,$31,$46
db $04,$31,$47
db $05,$32,$47
db $03,$32,$48
db $05,$33,$48
db $02,$33,$49
db $07,$34,$49
db $01,$34,$4A
db $07,$35,$4A
db $06,$36,$4B
db $02,$36,$4C
db $04,$37,$4C
db $03,$37,$4D
db $03,$38,$4D
db $05,$38,$4E
db $06,$39,$4F
db $01,$39,$50
db $05,$3A,$50
db $02,$3A,$51
db $04,$3B,$51
db $04,$3B,$52
db $02,$3C,$52
db $05,$3C,$53
db $01,$3D,$53
db $06,$3D,$54
db $06,$3E,$55
db $02,$3E,$56
db $03,$3F,$56
db $4F,$3F,$57
db $00

Table2:
db $07,$8C
db $0D,$8D
db $0B,$8C
db $0C,$8B
db $0D,$8A
db $0E,$89
db $0D,$88
db $0B,$87
db $0C,$86
db $0C,$85
db $0B,$84
db $0C,$83
db $53,$82
db $00

Table3:
db $04,$24,$43
db $08,$25,$43
db $05,$26,$43
db $03,$26,$42
db $08,$27,$42
db $08,$28,$42
db $08,$29,$42
db $08,$2A,$42
db $04,$2B,$42
db $03,$2C,$42
db $05,$2D,$43
db $04,$2E,$43
db $04,$2E,$44
db $06,$2F,$44
db $03,$2F,$45
db $07,$30,$45
db $02,$30,$46
db $08,$31,$46
db $01,$31,$47
db $08,$32,$47
db $01,$33,$47
db $08,$33,$48
db $03,$34,$48
db $06,$34,$49
db $04,$35,$49
db $04,$35,$4A
db $07,$36,$4A
db $02,$36,$4B
db $52,$37,$4B
db $00

Table4:
db $19,$8B
db $1E,$8C
db $0D,$8D
db $0B,$8C
db $0B,$8B
db $0D,$8A
db $0D,$89
db $0E,$88
db $0F,$87
db $4F,$86
db $00

Table5:
db $0D,$20,$40
db $02,$20,$41
db $17,$21,$41
db $07,$21,$42
db $13,$22,$42
db $0A,$22,$43
db $0F,$23,$43
db $0F,$23,$44
db $0B,$24,$44
db $12,$24,$45
db $08,$25,$45
db $16,$25,$46
db $3D,$26,$46
db $00

Table6:
db $0D,$82
db $0E,$83
db $0F,$84
db $0E,$85
db $0E,$86
db $0F,$87
db $0E,$88
db $0F,$89
db $0E,$8A
db $0E,$8B
db $0F,$8C
db $43,$8D
db $00