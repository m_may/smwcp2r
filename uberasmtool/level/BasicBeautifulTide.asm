;This ASM file includes wave effect for the bottom of layer 1 and proper translucency for layer 3.

!waveindex = $0F5E|!addr		;index for wave scroll

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond