;Asteroid Antics ASM (level 150).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Pal2
JSL Messages_DMA_FillProp
LDX #$04
JSL Messages_DMA_Finish
LDX #$00
LDA.w #Messages_Asteroid_3
JSL Messages_DMA
LDX #$04
LDA.w #Messages_Asteroid_4
JSL Messages_DMA
SEP #$20
RTL
