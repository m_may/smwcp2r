;Floral Fears/Briar Mire ASM.
;Includes:
;init: translucent layer 3 handler
;main: spawn cluster leaves, kill player when water is touched

init:
LDA #$15		;layer 1, layer 3 and sprites on main screen
STA $212C
STA $212E
LDA #$13		;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D
STA $212F
RTL

main:
LDA $75			;load player is in water flag
BEQ +			;if not, skip ahead.

STZ $19			;remove player's powerup.
JSL $00F5B7|!bank	;hurt the player

+
LDA #$0D		;load sprite number to spawn
JML clusterspawn_run	;spawn cluster leafy leaves