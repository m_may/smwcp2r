;Rusted Retribution ASM (level 1DB).
;Includes:
;init: message box text DMA
;main: move boos downwards is player is spinjumping on a certain X-pos continuously (anti-softlock measure)

init:
REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Normal
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_Rusted1DB
JSL Messages_DMA
SEP #$20
RTL

main:
LDA $9D
ORA $13D4
BNE ret

LDA $72
BEQ NotInAir

LDA $140D|!addr
BNE CheckRange

NotInAir:
STZ $0F5E|!addr

ret:
RTL

CheckRange:
REP #$20
LDA $94
CMP #$065D
BCC .notinrange
CMP #$0663
BCS .notinrange
SEP #$20

LDA $0F5E|!addr
CMP #$FF
BEQ .moveboosdown

INC $0F5E|!addr
RTL

.notinrange
SEP #$20
RTL

.moveboosdown
LDA $14
AND #$03
BNE ret

LDX #$0B

.sprloop
LDA $14C8,x
CMP #$08
BNE .redoloop

LDA $9E,x
CMP #$37
BEQ .movedn
CMP #$28
BNE .redoloop

.movedn
LDA $D8,x
CLC
ADC #$01
STA $D8,x
LDA $14D4,x
ADC #$00
STA $14D4,x

.redoloop
DEX
BPL .sprloop
RTL
