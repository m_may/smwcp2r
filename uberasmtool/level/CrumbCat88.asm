;Crumbling Catacombs ASM (level 88).
;Includes:
;main: map16 specific shattering effects, layer 2 falls

main:
	LDA $71
	ORA $9D
	BNE Return88
	LDA $13D4
	BEQ RunCode_88
	Return88:
	RTL

	RunCode_88:
	LDA $18C5
	BNE SkipTrigger88
	REP #$20
	LDA $D1
	CMP #$0A30
	BCC SkipTrigger88
	SEP #$20
	LDA #$01
	STA $18C5
	REP #$20
	LDA #$0A00
	STA $9A
	LDA #$0170
	STA $98
	SEP #$20
	Loop_88:
	JSL Shatter101_main
	LDA $9A
	CLC
	ADC #$50
	STA $9A
	CMP #$B0
	BCC Loop_88

	SkipTrigger88:
	SEP #$20
	LDA $18C5
	CMP #$02
	BEQ D2_88
	CMP #$03
	BEQ D3_88
	CMP #$04
	BEQ Mid88
	CMP #$00
	BEQ Mid88
	REP #$20
	LDA $1468
	CMP #$003E
	BCC SD2_88
	SEC
	SBC #$0003
	STA $1468
	STA $60
	SEP #$20
	BRA Mid88
	D2_88:
	REP #$20
	LDA $1468
	CMP #$0045
	BCS SD3_88
	INC $1468
	INC $1468
	STA $60
	SEP #$20
	BRA Mid88
	SD2_88:
	SEP #$20
	LDA #$09
	STA $1DFC
	LDA #$1A
	STA $1887
	LDA #$02
	STA $18C5
	BRA Mid88
	D3_88:
	REP #$20
	LDA $1468
	CMP #$003E
	BCC SD4_88
	DEC $1468
	STA $60
	SEP #$20
	BRA Mid88
	SD3_88:
	SEP #$20
	LDA #$03
	STA $18C5
	BRA Mid88
	SD4_88:
	SEP #$20
	LDA #$04
	STA $18C5

	Mid88:
	JML Shatter101_Mid88