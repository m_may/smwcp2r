;Stone Sea Sanctuary ASM (level 1F).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient, translucent layer 3 over layers 1, 2 and sprites
;init and main: layer 1 waves processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #StoneSeaGeneric_Table1>>16	;load bank of one of the tables
STA $02					;store to scratch RAM.
STA $05					;store to scratch RAM.
REP #$20				;16-bit A
LDA.w #StoneSeaGeneric_Table1		;load address of first table
STA $00					;store to scratch RAM.
LDA.w #StoneSeaGeneric_Table2		;load address of second table
STA $03					;store to scratch RAM.
JSL HDMA_Color2				;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
JML StoneSeaGeneric_mainwave		;process waves for HDMA and return.