;Asteroid Antics ASM (level 153).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
JSL Messages_DMA_DefaultPropFill
LDX #$00
LDA.w #Messages_Asteroid_1
JSL Messages_DMA
LDX #$04
LDA.w #Messages_Asteroid_2
JSL Messages_DMA
SEP #$20
RTL
