;Scorching Sepulcher ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: message box text DMA
;init/main: parallax HDMA init/processing
;main: heat RAM handler, eerie generator when on/off switch status is off


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL ProcOffsets			;process offsets for use before we set the effect on

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return

main:
JSR ActualGimmicks		;run gimmicks for level

ProcOffsets:
REP #$20			;16-bit A
LDA $1E				;load layer 2 X-pos current frame
STA !hdmaparallaxtbls+34	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $20				;load layer 2 Y-pos, current frame
SEC				;set carry
SBC #$0060			;subtract value
STA $00				;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $00				;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $00				;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $00				;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $00				;load layer 2 Y-pos
CLC				;clear carry
ADC #$0060			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
RTL				;return.

Tbl1:
db $7F,$3D,$01
Tbl1End:

ActualGimmicks:
LDA $14AF
BNE +

STZ $1473			;lolwhat, just this (we'd better use a decrementer but meh)
RTS

+
LDA $9D
ORA $13D4
BNE +

		LDA $14    
                    AND #$7F
                    ORA $9D   
                    BNE +
                    JSL $82A9DE
                    BMI +
                    TYX        
                    
                    LDA #$01                ; store sprite status
                    STA $14C8,x
                    
              LDA #$38      ; store sprite number
                    STA $9E,x
                    JSL $87F7D2

JSL $81ACF9
                    AND #$7F   
                    ADC #$40   
                    ADC $1C    
                    STA $D8,x  
                    LDA $1D    
                    ADC #$00   
                    STA $14D4,x
                    LDA $148E  
                    AND #$01   
                    TAY        
                    LDA TBL_B2D0,y
                    CLC        
                    ADC $1A    
                    STA $E4,x  
                    LDA $1B    
                    ADC TBL_B2D2,y
                    STA $14E0,x
                    LDA TBL_B2D4,y
                    STA $B6,x

+
RTS

TBL_B2D0:            db $F0,$FF
TBL_B2D2:            db $FF,$00
TBL_B2D4:            db $10,$F0

MsgDMA1:
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$13,$3D,$51,$3D,$40,$3D,$4F,$3D,$4F,$3D,$44,$3D,$43,$3D
db $FE,$3C,$48,$3D,$4D,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C
db $47,$3D,$44,$3D,$40,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$00,$3D,$51,$3D,$44,$3D,$FE,$3C,$52,$3D
db $44,$3D,$51,$3D,$55,$3D,$40,$3D,$4D,$3D,$53,$3D,$52,$3D,$FE,$3C
db $4E,$3D,$45,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C,$4F,$3D
db $58,$3D,$51,$3D,$40,$3D,$4C,$3D,$48,$3D,$43,$3D,$1B,$3D,$FE,$3C
db $FE,$3C,$13,$3D,$47,$3D,$4E,$3D,$52,$3D,$44,$3D,$FE,$3C,$46,$3D
db $51,$3D,$40,$3D,$41,$3D,$41,$3D,$44,$3D,$43,$3D,$FE,$3C,$4A,$3D
db $4D,$3D,$4E,$3D,$56,$3D,$FE,$3C,$43,$3D,$44,$3D,$45,$3D,$44,$3D
db $40,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$12,$3D,$56,$3D,$44,$3D,$4B,$3D,$53,$3D
db $44,$3D,$51,$3D,$48,$3D,$4D,$3D,$46,$3D,$FE,$3C,$47,$3D,$44,$3D
db $40,$3D,$53,$3D,$FE,$3C,$40,$3D,$4D,$3D,$43,$3D,$FE,$3C,$43,$3D
db $44,$3D,$40,$3D,$53,$3D,$47,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$40,$3D,$4C,$3D,$48,$3D,$43,$3D,$1B,$3D
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA2:
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$16,$3D,$47,$3D,$44,$3D,$4D,$3D,$FE,$3C,$53,$3D,$47,$3D
db $44,$3D,$FE,$3C,$4C,$3D,$4E,$3D,$4E,$3D,$4D,$3D,$FE,$3C,$40,$3D
db $4F,$3D,$4F,$3D,$44,$3D,$40,$3D,$51,$3D,$52,$3D,$1D,$3D,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$16,$3D,$47,$3D,$44,$3D,$4D,$3D,$FE,$3C
db $52,$3D,$54,$3D,$4D,$3D,$FE,$3C,$4B,$3D,$44,$3D,$40,$3D,$55,$3D
db $44,$3D,$52,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C,$43,$3D
db $40,$3D,$58,$3D,$1B,$3D,$1B,$3D,$1B,$3D,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$18,$3D,$4E,$3D,$54,$3D,$51,$3D,$FE,$3C,$44,$3D,$44,$3D
db $51,$3D,$48,$3D,$44,$3D,$52,$3D,$53,$3D,$FE,$3C,$45,$3D,$44,$3D
db $40,$3D,$51,$3D,$52,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$16,$3D,$48,$3D,$4B,$3D,$4B,$3D,$FE,$3C
db $42,$3D,$4E,$3D,$4C,$3D,$44,$3D,$FE,$3C,$4E,$3D,$54,$3D,$53,$3D
db $FE,$3C,$53,$3D,$4E,$3D,$FE,$3C,$4F,$3D,$4B,$3D,$40,$3D,$58,$3D
db $1B,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C