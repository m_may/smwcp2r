;Captain Hindenbird Arena ASM.
;note: it doesn't work.

!FreeRamForMode2 = $7F9D00	;what's this for exactly? haven't figured it out yet. Will have to change this to actual free RAM due to DynamicZ.
!FGTilemapRAM = $706000		;RAM area to write our layer 1 tilemap (to save us some ROM). it's SRAM because $1000 bytes are required.
!AFlag = $18C9|!addr		;some free RAM

load:
PHB			;preserve data bank

LDA #$02		;load value
STA $2105		;store to mode register (enable mode 2).

REP #$30		;16-bit AXY

LDA #$10F8		;load tile number and properties
LDX #$0000		;load value into X

.tileloop
STA.l !FGTilemapRAM,x	;store to RAM.
INX #2			;increment X twice
CPX #$1000		;compare X to value
BNE .tileloop		;if not equal, keep looping.

LDA #$05FF					;load amount of bytes to transfer
LDX.w #BlimpL1					;load absolute address value of data to be copied into X
LDY.w #!FGTilemapRAM+$840			;load absolute address value of destination for copied data into Y
MVN !FGTilemapRAM>>16, (BlimpL1>>16)|!bank8	;copy over data from BlimpL1 bank to !FGTilemapRAM bank.

SEP #$30		;8-bit AXY

PLB			;restore data bank
RTL			;return.

init:
STZ $4200
LDA #$80
STA $2100

REP #$20			;16-bit A
LDA #$1000			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #!FGTilemapRAM>>16	;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #!FGTilemapRAM		;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$3000			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after that)

REP #$20			;16-bit A
LDA #$0700			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #BlimpL2_Table>>16	;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #BlimpL2_Table		;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$3800			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after that)
REP #$20			;16-bit A
LDA #$3C00			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after that)

RTL			;return.

main:
LDA $71			;load player animation pointer
ORA $9D			;OR with sprites/animations locked flag
ORA $13D4		;OR with pause flag
BEQ Process		;if nothing is set, process the stuff.
RTL			;return.

Process:
STZ $1697|!addr		;reset consecutive enemies stomped.

LDA $15			;load controller data 1
AND #$D0		;preserve A/B, Y/X and start bits
BNE SkipRNG		;if any of those buttons are pressed, skip RNG.

JSL $01ACF9|!bank	;RNG

SkipRNG:
LDA !AFlag		;load address
BEQ .noWaveStuffJump	;if zero, skip wave stuff jump.
LDX #$00		;load value into X
BRA .waveLoop		;branch ahead

.noWaveStuffJump
JMP .noWaveStuff	;jump (branch was out of bounds, correct?)

.waveLoop
REP #$20		;16-bit A
AND #$00FF		;preserve low byte only
STA $03			;store to scratch RAM.
ASL #3			;multiply by 2 three times
STA $01			;store to scratch RAM.
LDA $03			;load value from scratch RAM
SEC			;set carry
SBC #$00FF		;subtract value
SEP #$20		;8-bit A
STA $00			;store to scratch RAM.

JSR SIN			;call sine routine

STZ $0A			;reset scratch RAM - $0A indicates if this number was negative or not.

REP #$20
LDA $03			;load value scratch RAM
BPL +			;if positive, branch.
EOR #$FFFF		;invert all bits
INC			;increment by one - get correct inversed value
INC $0A			;increment address by one

+
LSR			;divide A by 2

STA $00			;store to scratch RAM.
TXA			;transfer X to A
AND #$00FF		;preserve low byte only
CLC			;clear carry
ADC !AFlag		;add value from address
ASL #4			;multiply by 2 four times		
CLC			;clear carry
ADC #$0080		;add value
STA $01			;store to scratch RAM.
SEP #$20		;8-bit A
JSR SIN			;call sine routine

;just feel like commenting that I don't have any fucking idea of what's going on here...

REP #$20		;16-bit A
LDA $03			;load value from scratch RAM
LDY $0A			;load value from scratch RAM into Y
BNE .isNegative		;branch if it's negative.
REP #$20		;16-bit A (again? why?)
CLC			;clear carry
ADC #$2065		;add value
BRA +			;branch ahead

.isNegative
LDA #$2065		;load value
SEC			;set carry
SBC $03			;subtract value

+
STA $00			;store to scratch RAM.

PHX			;Preserve x; we need it to index a table.
TXA			;transfer X to A
ASL			;multiply by 2
CLC			;clear carry
ADC #$0040		;add value
TAX			;transfer A to X
LDA $00			;load value from scratch RAM
CMP #$2000		;compare to value
BCS +			;if higher or equal, branch.
LDA #$2000		;else load value

+
STA !FreeRamForMode2,x	;store to RAM table according to index.
PLX			;restore X
INX			;increment X by one
CPX #$20		;compare X to value
BNE .waveLoop		;if not equal, keep looping.
;LDA $14
;AND #$01
;BEQ +
SEP #$30		;8-bit AXY

;LDA $13D4
;BNE .noDecreaseTimer	;we already have a pause check before this code so yeah
DEC !AFlag		;decrement address by one

;at this point I didn't bother commenting on anymore - I am not rewriting this stuff nor understanding a fuck about it so yeah
;code is ugly but man, go easy on me, 2 years developing smwcp2, I need no stress - if it works and inserts it's alright
;that's why I optimized other stuff... so those bad but complex ones could pass

.noDecreaseTimer
;STA $7FFFE0	;this is a breakpoint

	LDA $0F6B	; Don't affect the player if he's flying.
	BEQ +
	RTS
	+
	REP #$30
	LDA $D3
	SEC
	SBC #$008B
	STA $04		; $04 contains the player's y coord (feet-based) "relative" to the blimp's position
	
	LDA $D1
	LSR #3
	ASL
	TAX
	LDA !FreeRamForMode2+$40,x
	AND #$0FFF
	STA $00
	LDA !FreeRamForMode2+$42,x
	AND #$0FFF
	CMP $00
	BCS +
	LDA $00
	+

	SEC
	SBC #$0065	; We have to flip this over 65; tiles are stored upside down (a large y is higher up on the screen than a lower y)
	EOR #$FFFF
	INC A
	CLC
	ADC #$0065
	
	STA $00		; $00 holds the y position of the highest blimp "piece" that the player is touching / over.
	CMP $04
	SEP #$30
	BEQ .return
	BPL .belowPlayer
			; The highest piece is above the player.  Launch the player upwards.
	REP #$20
	LDA $00	
	CLC
	ADC #$008B
	STA $96
	LDA $00
	SEC
	SBC $04
	AND #$00FF
	SEP #$20
	ASL #4
	ORA #$80

	CMP $7D
	BPL +
	STA $7D
	+
	RTS
	
	
.belowPlayer
			; The highest piece is below the player.  Make the player fall.
	LDA #$01
	STA $185C
	
;+
.return
	RTS			; Skip the code where we level the playing field.


.noWaveStuff
	STZ $185C
	
	REP #$20	; \
	LDA $96		; |
	BMI +		; |
	CMP #$00F0	; |
	BCC +		; | Don't let the player fall off the screen.
	LDA #$00F0	; |
	STA $96		; |
	+		; /
	
	LDX #$40

.loop2    
	CPX #$80
	BEQ .done2
	LDA #$2065
	STA !FreeRamForMode2,x
	INX
	INX
	BRA .loop2
.done2


	
SEP #$20
LDA $77
AND #$04
BEQ .sprites

REP #$30
LDA $D1

LSR
LSR
DEC A
DEC A
DEC A
AND #$FFFE
TAX


LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x

;;INC A
;;STA !FreeRamForMode2+$40

.sprites
SEP #$30


LDY #$FF
.loop
INY
CPY #$0C
BEQ .noadjust

LDA $14C8,y
BEQ .loop
LDA $1588,y
AND #$04
BEQ .loop

LDA $14E0,y
XBA
LDA $00E4,y
REP #$30
;SEC
;SBC #$0008
LSR
LSR
DEC A
DEC A
DEC A
AND #$FFFE
TAX


LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
SEP #$30
BRA .loop

.noadjust
	SEP #$10
	REP #$20
	LDX #$40

.loop3 
	CPX #$80
	BEQ .done3
	LDA !FreeRamForMode2,x
	CMP #$2061
	BCS $07
	LDA #$2061
	STA !FreeRamForMode2,x
	INX #2
	BRA .loop3
.done3

SEP #$30

RTL			;return.

;sine routine
;$00 = radius
;$01 - $02 = angle
;$03 - $04 = result

SIN:		PHP
		PHX

		REP #$30		;16bit
		LDA $01
		AND #$00FF
		ASL A
		TAX				
		LDA $07F7DB,x
		STA $03			

		SEP #$30		;8bit	
		LDA $02			;$02
		PHA
		LDA $03			;|sin|
		STA $4202		;
		LDA $00			;
		LDX $04			;|sin| = 1.00
		BNE IF1_SIN
		STA $4203		;
		ASL $4216		;
		LDA $4217		
		ADC #$00
IF1_SIN:	LSR $02			;
		BCC IF_SIN_PLUS

		EOR #$FF
		INC A
		STA $03
		BEQ IF0_SIN
		LDA #$FF
		STA $04
		BRA END_SIN

IF_SIN_PLUS:	STA $03
IF0_SIN:	STZ $04

END_SIN:	PLA
		STA $02			;$02
		PLX
		PLP
		RTS