;Digsite Dangers ASM (all outdoor sublevels).
;Includes:
;load: SMWC coin custom trigger init
;init/main: parallax HDMA init/processing
;init: message box text DMA (complete)
;main: spawn cluster sandstorm, conditional message DMA per screen

!MessageLoadingZone = $01D0

!DMAFlags = $0F5E|!addr			;dma flags

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$02
STA !DMAFlags			;reset dma update flags
JSL ProcOffsets			;process offsets for use before we set the effect on

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

REP #$20
JSL Messages_DMA_DefaultPropFill
LDA.w #Messages_Tile_Opaque
JSL Messages_DMA_FillTileVertMargins
SEP #$20

REP #$20			;16-bit A
LDA $010B   ;\
CMP #$019F  ;|
BEQ +       ;| if the sublevel is 19F, shake the screen and upload messages 5-6
SEP #$20
RTL

+
LDX #$B0
STX $1887

LDA.w #Messages_Digsite19F_1	;load absolute address of text tilemap table 1
LDX #$02
JSL Messages_DMA		;execute DMA
LDA.w #Messages_Digsite19F_2	;load absolute address of text tilemap table 2
LDX #$06

SetDest:
JSL Messages_DMA		;execute DMA and return
SEP #$20
RTL

main:
LDA $010B
CMP #$02
BNE +

JSL DMAProc			;process messages dma before anything else (please don't break my hdma, pleaseeeeee...)

+
LDA #$0B			;load sprite number to spawn
JSL clusterspawn_run		;spawn cluster sandstorm

ProcOffsets:
REP #$20			;16-bit A
LDA $1E				;load layer 2 X-pos current frame
STA !hdmaparallaxtbls+34	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $20				;load layer 2 Y-pos, current frame
SEC				;set carry
SBC #$0060			;subtract value
STA $0F60			;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $0F60			;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $0F60			;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $0F60			;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $0F60			;load layer 2 Y-pos
CLC				;clear carry
ADC #$0060			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
-

LDA $010B
CMP #$A2
BEQ +

LDA $9D
ORA $13D4
BNE +

LDA $14
AND #$03
BNE +

LDA $1887
BEQ +

LDA #$1A
STA $1DFC

+
RTL				;return.

Tbl1:
db $7F,$3D,$01
Tbl1End:

DMAProc:
LDA $14
AND #$03
ORA $9D
ORA $13D4
BNE -				;we won't do anything in these conditions

REP #$20			;16-bit A
LDA $94				;load player's X-pos within the level
CMP.w #!MessageLoadingZone	;compare to value
BCC ClearFlags			;if lower, clear flags, update to messages 1 and 2.

SEP #$20			;8-bit A
LDA !DMAFlags			;load flags prior checks
CMP #$02			;compare to value
BEQ AllSet			;if equal, return. messages were already set.

INC !DMAFlags			;increment flags by one
CMP #$01			;compare to value
BEQ three			;if set, update to message 3.

REP #$20			;16-bit A
LDA.w #Messages_Digsite102_4	;load absolute address of text tilemap table 4
BRA le2cont			;branch

three:
REP #$20			;16-bit A
LDA.w #Messages_Digsite102_3	;load absolute address of text tilemap table 3
BRA le1cont			;branch

ClearFlags:
SEP #$20			;8-bit A
LDA !DMAFlags			;load flags prior checks
BEQ AllSet			;if equal zero, return. messages were already set.

DEC !DMAFlags			;decrement flags by one
BEQ one				;if equal zero, update to message 1.

REP #$20			;16-bit A
LDA.w #Messages_Digsite102_2	;load absolute address of text tilemap table 2

le2cont:
LDX #$06
JMP SetDest			;jump - set destination and execute dma

one:
REP #$20			;16-bit A
LDA.w #Messages_Digsite102_1	;load absolute address of text tilemap table 1

le1cont:
LDX #$02
JMP SetDest			;jump - set destination and execute dma

AllSet:
RTL				;return.
