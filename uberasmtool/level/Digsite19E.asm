;Digsite Dangers ASM (level 19E).
;Includes:
;load: SMWC coin custom trigger init
;init: layer 2 RAM initializer
;main: layer 2 rises (19E)/falls (1A1) on custom trigger 0 active, violent quicksand handling

!L2Timer = $0F5E|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL quicksink_prephdma		;prepare HDMA effects and stuff for level from library code
JSL HDMA_Color3			;execute HDMA (A goes 8-bit after this routine)

STZ !L2Timer			;reset ram address
return:
RTL				;return.

main:
JSL quicksink_run		;run violent quicksand code

LDA $9D				;load sprites/animation locked flag
ORA $13D4|!addr			;OR with game paused flag
BNE return			;if any of them are set, return.

REP #$20			;16-bit A
LDA $1468|!addr			;load layer 2 Y-pos next frame
STA $24				;store to layer 3 Y-pos.
SEP #$20			;8-bit A

LDA $7FC0FC			;load custom trigger address 1
AND #$01			;check if bit 0 is set
BEQ return			;if not, return.

LDA !L2Timer			;load layer 2 timer
CMP #$FF			;compare to value
BEQ .movel2			;if equal, just move layer 2 and stop incrementing address.

INC !L2Timer			;increment layer 2 timer by one

LDA !L2Timer			;load layer 2 timer again
CMP #$E0			;compare to value
BCS .movel2slow			;if higher, move layer 2 upwards slowly.
CMP #$D8			;compare to value
BEQ .shangtsung			;if higher, change music and start layer 2 scrolling.
CMP #$80			;compare to value
BCS .shake			;if higher, shake layer 2.
CMP #$10			;compare to value
BCS .nomusic			;if higher, fade out music.

.nomove
STZ $13D4|!addr			;force game to be unpaused (to prevent softlocks).
STZ $15
STZ $16
STZ $17
STZ $18				;player can't move during animation phases.
RTL				;return.

.nomusic
LDA #$FF			;load value
STA $1DFB|!addr			;store to music address to make it fade out.
BRA .nomove			;branch to block player movement

.shake
LDA $14				;load effective frame counter
LSR				;divide by 2
AND #$01			;preserve bit 0 only
ASL				;multiply by 2
TAX				;transfer to X
REP #$20			;16-bit A
LDA $010B|!addr			;load sublevel number
CMP #$01A1			;compare to value
BNE +				;if not equal, make layer 2 shake at position zero.

LDA ShakeTbl2,x			;load value from table according to index
BRA ++				;branch ahead

+
LDA ShakeTbl,x			;load value from table according to index

++
STA $1468|!addr			;store to layer 2 Y-pos, next frame.
SEP #$20			;8-bit A
BRA .nomove			;branch to block player movement

.shangtsung
LDA #$71			;load music to play
STA $1DFB|!addr			;store to address to play it.
BRA .nomove			;branch to block player movement

.movel2slow:
LDA $14				;load effective frame counter
AND #$03			;preserve bits 0-3
BEQ +				;if none are set, branch.
RTL				;return.

.movel2:
LDA $14				;load effective frame counter
AND #$01			;preserve bits 0-2
BEQ +				;if none are set, branch.
RTL				;return.

+
REP #$20			;16-bit A
LDA $010B|!addr			;load sublevel number
CMP #$01A1			;compare to value
BNE +				;if not equal, make layer 2 go up.

DEC $1468|!addr			;decrement layer 2 position by one
BRA ++				;branch ahead

+
INC $1468|!addr			;increment layer 2 position by one

++
SEP #$20			;8-bit A

LDA $14				;load effective frame counter
AND #$0F			;preserve bits 0-4
BNE +				;if none are set, branch.

LDA #$1A			;load SFX value
STA $1DFC|!addr			;store to address to play it.
+
RTL				;return.


ShakeTbl:
dw $0000,$0001

ShakeTbl2:
dw $0600,$0601