;Hydrostatic Halt ASM (levels 1B and 59).
;Includes:
;load: SMWC coin custom trigger init
;init: wave HDMA init and processing, message box DMA
;main: wave HDMA processing, "water pressure" gimmick


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20
LDA #$0E03			;mode 3 on $210E (a.k.a. affect layer 1 Y-wise and layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

main:
	LDA $77
	AND #$04
	BNE Returnll1
	LDA #$80
	TRB $15
	TRB $16
	TRB $17
	TRB $18
	LDA #$08
	TRB $15
	LDA $14
	AND #$01
	BNE Returnll1
	LDA #$08
	TSB $15

Returnll1:
LDX #$00			;\  Init of
LDY #$00			; | X and Y. Y will be the loop counter, X the index for writing the table to the RAM
LDA $14				; | Speed of Waves
LSR #2				; | Slowing down A
STA $00				;/  Save for later use

.Loop
LDA #$08			;\  Set scanline height
STA !hdmaparallaxtbls,x		; | for each wave
TYA				; | Transfer Y to A
ADC $00				; | Add in frame counter
AND #$07			; | only the lower half of the byte
PHY				; | Push Y, so that the loop counter isn't lost.
TAY				;/  Transfer A to Y

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1C				; | Add value from the wave table to layer y position (low byte).
STA !hdmaparallaxtbls+1,x	; | X position low byte
LDA $1D				; | Load layer y position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+2,x	;/  X position high byte

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1E				; | Add value from the wave table to layer x position (low byte).
STA !hdmaparallaxtbls+3,x	; | X position low byte
LDA $1F				; | Load layer x position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+4,x	;/  X position high byte

PLY				;\  Pull Y (original loop counter)
CPY #$1A			; | Compare with #$1C scanlines
BPL .End			; | If bigger, end HDMA
INX #5				; | Increase X five times, so that in the next loop, it writes the new table data at the end of the old one...
INY				; | Increase Y
BRA .Loop			;/  Do the loop

.End
LDA #$00			;\  End HDMA by writing
STA !hdmaparallaxtbls+5,x	; | #$00 here
RTL				;return.

Wave_Table:
db $00,$01,$02,$03,$03,$02,$01,$00

MsgDMA1:
db $FE,$38,$16,$39,$47,$39,$4E,$39,$40,$39,$1A,$39,$FE,$38,$13,$39
db $47,$39,$44,$39,$FE,$B8,$4F,$39,$51,$39,$44,$39,$52,$39,$52,$39
db $54,$39,$51,$39,$44,$39,$FE,$38,$43,$39,$4E,$39,$56,$39,$4D,$39
db $FE,$38,$47,$39,$44,$39,$51,$39,$44,$39,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$52,$39,$54,$39,$51,$39,$44,$39,$FE,$38,$48,$39,$52,$39
db $FE,$38,$48,$39,$4D,$39,$53,$39,$44,$39,$4D,$39,$52,$39,$44,$39
db $1A,$39,$FE,$B8,$18,$39,$4E,$39,$54,$39,$FE,$B8,$4C,$39,$40,$39
db $58,$39,$FE,$B8,$4D,$39,$4E,$39,$53,$39,$FE,$B8,$FE,$38,$FE,$38
db $FE,$38,$41,$39,$44,$39,$FE,$B8,$40,$39,$41,$39,$4B,$39,$44,$39
db $FE,$38,$53,$39,$4E,$39,$FE,$B8,$52,$39,$56,$39,$48,$39,$4C,$39
db $FE,$38,$55,$39,$44,$39,$51,$39,$58,$39,$FE,$B8,$45,$39,$40,$39
db $51,$39,$FE,$B8,$FE,$38,$FE,$38,$FE,$38,$FE,$B8,$FE,$38,$FE,$38
db $FE,$38,$56,$39,$48,$39,$53,$39,$47,$39,$4E,$39,$54,$39,$53,$39
db $FE,$38,$53,$39,$47,$39,$44,$39,$FE,$38,$40,$3D,$48,$3D,$43,$3D
db $FE,$38,$4E,$3D,$45,$3D,$FE,$38,$52,$3D,$4E,$3D,$4C,$3D,$44,$3D
db $53,$3D,$47,$3D,$48,$3D,$4D,$3D,$46,$3D,$FE,$B8,$FE,$38,$FE,$38
db $FE,$38,$4C,$3D,$4E,$3D,$51,$3D,$44,$3D,$FE,$38,$41,$3D,$54,$3D
db $4E,$3D,$58,$3D,$40,$3D,$4D,$3D,$53,$3D,$1B,$39,$FE,$B8,$FE,$38
db $FE,$38,$FE,$38,$FE,$B8,$FE,$38,$FE,$38,$FE,$B8,$FE,$38,$FE,$38
db $FE,$38,$FE,$B8,$FE,$38,$FE,$38,$FE,$38,$FE,$B8,$FE,$38,$FE,$38