;Nexus Norvegicus ASM (level 132, 142, 143)
;Includes:
;load: SMWC coin custom trigger init
;init: 1-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JML NexusHDMA_init
