;Opulent Oasis ASM (levels 103 and 145 - parallax version).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 over layers 1, 2 and sprites, waves HDMA init/processing, layer 2 "water tide" interaction
;init/main: waves HDMA init/processing
;init/nmi: parallax HDMA init/processing

!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

JSL ProcOffsets			;process offsets for use before we set the effect on
JSR mainwave			;execute wave processing

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
LDA.w #!hdmaparallaxtbls+80	;address of table
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
STA $4364
LDA #$60			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channels 5 and 6

JSL InvisiL2_Water2		;make part of layer 2 BG act like water as if there was a layer 3 tide, but without current
RTL				;return

main:
JSR mainwave			;execute wave processing
RTL

ProcOffsets:
nmi:
REP #$20			;16-bit A
LDA $1466|!addr			;load layer 2 X-pos next frame
STA !hdmaparallaxtbls+34	;store to HDMA table.
STA !hdmaparallaxtbls+36	;store to HDMA table.
STA !hdmaparallaxtbls+38	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $1468|!addr			;load layer 2 Y-pos, next frame
STA $0F5F|!addr			;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $0F5F|!addr			;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $0F5F|!addr			;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $0F5F|!addr			;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $20				;load layer 2 Y-pos
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
RTL				;return.

Tbl1:
db $7F,$1D,$7F,$7F
Tbl1End:

mainwave:
LDA $71
CMP #$0B
BEQ .keeprunning
ORA $9D
ORA $13D4|!addr
BNE +

.keeprunning
LDA $14
LSR #3
AND #$07
STA !waveindex

LDA #$7F		;load scanline count
SEC			;set carry
SBC $1C			;subtract layer 1 Y-pos, current frame, low byte, from it
clc
adc $1888
STA $0F5F|!addr		;store to scratch RAM.
BMI zeroone

LDA #$80
BRA st

zeroone:
LDA.b #$FF
SEC
SBC $1C
clc
adc $1888

st:
STA $0F60|!addr

+
REP #$20
LDA !waveindex
ASL
TAY

STZ $0F61|!addr
LDX #$00		;starting index for loop

waveloop:
PHY

CPX #$00		;first entry of table varies depending on layer 1 Y-pos
BEQ first

SEP #$20
LDA #$08		;default scanline count value

LDY $0F61|!addr
BNE +
CPX #$03		;second entry
BNE +

second:
INC $0F61|!addr
LDA $0F60|!addr		;second scanline count value
BRA ssec

first:
SEP #$20
LDA $0F5F|!addr		;this holds the dynamic scanline count set before the loop
BMI second

ssec:
STA !hdmaparallaxtbls+80,x

REP #$20
LDA $1C
clc
adc $1888
BRA ++

+
STA !hdmaparallaxtbls+80,x
REP #$20

PLY
LDA $1C
CLC
adc $1888
ADC Index,y

PHA

INY #2
TYA
AND #$000F
TAY

PLA

PHY	;anticrash

++
STA !hdmaparallaxtbls+81,x
PLY
INX #3
CPX #$60
BNE waveloop

SEP #$20

LDA #$00
STA !hdmaparallaxtbls+80,x		;finalize table
RTS

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond