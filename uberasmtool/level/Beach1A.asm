;Fringe Forest ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;init and nmi: parallax HDMA
;main: parallax logic

!messageboxparallax = $7F890B		;change according to NoMesBosWin patch
!L3MsgYPos = $0108			;Y-pos (relative to screen) of the message on the layer 3 tilemap
					;(uberASM main code can't read values that change during an active message box period for some reason)

!parallax_autoscroll = $0F5E|!addr	;16-bit incrementing address for layer 3 clouds

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
REP #$20			;16-bit A
LDX #$08
LDA.w #Messages_Props_Pal5
JSL Messages_DMA_FillProp
LDX #$0C
LDA.w #Messages_Props_Pal5
JSL Messages_DMA_FillProp
LDX #$08
LDA.w #Messages_Beach1A_1
JSL Messages_DMA
LDX #$0C
LDA.w #Messages_Beach1A_2
JSL Messages_DMA
SEP #$20

LDA #$00			;load value
STA !hdmaparallaxtbls+10		;store to parallax table 1 (layer 3).
LDA #$78			;load value
STA !hdmaparallaxtbls		;store to parallax table 1 (layer 3).
STA !hdmaparallaxtbls+5		;store to parallax table 1 (layer 3).

JSL HDMA_FringeForest

nmi:
REP #$20			;kinda Variable 3 H-scroll (and also handles some parallax)
LDA !parallax_autoscroll
STA !hdmaparallaxtbls+1

LDA $1A
LSR
CLC
ADC $1A
LSR #3
STA $1E
STA $1466|!addr
STA !hdmaparallaxtbls+6

LDA $24				;use lm's default Y-pos for every part
STA !hdmaparallaxtbls+3
STA !hdmaparallaxtbls+8

LDA #$00BA			;no V-scroll (fixed Y-pos)
STA $20
STA $1468|!addr
SEP #$20

LDX $1B89|!addr			;load message box pointer
CPX #$03			;compare to value (check if on message being displayed state)
BNE .prochdma			;if not equal, use regular HDMA table.

LDA #$00			;load value
STA !messageboxparallax+15	;store to HDMA table (make it the last byte).
STA !messageboxparallax+11	;store to HDMA table.
INC				;increment A by one
STA !messageboxparallax+10	;store to HDMA table.
LDA $1426|!addr			;message index
DEC
STA !messageboxparallax+12	;store to HDMA table.

REP #$20			;16-bit A
LDA #!L3MsgYPos			;load Y-pos for layer 3 on this entry
STA !messageboxparallax+13	;store to message HDMA table.

LDA !hdmaparallaxtbls+8	;load actual value from effect HDMA table
STA !messageboxparallax+8	;copy over to message HDMA table.
LDA !hdmaparallaxtbls+6	;load actual value from effect HDMA table
STA !messageboxparallax+6	;copy over to message HDMA table.
SEP #$20			;8-bit A
LDA #$2F			;load subtracted scanline count ($A7-$78)
STA !messageboxparallax+5	;store to message HDMA table.

LDX #$04
.setuploop
LDA !hdmaparallaxtbls,x		;load value from effect table, indexed
STA !messageboxparallax,x	;store to message HDMA table, indexed.
DEX				;decrement X by one
BPL .setuploop			;loop while it's positive.

REP #$20
LDA.w #!messageboxparallax
STA $4352
SEP #$20
LDA.b #!messageboxparallax>>16
STA $4354
RTL

.prochdma
REP #$20
LDA #$1103			;mode 3 on $2111 (a.k.a. affect layer 3 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return.

main:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$07
ORA $9D
ORA $13D4|!addr
ORA $1426|!addr
ORA $1B89|!addr
BNE +

REP #$20
INC !parallax_autoscroll
SEP #$20

+
RTL			;return.
