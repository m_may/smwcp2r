;Treetop Toss-Up (sublevel 30) ASM.	
;Includes:
;load: SMWC coin custom trigger init
;init: 3-channel HDMA gradient
;nmi: custom layer 3 scrolling

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Gradient1_RedTable>>16	;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Gradient1_RedTable	;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Gradient1_GreenTable	;load address of second table
STA $03				;store to scratch RAM.
LDA.w #Gradient1_BlueTable	;load address of third table
STA $06				;store to scratch RAM.
JML HDMA_Color3			;execute HDMA (A goes 8-bit after this routine) and return.

nmi:
REP #$20
LDA $1466|!addr
LSR #2
CLC
ADC $1466|!addr
STA $22

LDA $1468|!addr
LSR #2
CLC
ADC $1468|!addr
ADC #$0100
STA $24
SEP #$20

+
RTL				;return.

Gradient1_RedTable:
db $0E,$27
db $05,$28
db $05,$29
db $04,$2A
db $04,$2B
db $05,$2C
db $13,$2D
db $0A,$2E
db $08,$2F
db $09,$30
db $19,$31
db $0B,$32
db $07,$33
db $06,$34
db $05,$35
db $05,$36
db $05,$37
db $05,$38
db $05,$39
db $05,$3A
db $05,$3B
db $06,$3C
db $07,$3D
db $0A,$3E
db $22,$3F
db $00

Gradient1_GreenTable:
db $17,$4C
db $1B,$4D
db $06,$4E
db $05,$4F
db $03,$50
db $04,$51
db $03,$52
db $03,$53
db $04,$54
db $03,$55
db $05,$56
db $07,$57
db $1B,$58
db $0C,$59
db $0A,$5A
db $09,$5B
db $0A,$5C
db $0A,$5D
db $0E,$5E
db $27,$5F
db $00

Gradient1_BlueTable:
db $0F,$92
db $04,$93
db $04,$94
db $03,$95
db $03,$96
db $04,$97
db $05,$98
db $13,$99
db $0A,$9A
db $08,$9B
db $09,$9C
db $2E,$9D
db $25,$9E
db $39,$9F
db $00