;Coaster Commotion ASM (levels 112, FC, FD and FF).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient, store gradient table to RAM
;init/nmi: scroll gradient with layer 3

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDX.b #TablesEnd-Table1

-
LDA Table1,x
STA !gradtable,x

DEX
CPX #$FF
BNE -

LDA.b #!gradtable>>16				;load bank of one of the tables
STA $02						;store to scratch RAM.
STA $05						;store to scratch RAM.
REP #$20					;16-bit A
LDA.w #!gradtable				;load address of first table
STA $00						;store to scratch RAM.
LDA.w #!gradtable+(Table2-Table1)		;load address of second table
STA $03						;store to scratch RAM.
JSL HDMA_Color2					;execute HDMA (A goes 8-bit after this routine).

nmi:
LDA $24					;load layer 3 Y-pos low byte
AND #$0F				;preserve bits 0-3 only
STA $0F5E|!addr				;store to free RAM.

LDA Table1				;load scanline count from first table
SEC					;set carry
SBC $0F5E|!addr				;subtract value
STA !gradtable				;store to gradient RAM table.

LDA Table2				;load scanline count from second table
SEC					;set carry
SBC $0F5E|!addr				;subtract value
STA.l !gradtable+(Table2-Table1)	;store to gradient RAM table.
RTL					;return.

Table1:
db $14,$21,$41
db $0C,$21,$42
db $0B,$22,$42
db $14,$22,$43
db $05,$23,$43
db $19,$23,$44
db $04,$23,$45
db $11,$24,$45
db $6B,$23,$44
db $00

Table2:
db $07,$82
db $09,$83
db $08,$84
db $09,$85
db $09,$86
db $09,$87
db $08,$88
db $09,$89
db $09,$8A
db $09,$8B
db $08,$8C
db $09,$8D
db $09,$8E
db $02,$8F
db $6B,$8B
db $00
TablesEnd: