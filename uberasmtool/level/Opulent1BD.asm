;Opulent Oasis ASM (level 1BD). EVEN MORE CURSED
;Includes:
;init: curses the player
;main: freeze player, no pause, press any button to go back to title screen

init:
LDA #$01			;load value
STA $700002+!TNKModeOffset	;store to SRAM save 1.
STA $702002+!TNKModeOffset	;store to SRAM save 2.
STA $704002+!TNKModeOffset	;store to SRAM save 3. tells you're a cheater.
RTL				;return.

main:
LDA #$02			;load value
STA $13D3|!addr			;store to pause flipping status timer.
LDA #$0D			;load value
STA $71				;store to player animation pointer.

LDA $18				;load controller data 2, first frame only
AND #$C0			;preserve A and X bits only
ORA $16				;OR with controller data 1, first frame only
AND #$F0			;preserve B, Y, Select and Start bits
BEQ ret				;if none of those buttons are pressed, return.

LDA #$02			;load value
STA $0100|!addr			;store to game mode address
LDA #$FF			;load value
STA $1DFB|!addr			;store to address to make song fade out.

ret:
RTL				;return.