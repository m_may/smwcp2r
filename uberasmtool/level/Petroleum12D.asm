;Petroleum Passage ASM (level 12D).
;Includes:
;init: message box text DMA
;init/main: set manual trigger C to 0 when prior screen 0D, and 1 when past screen 0D

init:
REP #$20			;16-bit A
LDX #$00
LDA.w #Messages_Props_Normal
JSL Messages_DMA_FillProp
LDX #$00
LDA.w #Messages_Petroleum12D
JSL Messages_DMA
SEP #$20

main:
LDA $95
CMP #$0D
BCC .nope

LDA #$01
BRA +

.nope
LDA #$00
+
STA $7FC07C
RTL
