;Tourou Temple ASM (levels 18, F1 and F2).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient, parallax HDMA for layer 2/parallax logic, disable BG V-scroll
;nmi: parallax logic


load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $1414|!addr			;no BG V-scroll

LDX #$00			;scanline index
LDY #$00			;loop count, starts at zero

.loop
LDA ParallaxScanlineCount,y	;load value from table according to index
STA !hdmaparallaxtbls,x		;store to HDMA table.
INX #3				;increment X three times
INY				;increment Y by one
CPY #$08			;compare Y to value
BNE .loop			;if not equal, keep looping.

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0F02			;mode 2 on $210F (a.k.a. affect layer 2 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
REP #$20			;16-bit A
LDA #$0000			;load value
STA !hdmaparallaxtbls+1		;store to HDMA table.
STA !hdmaparallaxtbls+19	;store to HDMA table.
LDA $1E				;load layer 2 X-pos current frame
LSR				;divide by 2
STA !hdmaparallaxtbls+13	;store to HDMA table.
CLC				;clear carry
ADC $1E				;add layer 2 X-pos current frame
LSR				;divide by 2
STA !hdmaparallaxtbls+16	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+10	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+7		;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+4		;store to HDMA table.
SEP #$20			;8-bit A
RTL				;return.

ParallaxScanlineCount:
db $30,$28,$3D,$06,$03,$06,$01,$00

Table1:
db $80,$20,$40
db $14,$20,$40
db $05,$20,$41
db $06,$20,$42
db $05,$20,$43
db $05,$20,$44
db $05,$20,$45
db $03,$20,$46
db $02,$21,$46
db $05,$21,$47
db $04,$21,$48
db $05,$21,$49
db $1F,$21,$4A
db $00

Table2:
db $05,$80
db $09,$81
db $09,$82
db $09,$83
db $09,$84
db $09,$85
db $0A,$86
db $09,$87
db $09,$88
db $09,$89
db $09,$8A
db $09,$8B
db $09,$8C
db $09,$8D
db $09,$8E
db $09,$8F
db $08,$90
db $05,$8F
db $05,$8E
db $04,$8D
db $01,$8C
db $02,$8D
db $03,$8E
db $03,$8F
db $03,$90
db $03,$91
db $03,$92
db $03,$93
db $03,$94
db $02,$95
db $03,$96
db $03,$97
db $1D,$98
db $00