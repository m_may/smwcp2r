;Fantastic Elastic ASM (level 115).
;Includes:
;init: message box text DMA, 2-channel HDMA gradient
;init/nmi: parallax stuff
;nmi: parallax stuff

init:
REP #$20			;16-bit A
LDX #$02 ;temp
LDA.w #Messages_Props_Normal ;temp
JSL Messages_DMA_FillProp ;temp
LDX #$06 ;temp
JSL Messages_DMA_Finish ;temp
LDX #$02
LDA.w #Messages_Fantastic115_1
JSL Messages_DMA
LDX #$06
LDA.w #Messages_Fantastic115_2
JSL Messages_DMA

LDX.b #Table1>>16		;load bank of one of the tables
STX $02				;store to scratch RAM.
STX $05				;store to scratch RAM.

LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.

JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $06,$21,$40
db $0B,$22,$40
db $08,$23,$40
db $03,$23,$41
db $0B,$24,$41
db $0B,$25,$41
db $0B,$26,$41
db $0B,$27,$41
db $02,$28,$41
db $09,$28,$42
db $0B,$29,$42
db $0B,$2A,$42
db $0A,$2B,$42
db $07,$2C,$42
db $04,$2C,$43
db $0B,$2D,$43
db $57,$2E,$43
db $00

Table2:
db $0F,$80
db $1C,$81
db $1D,$82
db $1C,$83
db $1D,$84
db $5F,$85
db $00