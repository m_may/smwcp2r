;Cumulus Chapel ASM (level 177).
;Includes:
;load: SMWC coin custom trigger init, parallax HDMA init
;init: 2-channel HDMA gradient, parallax HDMA processing, set up ram flag for ships killed
;init and nmi: parallax HDMA, and layer 3 message/parallax HDMA positioning.
;main: parallax logic, handle message displaying for when all ships are killed once
;nmi: parallax HDMA processing

!AlbatossNum = $05

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ $87				;reset address (corresponds to !ManualMsg in CumulusGeneric.asm, library file)

LDA $0F32|!addr
CMP #$04
BEQ +
BCS +

STZ $0F32|!addr

+
LDA $0F33|!addr
BNE +
STZ $0F5E|!addr
LDA #$09
STA $0F33|!addr
+

JSL CumulusGeneric_PrepTableAndGrad
BRA main			;branch ahead - part of nmi code isn't relevant here

nmi:
JSL CumulusGeneric_SetCoords

LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
RTL

main:
JSL CumulusGeneric_Logic

LDA $0F32|!addr
BEQ +

LDX $0F5E|!addr
BNE +

LDX #$01
STX $87

+
CMP #$04
BNE .ret


LDA $0F5E|!addr
BEQ .dispmsg
RTL

.dispmsg
INC $0F5E|!addr

LDA #$02
STA $1426|!addr

.ret
RTL