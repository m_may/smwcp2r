;Frostflow Freezer ASM (level 2D).
;Includes:
;load: SMWC coin custom trigger init
;init/main: translucent layer 3 over layers 1, 2 and sprites, 2-channel HDMA gradient, wave HDMA init and processing, ...
;... many stuff and workarounds related to water interaction

!waveindex = $0F5E|!addr		;index for wave scroll
!l3posindex = $0F60|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls	;address of table
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4364
LDA #$40			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 6

BRA main

nmi:
REP #$20
LDA !l3posindex
CLC
ADC $1462|!addr
STA $22
SEP #$20
RTL

main:
ldx #$09

-
lda $170B,x
cmp #$12
bne +

lda $1729,x
bne +
lda $1715,x
cmp #$c8
bcs +

stz $170B,x	;despawn bubbles above #$00C8 Y-pos

+
dex
bpl -

STZ $85		;no water level...

REP #$20
LDA $96
BMI +		;if negative Y-pos (top of screen)

LDX $19
BEQ .smol
LDX $73
BNE .smol

CMP #$00CC
BCC +
BRA .wat

.smol
CMP #$00C7
BCC +
SEP #$20

.wat
INC $85		;unless below #$00C7 (#$00CC if big and not ducking)
		;oh man look at the lolcode level i need to do to avoid slowdown or other overcomplicated alternatives

+
SEP #$20

LDA $14AF|!addr
BEQ +
LDA $71
ORA $9D
ORA $13D4|!addr
BNE +

LDA $14
AND #$07
BNE .noscroll

REP #$20
INC !l3posindex
SEP #$20

.noscroll
LDA $14
LSR #3
AND #$07
STA !waveindex

+
LDA #$7F		;load scanline count
SEC			;set carry
SBC $1C			;subtract layer 1 Y-pos, current frame, low byte, from it
STA $00			;store to scratch RAM.
BMI zeroone

sec:
LDA #$53
BRA st

zeroone:
LDA.b #$D1
SEC
SBC $1C

st:
STA $01

+
REP #$20
LDA !waveindex
ASL
TAY

STZ $0E
LDX #$00		;starting index for loop

waveloop:
PHY

CPX #$00		;first entry of table varies depending on layer 1 Y-pos
BEQ first

SEP #$20
LDA #$08		;default scanline count value

LDY $0E
BNE +
CPX #$03		;second entry
BNE +

second:
INC $0E
LDA $01			;second scanline count value
BRA ssec

first:
SEP #$20
LDA $00			;this holds the dynamic scanline count set before the loop
BMI second

ssec:
STA !hdmaparallaxtbls,x

REP #$20
LDA $1C
BRA ++

+
STA !hdmaparallaxtbls,x
REP #$20

PLY
LDA $1C
CLC
ADC Index,y

PHA

INY #2
TYA
AND #$000F
TAY

PLA

PHY	;anticrash

++
STA !hdmaparallaxtbls+1,x
PLY
INX #3
CPX #$60
BNE waveloop

SEP #$20

LDA #$00
STA !hdmaparallaxtbls,x		;finalize table

JSR palettespritestuff
RTL

palettespritestuff:
	LDX #$0B
.Loop
STZ $164A,x

lda $14D4,x
bne .onwater
lda $D8,x
CMP #$D0
BCC ++

.onwater
LDA $166E,x : ORA #$40 : STA $166E,x
LDA #$01
STA $164A,x

++
	LDA $9E,x
	CMP #$B2	; If falling spike
	BNE .LoopCall

	LDA $15F6,x
	ORA #$02
	STA $15F6,x	; Use palette 9 instead of palette 8.

.LoopCall
	DEX
	BPL .Loop

	LDA $14AF
	BNE .NoCorLay3

.NoCorLay3
	LDA $14AF
	BNE PalFade2D

	LDA $7FC086
	CMP #$08
	BCS Continue2D

	LDA #$00
	STA $7FC086
	STA $7FC085
	STA $7FC084
	BRA Continue2D

PalFade2D:
	LDA $7FC086
	CMP #$08
	BCC Continue2D

	LDA #$08
	STA $7FC086
	STA $7FC085
	STA $7FC084

Continue2D:
	RTS

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond

Table1:
	db $18,$41,$82
	db $08,$42,$83
	db $10,$43,$84
	db $10,$44,$85
	db $08,$44,$86
	db $10,$45,$86
	db $10,$45,$87
	db $20,$46,$88
	db $20,$47,$89
	db $08,$48,$8A
	db $08,$48,$8B
	db $28,$49,$8B
	db $00

Table2:
	db $18,$21
	db $28,$22
	db $38,$23
	db $30,$24
	db $38,$25
	db $00