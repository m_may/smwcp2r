;Coaster Commotion ASM (level FA).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDX #$04
LDA.w #Messages_Props_Normal
JSL Messages_DMA_FillProp
LDX #$04
LDA.w #Messages_CoasterFA
JSL Messages_DMA
SEP #$20
RTL
