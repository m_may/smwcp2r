;Tourou Temple ASM (level F0).
;Includes:
;init: 3-channel HDMA gradient
;main: make the player stand still relative to layer 2 when stepping on it

init:
LDA.b #Gradient1_RedTable>>16	;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Gradient1_RedTable	;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Gradient1_GreenTable	;load address of second table
STA $03				;store to scratch RAM.
LDA.w #Gradient1_BlueTable	;load address of third table
STA $06				;store to scratch RAM.
JML HDMA_Color3			;execute HDMA (A goes 8-bit after this routine) and return.

main:
JML MovePlayerAlongL2_run	;make the player stand still relative to layer 2

Gradient1_RedTable:
db $19,$20
db $0E,$21
db $08,$22
db $05,$23
db $05,$24
db $0F,$25
db $0B,$24
db $0B,$23
db $15,$22
db $02,$23
db $19,$24
db $52,$23
db $00

Gradient1_GreenTable:
db $1E,$40
db $0A,$41
db $31,$42
db $19,$41
db $03,$42
db $02,$43
db $0B,$44
db $08,$45
db $07,$46
db $07,$47
db $07,$48
db $07,$49
db $07,$4A
db $0A,$4B
db $0B,$4C
db $10,$4D
db $0E,$4E
db $00

Gradient1_BlueTable: 
db $1B,$86
db $07,$87
db $05,$88
db $08,$89
db $16,$8A
db $07,$89
db $06,$88
db $05,$87
db $08,$86
db $0B,$85
db $01,$86
db $01,$87
db $01,$88
db $01,$89
db $01,$8A
db $01,$8B
db $02,$8C
db $01,$8D
db $02,$8E
db $01,$8F
db $06,$90
db $0D,$91
db $0F,$92
db $0F,$93
db $0D,$94
db $0B,$95
db $0B,$96
db $0E,$97
db $08,$98
db $00