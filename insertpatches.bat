set ROM=SMWCP2R.smc
set ASAR=asar\asar.exe
set ASMDIR=asm\patches

@echo Welcome to Batman.
@echo.

%ASAR% "%ASMDIR%\1player.asm" %ROM%
%ASAR% "%ASMDIR%\antisoftlock.asm" %ROM%
%ASAR% "%ASMDIR%\asarspritetiles.asm" %ROM%
%ASAR% "%ASMDIR%\autosave.asm" %ROM%
%ASAR% "%ASMDIR%\BeatLevel.asm" %ROM%
%ASAR% "%ASMDIR%\better_powerdown.asm" %ROM%
%ASAR% "%ASMDIR%\blockmask.asm" %ROM%
%ASAR% "%ASMDIR%\CastleBlkFix.asm" %ROM%
%ASAR% "%ASMDIR%\classicfire.asm" %ROM%
%ASAR% "%ASMDIR%\CloudTimerWarning.asm" %ROM%
%ASAR% "%ASMDIR%\Counter Break Yoshi.asm" %ROM%
%ASAR% "%ASMDIR%\DebugCheats.asm" %ROM%
%ASAR% "%ASMDIR%\DisableScreenBarrierViaRam.asm" %ROM%
%ASAR% "%ASMDIR%\easymode7.asm" %ROM%
%ASAR% "%ASMDIR%\FallingSpikeFix.asm" %ROM%
%ASAR% "%ASMDIR%\frozensprfix.asm" %ROM%
%ASAR% "%ASMDIR%\GameOverHijack.asm" %ROM%
%ASAR% "%ASMDIR%\lakitu_limit.asm" %ROM%
%ASAR% "%ASMDIR%\LevelExtend.asm" %ROM%
%ASAR% "%ASMDIR%\MarioGFXDMA.asm" %ROM%
%ASAR% "%ASMDIR%\MidwayPointHijack.asm" %ROM%
%ASAR% "%ASMDIR%\MMP16.asm" %ROM%
%ASAR% "%ASMDIR%\NoMesBoxWin.asm" %ROM%
%ASAR% "%ASMDIR%\NoteWallFix.asm" %ROM%
%ASAR% "%ASMDIR%\OWMusic.asm" %ROM%
%ASAR% "%ASMDIR%\OWEnterLevelHijack.asm" %ROM%
%ASAR% "%ASMDIR%\optimize_2132_store.asm" %ROM%
%ASAR% "%ASMDIR%\PalaceSwitchHijack.asm" %ROM%
%ASAR% "%ASMDIR%\quickrom.asm" %ROM%
%ASAR% "%ASMDIR%\RedoneSMWCHUD.asm" %ROM%
%ASAR% "%ASMDIR%\reverse_gravity.asm" %ROM%
%ASAR% "%ASMDIR%\RolloverFix.asm" %ROM%
%ASAR% "%ASMDIR%\SMWCP2-HexEdits.asm" %ROM%
%ASAR% "%ASMDIR%\BoneFix.asm" %ROM%
%ASAR% "%ASMDIR%\sram_plus.asm" %ROM%
%ASAR% "%ASMDIR%\StartGame.asm" %ROM%
%ASAR% "%ASMDIR%\Title.asm" %ROM%
%ASAR% "%ASMDIR%\tweaker.asm" %ROM%
%ASAR% "%ASMDIR%\VScrollReprogrammed.asm" %ROM%
%ASAR% "%ASMDIR%\WindowingTransition.asm" %ROM%
%ASAR% "%ASMDIR%\yoshi.asm" %ROM%

@echo.
@echo - Check for any errors.
@echo.
@echo Not sure if it's up-to-date but here it is:
@echo.
@echo - btw remember to SHIFT+F8 your ROM in LM because SRAM Plus
@echo undoes the sprite 19 fix by default.
@echo.
@echo - If using LM v3.04, remember to apply tidefix.asm manually.
@echo.
