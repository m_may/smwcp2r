;------------------------ edit those ------------------------
!CurrentSRAMVersion = 1  ;Increment on changing the SRAM layout! Then supply a conversion routine (in uberasmtool/gamemode/3CServiceMenu.asm).
!CurrentOWVersion = 1  ;Increment on modifying the Overworld (level tile settings, events)



;------------------------ careful with editng those ------------------------
;refer to asm/patches/sram_table.asm
!SRAMVersionComplementRAM = $7FB5FC
!OWVersionComplementRAM = $7FB5FD
!SRAMVersionRAM = $7FB5FE
!OWVersionRAM = $7FB5FF

!SRAMVersionComplementSRAMOffset = $008D+$008D+$00FC
!OWVersionComplementSRAMOffset = $008D+$008D+$00FD
!SRAMVersionSRAMOffset = $008D+$008D+$00FE
!OWVersionSRAMOffset = $008D+$008D+$00FF
