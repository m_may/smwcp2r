251	0	251SolidCT1.asm
300	0	Teleports the player when touched.
301	0	301Kill.asm
302	0	Block that is solid for sprites but passable for the player.
303	0	Block that is solid for the player but passable for sprites.
304	0	A frozen block that disappears when thawed.
305	0	This block is solid when the on/off switch is set to 'off'.
306	0	This block is solid when the on/off switch is set to 'on'.
307	0	A block with a slippery top.
308	0	A block with a slippery top.
30b	0	When touched, sets blue POW timer to 1/4 and plays the music, shake screen etc.
30c	0	When touched, sets silver POW timer to 1/4 and plays the music, shake screen etc.
30d	0	Block that is solid when the Blue P-Switch is active.
30e	0	Block that is solid when the Silver P-Switch is active.
30f	0	30Fwaterice.asm
311	0	311noyoshi.asm
312	0	An icy ON/OFF block. Solid when ON.
313	0	An icy ON/OFF block. Solid when OFF.
314	0	314CrackIceL.asm
315	0	315CrackIceR.asm
316	0	316KillInside.asm
319	0	319railcollect.asm
31a	0	31AConveyerLeft.asm
31b	0	31BConveyerRight.asm
322	0	322violentsand.asm
323	0	323button.asm
325	0	325violentsand2.asm
328	0	328bounce.asm
32a	0	32Aslopefix.asm
331	0	A quicksand block that slowly lets Mario and sprites sink but still lets Mario walk
332	0	A quicksand block that slowly lets Mario and sprites sink and keeps Mario from walking
335	0	This block launches both player and sprites upwards with high speed.
336	0	This block launches both player and sprites rightwards with high speed.
337	0	This block launches both player and sprites upwards with moderate speed.
338	0	This block launches both player and sprites rightwards with moderate speed.
339	0	This block launches both player and sprites upwards with high speed only when the ON/OFF switch is ON.
33a	0	This block launches both player and sprites upwards with high speed only when ExAnimation frame is zero.
33b	0	This block launches both player and sprites leftwards with high speed.
33c	0	This block launches both player and sprites leftwards with moderate speed.
33d	0	This block launches both player and sprites downwards with high speed.
33f	0	33FQuicksandNorm2.asm
340	0	340spike.asm
341	0	341spike_sideways.asm
342	0	342spike_upsidedown.asm
343	0	382shatter.asm
344	0	344drum1.asm
345	0	345drum2.asm
346	0	346laser1.asm
347	0	347laser2.asm
348	0	348ct0rail.asm
34c	0	A coin that acts like a purple coin, but also adds one (second) to address $0F5E (stopwatch), used by The Magician (TM).
34f	0	A block that hurts the player from all sides. Unlike the muncher, this block will always hurt, regardless of the Silver P-Switch status.
351	0	351ledgeoverwater.asm
354	0	354ledge1.asm
355	0	355ledge2.asm
356	0	356ledge3.asm
357	0	357ledge4.asm
360	0	360animhurt1.asm
361	0	361animhurt2.asm
362	0	362animhurt3.asm
363	0	363animhurt4.asm
364	0	364LeftwardsOnly.asm
365	0	365LeftwardsOnlyCorner.asm
366	0	366rightcurrent.asm
367	0	367leftcurrent.asm
368	0	368upcurrent.asm
369	0	369downcurrent.asm
36a	0	36Awindup.asm
36c	0	36CRightwardsOnly.asm
371	0	371sun.asm
372	0	372shade.asm
375	0	375CustomTrigger0Activate.asm
377	0	377Oneshot01Activate.asm
37a	0	38Athunderbell.asm
37f	0	37FSpringRoom.asm
380	0	380bigdrum.asm
382	0	382shatter.asm
383	0	383keyblock.asm
385	0	A Donut Lift, which will fall shortly after being stepped on.
386	0	386brickv13.asm
387	0	387bounceSound1.asm
388	0	388bounceSound2.asm
389	0	389bounceSound3.asm
38a	0	38Athunderbell.asm
38c	0	38CP_teleport.asm
390	0	390Button1.asm
391	0	391Button2.asm
392	0	392Button3.asm
393	0	393Button4.asm
394	0	394Button5.asm
395	0	395Button6.asm
396	0	396Button7.asm
397	0	397Button8.asm
398	0	398Button9.asm
39c	0	39Cbackmario.asm
39d	0	39Dbehindwalls.asm
39e	0	39Efrontwalls.asm
39f	0	39Finsideledge.asm
3a0	0	3A0breakifshell.asm
3a1	0	3A1breakifrshell.asm
3a2	0	3A2breakifgshell.asm
3a3	0	3A3breakifyshell.asm
3a4	0	3A4breakifbshell.asm
3c0	0	Swaps the on/off switch status when Up is pressed.
3c2	0	3C2sushi.asm
3c3	0	3C3MathBlock0.asm
3c4	0	3C4MathBlock1.asm
3c5	0	3C5MathBlock2.asm
3c6	0	3C6MathBlock3.asm
3c7	0	3C7MathBlock4.asm
3c8	0	3C8MathBlock5.asm
3c9	0	3C9MathBlock6.asm
3ca	0	3CAMathBlock7.asm
3cb	0	3CBMathBlock8.asm
3cc	0	3CCMathBlock9.asm
3cd	0	3CDAnswerSubmit.asm
3ce	0	3CEfoultele.asm
3cf	0	3CFplacer.asm
3f5	0	3F5fire_block.asm
3f8	0	Force normal gravity when On/Off switch is Off
3f9	0	Force normal gravity when On/Off switch is On
3fa	0	Force reverse gravity when On/Off switch is Off
3fb	0	Force reversed gravity when On/Off switch is On
3fc	0	3FCHurtOff.asm
3fd	0	3FD1FrameKill.asm
3fe	0	Activates the nearest laser shooter when touched by the player or a sprite.
3ff	0	3FFHurtOn.asm
52c	0	Acts like a ledge when standing on it, press down and acts like a vine.
55f	0	This is SMW's regular lava surface, however it has custom block code implemented so it properly kills Mario when touched.
794	0	Teleports the player when the player presses Up.
9b8	0	This block is solid only for Sprites $4F & $50, Jumping Piranha Plant and Fire-Spitting variant.
cd0	0	An icy slope.
cd1	0	An icy slope.
cd2	0	An icy slope.
cd3	0	An icy slope.
1230	0	1230BeatLevel.asm
1231	0	1231FreeOWMovement.asm
1232	0	1232GameOverContinue.asm
1238	0	1238CheatResetter.asm
1247	0	1247GreenSwitchOn.asm
1248	0	1248YellowSwitchOn.asm
1249	0	1249BlueSwitchOn.asm
124a	0	124ARedSwitchOn.asm
1257	0	1257GreenSwitchOff.asm
1258	0	1258YellowSwitchOff.asm
1259	0	1259BlueSwitchOff.asm
125a	0	125ARedSwitchOff.asm
12d6	0	This block launches both player and sprites upwards with high speed only when One Shoot Trigger 0 frame is zero and active.
12e3	0	ConvLeftOnRightOff.asm
12ea	0	An electricity beam that hurts the player in Musical Mechanisms. Hurts in the 1st 4/4 beat.
12eb	0	An electricity beam that hurts the player in Musical Mechanisms. Hurts in the 2nd 4/4 beat.
12ec	0	An electricity beam that hurts the player in Musical Mechanisms. Hurts in the 3rd 4/4 beat.
12ed	0	An electricity beam that hurts the player in Musical Mechanisms. Hurts in the 4th 4/4 beat.
12f3	0	ConvRightOnLeftOff.asm
148e	0	148Eevent.asm
1700	0	3d50.asm
1701	0	3d51.asm
1702	0	3d52.asm
1703	0	3d53.asm
1704	0	3d54.asm
1705	0	3d55.asm
1706	0	3d56.asm
1707	0	3d57.asm
1708	0	3d58.asm
1709	0	3d59.asm
170a	0	3d5a.asm
170b	0	3d5b.asm
170c	0	3d5c.asm
170d	0	3d5d.asm
170e	0	3d5e.asm
170f	0	3d5f.asm
1760	0	A sign block that will prompt message 1 from level when the player presses Up in front of it.
1761	0	A sign block that will prompt message 2 from level when the player presses Up in front of it.
2732	0	2732checker.asm
2733	0	2733WROOOONG.asm
2745	0	Activates third exit when touched.
2773	0	The mysterious machine of Opulent Oasis that spawns a diamond sprite which triggers a cutscene event before warping the player to another sublevel when Up is pressed in front of it, if a real diamond was collected. Does nothing otherwise... or, you're free to try some combinations for a different outcome...
2db3	0	2D90eventdoors.asm
2dc8	0	2DC8RttFDoorA.asm
2dc9	0	2DC9RttFDoorB.asm
2dca	0	2DCApurplecoin.asm
2dcb	0	2DCBRttFSpike.asm
2dd3	0	2DD3message2.asm
2dd4	0	2DD4doortargets.asm
2dd6	0	2DD6flagpole.asm
2dd7	0	2DD7failuretele.asm
2de2	0	2DE2RttFDoorC.asm
2de3	0	2DE3doorcoins.asm
2f24	0	An icy ON/OFF block. Solid when OFF.
2f2e	0	3F0lavaon.asm
2f39	0	3F1vineoff.asm
34ba	0	34BAbellflower.asm
34bb	0	34BBbellmush.asm
34bc	0	34BCbellcape.asm
34bd	0	34BDbell1up.asm
34d8	0	34D8bell5coin.asm
35a5	0	A block that hurts the player from all sides. Unlike the muncher, this block will always hurt, regardless of the Silver P-Switch status.
37e0	0	37E0.asm
37e1	0	37E1.asm
37e2	0	37E2.asm
39b0	0	Block that acts like a ledge for sprites but is passable for the player.
3b82	0	3B82.asm
3b83	0	3B83.asm
3b84	0	3B84.asm
3b85	0	3B85.asm
4000	0	Top-left part of SMWC Coin #1. Make sure you configure Conditional Direct Map16 options for it (use flag 0, and mark 'always show objects/add 0x100 tiles').
4001	0	Top-right part of SMWC Coin #1. Make sure you configure Conditional Direct Map16 options for it (use flag 0, and mark 'always show objects/add 0x100 tiles').
4002	0	Top-left part of SMWC Coin #2. Make sure you configure Conditional Direct Map16 options for it (use flag 1, and mark 'always show objects/add 0x100 tiles').
4003	0	Top-right part of SMWC Coin #2. Make sure you configure Conditional Direct Map16 options for it (use flag 1, and mark 'always show objects/add 0x100 tiles').
4004	0	Top-left part of SMWC Coin #3. Make sure you configure Conditional Direct Map16 options for it (use flag 2, and mark 'always show objects/add 0x100 tiles').
4005	0	Top-right part of SMWC Coin #3. Make sure you configure Conditional Direct Map16 options for it (use flag 2, and mark 'always show objects/add 0x100 tiles').
4007	0	A second midway point. Make sure you configure Conditional Direct Map16 options for it (use flag 3, and mark 'always show objects/add 0x100 tiles').
4009	0	A third midway point. Make sure you configure Conditional Direct Map16 options for it (use flag 4, and mark 'always show objects/add 0x100 tiles').
400a	0	SMWC Coin #1, meant to be used in levels with HDMA since original coins can mess up the effects for a frame. Requires Custom Trigger F ExAnimation.
400c	0	SMWC Coin #2, meant to be used in levels with HDMA since original coins can mess up the effects for a frame. Requires Custom Trigger E ExAnimation.
400e	0	SMWC Coin #3, meant to be used in levels with HDMA since original coins can mess up the effects for a frame. Requires Custom Trigger D ExAnimation.
4010	0	Bottom-left part of SMWC Coin #1. Make sure you configure Conditional Direct Map16 options for it (use flag 0, and mark 'always show objects/add 0x100 tiles').
4011	0	Bottom-right part of SMWC Coin #1. Make sure you configure Conditional Direct Map16 options for it (use flag 0, and mark 'always show objects/add 0x100 tiles').
4012	0	Bottom-left part of SMWC Coin #2. Make sure you configure Conditional Direct Map16 options for it (use flag 1, and mark 'always show objects/add 0x100 tiles').
4013	0	Bottom-right part of SMWC Coin #2. Make sure you configure Conditional Direct Map16 options for it (use flag 1, and mark 'always show objects/add 0x100 tiles').
4014	0	Bottom-left part of SMWC Coin #3. Make sure you configure Conditional Direct Map16 options for it (use flag 2, and mark 'always show objects/add 0x100 tiles').
4015	0	Bottom-right part of SMWC Coin #3. Make sure you configure Conditional Direct Map16 options for it (use flag 2, and mark 'always show objects/add 0x100 tiles').
4300	0	cart.asm
4301	0	cart.asm
4302	0	cart.asm
4303	0	cart.asm
4304	0	cart.asm
4305	0	cart.asm
4306	0	cart.asm
4307	0	cart.asm
4308	0	cart.asm
4309	0	cart.asm
430a	0	cart.asm
430b	0	cart.asm
430c	0	cart.asm
430d	0	cart.asm
430e	0	cart.asm
430f	0	cart.asm
4310	0	cart.asm
4311	0	cart.asm
4312	0	cart.asm
4313	0	cart.asm
4314	0	cart.asm
4315	0	cart.asm
4316	0	cart.asm
4317	0	cart.asm
4318	0	cart.asm
4319	0	cart.asm
431a	0	cart.asm
431b	0	cart.asm
431c	0	cart.asm
431d	0	cart.asm
431e	0	cart.asm
431f	0	cart.asm
4320	0	cart.asm
4321	0	cart.asm
4322	0	cart.asm
4323	0	cart.asm
4324	0	cart.asm
4325	0	cart.asm
4326	0	cart.asm
4327	0	cart.asm
4328	0	cart.asm
4329	0	cart.asm
432a	0	cart.asm
432b	0	cart.asm
432c	0	cart.asm
432d	0	cart.asm
432e	0	cart.asm
432f	0	cart.asm
4330	0	cart.asm
4331	0	cart.asm
4332	0	cart.asm
4333	0	cart.asm
4334	0	cart.asm
4335	0	cart.asm
4336	0	cart.asm
4337	0	cart.asm
4338	0	cart.asm
4339	0	cart.asm
433a	0	cart.asm
433b	0	cart.asm
433c	0	cart.asm
433d	0	cart.asm
433e	0	cart.asm
433f	0	cart.asm
4340	0	cart.asm
4341	0	cart.asm
4342	0	cart.asm
4343	0	cart.asm
4344	0	cart.asm
4345	0	cart.asm
4346	0	cart.asm
4347	0	cart.asm
4348	0	cart.asm
4349	0	cart.asm
434a	0	cart.asm
434b	0	cart.asm
434c	0	cart.asm
434d	0	cart.asm
434e	0	cart.asm
434f	0	cart.asm
4350	0	cart.asm
4351	0	cart.asm
4352	0	cart.asm
4353	0	cart.asm
4354	0	cart.asm
4355	0	cart.asm
4356	0	cart.asm
4357	0	cart.asm
4358	0	cart.asm
4359	0	cart.asm
435a	0	cart.asm
435b	0	cart.asm
435c	0	cart.asm
435d	0	cart.asm
435e	0	cart.asm
435f	0	cart.asm
4360	0	cart.asm
4361	0	cart.asm
4362	0	cart.asm
4363	0	cart.asm
4364	0	cart.asm
4365	0	cart.asm
4366	0	cart.asm
4367	0	cart.asm
4368	0	cart.asm
4369	0	cart.asm
436a	0	cart.asm
436b	0	cart.asm
436c	0	cart.asm
436d	0	cart.asm
436e	0	cart.asm
436f	0	cart.asm
4370	0	cart.asm
4371	0	cart.asm
4372	0	cart.asm
4373	0	cart.asm
4374	0	cart.asm
4375	0	cart.asm
4376	0	cart.asm
4377	0	cart.asm
4378	0	cart.asm
4379	0	cart.asm
437a	0	cart.asm
437b	0	cart.asm
437c	0	cart.asm
437d	0	cart.asm
437e	0	cart.asm
437f	0	cart.asm
4380	0	cart.asm
4381	0	cart.asm
4382	0	cart.asm
4383	0	cart.asm
4384	0	cart.asm
4385	0	cart.asm
4386	0	cart.asm
4387	0	cart.asm
4388	0	cart.asm
4389	0	cart.asm
438a	0	cart.asm
438b	0	cart.asm
438c	0	cart.asm
438d	0	cart.asm
438e	0	cart.asm
438f	0	cart.asm
4390	0	cart.asm
4391	0	cart.asm
4392	0	cart.asm
4393	0	cart.asm
4394	0	cart.asm
4395	0	cart.asm
4396	0	cart.asm
4397	0	cart.asm
4398	0	cart.asm
4399	0	cart.asm
439a	0	cart.asm
439b	0	cart.asm
439c	0	cart.asm
439d	0	cart.asm
439e	0	cart.asm
439f	0	cart.asm
43a0	0	cart.asm
43a1	0	cart.asm
43a2	0	cart.asm
43a3	0	cart.asm
43a4	0	cart.asm
43a5	0	cart.asm
43a6	0	cart.asm
43a7	0	cart.asm
43a8	0	cart.asm
43a9	0	cart.asm
43aa	0	cart.asm
43ab	0	cart.asm
43ac	0	cart.asm
43ad	0	cart.asm
43ae	0	cart.asm
43af	0	cart.asm
43b0	0	cart.asm
43b1	0	cart.asm
43b2	0	cart.asm
43b3	0	cart.asm
43b4	0	cart.asm
43b5	0	cart.asm
43b6	0	cart.asm
43b7	0	cart.asm
43b8	0	cart.asm
43b9	0	cart.asm
43ba	0	cart.asm
43bb	0	cart.asm
43bc	0	cart.asm
43bd	0	cart.asm
43be	0	cart.asm
43bf	0	cart.asm
43c0	0	cart.asm
43c1	0	cart.asm
43c2	0	cart.asm
43c3	0	cart.asm
43c4	0	cart.asm
43c5	0	cart.asm
43c6	0	cart.asm
43c7	0	cart.asm
43c8	0	cart.asm
43c9	0	cart.asm
43ca	0	cart.asm
43cb	0	cart.asm
43cc	0	cart.asm
43cd	0	cart.asm
43ce	0	cart.asm
43cf	0	cart.asm
43d0	0	cart.asm
43d1	0	cart.asm
43d2	0	cart.asm
43d3	0	cart.asm
43d4	0	cart.asm
43d5	0	cart.asm
43d6	0	cart.asm
43d7	0	cart.asm
43d8	0	cart.asm
43d9	0	cart.asm
43da	0	cart.asm
43db	0	cart.asm
43dc	0	cart.asm
43dd	0	cart.asm
43de	0	cart.asm
43df	0	cart.asm
43e0	0	cart.asm
43e1	0	cart.asm
43e2	0	cart.asm
43e3	0	cart.asm
43e4	0	cart.asm
43e5	0	cart.asm
43e6	0	cart.asm
43e7	0	cart.asm
43e8	0	cart.asm
43e9	0	cart.asm
43ea	0	cart.asm
43eb	0	cart.asm
43ec	0	cart.asm
43ed	0	cart.asm
43ee	0	cart.asm
43ef	0	cart.asm
43f0	0	cart.asm
43f1	0	cart.asm
43f2	0	cart.asm
43f3	0	cart.asm
43f4	0	cart.asm
43f5	0	cart.asm
43f6	0	cart.asm
43f7	0	cart.asm
43f8	0	cart.asm
43f9	0	cart.asm
43fa	0	cart.asm
43fb	0	cart.asm
43fc	0	cart.asm
43fd	0	cart.asm
43fe	0	cart.asm
43ff	0	cart.asm
4503	0	An icy slope.
4504	0	An icy slope.
46dc	0	A door that's only enterable if the player has gone through all 4 paths of Perilous Paths successfully.
47bc	0	This block is a ledge when the on/off switch is set to 'on'.
4884	0	WaterPushL.asm
