SMW free RAMs used by important patches/uberASM codes:

RAM:
$62 = used in titlescreen for effects (trigger number 2 sprite), and as a flag to reset layer 3 coordinates to zero in the OW (fixes the border).
$87 = used in some levels to determine the message to display when a message box is triggered. Free to use in the overworld, and some other levels. Warning: will need to be changed because of SA-1.
$0EF9-$0F03 = used by the status bar as XY pos of counters, timers and stuff. Free to use in the overworld, but not in levels.
$0F2F = used in the OW as the X offset for indicators, and also as a flag to show them on screen.
$0F5E = used in titlescreen for effects (trigger copyright thingy sprite). Free to use in other places otherwise (e.g. used in stopwatch levels).
$0F5F = used in titlescreen for effects (titlescreen animation pointer). Free to use in other places otherwise.
$0F60 = used in titlescreen for effects (titlescreen animation timer). Free to use in other places otherwise.
$0F63-$0F6A = used in the OW for various indicator-related stuff. Free to use in levels.
$13C0 = used by Windowing Transition.
$1461 = used by GHB's Perfect Slippery Blocks. Free to use in the overworld and levels that don't use these blocks or its uberASM code.
$1696 = flag to disable screen barrier. Gets reset on level load, and is set to non-zero by the goal roulette and bosses. Also used on the main map - handles music changing.
$18C5-$18C8 = used in the OW for various indicator-related stuff. Also used in other places.
$18C9 = used by Hartfowl.
$1B7F = used by Reverse Gravity patch.

Note: $1EA2-$1F2E are transferred by SRAM Plus
$1F2B = used by Perilous Paths to keep track of rooms cleared. Saved to SRAM, so it cannot be used anywhere else.
$1F2C = bits 0 and 4 are used by the Asteroid Antics collectible when collected. Bits 1 and 5 by the fake one. (Feel free to reuse bits 4-7 if needed, they aren't used by anything meaningful, but remember to change the codes that set/read them)

Note: $1F49-$1FD5 are transferred by SRAM Plus
$1F49-$1FA8 = used to keep track of Multi-Midway Points and SMWC coins collected per level.
$1FA9-$1FB7 = which events are unlocked
$1FB8-$1FB9 = death counter
$1FBA-$1FBD = play time
$1FBE-$1FBF = SMWC coin total count
$1FC0-$1FC1 = cheats
$1FC2-$1FCD = reserved
$1FCE-$1FD1 = switches unlocked
$1FD2-$1FD4 = reserved
$1FD5 = events unlocked total count

$1FFE = fahrenheit hdma mirror
$7F8800-$7F8BFF = Hartfowl cos table. (Note: get rid of it)
$7F8888 = something?
$7F8900-$7F890A = used by the new message box windowing HDMA effect. Free to use in levels without message boxes.
$7F890B-varies (but let's assume $7F893F) = used by layer 3 parallax within message box code to display certain coordinates of it. Free to use in levels without message boxes.
$7F947B-varies ($7F9C7A at most) = used by DynamicZ for Variables. Refer to misc/dynamicz/Variables.txt. Note that Dynamic Sprites themselves may use space past DZ_FreeRams ($7F986E). Slideshow Showdown uses many of the addresses past DZ_FreeRams too. And vanilla Wiggler segments - so don't put dynamic sprites together with Wigglers.
$7F9C7B-$7F9FFB = used by DynamicZ for Variables2. Refer to misc/dynamicz/Variables.txt.
$7FACC1-$7FACD8 = used by GHB's Perfect Slippery Blocks.
$7FB000-$7FB009 = used by AMK.
$7FB00A-varies (theoretically up to $7FB409 with 255 samples (with noise)) = used by AMK for sample stuff.

Note: $7FB500-7FB5FF are transferred by SRAM Plus
$7FB500-$7FB5FC = reserved
$7FB5FD = reset OW flag
$7FB5FE = SRAM version address
$7FB5FF = OW version address

$7FB600-7FB9FF = Hartfowl sin table. Free to use for other stuff. (Note: get rid of it)
$7FBA00-varies (has to be $7FBBFF at most) = used for various (mostly parallax) HDMA tables in levels and sky map.


SRAM (excludes bytes used by SRAM Plus):
bank $71 - backups of save games from bank $70
