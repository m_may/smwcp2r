table "table.txt"
norom
org $1140
dw "                                "
dw " Rumors say that throughout     "
dw " these frozen ruins, some of    "
dw " icy blocks have shown to be    "
dw " passable and vice versa after  "
dw " performing a special jump...   "
dw "                                "
dw "                                "

org $1940
dw "                                "
dw " Ice skating is fun! Turning    "
dw " can be a bit difficult and you "
dw " can't turn while jumping. You  "
dw " also have less traction, so    "
dw " be careful. Good luck!         "
dw "       ...You're gonna need it. "
dw "                                "
