table "table.txt"
norom
org $1140
dw "                                "
dw " Behold the latest in high-tech "
dw " security, Mario: Electricity   "
dw " generators, switch-activated   "
dw " beams, and laser shooters! If  "
dw " the electric doesn't get you,  "
dw " the lasers will!       -Norveg "
dw "                                "

org $1940
dw "                                "
dw " Dear Mario, take this Star to  "
dw " help you get past Norveg's     "
dw " guards. But beware, it won't   "
dw " protect you against the elect- "
dw " ricity traps. Good luck Mario, "
dw " I believe in you.       -Peach "
dw "                                "
