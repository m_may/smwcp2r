Asar 1.71, originally developed by Alcaro, maintained by Asar devs.
Source code: https://github.com/RPGHacker/asar

General Variables
-------------------------------------------------------------------------------
 
DZ_Timer $7F947B
DZ_MaxDataPerFrameIn16x16Tiles $7F947C
DZ_MaxDataPerFrame $7F947D
DZ_CurrentDataSend $7F947F
 
Dynamic Sprites Variables
-------------------------------------------------------------------------------
 
DZ_DS_Length $7F9481
DZ_DS_LastSlot $7F9482
DZ_DS_FirstSlot $7F9483
DZ_DS_MaxSpace $7F9484
DZ_DS_FindSpaceMethod $7F9485
DZ_DS_StartingVRAMOffset $7F9486
DZ_DS_StartingVRAMOffset8x8Tiles $7F9488
DZ_DS_TotalSpaceUsed $7F9489
DZ_DS_TotalSpaceUsedOdd $7F948A
DZ_DS_TotalSpaceUsedEven $7F948B
DZ_DS_TotalDataSentOdd $7F948C
DZ_DS_TotalDataSentEven $7F948D
 
Dynamic Sprites Local Variables
-------------------------------------------------------------------------------
 
DZ_DS_Loc_UsedBy $7F948E
DZ_DS_Loc_SpriteNumber $7F94BE
DZ_DS_Loc_SpaceUsedOffset $7F94EE
DZ_DS_Loc_SpaceUsed $7F951E
DZ_DS_Loc_IsValid $7F954E
DZ_DS_Loc_FrameRateMethod $7F957E
DZ_DS_Loc_NextSlot $7F95AE
DZ_DS_Loc_PreviewSlot $7F95DE
DZ_DS_Loc_SafeFrame $7F960E
DZ_DS_Loc_SharedUpdated $7F966E
DZ_DS_Loc_SharedFrame $7F963E
 
Dynamic Sprites Local Variables Used Slot Tables
-------------------------------------------------------------------------------
 
DZ_DS_Loc_US_Normal $7F969E
DZ_DS_Loc_US_Cluster $7F96AA
DZ_DS_Loc_US_Extended $7F96BE
DZ_DS_Loc_US_OW $7F96C8
 
Semi Dynamic Sprites Variables
-------------------------------------------------------------------------------
 
DZ_SDS_Offset_Normal $7F96D8
DZ_SDS_Offset_Cluster $7F96E4
DZ_SDS_Offset_Extended $7F96F8
DZ_SDS_Offset_OW $7F9702
DZ_SDS_PaletteAndPage_Normal $7F9712
DZ_SDS_PaletteAndPage_Cluster $7F971E
DZ_SDS_PaletteAndPage_Extended $7F9732
DZ_SDS_PaletteAndPage_OW $7F973C
DZ_SDS_Size_Normal $7F974C
DZ_SDS_Size_Cluster $7F9758
DZ_SDS_Size_Extended $7F976C
DZ_SDS_Size_OW $7F9776
DZ_SDS_Valid_Normal $7F9786
DZ_SDS_Valid_Cluster $7F9792
DZ_SDS_Valid_Extended $7F97A6
DZ_SDS_Valid_OW $7F97B0
DZ_SDS_SendOffset_Normal $7F97C0
DZ_SDS_SendOffset_Cluster $7F97CC
DZ_SDS_SendOffset_Extended $7F97E0
DZ_SDS_SendOffset_OW $7F97EA
DZ_SDS_SpriteNumber_Normal $7F97FA
DZ_SDS_SpriteNumber_Cluster $7F9806
DZ_SDS_SpriteNumber_Extended $7F981A
DZ_SDS_SpriteNumber_OW $7F9824
DZ_SDS_PaletteLoaded_Normal $7F9834
DZ_SDS_PaletteLoaded_Cluster $7F9840
DZ_SDS_PaletteLoaded_Extended $7F9854
DZ_SDS_PaletteLoaded_OW $7F985E
 
FreeRams
-------------------------------------------------------------------------------
 
DZ_FreeRams $7F986E
 
PPU Mirrors
-------------------------------------------------------------------------------
 
VRAM Mirrors
-------------------------------------------------------------------------------
 
DZ_PPUMirrors_VRAM_Transfer_Length $7F9C7B
DZ_PPUMirrors_VRAM_Transfer_SourceLength $7F9C7C
DZ_PPUMirrors_VRAM_Transfer_Offset $7F9D7C
DZ_PPUMirrors_VRAM_Transfer_Source $7F9E7C
DZ_PPUMirrors_VRAM_Transfer_SourceBNK $7F9F7C
 
You can use DZ_FreeRams = $7F986E ($7F986E-$7F9C7A) as freerams. Size of Freerams: 0x040D.
 
Assembling completed without problems.
