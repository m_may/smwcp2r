#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define MSG_SIZE 0x200
#define NUM_MESSAGES 6

static uint8_t characterMap[0x200];
static uint8_t msgBinWhole[MSG_SIZE*NUM_MESSAGES];
static uint8_t msgProps[MSG_SIZE/2];

static int convertBinToTextAndProps(uint8_t msgBin[MSG_SIZE], FILE * outputTextFile) {
	uint8_t insideQuotes = 0;

	for(int i = 0; i < MSG_SIZE/2; ++i) {
		if(i%0x20 == 0) {
			if(fputs("db \"", outputTextFile) == EOF) return -1;
			insideQuotes = 1;
		}

		uint16_t tile = (msgBin[i*2]|(msgBin[i*2+1]<<8))&0x3FF;
		uint8_t currentASCIICharacter = 0;
		if(tile < 0x200) {
			currentASCIICharacter = characterMap[tile];
		} else {
			fprintf(stderr, "Tile index %X above 0x1FF.\n", tile);
		}
		if(currentASCIICharacter == 0) {
			msgProps[i] = msgBin[i*2+1];
			if(insideQuotes != 0) {
				if(putc('"', outputTextFile) == EOF) return -1;
				insideQuotes = 0;
			}
			if(fprintf(outputTextFile, ",$%02X", tile&0xFF) < 0) return -1;
		} else {
			msgProps[i] = (msgBin[i*2+1]&0xFC)|0x01;
			if(insideQuotes == 0) {
				if(fputs(",\"", outputTextFile) == EOF) return -1;
				insideQuotes = 1;
			}
			if(putc(currentASCIICharacter, outputTextFile) == EOF) return -1;
		}

		if(i%0x20 == 0x1F) {
			if(fputs("\"\n", outputTextFile) == EOF) return -1;
			insideQuotes = 0;
		}
	}
	return 0;
}


static char TOO_LONG_PATH[] = "Output file would have too long path.";
static char ERROR_OPEN_WRTIE[] = "Couldn't open file for reading";
static char ERROR_WRITING[] = "Failed writing to file";

static void logFileError(char * path, char * message) {
	fprintf(stderr, "%s: ", path);
	perror(message);
}

static int readBin(char * path, uint8_t buffer[], int item_size, int * num_items) {
	FILE * fp = fopen(path, "rb");
	if(fp == NULL) {
		logFileError(path, "Couldn't open file for reading");
		goto readBin_error_return;
	}
	*num_items = fread(buffer, item_size, *num_items, fp);
	if(ferror(fp) != 0) {
		logFileError(path, "Failed reading file");
		goto readBin_error_cleanup;
	}
	fclose(fp);
	return 0;

readBin_error_cleanup:
	fclose(fp);
readBin_error_return:
	return -1;
}

static int writeBin(char * path, uint8_t buffer[], int buffer_size) {
	FILE * fp = fopen(path, "wb");
	if(fp == NULL) {
		logFileError(path, ERROR_OPEN_WRTIE);
		goto writeBin_error_return;
	}
	fwrite(buffer, buffer_size, 1, fp);
	if(ferror(fp) != 0) {
		logFileError(path, ERROR_WRITING);
		goto writeBin_error_cleanup;
	}
	fclose(fp);
	return 0;

writeBin_error_cleanup:
	fclose(fp);
writeBin_error_return:
	return -1;
}


static char * getPathLastComponent(char * path) {
	char * lastSlash = strrchr(path, '/');
	char * pathLastComponent;
	if(lastSlash == NULL)
		pathLastComponent = path;
	else
		pathLastComponent = lastSlash+1;
	return pathLastComponent;
}

static char * getExtension(char * path) {
	char * pathLastComponent = getPathLastComponent(path);
	return strrchr(pathLastComponent, '.');
}

static int getStringLength(char * string, int maxLen) {
	size_t len = strlen(string);
	//fuck you C and your comparisons and overflows and UBs, assembly handles it better
	if(len >= maxLen-1)
		return -1;
	return (int)len+1;
}

static int getStringLengthUntil(char * string, char * end, int maxLen) {
	ptrdiff_t n = end-string;
	if(n >= maxLen-1)
		return -1;
	return (int)n;
}


#define MAX_OUTPUT_PATH_LENGTH 4096
static char outputFilename[MAX_OUTPUT_PATH_LENGTH];

static char TEXT_SUFFIX[] = "-text.asm";
static char PROPS_SUFFIX[] = "-props.bin";

int main(int argc, char ** argv) {
	if(argc < 3) {
		fprintf(stderr, "Usage: text-conv [pathToCharacterMap] [pathToTilemap ...]\n");
		return -1;
	}
	int characterMapLength = sizeof(characterMap);
	//memset(characterMap, 0, sizeof(characterMap));
	if(readBin(argv[1], characterMap, 1, &characterMapLength))
		return -1;
	for(int i = 2; i < argc; ++i) {
		int num_messages = NUM_MESSAGES;
		readBin(argv[i], msgBinWhole, MSG_SIZE, &num_messages);

		char * inputFilename = getPathLastComponent(argv[i]);
		char * inputExtension = getExtension(inputFilename);
		int inputFilenameWithoutExtensionLength;
		if(inputExtension == NULL) {
			int n = getStringLength(inputFilename, MAX_OUTPUT_PATH_LENGTH);
			if(n < 0) {
				fprintf(stderr, "%s\n", TOO_LONG_PATH);
				return -1;
			}
			inputFilenameWithoutExtensionLength = n-1;
		} else {
			int n = getStringLengthUntil(inputFilename, inputExtension, MAX_OUTPUT_PATH_LENGTH);
			if(n < 0) {
				fprintf(stderr, "%s\n", TOO_LONG_PATH);
				return -1;
			}
			inputFilenameWithoutExtensionLength = n;
		}
		memcpy(outputFilename, inputFilename, inputFilenameWithoutExtensionLength);

		for(int j = 0; j < num_messages; ++j) {
			int l = inputFilenameWithoutExtensionLength;
			if(num_messages > 1) {
				if(l+2 >= MAX_OUTPUT_PATH_LENGTH) {
					fprintf(stderr, "%s\n", TOO_LONG_PATH);
					return -1;
				}
				outputFilename[l++] = '-';
				outputFilename[l++] = j+'1';
			}
			if(l+sizeof(PROPS_SUFFIX) >= MAX_OUTPUT_PATH_LENGTH) {
				fprintf(stderr, "%s\n", TOO_LONG_PATH);
				return -1;
			}
			strcpy(&outputFilename[l], TEXT_SUFFIX);
			FILE* outputTextFile = fopen(outputFilename, "w");
			if(outputTextFile == NULL) {
				logFileError(outputFilename, ERROR_OPEN_WRTIE);
				return -1;
			}
			if(convertBinToTextAndProps(&msgBinWhole[MSG_SIZE*j], outputTextFile) != 0) {
				logFileError(outputFilename, ERROR_WRITING);
				return -1;
			}
			fclose(outputTextFile);
			if(l+sizeof(PROPS_SUFFIX) >= MAX_OUTPUT_PATH_LENGTH) {
				fprintf(stderr, "%s\n", TOO_LONG_PATH);
				return -1;
			}
			strcpy(&outputFilename[l], PROPS_SUFFIX);
			if(writeBin(outputFilename, msgProps, sizeof(msgProps)))
				return -1;
		}
	}
	return 0;
}
