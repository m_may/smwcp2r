db "                                "
db " Look inside the eye of your    "
db " mind. Don't you know you might "
db " find another place to play?    "
db " You say that you'd never been, "
db " but all things that you've     "
db " seen will slowly fade away.    "
db "                                "
