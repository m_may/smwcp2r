#include <stdint.h>
#include <stdio.h>

uint8_t mode7interleaved[0x8000];

uint8_t tilemap[0x4000];
uint8_t gfx[0x4000];

uint8_t tilemapcropped[0x100];
uint8_t gfxreducedcolor[0x2000];

static void uninterleave(void) {
	for(int i = 0; i < sizeof(mode7interleaved)/2; ++i) {
		tilemap[i] = mode7interleaved[i*2];
		gfx[i] = mode7interleaved[i*2+1];
	}
}

static void tilemapCrop(void) {
	for(int j = 0; j < 0x10; ++j) {
		for(int i = 0; i < 0x10; ++i) {
			tilemapcropped[i + j*0x10] = tilemap[i + j*0x80];
		}
	}
}

static void gfxReduceColor(void) {
	for(int i = 0; i < sizeof(gfxreducedcolor); ++i) {
		gfxreducedcolor[i] = gfx[i*2] | gfx[i*2+1]<<4;
	}
}

static void compress(void) {
	uninterleave();
	tilemapCrop();
	gfxReduceColor();
}


static void logFileError(char * path, char * message) {
	fprintf(stderr, "%s: ", path);
	perror(message);
}

static int readBin(char * path, uint8_t buffer[], int buffer_size) {
	FILE * fp = fopen(path, "rb");
	if(fp == NULL) {
		logFileError(path, "Couldn't open file for reading");
		goto readBin_error_return;
	}
	fread(buffer, buffer_size, 1, fp);
	if(ferror(fp) != 0) {
		logFileError(path, "Failed reading file");
		goto readBin_error_cleanup;
	}
	fclose(fp);
	return 0;

readBin_error_cleanup:
	fclose(fp);
readBin_error_return:
	return -1;
}

static int writeBin(char * path, uint8_t buffer[], int buffer_size) {
	FILE * fp = fopen(path, "wb");
	if(fp == NULL) {
		logFileError(path, "Couldn't open file for writing");
		goto writeBin_error_return;
	}
	fwrite(buffer, buffer_size, 1, fp);
	if(ferror(fp) != 0) {
		logFileError(path, "Failed writing to file");
		goto writeBin_error_cleanup;
	}
	fclose(fp);
	return 0;

writeBin_error_cleanup:
	fclose(fp);
writeBin_error_return:
	return -2;
}


int main(void) {
	int ret;
	ret = readBin("ferris.bin", mode7interleaved, sizeof(mode7interleaved));
	if(ret != 0) return ret;
	compress();
	ret = writeBin("ferris-comp-tilemap.bin", tilemapcropped, sizeof(tilemapcropped));
	if(ret != 0) return ret;
	ret = writeBin("ferris-comp-gfx.bin", gfxreducedcolor, sizeof(gfxreducedcolor));
	return ret;
}
