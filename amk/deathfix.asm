;WARNING: This file is obsolete!
;See uberasmtool/gamemode/GoalDeath.asm instead.

; You can leave 140A, but anything that is cleared on level load works best
!FreeRAM = $140A

; DO NOT CHANGE THESE
!W1 = $00
!W2 = $01
!W3 = $02
!W4 = $03
!W5 = $04
!W6 = $05
!W7 = $06
!W8 = $07
!W9 = $08

; very important if the max size changes!!!!!!
!size = $01E5

!DefARAMRet = $044E
!UploadSPCDataDynamic = $8080F7

org $00D0B6     			; nuke old patch
	autoclean JML Code
	NOP #3
	
org $00F60A
	NOP #5

freecode
prot death1,death2,death3,death4,death5,death6,death7,death8,death9
     
Code:
	LDA !FreeRAM
	CMP #$01
	BEQ .return
	CMP #$02
	BEQ .jukebox
	LDX $13BF
	LDA.l Death_Table,x
	STA $00
.jukebox
	LDA #$FF			; adapted from patch.asm
	STA $2141
	SEI
	STZ $4200
	REP #$30
	LDA $00				; get the index
	AND #$00FF	
	ASL
	TAX
	SEP #$20
	LDA.l Death_Ptrs,x		; source data address
	STA $00
	LDA.l Death_Ptrs+1,x
	STA $01
	LDA #Death_Data>>16
	STA $02

	REP #$20
	LDA #!DefARAMRet		; return address
	STA $03
	
	LDA #!size			; size
	STA $05
	
	LDA [$00]			; destination ARAM address
	SEC
	SBC #$0004
	STA $07
	
	SEP #$30
	JSL !UploadSPCDataDynamic
	
	LDA #$81
	STA $4200
	CLI
	
	SEP #$30
	LDA #$01
	STA $1DFB
	LDA !FreeRAM
	BEQ .not_jukebox
	RTL				; return to jukebox
	
.not_jukebox
	INC !FreeRAM
	
.return
	STZ $19
	LDA #$3E
	STA $13E0
	JML $80D0BD

Death_Table:
	db $00									; level 0
	db !W1,!W1,!W1,!W1,!W1,!W1,!W1,!W1,!W1					; World 1, levels 001-009
	db !W2,!W2,!W2,!W2,!W2,!W2,!W2,!W2					; World 2, levels 00A-011
	db !W3,!W3,!W3,!W3,!W3,!W3,!W3						; World 3, levels 012-018
	db !W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4,!W4      ; World 4, levels 019-024 + 101-104
	db !W7									; World 7, level  105
	db $00,$00,$00,$00,$00,$00,$00,$00,$00					; World ?, levels 106-10E
	db !W5,!W5,!W5,!W5,!W5,!W5,!W5						; World 5, levels 10F-115
	db !W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6,!W6			; World 6, levels 116-121
	db !W7,!W7,!W7,!W7,!W7,!W7,!W7,!W7,!W7					; World 7, levels 122-12A
	db !W8,!W8,!W8,!W8,!W8,!W8,!W8,!W8					; World 8, levels 12B-132
	db !W9,!W9,!W9,!W9,!W9,!W9,!W9,!W9,!W9					; World 9, levels 133-13A
	db $00									; World ?, level  13B

Death_Ptrs:
	dw death1, death2, death3, death4, death5, death6, death7, death8, death9
	
Death_Data:
death1: incbin "death/death1.bin"
death2: incbin "death/death2.bin"
death3: incbin "death/death3.bin"
death4: incbin "death/death4.bin"
death5: incbin "death/death5.bin"
death6: incbin "death/death6.bin"
death7: incbin "death/death7.bin"
death8: incbin "death/death8.bin"
death9: incbin "death/death9.bin"
