;Revamped by Dippy

#amk 2

#spc
{
	#title "Burnt Ice Beats"
	#game "SMW Central Production 2"
	#author "Fyord"
	#comment "Fire/Ice Level 1"
}


#samples
{
	#default
	"CCymbal.brr"
    "bth/r8_hi-hat.brr"
}

#instruments
{
    "bth/r8_hi-hat.brr" 			    $FF $FC $B8 $10 $00 ; @30
    "bth/r8_hi-hat.brr" 			    $FF $F5 $B8 $10 $00 ; @31
	@8  			$FF $EC $B8 $0F $00     ;@32 Bass
	@1     			$F1 $E6 $B8 $03 $00     ;@33 Strings Slow
	@1     			$F2 $EA $B8 $03 $00     ;@34 Strings Fast
	"CCymbal.brr"		$FF $F1 $00 $05 $00		;@35 Cymbal
	@1      		$FF $E6 $B8 $03 $00     ;@36 Strings no atk
}

w255 t41 $F4$02 

$EF $3D $46 $46
$F1 $03 $10 $01

"K=@21 o4 v235 e"
"S=@29 o3 v190 a"
"SV=@29 o3 v150 a"
"H=@30 o3 y09 v150 e"
"O=@31 o3 y09 v150 e"
"C=@35 o4 y09 v155 c"

#0 / @32 l16 o2 v210 q6f y10 ;Bass
[ [[aaa4]]2a+aga+]8
r1r1
aaa4aa>c4cccc<aaa4aae4eeee
aaa4aa>c4cccc<aaa4aaeee8aaaa
aaa4aa>c4cccc<aaa4aae4eeee
aaa4aa>c4cccc<aaa4aaeee8aaaa
>ddd4dc<bagab>cd<b
>eee4edc<b>cdedce
ddd4dc<bag+feg+b>d
ccc4c<bab>cdeeee
ddd4dfafdagec+e
<aaa4a>cec<a>cec<a>c
eee4eg+bg+ebaf+d+f+
eee4eee4efef

#1 / l8 ;Percussion
(1)[K4S8.K8.K8S4]3
K4S8.K8 S32SV32S16 K16 [S16]4
(1)5 K4S8. K16 S16S K16 S16S16 S32SV32S16
(1)16

#2 / @14 v145 o2 l16 q6f y09 $EE$0A ;Bass 2
[ [[aaa4]]2a+aga+]8
@36 o2 v122
[aaar8.]2a+aga+a1
@14 v140 o2
aaa4aa>c4cccc<aaa4aae4eeee
aaa4aa>c4cccc<aaa4aaeee8aaaa
aaa4aa>c4cccc<aaa4aae4eeee
aaa4aa>c4cccc<aaa4aaeee8aaaa
>dddr8.dc<bagab>cd<b
>eee4edc<b>cdedce
ddd4dc<bag+feg+b>d
ccc4c<bab>cdeeee
ddd4dfafdagec+e
<aaa4a>cec<a>cec<a>c
eee4eg+bg+ebaf+d+f+
eee4eee4efef

#3 / @33 o4 v122 y08 p30,15,40
r1r1
a1^1 @34
v122 > a1^1 f1^1
@36 o3 
[aaar8.]2a+aga+a1
@0 o5 v140 y12
a8^16g16a8e8g8f+8e8d8
c16<b16a16b16>c8d8e4<g+4
>a8^16g16a8e8g8f+8e8d8
c8^16d16e8c8e16d16c16<b16a4
@36v115c16r8c16c8<a8a8a8a8g8
e16r16a16e16a8>c8e2
c16r8c16c8<a8a8a8a8g8
a4g4e4a4
@0 o4 v140 y12
>f8e16f32e32d8c8<b8a8g8>f8
e8d16e32d32c8<b8>c8d8e4
d8c16d32c32<b8a8g+8b8>f8e8
<[a16]4>[c16]4[e16]4v125[a16]4
@36 v115 o5
<d16r8d16d8c+8d8c+8<a4
a16r8a16a8e8a8>c8e4
e16r8e16e8<b8>e4d+4
e1

#4 / @34 o5 v115 y12 p30,15,40
[r2]8
r1 g1^1 d+1
@36 o4 v122 
[eeer8.]2fedfe1 >
v100
a8e8c8e8g8e8c8e8a8e8c8e8g+8e8d8e8
a8e8c8e8g8e8c8e8a8e8c8e8g8e8a4
v130y10<e16r8e16e8c8e8d8c8<b8
a16r16>c16<a16>c8e8a4g+4
e16r8e16e8c8e8d8c8<b8
>c4<b4a4>e4
o5v100y12f8d8<b8>d8<g8b8>d8f8
e8c8<g8>c8<e8g8>c8e8
d8<b8g+8b8e8g+8b8>d8
c4d4e2
@36 v130 y10 o4
f16r8f16f8e8f8e8d8c+8
c16r8c16c8<a8>c8e8a4
g+16r8g+16g+8e8g+4f+4
a2g+2

#5 / @2 o3 l16 v115
[r2]8
[$DC$60$00aaaa>aa<aa$DC$60$13aaaa>aaaa<]4
@36 o4 v120 y10 p30,15,40
[cccr8.]2 c+c<a+>c+c1
@34 o5 y09
a2g2a2g+2
a2g2@33a1
@36<v125a16r8g16a8e8g8f+8e8d8
c16r16e16c16e8a8>c4<b4
a16r8g16a8e8g8f+8e8d8
e4d4c4a4
@34 o5 y09 v120
f2d2e2c2
d2<b2>@33c1
@36 v125 <
a16r8a16a8g8a8g8f8e8
e16r8e16e8c8e8a8>c4
<b16r8b16b8g+8b4a4
b1

#6 / l16 ;Hats 
(2)[HHOH]38 C4.OH
(2)64

#7 / y08
[r2]20
@0 o5 r16. v85 $EE$0F
a8^16g16a8e8g8f+8e8d8
c16<b16a16b16>c8d8e4<g+4
>a8^16g16a8e8g8f+8e8d8
c8^16d16e8c8e16d16c16<b16a4
[r2]8
@0 o4 
>f8e16f32e32d8c8<b8a8g8>f8
e8d16e32d32c8<b8>c8d8e4
d8c16d32c32<b8a8g+8b8>f8e8
<[a16]4>[c16]4[e16]4v70[a16]4
r1r1r1v85 r2..^32
