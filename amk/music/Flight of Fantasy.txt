#amk 2
#SPC
{
	#title "Flight of Fantasy"
	#author "Magi"
	#comment "Abstract/Dream Level 1"
	#game "SMW Central Production 2"
}

; Samples:
; KDL3 Strings
; SMRPG Accordion
; @5
; CT Choir
; @7
; KSS Synth Voice
; @1
; KDL3 Bell
; @2
; @8
; KDL3 Timbale
; Kirby and the Amazing Mirror Percussion, although bass drum done with @0
; SMW percussion, like @29 for example
; Earthbound Jingle Jangle
; CT Cymbal
; Kirby Super Star Toms

;BlindEdit: downsampled a few to get #optimized in... they're not perfect but sacrifice is demanded

#path ""
#samples
{
#optimized
"Flight of Fantasy/CT Choir.brr"
"Flight of Fantasy/ct-crash.brr"
"jingleb.brr"
"Flight of Fantasy/KDL3 Bell.brr"
"Flight of Fantasy/KDL3 High Snare.brr" ; part of KirbyAmazingMirror soundfont pack
"Flight of Fantasy/kdl3s.brr"				;<<< "KDL3 Strings.brr"
"Flight of Fantasy/KDL3 Timbale.brr"
"Flight of Fantasy/Kirby Cowbell.brr"
"Flight of Fantasy/khh.brr"				;<<< "Kirby Hi-Hat.brr" ; open and closed
"Flight of Fantasy/ksn.brr"				;<<< "Kirby Snare.brr"
"Flight of Fantasy/ksv1.brr"				;<<< "Kirby Synth Voice1.brr"
"Flight of Fantasy/KSS Toms.brr"
"Flight of Fantasy/smrpga.brr"				;<<< "SMRPG Accordion.brr"
"Flight of Fantasy/kshaker.brr"				;<<< "Kirby Shaker.brr"
}

#instruments
{
"Flight of Fantasy/CT Choir.brr" $8A $E0 $00 $02 $D1 ;30
"Flight of Fantasy/ct-crash.brr" $83 $EE $00 $01 $C0 ;31
"jingleb.brr" $8F $f3 $00 $02 $7f ;32
"Flight of Fantasy/KDL3 Bell.brr" $8F $E0 $00 $03 $D0 ;33
"Flight of Fantasy/KDL3 High Snare.brr" $8F $E0 $00 $04 $C0 ;34
"Flight of Fantasy/kdl3s.brr" $8A $E0 $00 $03 $D0 ;35
"Flight of Fantasy/KDL3 Timbale.brr" $8F $E0 $00 $07 $80 ;36
"Flight of Fantasy/Kirby Cowbell.brr" $8F $E0 $00 $05 $40 ;37
"Flight of Fantasy/khh.brr" $8F $F4 $00 $05 $40 ;38 open
"Flight of Fantasy/khh.brr" $8F $FC $00 $05 $40 ;39 closed
"Flight of Fantasy/ksn.brr" $8F $E0 $00 $05 $40 ;40
"Flight of Fantasy/ksv1.brr" $8F $E0 $00 $01 $E4 ;41
"Flight of Fantasy/KSS Toms.brr" $8F $E0 $00 $04 $50 ;42
"Flight of Fantasy/smrpga.brr" $8F $E0 $00 $02 $8C ;43
@5  $8F $F3 $00 $07 $00 ;44
@7 $8F $F0 $00 $03 $00 ;45
@1 $8F $E0 $00 $03 $00 ;46
@2 $8F $E0 $00 $03 $00 ;47
@8 $8F $E0 $00 $0F $00 ;48
@0 $8F $F8 $00 $03 $00 ;49 use it for bass drum purposes
@12 $8F $E0 $00 $14 $00 ;50 bongo
@29  $8F $E0 $00 $02 $A0 ;51
"Flight of Fantasy/kshaker.brr" $8F $E0 $00 $05 $40 ;52
}

"Kick=@49 $FA$03$A0 $EB$00$09$E2 c"
"KickOff=$EB$00$00$00 $FA$03$00"

t44
w150
$F4$02

#0
v255
@45
o3
(2)[e12 a24 b12 > c+8 e8 c+24 < a12 b8 a24 b8 > c+8] <
(12)[e12 a24 b12 > c+8 e8 d24 c+12 < b8 a24 b4]
(2)
(11)[e12 a24 b12 > c+8 e8 f24 g12 f8 e24] d4  >
/
@41
$ED$09$E0 v180 y11
e4 c+4 < a4 > c+4 < a4 e4 a4 > c+4 < b4 g4 b4 > d4 < a+4 g4 > d4 < b4


@45 y10
(1)[v255 c+12 r24 v150 c+24 r24 v255 e12 r24 v150 e24 
v255 c+12v150 c+24
v255 f12 g24. v150 g24. v255 f24 e12 v150 e24 v255 d12 v150 d24]
(3)[v255 c+12 r24 v150 c+24 r24 v255 e12 r24 v150 e24 <
v255 a12v150 a24 v255 b12v150 b24 > v255 c+12v150 c+24
v255 e12v150 e24 r8] <
(1)
(6)[v255 a12 > c+24 e12 f8 g8 f24
l32 e v150 $E8$27$DD eeeeeee d v150 $E8$27$FF ddddddd]

@41 $FA$02$0C (1) (3)
(7)[v255 c+12 r24 v150 c+24 r24 v255 e12 r24 v150 e24 
v255 d12v150 c+24
v255 g12 a24 r48 v150 a24 r48 v255 g24
f12 v150 f24 v255 e12 v150 e24<
v255 a12 > c+24 e12$FA$02$00 > g+8$DD$00$0Ca g8] f=7q7f^=1 f4$DD$18$18e  e4

@33 $DE$10$0C$20 (4)[v255 f8^12 g24 r12 v150 g24 v255 f8^4 d8 c-12 d24
f8^12 g24 r12 v150 g24 v255] a8^4 (8)[g8 r12 v150 g24
v255 e8^12 g24 r12 v150 g24 v255 e8^4 c+8 < a12 > c+24
e8^12 g24 r12 v150 g24 v255 e8^4 v150 e4]
(4)  b8^4 g12. r12 v150 g24
v255 c+8^12 e24 r12 v150 e24 v255 a8^4 d8 < b12 > e24
g8^12 d24 r12 v150 d24 v255 e8^4 v150 e4

@45  <$DF
(5)[v255 e12 a24 b12 > c+24. v150 c+24. v255 e24. v150 e24.
v255 c+24 < a12 b24. v150 b24. v255 a24 b12 v150 b24 > v255 c+12 v150 c+24] < 
(20)[v255 e12 a24 b12 > c+24. v150 c+24. v255 e24. v150 e24.] v255 d24 c=4 c+=12 < b24. v150 b24. v255 a24 b8 v150 b8
(5)
(20) > v255 f24 f+=4 g=12 f24. v150 f24. v255 e24 d8 v150 d8
(5)
(20) v255 c+24 f+=4 f=12 e24. v150 e24. v255 c+24 c-8 v150 c-8
(5)
v255 d+=4 e=12 c+24 c-12 c+24. v150 c+24. < v255 b24 v150 b24 r24 v255 a24 g8 v150 g8 v255 f8 v150 f8

@45
(1)
(3)
(1)
(6)

@41 $FA$02$0C (1) (3)
(7) > f24^4 g4
(4) g8$DD$00$0Ca^4 (8)
(4) $DF a8$DD$00$0Cb^=40^=8$DD$00$08a g12 r12. v150 g24 <
v255 a8^12 > e24 r12 v150 e24 v255 a8^4 d8 < a12 > c+24
e8^12 g24 r12 v150 g24 v255 g+8$DD$00$0Aa^4$E8$30$00^4 <
v255
(10)[e12 a24 b12 > c+8 d8$DD$00$08e c+24 < a12 b8 a24 b8 > c+8] <
e12 a24 b12 > c+8 e8 c+24 f+12$DD$00$0Cf e8 c+24 < a4 >
(10)
e12 c+24 c-12 c+8 < b8$DD$0C$0Ca a24 g4 a4

@35 $FA$02$0C
(2)(12)(2)(11) $FA$02$00 >d8 c+8
(13)[e8^12 c+24 r12 v150 c+24 v255 < a4
f12 r24 d12 f24 a12 b24 > c+8^12 d24 r12 v150 d24
v255 e8^4] d4
c+8^12 e24 r12 v150 e24 < v255 a4
g12 r24 f12 a24 g12 f24
g8^12 b24 r12 v150 b24 > v255 c+8^2

(13) f4 
g8^12 b24 r12 v150 b24 v255 a4
g12 v150 g24 v255 f12 a24 g12 f24
e8^12 g24 r12 v150 g24  v255 a8$E8$60$50^2

$FA$02$00

@45  v255 (2)(12)(2)(11) < d4

#1
@30 o5 y12 v200
(14)[$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 c+4$E8$50$C8^2. < $E8$30$A0 a4$E8$50$C8^2. $E8$30$A0 e4$E8$30$AC^4 $E8$30$BA d4 $E8$30$C8 f4] >
/
@43
$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 d4$E8$50$C8^2. $E8$30$A0 d4$E8$30$AC^4 $E8$30$BA d4 $E8$30$C8 f4
(15)[$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 c+4$E8$50$C8^4 $E8$30$A0 f4$E8$50$C8^4 $E8$30$A0 e4$E8$50$C8^2.] $E8$30$A0 c+4$E8$30$AC^4 $E8$30$BA c-4 $E8$30$C8 d4 <
$E8$30$A0 a4$E8$50$C8^2. $E8$30$A0 > c+4$E8$50$C8^4 $E8$30$A0 f4$E8$50$C8^4 $E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 e4$E8$30$AC^4 $E8$30$BA c+4 $E8$30$C8 e4 <
(18)[$E8$60$C8 a2$E8$60$A0^2 $E8$60$C8 f2$E8$60$A0^2 $E8$60$C8 g2$E8$60$A0^2 $E8$60$C8 e2$E8$60$A0^2
$E8$60$C8 a2$E8$60$A0^2 $E8$60$C8 f2$E8$60$A0^2 $E8$30$C8 g4. v160 $E8$60$C8 a8^2 ]v160 $E8$30$C8 g4. v160 $E8$60$C8 e8^2
(14) >
$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 c+4$E8$50$C8^4 $E8$30$A0 f4$E8$50$C8 f4 $E8$30$A0 a4$E8$50$C8^2. $E8$30$AA e4$E8$30$C8^4 $E8$30$AA d4 $E8$30$C8 f4
@35 (15) $E8$30$A0 c+4$E8$30$AC^4 $E8$30$BA c-4 $E8$30$C8 d4
(15) $E8$30$A0 e4$E8$30$AC^4 $E8$30$BA c+4 $E8$30$C8 g4


@30 (18) <v160 $E8$30$C8 e4. v160 $E8$60$C8 e8^2 >

@43 (55)[c+8 q1f c+12 q7f e12. q2f c+24 q7f e8] (56)[q1f e12 q7f c+24 e8 c+12 q2f c+24 q7f e8] 
(57)[q7f c+8 q1f c+12 q7f c+12. q2f < a24 q7f > c+8 q1f e12 q7f c+24 e8 < a12 q2f a24 q7f > c+8 <]
(58)[q7f a8 q1f a12 q7f a12. q2f e24 q7f a8 q1f a12 q7f e24 a8 e12 q2f e24 q7f a8]
(59)[q7f e8 q1f e12 q7f e12. q2f c+24 q7f e8 q1f d12 q7f c-24 d8 d12 q2f d24 q7f f8]

(14) >
$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 < b4$E8$18$C8^8 $E8$30$A0 a4$E8$18$C8^8 $E8$18$A0 g8$E8$18$C8^8 >
$E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 < b4$E8$18$C8^8 $E8$30$A0 > c+4$E8$18$C8^8 $E8$30$A0 ^4
$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 < g4$E8$18$C8^8 $E8$30$A0 e4$E8$18$C8^8 $E8$18$A0 f8$E8$18$C8^8
$E8$30$A0  g4$E8$18$C8^8 $E8$30$A0 a4$E8$18$C8^8 $E8$30$A0 ^4 $E8$30$A0  e4$E8$18$C8^8 $E8$30$A0 e4$E8$18$C8^8 $E8$30$A0 ^4 

@30 v200 (14)

#2
@30 o5 y8 v200
(16)[$E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 < a4$E8$50$C8^2. $E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 c+4$E8$30$AC^4 $E8$30$BA c-4 $E8$30$C8 d4] >
/
@43
$E8$30$A0 c+4$E8$50$C8^2. < $E8$30$A0 a4$E8$50$C8^2. $E8$30$A0 b4$E8$50$C8^2. $E8$30$A0 a+4$E8$30$AC^4 $E8$30$BA b4 $E8$30$C8 > d4 <
(17)[$E8$30$A0 a4$E8$50$C8^2. $E8$30$A0 g4$E8$50$C8^4 $E8$30$A0 > c+4$E8$50$C8^4 $E8$30$A0 < a4$E8$50$C8^2.] $E8$30$A0 g4$E8$30$AC^4 $E8$30$BA g4 $E8$30$C8 b4
$E8$30$A0 e4$E8$50$C8^2. $E8$30$A0 g4$E8$50$C8^4 $E8$30$A0 > c+4$E8$50$C8^4 $E8$30$A0 < g4$E8$50$C8^2. $E8$30$A0 > c+4$E8$30$AC^4 $E8$30$BA < a4 $E8$30$C8 > c+4 <
(19)[$E8$60$C8 f2$E8$60$A0^2 $E8$60$C8 d2$E8$60$A0^2 $E8$60$C8 e2$E8$60$A0^2 $E8$60$C8 d2$E8$60$A0^2
$E8$60$C8 f2$E8$60$A0^2 $E8$60$C8 d2$E8$60$A0^2 $E8$30$C8 e4. v160 $E8$60$C8 g8^2 v160 $E8$30$C8 d4.] v160 $E8$60$C8 d8^2 >
(16)
$E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 < a4$E8$50$C8^4 > $E8$30$A0 c+4$E8$50$C8 d4 $E8$30$A0 e4$E8$50$C8^2. $E8$30$AA c+4$E8$30$C8^4 $E8$30$AA < a4 $E8$30$C8 > d4 <
@35 (17) $E8$30$A0 g4$E8$30$AC^4 $E8$30$BA g4 $E8$30$C8 b4
(17) $E8$30$A0 > c+4$E8$30$AC^4 $E8$30$BA < a4 $E8$30$C8 > c+4

@30 (19) v160 $E8$60$C8 < c+8^2

@43 (62)[a8 q1f a12 q7f > c+12. < q2f a24 q7f > c+8] (61)[q1f c+12 < q7f a24 > c+8 < a12 q2f a24 q7f > c+8 <]
q7f a8 (63)[q1f a12 q7f a12. q2f e24 q7f a8 q1f > c+12 q7f < a24 > c+8 < e12 q2f e24 q7f g+8]
(64)[q7f e8 q1f e12 q7f e12. q2f c+24 q7f e8 q1f e12 q7f c+24 e8 c+12 q2f c+24 q7f e8]
(65)[q7f c+8 q1f c+12 q7f c+12. q2f < a24 q7f > c+8 q1f < b12 q7f g24 b8 a12 q2f a24 q7f  > d8]

(16) >
$E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 < e4$E8$18$C8^8 $E8$30$A0 e4$E8$18$C8^8 $E8$18$A0 d8$E8$18$C8^8
$E8$30$A0 a4$E8$50$C8^2. $E8$30$A0 g4$E8$18$C8^8 $E8$30$A0 a4$E8$18$C8^8 $E8$30$A0 ^4 >
$E8$30$A0 c+4$E8$50$C8^2. $E8$30$A0 < e4$E8$18$C8^8 $E8$30$A0 < b4$E8$18$C8^8 $E8$18$A0 > c+8$E8$18$C8^8
$E8$30$A0  e4$E8$18$C8^8 $E8$30$A0 f4$E8$18$C8^8 $E8$30$A0 ^4 $E8$30$A0  d4$E8$18$C8^8 $E8$30$A0 c+4$E8$18$C8^8 $E8$30$A0 ^4

@30 v200 (16)

#3
@48
o2
a1 e1 > c+1 < a2 g4 a4
/
(21)[a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 < a24. r48 > a24. r24. < a24 > a24. r48 a24 < a24. r48 > a24]
$FA$02$FB (21)
$FA$02$FE (21) <
a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 < g24. r48 > g24. r24. < g24 >> c+24. r48 c+24 < c+24. r48 > c+24 <
$FA$02$F8 (21)
$FA$02$FB < (22)[a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 d24. r48 > d24. r24. < d24 > d24. r48 d24 < d24. r48 > d24] <<
$FA$02$F8 (21)
$FA$02$FB a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 < a24. r48 > a24. r24. < a24 >> c24. r48 c24 < e24. r48 > e24 <
$FA$02$F8 (21)
$FA$02$FB (22)
$FA$02$F8 (21)
$FA$02$00 < (25)[a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 < f24. r48 > f24. r24. < f24 > a24. r48 a24 < a24. r48 > a24]
(23)[d24. r48 d24. r24. d24 > d8 < d8 r12 d24 d24. r48 d24 > d24. r48 < d24 d24. r48 > d24]
$FA$02$FD (23)
$FA$02$FF (23)
$FA$02$FB (23) 
$FA$02$00 (23) 
$FA$02$FD (23)
< e24. r48 e24. r24. e24 > e8 < g8 r12 g24 g24. r48 g24 > g24. r48 < g24 g24. r48 > g24 <<
$FA$02$00 b24. r48 b24. r24. b24 > b8 < b8 r12 b24 b24. r48 > b24 > b24. r48 < b24 b24. r48 > b24 <<
(24)[a24. r48 a12. a12. > a24 < a24. r48 > a24 < a24. r48 > a12. < a24 > a24. r48 a24 < a24. r48 > a24]
$FA$02$FB (24)
$FA$02$04 (24)
$FA$02$00 < (26)[a24. r48 a12. a12. > a24 < a24. r48 > e24 < g24. r48 > g12. < g24 > f8 < f24. r48 >> d24]
(24) <
e24. r48 e12. e12. > e24 < e24. r48 > e24 << a24. r48 > a12. < a24 > b24. r48 b24 < b24. r48 > b24
$FA$02$04 (24) <
$FA$02$00 a24. r48 a12. a12. > a24 < a24. r48 > a24 < g24. r48 > g12. < g24 > f24. r48 f24 < f24. r48 > a24 

$FA$02$F8 (21)
$FA$02$FB (22)
$FA$02$F8 (21)
$FA$02$FB < a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > a24 < a24. r48 > a24. r24. < a24 >> c24. r48 c24 < c24. r48 > e24

$FA$02$F8 (21)
$FA$02$FB (22)
$FA$02$04 (21)
$FA$02$00 (25)
<  d1 c-1 c+1 < a1 > d1 c-1 c+4. e8^2 < a4. > a8^2

(24)
< e24. r48 e12. e12. > e24 < e24. r48 > e24 < a24. r48 > a12. < a24 > e24. r48 e24 < e24. r48 > e24
$FA$02$04 (24)
$FA$02$00 (26)

(24)
$FA$02$FB (24)
$FA$02$04 (24)
$FA$02$00 (26)


< a8 r8 a8 r8 a24. r48 > d24 e8 < a8 > a8 <
a8 r8 a24. r48 d24 > a8 d8 > d8 << a+8 > a+8 <
e8 r8 e8 r8 e24. r48 a24 > c+8 < e8 > e8 <
e8 r8 > e8 r8 f+24. r48 f+24 > f+8 < f+8 > f+8 <<
[a8 r8 a8 r8 a24. r48 a24 > a8 < a8 > a8]
c+8 r8 c+8 r8 < a24. r48 a24 > a8 < a8 >> c+8 <
c-8 r8 c-8 r8  d24. r48 d24 > d8 < d8 > f8
*

(21)
$FA$02$FB (21)
$FA$02$04 (21)
$FA$02$00 << a24. r48 a24. r24. a24. r24. > a24 < a24. r48 > e24 < g24. r48 > g12. < g24 > f8 < f24. r48 >> d24

#4
v170 y9 (47)[@33 $ED$0F$EA
$DE$10$0B$28
o6  e1 c+1 < a1] e1

/
q7f 
@33 v170 y11 c+2. (99)[e4 < a2. > c+4 c-2. d4 < a+2 > d2] <
r1^2^8 (50)[v170 a24. r48 v130 a24 v170 b24. r48 v130 b24 > v170 c+24. r48 v130 c+24
v170 e4 v130 e4 v70  e4 r4^2] <
v170 a12 > c+24 e12 g8 f8 d24
c+4 v140 c+4 v100 c+4 r4
r2^8 < (52)[v170 a24. r48 v130 a24 > v170 c+24. r48 v130 c+24 v170 f24. r48 v130 f24
v170 e4 v130 e4 v70  e4 r4^2^8 <
v180 a12 > c+24 f12 < a24 > c+12 e24]

@46
$ED$0F$F1
v255  q5f y12
(28)[ [[f24. r48 d24 < a24. r48 > f24 $DC$30$08 d24. r48 < a24]]2 > $DC$30$0C f24. r48 d24 < a24. r48 f24> ] 
(27)[ [[f24. r48 d24 < b24. r48 > f24 $DC$30$08 d24. r48 < b24]]2 > $DC$30$0C f24. r48 d24 < b24. r48 f24] >
$FA$02$02 (27)
$FA$02$00 (33)[ [[e24. r48 d24 < a24. r48 > e24 $DC$30$08 d24. r48 < a24]]2 $DC$30$0C e24. r48 a24 > d24. r48 e24]
(28)
(27)
(34)[g24. r48 e24 d24. r48 g24 $DC$30$08 e24. r48 d24 a24. r48 g24 e24. r48 a24 g24. r48 e24  $DC$30$0C a24. r48 g24 e24. r48 a24]
g24. r48 d24 c-24. r48 g24 $DC$30$08 d24. r48 c-24 e24. r48 d24 c-24. r48 e24 d24. r48 c-24  $DC$30$0C e24. r48 d24 c-24. r48 e24 

@47 v180
(29)[ [[e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24]]2 > $DC$30$0C e24. r48 c+24 < a24. r48 e24 >]
(37)[ [[c+24. r48<  a24 e24. r48 > c+24 $DC$30$08 < a24. r48 e24]]2 > $DC$30$0C c+24. r48 < a24 e24. r48 a24]
(30)[ [[a24. r48 e24 c+24. r48 a24 $DC$30$08 e24. r48 c+24]]2 $DC$30$0C a24. r48 e24 c+24. r48 < a24 >]
(35)[e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24 > e24. r48 c+24   g24. r48 > d24 < b24. r48 g24 >  $DC$30$0C f24. r48 d24 < a24. r48 > f24]

(29)
c+24. r48<  a24 e24. r48 > c+24 $DC$30$08 < a24. r48 e24 > c+24. r48<  a24 > f24. r48  c+24 <a24. r48  > f24 $DC$30$0C f24. r48  d24 <b24. r48  f24
$FA$02$0C (30)
$FA$02$00 > e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24 > e24. r48 c+24  < g24. r48 > d24 < b24. r48 g24 > f24. r48 $DC$30$0C d24 < a24. r48 > f24

@46
$ED$0F$F1 v220
(31)[ [[e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24]]2 > $DC$30$0C e24. r48 c+24 < a24. r48 > c+24]
(32)[g24. r48 e24 c+24. r48 g24 $DC$30$08 e24. r48 c+24 g24. r48 e24 < a24. r48 > c+24  < a24. r48 f24 > $DC$30$0C c+24. r48 < a24 f24. r48 > c+24]
(31)
 g24. r48 e24 c+24. r48 g24 $DC$30$08 e24. r48 c+24 g24. r48 e24 < g24. r48 b24  g24. r48 e24 > $DC$30$0C d24. r48 < b24 g24. r48 > d24

(31)
(32)
[[g24. r48 e24 c+24. r48 g24 $DC$30$08 e24. r48 c+24]]2 $DC$30$0C g24. r48 e24 c+24. r48 < a24
> e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24  > e24. r48 c+24 < f24. r48 > c+24  < a+24. r48 f24 > $DC$30$0C e24. r48 c+24 < a24. r48 > c+24

@33$FA$02$F4  v180
(28)
(27)
$FA$02$F6  (27)
$FA$02$F4 (33)

(28)
(27)
(34)
(36)[e24. r48 d24 < a24. r48 > e24 $DC$30$08 d24. r48 < a24 > e24. r48 d24 < a24. r48 > e24 c+24. r48 < a24  $DC$30$0C > e24. r48 c+24 < a24. r48 > e24]

@46
$ED$0F$F1 v220
$FA$02$00

(29)
c+24. r48<  a24 e24. r48 > c+24 $DC$30$08 < a24. r48 e24 > c+24. r48<  a24 > f24. r48  c+24 <a24. r48  > f24 $DC$30$0C e24. r48  c+24 <a24. r48  e24
(30)
(35)

q7f
@33
r2 y9 
v200 a12 b24 v150 y11 b12 v200 y9 a24 b12 v150 y11 b24 > v200 y9 c+12 v150 y11 c+24
v200 y9 e4 v150 y11 e4 <
v200 a12 b24 v150 y9 b12 v200 y11 a24 > b12 v150 y9 b24 v200 y11 g12 v150 y9 g24
v200 y11 e4 v150 y9 e4 <
v200 a12 b24 v150 y11 b12 v200 y9 a24 b12 v150 y11 b24 v200 y9 a12 v150 y11 a24 >
v200 y9 c+4 v150 y11 c+4 v200 y9 d4 v150 y11 d4

q5f @47 v180
[ [[e24. r48 c+24 < a24. r48 > e24 $DC$30$08 c+24. r48 < a24]]2 > $DC$30$0C e24. r48 c+24 < a24. r48 > c+24] <
b24. r48 a24 e24. r48 b24 $DC$30$08 a24. r48 e24 a24. r48 e24 d24. r48 a24 e24. r48 d24  $DC$30$0C a+24. r48 g24 d24. r48 a+24
(30) 

b24. r48 g24 e24. r48 b24 $DC$30$08 g24. r48 e24 > c+24. r48 < a24 f+24. r48 > c+24 < a24. r48 f+24  $DC$30$0C > c+24. r48 < a24 f+24. r48 > c+24
@31 y10 $ED$0F$EE v255 e8 <  y12 @47 v180 a24. r48 > e24 $DC$30$08 c+24. r48 < a24 > e24. r48 c+24 < a24. r48 > e24 c+24. r48 < a24 > $DC$30$0C e24. r48 c+24 < a24. r48 > e24
g24. r48 e24 c+24. r48 g24 $DC$30$08 e24. r48 c+24 e+24. r48 < b24 a24. r48 > e24 < b24. r48 a24  $DC$30$0C > e24. r48 c+24 < a24. r48 > c+24
g24. r48 e24 c-24. r48 g24 $DC$30$08 e24. r48 c-24 a24. r48 f24 d24. r48 a24 f24. r48 d24  $DC$30$0C a24. r48 f24 d24. r48 f24 
(36)

(29)
(37)
(30)
(35)

#5
r4 y11 v140 (47) e4
$DF
o4
@42 v255
y14 b+12 b+8 y13 a48 a48 y12 g12 y11 f24 y10 d+12 y9 c24
/
y10
(38)[Kick12 KickOff @39 c24 q7f @38 c24$ED$0F$FD^24 @40 c24
@38 c24$ED$0F$FD^24 Kick24 KickOff @40 c12 @38 c24
Kick12 KickOff @34 g+24 @38 c12 Kick24 KickOff
@40 c12 @39 c24 @37 c12 c24]3

Kick12 KickOff @39 c24 q7f @38 c24$ED$0F$FD^24 @40 c24
@38 c24$ED$0F$FD^24 Kick24 KickOff @40 c12 @38 c24
Kick12 KickOff @34 g+24 @38 c12 Kick24 KickOff
@40 c12 @39 c24 @42 a12 f24

(39)[Kick12^8 KickOff @40 c8
 Kick24 KickOff @40 c12 r24
Kick12 KickOff @34 g+8 Kick24 KickOff
@40 c12 @39 c24 @37 c12 c24]3 

Kick12 KickOff @34 g+24 @39 c12 @34 c8
Kick24 KickOff @40 c12 @39 c24
Kick12 KickOff @34 g+24 @39 c12 Kick24 KickOff
@40 c12 @39 c24 @36 c12 c24

(38)4

[[ (46)[Kick12 KickOff] @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick8^12 KickOff
@50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick12 KickOff @34 g+24

(46) @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick8^12 KickOff
@52 c24 @34 g+12 Kick24 KickOff @34 g+12 @38 c24 @37 c12 c24]]3

(46) @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 (46) r8
@50 c24 @52 c12 @40 c24 @37 c12 @38 c24 * @34 g+24

* @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick8 KickOff
@42
y12 b12 y11 f24 y9 c-12 y12 b24 g+12 y11 e24 y9 d12 c-24
y10 
[[ (42)[Kick12 KickOff @39 c24 q7f @38 c24$ED$0F$FD^24 $FA$03$A0 @51 c24 $FA$03$00
@38 c24$ED$0F$FD^24 Kick24 KickOff $FA$03$A0 @51 c12 $FA$03$00 @38 c24
Kick12 KickOff @34 g+24 @38 c12 Kick24 KickOff
@40 c12 @39 c24 $FA$03$A0 @51 c8 $FA$03$00]
(46) @39 c24 q7f @38 c24$ED$0F$FD^24 $FA$03$A0 @51 c24 $FA$03$00
@38 c24$ED$0F$FD^24 Kick24 KickOff $FA$03$A0 @51 c12 $FA$03$00 @38 c24
(46) @34 g+24 @38 c12 Kick24 KickOff
@40 c12 @39 c24 $FA$03$A0 @51 c12 c24 $FA$03$00]]4

(39)3

Kick8 KickOff @45 q76 < a12 > c+24 e12 f8 g8 f24 e8^12 @39 q7f c24 @36 c12 c24

(38)4

Kick1 c1 c1 c2^8 KickOff @39 c12 @38 c24 @37 c24 c24 c24 c12 @36 c24

(40)[Kick12 KickOff @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick8^12 KickOff
@50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick12 KickOff @34 g+24]

(41)[Kick12 KickOff @50 c24 @52 c12 @40 c24 @37 c12 @38 c24 Kick8 KickOff]
@52 r12 c24 @34 c12 Kick24 KickOff @34 c12 @38 c24 @37 c12 c24 

(40)

(41) @42 y13 b+12 b+24 y12 a12 y13 b+24 y12 a12 y11 g24 y9 e12 y8 c+24
y10 
(42)3
(43)[Kick12 KickOff @39 c24 q7f @38 c24$ED$0F$FD^24 $FA$03$A0 @51 c24 $FA$03$00
@38 c24$ED$0F$FD^24 Kick24 KickOff $FA$03$A0 @51 c12 $FA$03$00 @38 c24]
r2
(42)3
(43)
@42 y11 g12 y10 f24 y9 c-12 y11 g24 y10 f12 y9 c-24 y11 g12 y9 c-24

y10 
(44)[Kick8 KickOff @38 c8 Kick12 KickOff @39 c24 @38 c12 @39 c24
Kick12 KickOff @39 c24 @38 c8 $FA$03$A0 @51 c8 $FA$03$00 @38 c8]

(45)[Kick8 KickOff @38 c12 @39 c24 Kick12 KickOff @39 c24 @38 c12 @39 c24
Kick12 KickOff @39 c24 @38 c8] $FA$03$A0 @51 c4 $FA$03$00

(44)

(45) @42 > y12 c+12 < y11 a24 y10 f12 y9 c+24
y10 
(44) (45)  $FA$03$A0 @51 c4 $FA$03$00 (44)

(45) Kick8 KickOff @38 c8

(38)4

#6
r8^12
v160
@45
o3
[e24 a12 b24 > c+8 e8 c+12 < a24 b8 a12 b8 > c+8] <
e24 a12 b24 > c+8 e8 d12 c+24 < b8 a12 b4
*
e24 a12 b24 > c+8 e8 f12 g24 f8 e12 d24
/<<< 
(60)[@35 v255 $E8$90$CC a1 v255 $E8$90$CC e1 v255 $E8$90$CC g1 v255 $E8$50$EE a+2 v200 $E8$40$FF f4 b4]
>>>> [@32 v255 c8 v200 c8 v150 c8 v100 c8 [[r2]]7 ]1
v200 @47(31)(32)
q5f g24. r48 e24 c+24. r48 g24 $DC$30$08 e24. r48 c+24 g24. r48 e24 c+24. r48 g24 e24. r48 c+24 $DC$30$0C g24. r48 e24 c+24. r48 < a24
> e24. r48 c+24 < a24. r48 > $DC$30$08 e24 c+24. r48 < a24 > e24. r48 c+24 < a24. r48 > f24 c+24. r48 < a24 $DC$30$0C > e24. r48 c+24 < a24. r48 > e24

@41 $ED$0A$E0 y12 q7f v180 (66)[$DC$30$08 f4 d4 < $DC$30$0C a4 > d4 $DC$30$08 f4 d4 < $DC$30$0C b4 > d4 $DC$30$08 g4 e4]
$DC$30$0C c+4 g4 $DC$30$08 e4 d4 < $DC$30$0C a4 > d4
(66) $DC$30$0C g4 a4 $DC$30$08 g4 d4 $DC$30$0C c-4 e4 y10
(51)[@32 v255 c8 v200 c8 v150 c8 v100 c8] @41 < (48)[y9 v200 a12 b24 v120 b12 v200 a24 b12 v120 b24 > v200 c+12 v120 c+24 v200 e4 v120 e4] <
 (54)[y11 v200 a12 b24 v120 b12 v200 a24 > b12 v120 b24 v200 g12 v120 g24 v200 e4 v120 e4] <
 (49)[y9 v200 a12 b24 v120 b12 v200 a24 b12 v120 b24 v200 a12 v120 a24 > v200 c+4] v120 c+4  v200 d4 v120 d4
y10 (51) @41 y9 (48)
y11 (53)[v200 f+12$DD$06$06f e24 v120 e12 v200 f24 a12 v120 a24 v200 f12 v120 f24 v200 c+4 v120 c+4]
(49) < b4 g4 > d4 $E8$C0$50 c+2 r1^8 <
@33 (50) v170 y10 a12 > c+24 e12 g8 f8 d24 (51) r1^8
@33 (52)
@35 y12 (66) $DC$30$0C c+4 g4 $DC$30$08 e4 d4 < $DC$30$0C a4 > d4
(66) $DC$30$0C a4 g4 $DC$30$08 d4 e4 $DC$30$0C c+4 < a4 y10
(51) @33  (48) (53) (49) > v120 c+4  v200 d4 @31 v255 $ED$05$EE y10 c4^2
@44 y9 (56) (67)[q7f c+8 q1f c+12 q7f c+12. q2f < a24 q7f > c+8 q1f c+12 q7f < a24 > c+8 < a12 q2f a24 q7f > c+8] (58) (59)
(67)
< e8 q1f e12 q7f e12. q2f < a24 q7f > e8 q1f e12 q7f < a24 > e8 e12 q2f < a24 > q7f e8
(58)
g8 q1f g12 q7f g12. q2f e24 q7f a8 q1f a12 q7f f+24 a8 f+12 q2f f+24 q7f a8 >
(67)
e8 q1f e12 q7f e12. q2f c+24 q7f < b8 q1f b12 q7f a24 b8  > c+12 q2f c+24 q7f f8
e8 q1f e12 q7f e12. q2f c-24 q7f f8 q1f f12 q7f d24 f8  d12 q2f d24 q7f f8
d8 q1f d12 q7f d12. q2f < a24 q7f > c+8 q1f c+12 q7f < a24 > c+8 < a12 q2f a24 > q7f c+8
y10 @35 v255 $E8$90$CC <<<< a1 v255 $E8$90$CC e1 v255 $E8$90$CC > c+1 v255 $E8$50$EE < a2 v200 $E8$40$FF g4 f4

#7
r1^1^1^1
/ $FA$02$00 o5
(51) r4. @33 v130 y9 (99) r4. [r2]7
(51) [r2]7
(51) [r2]6
@31 v255 q7f c1^2 [[r2]]2 @33 $DE$10$0C$20
< q7a g8^12 > c+24 r12 v150 c+24 v255 e8^4 < b8 a12 > c+24
d8^12 c-24 r12 v150 c-24 v255 c-8^4 v150 c-4 $DF
[r2]16 q7f
(51) [r2]15
(51) [r2]11 @41 q7a v255 < e8^12 > c+24 r12 v150 c+24 v255 e8^4 c-8 < e12 a24
a8^12 > d24 r12 v150 d24 v255 d+8$DD$00$0Ae^4 @31 $ED$05$EE c4^1 < q7f
[r2]5 @35 v70 $E8$50$FF g4 a4
@44(55)  y11  (61) q7f a8 q1f a12 q7f a12. q2f e24 q7f a8 q1f a12 q7f e24 a8 e12 q2f e24 q7f a8 (64) (65)
@31 $ED$0F$EE > c8 @44
q1f < a12 q7f a12. q2f e24 q7f a8 q1f a12 q7f e24 a8 e12 q2f e24 q7f a8 <
[a8 q1f a12 q7f a12. q2f e24 q7f a8 q1f a12 q7f e24 a8 e12 q2f e24 q7f a8]
$FA$02$07 *
$FA$02$0C e8 q1f e12 q7f e12. q2f c-24 q7f f+8 q1f f+12 q7f c+24 f+8 c+12 q2f c+24 q7f f+8
* >
$FA$02$0C c+8 q1f c+12 q7f c+12. q2f < a24 q7f a8 q1f a12 q7f e24 a8 e12 q2f e24 q7f a8
b8 q1f b12 q7f b12. q2f g24 q7f > d8 q1f d12 q7f < a24 > d8 < a12 q2f a24 q7f > d8
* 
@31 y10 $ED$0F$EE c1 [r2]7