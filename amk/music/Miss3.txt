#amk 2

#option TempoImmunity
#option NoLoop

#SPC
{
        #title "Nanakorobiyaoki"
	#author "Kipernal"
	#comment "Asian Death theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	"CStrings.brr"
}

#instruments
{
	"CStrings.brr" $B5 $A9 $00 $03 $C4	;@30
}

w200t54

#0 q7F @14 v180 y6 o3 $ED$5F$B0 $EB$12$36$01
c+8>c+16<b16>c+8<c+8b8b8<b8>g+8
a2
/r1

#1 q7F @14 v180 y6 o3 $ED$5F$B0 $EB$12$36$01
r8g+16f+16g+8r8f+8f+8r8d+8
e2
/r1

#2 q7F @30 v120 y10 o3
c+2<b2
a4 $E8$50$00 ^4
/r1

#3 q7F @30 v120 y10 o2
f+2f+2
e4 $E8$50$00 ^4
/r1

#4 q7F @13 v150 y10 o6 $ED$78$E3 p18,20,58
c+4 e8d8 $DD$01$18c+ <b8 b8 $DD$08$08>c+ <b8e8
a4 $E8$50$00 ^4
/r1

#5 q7F v255 y12,0,1 o6
@26c8 v190 @3$ED$7F$FF [[q06e16q0Ae16q0Fe8]]2 q06e32q08e32q0Ae32q0Ce32 q0Fe8 q0Be8
q0Fe2
/r1

#6 q7F @13 v80 y5,1,1 o6 $ED$78$E3 p18,20,58
r8. c+4 e8d8 $DD$01$18c+ <b8 b8 $DD$08$08>c+ <b8e8
a4 $E8$50$00 ^4
/r1

#7 q7F @13 v100 y10 o4 $ED$74$E0
c+1^4 $E8$50$00 ^4
/r1