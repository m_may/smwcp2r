;Formerly known as "Melancholic Forest Air". Revamped by Pink Gold Peach.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;A Summer Night's Melancholic Forest Air by Lunar Wurmple                     ;
;SEMI-FINISHED                                                                ;
;N-SPC Patch Needed: Yes, HuFlungDu's                                         ;
;Sample Bank Included: No                                                     ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#SPC
{
	#title "Massive Layoffs"
	#author "Lunar Wurmple"
	#comment "Factory Ghost House theme"
	#game "SMW Central Production 2"
        #length "3:00"
}

#samples
{
	#default
	"aa/DKC2/dkq-17_13.brr"                  
	"aa/DKC2/dkq-17_12.brr"                 
	"aa/DKC2/dkq-17_11.brr"                  
	"aa/DKC2/dkq-17_10.brr"                 
	"aa/DKC2/dkq-17_2f.brr"                 
	"aa/DKC2/dkq-17_2e.brr"                
	"aa/DKC2/dkq-17_2d.brr"
        "sawtooth.brr"
"metal clang.brr"
"metallic.brr"
	"OH75.brr" 
}
#instruments
{
@13 $C4 $EC $7F $03 $00 ;@30
"sawtooth.brr" $FE $68 $B8 $04 $00

	"aa/DKC2/dkq-17_13.brr"                  $FF $F5 $00 $05 $00 ; @45
	"aa/DKC2/dkq-17_12.brr"                  $FF $F5 $00 $05 $00 ; @44
	"aa/DKC2/dkq-17_11.brr"                  $FF $F5 $00 $05 $00 ; @43
	"aa/DKC2/dkq-17_10.brr"                  $FF $F5 $00 $05 $00 ; @42
	"aa/DKC2/dkq-17_2f.brr"                  $FF $F5 $00 $05 $00 ; @41
	"aa/DKC2/dkq-17_2e.brr"                  $FF $F5 $00 $05 $00 ; @40
	"aa/DKC2/dkq-17_2d.brr"                  $FF $F5 $00 $05 $00 ; @39

"metal clang.brr" $df $F0 $00 $03 $60
@1 $C7 $EC $7F $03 $00 ;@40
"metallic.brr" $ff $f8 $00 $06 $00
"OH75.brr" $9F $99 $00 $0a $86 
@0 $CA $EC $7F $06 $00 ;@40
}
$EF $FF $30 $D0
$F1 $04 $32 $01

#0 w160 t40	q7f v255 y10


o2
(2)[@38d4a8g16a8.@37g8f8g8
@36d4a8g16a8.@35g8f8g8
@34d4a8g16>c8.<a+8a8a+8
@33d4g8f16g8.d8@32c8<a8>
@32d4a8g16a8.g8@33f8g8
d4@34a8g16a8.g8f8@35g8
d4a8g16>c8.<a+8a8@36a+8
d4g8f16g8.@37d8c8<a8>]
/(2)3l16
@38dd@37dd@36ddd@35ddd@34ddd@33ddd
@32ccc@33ccc@34ccc@35ccc@36cc@37cc
<
@38a+a+@37a+a+@36a+a+a+@35a+a+a+@34a+a+a+@33a+a+a+
@32aaa@33aaa@34aaa@35aaa@36aa@37aa

>
(2);4+2

#1 v210	@31 $DE$00$04$30	q7f  y12
o2
d1
e1
f1
c1
d1
e1
f1
c1>
/[d1
c1<
a+1
a1>]10;4+2


#2	

o2y10

@39 
v170
c1^1
y4g1
>y7v255c2v150c2
v170y10
<c1^1
y4g1v255
>y7v255c2v150c4

@40 $DE$0C$16$60 v235 y8

d16e16f16>e16<
/[a2d2
g2c2
f2<a+2>
e2c2]2
[a2d2
g2c2
f2<a+2>
e2c2]8;4+2

#3 @39	
o2
r1
r1
r2y16v170c2
>y13r4v185c2v120c4
y6v90c1
r1
<r2y16v170e2.>y13v185c2v120c4
/[o2v170y5c1
@39v170y15c2y10v200@41$ED$5f$F2c2
@39y5c1
y15c4v225>y12c4<y10v200@41$ED$5f$F2c4>@39v160y8c4]4
@6 $DE$18$0D$30	q7f v245 y11
o4a4.f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>$EC$00$06$03c8$EC$00$00$00<{a4g4f4}
g8>$EC$00$06$03c4$EC$00$00$00<g8>$EC$00$06$03c8.$EC$00$00$00<a8.>c8
d4.<f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>$EC$00$06$03c8$EC$00$00$00<{a4g4f4}
g8>$EC$00$06$03c4$EC$00$00$00<g8>$EC$00$06$03c8.$EC$00$00$00<a8.>c8
d16f16d16c16g16d16c16<a16
g16a16g16f16g16a16d16c16
g16d16c16<a16>a16f16d16f16
g16a16d16f16g16a16d16g16
a16>c16d16c16d16f16g16f16
d16<a+16>g16a16e16c16g16a16
d16e16d16c16d32c32<a32>c32<a32g32a32g32
d32a32>c32f32c32<a32g32f32d32c32f32d32c32<a32g32f32>
d4.f16g16{a4g4f4}
g4.e16f16{g4f4e4}
a4.>$EC$00$06$03c8$EC$00$00$00<{a4g4f4}
g8>$EC$00$06$03c4$EC$00$00$00<g8>$EC$00$06$03c8.$EC$00$00$00<a8.>c8
d1$DD$00$ff$B1^1^1^1

#4	@30	q7f v240 y11

$DE$18$12$60
o4
[a1
g1
f1
e1]2
/o4
[a1
g1
f1
e1]10;4+2    

#5	@30	q7f v200

o4 $DE$28$0D$80
v200y10
[f1
e1
d1
c1]2/
@30v200v200
o4$DE$28$0D$80
[f1
e1
d1
c1]2
$DE$18$16$70
<@43v225
f4.g16e16{f4e4d4}
e4.f16e16{g4f4e4}
a+4.a16a+16{>d4c4<a+4}
a4.e16g16{a4g4e4}
f2>{d4e4f4}
e2{>c4<a+4e4}
f4.d16<a+16>{g4f4d4}
e4.e8{a4g4e4}
o3
$ED $7F $E1
@0v255y10
[f1
e1
d1
c1]6;4+2

#6	@30	q7f v200 y9
$DE$30$0B$60
o4
[d1
c1<
a+1
a1>]2

/o4[d1
c1<
a+1
a1>]10;4+2            

#amk=1

#7 


[r2]14
r2. v245y6@41c8v130c8
/(1)[y10v245
o2@39$ed$7f$f9e8o4@21e8y12v180@42c8v110y8c8y10v245@29c8@21e8y6@41c8y10@21e8]15
y10v245
o2@39$ed$7f$f9e8o4@21e8y12v180@42c8v110y8c8y10v245@29c8@21e8@29b16@29b16@21f16@21f16

(1)24
