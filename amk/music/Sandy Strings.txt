#SPC
{
	#title "Sandy Strings"
	#author "Bloop"
	#comment "Desert Ghost House theme"
	#game "SMWCP2"
}

#samples
{
	#default
	#AMM
	"CChoir.brr"
}

#instruments
{
"CChoir.brr"	$98$C6$00$05$A0		;@30
@23		$FA$F0$00$05$00		;@31
@29		$FF$F0$00$04$90		;@32
}

#amk 2

$EF$1F$30$30
$F1$04$20$01

w220 t46

#0 @5 q7F v140 y13 o4
(1)[g4d+8c8g8f16d+16f4
g+4d+8c8g+8g16f16g4
g4d+8c8g8f16d+16f4
c16d+16g+16 q7B c16d+16g+16 q77 c16d+16 q7F
c16f16g+16 q7B c16f16g+16 q77 c16f16 q7F]2
q77g+1/
(1)3
(2)[q7Fg4d+8c8g8f16d+16f4
g+4d+8c8g+8g16f16g4
g4d+8c8g8f16d+16f4
c16d+16g+16 q7B c16d+16g+16 q77 c16d+16 q7F
d16f16a+16 q7B d16f16a+16 q77 d16f16 q7F
[[d+16g16>c16< q7B d+16g16>c16< q77 d+16g16 q7F]]2 ]
(1)3
(2)
[g+4f8c8g+8g16f16g+4
g4d+8c8g8f16d+16f4
g4d8<b8>g8f16d+16f4
[[c16d+16g16 q7B c16d+16g16 q77 c16d+16 q7F]]2 ]2
(1)2

#1 @1 $ED$79$E4 q7F v255 y10,1,1 o2 p12,50
[c1<g+1>c1<g+2]o1f2>
*
o1f1.>/
[[ (11)[c1<g+1>c1<g+2f2>]3
c1<g+1>c1<g+2a+2>c1]]2
[[f1c1<g1>c1]]2
(11)2

#2
(21)[@13 $ED$74$E9 q7F v160 y5,1,1 o4 p12,50 g1g+1g1g+2]f2
*f1./
[[g1g+1g1g+2f2]]3
g1g+1g1g+2a+2>c1< @2 o5 v80 $DF
[g8c8d+8c8g8f16d+16f8c8
g+8c8d+8c8g+8g16f16g8c8
g8c8d+8c8g8f16d+16f8c8
c16d+16g+16 q7B c16d+16g+16 q77 c16d+16 q7F
c16f16g+16 q7B c16f16g+16 q77 c16f16 q7F]2 @30 q7F v110 $DF y12,1,1 o4
[g1g+1g1g+2]f2
*a+2>c1<
[f1d+1g1g1]2
(21)f2(21)f2

#3
(31)[@13 $ED$74$E9 q7F v160 y5,1,1 o4 p12,50 d+1d+1d+1d+2]c2
*c1./
[d+1d+1d+1d+2c2]3
d+1d+1d+1d+2f2g1
^1^1^1^1
(40)[r2]8 @30 q7F v110 $DF y12,1,1 o5
c1c1c1c2<g+2>
c1c1c1c2d2<g1
[g+1g1b1>c1<]2
(31)c2(31)c2

#4 v180 y10 o3
(40)16
[[@31q7F a16 q78 a16a16a16]]3 $F4$03@32q7Fg16g16g16g16$F4$03/
(41)[ [[q7Fv255@12o1d8 v180o3@31q78a16a16 q7Fa8 q78a8 $F4$03@32q7Fg8$F4$03 v180@31q78a16a16 q7Fa8 q78a8]]3
[[q7Fv255@12o1d8 v180o3@31q78a16a16q7Fa8 $F4$03@32q7Fg16g16$F4$03]]2 ]4
(42)[q7Fv255@12o1d8 v180o3@31q78a16a16 $F4$03@32q7Fg8$F4$03 v180@31q78a16a16]2
(41)4(42)2
(41)2
(40)16

#5
(40)18/@30 q7F v120 y12,1,1 o4 $DF
(51)[c2d2d+1c2d2d+2f2]2 y9 @6 v140 $ED$79$E7$F4$03 p40,16,50
[c4.<a+8>c8<a+8>d16 $DD$06$01c ^16<a+8>]
d+4.d16c16d2
c4.<a+8>d16 $DD$06$01c ^16d+8g8d+8
g+2f2
*
d+4d+8d16c16d4.c16<a+16>
c4.<a+8>d16 $DD$06$01c ^16d+8g8d+8
g+4.c16d+16f8f8d8<a+8>c1 @30 q7F v120 y12,1,1 o4 $DF
(51)2 @6 v140 $ED$79$E7 p40,16,50 o5
d16 $DD$06$01c ^16d8d+2f8g8
g+4.g16f16 g+16 $DD$06$01g ^16f8d+8d8
c8d8d+2f8g8
g+8g8f8d+8f8d+8d8d+8
c16d16d+16d16d+16d16d+16f16g16f16d+16f16d+16d16d+16f16g16g+16a+16g+16g16f16g16f16d+16f16d+16d16d+16d16c16<a+16>
c8d16d+16f8d+16d16d+8f16g16g+8g16f16
d+8c16d16d+16c16d16d+16f8d16d+16f16d16d+16f16
g2.g8g8
g+4.d+16f16g8g+8a+8g+8
g4d+8f8g8g+8g8f8
g4d8d+8f8d+8d8d+8c2.d16d+16f16g16
g+4.g+16a+16>c8<a+8g+8d+16f16g8g+8g8f8g4.d+16f16
g8f8d+8f8d+8d8d+8<a+8>c1
^1^1^1^1 $F4$03
(40)8

#7
(40)18/@30 q7F v120 y12,1,1 o4
(40)16
c1^1
(40)14 @12 o4 v170 $F4$03
[r8c8f8c4c16c16f8c8]25 $F4$03
(40)16

#6 @8 q7F $ED$7B$E6 v205 y10 o3 p12,50
(40)18/
(71)[ [[c8<d+8g8d+8>]]2 <
[[g+8c8d+8c8]]2 >
[[c8<d+8g8d+8>]]2 <
g+8c8d+8c8g+8c8f8c8 >]3
(72)[ [[c8<d+8g8d+8>]]2 <
[[g+8c8d+8c8]]2 >
[[c8<d+8g8d+8>]]2 <
g+8c8d+8c8 a+8d8f8d8 [[>c8<d+8g8d+8]]2]
(71)3(72)>
[ [[c8<f8g+8f8>]]2
[[c8<d+8g8d+8>]]2 <
[[b8d8g8d8]]2 >
[[c8<d+8g8d+8>]]2]2
(40)16