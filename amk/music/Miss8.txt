#amk 2

#option TempoImmunity
#option NoLoop

#SPC
{
        #title "No O.S.H.A. Compilance"
	#author "Kipernal"
	#comment "Factory Death theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	"CChoir.brr"
}

#instruments
{
	"CChoir.brr"	$98$C6$00$05$A0		;@30
}

w200t57

#0 q7F @29 v230 q7F y10 o3
a16a16
@21>e8 e16e16 @29<a8 @21>e16e16 @29<a16a16 @21>e8 @29<a8 @21>e8
@29a1
/r1

#1 q7F @30 v150 q7F y12,1,1 o4
r8
e2d+2
c+2 $E8$A0$00 ^2
/r1

#2 q7F @30 v150 q7F y12,1,1 o4
r8
c+2<b2
a2 $E8$A0$00 ^2
/r1

#3 q7F @30 v150 q7F y12,1,1 o3
r8
g+2f+2
e2 $E8$A0$00 ^2
/r1

#4 q7F @14 v230 q7F y10 o2
r8
c+8>c+8<c+16>c+8<c+16b8<b16>b8<b16>g+8
a2 $E8$A0$00 ^2
/r1

#5 q7F @1 v190 $ED$7F$EF q7F y10 o4 p30,12,32
r8
b4 $DD$01$0F>c+ e8d16c+16<b16a16b8>e8<g+8
a1
/r1

#6 q7F @1 v190 $ED$7F$EF q7F y10 o5 p30,12,32
r8
b4 $DD$01$0F>c+ e8d16c+16<b16a16b8>e8<g+8
a1
/r1

#7 q7F @0 v160 $ED$7F$F0 q2F y10 o5
r8
e16c+16<g+16e16g+16>c+16e16c+16
d+16<b16f+16d+16>c+16<b16f+16d+16>
c+16<a16e16c+16> q2A c+16<a16e16c+16> q25 c+16<a16e16c+16> q22 c+16<a16e16c+16>
/r1