#amk 2

#pad $01E5

#option TempoImmunity
#option NoLoop

#SPC
{
        #title "Phantasmagorical Flub"
	#author "Kipernal"
	#comment "Abstract Death theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#amk 2
"bells.brr"
}
#instruments
{
@13  $BE $6D $00 $03 $00   ;@30
@8   $BF $F3 $B8 $1E $00   ;@31
@2   $8E $F0 $CF $01 $7f   ;@32
@1   $87 $AF $B8 $03 $00   ;@33
@6   $85 $EF $CF $03 $00   ;@34
"bells.brr" $FF $ED $B8 $04 $4E      ;@35
}

#0 $F4 $02 t23
@30 y9
o4v235 r16c+32g+32>c+16<b8b16 a8 e32f+32 a8r1


#1
@30 y11
o4v235 r8g+16f+8f+16e8.e8r1


#2 @31 l32 v200
o3c+16g+rg+r<b16>f+rf+rerer<ar>e1

#3 @32 p16,72
o4v150 y8c+8>>y12e16<b8b16 a16$DD$0C$18$C0^8>e1

#4 @33 v170 p12,32 y12
o4c+8.<b8.a8.p12,82a1

#5 @33 v170 p12,32 y8
o3g+8.f+8.e8.p12,82e1

#6 @34 v235 y13 p16,60
o2g+8.y7f+8.y10e1

#7 @35 v255 y10
o3c+4.<a1