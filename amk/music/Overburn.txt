;An SMW Central Production - "Fire Level" - SNN            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#SPC
{
	#title "Overburn"
	#author "S.N.N."
	#comment "Fire Level 1"
	#game "SMW Central Production 2"
}
#samples
{
#optimized
"CCymbal.brr"
"CChoir.brr"
"Electric Bass.brr"
"bth/r8_kick_2.brr"        
"bth/yasui_snare.brr"
"bth/r8_hi-hat.brr"
"Sitar2.brr"
"shreddage_muted_guitar.brr"
"bth/jv1080_impact_hit_chd.brr"
}
#instruments
{
"Electric Bass.brr"	       $FF $ED $B8 $03 $16  ;@30	
"bth/r8_kick_2.brr"            $CF $20 $00 $10 $00  ;@31
"bth/yasui_snare.brr"          $9F $00 $00 $09 $70  ;@32
"bth/r8_hi-hat.brr" 	       $FF $FE $B8 $10 $00  ;@33 closed
"bth/r8_hi-hat.brr" 	       $FF $F4 $B8 $10 $00  ;@34 open
"Sitar2.brr"                   $FD $EE $B8 $09 $04  ;@35
"shreddage_muted_guitar.brr"   $FF $F2 $B8 $05 $40  ;@36
@6                             $FF $F5 $B8 $03 $00  ;@37 synth
"CChoir.brr"                   $FC $E8 $B8 $05 $A3  ;@38
"CCymbal.brr"	               $FF $F2 $00 $03 $C0  ;@39
@29                            $FF $E0 $00 $01 $7F  ;@40
"bth/jv1080_impact_hit_chd.brr"    $FF $F0 $00 $05 $00  ;@41
n13 $82 $EF $00 $05 $00                             ;@42 fire
@4                             $FE $E1 $B8 $03 $00  ;@43
}
$EF $FE $20 $20 
$F1 $04 $40 $01

#0 $DF

w170 t70


[o4 y10
@30 v220 c8  c16c16c8r4c8d+8<a+8]3
> c2^8c8$EB$0C$18$F4f4$EB$00$00$00

(1)[
c8c16c16g8g8c8c16c16g8g8
<a+8a+16a+16>f8f8<g8g16g16>d+8d+8
c8c16c16c8d+8d+8d+8f8f8
g8g16g16g+8f8f8f8g+8g+8
g8<g8>g8<g4>f=72
c8c16c16f8f16f16a+8a+8f8f8
g8<g8>g8<g4>f4^8
c8c16c16f8f16f16a+8a+8]1>$EB$0C$18$F4c+4$EB$00$00$00
<(1)f8f8

c4c8c8c2c1
d+4d+8d+8d+2d1
c4c8c8c2c1
d+4d+8d+8d+2f4.a+4.>$EB$0C$18$F4c4$EB$00$00$00
<
[c8c16c16>c8<c4<f8a+8>c8
d+8d+16d+16>d+8<d+4c8d+8f8
c8c16c16>c8<c8c16c16>d+8<d+8f8
c8c16c16c8d+4.f4]2
[c8c16c16c8r4c8d+8f8r8f8c8r8d+8<a+8r4]2



#1 ;drums
o3 
y10 v225
r1r1
@31e2e2e2e8@32e8e8e8

(5)[@31e4@32e4@40e8@31e8@32e4]7
@31e4@32e4@40e8@31e8@32e8e16e16
(5)7
@31e4@32e4@40e8@31e8@32e8e16e16
[@31e4@32e4@31e2@31e2.e4]3
@31e4@32e4@31e2@31e8@32v80$E8$90$E1e8e8e8e8e8e8e8
(5)8
@31e2e2e2e2
r1r1;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#2 ;hats

@33o3 v190 y11
e2e2e2e2
e4e4e4e4e4e4e4@34e4
[@33e8e16e16e8e8@34e8@33e8@34e8@33e8e8e16e16e8e8@34e8@33e8e8e8]8
@33[e4]15@34e4
@33[e4]13e8e8e8e8@34e8@33e8
[@33e8e16e16e8e8e8e8e8@34e8
@33e8e16e16e8e8e8e8e8e16e16]4
$fa$03$48@39y10o3v225a2$fa$03$00@33v190e2e2e2e2e2e2e2;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#3 $DF

[r2]8

@35 v215 o3 y9
(30)[c1<a+4^8g4^8g+4 c4^8d+4^8 f8^16^32 f32g4g+8 f8^16^32 c32f4g+8
g1>c4f4<a+4>f4<g1c4f4<a+4>]1   f8>c+8
(30)   f4


(31)[
v195 c8c8   v170 c4  v140 c8c8   v100 c4
v195 c8c8   v170 c4  v140 c8c8   v100 c4
v195 d+8d+8 v170 d+4 v140 d+8d+8 v100 d+4]1
v195 d8d8   v170 d4  v140 d8d8   v100 d4
(31)
v195 f8f8   v170 f4  v140 f8f8   v100 f4
@36v235y12
[c8c8c8c4<f8a+8>c8
d+8d+8d+8d+4c8d+8f8
c8c8c8c8c16c16d+8d+8f8
c8c8c8d+4.f4]2
@37 v202v180 y8 o4
>c16<a+16>c16<g16>c16<a+16g16a+16>c16<a+16>c16<g16>c16<a+16g16f16
>c16<a+16>c16<g16>c16<a+16g16f16c16f16g16f16a+16g16>c16<a+16
  
[@0$ED $7F $F3 y12v200 c16<a+16> v180 c16<g16>]4
v190 c16<a+16> v170 c16<g16>
v140 c16<a+16> v120 c16<g16>
v110 c16<a+16> v90 c16<g16>
v70 c16<a+16> v50 c16<g16>;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#4 $DF

r1r1r1.r8$fa$03$00

@35 v215 y11
o4<a+8>c8c+8
(40)[c1<a+4^8g4^8g+4c4^8d+4^8f8^16^32f32g4g+8f8^16^32c32f4g+8g1>c4f4<a+4>f4<g1c4f4<a+4>]1 f8  >c+8
(40) f4

(41)[
v195 c4  v170 c4  v140 c4  v100 c4]2
(42)[
v195 d+4 v170 d+4 v140 d+4 v100 d+4]1
v195 d4  v170 d4  v140 d4  v100 d4
(41)2
(42)
v195 f4  v170 f4  v140 f4  v100 f4
v180[c4^8c2^8d+4^8d+2^4^8c4d+8c4^8f4^8g2^8]2
@42v110y11c1^1y9c1^1;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#7 $DF

[r2]8

(52)[v255$fa$03$00@41y10o4c1^1o2@15y5c1^1]

(51)[$fa$03$30v255 @37o5
y13g16d16d16d16g16d16d16g8^16d8<a+8>c8<g4>c4<a+4>c4<
y7@37g16d16d16d16g16d16d16g8^16d8<a+8>c8<g4>c4<a+4>c8c+8]1


(52)
(51)


[o4y10v255$fa$03$48y10@39o3a4.y13$fa$03$30@40c8c2c4c1y7c8c8c2c4c2.]2


[$fa$03$48y10@39o3a4.@41o4$fa$03$30< c=120 c1^4> d+4. d+4. c4.c2r8]2
c1^1^1^1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#6 $DF

r1r1r1r1
v235 y12 @36 
[o3c8c8g8g8c8c8g8g8
<a+8a+8>f8f8<g8g8>d+8d+8
c8c8c8d+8d+8d+8f8f8
g8g8g+8f8f8f8g+8g+8
g8<g8>g8<g4>f=72
c8c8f8f8a+8a+8f8f8
g8<g8>g8<g4>f4^8
c8c8f8f8a+8a+8f8f8]2
[@42v100y12c1^1y8c1^1n11]2
@43 v210 $DE$12$0C$27
o5 y11
(32)[c4$F4$01c8<f8f32&g16.f4^8d+8d+32&f16.d+8$F4$01c4^8<a+2]1
>c8d+8f8d+16f16c4
<a+8g8a+8>c2^8
(32)
g8f8g4d+4
a+4^8g4^8f4
c8c8c8f1^8
d+2
$E8$C0$7Fc1
$E8$C0$00^1
y10


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#5 $DF

;;;;;;;
;CHOIR;
;;;;;;;
[r2]14 r2^4 v230 y8

@38 p10,66 h1
(70)[a8g8f+1d2e2f+1^1]1
r1r1r1..
(70)
v255@41p0,0h0y10o4c4
@38p10,66h1
(71)[ y10 <b4> y9 f+4 y8 e4 y9 c+4 y10 d4< y11 b4 y12 a4> y11 d4 y10 c+4< y9 a4 y8 b4> y9 d4]1 y10 c+4< y11 b4 y12 a4>r4
(71)   y10 e4 y11 d4 y12f+4
[r2]11y8
f+2
a1
f+2a4b8a8
b1^1
[r2]20                

#amk=1
