#amk 1

#SPC
{
	#title "Switch Scramble"
	#game "SMW Central Production 2"
	#author "Todd/Torchkas"
	#comment "P-Switch theme"
}

#tempoimmunity

#instruments {
	@2 $9F $6C $00 $03 $00 ;@30
	@5 $9F $6C $00 $0E $00 ;@30
}

#0 w220 t45
$EF $1D $12 $12
$F4 $02
v190@12
[q7fo6c8q5co3f8q7f>a+8q5c<a+16q6bg16q7f>a+16q4d<<a+8q6ca+16q7f>>a+8q5c<d+8]2
$E3 $C0 $34*
$E3 $C0 $3B*
$E3 $FF $45*
*3
/
*4

#1 h-1
y2v215@3
[$DC $84 $12o2d8a8d8>d16<d16a16d8
$DC $84 $02 >d8<d16a16>d16<d+8a+8d+8
$DC $78 $12 >d+16<d+16a+16d+8>d+8<d+16a+16>d+16
$DC $84 $02 o2d8a8d8>d16<d16a16d8
$DC $84 $12 >d8<d16a16>d16<d+8a+8d+8
$DC $78 $02 >d+16<d+16a+16d+8>d+8<d+16a+16>d+16]2
/
*

#2 h-1
y17v200@3
[[$FA $02 $00
$DC $C0 $0A[o3a8d16>d16<a8f+16d8a16f+16>d16<a8f+8]
$FA $02 $01
$DC $C0 $03*
$FA $02 $00
$DC $C0 $0A*
$FA $02 $01
$DC $C0 $11* ]]2
/
$FA $02 $00
$DC $C0 $0A[o3a8d16>d16<a8f+16d8a16f+16>d16<a8f+8]
$FA $02 $01
$DC $C0 $03*
$FA $02 $00
$DC $C0 $0A*
$FA $02 $01
$DC $C0 $11*

#3 h-1 $DE $12 $07 $30
y11v100@31
(1)[o4f+4r8f+16g16a4d4
a+4r8>c16<a+16a4g4
f+16g16a4.d4r16>d16$F4 $01c+16d32$F4 $01^32
c8<a+8a8g8f+8^32r16.e4]2
/
(1)

#4 h-1 $DE $12 $07 $20
v135@30(1)2
/
(1)

#5 h-1
r8y8v62@31(1)
f+4r8f+16g16a4d4
a+4r8>c16<a+16a4g4
f+16g16a4.d4r16>d16$F4 $01c+16d32$F4 $01^32
c8<a+8a8g8f+8^32r16.e8
/
r8
(1)