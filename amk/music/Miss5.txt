#amk 2

#option TempoImmunity
#option NoLoop

#SPC
{
        #title "Show's Cancelled"
	#author "Kipernal"
	#comment "Carnival Death theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
}

w200t58

#0 q7F y10 v200 o4
[{@21c4@10c8@21c8@10c4}]2
{@21c4<c4>@10c4}
@21c1
/r1

#1 q7F y16 v130 @3 o4 $ED$7F$F8 $EE$50
{g+4>c+8<g+8>c+4<a4>d8<a8>d4<
g+4>c+8<a8b8e8}
a1
/r1

#2 q7F y10 v120 @4 o2 $ED$7D$E0 $EE$50
{c+4g+4g+4d4a4a4c+4g+4<e4}
a4 $E8$50$00 ^2.
/r1

#3 q7F y8 v210 @9 o3 $EE$20
{c+4>c+8d8e4<c+4>d8c+8<b4c+4e8a8b8e8}
a4 $E8$50$00 ^2.
/r1

#4 q7F y8 v210 @9 o3 $EE$20
{r4g+8a8b2a8g+8f+2<b8>e8f+8<b8>}
e4 $E8$50$00 ^2.
/r1

#5 q7F y11,1,1 v140 @13 o4 $ED$65$E8 p13,58 $EE$50
g+2a2g+2
e4 $E8$58$00 ^2.
/r1

#6 q7F y11,1,1 v140 @13 o4 $ED$65$E8 p13,58 $EE$50
e2e2e2
c+4 $E8$58$00 ^2.
/r1