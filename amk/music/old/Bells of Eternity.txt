#SPC
{
	#author "Moose"
	#comment "Abstract/Dream Level 5"
	#game "SMW Central Production 2"
}

#samples
{
	#optimized
	#AMM
	#sky_and_dream
	"CStrings.brr"
	"CCymbal.brr"
}


#0 
$EF $D1 $00 $00
$F1 $06 $64 $01
$F2 $DC $14 $14
#0 v200 @2 l8 q7f t50 w160
o4
$ED $7E $F4
(1)[>d<a>c<gafgd]4
(2)[afgdfcd<a>]4
(1)4 >d1
/
(1)4
(2)3
<afgdfcd16f16>f16e16
(1)4
(2)3
<afgdfcd16f16>f16e16
@6 l16 y10
o5r4^8def8ga4^ragfgfegfedfedc<a>cd<a2^4^8r1^4^8>def8ga4^ragfgfegfedfedcd>cd<a2^4^8r1 @0 $ED $6C $C5
< r4^8def8ga4^ragfgfegfedfedc<a>cd<a2^4^8r1^4^8>def8ga4^ragfgfegfedfedcd>cd<a2^4^8
[r1]5 @6
>ragfgfegfedfedcd>cd<a2^4^8 @2

#1 v215 @15 ("CStrings.brr", $03) q7f
o2
$ED $29 $60 y5
d1^1^1^1
f1^1^1^1
d1^1^1^1^1
/
@6 $EE $00 v255 l16
o4 (3)[y15defga8f8y10b-agb-a8f8y5defga8f8y10efgb-a8^r]2
(5)[y15<b->cdef8d8y10gfegf8d8y5<b->cdef8d8y10cdegf8^ry15<b->cdef8d8y10gfegf8d8y5<b->cdef8d8y10cdega8^r]
(3)2 (5)
 @15 ("CStrings.brr", $03) $ED $29 $60 v215
o2 y5 [f1g1a1^1]3
f1g1 [r1]2
<a1>d1g1>c1
[r1]2

#2 v180 @15 ("CStrings.brr", $03) q7f
o2
$ED $29 $60 y15
a1^1^1^1
<a+1^1^1^1
>a1^1^1^1^1
/
@6 $EE $00 v255 l16
o3 (4)[y5a4>dedey10f4edcdy15agdedefgy10gfede8^r]2
(6)[y5<f4b->c<b->cy10d4c<b-gb->y15fd<b->c<b->cdey10edc<b->c8^ry5<f4b->c<b->cy10d4c<b-gb->y15fd<b->c<b->cdey10edc<b->c+8^r]
(4)2 (6)
 @15 ("CStrings.brr", $03) v180
o1 y15 [b-1>c1d1^1<]3
b-1>c1 [r1]2
d1a1>d1g1
[r1]2

#3 v255 @14 q7f l8
o2 $ED $7F $EF
[r1]8 y10
ddrd16r^16<ff16g16a16>c16
ddrd16r^16<d4^
>ddrd16r^16<ff16g16a16>c16
ddr<dr16a16>c16d16f16e16d16c16
d1
/
(7)[ddrd16r^16<ff16g16a16>c16
ddrd16r^16<d4^>]2
(8)[<a+8a+8r8a+16r8^16d8d+16e16f16g16
a+8a+8r8a+16r8^16a+4^8a+8a+8r8a+16r8^16d8d+16e16f16g16
a+8a+8r8a+8r16a+16c16c+16d16f16g16a16>]
(7)2 (8)
[r1]14
[ddrd16r^16<ff16g16a16>c16]6 l16
ragfgfegfedfedcd>cd<a2^4^8

#5 v255 q7f l16
o4
[@21c4^8@21c8@29g@29g@21c4^8@21c4^8@21c2^8]6
@21c2{v250@29cv200@29cv150@29cv250@29cv200@29cv150@29c}v255@29c@29c@29c@29c
/
(9)[v255@21c8v200@22c@22cv255@29c8v200@22cv255@21cv200@22c@22cv255@21cv200@22cv255@29c8v200@22c@22c]16
[r1]8 v255
@21c1@21c4@21c4@21c4@21c4
(9)10
@21c1@21c4@21c4{v250@29cv200@29cv150@29cv250@29cv200@29cv150@29c}v255@29c@29c@29c@29c

#7 q7f o4 @0 ("CCymbal.brr", $04) $ED $6F $AF l1
[r]13
/
c1^1 [r1]14 c1^1 [r1]2 c1^1 [r1]2 c1^1 [r1]10 c1^1                

#amk=1
