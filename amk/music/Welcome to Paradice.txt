;Revamped by Pink Gold Peach.

#SPC
{
	#title "Welcome to Paradice"
	#author "Jimmy"
	#comment "Ice Level 1"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	"CStrings.brr"
	"CChoir.brr"
	"Jingle Bell.brr"
}

#instruments
{
"Jingle Bell.brr" $FF $F6 $00 $05 $40           ;@30
@2 $DB $71 $00 $03 $00                          ;@31
"CStrings.brr" $95 $A1 $00 $01 $e2              ;@32
"CStrings.brr" $9D $A0 $00 $01 $e2              ;@33
@0 $FF $FB $00 $06 $00                          ;@34
@8 $FF $F0 $00 $1E $00                          ;@35
@0 $EF $8A $00 $06 $00                          ;@36
"CChoir.brr" $DB $A1 $00 $05 $A0                ;@37
@6 $FC $E1 $00 $03 $00                          ;@38
}

$EF $BF $25 $25
$F1 $03 $55 $01



;;;;;;;;;;; INSTRUMENTS ;;;;;;;;;;;


(108)[$ED $5F $C6
      
      v200]1		; Piano (low)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#0 w170 t64

@31
y12

o4
f1
f1
e1
e1
d1
f1
a1
g1
o2
@32v155
y10
a1^1
g1^1
o4
@34v175
y11
(10)[d16r8^16d16r4^8^16c16r16d16r8^16
     c16r16d16r8d16f16r16e16r16d16r16c16r16
     c16r8^16c16r4^8^16<a+16r16>c16r8^16
     <a+16r16>c16r16d16r16f16r16e16r16d16r16e16r16]1
r1^1
@35 $F4$03
o2 y10
[v240 d8 v120 d8d8d8 v240 d8d8c8d8
 v120 d8 v240 c8d8 v120 d8 v240 d4 $DD $00 $18 $95 v120 a4
 v240 c8 v120 c8c8c8 v240 c8c8<b8>c8
 v120 c8 v240 <b8>c8 v120 c8 v240 c4 $DD $00 $18 $93 v120 g4]5
r1^1^1
@36v175$F4$03

o3
y15 $DC$A0$05 d32c32d32f32d32f32g32f32g32a32g32a32>c32<a32>c32d32c32d32f32d32f32g32f32g32a32g32a32>c32<a32>c32d32c32
@35v240$F4$03
y10
o2
[d8d8d4d8r8d8r8
 d8r8d8r8d8r8d8r8
 c8c8c4c8r8c8r8
 c8r8c8r8c8r8c8r8
 <a+8a+8a+4a+8r8a+8r8
 a+8r8a+8r8a+8r8a+8r8
 a8a8a4a8r8a8r8
 a8r8a8r8a8r8a8r8>]2

@9 v200 $F4$03
y12
o4
d16r16d16r16d16r16d16r16d4e4
f4r4f4e4
c16r16c16r16c16r16c16r16c4d4
c2r2
<a+16r16a+16r16a+16r16a+16r16a+4>c4
d4r4d4c4
<a16r16a16r16a16r16a16r16a4a+4
a2r2

@35$F4$03
y10
o2
[d8]16
[c8]16
<
(11)[a+8]16
(12)[a8]16
(11)16
(12)16
(11)16
(12)16
$E1 $C0 $7F
a1 $E1 $C0 $00 ^1$F4$03

#1

@31 v178
y8

o4
d1
d1
c1
c1
<a1
>d1
e1
e1

@32v155
y10
o3
d1^1
c1^2
c+2
d1^1
c1^2
c+2
@33v175
(20)[d16r16d16r16d8^16r16]4
@32v155
d1^1
c1^2
c+2
d1^1
c1^2
c+2

@31 v178
y9
o4
a8>d8<a8>d8<a8>d8<a8>d8
f8e8d8<a8a8>d8d8d8
<g4>c4c4c4
e4c4c+4c+4
@32v155
y10
o3
d1^1
c1^2
c+2
@34v175
(10)1
@33
(20)8
@32v155
o5
@38 v170
y10
$DE $2E $0C $38
$EA $35
d4 $DD $00 $2A $B5 ^2^8r8
f8d8<a8>f8r8a4r8
c4 $DD $00 $2A $B4 ^2^8r8
e8<g8>c8g8r8g8f8e8
d4 $DD $00 $2A $B5 ^2^8r8
<a+8>d8e8f8r8a+4^8
a1^2^4^8
r8
f4 $DD $00 $2A $B9 ^2^8r8
d8e8f8g8r8a4r8
e4 $DD $00 $2A $B7 ^2^8r8
g8c8g8a+4^8a8g8
f4 $DD $00 $2A $B9 ^2^8r8
a8f8a8>d4<a8>d8f8
c+4 $DD $00 $30 $C0 ^1^2^4

@9v200 y8
p0,0
o3
a16r16a16r16a16r16a16r16a4a4
>d4r4d4d4
<g16r16g16r16g16r16g16r16g4a4
g2r2
f16r16f16r16f16r16f16r16f4f4
a+4r4a+4a+4
e16r16e16r16e16r16e16r16e4e4
e2r2

 y11
@33
v185
o5
f16r16f16r16f16r16f16r16f4g4
a4r4a4g4
e16r16e16r16e16r16e16r16e4f4
e2r2
d16r16d16r16d16r16d16r16d4e4
f4r4f4e4
c+16r16c+16r16c+16r16c+16r16c+4d4
c+2r2<
(25)[a+16r16a+16r16a+16r16a+16r16a+4>c4
     d4]1 r4d4c4
<a16r16a16r16a16r16a16r16a4a+4
a2r2
(25)1 >f4a4a+4
a1^1^1^1

#2

@31 v178
y9

o5
(30)[a8 a16r8^16a16r8^16a1^4^8]1
g8g16r8^16g16r8^16g1^4^8
(30)1
>c8c16r8^16c16r8^16c1^4^8<
(31)[d8f8<a8>d8e8g8e8f8
     r8f8<a8>d8e8g8e8f8]3
(32)[c8e8<g8>c8e8g8d+8e8
     r8e8<g8>c8e8g8e8f8]1
(33)[a4r4a4r4
     a8a8r8a4r4^8]1
[r2]8
<a8>f8d8f8d8<a8f8a8
>a8g8f8g8e8f8d8<a8
>g8f8c8e8c8e8<g8>c8
g8f8c8e8c+8e8<g8>c+8
[d8f8]2 [d8e8]2
a8g8f8e8d8e8g8f8
c4e4f4e4
g4f4e4f4
(31)3
(32)1
(33)2
v150
(34)[a4r4a4a4
     a8a8r8a4r8a4]1
g4r4g4g4
g8g8r8g4r8g4
(34)1
e4r4e4e4
e8e8r8e4r8e4
@36v175 
o3 
[
y13$DC$60$07d16f16a16>d16<a16>d16<a16>d16
$DC$60$0Df16d16<a16>d16<a16f16a16f16]2
[
$DC$60$07c16e16g16e16g16>c16<g16>c16
$DC$60$0De16c16<g16>c16<g16e16g16e16]2
[
$DC$60$07<a+16>d16f16d16f16a+16f16a+16
$DC$60$0D>d16<a+16f16a+16f16d16f16d16]2
(35)[y13$DC$60$07<a16>c+16e16c+16e16a16e16a16>$DC$60$0Dc+16<a16]1 e16a16e16c+16e16c+16
(35)1 >c+16e16c+16e16a16>c+16
@9 v200 y11

o3
f16r16f16r16f16r16f16r16f4f4
a4r4a4a4
e16r16e16r16e16r16e16r16e4e4
e2r2
d16r16d16r16d16r16d16r16d4d4
f4r4f4f4
c+16r16c+16r16c+16r16c+16r16c+4c+4
c+2r2
 y9
@31 v178
v160
o5
[a4f4]3
a4a+4
[g4e4]3
a+4a4
[f4d4f4d4
 f4d4f4a+4
 a4e4a4e4
 a4e4a4e4]3
a1^1

#3

@32v155
y8

o4
d1^1
c1^2
<a+2
a1^1
g1^1
>
(41)[d1^1
     c1^2
     c+2]2
@33v175
(40)[d16r16d16r16d8^16r16]4
@32v155
(41)5
@33v175
(40)8

@37v165
y12
$DE $2E $0C $38
$EA $35
a1^1
g1^1
f1^1
e1^1
a1^1
g1^1
a1^1
>c+1^1
p0,0
[r2]14
<
@33v175
v200
y8
{c+8<a8>c+8e8c+8e8a8e8a8>c+8<a8>c+8}
v155
<a1^1
g1^1
f1^1
e1^1
d1^1
c+1^1
d1^1
c+1^1^1^1

#7

@32v155
y12

o3
r1
a1
g1^1
f1^1
e1^1
(50)[a1^1
     g1^1]2
@33v175
(51)[a16r16a16r16a8^16r16]4
@32v155
(50)5
@33v175
(51)8

@37v165
y8
$DE $2E $0C $38
$EA $35
>
f1^1
e1^1
d1^1
c+1^1
f1^1
e1^1
f1^1
a1^1
p0,0
[r2]16

@33v175
y12
f1^1
e1^1
d1^1
c+1^1
<a+1^1
a1^1
a+1^1
a1^1^1^1

#5

@32v155
y11
[r2]8
o3
d1^1
c1^2
c+2
(60)[f1^1
     e1^1]2
@33v175

(61)[f16r16f16r16f8^16r16]4
@32v155

(60)5
@33v175

(61)8

@37v165
y10
$DE $2E $0C $38
$EA $35
>
d1^1
c1^1
<a1^1
a1^1
>d1^1
c1^1
d1^1
e1^1
p0,0
o2
@9 v200
d4d2^4
d2d2
c4c2^4
c2c2
<a+4a+2^4
a+2a+2
a4a2^4
a2a2

@33v175
y11
o4
d1^1
c1^1
<a+1^1
a1^1
f1^1
e1^1
f1^1
e1^1^1^1

#4


y11
o4
[r2]16
 (80)[@30 v195c8v135c8v190c8v130c8v195c8v135c8v195c8v180c8]8
(81)[@30 v195r4c4]4
(80)20
(81)8
[c4]53
(82)[v195c8v130c8v195c4]4
c4c2
(82)1 r2
(82)1 v195c8v130c8c4
(82)1 r2
[v195c8v130c8]4
(82)1 r1^2
(82)1 r1^8
v195c16v190c16v195c4

@33
v185
y9
o5
d16r16d16r16d16r16d16r16d4e4
f4r4f4e4
c16r16c16r16c16r16c16r16c4d4
c2r2
<a+16r16a+16r16a+16r16a+16r16a+4>c4
d4r4d4c4
<a16r16a16r16a16r16a16r16a4a+4
a2r2
(83)[f16r16f16r16f16r16f16r16f4a4
     a+4]1 r4a+4a4
e16r16e16r16e16r16e16r16e4f4
e2r2
(83)1 >d4f4f4
e1^1^1^1

#6
$FA$03$45
v220
y10

[r2]8
o3
(70)[ @26 f8 @21 c16c16 @22 f+8 @21 c8 @26 f8 @21 c8 @29 b8 @22 f+8]3
 @26 f8 @21 c8 @22 f+8 @21 c8 @26 f8v096 @29 b16v160 @29 b16v220 @29 b8 @29 b8
(70)8
(71)[ @26 f8 @21 c8 @29 b8 @21 c8]4
(70)20
(71)8
[v220 @21 c8v160c8v110c8v080c8r2
 v220c8v160c8v110c8v080c8v220c8v160c8v220 @29 b8r8]6
[ @26 f8 @21 c8 @29 b8r8]6
 @21 c8c8c8c8 @29 b8r4^8
[ @21 c4c4r2
 c4c4c4c4]2
[c4c4r2]3
c4c4r8c8 @29 b8r8
(70)16
 @22 a+1^1                

#amk=1
