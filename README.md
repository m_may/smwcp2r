# SMWCP2 Development repo #
[Repo link](https://bitbucket.org/m_may/smwcp2r/)

First, apply the `SMWCP2R-date.bps` patch in bps folder and name the output rom `SMWCP2R.smc`, place it in the root directory.

## Using Lunar Helper ##
Lunar Helper is a tool to build a new ROM from extracted sources.

### Setting up Lunar Helper ###
Two things need to be done:

- Create a `config_user.txt` file in `helper` folder and add this line: `dir = C:/path/to/SMWCP2`, and replace `C:/path/to/SMWCP2` with a real path on your computer to the SMWCP2 base rom directory
- Place a clean SMW rom in `roms` folder (create it) and name it `SMW_clean.smc`.

### Porting to a new ROM ###
Make a backup of SMWCP2R.smc first. Run Lunar Helper. Press S key and verify everything got saved. Then press B key. Wait for it to build - the ROM should be now ready.

## Using git ##
### How do I get set up? ###
The only thing you need is git ([Windows download here](https://git-scm.com/downloads))

When installing git, make sure you select "Use from windows command line", otherwise it won't be added to the path/

After installing git, you will need to generate an ssh key. To do so, open a cmd window and type `ssh-keygen`, then enter. Follow the prompts to create your key. It will tell you where the key is output, take note of that location and open that folder. Find the file named `id_rsa.pub` and open that in a text editor.

Now, enter your bitbucket settings and find "SSH keys" on the sidebar. Choose "Add Key", copy the `id_rsa.pub` text from before, and paste it into the box labeled "Key". Also, label the key, so you know where it came from. For instance, if it's on your laptop, name it "laptop". Just enough to differentiate it from your other devices.

Once you have created the ssh key, you need to set up your email/username, like so:

    git config --global user.email email@example.com

and

    git config --global user.name "User Name"
	
Next, request access to the repo (recommended to keep friction low) or fork it.

Now that that is set up, you need to clone the repo. Open a command window in the location you want the repo, the copy the text from the top of the page (Next to where it says ssh) and run `git clone <repo_location>`, example `git clone https://bitbucket.org/m_may/smwcp2r.git`. Make sure you point it to your forked repo, not the main repo, otherwise you will not be allowed to push any changes. That will download all the latest source and put it on your local drive for editing.

### Contribution guidelines ###
Once you have done all that, you are ready to contribute. As of now, you are on the master branch. You should create your own branch.

To create a branch, run `git branch <branch_name>`. Try to give your branch name a meaningful name. For instance, if you are fixing a bug in a sprite, name your branch something like `fix/sprite-name-bug`, e.g. `fix/bute-arrows`. If you are fixing a level, name it something like `fix/level-name`, and if you are adding a new feature, name it something like `feat/new-world-2-boss` or something like that.

After creating a branch, run `git checkout <branch_name>`, and then make your changes as you see fit. Anytime you reach a good stopping point (e.g. if you are fixing a boss, each time you fix one of the broken parts), you can make a commit to save your changes. To make a commit, you must add your files to git. To add a file, run `git add <filepath>`. You can add single files or entire folders. If you want to add every file that you have changed, you can run `git add .`. If you want to look at all files that have changed, you can run `git status`. Only add the files that you want to commit.

To finally do the commit, use `git commit -m <commit message>`. Commit message is a description of what you have changed in this commit. It will help to track what has been done to the game. If one line is not enough, you can omit the `-m <commit message>` bit and it will bring up a text editor (vi by default) and you can add text there. However, vi is hard to use, and if you are reading this description because you don't know how git works, you probably don't know how vi works. If you accidentally get into vi and can't leave, press escape a bunch, then type `:q!` and enter. It will leave.

Once you have done all the commits you want, you can push the branch to the server with `git push origin <branch_name>`. Then, you can [create a pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html#Workwithpullrequests-Createapullrequest) to merge your changes with the master repo, and they will be reviewed and merged if it is decided that they should be in the ROM.

Important note: When you switch to working on an unrelated thing, DO NOT just run `git branch <branch_name>` again. That will create a branch from your current branch, instead of from master. Instead, run `git checkout master` to switch back to master (All of your changes will disappear, but don't worry, they aren't gone, they are just stashed away. Run `git checkout <branch_name>` at any time to get them back), `git pull` to update to the latest master, then run `git branch <other_branch>` to create your new branch and continue on.

If you ever need help with bitbucket realated things, they have a pretty good [help page](https://confluence.atlassian.com/bitbucket/use-your-repository-675189337.html) that you can check out.

## Who do I talk to? ##

If you need help with contributing using git, ask Disk Poppy. General project-specific questions should be directed to Falconpunch. You're welcome to join our Discord server as well.
