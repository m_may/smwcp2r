0	norveg.json
1	norveg_projectile.json
2	SMWCP2/GoalRoulette.cfg
3	SMWCP2/ButeA.cfg
4	norveg_particle.json
5	SMWCP2/albatoss.cfg
6	SMWCP2/FlameLakitu.cfg
7	SMWCP2/caveinblock.cfg
8	SMWCP2/P-trick.cfg
9	SMWCP2/frostee.cfg
A	SMWCP2/fire.cfg
B	SMWCP2/fire_pillar.cfg
C	SMWCP2/firebro.cfg
E	SMWCP2/icebro.cfg
F	SMWCP2/chainchomp.cfg
10	SMWCP2/boomerang_bro.cfg
11	SMWCP2/boomerang.cfg
12	SMWCP2/TorpedoTed.cfg
13	SMWCP2/firebar.json
14	SMWCP2/hammer_bro.json
15	SMWCP2/horizontal_thwomp.cfg
16	SMWCP2/projectile.cfg
17	SMWCP2/piranha_venus_all.cfg

1A	SMWCP2/TrollGoomba.cfg
1B	SMWCP2/Claw.cfg
1C	SMWCP2/nondynamicchomprock.cfg

1E	SMWCP2/roto_disc_m.cfg
1F	SMWCP2/lilypad.cfg
20	SMWCP2/flying_carpet_cardinal.cfg
21	SMWCP2/Bute2.cfg
22	SMWCP2/Bute3.cfg
23	SMWCP2/firesnake.cfg
24	SMWCP2/efreet.cfg
25	SMWCP2/fallingstuff.cfg
26	SMWCP2/bubble2.cfg
27	SMWCP2/rideable_balloon.cfg

2A	SMWCP2/timed_lift2.cfg
2B	SMWCP2/ClawHalf.cfg

2D	SMWCP2/TwoOfSMWCP.cfg
2E	SMWCP2/holdboo.cfg
2F	SMWCP2/ThrownVial.cfg
30	SMWCP2/KoopaPhD.cfg
31	SMWCP2/Jeptak.cfg
32	SMWCP2/BonusMagician.cfg

34	SMWCP2/JeptakGenerator.cfg
36	SMWCP2/porcupuffer_immune.cfg
37	SMWCP2/porcupuffer_boss.cfg

3A	SMWCP2/bowser_bowling_ball_A1.cfg
3B	SMWCP2/nipper_smb3.cfg
3C	SMWCP2/electricity.cfg
3D	SMWCP2/panserfire.cfg
3E	SMWCP2/ice_panser.cfg
3F	SMWCP2/crystal.cfg

40	SMWCP2/elitekoopa_g.cfg
41	SMWCP2/elitekoopa_r.cfg
42	SMWCP2/elitekoopa_y.cfg
43	SMWCP2/elitekoopa_b.cfg
44	SMWCP2/laser.cfg
45	SMWCP2/bullet_split.cfg

48	SMWCP2/Thwimp_Ninji.cfg
49	SMWCP2/yoshi_block.cfg
4A	SMWCP2/MoveLight.cfg

4C	SMWCP2/ShinyDimoFake.cfg
4D	SMWCP2/ShinyDimo.cfg
4E	SMWCP2/fakegravestone.cfg

54	SMWCP2/SMW_Hammer_Bro.cfg
55	SMWCP2/buster.cfg
56	SMWCP2/busterblk.cfg

5A	SMWCP2/disappear_sprite1.cfg
5B	SMWCP2/disappear_sprite2.cfg
5C	SMWCP2/disappear_sprite3.cfg
5D	SMWCP2/disappear_sprite4.cfg

5E	SMWCP2/NPC_Stand.cfg
5F	SMWCP2/NPC_Move.cfg
60	SMWCP2/Cat.cfg
61	SMWCP2/Push_Block.cfg
62	SMWCP2/pionpi.cfg
63	SMWCP2/pakkunflower.cfg
64	SMWCP2/smlorb.cfg
65	SMWCP2/gau.cfg
66	SMWCP2/smlfire.cfg
67	SMWCP2/door_gen.cfg
68	SMWCP2/snifit.cfg

6C	SMWCP2/spotlight.cfg
6D	SMWCP2/smb3_spike.cfg
6E	SMWCP2/spikes_ball.cfg
6F	SMWCP2/anti_boo.cfg
70	SMWCP2/baron_von_zeppelin.cfg
71	SMWCP2/dropper.cfg
72	SMWCP2/ptooie.cfg

74	SMWCP2/Sandcroc.cfg
75	SMWCP2/cbeetle2.cfg
76	SMWCP2/panser_remap.cfg
77	SMWCP2/timer.cfg
78	SMWCP2/competitor.cfg
79	SMWCP2/para_beetle.cfg
7A	SMWCP2/tanuki.cfg

7C	SMWCP2/dryad.cfg
7E	SMWCP2/PSwitch2.cfg
7F	SMWCP2/platform.cfg

81	SMWCP2/motor.cfg

83	SMWCP2/air_mushroom.cfg
84	SMWCP2/badge.cfg

86	SMWCP2/air_meter.cfg
87	SMWCP2/target2.cfg
88	SMWCP2/target.cfg
89	SMWCP2/math.cfg

8B	SMWCP2/Mini-puffer.cfg
8C	SMWCP2/verticalPuffer.cfg
8D	SMWCP2/horizontalPuffer.cfg
8E	SMWCP2/sparabuzzy_flying.cfg

92	SMWCP2/firechomp.json
93	SMWCP2/firechompflame.json
94	SMWCP2/piledriver.cfg
95	SMWCP2/piranjump.cfg
96	SMWCP2/drybones.cfg
97	SMWCP2/floorswooper.cfg
98	SMWCP2/beachball.cfg
99	SMWCP2/Remapped_swooper.cfg

9B	SMWCP2/arrow.cfg

9D	SMWCP2/score.cfg

9E	SMWCP2/ball-n-chain-pendulum.cfg

A2	SMWCP2/NewCart2.cfg
A3	SMWCP2/donut_lift.cfg
A4	SMWCP2/RotationalLift.cfg
A5	SMWCP2/RotationalLift2.cfg
A6	SMWCP2/SeeSawLift.cfg
A7	SMWCP2/mar_pop.cfg
A8	SMWCP2/poptorpedo.cfg

A9	doccroc.json
AA	crocmisc.cfg
AB	SMWCP2/chomp_shark_mini.cfg

AE	SMWCP2/tilesm7.cfg

B0	SMWCP2/Jelectro.cfg
B1	SMWCP2/Elec.cfg

B4	SMWCP2/desertboss.cfg
B5	SMWCP2/balloyarn.cfg
B6	SMWCP2/sandblock.cfg
B7	SMWCP2/pyramid.cfg

B9	SMWCP2/mechatree.json

BB	SMWCP2/wheel_boss.cfg
BF	SMWCP2/firework.cfg

BC	SMWCP2/frankboss.json
BD	SMWCP2/slidepillar.json

C0	split_shooter.cfg

C3	diag_bill.cfg
C4	Throwpipe.cfg

C6	CustomShooter.cfg
C7	bubble_shooter.cfg
C8	upwards_launcher.cfg
C9	lightning_shoot.cfg
CA	random_shell.cfg

CC	goombagun.cfg
CE	lasershooter.cfg
CF	arrowlaunch.cfg

D6	iceskate.json

DA	caveingen.cfg

CLUSTER:
02 sandstorm.asm
03 laser.asm
04 flowers.asm
05 bullet_croc.asm
06 particles.asm
07 rain.asm
08 crocfire.asm

EXTENDED:
00 fireball.asm
01 iceball.asm
02 crocglass.asm