;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cave-In Generator, by yoshicookiezeus
;;
;; Description: Spawns Cave-In Blocks at random x positions at a set height.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; If your ceiling is too high or !BlockHeight is too low, the blocks could
; get "killed" when they spawn. If that happens, try increasing the !BlockHeight.

!BlockNumber = $07      ; Sprite number the cave-in block sprite is inserted as
!BlockHeight = $00C0    ; The Y position at which blocks are spawned.

print "INIT ",pc
print "MAIN ",pc
    PHB : PHK : PLB
    JSR GeneratorCode
    PLB
    RTL

GeneratorCode:
    LDA $14                 ;\ if not yet time to spawn new block,
    AND #$1F                ; |
    BNE Return              ;/ return

    LDA #$40                ;\ shake ground
    STA $1887|!Base2        ;/

    JSL $02A9DE|!BankB      ;\ if no empty slots,
    BMI Return              ;/ return

    TYX

    LDA #$01                ;\ set sprite state for new sprite
    STA !14C8,x             ;/

    LDA.b #!BlockHeight     ;\ set new sprite y position
    STA !D8,x               ; |

    if !BlockHeight>>8&$FF == 0
        STZ !14D4,x         ;/
    else
        LDA.b #!BlockHeight>>8
        STA !14D4,x         ;/
    endif

    JSL $01ACF9|!BankB      ; random number generation subroutine
    REP #$20                ;\  set new sprite y position
    LDA $148B|!Base2        ; |
    AND #$00FF              ; |
    CLC : ADC $1A           ; |
    SEP #$20                ; |
    STA !E4,x               ; |
    XBA                     ; |
    STA !14E0,x             ;/

    LDA.b #!BlockNumber     ;\ set new sprite number
    STA !7FAB9E,x           ;/
    JSL $07F7D2|!BankB      ; clear out sprite tables
    JSL $0187A7|!BankB      ; get table values for custom sprite
    LDA $148B|!Base2        ;\ set extra bit randomly
    AND #$04                ;/
    ORA #$88                ;\ mark sprite as custom
    STA !7FAB10,x           ;/
Return:
    RTS
