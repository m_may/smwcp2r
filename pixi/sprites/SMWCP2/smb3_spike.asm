;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Spike, by mikeyk
;;
;; Uses first extra bit: YES
;; If the fitrt extra bit is set, the sprite generated is controlled by !BallSpriteNum
;; below.  This is so different color spikes can share the same ball sprite.
;; If the bit is not set, the ball must be the next sprite in the sprite list.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!UpdateSpritePos = $81802A  
	!SprSprInteract = $818032   
	!MarioSprInteract = $81A7DC 
	!FinishOAMWrite = $81B7B3
	!FindFreeSlotLowPri = $82A9DE
	!InitSpriteTables = $87F7D2
	!InitCustomSprTables = $8187A7
	!GetSpriteClippingA = $83B69F
	!CheckForContact = $83B72B	
	!GetRand  = $81ACF9
	
	!ExtraProperty1 = !7FAB28
        !NewSpriteNum = !7FAB9E
        !ExtraBits = !7FAB10	
	
	!BallDisplacement = $0E
	!BallTile = $A4
	!Timing1 = $10	    	   ; Paused after throwing    ;10
	!Timing2 = !Timing1+$0C      ; Holding ball above head	;0c
	!Timing3 = !Timing2+$0A 	   ; Retrieving ball frame 2	;0a
	!Timing4 = !Timing3+$08  	   ; Retrieving ball frame 1	;08
	!TimeToPause = !Timing4+$0c  ; Paused before throwing	;0c
	!MinTimeToWalk = $A0
	!BallSpriteNum = $41 	   ; Sprite number of Spike's Ball (Only used if first extra bit is set)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        PRINT "INIT ",pc
	LDA #$01
	STA $151C,x
        %SubHorzPos()
        TYA
        STA $157C,x
	JSR SETWALKTIME
        RTL     

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        PRINT "MAIN ",pc
	PHB        
        PHK        
        PLB
	CMP #$02
	BEQ KILLED
	CMP #$08
	BNE SKIPMAIN
        JSR MAINSUB
        PLB        
        RTL        

KILLED:	
	LDY $15EA,x
	LDA #$A0
	STA $0302,y
SKIPMAIN:		
	PLB        
        RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:
	RTS
MAINSUB:
	LDA $1656,x		; Can be stomped on
	ORA #$10
	STA $1656,x
	LDA $1662,x		; 1 tile high
	AND #$C0		
	STA $1662,x
	
	JSR SUBGFX
	LDA $9D
	BNE RETURN
	LDA #$00
	%SubOffScreen()

	INC $1570,x
	
	LDA $C2,x
	BEQ WALKINGSTATE
	
THROWINGSTATE:
	LDA $1540,x
	CMP #!Timing1
	BNE NOTHROW
	JSR GENERATESPRITE
	;; 	LDA #$F0
	;; 	STA $AA,x
	;; 	LDY $157C,x
	;; 	LDA RECOILSPEEDX,y
	;; 	STA $B6,x
NOTHROW:
	JSR MAYBEFACEMARIO
	
	JSL $81802A             ; update position based on speed values
        JSL $818032             ; interact with other sprites
	LDA $1656,x
	PHA
	AND #$EF
	STA $1656,x
	JSL $81A7DC             ; Interact with Mario
	PLA
	STA $1656,x
	
	LDA $1540,x
	BNE RETURN2
	STA $C2,x
	JSR SETWALKTIME
RETURN2:	
	RTS                     ; RETURN
	
RECOILSPEEDX:
	db $FB,$05
SPEEDX:
	db $08,$F8,$0C,$F4
	
WALKINGSTATE:	
	LDA $1588,x             ; If sprite is in contact with a wall,
        AND #$03                
        BEQ NOWALLCONTACT	;   change direction
        JSR CHANGEDIRECTION
NOWALLCONTACT:

	JSR MAYBESTAYONLEDGES

	LDA $1588,x             ; if on the ground, reset the turn counter
        AND #$04
        BEQ NOTONGROUND
	STZ $AA,x
	STZ $151C,x		; Reset turning flag (used if sprite stays on ledges)
	JSR MAYBEFACEMARIO
	JSR MAYBEJUMPSHELLS
NOTONGROUND:	

	LDY $157C,x             ; Set x speed based on direction
	LDA !ExtraProperty1,x
	AND #$01
	BEQ NOFASTSPEED		; Increase speed if bit is set
	INY
	INY
NOFASTSPEED:
        LDA SPEEDX,y           
        STA $B6,x

	JSL $81802A             ; update position based on speed values
        JSL $818032             ; interact with other sprites
	LDA $1656,x
	PHA
	AND #$EF
	STA $1656,x
	JSL $81A7DC             ; Interact with Mario
	PLA
	STA $1656,x

	LDA $1588,x
	AND #$04
	BEQ RETURN1
	LDA $1540,x
	BNE RETURN1
	INC $C2,x
	STZ $B6,x
	STZ $AA,x
	;; 	LDA #$FF
	;; 	STA $1528,x
	LDA #!TimeToPause
	STA $1540,x

RETURN1:	
	RTS                     ; RETURN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MAYBESTAYONLEDGES:	
	LDA !ExtraProperty1,x	; Stay on ledges if bit is set
	AND #$02                
	BEQ NOFLIPDIRECTION
	LDA $1588,x             ; If the sprite is in the air
	ORA $151C,x             ;   and not already turning
	BNE NOFLIPDIRECTION
	JSR FLIPDIRECTION	;   flip direction
        LDA #$01                ;   set turning flag
	STA $151C,x    
NOFLIPDIRECTION:
	RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
MAYBEFACEMARIO:
	;LDA !ExtraProperty1,x	; Face Mario if bit is set
	AND #$04
	BEQ RETURN4	
	LDA $1570,x
	AND #$2F
	BNE RETURN4
	%SubHorzPos()
        TYA
        STA $157C,x
RETURN4:	
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
MAYBEJUMPSHELLS:
	LDA !ExtraProperty1,x	; Face Mario if bit is set
	AND #$08
	BEQ RETURN4
	TXA                       ; \ Process every 4 frames 
        EOR $13                   ;  | 
        AND #$03		  ;  | 
        BNE RETURN0188AB          ; / 
        LDY #$09		  ; \ Loop over sprites: 
JUMPLOOPSTART:
	LDA $14C8,Y               ;  | 
        CMP #$0A       		  ;  | If sprite status = kicked, try to jump it 
        BEQ HANDLEJUMPOVER	  ;  | 
JUMPLOOPNEXT:
	DEY                       ;  | 
        BPL JUMPLOOPSTART         ; / 
RETURN0188AB:
	RTS                       ; RETURN 

HANDLEJUMPOVER:
	LDA $00E4,Y             
        SEC                       
        SBC #$1A                
        STA $00                   
        LDA $14E0,Y             
        SBC #$00                
        STA $08                   
        LDA #$44                
        STA $02                   
        LDA $00D8,Y             
        STA $01                   
        LDA $14D4,Y             
        STA $09                   
        LDA #$10                
        STA $03                   
        JSL !GetSpriteClippingA  
        JSL !CheckForContact     
        BCC JUMPLOOPNEXT          ; If not close to shell, go back to main loop
	LDA $1588,x 		  ; \ If sprite not on ground, go back to main loop 
	AND #$04		  ;  |
        BEQ JUMPLOOPNEXT          ; / 
        LDA $157C,Y               ; \ If sprite not facing shell, don't jump 
        CMP $157C,X               ;  | 
        BEQ RETURN0188EB          ; / 
        LDA #$C0                  ; \ Finally set jump speed 
        STA $AA,X                 ; / 
	;;         STZ $163E,X             
RETURN0188EB:
	RTS                       ; RETURN
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
FLIPDIRECTION:	
	LDA $B6,x
        EOR #$FF
        INC A
        STA $B6,x
CHANGEDIRECTION:	
        LDA $157C,x
        EOR #$01
        STA $157C,x
        RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

SETWALKTIME:
	JSL !GetRand
	AND #$3F
	CLC
	ADC #!MinTimeToWalk
	STA $1540,x
	RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw Sprite
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
TILES:           	                   
	db $A0,$A2,$A0,$A2
	db $A2,$C0,$C2,$C4,$A0
		
SUBGFX:
	%GetDrawInfo()

	LDA $157C,x		; $02 = X flip info
	BNE STOREDIRECTION
	LDA #$40
STOREDIRECTION:	
	STA $02

	LDA #$01
	STA $03

	PHX
	LDA $C2,x
	BEQ WALKINGGFX

THROWINGGFX:	
	LDA $1540,x
	LDX #$04
	CMP #!Timing4
	BCS DRAWBODY
	INX
	CMP #!Timing3
	BCS DRAWBODY
	INX
	CMP #!Timing2
	BCS DRAWBODY
	INX
	CMP #!Timing1+$01
	BCS DRAWBALL
	INX
	BRA DRAWBODY
	
WALKINGGFX:	
	LDA $14			; X = Tile index
	LSR
	LSR
	LSR
	;; 	LSR
	CLC
	ADC $15E9	
	;; 	AND #$01
	AND #$03
	TAX
	BRA DRAWBODY

DRAWBALL:
	PHX
	LDX $15E9		
	LDA $1656,x		; Can't be stomped on
	AND #$EF
	STA $1656,x
	LDA $1662,x		; 2 TILES high
	ORA #$37
	STA $1662,x
	PLX
	
	INC $03

	LDA $00
        STA $0304,y

	LDA $01
	SEC
        SBC #!BallDisplacement
        STA $0305,y   	

	LDA #!BallTile
	STA $0306,y

	LDA #$03
        ORA $64                 ; Add in tile priority
        STA $0307,y             
	
DRAWBODY:	
        LDA $00               
        STA $0300,y           
        
        LDA $01               
        STA $0301,y           

	LDA TILES,x
	STA $0302,y
	
        PLX           
        LDA $15F6,x             ; Get palette number
        ORA $02
        ORA $64                 ; Add in tile priority
        STA $0303,y             

        LDY #$02		; 16x16 tile
        LDA $03			; $03 = number of TILES drawn
        JSL !FinishOAMWrite
NODRAW:	
        RTS	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GENERATESPRITE:	
	LDA $15A0,x            	; Don't generate if off screen
        ORA $186C,x            
        ORA $15D0,x		;   or being eaten
        BNE RETURN3

        JSL !FindFreeSlotLowPri	; Y = free sprite slot
        BMI RETURN3

        LDA #$08  		; Status = normal
        STA $14C8,y

        PHX
	LDA !ExtraBits,x
	AND #$04
	BEQ NEXTSPRITE
	LDA #!BallSpriteNum
	BRA SETSPRITE
NEXTSPRITE:	
        LDA !NewSpriteNum,x
        INC A
SETSPRITE:	
        TYX
        STA !NewSpriteNum,x
        PLX

        LDA $E4,x		; Set X position for new sprite
        STA $00E4,y
        LDA $14E0,x
        STA $14E0,y

        LDA $D8,x		; Set Y position for new sprite
        SEC             
        SBC #!BallDisplacement        
        STA $00D8,y     
        LDA $14D4,x     
        SBC #$00        
        STA $14D4,y     

        PHX                     
        TYX                     
        JSL !InitSpriteTables
        JSL !InitCustomSprTables
        LDA #$08		; Set custom flag
        STA !ExtraBits,x
        PLX                  

        LDA $157C,x		
        STA $157C,y

	;; 	TYA			; Save index of new sprite
	;; 	STA $1528,x
	
RETURN3:
	RTS 