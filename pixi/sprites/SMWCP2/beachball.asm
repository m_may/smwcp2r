;Beach Ball Spawner + Bouncy Beach Ball by Blind Devil
;Guess what? The sprite was redone!

!Interval = $D0		;amount of frames between every ball spawned

BallSpds:
db $18,$E8

Tilemap:
db $86,$86	;top (frame 1)
db $A6,$A6	;bottom (frame 1)

db $88,$88	;top (frame 2)
db $A8,$A8	;bottom (frame 2)

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
print "INIT ",pc
RTL

SpriteCode:
JSR Graphics

LDA !14C8,x
CMP #$08
BNE return
LDA $9D
BNE return

LDA #$03
%SubOffScreen()

LDA !C2,x		;if !C2,x is non-zero, then it's a beach ball
BNE BeachBall		;otherwise it's a spawner.

LDA !15AC,x
BNE return

STZ $00
LDA #$FF
STA $01
LDA !7FAB9E,x
SEC
%SpawnSprite()

LDA #!Interval
STA !15AC,x

LDA #$28
STA !154C,y

CPY #$FF
BEQ return

LDA #$01
STA !C2,y

LDA !7FAB10,x
AND #$04
BEQ +

LDA #$01
STA !157C,y

+
LDA #$02
STA $1DFC|!Base2

return:
RTS

BeachBall:
LDY !157C,x
LDA BallSpds,y
STA !B6,x

JSL $01A7DC|!BankB
BCC NoContact

JSR PlayerInteraction

NoContact:
JSL $018022|!BankB

LDA !154C,x
BNE return

JSL $019138|!BankB

LDA !1588,x
AND #$03	;if touching any walls
BEQ return

STZ !14C8,x	;adios sprite

LDA #$08
STA $00
STA $01
LDA #$1B
STA $02
LDA #$01
%SpawnSmoke()
RTS

PlayerInteraction:
		LDA !163E,x
		BNE INVTIME2
		LDA #$10
		STA !163E,x
		LDA $0E			; is mario above the sprite?
		CMP #$E8		; cause if he is.
		BMI NOHORZ_PUSH		; sprite should not push him horz.
		CMP #$0C
		BPL NOHORZ_PUSH
		JSR SUB_HORZ2_POS
		LDA !157C,x
		BEQ NOADDFLIP
		INY
		INY
NOADDFLIP:
	LDA PUSH_SP,y
        STA $7B		;Mario X speed

NOHORZ_PUSH:
		LDA $0E		; is mario above the sprite?
		CMP #$E8	; cause if he is.
		BMI DOWN_CONTACT	; sprite should not push upwards (down CONTACT)
		CMP #$00
		BPL UP_CONTACT

CONTINUE_CONTACT:
        LDA #$08
        STA $1DFC|!Base2 ;Sound

CONTINUE_CONTACT2:
INVTIME2:
        RTS

DOWN_CONTACT:
		LDA $77
		AND #$04
		BNE CONTINUE_CONTACT2
		LDA #$B0
		STA $7D
		LDA $75
		BNE WATER_CONTACT
		LDA $15
		AND #$80
		BNE EXTRA_BOOST
		BRA CONTINUE_CONTACT
WATER_CONTACT:
		LDA #$D0
		STA $7D
		BRA CONTINUE_CONTACT
EXTRA_BOOST:
		LDA #$A3
		STA $7D
		BRA CONTINUE_CONTACT
UP_CONTACT:
		LDA #$09
		STA $7D
		BRA CONTINUE_CONTACT

PUSH_SP:
db $38,$E4,$1C,$C8

XDisp:
db $00,$10
db $00,$10

YDisp:
db $00,$00
db $10,$10

Props:
db $00,$40	;tiles are mirrored horizontally
db $00,$40

Graphics:
LDA !C2,x
BNE +
RTS

+
%GetDrawInfo()

LDA $14
LSR #2
CLC
ADC $15E9|!Base2
AND #$01
ASL #2
STA $02		;00 or 04

PHX
LDX #$03

GFXLoop:
LDA $00
CLC
ADC XDisp,x
STA $0300|!Base2,y

LDA $01
CLC
ADC YDisp,x
STA $0301|!Base2,y

PHX
TXA
CLC
ADC $02
TAX
LDA Tilemap,x
STA $0302|!Base2,y

LDX $15E9|!Base2
LDA !15F6,x
PLX
ORA Props,x
ORA $64
STA $0303|!Base2,y

INY #4
DEX
BPL GFXLoop

PLX

LDY #$02
LDA #$03
JSL $01B7B3|!BankB
RTS

SUB_HORZ2_POS:		LDA !157C,x
					BEQ RIGHT_SUB
					LDY #$00				;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
					LDA !E4,x
					CLC
					ADC #$10
					STA $00
					LDA !14E0,x
					ADC #$00
					STA $01
					LDA $94					;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
					SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
					SBC $00				;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
					STA $0F					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
					LDA $95					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
					SBC $01			;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
					BPL SPR2_L16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
					INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
SPR2_L16:				
					RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642
RIGHT_SUB:
					LDY #$00				;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
					LDA !E4,x
					STA $00
					LDA !14E0,x
					STA $01
					REP #$20
					LDA $94					;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
					SEC
					SBC $00
					BPL SPR2R_L16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
					INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
SPR2R_L16:				
                    SEP #$20
					RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642