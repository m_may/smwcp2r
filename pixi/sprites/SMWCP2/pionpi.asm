;Pionpi by smkdan (optimized by Blind Devil)
;if extra bit is set, spin jumps / yoshi won't kill and it will just knockout as normal

;GFX from andyk250

;I lifted some of the code from the Rex since it's stomping behaviour can be applied here

;Tilemap defines:
TILEMAP:	db $80,$84	;head, sitting frame
		db $80,$82	;head, jumping frame
		db $8E,$8E	;knock out frame x2

;how long to stay knocked out
!KOtime = $80

;how long between jumps
!JumpInterval = $40

;speeds for jumping
!JumpX = $20
!JumpY = $20

;some ram
;1570:	state
;	00: living and standing (going to jump)
;	01: living and in air
;	02: knocked out
;1602:	general timer for above states

print "INIT ",pc
	%SubHorzPos()		;face mario
	TYA
	STA !157C,x		;new direction

	STZ !1570,x
	LDA #!JumpInterval	;set timer for jumping interval
	STA !1602,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

Return_I:
	RTS

Run:
	LDA #$00
	%SubOffScreen()
	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE Return_I           
	LDA $9D			;locked sprites?
	BNE Return_I
	LDA !15D0,x		;yoshi eating?
	BNE Return_I

	JSL $01802A|!BankB	;speed

;figure out what to do according to state
	LDA !1570,x		;load state
	BEQ DoLiveStand		;00
	DEC A
	BEQ DoLiveAir		;01
	DEC A
	BEQ DoKnockedOut	;02

DoLiveStand:
	DEC !1602,x		;decrement timer
	BEQ Jump
	JMP Interaction

Jump:
	%SubHorzPos()		;first, face mario just before the jump
	TYA
	STA !157C,x		;no direction

	LDA #!JumpX		;load !JumpX speed
	LDY !157C,x		;load direction
	BEQ NoFlipJ
	EOR #$FF		;invert bits
	INC A			;+1, get correct negative value
NoFlipJ:
	STA !B6,x		;set xspd

	LDA #!JumpY		;load !JumpY speed
	EOR #$FF		;invert
	STA !AA,x		;set yspd
	
	LDA #$01		;set in air status
	STA !1570,x

	JMP Interaction		;just jump to interaction routine

DoLiveAir:
	LDA !1588,x		;test for wall contact
	AND #$03
	BEQ +

	LDA !157C,x		;flip direction because it's touching a wall
	EOR #$01
	STA !157C,x

+
	LDA !1588,x		;test if on ground
	BIT #$04
	BNE OnGround

;restore xspd according to new direction
	LDA #!JumpX		;load !JumpX speed
	LDY !157C,x		;load direction
	BEQ NoFlipJA
	EOR #$FF		;invert bits
NoFlipJA:
	STA !B6,x		;set xspd

	JMP Interaction	

OnGround:
	STZ !1570,x
	LDA #!JumpInterval
	STA !1602,x		;set jump interval in timer
	STZ !B6,x		;no xspd
	JMP Interaction		;just jump to interaction routine

DoKnockedOut:
	LDA !B6,x		;test xspd, slow down if not stopped already
	BEQ Still
	BMI IncX
	DEC !B6,x		;going right, --
	BRA Still

IncX:
	INC !B6,x		;going left, --

Still:
	LDA !1588,x		;test for wall contact
	AND #$03
	BEQ +

	STZ !B6,x		;nullify x speed immediately
	LDA !157C,x		;flip direction because it's touching a wall
	EOR #$01
	STA !157C,x

+
	DEC !1602,x		;decrement timer
	BEQ LiveAgain
	RTS			;just return

LiveAgain:
	STZ !1570,x
	LDA #!JumpInterval	;set jump interval in timer
	STA !1602,x

Interaction:
	JSL $01A7DC|!BankB	;mario interact
	BCC NO_CONTACT          ; (carry set = mario/rex contact)
	LDA $1490|!Base2        ; \ if mario star timer > 0 ...
	BNE HAS_STAR            ; /    ... goto HAS_STAR
	LDA !154C,x             ; \ if rex invincibility timer > 0 ...
	BNE NO_CONTACT          ; /    ... goto NO_CONTACT
	LDA $7D                 ; \  if mario's y speed < 10 ...
	CMP #$10                ;  }   ... rex will hurt mario
	BMI REX_WINS            ; /    

MARIO_WINS:
	JSR SUB_STOMP_PTS       ; give mario points
	JSL $01AA33|!BankB      ; set mario speed
	JSL $01AB99|!BankB      ; display contact graphic
	LDA $140D|!Base2        ; \  if mario is spin jumping...
	ORA $187A|!Base2        ;  }    ... or on yoshi ...
	BNE SPIN_KILL           ; /     ... goto SPIN_KILL

;set state to knocked out
	LDA #$02		;set knockout state
	STA !1570,x	
	LDA #!KOtime
	STA !1602,x		;set time to be knocked out
	RTS                     ; return 

REX_WINS:
	LDA $1497|!Base2        ; \ if mario is invincible...
	ORA $187A|!Base2        ;  }  ... or mario on yoshi...
	BNE NO_CONTACT          ; /   ... return
	%SubHorzPos()           ; \  set new rex direction
	TYA                     ;  }  
	STA !157C,x             ; /
	JSL $00F5B7|!BankB      ; hurt mario
	RTS

SPIN_KILL:
;first we need to check the extra bit
	LDA !7FAB10,x		;custom sprite properties
	AND #$04		;test extra bit
	BEQ ProceedSpinKill

	JSL $01AA33|!BankB	; set mario speed
	JSL $01AB99|!BankB	; display contact graphic

;set state to knocked out
	LDA #$02		;set knockout state
	STA !1570,x	
	LDA #!KOtime
	STA !1602,x		;set time to be knocked out
	RTS                     ; return 
		
ProceedSpinKill:
	LDA #$04                ; \ rex status = 4 (being killed by spin jump)
	STA !14C8,x             ; /   
	LDA #$1F                ; \ set spin jump animation timer
	STA !1540,x             ; /
	JSL $07FC3B|!BankB      ; show star animation
	LDA #$08                ; \ play sound effect
	STA $1DF9|!Base2        ; /

NO_CONTACT:
	RTS                     ; return

STAR_SOUNDS:        db $00,$13,$14,$15,$16,$17,$18,$19
KILLED_X_SPEED:     db $F0,$10

HAS_STAR:
	%Star()
	RTS                     ; final return

SUB_STOMP_PTS:      PHY                     ; 
                    LDA $1697|!Base2        ; \
                    CLC                     ;  } 
                    ADC !1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697|!Base2        ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y             ; \ play sound effect
                    STA $1DF9|!Base2        ; /   
NO_SOUND:           TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:           JSL $02ACE5|!BankB      ; give mario points
                    PLY                     ;
                    RTS                     ; return
	
;=====

XDISP:		db $02,$00
		db $02,$00
		db $00,$00

YDISP:		db $F5,$03
		db $F2,$00
		db $FF,$FF

GFX:
	%GetDrawInfo()
	LDA !157C,x	;direction...
	STA $03

	LDA !15F6,x	;properties...
	STA $04

	LDA $03		;test direction
	BNE NoFlip
	LDA #$40
	TSB $04		;set flip bit

NoFlip:
	LDA !1570,x	;load state (and frame to use)
	ASL		;x2
	STA $09		;convenient frame index

	LDX #$00	;reset loop index

OAM_Loop:
	PHX		;preserve loop index
	LDX $09		;load frame index

	LDA $03		;test direction
	BEQ FlipX

	LDA $00
	CLC
	ADC XDISP,x		;xpos
	STA $0300|!Base2,y
	BRA DoY

FlipX:
	LDA $00
	SEC
	SBC XDISP,x		;xpos flip
	CLC
	ADC #$04
	STA $0300|!Base2,y

DoY:
	LDA $01
	CLC
	ADC YDISP,x		;ypos
	STA $0301|!Base2,y

	LDA TILEMAP,x		;chr
	STA $0302|!Base2,y

	LDA $04			;properties
	ORA $64
	STA $0303|!Base2,y

	INY			;next tile
	INY
	INY
	INY

	INX
	STX $09			;store to frame index

	PLX			;pull loop index
	INX
	CPX #$02		;2 tiles
	BNE OAM_Loop

	LDX $15E9|!Base2	;restore sprite index
	LDY #$02		;16x16
        LDA #$01		;2 tiles
        JSL $01B7B3|!BankB	;reserve
	RTS