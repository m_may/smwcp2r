;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FOR USE WITH DESERT BOSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!FrameRate = $04

;Adresses
!IsReturning = !C2         ; \
!Variant = !151C           ; | MUST BE THE SAME ADDRESSES AS IN desertboss.asm
!TimerBeforeReturn = !15AC ; /

PyramidTile:
db $80,$82,$84

Acceleration:
db $FF,$01,$FE,$02

	print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR GraphicsRoutine
	JSR SpriteCode
	PLB
	print "INIT ",pc
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
	LDA !14C8,x
	CMP #$08
	BNE .Return

	LDA $9D
	BNE .Return

	LDA !15AC,x  ; timer
	BNE .EndIncState
.IncState
	LDA #$01
	STA !C2,x
.EndIncState
	LDA !C2,x
	BEQ .EndReturnPyramid
.ReturnPyramid
	LDY !157C,x  ; direction
	LDA !Variant,x
	BEQ +
	INY
	INY
+
	LDA !sprite_speed_x,x
	CLC : ADC Acceleration,y
	BVS .EndReturnPyramid
	STA !sprite_speed_x,x
.EndReturnPyramid
	

	JSL $018022|!bank  ; update x position from x speed
	JSL $01A7DC|!bank  ; interaction with player

	LDA !1570,x
	INC
	CMP #!FrameRate
	BEQ .NewFrame
	STA !1570,x
.Return
	RTS

.NewFrame
	STZ !1570,x
	LDA !1602,x
	CMP #$02
	BEQ .ZeroFrame
	INC
	STA !1602,x
	RTS

.ZeroFrame
	STZ !1602,x
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GraphicsRoutine:
	%GetDrawInfo()

	LDA $00
	STA $0300|!addr,y
	LDA $01
	STA $0301|!addr,y

	LDA !1602,x
	TAX
	LDA PyramidTile,x
	LDX $15E9|!addr
	STA $0302|!addr,y

	LDA !15F6,x
	ORA #$30
	STA $0303|!addr,y

	LDY #$02
	LDA #$00
	JSL $01B7B3|!bank
	RTS