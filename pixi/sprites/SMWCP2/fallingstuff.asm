;extra bit clear = spawn normal sprite
;extra bit set = spawn custom sprite
;extension 1 = sprite number to spawn
;extension 2 = spawning interval, in frames


			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR Main
			PLB
			print "INIT ",pc 
			RTL

Main:
			LDA !C2,x
			BNE Object

			LDA !15AC,x
			ORA $9D
			BNE Ret

.DoSpawn
			STZ $00 : LDA #$F4 : STA $01
			STZ $02 : STZ $03
			LDA !7FAB40,x
			XBA 

			LDA !7FAB10,x
			AND #$04
			BEQ .normal

			XBA
			SEC
			BRA .spawn

.normal
			XBA
			CLC
.spawn
			%SpawnSprite()

			PHX
			TYX
			LDA !7FAB9E,x
			PLX
			CMP !7FAB9E,x
			BNE +

			LDA #$01
			STA !C2,y

+
			LDA !7FAB4C,x
			STA !15AC,x

Ret:
			RTS

Object:			JSR SpriteGraphics



			LDA $9D
			BNE Ret

			LDA !14C8,x
			CMP #$08
			BNE Ret

			LDA #$00
			%SubOffScreen()

			JSL $01802A|!BankB	;x/y with gravity
			JSL $01A7DC|!BankB
			RTS

SpriteGraphics:		%GetDrawInfo()

			REP #$20
			LDA $00
			STA $0300|!Base2,y
			SEP #$20
			LDA #$A0
			STA $0302|!Base2,y
			LDA !15F6,x
			ORA $64
			STA $0303|!Base2,y

			LDY #$02
			LDA #$00
			JSL $01B7B3|!BankB
			RTS