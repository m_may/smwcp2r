;pakkun flower (SML) by smkdan (optimized by Blind Devil)
;GFX from andy_k_250

;I lifted some of the code from the Rex since it's stomping behaviour can be applied here

!Projectile = $64				;<<< set this to the same slot number as smlorb.cfg!

;Tilemap defines:
!StemProp =	$0A	;stem palette properties, YXPPCCCT format ($FF to use properties from CFG, which's same as the body)

;fire timers
!FireInterval = $80
!FireTime = $20

!SquishTime =	$40		;time for flower to stay squished before falling off

;speed
XSPD:	db 08,$F8

;some ram
;1504: seperate frame index
;1570:	state
;	00: living and idle
;	01: living and in air
;	02: squished
;	03: dying
;1602:	general timer for above states

print "INIT ",pc
	%SubHorzPos()		;face mario
	TYA
	STA !157C,x		;new direction

	STZ !1570,x
	LDA #!FireInterval	;jump interval into timer
	STA !1602,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

Return_I:
	RTS

Run:
	LDA #$00
	%SubOffScreen()
	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE Return_I           
	LDA $9D			;locked sprites?
	BNE Return_I
	LDA !15D0,x		;yoshi eating?
	BNE Return_I

	JSL $01802A|!BankB	;regular speed update
	JSL $018032|!BankB	;interact with sprites

;figure out what to do according to state
	LDA !1570,x		;load state
	BEQ DoLiveWalk		;00
	DEC A
	BEQ DoLiveFire		;01
	DEC A
	BEQ DoSquish		;02
	JMP DoDieing		;03

DoLiveWalk:
	DEC !1602,x		;decrement timer
	BEQ EnterLiveFire

	LDA $14			;frame counter
	LSR #2			;/4
	AND #$03		;4 frames
	STA !1504,x

	LDA !1588,x		;touching wall?
	AND #$03
	BEQ NoWall

	LDA !157C,x		;direction
	EOR #$01		;flip
	STA !157C,x

NoWall:
	LDY !157C,x		;load direction
	LDA XSPD,y
	STA !B6,x		;xspd

	JMP Interaction

EnterLiveFire:
	JSL $02A9E4|!BankB	;grab free sprite slot
	BMI NoneFree		;return if none left before anything else...

	LDA #$01		;set fire status
	STA !1570,x
	LDA #!FireTime		;set firing time
	STA !1602,x

	JSR SetupCust		;generate sprite
	JMP Interaction

NoneFree:
	STZ !1570,x
	LDA #!FireInterval	;interval timer
	STA !1602,x

	JMP Interaction

DoLiveFire:
	DEC !1602,x		;decrement timer
	BEQ EnterLiveIdle

	STZ !B6,x		;no xspd

	LDA #$02		;set firing frame
	STA !1504,x
	JMP Interaction

EnterLiveIdle:
	STZ !1570,x
	LDA #!FireInterval	;set duration timer
	STA !1602,x

	%SubHorzPos()		;face mario
	TYA
	STA !157C,x		;new direction

	JMP Interaction

;STANDARD
DoSquish:
	LDA !B6,x		;test xspd, slow down if not stopped already
	BEQ Still
	BMI IncX
	DEC !B6,x		;going right, --
	BRA Still

IncX:
	INC !B6,x		;going left, --

Still:
	LDA #$04		;set squished frame
	STA !1504,x
	LDA !167A,x		;load fourth tweaker byte
	ORA #$02		;set bit to make it immune to cape/fire/bricks
	STA !167A,x		;store result back.
	LDA !166E,x		;load third tweaker byte
	ORA #$20		;set bit to make it not interact to cape spins (immune =/= interactable)
	STA !166E,x		;store result back.
	LDA !1686,x		;load fifth tweaker byte
	ORA #$01		;set bit to make it inedible by Yoshi
	STA !1686,x		;store result back.

	LDA !1588,x		;don't touch counter if not on ground
	BIT #$04
	BNE OnGround
	RTS

OnGround:
	DEC !1602,x		;decrement timer
	BEQ EnterDieing
	RTS			;don't interact

;STANDARD
EnterDieing:
	LDA #$FF		;erase sprite permanetley
	STA !161A,x

	LDA #$03		;enter dieing status
	STA !1570,x
	LDA !1686,x		;load 1686 tweaker byte
	ORA #$80		;don't interact wiht objects
	STA !1686,x

	LDA #$E0		;rise a bit with new yspeed
	STA !AA,x
	LDA #$10		;new xspeed
	STA !B6,x

DoDieing:
	RTS			;do nothing in dieing state

Interaction:
	JSL $01A7DC|!BankB	;mario interact
	BCC NO_CONTACT          ; (carry set = mario/rex contact)
	LDA $1490|!Base2        ; \ if mario star timer > 0 ...
	BNE HAS_STAR            ; /    ... goto HAS_STAR
	LDA !154C,x             ; \ if rex invincibility timer > 0 ...
	BNE NO_CONTACT          ; /    ... goto NO_CONTACT
	LDA $7D                 ; \  if mario's y speed < 10 ...
	CMP #$10                ;  }   ... rex will hurt mario
	BMI REX_WINS            ; /    

MARIO_WINS:
	JSR SUB_STOMP_PTS       ; give mario points
	JSL $01AA33|!BankB      ; set mario speed
	JSL $01AB99|!BankB      ; display contact graphic
	LDA $140D|!Base2        ; \  if mario is spin jumping...
	ORA $187A|!Base2        ;  }    ... or on yoshi ...
	BNE SPIN_KILL           ; /     ... goto SPIN_KILL

;set state to squished
	LDA #$02
	STA !1570,x		;new status
	LDA #!SquishTime
	STA !1602,x		;squish time
	RTS                     ; return 

REX_WINS:
	LDA $1497|!Base2        ; \ if mario is invincible...
	ORA $187A|!Base2        ;  }  ... or mario on yoshi...
	BNE NO_CONTACT          ; /   ... return
	%SubHorzPos()		; \  set new rex direction
	TYA                     ;  }  
	STA !157C,x             ; /
	JSL $00F5B7|!BankB	; hurt mario
	RTS

SPIN_KILL:
	LDA #$04                ; \ rex status = 4 (being killed by spin jump)
	STA !14C8,x             ; /   
	LDA #$1F                ; \ set spin jump animation timer
	STA !1540,x             ; /
	JSL $07FC3B|!BankB      ; show star animation
	LDA #$08                ; \ play sound effect
	STA $1DF9|!Base2        ; /

NO_CONTACT:
	RTS                     ; return

STAR_SOUNDS:        db $00,$13,$14,$15,$16,$17,$18,$19
KILLED_X_SPEED:     db $F0,$10

HAS_STAR:
	%Star()
	RTS                     ; final return
	
SUB_STOMP_PTS:      PHY                     ; 
                    LDA $1697|!Base2        ; \
                    CLC                     ;  } 
                    ADC !1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697|!Base2        ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y       ; \ play sound effect
                    STA $1DF9|!Base2        ; /   
NO_SOUND:           TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:           JSL $02ACE5|!BankB      ; give mario points
                    PLY                     ;
                    RTS                     ; return

;=====

SIZE:		db $02,$02,$00,$00,$00
		db $02,$02,$00,$00,$00
		db $02,$02,$00,$00,$00
		db $02,$02,$00,$00,$00
		db $02,$02,$00,$00,$00

PROP:		db $00,$40,$00,$00,$40
		db $00,$40,$00,$00,$40
		db $00,$40,$00,$00,$40
		db $00,$40,$00,$00,$40
		db $00,$40,$00,$00,$40

PALETTE:	db $FF,$FF,!StemProp,!StemProp,!StemProp	;FF: .cfg palette, else custom
		db $FF,$FF,!StemProp,!StemProp,!StemProp
		db $FF,$FF,!StemProp,!StemProp,!StemProp
		db $FF,$FF,!StemProp,!StemProp,!StemProp
		db $FF,$FF,!StemProp,!StemProp,!StemProp

TILEMAP:	db $E9,$E9,$DC,$DD,$DC		;walk frame one
		db $E9,$E9,$C4,$C5,$C4		;different feet
		db $A4,$A4,$DC,$DD,$DC		;walk frame two
		db $A4,$A4,$C4,$C5,$C4		;different feet
		db $A0,$EE,$C4,$C5,$C4		;squished

XDISP:		db $FC,$04,$FC,$04,$0C		;4px to the left
		db $FC,$04,$FC,$04,$0C
		db $FC,$04,$FC,$04,$0C
		db $FC,$04,$FC,$04,$0C
		db $FC,$04,$FC,$04,$0C

YDISP:		db $F8,$F8,$08,$08,$08		;-8px
		db $F8,$F8,$08,$08,$08		;-0px
		db $F8,$F8,$08,$08,$08
		db $F8,$F8,$08,$08,$08
		db $F8,$F8,$08,$08,$08		;-0px
	
GFX:
	%GetDrawInfo()
	LDA !15F6,x	;properties...
	STA $04

	LDA !1570,x	;test dying state
	CMP #$03
	BNE Notdying	;don't yflip if not dying

	LDA #$80
	TSB $04		;set the v bit

Notdying:
	LDA !1504,x	;load frame index
	ASL #2		;x4
	CLC
	ADC !1504,x	;x5
	STA $09		;and into frame index

	LDX #$00	;reset loop index
OAM_Loop:
	PHY			;preserve OAM index
	TYA
	LSR #2
	TAY
	LDA SIZE,x		;use loop index
	STA $0460|!Base2,y
	PLY

	PHX			;preserve loop index
	LDX $09			;load frame index

	LDA $00
	CLC
	ADC XDISP,x		;xpos (no direction applied)
	STA $0300|!Base2,y

	LDA $04			;test y flip
	BIT #$80
	BNE FlipY
	
	LDA $01
	CLC
	ADC YDISP,x		;ypos
	STA $0301|!Base2,y
	BRA DoCHR

FlipY:
	LDA $01
	SEC
	SBC YDISP,x
	STA $0301|!Base2,y
	LDA SIZE,x		;add 8 on 8x8 tiles
	BNE DoCHR

	LDA $0301|!Base2,y
	CLC
	ADC #$08		;add
	STA $0301|!Base2,y


DoCHR:
	LDA TILEMAP,x		;chr
	STA $0302|!Base2,y

	LDA #$00		;start with zero
	BIT PALETTE,x
	BMI NoCustomPal
	
	LDA PALETTE,x

NoCustomPal:
	ORA $04			;properties
	EOR PROP,x		;apply flip
	ORA $64
	STA $0303|!Base2,y

	INY			;next tile
	INY
	INY
	INY

	INC $09			;advance frame index
	PLX			;restore loop index

	INX			;advance loop
	CPX #$05		;5 tiles
	BNE OAM_Loop

	LDX $15E9|!Base2	;restore sprite index
	LDY #$FF		;$0460 has been written to
        LDA #$04		;5 tiles
        JSL $01B7B3|!BankB	;reserve
	RTS

SetupCust:
	STZ $00			;no X offset.
	LDA #$F0		;load Y offset
	STA $01			;store to scratch RAM.
	STZ $02			;no X speed.
	LDA #$A0		;load Y speed
	STA $03			;store to scratch RAM.
	LDA #!Projectile	;load sprite number to be spawned
	SEC			;set carry - spawn custom sprite
	%SpawnSprite()
	RTS