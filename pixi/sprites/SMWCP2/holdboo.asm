;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Boo (sprite 37), by imamelia
;;
;; This is a disassembly of sprite 37 in SMW, the Boo.
;;
;; Uses first extra bit: YES
;;
;; If the first extra bit is set, the Boo will keep following you even when you look away.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpeedMax:
db $08,$F8

IncTable:
db $01,$FF

Frame:
db $01,$02,$02,$01

Tilemap:
db $88,$8C,$A8,$8E,$AA,$AE,$8C,$88,$A8,$AE,$AC,$8C,$8E

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR BooMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BooMain:

LDA #$00
%SubOffScreen()

LDA #$10		;
STA $18B6|!Base2	; unknown RAM address?

LDA !14C8,x	;
CMP #$08		; if the sprite is not in normal status...
BNE SkipToGFX	;
LDA $9D		; or sprites are locked...
BEQ ContinueMain	; skip most of the main routine and just run the GFX routine
SkipToGFX:	;
JMP InteractGFX	;

ContinueMain:	;

%SubHorzPos()

LDA !1540,x	; if the timer is set...
BNE NoChangeState	;

LDA #$20			;
STA !1540,x		;
LDA !C2,x		; if the sprite state is zero...
BEQ NoCheckProximity	;
LDA $0E			;
CLC			;
ADC #$0A		;
CMP #$14			;
BCC Skip1		; skip the next part of code if the Boo is within a certain distance
NoCheckProximity:		;
LDA !7FAB10,x		;
AND #$04		; if the extra bit is set...
BNE NoChangeState		; make the Boo always follow the player
STZ !C2,x		;
CPY $76			; if the Boo is facing the player...
BNE NoChangeState		; don't make it follow him/her
INC !C2,x		;
NoChangeState:		;

LDA $0E			;
CLC			;
ADC #$0A		;
CMP #$14			;
BCC Skip1		;

LDA !15AC,x		; if the sprite is turning...
BNE Skip2			; skip the check and set
TYA			;
CMP !157C,x		;
BEQ Skip1			;
LDA #$1F			;
STA !15AC,x		; set the turn timer
BRA Skip2			;

Skip1:

STZ !1602,x		;
LDA !C2,x		;
BEQ Skip3			;
LDA #$03			;
STA !1602,x		;
;LDY $9E,x		; removed sprite number check
;CPY #$28		;
;BEQ Code01F948		;
LDA #$01			; this would have been 00
;CPY #$AF		;
;BEQ Code01F948		;
;INC A			;
;Code01F948:		;
AND $13			;
BNE Skip4			;
INC !1570,x		;
LDA !1570,x		;
BNE NoSetTimer2		;
LDA #$20			;
STA !1558,x		;
NoSetTimer2:		;
LDA !B6,x		; increment or decrement the sprite X speed
BEQ XSpdZero		; depending on whether it is positive, negative, or zero
BPL XSpdPlus		;
INC			;
INC			;
XSpdPlus:			;
DEC			;
XSpdZero:			;
STA !B6,x			;
LDA !AA,x		; same for the Y speed
BEQ YSpdZero		;
BPL YSpdPlus		;
INC			;
INC			;
YSpdPlus:			;
DEC			;
YSpdZero:			;
STA !AA,x		;

Skip4:
JMP UpdatePosition

Skip2:

CMP #$10		;
BNE NoFlipDir	;
PHA		;
LDA !157C,x	;
EOR #$01		; flip sprite direction
STA !157C,x	;
PLA		;
NoFlipDir:	;
LSR #3		;
TAY		;
LDA Frame,y	;
STA !1602,x	;

Skip3:

STZ !1570,x	;
LDA $13		;
AND #$07	; skip this every 8 frames
BNE UpdatePosition	;

%SubHorzPos()	;

LDA !B6,x	;
CMP SpeedMax,y	;
BEQ NoIncXSpeed	;
CLC		;
ADC IncTable,y	;
STA !B6,x		;
NoIncXSpeed:	;

LDA $D3		;
PHA		;
SEC		;
SBC $18B6|!Base2	;
STA $D3		;
LDA $D4		;
PHA		;
SBC #$00		;
STA $D4		;

JSR SubVertPos2	;

PLA		;
STA $D4		;
PLA		;
STA $D3		;

LDA !AA,x	;
CMP SpeedMax,y	;
BEQ UpdatePosition	;
CLC		;
ADC IncTable,y	;
STA !AA,x	;

UpdatePosition:

JSL $018022	;
JSL $01801A	;

InteractGFX:	;

LDA !14C8,x	;
CMP #$08		; if the sprite is not in normal status...
BNE NoInteraction	; don't interact with the player

JSL $01A7DC	;
BCC NoInteraction
LDA $1497
BNE NoInteraction

LDA $18BD
BEQ +
CMP #$03
BCS +

LDA #$40
STA $154C,x
LDA #$28
STA $1497

+
ORA $154C,x
BNE NoInteraction

LDA #$9F
STA $18BD
LDA #$FF
STA $154C,x

NoInteraction:	;

JSR BooGFX	;

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BooGFX:

;LDA $9E,x	;
;CMP #$37	;
;BNE NotBoo	;
LDA #$00		;
LDY !C2,x	;
BEQ SetFrame	;
LDA #$06		;
LDY !1558,x	;
BEQ SetFrame	;
TYA		;
AND #$04	;
LSR #2		;
ADC #$02	;
SetFrame:		;
STA !1602,x	;

JSR DrawBoo	; this was originally a JSL to $0190B2

RTS

;NotBoo:		;

DrawBoo:		;

%GetDrawInfo()	;

LDA !157C,x	;
STA $02		;

LDA !1602,x	;
TAX		;
LDA Tilemap,x	; set the sprite tilemap
STA $0302|!Base2,y	;

LDX $15E9|!Base2	;
LDA $00		;
STA $0300|!Base2,y	; no X displacement
LDA $01		;
STA $0301|!Base2,y	; no Y displacement

LDA !157C,x	;
LSR		; if the sprite is facing right...
LDA !15F6,x	;
BCS NoXFlip	; X-flip it
EOR #$40		;
NoXFlip:		;
ORA $64		;
STA $0303|!Base2,y	;

TYA		;
LSR #2		;
TAY		;

LDA #$02		;
ORA !15A0,x	;
STA $0460|!Base2,y	; set the tile size

PHK		;
PER $0006	;
PEA $8020	;
JML $01A3DF	; set up some stuff in OAM

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SubVertPos2:

LDY #$00
LDA $D3
SEC
SBC !D8,x
STA $0E
LDA $D4
SBC !14D4,x
BPL $01
INY
RTS