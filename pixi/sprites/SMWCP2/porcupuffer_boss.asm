;Fucking Annoying Fast Jumping Porcupuffer by Blind Devil
;guess what... had to redo this sprite because the original one sucked
;the code might be bigger but it's way less janky, trust me
;and by making it less janky it's much less unfair as well

!Accel = $01			;amount to accelerate every frame
!MaxSpd = $18			;max X speed for porcupuffer
!JumpXSpdTrg = $10		;if the sprite's X speed is lower than this, it won't jump at you
!JumpSpd = $C8			;Y speed for sprite when executing a jump (should be negative)

Tilemap:
db $86,$C0,$A6,$C2
db $86,$C0,$A6,$8A

;Actual sprite code starts below.

print "INIT ",pc
%SubHorzPos()
TYA
STA !157C,x

LDA #!MaxSpd
LSR

CPY #$00
BEQ +

EOR #$FF
INC

+
STA !B6,x
RTL
       
print "MAIN ",pc
PHB   
PHK   
PLB         
JSR SpriteCode
PLB   
RTL

AccTbl:
db !Accel,$100-!Accel

SpriteCode:
JSR Graphics		;call graphics drawing routine

LDA !14C8,x
CMP #$08
BNE Return
LDA $9D     
BNE Return

LDA #$03		;load offscreen handling option
%SubOffScreen()		;call offscreen handling routine

JSR UpdateXSpeed	;update X-pos based on speed and apply object interaction

LDA !C2,x		;load sprite state/behavior
JSL $0086DF|!BankB	;call 2-byte pointer subroutine
dw Follow		;$00 = follow the player
dw PreJump		;$01 = sink a bit
dw JUMP			;$02 = jumping into player
dw Surface		;$03 = sink a while, raise to surface

Follow:
%SubHorzPos()		;get player's horizontal distance relative to sprite
TYA			;transfer Y to A
STA !157C,x		;store to sprite direction. make it face the player.
TAY			;transfer to Y

LDA !B6,x		;load sprite's X speed
CPY #$00		;compare Y to value (check if sprite is facing right)
BNE GoingLeft		;if not, branch.

CMP #!MaxSpd		;compare speed to max positive value
BEQ NoAccel		;if higher, stop accelerating.
BRA +			;branch ahead

GoingLeft:
CMP.b #$100-!MaxSpd	;compare speed to max negative value
BEQ NoAccel		;if lower, stop accelerating.

+
LDA !B6,x		;load sprite's X speed
CLC			;clear carry
ADC AccTbl,y		;add acceleration value from table according to index
STA !B6,x		;store result back.

CMP #!JumpXSpdTrg		;compare speed to value (positive)
BCC Return			;if slower, don't trigger pre-jumping phase.
CMP.b #$100-!JumpXSpdTrg	;compare speed to value (negative)
BCS Return			;if slower, don't trigger pre-jumping phase.

NoAccel:
%SubVertPos()		;get player's vertical distance relative to sprite
CPY #$00		;compare Y to value
BEQ Return		;if equal, this means that the player is under the sprite, so don't trigger pre-jumping phase.

JSR Proximity		;check player/sprite proximity horizontally
BCC Return		;if not near enough, don't trigger pre-jumping phase.

STZ !AA,x		;reset sprite's Y speed.
INC !C2,x		;increment state by one

Return:
RTS			;return.

PreJump:
LDA !AA,x		;load sprite's Y speed
CMP #$18		;compare to value
BCC incY		;if lower, increase it.

LDA #!JumpSpd		;load speed value
STA !AA,x		;store to sprite's Y speed.

sharedchangestate:
INC !C2,x		;increment state by one
BRA UpdateYSpeed	;branch to update Y-pos of sprite

incY:
INC !AA,x		;increment sprite's Y speed by one

UpdateYSpeed:
JSL $01801A|!BankB	;update sprite's Y-pos based on speed (no gravity/object interaction)
RTS			;return.

JUMP:
LDA !157C,x		;load sprite direction
TAY			;transfer to Y

LDA $14			;load effective frame counter
AND #$0F		;preserve bits 0, 1, 2 and 3
BNE +			;if any bits are set, don't decrease X speed.

LDA !B6,x		;load sprite's X speed
SEC			;set carry
SBC AccTbl,y		;subtract acceleration value from table according to index
STA !B6,x		;store result back.

+
LDA !AA,x		;load sprite's Y speed
BPL .tchibum2		;if positive, increment sprite's Y speed and check whenever the sprite touches water/lava.
CMP #$20		;compare to value
BEQ .tchibum		;if equal, check whenever the sprite touches water/lava.

INC !AA,x		;increment sprite's Y speed by one
BRA UpdateYSpeed	;branch to update Y-pos of sprite

.tchibum2
INC !AA,x		;increment sprite's Y speed by one

.tchibum
LDA !164A,x		;load sprite in liquid flag
BEQ UpdateYSpeed	;if not in liquid, don't change state.

LDA #$08		;load value
STA !AA,x		;store to sprite's Y speed.
BRA sharedchangestate	;branch to change state

Surface:
LDA !AA,x		;load sprite's Y speed
CMP #$E8		;compare to value
BEQ .nomoreraise	;if equal, stop raising.

DEC !AA,x		;decrement Y speed by one

.nomoreraise
LDA !164A,x		;load sprite in liquid flag
BNE UpdateYSpeed	;if in liquid, don't change state.

STZ !C2,x		;reset sprite state
BRA UpdateYSpeed	;branch to update Y-pos of sprite

Proximity:
%SubHorzPos()		;get player's horizontal distance relative to sprite
LDA $0F			;load high byte of player's distance from sprite
XBA			;flip high/low bytes of A
LDA $0E			;load low byte of player's distance from sprite
REP #$20		;16-bit A
LDY !B6,x		;load sprite's X speed into Y
BPL .right		;if positive, branch.

CMP #$FFB0		;compare to value
BCS .canjump		;if higher, the sprite can jump.
BRA .cant		;else, it can't, so clear carry.

.right
CMP #$0050		;compare to value
BCC .canjump		;if lower, the sprite can jump.

.cant
SEP #$20		;8-bit A
CLC			;clear carry
RTS			;return.

.canjump
SEP #$20		;8-bit A
SEC			;set carry
RTS			;return.

UpdateXSpeed:
JSL $018022|!BankB	;update sprite's X-pos based on speed (no gravity/object interaction)
JSL $019138|!BankB	;force sprite interaction with objects

JSL $01A7DC|!BankB	;process default interaction with player
RTS			;return.

;Graphics routine below.

XDisp:
db $F8,$08,$F8,$08
db $08,$F8,$08,$F8

YDisp:
db $F8,$F8,$08,$08

Properties:
db $40,$00

Graphics:
%GetDrawInfo()
LDA $14
CLC
ADC $15E9|!Base2
AND #$04
STA $03
LDA !157C,x
STA $02

LDA !15F6,x
STA $05

PHX
LDX #$03

GFXLoop:
LDA $01
CLC
ADC YDisp,x
STA $0301|!Base2,y
PHX
LDA $02
BNE FaceRight

TXA
ORA #$04
TAX

FaceRight:
LDA $00
CLC
ADC XDisp,x
STA $0300|!Base2,y
PHX
LDX $02
LDA Properties,x
ORA $05
ORA $64
STA $0303|!Base2,y
PLX
TXA
EOR $03
TAX
LDA Tilemap,x  
STA $0302|!Base2,y
PLX
INY #4
DEX
BPL GFXLoop
PLX

LDY #$02
LDA #$03
JSL $01B7B3|!BankB
RTS