;(PIXI conversion by Blind Devil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 回転リフト / 一時回転リフト
; 
; SMB3の回転リフト
; ExtraBitsで回転方向を設定
; ExtraPropertyByte1に左右の粒の数-1を設定
; ExtraPropertyByte2でリフトの属性を設定
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Uses first extra bit: YES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!ClockSpeed =	$18
		!CounterClock =	$E8
		!GetSin =	$07F7DB|!BankB
		!GetCos =	!GetSin

		print "INIT ",pc
InitCode:	STZ !1510,x
		;LDA #$20
		;STA !151C,x
		LDA !E4,x
		ORA #$04
		STA !E4,x
		LDA !D8,x
		ORA #$04
		STA !D8,x
		LDA !7FAB28,x
		INC A
		ASL A
		STA !187B,x
		LDA !7FAB34,x
		BEQ .L10
		DEC !C2,x
		LDY #$38
		LDA !7FAB10,x
		AND #$04
		BEQ .L11
		LDY #$C8
.L11		TYA
		STA !151C,x
		RTL

.L10		LDY #!ClockSpeed
		LDA !7FAB10,x
		AND #$04
		BEQ .L00
		LDY #!CounterClock
.L00		TYA
		STA !1504,x
		RTL

		print "MAIN ",pc
		PHB
		PHK
		PLB
		JSR MainCode
		PLB
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Table0:		db $1C,$14,$08,$00,$08,$14
Timer0:		db $3C,$02,$02,$40,$02,$02

MainCode:
		LDA #$03
		%SubOffScreen()

	%GetDrawInfo()		; 画面外判定で2回RTSしない設計
		LDA $9D
		BNE Start
		LDA !7FAB34,x
		BEQ .L80
		LDA !1540,x
		BNE .L80
		LDA !C2,x
		INC A
		CMP #$06
		BNE .L81
		TDC
.L81		STA !C2,x
		TAY
		LDA !7FAB10,x
		AND #$04
		CMP #$04
		LDA Table0,y
		BCC .L82
		EOR #$FF
		INC A
.L82		STA !1504,x
		LDA Timer0,y
		STA !1540,x
.L80
		LDY #$00
		LDA !1504,x
		BPL .IfPlus
		DEY
.IfPlus		CLC
		ADC !151C,x
		STA !151C,x
		TYA
		ADC !1528,x
		AND #$01
		STA !1528,x
Start:		LDA !1528,x
		XBA
		LDA !151C,x
		REP #$31
		STA $00
		ADC.w #$0080
		AND.w #$01FF
		TAX
		AND.w #$00FF
		ASL A
		CPX.w #$0100
		TAX
		LDA.l !GetCos,x
		BCC .IfPlus0
		EOR.w #$FFFF
		INC A
		BEQ .IfPlus0
.IfPlus0	ASL #3
		STA $02
		STA $9A
		LDA $00
		TAX
		AND.w #$00FF
		ASL A
		CPX.w #$0100
		TAX
		LDA.l !GetSin,x
		BCC .IfPlus1
		EOR.w #$FFFF
		INC A
.IfPlus1	ASL #3
		STA $00
		STA $98
		SEP #$10
		LDX $15E9|!Base2
		TDC
		LDA !187B,x
		LSR A
		DEC A
		TAY
		STY $0D
		TDC
.Loop0		CLC
		ADC $00
		DEY
		BPL .Loop0
		LDY #$00
		EOR.w #$FFFF
		INC A
		BPL .IfPlus2
		DEY
.IfPlus2	STA $04
		STY $06
		LDY $0D
		TDC
.Loop1		CLC
		ADC $02
		DEY
		BPL .Loop1
		LDY #$00
		EOR.w #$FFFF
		INC A
		BPL .IfPlus3
		DEY
.IfPlus3	STA $07
		STY $09
		SEP #$20
		LDA !E4,x
		STA $0A
		LDA !14E0,x
		STA $0B
		LDA !D8,x
		STA $0C
		LDA !14D4,x
		STA $0D
		LDY !187B,x
		REP #$20
		BRA .InitLoop
.Loop2		LDX #$00
		LDA $04
		CLC
		ADC $00
		STA $04
		BPL .L10
		DEX
.L10		STX $06
		LDX #$00
		LDA $07
		CLC
		ADC $02
		STA $07
		BPL .L11
		DEX
.L11		STX $09
.InitLoop	LDA $0A
		CLC
		ADC $08
		PHA
		LDA $0C
		CLC
		ADC $05
		PHA
		DEY
		BPL .Loop2
		SEP #$20
		LDX $15E9|!Base2
		LDA !187B,x
		STA $0D
		JSR SubGFX

		LDA !187B,x
		STA $0D

		LDA $71
		ORA $9D
		ORA $13F9|!Base2
		BEQ .Normal
		JMP PullPosData

.Normal		LDA $0D
		LSR A
		DEC A
		TAY
		STY $0E
		LDA !1534,x		;\
		STA $4D			; | $4D:8sinθ{m-1}
		LDA !1570,x		; |
		STA $4E			;/
		LDA !157C,x		;\
		STA $4F			; | $4F:8cosθ{m-1}
		LDA !1594,x		; |
		STA $50			;/
		REP #$20
		LDA $98			;
		SEC			;
		SBC $4D			;
		STA $4D			;
.Loop4		CLC			;
		ADC $4D			;
		DEY			;
		BNE .Loop4		;
		STA $51			;
		LDA $9A			;
		SEC			;
		SBC $4F			;
		STA $4F			;
		LDY $0E			;
.Loop5		CLC			;
		ADC $4F			;
		DEY			;
		BNE .Loop5		;
		STA $53			;
		SEP #$20

		STZ $0E
.Loop3		LDA !1504,x
		BEQ .L20
		LDA $0D
		EOR $14
		LSR A
		BCC .Next0
.L20		LDA $94
		SEC
		SBC $03,s
		CLC
		ADC #$18
		CMP #$30
		BCS .Next0
		LDA $96
		ADC #$30
		SEC
		SBC $01,s
		CMP #$60
		BCS .Next0
		PLA
		PLY
		ADC #$01
		STA $05
		BCC .L00
		INY
		CLC
.L00		STY $0B
		PLA
		PLY
		ADC #$01
		STA $04
		BCC .L01
		INY
.L01		STY $0A
		LDA #$06
		STA $06
		STA $07
		JSL $03B664|!BankB
		JSL $03B72B|!BankB
		BCC .Next1
		JSR SetMarioSpeed
		BRA .Next1
.Next0		PLA
		PLA
		PLA
		PLA
.Next1		REP #$20
		LDA $51
		SEC
		SBC $4D
		STA $51
		LDA $53
		SEC
		SBC $4F
		STA $53
		SEP #$20
		DEC $0D
		BPL .Loop3
L50:		LDA $98
		STA !1534,x
		LDA $99
		STA !1570,x
		LDA $9A
		STA !157C,x
		LDA $9B
		STA !1594,x
		RTS

PullPosData:	PLA
		PLA
		PLA
		PLA
		DEC $0D
		BPL PullPosData
		BRA L50

SetMarioSpeed:	LDA !1504,x
		BEQ .Stop
.Rolling	LDA !187B,x
		LSR A
		CMP $0D
		BEQ .Return
		LDA $52
		ASL #2
		STA $00
		ASL A
		ADC $00
		STA $7D
		LDA $54
		ASL #2
		STA $00
		ASL A
		ADC $00
		BPL .IfPlus0
		CMP #$C1
		BCS .Store
		LDA #$C1
		BRA .Store
.IfPlus0	CMP #$3F
		BCC .Store
		LDA #$3F
.Store		STA $7B
.Return		RTS

.Stop		LDA $05
		SEC
		SBC $1C
		STA $00
		LDA $80
		CLC
		ADC #$18
		CMP $00
		BPL .L00
		LDA $7D
		BMI .Return
		LDA $77
		AND #$08
		BNE .Return
		LDA #$10
		STA $7D
		LDA #$03
		STA $1471|!Base2
		LDA #$1F
		LDY $187A|!Base2
		BEQ .NoYoshi
		LDA #$2F
.NoYoshi	STA $01
		LDA $05
		SEC
		SBC $01
		STA $96
		LDA $0B
		SBC #$00
		STA $97
		LDA $0E
		BNE .Return
		INC $0E
		LDA $7B
		SEC
		SBC !1510,x
		STA $7B
		STZ !1510,x
		;LDA $FFFFFF
		LDA $9B
		ASL A
		REP #$20
		LDA $98
		BCC .IfPlus
		EOR.w #$FFFF
		INC A
.IfPlus		SEP #$20
		XBA
		STA $00
		CLC
		ADC $7B
		STA $7B
		LDA $00
		ASL $00
		ROR A
		STA !1510,x
		;JSL $00CFC3|!BankB
		;LDX $15E9|!Base2
		RTS
.L00		LDA $190F,x
		LSR A
		BCS .Return
		TDC
		LDY $73
		BNE .Ducking
		LDY $19
		BNE .PowerUp
.Ducking	LDA #$10
.PowerUp	LDY $187A|!Base2
		BEQ .NoYoshi0
		ADC #$08
.NoYoshi0	CLC
		ADC $80
		CMP $00
		BCC .Return0
		LDA $7D
		BPL .Return0
		LDA #$10
		STA $7D
		LDA #$01
		STA $1DF9|!Base2
.Return0	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SubGFX:		LDA !15C4,x
		BNE .Return
		LDA.b #$03|(!Base2>>8)
		XBA
		LDA $0D
		ASL A
		ASL A
		ORA #$03
		ADC !15EA,x
		REP #$30
		TSX
		STX $010E|!Base2
		TCS
		SEP #$20
		LDY.w #$283D
.Loop		PHY
		LDA $03,x
		INX #2
		SEC
		SBC $1C
		PHA
		LDA $03,x
		INX #2
		SEC
		SBC $1A
		PHA
		DEC $0D
		BPL .Loop
		LDX $010E|!Base2
		TXS
		SEP #$10
		LDX $15E9|!Base2
		LDA !187B,x
		LSR A
		ASL A
		ASL A
		ADC !15EA,x
		TAY
		LDA #$2A
		STA $0303|!Base2,y
		;STA $0307,y
		LDY #$00
		LDA !187B,x
		JSL $01B7B3|!BankB
.Return		RTS