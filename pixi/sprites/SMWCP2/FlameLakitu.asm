;; Flame LAKITU (and projectile)
;; Aims at player and throws a FIREBALL.
;; FIREBALL spawns 2 fireballs that melt a specified tile.

!TILE	= $40	; type of tile to eat through (projectile, page 1 only)

	PRINT "INIT ",pc
	RTL

	PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR MAIN
	PLB
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN sprite routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ACCEL:	db $01,$FF
MAX_Y:	db $10,$F0
MAX_X:	db $20,$D0
RETURN:	RTS
MAIN:	JSR SUB_OFF_SCREEN_X0
	LDA $1510,x
	JSL $0086DF
	dw LAKITU&$FFFF
	dw FIRE1&$FFFF
	dw FIRE2&$FFFF
MARDEFL:	db $20,$E0
LAKITU:	LDA $167A,x
	ORA #$04
	STA $167A,x
	JSR SUB_GFX
	LDA $9D
	BNE RETURN
	LDA #$06
	STA $1662,x
	JSL $01A7DC
	BCC NOCON1
	JSR SUB_VERT_POS
	LDA $0E
	CMP #$E6
	BPL HURTMAR
	LDA $7D
	BMI NOCON1
	LDA #$01
	CMP $1602,x
	BEQ HURTMAR
	JSL $01AA33
	JSL $01AB99
	LDA $1558,x
	BNE NOSTUN
	LDA #$50
	STA $1558,x
	LDA #$28
	STA $1DFC
NOSTUN:	LDA #$02
	STA $1DF9
	JSR SUB_HORZ_POS
	TYA
	LDA MARDEFL,y
	STA $7B
	BRA NOCON1
HURTMAR:	JSL $00F5B7
NOCON1:	JSL $01801A
	JSL $018022
	LDA $14
	AND #$01
	BNE NOFLIP
	JSR SUB_HORZ_POS
	TYA
	STA $157C,x
	LDA $B6,x
	CMP MAX_X,y
	BEQ HITMAX
	CLC
	ADC ACCEL,y
	STA $B6,x
HITMAX:	LDA $1534,x
	AND #$01
	TAY
	LDA $AA,x
	CLC
	ADC ACCEL,y
	STA $AA,x
	CMP MAX_Y,y
	BNE NOFLIP
	INC $1534,x
NOFLIP:	LDA $1540,x
	BNE TIMERSET
	LDA #$FF
	STA $1540,x
TIMERSET:
	CMP #$70
	BEQ SPAWN
	BCS NORMFRM
	CMP #$0C
	BEQ THROW
	BCC TOSSED
	LDA #$01
	STA $1602,x
	RTS
NORMFRM:	LDA $1558,x
	BEQ SHOWNOR
	LDA #$02
	STA $1602,x
	STZ $1540,x
	RTS
SHOWNOR:	STZ $1602,x
	RTS
THROW:	LDA #$27
	STA $1DFC
	JSL GEN_SPRITE
	LDA $E4,x
	STA $00E4,y
	LDA $14E0,x
	STA $14E0,y
	LDA $D8,x
	SEC
	SBC #$0A
	STA $00D8,y
	LDA $14D4,x
	SBC #$00
	STA $14D4,y
	JSL SET_SPRITE
	LDA #$01
	STA $1510,y
	RTS
SPAWN:	LDA #$03
	STA $1DFC
	JSR SUB_IS_OFF_SCREEN
	BNE RETURN2
	JSR SMOKE
	LDA $D8,x
	SEC
	SBC #$07
	STA $17C4,y
	LDA $E4,x
	STA $17C8,y
	RTS
TOSSED:	LDA #$02
	STA $1602,x
RETURN2:	RTS

CLOUD_X:	db $FC,$04,$FE,$02,$FB,$05,$FD,$03
	db $FA,$06,$FC,$04,$FB,$05,$FD,$03
CLOUD_Y:	db $00,$FF,$03,$04,$FF,$FE,$04,$03
	db $FE,$FF,$03,$03,$FF,$00,$03,$03
	db $F8,$FC,$00,$04
LAKITU_FACE:
	db $EC,$EE,$A6,$EE,$CE,$EE
FIREBALL:
	db $E8,$EA,$E8,$EA
FIREPROP:
	db $05,$05,$C5,$C5
SUB_GFX:	%GetDrawInfo()

	LDA $157C,x
	STA $02

	LDA $14
	LSR : LSR
	AND #$0C
	STA $03

	LDA $1602,x
	ASL
	STA $04

	LDA $14
	LSR
	AND #$03
	STA $05

	LDA $00
	CLC
	ADC #$04
	STA $0300,y

	LDA $01
	CLC
	ADC #$16
	STA $0301,y

	LDA #$4D
	STA $0302,y

	LDA #$09
	ORA $64
	STA $0303,y

	PHX
	TYA
	LSR
	LSR
	PHY
	TAY
	LDA #$00
	STA $0460,y
	PLY
	PLX

	INY
	INY
	INY
	INY

	PHX
	LDX #$03
CLOUDLOOP:
	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA $00
	CLC
	ADC CLOUD_X,x
	STA $0300,y

	LDA $01
	CLC
	ADC CLOUD_Y,x
	ADC #$10
	STA $0301,y

	LDA #$60
	STA $0302,y

	LDA $64
	STA $0303,y

	TYA
	LSR
	LSR
	PHY
	TAY
	LDA #$02
	STA $0460,y
	PLY
	PLX



	INY
	INY
	INY
	INY
	DEX
	BPL CLOUDLOOP
	PLX


	LDA $1602,x
	CMP #$01
	BNE NORMAL

	PHX
	LDA $00
	STA $0300,y

	LDA $01
	SEC
	SBC #$0A
	STA $0301,y

	LDX $05
	LDA FIREBALL,x
	STA $0302,y

	LDA FIREPROP,x
	ORA $64
	STA $0303,y

	TYA
	LSR
	LSR
	PHY
	TAY
	LDA #$02
	STA $0460,y
	PLY
	PLX

	INY
	INY
	INY
	INY

NORMAL:	PHX
	LDA $00
	STA $0300,y

	LDA $01
	STA $0301,y

	LDA #$09
	LDX $02
	BNE NO_FLIP
	ORA #$40
NO_FLIP:	ORA $64
	STA $0303,y

	LDX $04
	LDA LAKITU_FACE,x
	STA $0302,y

	TYA
	LSR
	LSR
	PHY
	TAY
	LDA #$02
	STA $0460,y
	PLY
	PLX

	LDA #$01
	CMP $1602,x
	BNE SIXTI
	LDY #$FF
	LDA #$06
	BRA OAMWRIT
SIXTI:	LDY #$FF
	LDA #$05
OAMWRIT:	JSL $01B7B3
RETURN_GFX:
	RTS
SPEED:	db $30,$D0
FIRE1:	JSR FIREBALLGFX
	LDA $9D
	BNE RETURN_GFX
	LDA $C2,x
	BNE NOCHASE
	LDA #$20
	JSR SUB_AIMING
	LDX $15E9
	LDA $00
	STA $AA,x
	LDA $01
	STA $B6,x
	INC $C2,x
NOCHASE:	JSL $01801A
	JSL $018022
	JSL $019138
	LDA $1588,x
	BEQ NOOBJ
	LDA #$04
	STA $14C8,x
	LDA #$1F
	STA $1540,x
	LDA #$17
	STA $1DFC
	JSR SPWNSPR
	LDA #$50
	STA $00B6,y
	JSR SPWNSPR
	LDA #$B0
	STA $00B6,y
	RTS
SPWNSPR:	JSL GEN_SPRITE
	LDA $E4,x
	STA $00E4,y
	LDA $14E0,x
	STA $14E0,y
	LDA $D8,x
	DEC A
	STA $00D8,y
	LDA $14D4,x
	STA $14D4,y
	JSL SET_SPRITE
	LDA #$02
	STA $1510,y
	LDA $D8,x
	STA $1570,y
	LDA $14D4,x
	STA $157C,y
NOOBJ:	RTS
BLOCK_X:	db $10,$00
FIRE2:	JSR FIREBALLGFX
	LDA $1570,x
	STA $D8,x
	LDA $157C,x
	STA $14D4,x
	LDA $9D
	BNE NOOBJ
	JSL $01802A
	STZ $AA,x
	LDA $1588,x
	AND #$03
	BEQ NOOBJ2
	LDA #$01
	STA $1DF9
	LDA $18A7
	CMP #!TILE
	BNE NOCANNON
	LDA $D8,x
	STA $98
	LDA $14D4,x
	STA $99
	LDA $B6,x
	LSR : LSR : LSR : LSR : LSR : LSR : LSR
	AND #$01
	TAY
	LDA $E4,x
	CLC
	ADC BLOCK_X,y
	STA $9A
	LDA $14E0,x
	ADC #$00
	STA $9B
	LDA #$02
	STA $9C
	JSL $00BEB0
	JSR SUB_IS_OFF_SCREEN
	BNE NOCANNON
	JSR SMOKE
	LDA $98
	STA $17C4,y
	LDA $9A
	STA $17C8,y
	RTS
NOCANNON:
	LDA #$04
	STA $14C8,x
	LDA #$1F
	STA $1540,x
NOOBJ2:	RTS
FIREBALLGFX:
	JSL $01A7DC
	BCC NOCON2
	JSL $00F5B7
NOCON2:	JSL $0190B2
	LDY $15EA,x
	PHX
	LDA $14
	LSR
	AND #$03
	TAX
	LDA FIREBALL,x
	STA $0302,y

	PHX
	LDX $15E9

	LDA $15F6,x

	PLX
	ORA FIREPROP,x
	ORA $64
	STA $0303,y
	PLX
	RTS
GEN_SPRITE:
	JSL $02A9DE
	BMI NO_SPRITE
	LDA #$08
	STA $14C8,y
	LDA $7FAB9E,x
	PHX
	TYX
	STA $7FAB9E,x
	PLX
NO_SPRITE:
	RTL
SET_SPRITE:
	PHX
	TYX
	JSL $07F7D2
	JSL $0187A7
	LDA #$08
	STA $7FAB10,x
	PLX
	RTL
SMOKE:	LDY #$03
FIND_FREE:
	LDA $17C0,y
	BEQ FOUND_ONE
	DEY
	BPL FIND_FREE
	RTS
FOUND_ONE:
	LDA #$01
	STA $17C0,y
	LDA #$14
	STA $17CC,y
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; aiming routine
; input: accumulator should be set to total SPEED (x+y)
; output: $00 = y SPEED, $01 = x SPEED
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_AIMING:		STA $01
			PHX					;\ preserve sprite indexes of Magikoopa and magic
			PHY					;/
			JSR SUB_VERT_POS			; $0E = vertical distance to Mario
			STY $02					; $02 = vertical direction to Mario
			LDA $0E					;\ $0C = vertical distance to Mario, positive
			BPL CODE_01BF7C				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
CODE_01BF7C:		STA $0C					;/
			JSR SUB_HORZ_POS			; $0F = horizontal distance to Mario
			STY $03					; $03 = horizontal direction to Mario
			LDA $0E					;\ $0D = horizontal distance to Mario, positive
			BPL CODE_01BF8C				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
CODE_01BF8C:		STA $0D					;/
			LDY #$00
			LDA $0D					;\ if vertical distance less than horizontal distance,
			CMP $0C					; |
			BCS CODE_01BF9F				;/ branch
			INY					; set y register
			PHA					;\ switch $0C and $0D
			LDA $0C					; |
			STA $0D					; |
			PLA					; |
			STA $0C					;/
CODE_01BF9F:		LDA #$00				;\ zero out $00 and $0B
			STA $0B					; | ...what's wrong with STZ?
			STA $00					;/
			LDX $01					;\ divide $0C by $0D?
CODE_01BFA7:		LDA $0B					; |\ if $0C + loop counter is less than $0D,
			CLC					; | |
			ADC $0C					; | |
			CMP $0D					; | |
			BCC CODE_01BFB4				; |/ branch
			SBC $0D					; | else, subtract $0D
			INC $00					; | and increase $00
CODE_01BFB4:		STA $0B					; |
			DEX					; |\ if still cycles left to run,
			BNE CODE_01BFA7				;/ / go to start of loop
			TYA					;\ if $0C and $0D was not switched,
			BEQ CODE_01BFC6				;/ branch
			LDA $00					;\ else, switch $00 and $01
			PHA					; |
			LDA $01					; |
			STA $00					; |
			PLA					; |
			STA $01					;/
CODE_01BFC6:		LDA $00					;\ if horizontal distance was inverted,
			LDY $02					; | invert $00
			BEQ CODE_01BFD3				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
			STA $00					;/
CODE_01BFD3:		LDA $01					;\ if vertical distance was inverted,
			LDY $03					; | invert $01
			BEQ CODE_01BFE0				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
			STA $01					;/
CODE_01BFE0:		PLY					;\ retrieve Magikoopa and magic sprite indexes
			PLX					;/
			RTS					; RETURN



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, RETURN
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; RETURN

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / RETURN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00				;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
					LDA $94					;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
					SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
					SBC $E4,x				;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
					STA $0E					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
					LDA $95					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
					SBC $14E0,x				;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
					BPL SPR_L16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
					INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
SPR_L16:				RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_VERT_POS
; This routine determines if Mario is above or below the sprite.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B829
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_VERT_POS:		LDY #$00               ;A:25A1 X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizCHC:0130 VC:085 00 FL:924
					LDA $96                ;A:25A1 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdiZCHC:0146 VC:085 00 FL:924
					SEC                    ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0170 VC:085 00 FL:924
					SBC $D8,x              ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0184 VC:085 00 FL:924
					STA $0E                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0214 VC:085 00 FL:924
					LDA $97                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0238 VC:085 00 FL:924
					SBC $14D4,x            ;A:2501 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizcHC:0262 VC:085 00 FL:924
					BPL SPR_L11            ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0294 VC:085 00 FL:924
					INY                    ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0310 VC:085 00 FL:924
SPR_L11:				RTS                    ;A:25FF X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizcHC:0324 VC:085 00 FL:924
