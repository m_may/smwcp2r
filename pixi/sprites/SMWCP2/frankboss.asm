;Frank (World 8 boss, fire/ice world), by Blind Devil - yes I had to recode it from scratch. wonderful.

!ExtendedFireball = $02
!WallProjectile = $BD		;also water puddle for ice death animation
!CeilingRocks = $25
!Frostee = $09

WalkSpeeds:
db $1C,$E4,$28,$D8	;left/right (normal), left/right (3 hp or less)

Tilemaps:
db $00,$02
db $20,$22	;idle frame 1

db $04,$06
db $24,$26	;idle frame 2

db $80,$82
db $CC,$A2	;turning (idle to walk/fireball/wall spawn transition)

db $08,$0A
db $28,$2A	;walk frame 1/wall launch 1

db $44,$46
db $64,$66	;walk frame 2/wall launch 2

db $08,$0A
db $2C,$2E	;walk frame 3

db $44,$46
db $64,$66	;walk frame 4 (same as 2)

db $4C,$4E
db $6C,$6E	;jumping up

db $E6,$CE
db $A4,$A6	;pounding

db $48,$4A
db $68,$6A	;fireball spawning

db $C0,$C2
db $E0,$E2	;hurt
!HandTile = $D4	;hand tile for hurt frame

!DizzyTile = $EE	;animated dizzy effect for hurt frame (flips horizontally)
!DizzyXDisp = $FE
!DizzyYDisp = $DC

db $40,$42
db $60,$62	;final hurt

;pose cheatsheet:
;idle: 00, 01
;turn: 02
;walk: 03, 04, 05, 06
;jump: 07
;fall: 08
;fireball: 09
;hurt: 0A
;dead: 0B

XDisp:
db $08		;extra for facing right
db $F8,$08
db $F8,$08

YDisp:
db $F0,$F0
db $00,$00

Properties:
db $40,$00

OnOffPal:
db $0F,$0D	;fire, ice (plus use SP3/SP4)

print "INIT ",pc
STZ $0F5E|!Base2	;reset key on screen index.
STZ $0F5F|!Base2	;reset timer for key.

LDA #$01		;load value
STA !157C,x		;store to sprite direction (face left).
LDA #$40		;load amount of frames
STA !1540,x		;store to action timer.

JSL $01ACF9|!BankB	;generate random number
ADC $148E|!Base2	;add more randomness
ADC $13			;add more frame randomness
STA !1510,x		;store to fireball/wall attack alternator.

LDA #$05		;load amount of hp
STA !1528,x		;store it.
RTL			;return.

print "MAIN ",pc
PHB			;preserve data bank
PHK			;push program bank
PLB			;pull it as new data bank
JSR SpriteCode		;call actual sprite code
PLB			;restore previous data bank
RTL			;return.

SpriteCode:
JSR Graphics		;call graphics drawing routine

LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE Return		;if not equal, return.
LDA $9D			;load sprites/animations locked flag
BNE Return		;if set, return.

JSR PlayerInteraction	;call custom player interaction routine
JSR HurtSprInteraction	;call sprite/sprite interaction routine
JSR HandleOrb		;handle orb on screen, make it vanish when due and stuff
JSR HandleEnemySprites	;handle fire/ice enemy despawning

LDA !C2,x		;load action state pointer
CMP #$08		;compare to value
BCC +			;if lower, keep it untouched.
LDA #$00		;load value
STA !C2,x		;store to action state pointer (not STZ because we need 00 in A).

+
ASL			;multiply by 2
TAX			;transfer to X (remember to restore it later on)
JMP (States,x)		;jump to pointer according to index

States:
dw Idle			;$00 - stand still
dw Walking		;$01 - walk, and change state when a wall is touched
dw FireWall		;$02 - shoot fireballs or spawn rock walls
dw Idle			;$03 - stand still... again
dw Walking		;$04 - walk... again
dw Pounding		;$05 - jump, ground pound (spawn sprites from ceiling on earthquake), and spawn baddies + orb when it's over
dw Hurt			;$06 - ugh, my head
dw Die			;$07 - MELTDOWN... or shatter

Return:
RTS			;return.

Idle:
LDX $15E9|!Base2	;load index of current sprite being processed into X
STZ !B6,x		;reset sprite's X speed.

Idling:
LDA !1540,x		;load action timer
BEQ .incstate		;if zero, increment state.
CMP #$0C		;compare to value
BCC .turning		;if lower, use turning pose

LDA $14			;load effective frame counter
LSR #3			;divide by 2 three times
AND #$01		;preserve bit 0 only

.storepose
STA !151C,x		;store to pose index.
JMP updpos		;update sprite positions

.turning
LDA #$02		;load pose value
BRA .storepose		;branch to store it

.incstate
INC !C2,x		;increment action value by one
JMP updpos		;update sprite positions

Walking:
LDX $15E9|!Base2	;load index of current sprite being processed into X

LDA $14			;load effective frame counter
LSR #3			;divide by 2 three times
AND #$03		;preserve bits 0 and 1
CLC			;clear carry
ADC #$03		;add value
STA !151C,x		;store to pose index.

LDA !1588,x		;load sprite blocked status flags
AND #$03		;check if blocked on sides (touching walls)
BNE .changestate	;if yes, swap direction, set timer for next state and enable it

LDA !157C,x		;load sprite direction
LDY !1528,x		;load sprite hp into Y
CPY #$03		;compare Y to value
BCS +			;if higher, use regular speeds.
ORA #$02		;set bit 1 (use faster speeds)
+
TAY			;transfer A to Y
LDA WalkSpeeds,y	;load speed from table according to index
STA !B6,x		;store to sprite's X speed.
JMP updpos		;update sprite positions

.changestate
LDA !157C,x		;load sprite direction
EOR #$01		;flip bit 0
STA !157C,x		;store result back.

LDA #$02		;load pose value
STA !151C,x		;store to pose index.
LDA #$C0		;load amount of frames
STA !1540,x		;store to action timer.
INC !C2,x		;increment action value by one
STZ !1FD6,x		;reset projectile speed/earthquake phase index.
JMP updpos		;update sprite positions

FireWall:
LDX $15E9|!Base2	;load index of current sprite being processed into X
STZ !B6,x		;reset sprite's X speed.

LDA !1540,x		;load action timer
BNE +			;if not zero, don't change action.

LDA #$40		;load amount of frames
STA !1540,x		;store to action timer.
INC !1510,x		;increment fireball/wall attack flag by one
INC !C2,x		;increment action value by one
JMP updpos		;update sprite positions

+
LDA !1510,x		;load attack type flag
AND #$01		;preserve bit 0 only
BEQ FireballAtk		;if not set, do fireball attack.

LDA !1540,x		;load action timer
CMP #$B8		;compare to value
BEQ .setwalltimer	;if equal, set the wall spawning action timer.
CMP #$78		;compare again
BEQ .setwalltimer2	;if equal, set the wall spawning action timer.
CMP #$30		;compare again
BCC .dontset		;if lower, just change state.

LDA !15AC,x		;load timer for wall spawning action
BNE .itswalltime	;if not zero, handle wall action.

JMP Idling		;else keep idling.

.setwalltimer2
LDA !1528,x		;load boss hp
CMP #$04		;compare to value
BCS .dontset		;if higher or equal, don't set the timer to spawn a second wall.

.setwalltimer
LDA #$10		;load amount of frames
STA !15AC,x		;store to timer of wall spawning action.
JMP updpos		;update sprite positions

.dontset
INC !1510,x		;increment fireball/wall attack flag by one
INC !C2,x		;increment action value by one
JMP updpos		;update sprite positions

.itswalltime
CMP #$01		;compare to value
BEQ .SpawnWall		;if equal, spawn wall sprite.

LSR #2			;divide by 2 twice
CLC			;clear carry
ADC #$03		;add value
STA !151C,x		;store to pose index.
JMP updpos		;update sprite positions

.SpawnWall
LDA #!WallProjectile	;load sprite number to spawn
SEC			;set carry - spawn custom sprite
JSR SpawnSpr		;set offsets and speeds and spawn sprite
CPY #$FF		;compare Y to value
BEQ .spawnfailed	;if spawn has failed somehow, don't play SFX.

LDA #$10		;load SFX value
STA $1DF9|!Base2	;store to address to play it.

.spawnfailed
JMP updpos		;update sprite positions

T_SPEEDX:
db $40,$40,$38,$30,$2C,$28

T_SPEEDY:
db $04,$02,$00,$FE,$01,$F3

FireballAtk:
LDA !1540,x		;load action timer
PHA			;preserve into stack

CMP #$BC		;compare to value
BCS .noshootpose	;if higher, don't use shooting pose yet.

LDA #$09		;load pose value
STA !151C,x		;store to pose index.

.noshootpose
PLA			;restore action timer
CLC			;clear carry
ADC #$08		;add value
AND #$1F		;preserve bits 0-4
BNE .nofire		;if any are set, don't fire.

LDY !1FD6,x		;load projectile speed index into X
LDA T_SPEEDX,y		;load X speed from table according to index
STA $02			;store to scratch RAM.
LDA T_SPEEDY,y		;load Y speed from table according to index
STA $03			;store to scratch RAM.
LDA #$0B		;load X offset
STA $00			;store to scratch RAM.
LDA #$FD		;load Y offset
STA $01			;store to scratch RAM.
LDA #!ExtendedFireball	;load fireball number
%SpawnExtended()	;spawn extended sprite

INC !1FD6,x		;increment speed index for fireballs

CPY #$FF		;compare Y to value, check if sprite was spawned
BEQ .nofire		;if equal, don't play SFX.

LDA #$06		;load SFX value
STA $1DFC|!Base2	;store to address to play it.

.nofire
JMP updpos		;update sprite positions

Pounding:
LDX $15E9|!Base2	;load index of current sprite being processed into X
STZ !B6,x		;reset sprite's X speed.
LDA !1FD6,x		;load earthquake phase index
BEQ .settimer		;if zero, set timer to jump.
DEC			;decrement A
BEQ .jump		;if #$01, wait for timer to reach zero and make sprite jump
DEC			;decrement A
BNE +			;if not equal #$02, branch.
JMP .onair		;if #$02, handle air pose and when the sprite touches the ground, do earthquake

+
LDA $1887|!Base2	;load shake ground timer
BNE +			;if not zero, don't revert boss action.

JSR SpawnOrbAndEnemies	;spawn orb and enemies

LDA #$08		;load amount of frames
STA !1540,x		;store to action timer.
STZ !C2,x		;reset action pointer.
JMP updpos		;update sprite positions

+
INC !1540,x		;increment state timer so the idle state never increments !C2,x (we'll set it to #$01 when $1887 is zero instead)

LDA $14			;load effective frame counter
AND #$0F		;preserve bits 0-2
BNE .nospawn		;if any are set, don't spawn anything.

STZ $02			;no X speed.
STZ $03			;no Y speed.
LDA #!CeilingRocks	;load sprite number to spawn (hint: fallingstuff)
SEC			;set carry - spawn custom sprite
%SpawnSprite()		;spawn sprite

LDA #$08		;load value
STA !14C8,y		;store to spawned sprite status (override init).
LDA #$01		;load value
STA !C2,y		;store to spawned sprite address (make fallingstuff act as a falling sprite, and not as a spawner)
LDA #$03		;load palette/properties for spawned sprite
STA !15F6,y		;store result back.

JSL $01ACF9|!BankB	;generate random number
ADC $148E|!Base2	;add more randomness
ADC $13			;add frame randomness
CMP #$10		;compare to value
BCC +			;if lower, branch.

CMP #$E0		;compare to value
BCC ++			;if lower, skip ahead.
SEC			;set carry
SBC #$20		;subtract value
BRA ++			;branch ahead

+
CLC			;clear carry
ADC #$10		;add value

++
STA !E4,y		;store to spawned sprite's X-pos, low byte.
LDA #$C0		;load value
STA !D8,y		;store to spawned sprite's Y-pos, low byte.
LDA #$00		;load value (there's no STZ for Y indexed stuff)
STA !14E0,y		;store to sprite's X-pos, high byte.
STA !14D4,y		;store to sprite's Y-pos, high byte.

.nospawn
JMP Idling		;execute idle state in the meantime

.settimer
LDA #$08		;load amount of frames
STA !1540,x		;store to action timer.
LDA #$01		;load pose value
STA !151C,x		;store to pose index.
INC !1FD6,x		;increment phase of state

JMP updpos		;update sprite positions

.jump
LDA !1540,x		;load action timer
BNE .nojumpyet		;if not zero, don't jump yet.

INC !1FD6,x		;increment phase of state
LDA #$A0		;load value
STA !AA,x		;store to sprite's Y speed.
LDA #$07		;load pose value
STA !151C,x		;store to pose index.

.nojumpyet
JMP updpos		;update sprite positions

.onair
LDA !AA,x		;load sprite's Y speed
BMI +			;if negative, don't change pose.

LDA #$08		;load pose value
STA !151C,x		;store to pose index.

+
LDA !1588,x		;load sprite blocked flags
AND #$04		;check if blocked below (touching ground)
BEQ +			;if not touching, don't do earthquake yet.

STZ !AA,x		;reset sprite's Y speed.
INC !1FD6,x		;increment phase of state
LDA #$09		;load SFX value
STA $1DFC|!Base2	;store to address to play it.

LDA $72			;load player is in the air flag
BNE .nostun		;if in the air, don't stun the player.
LDA #$30		;load value
STA $18BD|!Base2	;store to player's stun timer.

.nostun
LDA #$FF		;load value
STA $1887|!Base2	;store to shake ground timer.
STA !1540,x		;store to action timer as well.

+
JMP updpos		;update sprite positions

Hurt:
LDX $15E9|!Base2	;load index of current sprite being processed into X
STZ !B6,x		;reset sprite's X speed.

LDA #$4F		;load amount of frames
STA !154C,x		;store to sprite flashing invulnerability timer.

LDA #$0A		;load value
STA !151C,x		;store to pose index.

LDA !1540,x		;load action timer
BNE +			;if not equal zero, skip ahead.

LDA !1528,x		;load boss hp
BEQ ItsADeath		;if zero, it's a death. rip frank.

LDA #$04		;load value

LDY !157C,x		;load sprite direction into Y
BEQ .runright		;if equal zero, keep A untouched.

LDA #$01		;load value

.runright
STA !C2,x		;store to action pointer.

+
JMP updpos		;update sprite positions

ItsADeath:
STZ !154C,x		;reset sprite flashing invulnerability timer
LDA #$FF		;load amount of frames
STA !1540,x		;store to action timer.
INC !C2,x		;increment action pointer by one.

LDA $14AF|!Base2	;load on/off status
BEQ +			;if on (fire), return.

LDA #!WallProjectile	;load sprite number to spawn
SEC			;set carry - spawn custom sprite
JSR SpawnSpr		;set offsets and speeds and spawn sprite
PHX			;preserve sprite index
TYX			;transfer Y to X
LDA #$08		;load value
STA !14C8,x		;store to spawned sprite status.
LDA !7FAB10,x		;load extra bits
ORA #$04		;check first extra bit
STA !7FAB10,x		;store result back.
PLX			;restore sprite index

+
RTS			;return.

Die:
LDX $15E9|!Base2	;load index of current sprite being processed into X
STZ !B6,x		;no X speed

LDA #$0B		;load value
STA !151C,x		;store to pose index.

LDA $14AF|!Base2	;load on/off status
BEQ .firedeath		;if on (fire), flash and explode.

LDA !1540,x		;load action timer
CMP #$B0		;compare to value
BEQ EndLevel		;if equal, end the level.

LDA #$04		;load value
STA !AA,x		;store to sprite's X speed.
JSL $01801A|!BankB	;update Y position based on speed (no gravity/object interaction)
RTS			;return.

.firedeath
LDA !1540,x		;load action timer
CMP #$C0		;compare to value
BNE updpos		;if not equal, do not explode.

JSR SpawnSpr		;set offsets, speeds, and spawn sprite (what sprite? doesn't matter, will explode anyway)
PHX
TYX

LDA #$15
STA $1887|!Base2	; shake the ground
LDA #$0D		;/
STA !9E,x		;\ Sprite = Bob-omb.
LDA #$08		;/
STA !14C8,x		;\ Set status for new sprite.
JSL $07F7D2|!BankB	;/ Reset sprite tables (sprite becomes bob-omb)..
LDA #$01		;\ .. and flag explosion status.
STA !1534,x		;/
LDA #$40		;\ Time for explosion.
STA !1540,x
LDA #$09		;\
STA $1DFC|!Base2	;/ Sound effect.
LDA #$1B
STA !167A,x
PLX

EndLevel:
STZ !1540,x		;reset action timer.
LDA #$0B		;load action value
STA $1696|!Base2	;store to screen barrier flag (play boss victory song and set normal exit).
RTS			;return.

updpos:
JSL $01802A|!BankB	;update positions based on speed (with gravity and object interaction)
RTS			;return.

SpawnOrbAndEnemies:
LDA $14AF|!Base2	;load on/off status
BEQ .fire		;if on, branch to spawn flame.

LDA #!Frostee		;load sprite number (frostee - ice)
SEC			;set carry - spawn custom sprite
BRA .dospawn		;branch to spawn the sprite

.fire
LDA #$1D		;load sprite number (hopping flame - fire)
CLC			;clear carry - spawn normal sprite
JSR SpawnSpr		;set offsets, speeds, and spawn sprite

PHX			;preserve sprite index
TYX			;transfer Y to X
LDA #$00		;load value
STA !7FAB9E,x		;dem workarounds - prevent hopping flame from being treated as a frostee and die on fire state
PLX			;restore sprite index
BRA .keepgoing		;branch ahead

.dospawn
JSR SpawnSpr		;set offsets, speeds, and spawn sprite

.keepgoing
LDA #$08		;load value
STA !14C8,y		;store to spawned sprite status (something seems to conspire against the poor hopping flame sprite...)

LDA $0F5E|!Base2	;load key slot holder
BEQ +			;if zero, spawn sprite.
RTS			;return.

+
LDA #$80		;load sprite number (key, a.k.a. the orb in disguise)
CLC			;clear carry - spawn normal sprite
JSR SpawnSpr		;set offsets, speeds, and spawn sprite

PHX			;preserve sprite index
LDX $14AF|!Base2	;load on/off status into X
LDA #$09		;load value
STA !14C8,y		;store to spawned sprite status.
LDA !15F6,y		;load palette/properties of spawned sprite
ORA OnOffPal,x		;set palette/gfx page accordingly
STA !15F6,y		;store result back.
TYA			;transfer Y to A
INC			;increment A by one
STA $0F5E|!Base2	;store to key slot holder.
LDA #$D0		;load value
STA $0F5F|!Base2	;store to orb timer.
PLX			;restore sprite index
RTS			;return.

SpawnSpr:
STZ $00			;no X offset.
STZ $01			;no Y offset.
STZ $02			;no X speed.
STZ $03			;no Y speed.
%SpawnSprite()		;spawn sprite
RTS			;return.

PlayerInteraction:
LDA !154C,x		;load timer to disable sprite interaction
ORA $1490|!Base2	;OR with star power timer
ORA $1497|!Base2	;OR with flashing invulnerability timer
BNE NoPlayerContact	;if any are set, return.

LDA !1528,x		;load boss hp
BEQ NoPlayerContact	;if zero, return.

JSL $01A7DC|!BankB	;call player/sprite interaction routine
BCC NoPlayerContact	;if there's no contact, return.

LDA $14AF|!Base2	;load on/off status
BEQ hesonfire		;if on, branch.

LDA $18BD|!Base2	;load player stun timer
BNE +			;if not zero, don't freeze the player.

LDA #$40		;load value
STA $18BD|!Base2	;store to player stun timer.
LDA #$10		;load SFX value
STA $1DF9|!Base2	;store to address to play it.
RTS			;return.

+
CMP #$10		;compare to value
BCS +			;if higher, return.

LDA #$40		;load value
STA $1497|!Base2	;store to player's flashing invulnerability timer.

+
RTS			;return.

hesonfire:
JSL $00F5B7|!BankB	;plain hurt the player (I don't like it but the original didn't care for spinjumps or stuff so welp)

NoPlayerContact:
RTS			;return.

HurtSprInteraction:
LDA $0F5E|!Base2	;load orb slot holder
BEQ .noorb		;if zero, there's no orb, so return.
DEC			;decrement A by one (now we have the real slot number)
TAY			;transfer to Y
LDA !15F6,y		;load palette/properties of orb
AND #$0E		;only the palette bits matter for us
CMP #$0E		;compare to value (check if it uses palette F, the fire palette)
BEQ .fireorb		;if it's a fire orb, branch.

LDA $14AF|!Base2	;load on/off status
BEQ .checkcontact	;if on (fire, means the boss is on fire state as well), check for contact.
RTS			;return.

.fireorb
LDA $14AF|!Base2	;load on/off status
BEQ .noorb		;if on (fire, means the boss is on fire state as well), return.

.checkcontact
PHX			;preserve sprite index
TYX			;transfer Y to X
JSL $03B6E5|!BankB	;get sprite clipping A
PLX			;restore sprite index
JSL $03B69F|!BankB	;get sprite clipping B
JSL $03B72B|!BankB	;check for contact
BCC .noorb		;if there's no contact, return.

JSR KillOrb		;kill the orb
LDA #$28		;load SFX value
STA $1DFC|!Base2	;store to address to play it.
DEC !1528,x		;decrement hp by one
STZ !AA,x		;reset sprite's Y speed.

LDA #$06		;load value
STA !C2,x		;store to action pointer.
LDA #$50		;load amount of frames
STA !1540,x		;store to action timer.
STZ $1887|!Base2	;reset layer 1 shake timer.

.noorb
RTS			;return.

HandleEnemySprites:
PHX			;preserve sprite index
LDX #$09		;loop count

.eraseembers
LDA $170B|!Base2,x	;load extended sprite number
CMP #$03		;check if small hopping flame ember
BNE +			;if not, redo loop.

STZ $170B|!Base2,x	;erase extended sprite.

+
DEX			;decrement X by one
BPL .eraseembers	;loop while it's positive.

LDA $14AF|!Base2	;load on/off status
BEQ .KillIce		;if on, kill frostee.

LDX #!SprSize-1		;loop count

.loop1
LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE .redoloop1		;if not, redo the loop.

LDA !9E,x		;load sprite number
CMP #$1D		;check if it's a hopping flame
BNE .redoloop1		;if not, redo the loop.

TXY			;transfer X to Y
JSR KillSprite		;kill the sprite

.redoloop1
DEX			;decrement X by one
BPL .loop1		;loop while it's positive.
PLX			;restore sprite index
RTS			;return.

.KillIce
LDX #!SprSize-1		;loop count

.loop2
LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE .redoloop2		;if not, redo the loop.

LDA !7FAB9E,x		;load custom sprite number
CMP #!Frostee		;check if it's a frostee
BNE .redoloop2		;if not, redo the loop.

TXY			;transfer X to Y
JSR KillSprite		;kill the sprite

.redoloop2
DEX			;decrement X by one
BPL .loop2		;loop while it's positive.
PLX			;restore sprite index
RTS			;return.

HandleOrb:
LDA $0F5E|!Base2	;load orb sprite slot plus one
BEQ .NoOrb		;if there's no orb, return.
DEC			;decrement A by one (now we have the real slot number)
TAY			;transfer to Y
LDA !15F6,y		;load palette/properties of sprite
AND #$0E		;only the palette bits matter for us
CMP #$0E		;compare to value (check if it uses palette F, the fire palette)
BEQ .UsingFire		;if yes, branch.

LDA $14AF|!Base2	;load on/off status
BEQ .Decrement		;if on (fire), decrement timer. orb is ice, level is fire, so erase it.
RTS			;return.

.UsingFire
LDA $14AF|!Base2	;load on/off status
BEQ .NoOrb		;if on (fire), don't decrement timer. orb is fire, level is fire, so keep it.

.Decrement
LDA $0F5F|!Base2	;load orb timer
BEQ KillOrb		;if zero, despawn it.
DEC $0F5F|!Base2	;decrement orb timer by one

.NoOrb
RTS			;return.

KillOrb:
STZ $0F5E|!Base2	;reset orb slot holder.

KillSprite:
LDA #$1B		;load value
STA !1540,y		;store to spin kill smoke animation timer.
LDA #$04		;load value
STA !14C8,y		;store to sprite status (spinkilled).
RTS			;return.

HurtHandXDisp:
db $E8,$18

!HurtHandYDisp = $F8

Graphics:
LDA !154C,x		;load timer to disable sprite interaction
AND #$01		;preserve bit 0 only
BEQ +			;if not set, draw graphics normally (sprite flashes after being hit).
RTS			;return.

+
%GetDrawInfo()		;get free OAM slot and sprite offsets within the screen

LDA !157C,x		;load sprite direction
STA $02			;store to scratch RAM.
STZ $03			;reset amount of tiles drawn.

LDA $14			;load effective frame counter
LSR #2			;divide by 2 twice
AND #$01		;preserve bit 0 only
STA $06			;store to scratch RAM.

LDA !151C,x		;load pose index
ASL #2			;multiply by 2 twice
STA $04			;store to scratch RAM.
CMP #$28		;compare to value
BNE +			;if not equal to hurt pose index, skip drawing the hand.

JSR DrawHanDizzy	;draw the hand and the dizzy effect over the boss head

+
PHX			;preserve sprite index

LDX $14AF|!Base2	;load on/off status into X
LDA OnOffPal,x		;load palette from table according to index
STA $05			;store to scratch RAM.
CPX #$01		;compare X to value
BEQ .skipflashing	;if equal, on/off status is off (ice), so don't do palette flash.

LDX $15E9|!Base2	;load index of current sprite being processed into X
LDA !1528,x		;load boss hp
BNE .skipflashing	;if not zero, don't do palette flash.

LDA !C2,x		;load sprite action pointer
CMP #$07		;compare to value (check if dying)
BNE .skipflashing	;if not zero, don't do palette flash.

LDX $06			;load animating index from scratch RAM
LDA OnOffPal,x		;load palette from table according to index
STA $05			;store to scratch RAM.

.skipflashing
LDX #$03		;loop count

GFXLoop:
LDA $01			;load Y-pos of sprite within the screen
CLC			;clear carry
ADC YDisp,x		;add Y displacement from table according to index
STA $0301|!Base2,y	;store to OAM.

PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $02			;add sprite direction
TAX			;transfer A to X
LDA $00			;load X-pos of sprite within the screen
CLC			;clear carry
ADC XDisp,x		;add X displacement from table according to index
STA $0300|!Base2,y	;store to OAM.
PLX			;restore loop count

PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $04			;add pose index value
TAX			;transfer A to X
LDA Tilemaps,x		;load tile number from table according to index
STA $0302|!Base2,y	;store to OAM.

LDX $15E9|!Base2	;load index of current sprite being processed into X
LDA !15F6,x		;load palette/properties from CFG
AND #$F1		;preserve all bits except palette ones
ORA $05			;set palette from scratch RAM
LDX $02			;load sprite direction into X
ORA Properties,x	;set X-flip accordingly
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

PLX			;restore loop count
INY #4			;increment Y four times
INC $03			;increment number of tiles drawn
DEX			;decrement X by one
BPL GFXLoop		;loop while it's positive.

PLX			;restore sprite index
LDY #$02		;load default size for tiles drawn (all 16x16)
LDA $03			;load number of tiles drawn
DEC			;decrement by one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.

DrawHanDizzy:
PHX			;preserve sprite index

LDX $02			;load sprite direction into X
LDA $00			;load X-pos of sprite within the screen
CLC			;clear carry
ADC HurtHandXDisp,x	;add X displacement from table according to index
STA $0300|!Base2,y	;store to OAM.

LDA $01			;load Y-pos of sprite within the screen
CLC			;clear carry
ADC #!HurtHandYDisp	;add Y displacement value
STA $0301|!Base2,y	;store to OAM.

LDA #!HandTile		;load hand tile number
STA $0302|!Base2,y	;store to OAM.

LDX $15E9|!Base2	;load index of current sprite being processed into X
LDA !15F6,x		;load palette/properties from CFG
AND #$F1		;preserve all bits except palette ones
LDX $14AF|!Base2	;load on/off flag into X
ORA OnOffPal,x		;set palette accordingly
LDX $02			;load sprite direction into X
ORA Properties,x	;set X-flip accordingly
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

INY #4			;increment Y four times
INC $03			;increment number of tiles drawn

LDA $00			;load X-pos of sprite within the screen
CLC			;clear carry
ADC #!DizzyXDisp	;add X displacement value
STA $0300|!Base2,y	;store to OAM.

LDA $01			;load Y-pos of sprite within the screen
CLC			;clear carry
ADC #!DizzyYDisp	;add Y displacement value
STA $0301|!Base2,y	;store to OAM.

LDA #!DizzyTile		;load dizzy effect tile number
STA $0302|!Base2,y	;store to OAM.

LDX $06			;load animating index from scratch RAM into X
LDA #$0F		;load palette/gfx page bits
ORA Properties,x	;set X-flip accordingly
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

PLX			;restore sprite index
INY #4			;increment Y four times
INC $03			;increment number of tiles drawn
RTS			;return.