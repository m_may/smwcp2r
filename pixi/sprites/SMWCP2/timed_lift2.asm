;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Timed Lift 2, adapted by Davros.
;;
;; Description: A timed lift that will fall down once it runs out of time.
;;
;; Uses first extra bit: YES
;; If the extra bit is set, it will go to the left.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra Byte 1: time before sprite falls down after activated
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !RIGHT_X_SPEED = $10
                    !LEFT_X_SPEED = $F0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    LDA !7FAB40,x	    ; \ time until the platform falls down (ranges from 1 to 4 only)
		    STA !1570,x             ; /
                    RTL                     ; RETURN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc                                    
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    JSR SPRITE_CODE_START   ;  |
                    PLB                     ;  |
                    RTL                     ; /

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SPRITE_CODE_START:  JSR SUB_GFX             ; graphics routine
		    LDA $9D                 ; \ if sprites locked, RETURN
		    BNE RETURN              ; /

		    LDA #$03
		    %SubOffScreen()

		    LDA $13                 ; \ if the animated frame is zero...
		    AND #$00                ;  |
		    BNE CALC_FRAME          ; /
		    LDA !C2,x               ; \ if the sprite state is set
		    BEQ CALC_FRAME          ; /
		    LDA !1570,x             ; \ calculate which frame to show
		    BEQ CALC_FRAME          ; /
		    DEC !1570,x             ; decrease number of frames
CALC_FRAME:	    LDA !1570,x             ; \ calculate which frame to show
		    BEQ UPDATE              ; /

		    JSL $018022|!BankB      ; UPDATE speed position without y speed

		    STA !1528,x             ; prevent Mario from sliding horizontally
		    JSL $01B44F             ; interact with sprites
		    BCC RETURN              ; RETURN if no contact
                
                    LDA !7FAB10,x        ; \ set to the left if the extra bits are set...
	            AND #$04             ;  |
	            BNE LEFT_SPEED       ; /

RIGHT_SPEED:        LDA #!RIGHT_X_SPEED      ; \ set right x speed
                    BRA STORE_X_SPEED       ; /

LEFT_SPEED:         LDA #!LEFT_X_SPEED       ; \ set left x speed
STORE_X_SPEED:	    STA !B6,x               ; / 
		    STA !C2,x               ; store sprite state
RETURN:		    RTS                     ; RETURN
                    
UPDATE:	            JSL $01802A|!BankB      ; UPDATE position based on speed values
		    LDA $1491|!Base2        ; amount to move Mario                    
		    STA !1528,x             ; prevent Mario from sliding horizontally
		    JSL $01B44F|!BankB      ; interact with sprites
		    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


TILEMAP:	    db $C4,$C4
TILE_SIZE:	    db $02,$02,$00
PROPERTIES:	    db $0B,$4B,$03
HORZ_DISP:	    db $00,$10,$0C
VERT_DISP:	    db $00,$00,$04
NUM_TILES:	    db $B6,$B5,$B4,$B3


SUB_GFX:		%GetDrawInfo()      ; after: Y = index to sprite tile map ($300)
                                            ; $00 = sprite x position relative to screen boarder 
                                            ; $01 = sprite y position relative to screen boarder
		    LDA !1570,x             ; \ calculate the frames for the TILEMAP
		    PHX                     ;  |
		    PHA                     ;  |
		    LSR #6                  ;  |
		    TAX                     ; /
		    LDA NUM_TILES,x         ; \ store TILEMAP for the numbers
		    STA $02                 ; /

		    LDX #$02
		    PLA
		    CMP #$08
		    BCS LOOP_START
		    DEX

LOOP_START:	    LDA $00                 ; \ tile x position = sprite x location ($00) + tile displacement
		    CLC                     ;  |
		    ADC HORZ_DISP,x         ;  |
		    STA $0300|!Base2,y             ; /

		    LDA $01                 ; \ tile y position = sprite x location ($01) + tile displacement
		    CLC                     ;  |
		    ADC VERT_DISP,x         ;  |
		    STA $0301|!Base2,y             ; /

		    LDA TILEMAP,x           ; \ store TILEMAP
		    CPX #$02                ;  |
		    BNE STORE_TILE          ;  |
		    LDA $02                 ;  |
STORE_TILE:	    STA $0302|!Base2,y             ; /

                    ;PHX                     ;
                    ;LDX $15E9|!Base2      ;
                    ;LDA !15F6,x
                    ;PLX                     ;
                    LDA PROPERTIES,x
		    ORA $64                 ; add in tile priority of level
		    STA $0303|!Base2,y             ; store tile PROPERTIES

                    PHY                     ; set tile to be 8x8 or 16x16
                    TYA                     ;
                    LSR A                   ;
                    LSR A                   ;
                    TAY                     ;
                    LDA TILE_SIZE,x         ; 
                    STA $0460|!Base2,y      ;
                    PLY                     ;

                    INY                     ; \ increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 1 16x16 tile...
                    INY                     ;  |    ...sprite OAM is 8x8...
                    INY                     ;  |    ...so increment 4 times
                    DEX                     ;  | go to next tile of frame and loop
                    BPL LOOP_START          ; /

                    PLX                     ; pull, X = sprite index
                    LDY #$FF                ; \ we've already set 460 so use FF
                    LDA #$02                ;  | A = number of tiles drawn - 1
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; RETURN