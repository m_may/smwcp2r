;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!State = $1534
!StateTimer = $163E

                    print "INIT ",pc
					STZ !State,x
					LDA #$50
					STA !StateTimer,x
					LDA #$E0
					STA $AA,x
					STZ $B6,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITE_ROUTINE:	  	
JSR SUB_GFX					;Draw the graphics

					LDA $9D
					BNE return
					LDA !State,x
					BEQ .Rising
					.Exploding
						JSL $01A7DC
						
						LDA !StateTimer,x
						bne +
						stz $14C8,x
						+
						RTS
					.Rising
						LDA !StateTimer,x
						BNE +
						JSR DrawSmoke
						lda #$20
						sta !StateTimer,x
						inc !State,x
						LDA #$27
						STA $1DF9
						+
						;JSL $018022
						JSL $01801A
					return:
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Tiles:
db $C4,$C5

SUB_GFX:            %GetDrawInfo()     	;Get all the drawing info, duh
					LDA $157C,x
					STA $05
                    PHX
					LDA $14
					LSR
					LSR
					AND #$01
					TAX
					
					
					LDA $00 : clc : adc #$04
					STA $0300,y
					
					LDA $01 : clc : adc #$04
					STA $0301,y
					
					LDA Tiles,x
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					;ORA #$30
					PHX
					LDX $05
					BNE +
					ORA #$40
					+
					PLX
					STA $0303,y
					INY
					INY
					INY
					INY
					PLX
					LDA #$00
					LDY #$00					;|they were all 16x16 tiles
					JSL $01B7B3					;/and then draw em
					EndIt:
					RTS

DrawSmoke:
STZ $00
STZ $01
LDA #$20
STA $02
LDA #$02
%SpawnSmoke()
RTS