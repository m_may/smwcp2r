;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FOR USE WITH DESERT BOSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!PopOutTimer = !15AC  ; MUST BE THE SAME ADDRESS AS IN desertboss.asm

			!SandBlockTile = $8E

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR GraphicsRoutine
			JSR SpriteCode
			PLB
			print "INIT ",pc
			RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
			LDA !14C8,x
			CMP #$08
			BNE .Return

			LDA $9D
			BNE .Return

			LDA !PopOutTimer,x
			BNE .PopOut

			JSL $01802A|!bank
			JSL $01A7DC|!bank  ; interaction with player

			LDA !1588,x
			AND #$04
			BEQ .Return

			LDA #$01
			STA $1DF9|!addr

			LDA #$20
			STA !1540,x
			LDA #$04
			STA !14C8,x

.Return
			RTS

.PopOut
			JSL $01801A|!bank  ; update y position
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GraphicsRoutine:
			%GetDrawInfo()

			LDA $00
			STA $0300|!addr,y
			LDA $01
			STA $0301|!addr,y

			LDA #!SandBlockTile
			STA $0302|!addr,y

			LDA !15F6,x
			STA $0303|!addr,y

			LDY #$02
			LDA #$00
			JSL $01B7B3|!bank
			RTS