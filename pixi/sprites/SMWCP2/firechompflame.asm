;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		print "MAIN ",pc
		PHB
		PHK				
		PLB				
		JSR SPRITE_ROUTINE	
		PLB
		print "INIT ",pc
		RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITE_ROUTINE:
	JSR SUB_GFX
	LDA !14C8,x
	EOR #$08
       ORA $9D
	BNE RETURN
       %SubOffScreen()	;   only process sprite while on screen

	JSL $018022|!BankB   ;   Update X position without gravity
       JSL $01801A|!BankB   ;   Update Y position without gravity
	JSL $018032|!BankB	;   interact with other sprites

	LDA $187A|!Base2	;\  if mario is on Yoshi, return because
	BNE RETURN		;/  losing Yoshi is calculated elsewhere
	JSL $01A7DC|!BankB   ;\  skip to end if
	BCC RETURN		;/  no interaction
	LDA $1490|!Base2	;\  branch if mario
	BNE HASSTAR		;/  has the star
	JSL $00F5B7|!BankB   ;   hurt mario
RETURN:
	RTS

HASSTAR:
	LDA #$02             ;\  kill
	STA !14C8,x          ;/  sprite
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!GFXTMP1 = $02

FLAMETOP:	db $0C,$0D
FLAMEBOTTOM:	db $1C,$1D

SUB_GFX:		%GetDrawInfo()
		LDA $15E9		; \
		ASL A			;  | get
		CLC			;  | flame
		ADC $14			;  | frame
		LSR A			;  | index
		AND #%00000001		;  |
		STA !GFXTMP1		; /

		LDA $00			; \
		CLC			;  | top
		ADC #$04		;  | X
		STA $0300,y		; /
		LDA $01			; \ top
		STA $0301,y		; / Y
		PHY			; \
		LDY !GFXTMP1		;  | get
		LDA FLAMETOP,y		;  | tile #
		PLY			; /
		STA $0302,y		; set tile #
		LDA $15F6,x		; get sprite palette info
		ORA $64                 ; add in the priority bits from the level settings
		STA $0303,y             ; set properties
		INY			; \
		INY			;  | next OAM
		INY			;  | index
		INY			; /
		LDA $00			; \
		CLC			;  | bottom
		ADC #$04		;  | X
		STA $0300,y		; /
		LDA $01			; \
		CLC			;  | bottom
		ADC #$08		;  | Y
		STA $0301,y		; /
		PHY			; \
		LDY !GFXTMP1		;  | get
		LDA FLAMEBOTTOM,y	;  | tile #
		PLY			; /
		STA $0302,y		; set tile #
		LDA $15F6,x		; get sprite palette info
		ORA $64                 ; add in the priority bits from the level settings
		STA $0303,y             ; set properties
		INY			; \
		INY			;  | next OAM
		INY			;  | index
		INY			; /
		LDY #$00		; #$00 means the tiles are 8x8
		LDA #$01		; This means we drew two tiles
		JSL $01B7B3		; don't draw if offscreen, set tile sizes
		RTS