;Gau/Nyololin (SML) by smkdan (optimized by Blind Devil)
;GFX from andy_k_250

;I lifted some of the code from the Rex since it's stomping behaviour can be applied here

!Projectile = $66			;<<< set this to the same slot number as smlfire.cfg!

;Tilemap defines (in case you want to use Nyololin's graphics, change the define below to 1,
;as well as the palette in CFG editor to green):
!UseNyololin = 0

if !UseNyololin == 0	;<<< do NOT change this, it's not a define
TILEMAP:		db $A8,$A9,$C8,$C9,$CA	;mouth cloused
			db $AB,$AC,$CB,$CC,$CD	;mouth open
			db $E4,$E5,$E4,$E4,$E4	;dead
else
TILEMAP:	db $D0,$D1,$F0,$F1,$F2	;top, top 2, bottom, bottom 2, bottom 3 (mouth closed) - Nyololin (snake)
		db $C3,$C4,$F0,$F1,$F2	;(mouth open)
		db $E3,$E4,$E3,$E3,$E3	;(squished)
endif

;fire interval
!FireInterval = $40

;fire sound
!FireSFX = $20
!FirePort = $1DFC

!SquishTime = $40	;time for Gau to stay squished before falling off

;some ram
;1570:	state
;	00: living and idling (mouth closed)
;	01: living and firing (mouth open)
;	02: squished
;	03: dying (falling offscreen)
;1602:	general timer for above states

print "INIT ",pc
	%SubHorzPos()		;face mario
	TYA
	STA !157C,x		;new direction

	STZ !1570,x
	LDA #!FireInterval	;set firing interval
	STA !1602,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

Return_I:
	RTS

Run:
	LDA #$00
	%SubOffScreen()
	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE Return_I           
	LDA $9D			;locked sprites?
	BNE Return_I
	LDA !15D0,x		;yoshi eating?
	BNE Return_I

	JSL $01802A|!BankB	;update speed

;figure out what to do according to state
	LDA !1570,x		;load state
	BEQ DoLiveIdle		;00
	DEC A
	BEQ DoLiveFire		;01
	DEC A
	BEQ DoSquish		;02
	DEC A
	BEQ Dodying		;03
	RTS

DoLiveIdle:
	DEC !1602,x		;decrement timer
	BEQ EnterLiveFire
	JMP Interaction

EnterLiveFire:
	LDA #$01		;set fire status
	STA !1570,x
	LDA #$20		;fixed mouth open time
	STA !1602,x		;into timer
	LDA #!FireSFX		;fire sound effect
	STA !FirePort|!Base2	;sound effect

;generate projectile

GenCust:
	JSL $02A9E4|!BankB	;grab free sprite slot
	BMI Interaction		;return if none left before anything else...

	JSR SetupCust		;run sprite generation routine

	JMP Interaction		;just jump to interaction routine

DoLiveFire:
	DEC !1602,x		;decrement
	BEQ EnterLiveIdle
	JMP Interaction

EnterLiveIdle:
	LDA #$00		;set idle status
	STA !1570,x
	LDA #!FireInterval	;time between firing timer
	STA !1602,x
	JMP Interaction	

DoSquish:
	DEC !1602,x		;decrement
	BEQ Enterdying
	LDA !167A,x		;load fourth tweaker byte
	ORA #$02		;set bit to make it immune to cape/fire/bricks
	STA !167A,x		;store result back.
	LDA !166E,x		;load third tweaker byte
	ORA #$20		;set bit to make it not interact to cape spins (immune =/= interactable)
	STA !166E,x		;store result back.
	LDA !1686,x		;load fifth tweaker byte
	ORA #$01		;set bit to make it inedible by Yoshi
	STA !1686,x		;store result back.
	RTS			;don't interact

Enterdying:
	LDA #$03		;enter dying status
	STA !1570,x
	LDA !1686,x		;load 1686 tweaker byte
	ORA #$80		;don't interact wiht objects
	STA !1686,x

	LDA #$E0		;rise a bit with new yspeed
	STA !AA,x
	LDA #$10		;new xspeed
	STA !B6,x

Dodying:
	RTS			;do nothing in dying state

Interaction:
	JSL $01A7DC|!BankB	;mario interact
	BCC NO_CONTACT          ; (carry set = mario/rex contact)
	LDA $1490|!Base2        ; \ if mario star timer > 0 ...
	BNE HAS_STAR            ; /    ... goto HAS_STAR
	LDA !154C,x             ; \ if rex invincibility timer > 0 ...
	BNE NO_CONTACT          ; /    ... goto NO_CONTACT
	LDA $7D                 ; \  if mario's y speed < 10 ...
	CMP #$10                ;  }   ... rex will hurt mario
	BMI REX_WINS            ; /    

MARIO_WINS:
	JSR SUB_STOMP_PTS       ; give mario points
	JSL $01AA33|!BankB      ; set mario speed
	JSL $01AB99|!BankB      ; display contact graphic
	LDA $140D|!Base2        ; \  if mario is spin jumping...
	ORA $187A|!Base2        ;  }    ... or on yoshi ...
	BNE SPIN_KILL           ; /     ... goto SPIN_KILL

;set state to squished
	LDA #$02
	STA !1570,x		;new status
	LDA #!SquishTime
	STA !1602,x		;squish time
	RTS                     ; return 

REX_WINS:
	LDA $1497|!Base2        ; \ if mario is invincible...
	ORA $187A|!Base2        ;  }  ... or mario on yoshi...
	BNE NO_CONTACT          ; /   ... return
	%SubHorzPos()           ; \  set new rex direction
	TYA                     ;  }  
	STA !157C,x             ; /
	JSL $00F5B7|!BankB      ; hurt mario
	RTS

SPIN_KILL:
	LDA #$04                ; \ rex status = 4 (being killed by spin jump)
	STA !14C8,x             ; /   
	LDA #$1F                ; \ set spin jump animation timer
	STA !1540,x             ; /
	JSL $07FC3B|!BankB      ; show star animation
	LDA #$08                ; \ play sound effect
	STA $1DF9|!Base2        ; /

NO_CONTACT:
	RTS                     ; return

STAR_SOUNDS:        db $00,$13,$14,$15,$16,$17,$18,$19
KILLED_X_SPEED:     db $F0,$10

HAS_STAR:
	%Star()
	RTS                     ; final return

SUB_STOMP_PTS:      PHY                     ; 
                    LDA $1697|!Base2        ; \
                    CLC                     ;  } 
                    ADC !1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697|!Base2        ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y             ; \ play sound effect
                    STA $1DF9|!Base2        ; /   
NO_SOUND:           TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:           JSL $02ACE5|!BankB      ; give mario points
                    PLY                     ;
                    RTS                     ; return
			
;=====

SIZE:	db $02,$02,$00,$00,$00
	db $02,$02,$00,$00,$00
	db $02,$02,$02,$02,$02

XDISP:	db $00,$08,$00,$08,$10
	db $00,$08,$00,$08,$10
	db $00,$08,$00,$00,$00

YDISP:	db $00,$00,$10,$10,$10
	db $00,$00,$10,$10,$10
	db $08,$08,$08,$08,$08
	
GFX:
	%GetDrawInfo()
	LDA !157C,x	;direction...
	STA $03

	LDA !15F6,x	;properties...
	STA $04

	LDA $03		;test direction
	BNE NoFlip
	LDA #$40
	TSB $04		;set flip bit

NoFlip:
	LDA !1570,x	;test status for dying
	STA $05		;into $05
	CMP #$03
	BNE Notdying
	LDA #$80
	TSB $04		;set the v bit
	LDA #$02	;maintain squish frame
	STA $05

Notdying:
	LDA $05	;load state
SkipA:
	ASL #2		;x4
	CLC
	ADC $05		;x5
	STA $09		;and into frame index

	LDX #$00	;reset loop index

OAM_Loop:
	PHX		;preserve loop index
	LDX $09		;load frame index

	TYA			;OAM index to A
	LSR #2			;/4
	PHY			;preserve oam index
	TAY			;oam index / 4
	LDA SIZE,x		;read size
	STA $0460|!Base2,y	;size table
	PLY			;restore oam index

	LDA $03			;test direction
	BEQ FlipX

	LDA $00
	CLC
	ADC XDISP,x		;xpos
	STA $0300|!Base2,y
	BRA DoY

FlipX:
	LDA $00
	SEC
	SBC XDISP,x		;xpos flip
	CLC
	ADC #$08		;compensate
	STA $0300|!Base2,y

	LDA SIZE,x		;test size
	BEQ DoY
	LDA $0300|!Base2,y
	SEC
	SBC #$08		;don't add anything for 8px tiles
	STA $0300|!Base2,y

DoY:
	LDA $04			;test flip
	BIT #$80
	BNE FlipY
	
	LDA $01
	CLC
	ADC YDISP,x		;ypos
	SEC
	SBC #$08		;no object clipping accounts for 24px
	STA $0301|!Base2,y
	BRA DoCHR

FlipY:
	LDA $01
	SEC
	SBC YDISP,x
	CLC
	ADC #$08
	STA $0301|!Base2,y

DoCHR:
	LDA TILEMAP,x		;chr
	STA $0302|!Base2,y

	LDA $04			;properties
	ORA $64
	STA $0303|!Base2,y

	INY			;next tile
	INY
	INY
	INY

	INC $09			;advance frame index
	PLX			;restore loop index

	INX			;advance loop
	CPX #$05		;5 tiles
	BNE OAM_Loop

	LDX $15E9|!Base2	;restore sprite index
	LDY #$FF		;$460 written to
        LDA #$04     	  	;5 tiles
        JSL $01B7B3|!BankB	;reserve
	RTS
	
SetupCust:
	LDA !157C,x		;load sprite direction
	BNE GoinLeft		;if not equal zero (going left), branch.
	LDA #$0C		;load X offset for facing right.
	BRA +

GoinLeft:
	LDA #$00		;load X offset.
+
	STA $00			;store to scratch RAM.

	LDA #$F9		;load Y offset
	STA $01			;store to scratch RAM.

	LDA !157C,x		;load sprite direction
	BNE GoinLeft2		;if not equal zero (going left), branch.
	LDA #$18		;load X speed for facing right.
	BRA +

GoinLeft2:
	LDA #$E8
+
	STA $02			;store to scratch RAM.

	%SubVertPos()
	CPY #$00
	BNE IsUp
	LDA #$08
	BRA +

IsUp:
	LDA #$F8
+
	STA $03			;store to scratch RAM.

	LDA #!Projectile	;load sprite number to spawn
	SEC			;set carry - spawn custom sprite
	%SpawnSprite()
	RTS