;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Spike's Ball, by mikeyk
;;
;; Description: This is the ball that Spike throws.
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!DIR = $03
!PROPRAM = $04
!TEMP = $09

SPEED:	db $20,$E0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
PRINT "INIT ",pc
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Run:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()
	JSR SUB_GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN           
	LDA $9D			;locked sprites?
	BNE RETURN
	LDA $15D0,x		;yoshi eating?
	BNE RETURN

	LDY $157C,x
	LDA SPEED,y
	STA $B6,x

	JSL $818022		;no x gravity
	JSL $81A7DC		;mario interact

RETURN:
	RTS        
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


TILEMAP:	        db $A4,$A4,$A4,$A4
PROPERTIES:	db $80,$00,$80,$00


SUB_GFX:
	LDA $15F6,x	;PROPERTIES...
	STA !PROPRAM

	LDA $14		;frame counter
	LSR A
	LSR A		;every 4th frame
	AND #$01	;3 frames
	STA !TEMP	;frame index

OAM_LOOP:
	LDA $00
	STA $0300,y	;xpos
	LDA $01
	STA $0301,y	;ypos
	LDX !TEMP
	LDA TILEMAP,x
	STA $0302,y	;TILEMAP
	LDA !PROPRAM
	ORA PROPERTIES,x	;PROPERTIES
        ORA $64                 ; add in tile priority of level
	STA $0303,y

	LDX $15E9	;restore sprite index

	LDY #$02	;16x16
	LDA #$00	;1 tile
	JSL $81B7B3	;reserve

	RTS