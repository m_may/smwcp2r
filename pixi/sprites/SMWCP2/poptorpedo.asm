;marine pop torpedo projectile
;by smkdan
;GFX from Mattrizzle
;thanks mior for locating the block bounce routine

;$019138: grab map16# of block sprite is touching
;$028663: shatter block
;$00BEB0: generate tile

;enemy hit sound
!ENEMYHITSOUND = $25
;map16 block (0-0x200)
!TURNBLOCKMAP16 = $132

!DIR = $03
!PROPRAM = $04
!TEMP = $09

!TILEMAP = $E8

PRINT "INIT ",pc
	LDA #!TILEMAP		;tilemap into stunned goomba tile
	STA $15EA,x
	RTL

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

RETURN_I:
	RTS

Run:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()
	JSR GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_I           
	LDA $9D			;locked sprites?
	BNE RETURN_I
	LDA $15D0,x		;yoshi eating?
	BNE RETURN_I

	JSL $018022		;update xpos no gravity
	STZ $AA,x		;no yspd

;block interaction
	LDA $15A0,x		;test something
	ORA $186C,x
	BNE CHECKWALL

	JSL $019138		;map 16 'query'
	LDA $1862		;high byte
	XBA
	LDA $1860		;lowbyte
	REP #$20
	CMP.w #!TURNBLOCKMAP16	;11E = turn block
	SEP #$20
	BNE CHECKWALL

	PHB			;preserve current bank
	LDA #$02		;push 02
	PHA
	PLB			;bank = 02
	LDA #$00		;default shatter
	JSL $028663		;shatter block
	PLB			;restore bank

	LDA #$02		;block to generate
	STA $9C
	JSL $00BEB0		;generate

	JSR SUB_SMOKE		;smoke
	STZ $14C8,x		;kill sprite, already hit something

	RTS 			;return here

CHECKWALL:			;not turn block
	LDA $1588,x		;test wall
	AND #$03
	BEQ CHECKSPRITE

;touchingwall	
	PHK			;push current program bank
	PEA RETURNADD		;push return address -1
	PHB			;preserve bank
	LDA #$01		;bank 01
	PHA		
	PLB			;new data bank
	PEA $801F		;push return address containing PLB + RTL
	JML $01999E		;long jump to local routine

RETURNADD:
	db $FF			;because of broken math system

	JSR SUB_SMOKE		;smoke
	STZ $14C8,x		;erase sprite

CHECKSPRITE:
	STX !TEMP		;store self to !TEMP
	LDY #$0B		;12 entries
CHECKLOOP:
	CPY !TEMP		;skip self
	BEQ NEXTSPRITE

	LDA $14C8,y		;check status
	BEQ NEXTSPRITE
	
	LDA $1686,Y		;skip sprites that don't interact with others
	AND #$08
	BNE NEXTSPRITE

	JSL $03B69F		;clipA
	PHX
	TYX
	JSL $03B6E5		;clipB
	PLX
	JSL $03B72B		;check contact between A and B
	BCC NEXTSPRITE		;no contact = don't set

	LDA #$00
	STA $14C8,y		;erase sprite
	STZ $14C8,x		;also erase self

	JSR SUB_SMOKE		;and smoke

	LDA #!ENEMYHITSOUND	;and sound
	STA $1DFC

	RTS

NEXTSPRITE:
	DEY			;next sprite
	BPL CHECKLOOP
	RTS
       
			
;=====

GFX:
	LDA $157C,x	;direction...
	STA !DIR

	LDA $15F6,x	;properties...
	STA !PROPRAM

	LDA $00		;xpos
	STA $0300,y
	LDA $01
	STA $0301,y	;ypos
	LDA #!TILEMAP
	STA $0302,y	;chr
	LDA $15F6,x
	ORA $64
	STA $0303,y	;prop

	LDY #$02	;16x16
	LDA #$00	;1 tile
	JSL $01B7B3	;reserve

	RTS

SUB_SMOKE:
STZ $00
STZ $01
LDA #$1B
STA $02
LDA #$01
%SpawnSmoke()
RTS