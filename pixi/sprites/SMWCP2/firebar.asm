;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Roto Disc, by mikeyk
;;
;; Description: This sprite circles a block.  Mario cannot touch it, even with a spin
;; jump.  The direction of rotation is determined by the x position.  The RADIUS is
;; specified in the .cfg file
;;
;; NOTE: Like the Ball and Chain, this enemy should not be used in levels that
;; allow Yoshi.
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    !CIRCLE_COORDS = $07F7DB|!BankB                    
                    !SPRITE_STATE = !C2
                    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            
                    PRINT "MAIN ",pc                        
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    LDA #$02
                    STA !SPRITE_STATE,x
                    JSR START_SPRITE_CODE   ;  |
                    DEC !SPRITE_STATE,x
                    JSR START_SPRITE_CODE   ;  |
                    DEC !SPRITE_STATE,x
                    JSR START_SPRITE_CODE   ;  |
                    PLB                     ;  |
                    PRINT "INIT ",pc
                    RTL                     ; /


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RADIUS:              db $28,$18,$08
CLOCK_SPEED:         db $03,$FD

START_SPRITE_CODE:  LDA #$01
			%SubOffScreen()
		    LDY !SPRITE_STATE,x
                    LDA RADIUS,y
                    STA !187B,x
                    
                    LDA $9D
                    BNE LABEL92
                    
                    LDA !SPRITE_STATE,x
                    CMP #$02
                    BNE LABEL92
		    
                    LDY #$00
                    LDA !E4,x
                    AND #$10
                    BNE LABEL90
                    INY
LABEL90:             TYA
                    STA !157C,x
                    LDA CLOCK_SPEED,y
                    LDY #$00
                    CMP #$00
                    BPL LABEL91
                    DEY
LABEL91:             CLC
                    ADC !1602,x
                    STA !1602,x
                    TYA
                    ADC !151C,x
                    AND #$01
                    STA !151C,x
LABEL92:             LDA !151C,x
                    STA $01
                    LDA !1602,x
                    STA $00
                    REP #$30
                    LDA $00
                    CLC
                    ADC.w #$0080
                    AND.w #$01FF
                    STA $02
                    LDA $00
                    AND.w #$00FF
                    ASL A
                    TAX
                    LDA $07F7DB,x
                    STA $04
                    LDA $02
                    AND.w #$00FF
                    ASL A
                    TAX
                    LDA.l $07F7DB|!BankB,x
                    STA $06
                    SEP #$30
                    LDX $15E9|!Base2
                    LDA $04
                    STA $4202
                    LDA !187B,x
                    LDY $05
                    BNE LABEL93
                    STA $4203
                    ASL $4216
                    LDA $4217
                    ADC #$00
LABEL93:             LSR $01
                    BCC LABEL94
                    EOR #$FF                ; \ reverse direction of rotation
                    INC A                   ; /
LABEL94:             STA $04
                    LDA $06
                    STA $4202
                    LDA !187B,x
                    LDY $07
                    BNE LABEL95
                    STA $4203
                    ASL $4216
                    LDA $4217
                    ADC #$00
LABEL95:             LSR $03
                    BCC LABEL96
                    EOR #$FF
                    INC A
LABEL96:             STA $06
                    LDA !E4,x
                    PHA
                    LDA !14E0,x
                    PHA
                    LDA !D8,x
                    PHA
                    LDA !14D4,x
                    PHA
                    LDY $0F86,x
                    STZ $00
                    LDA $04
                    BPL LABEL97
                    DEC $00
LABEL97:             CLC
                    ADC !E4,x
                    STA !E4,x
                    PHP
                    PHA
                    SEC
                    SBC $1534,x
                    STA $1528,x
                    PLA
                    STA $1534,x
                    PLP
                    LDA !14E0,x
                    ADC $00
                    STA !14E0,x
                    STZ $01
                    LDA $06
                    BPL LABEL98
                    DEC $01
LABEL98:             CLC
                    ADC !D8,x
                    STA !D8,x
                    LDA !14D4,x
                    ADC $01
                    STA !14D4,x
            
                    JSL $01A7DC|!BankB    ; check for mario/sprite contact
                    BCC RETURN_EXTRA          ; (carry set = mario/sprite contact)
                    LDA $1490|!Base2      ; \ if mario star timer > 0 ...
                    BNE HAS_STAR            ; /    ... goto HAS_STAR

SPRITE_WINS:         LDA $1497               ; \ if mario is invincible...
                    ;ORA $187A               ;  }  ... or mario on yoshi...
                    BNE RETURN_EXTRA          ; /   ... return
                    JSL $00F5B7|!BankB       ; hurt mario

RETURN_EXTRA:        JSR SUB_GFX
                    
                    LDA $14C8,x             ; \ if sprite status != 8...
                    CMP #$08
                    BEQ ALIVE
                    
                    PLA
                    PLA
                    PLA
                    PLA
                    BRA DONE
                    
ALIVE:               PLA     
                    STA !14D4,x
                    PLA        
                    STA !D8,x  
                    PLA        
                    STA !14E0,x
                    PLA        
                    STA !E4,x                   
                    
DONE:                LDA !SPRITE_STATE,x
                    BNE RETURN78
                    LDY #$02                ; \ 02 because we haven't written to $0460
                    LDA #$02                ; | A = number of tiles drawn - 1
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
               
RETURN78:            RTS

HAS_STAR:
		%Star()
BRA RETURN_EXTRA	;damn these stack push/pulls


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine - specific to sprite
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    TILEMAP:     db $E8,$EA,$E8,$EA
                    PROPERTIES:  db $00,$00,$C0,$C0

SUB_GFX:             %GetDrawInfo()      ; after: Y = index to sprite tile map ($300)
                                            ;      $00 = sprite x position relative to screen boarder 
                                            ;      $01 = sprite y position relative to screen boarder  
                    
                    LDA !SPRITE_STATE,x
                    ASL A
                    ASL A
                    STA $02
                    TYA
                    CLC
                    ADC $02
                    TAY
                   
                    LDA !157C,x
                    STA $02

		    REP #$20
                    LDA $00
                    STA $0300|!Base2,y
		    SEP #$20

                    PHX
		LDA $13BF|!Base2		;BlindEdit: a translevel check for the GFX in crystal conception
		CMP.b #$137-$DC			;(because why tf have a copy of the entire sprite just for different gfx?)
		BNE +

LDX $15E9|!Base2
LDA !15F6,x
AND #$F1
ORA #$06	;force blue palette as well
STA !15F6,x
		LDX #$01
		BRA ++
+
                    LDA $14
                    LSR A
                    LSR A
                    AND #$03
                    TAX
++
                    LDA TILEMAP,x
                    STA $0302|!Base2,y             ; / 
                    
                    LDA PROPERTIES,x
                    LDX $15E9|!Base2
                    ORA !15F6,x             ; load tile pallette
                    LDX $02
                    BNE NO_XOR
                    EOR #$40
NO_XOR:             ORA $64                 ;  | 
                    STA $0303|!Base2,y       ; / store tile PROPERTIES
                    PLX

                    RTS                     ; return