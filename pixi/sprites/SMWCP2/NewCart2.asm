;Coaster Commotion Cart by Blind Devil
;guess what? original one was broken so the imbecile here had to recode everything from scratch.
;another note: NO LONGER USES FREE RAM ADDRESSES!!!

;Main sprite works like this: lead cart initializes, and spawns two more carts behind it;
;they're labelled in this order: [(3)>[(2)>[(1)>
;if the extra bit is clear.
;If it is set, it'll be an enemy cart that comes towards the player.

!MaxSpd = $28			;X speed of the cart (it now accelerates!)
!EnemySpd = $E0			;X speed of the enemy cart (negative as it goes left)
!JumpSpd = $B8			;player/cart jump speed
!JumpDelay = $08		;delay for non-ridden carts to jump after the ridden one jumps
!InitLockJump = $10		;amount of frames to prevent the player from jumping off of the cart right after riding it

PlayerHead:
db $8E,$C2,$C0	;straight, up, down (small)
db $8C,$C2,$C0	;straight, up, down (big)

PHXDisp:
db $FC,$F7,$05

PHYDisp:
db $F2,$FA,$F4


EnemyHead:
db $AC,$CC,$AE	;straight, up, down (koopa)
db $CE,$EC,$EE	;straight, up, down (goomba)

EHXDisp:
db $06,$0C,$FC

EHYDisp:
db $F2,$FB,$F3


CartTiles:
db $08,$08	;straight: upper part
db $A0,$A2	;straight: lower part

db $88,$8A	;steep up: upper part
db $A8,$AA	;steep up: lower part

db $C8,$CA	;steep down: upper part
db $E8,$EA	;steep down: lower part

!Wheel = $80	;wheel tile


CartXDisp:
db $08,$F8	;straight: upper part (facing right)
db $08,$F8	;straight: lower part (facing right)

db $08,$F8	;steep up (facing right)
db $08,$F8	;steep up (facing right)

db $08,$F8	;steep down (facing right)
db $08,$F8	;steep down (facing right)

db $F8,$08	;straight: upper part (facing left)
db $F8,$08	;straight: lower part (facing left)

db $FA,$0A	;steep up (facing left)
db $FA,$0A	;steep up (facing left)

db $F8,$08	;steep down (facing left)
db $F8,$08	;steep down (facing left)


CartYDisp:
db $EF,$EF	;straight: upper part
db $FF,$FF	;straight: lower part

db $F6,$F6	;steep up
db $06,$06	;steep up

db $F7,$F7	;steep down
db $07,$07	;steep down


WheelXDisp:
db $0A,$FE	;straight: wheel 1, wheel 2 (facing right)
db $0C,$04	;steep up (facing right)
db $04,$FC	;steep down (facing right)

db $FE,$0A	;straight (facing left)
db $FE,$06	;steep up (facing left)
db $05,$0D	;steep down (facing left)

WheelYDisp:
db $09,$09	;straight: left wheel, right wheel
db $05,$0D	;steep up
db $0C,$04	;steep down


Properties:
db $40,$00



print "INIT ",pc
LDA $7FAB10,x
AND #$04
BEQ +

INC !C2,x		;animate cart wheels
LDA #$01
STA !157C,x		;initially face left
LDA !E4,x
AND #$10
LSR #4
STA !1510,x		;used to determine the enemy which will be riding the cart (a.k.a. it's the X-pos that handles it)
RTL

+
STZ !157C,x		;initially face right
STZ !1510,x		;identify this cart (main) as cart 0

LDA #$E0
JSR SharedSpawn
LDA #$01
STA !1510,y		;identify spawned cart as cart 1
PHY
LDA #$C0
JSR SharedSpawn
LDA #$02
STA !1510,y		;identify spawned cart as cart 2

TYA
STA !1FD6,x		;cart 2 index in ram 2 of cart 0
PLA
STA !1534,x		;cart 1 index in ram 1 of cart 0
TXA
STA !151C,x		;cart 0 index in ram 0 of cart 0

TYA
STA !1FD6,y		;cart 2 index in ram 2 of cart 2
LDA !1534,x
STA !1534,y		;cart 1 index in ram 1 of cart 2
TXA
STA !151C,y		;cart 0 index in ram 0 of cart 2

LDY !1534,x
LDA !1FD6,x
STA !1FD6,y		;cart 2 index in ram 2 of cart 1
TYA
STA !1534,y		;cart 1 index in ram 1 of cart 1
TXA
STA !151C,y		;cart 0 index in ram 0 of cart 1
;yeah I know I could optimize the above id portions but I'm afraid it'll break so I'll keep it that way .-.

RTL

SharedSpawn:
STA $00
STZ $01
STZ $02
STZ $03
LDA !7FAB9E,x
SEC
%SpawnSprite()
LDA #$08
STA !14C8,y
LDA #$00
STA !C2,y
RTS

KillXSpd:
db $F0,$10

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
RTL

Return:
RTS

SpriteCode:
JSR Graphics

LDA !14C8,x
CMP #$08
BNE Return
LDA $9D
BNE Return

LDA $7FAB10,x
AND #$04
BEQ +

LDA #$00
%SubOffScreen()

JMP EnemyCart

+
LDA #$03
%SubOffScreen()

LDA #$02
STA $1401|!Base2	;prevent L/R scrolling at any time

JSL $01802A|!BankB	;apply speeds, gravity and object interaction

LDA !C2,x		;states (00 = still, 01 = moving, 02-FF = falling off)
BEQ Still
DEC
BEQ Moving

LDA !1686,x
ORA #$80
STA !1686,x
RTS

Still:
STZ !B6,x

JSL $01A7DC|!BankB
BCC .ret
JSR ReposPlayer

LDA !151C,x
TAY
LDA #$01
STA !C2,y

LDA !1534,x
TAY
LDA #$01
STA !C2,y

LDA !1FD6,x
TAY
LDA #$01
STA !C2,y

LDA #!InitLockJump
STA !1540,x

.ret
RTS

Moving:
LDA !1588,x
AND #$03
BEQ ++

BumptyBumpEnemy:
LDA !157C,x
TAY
LDA KillXSpd,y
STA !B6,x
LDA #$D0
STA !AA,x
LDA #$02
STA !1528,x

STZ !1564,x
INC !C2,x

JSL $01A7DC|!BankB
BCC +

LDA !157C,x
TAY
LDA #$C8
STA $7D
LDA KillXSpd,y
STA $7B

STZ $73

JSL $00F5B7|!BankB

+
RTS

++
LDA !1588,x
AND #$04
BEQ +			;if not on ground don't change cart image and don't jump if set to

JSR CheckSurface

LDA !1FE2,x		;load delayed jump timer
CMP #$01		;compare to value
BNE +			;if not equal, don't jump.

JSR DelayedJump		;jump! jump!

STZ !1FE2,x		;reset le timer

+
LDA !B6,x
CMP #!MaxSpd
BCS +

INC !B6,x

+
LDA !154C,x
BNE +
JSL $01A7DC|!BankB
BCC +
JSR ReposPlayer
JSR HandlePlayerRideActions

+
RTS

CheckSurface:
LDA !15B8,x
CMP #$03
BEQ .steep2
CMP #$FD
BNE .straight

LDA !157C,x
BNE .inv2

.inv1
LDA !1588,x
AND #$04
BEQ +
STZ !AA,x
+

LDA #$01
BRA .storesurface

.steep2
LDA !157C,x
BNE .inv1

.inv2
LDA #$02
BRA .storesurface

.straight
LDA #$00

.storesurface
STA !1528,x
RTS

HandlePlayerRideActions:
LDA !1540,x
BNE .nothing

LDA $16
AND #$80
BEQ +
JSR CartJump

+
LDA $18
AND #$80
BEQ .nothing

JSR PlayerJump

.nothing
RTS

CartJump:
LDA !1588,x
AND #$04
BEQ +

LDA #$2B
STA $1DF9|!Base2

JSR SetSimultaneousJump

DelayedJump:
LDA #!JumpSpd
STA !AA,x		;set jump speed for this cart

+
RTS

PlayerJump:
LDA #$12
LDY !151C,x
STA !154C,y
LDY !1534,x
STA !154C,y
LDY !1FD6,x
STA !154C,y

LDA #$35
STA $1DFC|!Base2	;play sfx

STZ $73			;don't duck anymore

LDA #!JumpSpd
STA $7D			;set jump speed for mario

LDA $15
AND #$01
BEQ +			;if holding right while jumping, make mario jump forwards a bit

LDA $7B
CLC
ADC #$18
STA $7B

+
RTS

SetSimultaneousJump:
LDA !1510,x		;load cart ID
BEQ ID0			;if 0, set timer for 1 and double for 2.
DEC			;decrement A
BEQ ID1			;if 1, set timers for 0 and 2.

LDA !1534,x		;load index of cart 1
TAY			;transfer to Y
LDA #!JumpDelay		;load value
STA !1FE2,y		;store to cart's delayed jump timer.
LDA !151C,x		;load index of cart 0
TAY			;transfer to Y
LDA #!JumpDelay*2	;load value
STA !1FE2,y		;store to cart's delayed jump timer.
RTS

ID0:
LDA !1534,x		;load index of cart 1
TAY			;transfer to Y
LDA #!JumpDelay		;load value
STA !1FE2,y		;store to cart's delayed jump timer.
LDA !1FD6,x		;load index of cart 2
TAY			;transfer to Y
LDA #!JumpDelay*2	;load value
STA !1FE2,y		;store to cart's delayed jump timer.
RTS

ID1:
LDA !151C,x		;load index of cart 0
TAY			;transfer to Y
LDA #!JumpDelay		;load value
STA !1FE2,y		;store to cart's delayed jump timer.
LDA !1FD6,x		;load index of cart 2
TAY			;transfer to Y
LDA #!JumpDelay		;load value
STA !1FE2,y		;store to cart's delayed jump timer.
RTS

ReposPlayer:
LDA #$02
STA !1564,x
STA $13E4|!Base2
LDA #$40
CMP $142A|!Base2
BCC .LessThan
BEQ .ScrollCorrect
INC $142A|!Base2
BRA .ScrollCorrect
.LessThan
DEC $142A|!Base2
.ScrollCorrect

LDA #$80
STA $1406|!Base2
STZ $140D|!Base2
LDA #$7F
STA $78
STA $73
LDA !E4,x
STA $94
LDA !14E0,x
STA $95
LDA !D8,x
SEC
SBC #$18
STA $96
LDA !14D4,x
SBC #$00
STA $97
STZ $7D
LDA !B6,x
STA $7B
RTS

EnemyCart:
JSL $01802A|!BankB	;apply speeds, gravity and object interaction

LDA !C2,x
CMP #$02
BEQ Dead

STZ !1FD6,x

JSR CheckCartsContact
BCS BUMP

LDA !1588,x
AND #$03
BEQ +

INC !1FD6,x

BUMP:
PHY

LDA !157C,x
TAY
LDA KillXSpd,y
STA !B6,x
LDA #$D0
STA !AA,x
LDA #$02
STA !1528,x

LDA #$02
STA !C2,x
LDA !1686,x
ORA #$80
STA !1686,x

PLY

LDA !1FD6,x
BNE Dead

PHX
TYX
JSR BumptyBumpEnemy 
PLX

Dead:
RTS

+
LDA #!EnemySpd
STA !B6,x

LDA !1588,x
AND #$04
BEQ +			;if not on ground don't change cart image

JSR CheckSurface

+
LDA !154C,x
BNE +
JSL $01A7DC|!BankB
BCC +

JSL $00F5B7|!BankB

+
RTS

CheckCartsContact:
LDY #!SprSize-1

IntLoop:
CPY #$00
BEQ SprIntRet

CPY $15E9|!Base2	;if checking its own slot, redo the loop
BEQ RedoSILoop

LDA !14C8,y
CMP #$08
BNE RedoSILoop		;if sprite isn't active, redo the loop

PHX
TYX
LDA !7FAB9E,x
PLX
CMP !7FAB9E,x
BNE RedoSILoop		;if custom sprite numbers don't match, redo the loop

LDA !C2,y
CMP #$02
BEQ RedoSILoop		;if the cart is falling down, redo the loop

JSL $03B69F|!BankB
PHX
TYX
JSL $03B6E5|!BankB
PLX
JSL $03B72B|!BankB
BCC RedoSILoop		;finally, if there's no contact, redo the loop
RTS

SprIntRet:
CLC
RTS

RedoSILoop:
DEY
BRA IntLoop

Graphics:
%GetDrawInfo()

LDA !1564,x
STA $09

STZ $03
STZ $04
STZ $08

LDA !C2,x
DEC
BNE +

LDA $14
LSR #2
AND #$01
STA $08

+
LDA !157C,x
STA $02
BEQ +		;if facing right don't modify XDisps

LDA #$0C
STA $03
LDA #$06
STA $04

+
STZ $05		;tiles drawn = 0

LDA !1528,x
ASL
STA $06		;surface/slope type for wheels
ASL
STA $07		;surface/slope type for cart

PHX

LDA $7FAB10,x
AND #$04
BEQ +

LDA !1510,x
STA $09

JSR DrawEnemyHead
BRA NoHead

+
LDA $09
BEQ NoHead
LDA $71
BNE NoHead

LDA $06
LSR
TAX

LDA $19
BEQ +

TXA
CLC
ADC #$03
TAX

+
JSR DrawPlayerHead

NoHead:
LDX #$05	;gfx loop count (cart = 4 tiles, wheels = 2 tiles)

GFXLoop:
CPX #$04
BCC DrawCart

PHX
TXA
CLC
ADC $06
CLC
ADC $04
TAX

LDA $00
CLC
ADC WheelXDisp-4,x
STA $0300|!Base2,y
PLX

PHX
TXA
CLC
ADC $06
TAX

LDA $01
CLC
ADC WheelYDisp-4,x
STA $0301|!Base2,y

LDA #!Wheel
STA $0302|!Base2,y

LDX $15E9|!Base2
LDA !15F6,x
LDX $08
ORA Properties,x
ORA $64
STA $0303|!Base2,y
PLX
PHY
TYA
LSR #2
TAY
LDA #$00
STA $0460|!Base2,y
PLY
INY #4
INC $05

RedoLoop:
DEX
BPL GFXLoop

PLX

LDY #$FF		;load tile size into Y (00 = 8x8, 01 = 16x16, FF = manually set)
LDA $05			;load amount of tiles drawn
DEC			;minus one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.

DrawCart:
PHX
TXA
CLC
ADC $03
CLC
ADC $07
TAX

LDA $00
CLC
ADC CartXDisp,x
STA $0300|!Base2,y
PLX

PHX
TXA
CLC
ADC $07
TAX

LDA $01
CLC
ADC CartYDisp,x
STA $0301|!Base2,y

LDA CartTiles,x
STA $0302|!Base2,y

LDX $15E9|!Base2
LDA !15F6,x
LDX $02
ORA Properties,x
ORA $64
STA $0303|!Base2,y
PLX

PHY
TYA
LSR #2
TAY
LDA #$02
STA $0460|!Base2,y
PLY
INY #4
INC $05
BRA RedoLoop

DrawPlayerHead:
LDA PlayerHead,x
STA $0302|!Base2,y

LDA $06
LSR
TAX

LDA $00
CLC
ADC PHXDisp,x
STA $0300|!Base2,y

LDA $01
CLC
ADC PHYDisp,x
STA $0301|!Base2,y

LDA #$41

RestOfHeadStuff:
ORA $64
STA $0303|!Base2,y

PHY
TYA
LSR #2
TAY
LDA #$02
STA $0460|!Base2,y
PLY

INY #4
INC $05
RTS

DrawEnemyHead:
LDA $06
LSR
TAX

LDA $00
CLC
ADC EHXDisp,x
STA $0300|!Base2,y

LDA $01
CLC
ADC EHYDisp,x
STA $0301|!Base2,y

LDA $09
BEQ +

TXA
CLC
ADC #$03
TAX

+
LDA EnemyHead,x
STA $0302|!Base2,y

LDA #$09
BRA RestOfHeadStuff