;NOTE: The tilemap/property bytes for the arrow are handled within the graphics routine itself.

Xoffsets:		
db $00,$10,$00,$10

TilePropLeft:		
db $3B,$3B,$3B,$3B
TilePropRight:
db $7B,$7B,$7B,$7B

TilemapLeft:			;These are both part of the same tilemap
db $E0
TilemapLeft2: 
db $E2,$E4,$E6


TilemapRight:			;These are both part of the same tilemap
db $E6
TilemapRight2:
db $E4,$E2,$E0

FireSFXGoingRight:
db $2D,$2E,$2F,$30,$31,$32,$33,$34

FireSFXGoingLeft:
db $34,$33,$32,$31,$30,$2F,$2E,$2D



;For anyone looking over this code, don't expect "left" and "right" to mean anything useful 90% of the time,
;considering the fact that when the enemy is on the left side of the screen, it's about to go right, and vice-versa.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA #$80
STA $15AC,x
STZ $0F5E|!Base2

JSR FacePlayer	; face the player initially

RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR BirdMain

LDA $1528,x
BEQ DontReadjustMariosYPosition
REP #$20
LDA $D3
SEC
SBC #$0008
STA $D3
SEP #$20
DontReadjustMariosYPosition:

PLB
RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BirdMain:
STZ $1528,x
;;;;;LDA $19					
;;;;;BNE DontInternallyAdjustMariosYPosition
REP #$20
LDA $D3
CLC
ADC #$0008
STA $D3
SEP #$20
LDA #$01
STA $1528,x
;;;;;DontInternallyAdjustMariosYPosition:



LDA $14C8,x
CMP #$08	; if the sprite is in normal status...
BEQ NormalRt	; run the standard code
JMP GraphicsAiR	;

NormalRt:		;
LDA $9D			; if sprites are locked...
BEQ Continue1		;
JMP GraphicsAiR		; For anyone looking over this code, the "AiR" used to stand for something when I was first designing the sprite.  It means nothing now.

Continue1:

LDA #$C0		;load value
STA $0F5E|!Base2	;store to respawn ram timer (uberasm). while sprite is active, this is a constant value.

LDA $157C,x
BEQ GoingRight
JMP GoingLeft		;Determine attack direction



GoingRight:
LDA $1594,x
BEQ IsAimingRight

IsAttackingRight:
LDA $166E,x
AND #$CF
STA $166E,x
STZ $151C,x
JSR GraphicsAtR
LDA #$A0
STA $B6,x
LDA #$12
STA $AA,x

JSL $818032
JSL $81802A		; update sprite position  
JSL $81A7DC

LDA $14E0,x
STA $01
LDA $E4,x
STA $00
REP #$20
LDA $00
SEC
SBC $1A
BMI PastScreenEdgeOnLeft

STA $02
SEP #$20
LDA $13
AND #$03
CMP #$03
BNE DontPlaySoundGoingRight

LDA $02
LSR #5
PHX
TAX
LDA FireSFXGoingRight,x
STA $1DFC


PLX
RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DontPlaySoundGoingRight:
;;;;;;;;;;JMP GraphicsAtR
RTS


PastScreenEdgeOnLeft:
SEP #$20
LDA $160E,x
BNE DontSetWaitTimerRight
LDA #$40
STA $160E,x
DontSetWaitTimerRight:
DEC $160E,x
LDA $160E,x
BNE DontSetToAimingGoingLeft
STZ $1594,x
LDA #$80
STA $15AC,x
LDA #$01
STA $157C,x

DontSetToAimingGoingLeft:
RTS

IsAimingRight:
LDA $166E,x
ORA #$30
STA $166E,x
STZ $AA,x
LDA $15Ac,x
CMP #$20
BCC MaybePlaySoundEffectLeft

LDA $97			;Set the Y position to Mario's
STA $14D4,x
LDA $D3
STA $D8,x

BRA DontAdjustYForRight
MaybePlaySoundEffectLeft:
LDA $151C,x
BNE DontAdjustYForRight
LDA #$1E
STA $1DFC
LDA #$01
STA $151C,x

DontAdjustYForRight:
REP #$20		;We still have to update the X position
LDA $1A
CLC
ADC #$00F0
STA $00
SEP #$20
LDA $01
STA $14E0,x
LDA $00
STA $E4,x



LDA $15AC,x
BEQ StartAttackingRight
JMP GraphicsAiR
StartAttackingRight:

;LDA $97
;STA $14D4,x
;LDA $D3
;STA $D8,x
REP #$20
LDA $1A
CLC
ADC #$0100
STA $00
SEP #$20
LDA $01
STA $14E0,x
LDA $00
STA $E4,x
LDA #$01
STA $1594,x

LDA $D8,x
STA $00
LDA $14D4,x
STA $01
REP #$20
LDA $00
SEC
SBC #$0014
STA $00
SEP #$20
LDA $00
STA $D8,x
LDA $01
STA $14D4,x

LDA #$12
STA $AA,x

GAiRJump:
JMP GraphicsAiR

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;Going left code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GoingLeft:
LDA $1594,x
BEQ IsAimingLeft
BRA IsAttackingLeft


IsAttackingLeft:
LDA $166E,x
AND #$CF
STA $166E,x
STZ $151C,x
JSR GraphicsAtR
LDA #$12
STA $AA,x
LDA #$60
STA $B6,x

JSL $018032
JSL $01802A		; update sprite position  
JSL $01A7DC



LDA $14E0,x
STA $01
LDA $E4,x
STA $00
REP #$20
LDA $1A
CLC
ADC #$0100
SEC
SBC $00
BCC PastScreenEdgeOnRight

STA $02
SEP #$20
LDA $13
AND #$03
CMP #$03
BNE DontPlaySoundGoingLeft

LDA $02
LSR #5
PHX
TAX
LDA FireSFXGoingLeft,x
STA $1DFC


PLX
;;;;;JMP GraphicsAtR
RTS
DontPlaySoundGoingLeft:
;;;JMP GraphicsAtR
RTS


PastScreenEdgeOnRight:
SEP #$20
LDA $160E,x
BNE DontSetWaitTimerLeft
LDA #$40
STA $160E,x
DontSetWaitTimerLeft:
DEC $160E,x
LDA $160E,x
BNE DontSetToAimingGoingRight
STZ $1594,x
LDA #$80
STA $15AC,x
STZ $157C,x


DontSetToAimingGoingRight:
RTS

IsAimingLeft:
LDA $166E,x
ORA #$30
STA $166E,x
STZ $AA,x
LDA $15Ac,x
CMP #$20
BCC MaybePlaySoundEffectRight

LDA $97			;Set the Y position to Mario's
STA $14D4,x
LDA $D3
STA $D8,x

BRA DontAdjustYForLeft
MaybePlaySoundEffectRight:
LDA $151C,x
BNE DontAdjustYForLeft
LDA #$1E
STA $1DFC
LDA #$01
STA $151C,x

DontAdjustYForLeft:
REP #$20		;We still have to update the X position
LDA $1A
STA $00
SEP #$20
LDA $01
STA $14E0,x
LDA $00
STA $E4,x



LDA $15Ac,x
BEQ StartAttackingLeft
JMP GraphicsAiR
StartAttackingLeft:

;LDA $97
;STA $14D4,x
;LDA $D3
;STA $D8,x
REP #$20
LDA $1A
SEC
SBC #$0020
STA $00
SEP #$20
LDA $01
STA $14E0,x
LDA $00
STA $E4,x
LDA #$01
STA $1594,x

LDA $D8,x
STA $00
LDA $14D4,x
STA $01
REP #$20
LDA $00
SEC
SBC #$0014
STA $00
SEP #$20
LDA $00
STA $D8,x
LDA $01
STA $14D4,x

LDA #$12
STA $AA,x


GAiRJump2:
JMP GraphicsAiR

LDA #$00
%SubOffScreen()		; offscreen processing
JSL $81802A		; update sprite position         



FacePlayer:
%SubHorzPos()	;
TYA		;
STA $157C,x	;
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routines
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


GraphicsAiR:
GraphicsAtR:


%GetDrawInfo()

LDA $1594,x
BNE DrawAttacking

LDA $157C,x	;Aiming graphics
ASL #6
STA $0B

LDA $00
STA $0300,y
LDA $01
SEC
SBC #$14
STA $0301,y
LDA #$E8		;TILEMAP FOR ARROW
STA $0302,y

LDA $15AC,x
CMP #$10
BCC DontAdjustPalette
LDA $14
LSR 
AND #$07
ASL
BRA DoneAdjustingPalette
DontAdjustPalette:
LDA #$0A
DoneAdjustingPalette:

ORA #$31	;Use #$31 to use second graphics page;	PROPERTY BYTE FOR ARROW
ORA $0B		;Add in X flip
STA $0303,y
		; pull back the sprite index
LDY #$02	; the tiles are 16x16
LDA #$00	; drew 1 tile
JSL $81B7B3	; finish off the OAM write

RTS		;

DrawAttacking:

LDA $14C8,x
CMP #$08
BNE NoAlternateFrame
LDA $14
LSR
LSR
AND #$01
ASL
STA $0C			;$0C = Animation frame
BRA $02
NoAlternateFrame:
STZ $0C

LDA $157C,x
BEQ DrawForGoingLeft

PHX
LDX $0C

LDA $00
SEC
SBC #$10
STA $0300,y
LDA $01
STA $0301,y
LDA TilemapRight,x
STA $0302,y
LDA #$7B
STA $0303,y

INY #4
LDA $00

STA $0300,y
LDA $01
STA $0301,y
LDA TilemapRight2,x
STA $0302,y
LDA #$7B
STA $0303,y

BRA DoneWithGraphics


DrawForGoingLeft:
PHX
LDX $0C

LDA $00
STA $0300,y
LDA $01
STA $0301,y
LDA TilemapLeft,x
STA $0302,y
LDA #$3B
STA $0303,y

INY #4
LDA $00
CLC
ADC #$10
STA $0300,y
LDA $01
STA $0301,y
LDA TilemapLeft2,x
STA $0302,y
LDA #$3B
STA $0303,y




DoneWithGraphics:
PLX
LDY #$02	; the tiles are 16x16
LDA #$01	; drew 2 tiles
JSL $81B7B3	; finish off the OAM write

RTS		;

SetSomeYSpeed:	; ripped from $019A04

LDA $1588,x
BMI $07
LDA #$00
LDY $15B8,x
BEQ $02
LDA #$18
STA $AA,x
RTS

SetAnimationFrame:	; ripped from $018E5E

INC $1570,x
LDA $1570,x
LSR #3
AND #$01
STA $1602,x
RTS

SpriteMightWin:
LDA $77
AND #$04
CMP #$04
BEQ SpriteWins
BRA MarioWins

ImStickingTheInteractionRoutineElsewhereForReadability:
	JSL $01A7DC
	BCC NoContact95
	LDA $7D				; |
	BMI SpriteWins			;/ If he is, branch.

	LDA $72
	BEQ SpriteMightWin

	
MarioWins:
;===========================================
;Mario Hurts Sprite
;===========================================
	LDA #$01
	STA $1594,x
	
	JSL $01AA33             ;\ Set mario speed.
	JSL $01AB99             ;/ Display contact graphic.
	LDA $140D		; IF spin-jumping ..
	BNE SpinKill		; Go to spin kill.
	
	LDA #$E0		;\
	STA $AA,x		; | Make sprite fall.
	LDA #$02		; |
	STA $14C8,x		;/
	LDA #$13		;\
	STA $1DF9		;/ Play sound.
	RTS

SpinKill:
	LDA #$04		;\
	STA $14C8,x		;/ Sprite status = killed by a spin-jump.
	LDA #$08		;\
	STA $1DF9		;/ Sound to play.
	JSL $07FC3B		; Show star animation effect.
	RTS
;===========================================
;Sprite Hurts Mario
;===========================================

SpriteWins:
	LDA $1490			;\ IF Mario has star power ..
	BNE HasStar			;/ Kill sprite.
	JSL $00F5B7			;\ Otherwise, hurt Mario.
NoContact95:
	RTS

;===========================================
;Killed by Star Routine
;===========================================

HasStar:
	
	LDA $167A,x		;\ Don't disable clipping when killed by a star. 
	ORA #$01		; | NOTE: We do this to not make glitched graphics show up ..
	STA $167A,x		; | when the sprite is killed by a star.
				;/  You can also check this option in the .cfg editor.

	LDA #$02		;\ Set the sprite status to ..
	STA $14C8,x		;/ Killed by a star.
	LDA #$D0		;\
	STA $AA,x		;/ Set Y speed so sprite falls down.
	INC $18D2		; Increase number of consective enemies stomped/ killed by star.
	LDA $18D2		; IF consecutive # of enemies killed by star/stomped ..
	CMP #$08		; IS 8 ..
	BCC NotEight
	LDA #$08		;\ Keep it static at 8, because you get 1-ups all the time afterwards.
	STA $18D2		;/
NotEight:
	JSL $02ACE5		; This code calls the "give points" routine.
	LDY $18D2		; Get consecutive # of enemies stomped in Y.
	CPY #$08		; If it's less than 8 once again, return.
	BCC NoSound
	LDA StarSFX,y		;\
	STA $1DF9		;/ Play sounds depending on how many enemies were stomped/killed by star.
NoSound:
	RTS			; Return.

StarSFX:         db $00,$13,$14,$15,$16,$17,$18,$19