;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Bowser Bowling Ball (sprite A1), by imamelia
;;
;; This is a disassembly of sprite A1 in SMW, Bowser's bowling ball.
;;
;; Uses first extra bit: YES
;;
;; If the extra bit is set, the sprite will detect ground.  If not, it will act like the
;; original and travel along hardcoded paths.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BowserBallXSpeed:
db $21,$DF

BowserBallYSpeed:
db $00,$00,$00,$00,$FE,$FC,$F8,$EC
db $EC,$EC,$E8,$E4,$E0,$DC,$D8,$D4
db $D0,$CC,$C8

BowserBallDispX:
db $F0,$00,$10,$F0,$00,$10,$F0,$00,$10,$00,$00,$00

BowserBallDispY:
db $E2,$E2,$E2,$F2,$F2,$F2,$02,$02,$02,$00,$00,$00

BowserBallTilemap:
db $29,$2B,$2D,$49,$4B,$4D,$69,$6B,$6D,$0E,$0E,$0E

BowserBallTileProp:
db $0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$01,$01,$01

BowserBallTileSize:
db $02,$02,$02,$02,$02,$02,$02,$02,$02,$00,$00,$00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR BowserBallMain
PLB

print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BowserBallMain:

JSR BowserBallGFX	;

LDA $9D		; if sprites are locked...
BNE Return0	; return

LDA #$03
%SubOffScreen()
JSL $81A7DC	;
JSL $818022	;
JSL $81801A	;

LDA !AA,x	; if the sprite's Y speed
CMP #$40		; has not reached 40...
BPL MaxSpeed	;
CLC		;
ADC #$03	; make the sprite accelerate
BRA StoreSpeed	;

MaxSpeed:	;

LDA #$40		;

StoreSpeed:	;

STA !AA,x	;

LDA !AA,x	; if the sprite speed is negative...
BMI SkipSpeedSet	;
LDA !14D4,x	; or its Y position high byte is negative...
BMI SkipSpeedSet	; branch

LDA !7FAB10,x	;
AND #$04	; if the extra bit is set...
BNE DetectGround	; make the sprite detect ground like a normal sprite

LDA $D8,x	; if not...
CMP #$B0		; check the sprite's Y position
BCC SkipSpeedSet	; don't set its X speed if the sprite Y position is less than B0
LDA #$B0		; if it is B0 or greater...
STA $D8,x	; make it stay at B0

Continue:		;

LDA $AA,x	; if the sprite Y speed
CMP #$3E		; is less than 3E...
BCC NoShake	; don't shake the ground

LDY #$25		;
STY $1DFC	; play a sound effect
LDY #$20		;
STY $1887	; shake the ground

NoShake:
CMP #$08		; if the sprite's Y speed is less than 08...
BCC NoSound2	; don't play the second sound effect

LDA #$01		;
STA $1DF9	;

NoSound2:	;

JSR HandleYSpeed	;

LDA $B6,x	; if the sprite's X speed is nonzero...
BNE SkipSpeedSet	; there is no need to set it again

%SubHorzPos()
LDA BowserBallXSpeed,y
STA $B6,x

SkipSpeedSet:
LDA !B6,x	;
BEQ Return0	; return if the sprite's X speed is zero
BMI IncFrame	; increment the frame if the sprite's X speed is negative
DEC !1570,x	; decrement the frame if the sprite's X speed is positive
DEC !1570,x	;
IncFrame:		;
INC !1570,x	;

Return0:		;
RTS

DetectGround:	;

JSL $819138	; interact with objects

LDA !1588,x	;
AND #$04	;
BEQ SkipSpeedSet	;
BRA Continue	;

HandleYSpeed:	;

LDA !AA,x		;
STZ !AA,x		;
LSR			;
LSR			;
TAY			;
LDA BowserBallYSpeed,y	;
LDY !1588,x		;
BMI Return1		;
STA !AA,x		;

Return1:			;
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BowserBallGFX:

%GetDrawInfo()

;LDA #$70			;
;STA $15EA,x		;

PHX			;
LDX #$09			; 12 tiles to draw

GFXLoop:			;

LDA $00			;
CLC			;
ADC BowserBallDispX,x	;
STA $0300,y		;

LDA $01			;
CLC			;
ADC BowserBallDispY,x	;
STA $0301,y		;

LDA BowserBallTilemap,x	;
STA $0302,y		;

LDA BowserBallTileProp,x	;
ORA $64			;
STA $0303,y		;

PHY			;
TYA			;
LSR #2			;
TAY			;
LDA BowserBallTileSize,x	;
STA $0460,y		;
PLY			;

INY #4			;
DEX			;
BPL GFXLoop		;

PLX			;
LDA #$09			;
LDY #$FF			;
JSL $81B7B3		;

RTS