;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Albatoss, by mikeyk (optimized by Blind Devil)
;;
;; Description: This is a flying bird that can drop sprites.
;;
;; Uses first extra bit: YES
;; > clear: doesn't drop sprites
;; > set: drops sprites
;;
;; Extra Bytes (extension):
;; Extra byte 1:
;;	> 00: spawn normal sprite
;;	> 01: spawn custom sprite
;; Extra byte 2:
;;	> 00: don't stop when spawning sprites
;;	> 01: stop for a while when spawning sprites
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!IsRideable = 1			;0 = not rideable, 1 = is rideable (spinkill disabled), 2 = is rideable (spinkill enabled)

!BobOmbNum = $0D		;sprite number for bob-omb (or any other normal sprite)
!BobOmbTile = $CA		;tile used for the above
!BobOmbProp = $07		;palette/properties for tile above, YXPPCCCT format

!CustomBombNum = $AE		;sprite number for the custom bomb (or any other custom sprite)
;Note that this isn't included with the pack so get it here: https://www.smwcentral.net/?p=section&a=details&id=16347
!CustomBombTile = $AD		;tile used for the above
!CustomBombProp = $05		;palette/properties for tile above, YXPPCCCT format

!TIME_TILL_DROP = $48		;interval for spawning sprites
!TIME_TILL_EXPLODE = $40	;if spawning a bob-omb, how long does it take to explode (00 to drop it in normal state)
!TIME_TO_PAUSE = $10		;if set to pause when about to spawn sprites, amount of frames to stay in place

TILEMAP:
db $00,$02,$00,$06,$00,$02,$00,$06,$00,$02,$00,$06,$00,$02,$00,$06	;head, body (frame 1), head, body (frame 2), ... - going right
db $00,$02,$00,$06,$00,$02,$00,$06,$00,$02,$00,$06,$00,$02,$00,$06	;head, body (frame 1), head, body (frame 2), ... - going left
                    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    print "INIT ",pc
                    %SubHorzPos()
                    TYA
                    STA !157C,x

LDA !167A,x

if !IsRideable == 0
	AND #$7F
else
	ORA #$80
endif

STA !167A,x


lda $010B
cmp #$77
bne +

lda !1686,x
ora #$08
sta !1686,x
+
                    RTL                 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    print "MAIN ",pc
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    JSR SPRITE_CODE_START   ;  |
                    PLB                     ;  |
                    RTL                     ; /


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_SPEED:            db $10,$F0
KILLED_X_SPEED:     db $F0,$10

RETURN:             RTS
SPRITE_CODE_START:  JSR SPRITE_GRAPHICS       ; graphics routine
                    LDA !14C8,x             ; \ 
                    CMP #$08                ;  | if status != 8, return
                    BNE RETURN              ; /
                    LDA #$00
		    %SubOffScreen()   ; handle off screen situation

lda $010B
cmp #$77
bne +

jsr SprSprContact

+
                    LDA !7FAB10,x    ; only drop if extra bit is set
                    AND #$04
                    BEQ MOVE

                    LDA !1540,x
                    CMP #!TIME_TO_PAUSE
                    BCS MOVE
                    CMP #$00
                    BNE NO_RESET3           ;reset timer if it equals 0
                    LDA #!TIME_TILL_DROP
                    STA !1540,x
NO_RESET3:          CMP #$01
                    BNE DONT_DROP
                    JSR SUB_THROW    ;drop code
DONT_DROP:          STZ !B6,x

                    LDA !7FAB4C,x
                    BNE DONT_MOVE	;if extra byte 2 is non-zero, stop when about to spawn sprite

MOVE:
                    LDY !157C,x             ; \ set x speed based on direction
                    LDA X_SPEED,y           ;  |
                    STA $B6,x               ; /
                    LDA $9D                 ; \ if sprites locked, return
                    BNE RETURN              ; /
DONT_MOVE:
                    STZ !AA,x
                    JSL $01802A|!BankB      ; update position based on speed values
         
                    LDA !1588,x             ; \ if sprite is in contact with an object...
                    AND #$03                ;  |
                    BEQ NO_CONTACT          ;  |
                    LDA !157C,x             ;  | flip the direction status
                    EOR #$01                ;  |
                    STA !157C,x             ; /
NO_CONTACT:         
                    JSL $01A7DC|!BankB      ; check for mario/sprite contact (carry set = contact)

if !IsRideable		;if not rideable we don't insert custom interacton code
                    BCC RETURN_24           ; return if no contact
                    %SubVertPos()	    ; 
                    LDA $0E                 ; \ if mario isn't above sprite, and there's vertical contact...
                    CMP #$E6                ;  |     ... sprite wins
                    BPL SPRITE_WINS         ; /
                    LDA $7D                 ; \if mario speed is upward, return
                    BMI RETURN_24           ; /

if !IsRideable == 2
                    LDA $140D|!Base2        ; \ if mario is spin jumping, goto SPIN_KILL
                    BNE SPIN_KILL           ; /
endif

SPIN_KILL_DISABLED: LDA #$01                ; \ set "on sprite" flag
                    STA $1471|!Base2        ; /
                    LDA #$06                ; \ set riding sprite
                    STA !154C,x             ; / 
                    STZ $7D                 ; y speed = 0
                    LDA #$E2                ; \
                    LDY $187A|!Base2        ;  | mario's y position += E1 or D1 depending if on yoshi
                    BEQ NO_YOSHI            ;  |
                    LDA #$D2                ;  |
NO_YOSHI:           CLC                     ;  |
                    ADC !D8,x               ;  |
                    STA $96                 ;  |
                    LDA !14D4,x             ;  |
                    ADC #$FF                ;  |
                    STA $97                 ; /
                    LDY #$00                ; \ 
                    LDA $1491|!Base2        ;  | $1491 == 01 or FF, depending on direction
                    BPL LABEL9              ;  | set mario's new x position
                    DEY                     ;  |
LABEL9:             CLC                     ;  |
                    ADC $94                 ;  |
                    STA $94                 ;  |
                    TYA                     ;  |
                    ADC $95                 ;  |
                    STA $95                 ; /
RETURN_24B:         RTS                     ;

SPRITE_WINS:        LDA !154C,x             ; \ if riding sprite...
                    ORA !15D0,x             ;  |   ...or sprite being eaten...
                    BNE RETURN_24           ; /   ...return
                    LDA $1490|!Base2        ; \ if mario star timer > 0, goto HAS_STAR 
                    BNE HAS_STAR            ; / NOTE: branch to RETURN_24 to disable star killing                  
                    JSL $00F5B7|!BankB      ; hurt mario
RETURN_24:          RTS                     ; final return

SPIN_KILL:          JSR SUB_STOMP_PTS       ; give mario points
                    JSL $01AA33|!BankB      ; set mario speed
                    JSL $01AB99|!BankB      ; display contact graphic
                    LDA #$04                ; \ status = 4 (being killed by spin jump)
                    STA !14C8,x             ; /   
                    LDA #$1F                ; \ set spin jump animation timer
                    STA !1540,x             ; /
                    JSL $07FC3B|!BankB      ; show star animation
                    LDA #$08                ; \ play sound effect
                    STA $1DF9|!Base2        ; /
                    RTS                     ; return
HAS_STAR:
		    %Star()
endif
		    RTS                     ; final return

SprSprContact:
	LDY #!SprSize-1
Loop:
	LDA !14C8,y
	CMP #$09
	BEQ Process
	CMP #$0A
	BEQ Process
LoopSprSpr:
	DEY
	BPL Loop
NoProc:
	RTS
Process:
	PHX
	TYX
	JSL $03B6E5|!BankB
	PLX
	JSL $03B69F|!BankB
	JSL $03B72B|!BankB
	BCC LoopSprSpr

	LDA #$13
	STA $1DF9
	LDA #$04
	STA !14C8,x
	LDA #$1B
	STA !1540,x
	INC $0F32		;> count this kill

	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; points routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STAR_SOUNDS:        db $00,$13,$14,$15,$16,$17,$18,$19

SUB_STOMP_PTS:      PHY                     ; 
                    LDA $1697|!Base2        ; \
                    CLC                     ;  | 
                    ADC !1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697|!Base2        ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y       ; \ play sound effect
                    STA $1DF9|!Base2        ; /   
NO_SOUND:           TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:           JSL $02ACE5|!BankB      ; give mario points
                    PLY                     ;
                    RTS                     ; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; hammer routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_OFFSET:
db $0A,$07

!Y_OFFSET = $0C

SUB_THROW:
                    LDA !157C,x
                    TAY
                    LDA X_OFFSET,y
                    STA $00
                    LDA #!Y_OFFSET
                    STA $01
		    STZ $02
		    STZ $03

		    LDA !7FAB40,x
		    BNE SpawnCustom		;if extra byte 1 is non-zero, spawn a custom sprite

		    LDA #!BobOmbNum
		    CLC
		    BRA Spawn

SpawnCustom:
		    LDA #!CustomBombNum
		    SEC

Spawn:
		    %SpawnSprite()
		    CPY #$FF
		    BEQ RETURN67

		    LDA !157C,x
		    STA !157C,y

		    LDA !7FAB40,x
		    BNE RETURN67

if !BobOmbNum == $0D && !TIME_TILL_EXPLODE != 0
                    LDA #$09
                    STA !14C8,y
                    LDA #$0C
                    STA !1564,y
                    LDA #!TIME_TILL_EXPLODE
                    STA !1540,y
endif

RETURN67:           RTS                     ; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HORZ_DISP:          db $10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00
                    db $00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10,$00,$10
                    

SPRITE_GRAPHICS:    %GetDrawInfo()      ; sets y = OAM offset                    
                    PHX

		    LDA !7FAB40,x
		    BEQ +

LDA #!CustomBombTile
XBA
LDA #!CustomBombProp
BRA ++

+
LDA #!BobOmbTile
XBA
LDA #!BobOmbProp

++
STA $05
XBA
STA $04

                    LDA !157C,x             ; \ $02 = direction
                    STA $02                 ; /
                    
                    LDA !14C8,x
                    CMP #$02
                    BNE NO_STAR
                    LDA !15F6,x
                    ORA #$80
                    STA !15F6,x                    
                    LDA #$00
                    STA $03
                    BRA CHECK_POS
NO_STAR:
                    LDA $14                 ; \ 
                    LSR #3                  ;  |
                    CLC                     ;  |
                    ADC $15E9|!Base2        ;  |
                    AND #$07                ;  |
                    ASL A

CHECK_POS:          PHY
                    LDY $02
                    BEQ STORE03
                    CLC
                    ADC #$10
STORE03:            STA $03                 ; $03 = index to frame start (0 or 1)
                    PLY

                    LDX #$01 
LOOP_START_2:       PHX
                    TXA
                    ORA $03
                    TAX
                    
                    LDA $00                 ; tile x position = sprite x location ($00)
                    CLC
                    ADC HORZ_DISP,x
                    STA $0300|!Base2,y  

                    LDA $01                 ; tile y position = sprite y location ($01)
                    STA $0301|!Base2,y  

                    LDA TILEMAP,x  
                    STA $0302|!Base2,y    

                    LDX $15E9|!Base2
                    LDA !15F6,x             ; tile properties yxppccct, format
                    LDX $02                 ; \ if direction == 0...
                    BNE NO_FLIP             ;  |
                    ORA #$40                ; /    ...flip tile
NO_FLIP:            ORA $64                 ; add in tile priority of level
                    STA $0303|!Base2,y      ; store tile properties

                    PLX
                    INY                     ; \ increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 4 bytes...
                    INY                     ;  |    
                    INY                     ; /    ...so increment 4 times
                    DEX                     ;  | go to next tile of frame and loop
                    BPL LOOP_START_2        ; / 

                    PLX
                    LDA !14C8,x
                    CMP #$08
                    BNE NO_SHOW
                    LDA !1540,x
                    CMP #$20
                    BCS NO_SHOW
                    CMP #$01
                    BCC NO_SHOW
                    BRA SHOW


NO_SHOW:            LDY #$02                ; \ FF, because we wrote to 460
                    LDA #$02                ;  | A = number of tiles drawn - 1
                    BRA FinishDraw 
SHOW:
                    LDA $00                 ; tile x position = sprite x location ($00)
                    PHX
                    LDX $02
                    CLC
                    ADC X_OFFSET,x
                    PLX
                    STA $0300|!Base2,y  

                    LDA $01                 ; tile y position = sprite y location ($01)
                    CLC
                    ADC #!Y_OFFSET
                    STA $0301|!Base2,y  

                    LDA $04
                    STA $0302|!Base2,y    
                    
                    LDA $05
                    PHX
                    LDX $02
                    BNE NO_FLIP2
                    ORA #$40
NO_FLIP2:           PLX
                    ORA $64                 ; add in tile priority of level
                    STA $0303|!Base2,y      

                    LDY #$02                ; \ FF, because we wrote to 460
                    LDA #$03                ;  | A = number of tiles drawn - 1
FinishDraw:
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; return