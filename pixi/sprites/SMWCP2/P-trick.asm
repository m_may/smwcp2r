;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!FrameNum = $C2

                    print "INIT ",pc
					STZ !FrameNum,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $10,$F0

SPRITE_ROUTINE:	  	
					LDA $1588,x
					BIT #$07
					BEQ ++
					LDA $1588,x
					BIT #$03
					BNE +
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					EOR #$FF
					INC
					STA $B6,x
					++
					LDA $9D
					BNE return
					LDA $14C8,x
					CMP #$08
					BNE return
					STZ $00
					STZ !FrameNum,x
					JSR Proximity
					REP #$20
					PHA
					CMP #$0050
					BCS +
					PLA
					PHA
					SEP #$20
					LDA #$01
					STA !FrameNum,x
					STA $00
					REP #$20
					PLA
					PHA
					CMP #$0020
					BCS +
					PLA
					SEP #$20
					LDA #$02
					STA !FrameNum,x
					STA $00
					REP #$20
					PHA
					+
					PLA
					SEP #$20
					
					LDA $00
					CMP #$02
					BCC +
					LDA $1588,x
					BIT #$04
					BEQ +
					LDA #$E0
					STA $AA,x
					PHX
					%SubHorzPos()
					TYX
					LDA XSpeeds,x
					PLX
					STA $B6,x
					+
					JSL $81802A
					JSL $81803A
					return:
					LDA #$00 : %SubOffScreen()
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Proximity:
LDA $14E0,x
XBA
LDA $E4,x
REP #$20
SEC
SBC $D1
BPL +
EOR #$FFFF
INC
+
SEP #$20
RTS

TileNums:
db $00,$00,$02
TileSize:
db $02,$00,$00

TilePointers:
dw Tile1
dw Tile2
dw Tiles3

XOff:
db $00,$00,$08

YOff:
db $00,$F8,$F8

Tile1:
db $A7
Tile2:
db $A9
Tiles3:
db $AB,$98,$99

SUB_GFX:            %GetDrawInfo()      	;Get all the drawing info, duh
					STZ $08
					LDA $14C8,x
					CMP #$08
					BEQ +
					LDA #$80
					STA $08
					LDA #$00
					STA !FrameNum,x
					+
					PHX
					LDA !FrameNum,x
					ASL
					TAX
					REP #$20
					LDA TilePointers,x
					STA $05
					SEP #$20
					TXA
					LSR
					TAX
					LDA TileNums,x
					STA $07
					TAX
					.GFXLoop
					
					LDA $00
					CLC
					ADC XOff,x
					STA $0300,y
					
					LDA $01
					CLC
					ADC YOff,x
					STA $0301,y
					
					PHY
					TXY
					LDA ($05),y
					PLY
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					ORA $08
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA $07
					LDY #$FF
					JSL $81B7B3					;/and then draw em
					EndIt:
					RTS