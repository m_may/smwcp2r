;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Archer Bute
; by Sonikku
; Description: Stands where you place it and shoots arrows. If Mario is above
; this sprite, it uses different frames and aims a bit higher. Like the flying
; Bute, if killed it falls to the ground and disappears in a cloud of smoke.
!PROJECT = $03 ; custom sprite number of ButeArrow.cfg
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    %SubHorzPos()
                    TYA
                    STA !157C,x
                    LDA #$B0
                    STA !1540,x
                    RTL

	            PRINT "MAIN ",pc			
                    PHB
                    PHK				
                    PLB				
                    JSR SPRITE_ROUTINE	
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SPRITE_ROUTINE:
	JSR SUB_GFX
	LDA #$00
	%SubOffScreen()
	LDA !C2,x
	JSL $0086DF|!BankB
	dw STATE0
	dw STATE1

STATE0:
	LDA $9D

	BNE RETURN
	JSL $01802A|!BankB
	%SubVertPos()
	LDA !14C8,x

	CMP #$08

	BNE DEAD
	JSL $018032|!BankB
	JSL $01A7DC|!BankB
	LDA !1528,x
	BEQ NOFIRE
	LDA #$01
	JSL $02ACE5|!BankB
DEAD:	INC !C2,x
	LDA #$D0
	STA !AA,x
RETURN:	RTS
NOFIRE:	LDA !1540,x
	CMP #$80
	BCS NOFRM
	CMP #$10
	BEQ SHOT
	BMI WAIT
	CMP #$18
	BCC RETURN
	BRA CHARGE

CHKBEL:	LDA !D8,x
	SEC
	SBC $96
	STA $0F
	LDA !14D4,x
	SBC $97
	BMI NOPROX
	BNE NOPROX
	LDA $0F
	CMP #$20
NOPROX:	RTS

SHOTFRM:	db $01,$02,$04,$05
CHARGE:
	%SubHorzPos()
	TYA
	STA !157C,x
	LDA #$70
	CMP !1540,x
	BCC CHARGIN
	INC !1570,x

CHARGIN:
	LDA !1570,x
	LSR A
	AND #$01
	TAY
	JSR CHKBEL
	BCC ABO1
	INY
	INY
ABO1:	LDA SHOTFRM,y
	STA !1602,x
	RTS
NOFRM:	STZ !1602,x
	RTS
SHOT:	JSR GENSPRITE
	RTS
WAIT:	CMP #$01
	BCC RESET
	JSR CHKBEL
	BCC ABO2
	LDA #$06
	BRA STORE
ABO2:	LDA #$03
STORE:	STA !1602,x
	RTS
RESET:	LDA #$B0
	STA !1540,x
	RTS
KILLDSPD:	db $08,$F8
KILLDFRM:	db $08,$09
STATE1:	LDA $9D

	BNE RETURN1
	JSL $01802A|!BankB
	LDA !1588,x
	AND #$08
	BEQ NOCEIL
	STZ !AA,x
NOCEIL:	LDA #$18
	STA $1686,x
	LDA #$A1
	STA $167A,x
	LDA $167A,x
	ORA #$04
	STA $167A,x
	LDA #$08
	STA !14C8,x
	STA !154C,x
	STA !1FE2,x
	STZ !1528,x
	LDA !1588,x
	AND #$04
	BEQ INAIR
	STZ !AA,x
	STZ !B6,x
	LDA !1570,x
	LSR A
	LSR A
	AND #$01
	TAY
	LDA KILLDFRM,y
	STA !1602,x
	INC !1594,x
	LDA !1594,x
	CMP #$20
	BCC RETURN1
	INC !1570,x
	LDA !1570,x
	CMP #$50
	BCC RETURN1
	LDA #$04
	STA !14C8,x
	LDA #$1F
	STA !1540,x
	JSL $07FC3B|!BankB
	LDA #$08
	STA $1DF9|!Base2
	RTS
INAIR:	LDY !157C,x
	LDA KILLDSPD,y
	STA !B6,x
	LDA #$07
	STA !1602,x
RETURN1:	RTS
GENSPRITE:
	LDA !15A0,x
	ORA !186C,x
	BNE RTNSPWN
	JSL $02A9DE|!BankB
	BMI RTNSPWN
	LDA #$01
	STA !14C8,y
	PHX
	LDA #!PROJECT
	TYX
	STA !7FAB9E,x
	PLX
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y
	LDA !D8,x
	SEC
	SBC #$03
	STA !D8,y
	LDA !14D4,x
	SBC #$00
	STA !14D4,y
	PHX
	TYX
	JSL $07F7D2|!BankB
	JSL $0187A7|!BankB
	LDA #$08
	STA !7FAB10,x
	PLX
	LDA !157C,x
	STA !157C,y
	JSR CHKBEL
	BCC ABO3
	LDA #$E0
	BRA STRSPD
ABO3:	LDA #$00
STRSPD:	STA !AA,y
	STA !1594,y
	LDA #$30
	STA !1540,y
RTNSPWN:	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:	db $0A,$0C,$0E,$28,$2A,$2C,$2E,$04,$06,$08

SUB_GFX:	    %GetDrawInfo()      ; sets y = OAM offset
		    LDA !1602,x
		    STA $03		 ;  | $03 = index to frame start (0 or 1)
		    LDA !157C,x             ; $02 = sprite direction
		    STA $02
		    PHX		     ; /

		REP #$20
                    LDA $00		 ; \ tile xy position = sprite xy location ($00)
		    STA $0300|!Base2,y	     ; /
		SEP #$20

		    LDA !15F6,x	     ; tile properties xyppccct, format
		    LDX $02		 ; \ if direction == 0...
		    BNE NO_FLIP	     ;  |
		    ORA #$40		; /    ...flip tile
NO_FLIP:	                  ORA $64		 ; add in tile priority of level
		    STA $0303|!Base2,y	     ; STORE tile properties

		    LDX $03		 ; \ STORE tile
		    LDA TILEMAP,x	   ;  |
		    STA $0302|!Base2,y	     ; /

		    PLX		     ; pull, X = sprite index
		    LDY #$02		; \ 460 = 2 (all 16x16 tiles)
		    LDA #$00		;  | A = (number of tiles drawn - 1)
		    JSL $01B7B3|!BankB	     ; / don't draw if offscreen
		    RTS		     ; RETURN