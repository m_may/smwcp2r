;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SWORD Bute
; by Sonikku
; Description: Stands in place until you get near, then it runs a little and
; slashes its SWORD. Like the flying Bute, it falls to the ground and turns
; into a puff of smoke if killed.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    %SubHorzPos()
                    TYA
                    STA !157C,x
                    RTL

	            PRINT "MAIN ",pc			
                    PHB
                    PHK				
                    PLB				
                    JSR SPRITE_ROUTINE	
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
XSPD:	db $18,$E8,$E0,$20
SPRITE_ROUTINE:	JSR SUB_GFX
	LDA #$00
	%SubOffScreen()
	LDA !1588,x
	AND #$08
	BEQ NOCEIL
	STZ !AA,x
NOCEIL:	LDA !C2,x
	BEQ STATE0
	JMP STATE1

STATE0:	LDA $9D

	BNE RETURN
	JSL $01802A|!BankB
	%SubVertPos()
	LDA !14C8,x

	CMP #$08

	BNE DEAD
	JSL $018032|!BankB
	JSL $01A7DC|!BankB
	LDA !1528,x
	BEQ NOFIRE
	LDA #$01
	JSL $02ACE5
DEAD:	INC !C2,x
	LDA #$D0
	STA !AA,x
RETURN:	RTS
NOFIRE:	LDA !1540,x
	BEQ FACEMAR
	CMP #$2B
	BCS SLASH
	BCC NOSLASH
FACEMAR:	%SubHorzPos()
	TYA
	STA !157C,x
	LDA $0F
	CLC

	ADC #$78

	CMP #$F0

	BCS RETURN
	LDA $0F
	CLC

	ADC #$20

	CMP #$40
	BCS RUN
	JSR CHKGND
	BEQ RETURN
	LDA #$D8
	STA !AA,x
	LDA #$40
	STA !1540,x
	LDA #$02
	STA !1602,x
	BRA DOX
RUN:	JSR CHKGND
	BEQ RETURN
	STZ !AA,x
	LDA $14
	LSR A
	LSR A
	AND #$01
	TAY
	STA !1602,x
DOX:	LDY !157C,x
	LDA !1588,x
	AND #$03
	BNE CHKWAL2
	LDA XSPD,y
	STA !B6,x
	RTS
SLASH:	LDA !AA,x
	BPL UP
	LDA #$02
	BRA STORE
UP:	LDA #$03
	BRA STORE
NOSLASH:	JSR CHKGND
	BNE ONGND
	LDA #$04
STORE:	STA !1602,x
	BRA CHKWALL
ONGND:	STZ !1602,x
	STZ !B6,x
CHKWALL:	LDA !1588,x
	AND #$03
	BEQ RETURN2
	LDY !157C,x
CHKWAL2:	INY
	INY
	LDA XSPD,y
	STA !B6,x
CHKGND:	LDA !1588,x
	AND #$04
RETURN2:	RTS
KILLDSPD:	db $08,$F8
KILLDFRM:	db $01,$02
STATE1:	LDA $9D

	BNE RETURN1
	JSL $01802A
	LDA #$18
	STA !1686,x
	LDA #$A1
	STA !167A,x
	LDA !167A,x
	ORA #$04
	STA !167A,x
	LDA #$08
	STA !14C8,x
	STA !154C,x
	STA !1FE2,x
	STZ !1528,x
	LDA !1588,x
	AND #$04
	BEQ INAIR
	STZ !AA,x
	STZ !B6,x
	LDA !1570,x
	LSR A
	LSR A
	AND #$01
	TAY
	LDA KILLDFRM,y
	STA !1602,x
	INC !1594,x
	LDA !1594,x
	CMP #$20
	BCC RETURN1
	INC !1570,x
	LDA !1594,x
	CMP #$50
	BCC RETURN1
	LDA #$04
	STA !14C8,x
	LDA #$1F
	STA !1540,x
	JSL $07FC3B|!BankB
	LDA #$08
	STA $1DF9|!Base2
	RTS
INAIR:	LDY !157C,x
	LDA KILLDSPD,y
	STA !B6,x
	STZ !1602,x
RETURN1:	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:	db $40,$41
		db $43,$44
		db $46,$47
		db $49,$4A
		db $4C,$4A
X_OFFSET:        db $02,$FA,$FA,$02
KILLTIL:		db $04,$06,$08
SUB_GFX:	    %GetDrawInfo()     ; sets y = OAM offset
                    LDA !C2,x
                    BEQ SWORD
		    LDA !1602,x
		    STA $03		 ;  | $03 = index to frame start (0 or 1)
		    LDA !157C,x             ; $02 = sprite direction
		    STA $02
		    PHX		     ; /
		REP #$20
                    LDA $00		 ; \ tile x position = sprite x location ($00)
		    STA $0300|!Base2,y	     ; /
		SEP #$20

                    JSR SETPROP

		    LDX $03		 ; \ STORE tile
		    LDA KILLTIL,x	   ;  |
		    STA $0302|!Base2,y	     ; /

		INY #4

		    PLX		     ; pull, X = sprite index
		    LDY #$02		; \ 460 = 2 (all 16x16 tiles)
		    LDA #$00		;  | A = (number of tiles drawn - 1)
FINISH:		    JSL $01B7B3|!BankB	     ; / don't draw if offscreen
		    RTS		     ; RETURN

SWORD:	LDA !157C,x             ; \ $02 = direction
	ASL A
	STA $02                 ; /     
	LDA !1602,x
	ASL A
	STA $03             
	
	PHX
	LDX #$01
LOOP_START:          PHX
	TXA
	CLC
	ADC $02
	TAX
	LDA X_OFFSET,x
	CLC
	ADC $00                 ; \ tile x position = sprite y location ($01)
	STA $0300|!Base2,y             ; /
	PLX

	LDA $01                 ; \ tile y position = sprite x location ($00)
	STA $0301|!Base2,y             ; /
	
	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA TILEMAP,x
	STA $0302|!Base2,y 
	PLX
	
	PHX
	LDX $15E9|!Base2
        JSR SETPROP
	PLX
	INY	 ; \ increase index to sprite tile map ($300)...
	INY	 ;  |    ...we wrote 1 16x16 tile...
	INY	 ;  |    ...sprite OAM is 8x8...
	INY	 ; /    ...so increment 4 times
	DEX
	BPL LOOP_START
	PLX		     ; pull, X = sprite index
	LDY #$02		; \ 460 = 2 (all 16x16 tiles)
	LDA #$01		;  | A = (number of tiles drawn - 1)
        BRA FINISH
SETPROP:
		    LDA !15F6,x	     ; tile properties xyppccct, format
		    LDX $02		 ; \ if direction == 0...
		    BNE NO_FLIP2	     ;  |
		    ORA #$40		; /    ...flip tile
NO_FLIP2:           ORA $64		 ; add in tile priority of level
		    STA $0303|!Base2,y	     ; STORE tile properties
                    RTS