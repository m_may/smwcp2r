!Interval = $80		;previously #$50, and previously not a define

XSPD:
	db $E0,$20,$E0,$20,$00,$20,$00,$E0
YSPD:
	db $E0,$E0,$20,$20,$20,$00,$E0,$00

TILEMAP:	
	db $22,$26
	db $24,$28

print "MAIN ", pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
print "INIT ", pc
	RTL
	
Run:
	JSR GFX
	LDA #$00
	%SubOffScreen()
	LDA !14C8,x		;If the sprite is dead..
	CMP #$08
	BNE Return		;..return
	LDA $9D
	BNE Return		;Also return if sprites are locked.
	JSR SHOOTING_STARS	
	
	;JSL $01A7DC|!BankB
	;BCC Return
	;JSL $00F5B7|!BankB	;don't think it's fair to hurt the player since it looks decorative like other fg crystals

Return:
	RTS

SHOOTING_STARS:
	LDA !163E,x
	BNE NOSPAWN
	JSR SPAWNM
	LDA #!Interval
	STA !163E,x
NOSPAWN:
	RTS

SPAWNM:
LDY #$07

LOOPFRE:
LDA #$0A
STA $00
STA $01
LDA XSPD,y
STA $02
LDA YSPD,y
STA $03
PHY
LDA #$02
%SpawnExtended()
PLY
DEY
BPL LOOPFRE
RTS

;Sprite Routines
	
XDISP:	
	db $00,$10
	db $00,$10
	
YDISP:	
	db $00,$00
	db $10,$10
	
GFX:
	%GetDrawInfo()
	
	LDA !15F6,x
	STA $04
	
	LDX #$00
	
OAM_Loop:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300|!Base2,y
	
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301|!Base2,y
	
	LDA TILEMAP,x
	STA $0302|!Base2,y
	
	LDA $04
	ORA $64
	STA $0303|!Base2,y
	
	INY
	INY
	INY
	INY
	
	INX
	CPX #$04
	BNE OAM_Loop
	
	LDX $15E9|!Base2
	LDY #$02
	LDA #$03
	JSL $01B7B3|!BankB
	RTS