;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Dolphin (sprites 41-43), by imamelia
;;
;; This is a disassembly of sprites 41-43 in SMW, the dolphins.
;;
;; Uses first extra bit: NO
;; Uses extra property bytes: YES
;;
;; The first extra property byte determines which dolphin this sprite will act like.
;; (Add 41 to it to get the equivalent sprite number.)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedInc:
db $FF,$01,$FF,$01,$00,$00

XSpeedMax:
db $EA,$16,$FA,$06,$00,$00

tiles: db $6B,$6D

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR DolphinMain
PLB

print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Quit:
JSR DolphinGFX
RTS

DolphinMain:

LDA #$05
%SubOffScreen()

LDA !14C8,x
CMP #$08
BNE Quit
LDA $9D
BNE Quit

LDA $13BF|!Base2
CMP #$17
BNE NoAdjust
LDA !166E,x
AND #$EF
STA !166E,x
NoAdjust:

JSR DolphinGFX	;

LDA $9D		; if sprites are locked...
BNE Return0	; return

JSL $01803A|!BankB
JSL $01801A|!BankB	;
JSL $018022|!BankB	;

STA !1528,x	; prevent the player from sliding horizontally

LDA !AA,x	; sprite Y speed
BMI MovingUp	; if the sprite is moving down...
CMP #$3F		; and its Y speed has not reached 3F...
BCS MaxYSpeed	;
MovingUp:	;
INC !AA,x	; increment its Y speed
MaxYSpeed:	;

TXA		; sprite index -> A
EOR $13		;
LSR		; every other frame depending on the sprite index...
BCC NoObjInteract	; don't interact with objects
JSL $019138|!BankB	;
NoObjInteract:	;
LDA !AA,x	; if the sprite Y speed is negative...
BMI FinishMain	;
LDA !164A,x	; or the sprite isn't touching any water...
BEQ FinishMain	; skip down to the interaction
LDA !AA,x	;
SEC		;
SBC #$08		; diminish the sprite Y speed by 8
STA !AA,x	;
BPL NoZeroYSpeed	; and if the result was negative...
STZ !AA,x	; just reset it to 0
NoZeroYSpeed:	;

LDA !151C,x	; if the unidirectional flag is set (the generator sets this)...
BNE SetYSpeed	; don't change direction

LDA !C2,x	;
LSR		; sprite state into carry flag
PHP		;
LDA !7FAB28,x	;
PLP		;
ROL		; sprite type + state
TAY		; into Y
LDA !B6,x	;
CLC		;
ADC XSpeedInc,y	; increment or decrement the sprite's X speed
STA !B6,x		;
CMP XSpeedMax,y	; if it has reached maximum...
BNE FinishMain	;
INC !C2,x	; change the sprite state

SetYSpeed:
LDA !7FAB10,x
AND #$04
BEQ HighJump
LDA #$C8
BRA Done		; and set the sprite Y speed
HighJump:
LDA #$D8		; (Jumps higher when extra bit is set)
Done:
STA !AA,x

FinishMain:

Return0:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DolphinGFX:		
			LDA $14
			AND #$04
			LSR #2
			STA $1626,x
			NoIncrement:
		
			PHY
			LDY #$01
			LDA $B6,x
			BPL LABELTHING
			DEY
			LABELTHING:
			TYA
			ASL #6
			STA $0E
			LDA #$2D
			ORA $0E
			STA $0E
			PLY

			%GetDrawInfo()

			REP #$20
			LDA $00
			STA $0300|!Base2,y
			SEP #$20
			
			LDA !1626,x
			PHX
			TAX
			LDA tiles,x
			PLX
			STA $0302|!Base2,y
	
			LDA $0E
			STA $0303|!Base2,y

			LDY #$02                ; \ we've already set 460 so use FF
			LDA #$00                ; | A = number of tiles drawn - 1
			JSL $01B7B3|!BankB      ; / don't draw if offscreen
QuitGraphics:		RTS                     ; return