;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Splitting Bullet Bill
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!Tile = $A6
Palettes:		db $02,$0E

			!RAM_FrameCounter	= $13
			!RAM_FrameCounterB	= $14
			!RAM_ScreenBndryXLo	= $1A
			!RAM_ScreenBndryYLo	= $1C
			!RAM_MarioDirection	= $76
			!RAM_MarioSpeedX	= $7B
			!RAM_MarioSpeedY	= $7D
			!RAM_MarioXPos		= $94
			!RAM_MarioXPosHi	= $95
			!RAM_MarioYPos		= $96
			!RAM_SpritesLocked	= $9D
			!RAM_SpriteNum		= $9E
			!RAM_SpriteSpeedY	= $AA
			!RAM_SpriteSpeedX	= $B6
			!RAM_SpriteState	= $C2
			!RAM_SpriteYLo		= $D8
			!RAM_SpriteXLo		= $E4
			!OAM_DispX		= $0300
			!OAM_DispY		= $0301
			!OAM_Tile		= $0302
			!OAM_Prop		= $0303
			!OAM_Tile2DispX		= $0304
			!OAM_Tile2DispY		= $0305
			!OAM_Tile2		= $0306
			!OAM_Tile2Prop		= $0307
			!OAM_TileSize		= $0460
			!RAM_RandomByte1	= $148D
			!RAM_RandomByte2	= $148E
			!RAM_KickImgTimer	= $149A
			!RAM_SpriteYHi		= $14D4
			!RAM_SpriteXHi		= $14E0
			!RAM_Reznor1Dead	= $1520
			!RAM_Reznor2Dead	= $1521
			!RAM_Reznor3Dead	= $1522
			!RAM_Reznor4Dead	= $1523
			!RAM_DisableInter	= $154C
			!RAM_SpriteDir		= $157C
			!RAM_SprObjStatus	= $1588
			!RAM_OffscreenHorz	= $15A0
			!RAM_SprOAMIndex	= $15EA
			!RAM_SpritePal		= $15F6
			!RAM_Tweaker1662	= $1662
			!RAM_ExSpriteNum	= $170B
			!RAM_ExSpriteYLo	= $1715
			!RAM_ExSpriteXLo	= $171F
			!RAM_ExSpriteYHi	= $1729
			!RAM_ExSpriteXHi	= $1733
			!RAM_ExSprSpeedY	= $173D
			!RAM_ExSprSpeedX	= $1747
			!RAM_OffscreenVert	= $186C
			!RAM_OnYoshi		= $187A

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "INIT ",pc
			%SubHorzPos()
			TYA
			STA $C2,x
			LDA #$10
			STA $1540,x
			LDA #$40
			STA $1594,x
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR MainCode
			PLB
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeed:			db $20,$E0

MainCode:		JSR SpriteGraphics
			LDA $14C8,x			;\ if sprite status not normal,
			CMP #$08			; |
			BNE .Return			;/ branch
			LDA !RAM_SpritesLocked		;\ if sprites locked,
			BNE .Return			;/ branch
			LDA #$00 : %SubOffScreen()

			LDY $C2,x			;\ set sprite speed depending on direction
			LDA XSpeed,y			; |
			STA !RAM_SpriteSpeedX,x		;/
			JSL $818022			; update sprite position
			JSL $81803A			; interact with Mario

			DEC $1594,x			;\ if time to split,
			BEQ Split			;/ branch

.Return			RTS


Split:			JSR Smoke			; display smoke
			LDA #$01
			STA $1528,x
.LoopStart		JSL $02A9DE			; get empty slot for spawned sprite
			BMI .Return

			LDA #$08			;\ set status of new sprite
			STA $14C8,y			;/
			LDA #$1C			;\ set number of new sprite (bullet bill)
			STA.w !RAM_SpriteNum,y		;/
			LDA !RAM_SpriteXLo,x		;\ set position of new sprite
			STA.w !RAM_SpriteXLo,y		; |
			LDA !RAM_SpriteXHi,x		; |
			STA !RAM_SpriteXHi,y		; |
			LDA !RAM_SpriteYLo,x		; |
			STA.w !RAM_SpriteYLo,y		; |
			LDA !RAM_SpriteYHi,x		; |
			STA !RAM_SpriteYHi,y		; /

			PHX				;\ clear out sprite table values for new sprite
			TYX				; |
			JSL $07F7D2			; |
			PLX				;/

			LDA $C2,x			;\ set direction of new sprite
			ASL				; |
			CLC				; |
			ADC #$04			; |
			CLC
			ADC $1528,x			; |
			STA.w $C2,y			;/

			LDA #$09			;\ play sound effect
			STA $1DFC			;/

			DEC $1528,x			;\ if still sprites left to spawn,
			BPL .LoopStart			;/ branch

.Return			STZ $14C8,x			; erase sprite
			
			RTS


Smoke:
STZ $00
STZ $01
LDA #$1B
STA $02
LDA #$01
%SpawnSmoke()
RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Properties:		db $40,$00

SpriteGraphics:		%GetDrawInfo()

			PHX
			LDA $14
			LSR #2
			AND #$01
			TAX
			LDA Palettes,x
			STA $05
			PLX

			LDA !RAM_SpritePal,x
			AND #$F1
			ORA $05
			STA !RAM_SpritePal,x

			LDA $C2,x
			PHX
			TAX
			LDA Properties,x
			STA $02
			PLX

			LDA $14C8,x
			CMP #$08
			BEQ .NoFlip
			LDA $02
			ORA #$80
			STA $02

.NoFlip			LDA $1540,x
			BNE .LowPriority
			LDA $02
			ORA $64
			STA $02
			BRA .GraphicsMain

.LowPriority		LDA $02
			ORA #$10
			STA $02			

.GraphicsMain		LDA $00
			STA !OAM_DispX,y
			LDA $01
			STA !OAM_DispY,y
			LDA #!Tile
			STA !OAM_Tile,y
			LDA !RAM_SpritePal,x
			ORA $02
			STA !OAM_Prop,y

			LDA #$00
			LDY #$02
			JSL $81B7B3
			RTS