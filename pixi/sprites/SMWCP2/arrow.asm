;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This is an arrow (used for the shooter in the Emerald Escapade level (23)
; Destroys enemies on contact and hurts Mario.
; Makes a sound when it hits something.
;
; Cstutor89
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc
		LDA #$08
		STA !15AC,x
PRINT "MAIN ",pc	
                PHB                     ; \
          	PHK                     ;  | main sprite function, just calls local subroutine
         	PLB                     ;  |
         	JSR S_CODE_START   	;  |
       		PLB                     ;  |
          	RTL                     ; /

S_CODE_START:	JSR SPRITE_GRAPHICS
                LDA $14C8,x             ; \ 
                CMP #$08                ;  | if status != 8, RETURN
                BNE RETURN              ; /
                LDA #$00
		%SubOffScreen()   ; handle off screen situation
                LDA $9D                 ; \ if sprites locked, RETURN
                BNE RETURN              ; /

		LDA $1588,x		; ... if sprite is touching a wall.
		AND #$03		; (bit 0 and 1)..
		BNE DESTROYARROW

		JSL $818022		; update sprite X position without gravity

		LDA !15AC,x
		BNE +
		JSL $819138
+
		JSR KILL

INTERACT:	LDA #$83
		LDY $1490
		BEQ NOTHAVESTAR
		LDA #$01
NOTHAVESTAR:	STA $167A,x
		JSL $81A7DC
		LDA #$83
		STA $167A,x
		BCC INTERACTRET
		JSL $80F5B7

DESTROYARROW:	LDA $1656,x		; \  force sprite
		ORA #%10000000		;  | to disappear
		STA $1656,x		; /  in smoke
		LDA #$02                ; \ status = 4 (being killed by spin jump)
                STA $14C8,x             ; /   
INTERACTRET:
ENDKILLLOOP:
RETURN:         RTS

KILL:		LDY #$0C		; load number of times to go through loop
KILLLOOP:	CPY #$00		; \ zero? if so,
		BEQ ENDKILLLOOP		; / end loop
		DEY			; decrease # of times left+get index
		STX $06			; \  if sprite is
		CPY $06			;  | this sprite
		BEQ KILLLOOP		; /  then ignore it
		LDA $14C8,y		; \  if sprite is not
		CMP #$08		;  | in a "tangible"
		BCC KILLLOOP		; /  mode, don't KILL
		LDA $167A,y		; \  if sprite doesn't
		AND #%00000010		;  | INTERACT with stars/cape/fire/bricks
		BNE KILLLOOP		; /  don't continue
		JSL $83B69F		; \
		PHX			;  | if sprite is
		TYX			;  | not touching
		JSL $83B6E5		;  | this sprite
		PLX			;  | don't continue
		JSL $83B72B		;  |
		BCC KILLLOOP		; /
		LDA #$04		; \ set sprite into
		STA $14C8,y		; / death mode (status=2)
		LDA #$1B
		STA $1540,y
                LDA #$08                ; \ play sound effect
                STA $1DF9               ; /
		BRA DESTROYARROW

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:             db $A6,$C6

SPRITE_GRAPHICS:    %GetDrawInfo()      ; sets y = OAM offset
                    STZ $02                 ; / 
                    LDA $14                 ; \ 
                    LSR A                   ;  |
                    LSR A                   ;  |
                    ;LSR A                   ;  |
                    CLC                     ;  |
                    ADC $15E9               ;  |
                    AND #$01                ;  |
                    STA $03                 ;  | $03 = index to frame start (0 or 1)
                    PHX                     ; /
                    
                    LDA $14C8,x
                    CMP #$02
                    BNE LOOP_START_2
                    STZ $03
                    LDA $15F6,x
                    ORA #$80
                    STA $15F6,x

LOOP_START_2:        LDA $00                 ; \ tile x position = sprite x location ($00)
                    STA $0300,y             ; /

                    LDA $01                 ; \ tile y position = sprite y location ($01)
                    STA $0301,y             ; /

                    LDA $15F6,x             ; tile properties xyppccct, format
                    LDX $02                 ; \ if direction == 0...
                    BNE NO_FLIP             ;  |
                    ORA #$40                ; /    ...flip tile
NO_FLIP:             ORA $64                 ; add in tile priority of level
                    STA $0303,y             ; store tile properties

                    LDX $03                 ; \ store tile
                    LDA TILEMAP,x           ;  |
                    STA $0302,y             ; /

                    INY                     ; \ increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 1 16x16 tile...
                    INY                     ;  |    ...sprite OAM is 8x8...
                    INY                     ; /    ...so increment 4 times

                    PLX                     ; pull, X = sprite index
                    LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
                    LDA #$00                ;  | A = (number of tiles drawn - 1)
                    JSL $81B7B3             ; / don't draw if offscreen
                    RTS                     ; RETURN