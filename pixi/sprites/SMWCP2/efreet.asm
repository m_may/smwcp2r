;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Efreet, by Milk
; Based on the 32x32 Eerie, by Fierce Deity Manuz OW Hacker
; and uses code from imamelia's Piranha/Venus sprites
;
; Originally based on Ersanio's Eerie disassembly
;
; This is a sprite that will travel continuously in one
; direction in a wavy motion and will periodically shoot
; fireballs at Mario.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!Interval = $70		;amount of frames between every fireball spawned

!EXTRA_BITS 		= $7FAB10

	print "INIT ",pc
		PHB			;\Sadly, this is needed
		PHK			; |otherwise the sprite would access
		PLB			;/the Xspeedtable in bank 01 instead of the current

		LDA !EXTRA_BITS,x	; \ 
		AND #$04		;  |If the Extra Bit is set,	
		BEQ Slow		; / use the alternate x-speed table

		%SubHorzPos()
		TYA
		STA $157C,x
		LDA XSpeedtable2,y
		STA $B6,x
		BRA EndInit
Slow:
		%SubHorzPos()		;\
		TYA			; |
		STA $157C,x		; |setup direction
		LDA XSpeedtable,y	; |Store Eerie's speed
		STA $B6,x		;/depending on which side Mario is

EndInit:
		PLB			;\I wonder what this does!
		RTL			;/

	print "MAIN ",pc

		PHB
		PHK
		PLB
		JSR Eerie
		PLB
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Defines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedtable:		db $08,$F8	;X speed table: Right, left
XSpeedtable2:		db $08,$F8	;X speed table: Right, left
YSpeedtable:		db $18,$E8	;Y speed acceleration table: down, up (?)

YAcceleration:		db $01,$FF	;Y Acceleration stuff

TILEMAP:             	db $80,$82,$A0,$A2,$84,$86,$A4,$A6
                    	db $80,$82,$A0,$A2,$84,$86,$A4,$A6
HORIZ_DISP:          	db $F8,$08,$F8,$08,$F8,$08,$F8,$08
                    	db $08,$F8,$08,$F8,$08,$F8,$08,$F8
VERT_DISP:           	db $F8,$F8,$08,$08,$F8,$F8,$08,$08
                    	db $F8,$F8,$08,$08,$F8,$F8,$08,$08
PROPERTIES:          	db $48,$08,$08,$08             ;xyppccct format

FireXOffsetsLo:
db $0F,$FA,$0E,$00,$0E,$FE,$0D,$FE,$FB,$FB,$FE,$FE,$1D,$1D,$1D,$1D
FireXOffsetsHi:
db $00,$FF,$00,$00,$00,$FF,$00,$FF,$FF,$FF,$FF,$FF,$00,$00,$00,$00
FireYOffsetsLo:
db $09,$09,$FF,$FF,$18,$18,$13,$13,$06,$06,$01,$01,$0A,$0A,$01,$01
FireYOffsetsHi:
db $00,$00,$FF,$FF,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
FireXSpeeds:
db $0C,$F4,$0C,$F4
FireXSpeeds2:
db $16,$EA,$16,$EA
FireYSpeeds:
db $06,$06,$FA,$FA

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main Routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Eerie:		
		JSR SUB_GFX		;Handle graphics
		LDA $14C8,x		;\
		CMP #$08		; |Sprite status. If (so) not normal, return
		BNE Label1		;/
		LDA $9D			;\If sprites locked, return
		BNE Label1		;/

		JSR SpitFire

		JSL $818022		;Update X pos without gravity

		LDA $C2,x		;Load... something
		AND #$01		;AND #$01
		TAY			;Use as index for acceleration table
		LDA $AA,x		;\
		CLC			; |Load Y speed
		ADC YAcceleration,y	; |Accelerate it
		STA $AA,x		;/
		CMP YSpeedtable,y	;If it didn't reach the desired Y speed
		BNE Straight		;Continue processing the sprite
		INC $C2,x		;If it reached the desired speed, change Y direction

Straight:	JSL $81801A		;Update Y pos without gravity
		LDA #$00
		%SubOffScreen()

		JSL $81A7DC		;Mario<->sprite interact routine
		BCC Label1
		LDA $0E
		CMP #$D8
		BPL Hurt
		LDA $140D
		BEQ Hurt
Hurt:
		JSL $80F5B7
Label1:		
		
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Spit Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpitFire:
LDA !15AC,x
BEQ DoSpit

Flameless:
	RTS
DoSpit:
	LDA #!Interval
	STA !15AC,x

	JSR FacePlayer

	LDY #$07
ExSpriteLoop:
	LDA $170B,y
	BEQ FoundExSlot
	DEY
	BPL ExSpriteLoop
	RTS

FoundExSlot:

	STY $00

	LDA #$02
	STA $170B,y

	LDA $1510,x
	AND #$03
	ASL
	ASL
	STA $01
	LDA $157C,x
	TSB $01

	LDA $157C,x
	STA $02

	LDA $E4,x
	LDY $01
	CLC
	ADC FireXOffsetsLo,y
	LDY $00
	STA $171F,y
	LDA $14E0,x
	LDY $01
	ADC FireXOffsetsHi,y
	LDY $00
	STA $1733,y

	LDA $D8,x
	LDY $01
	CLC
	ADC FireYOffsetsLo,y
	LDY $00
	STA $1715,y
	LDA $14D4,x
	LDY $01
	ADC FireYOffsetsHi,y
	LDY $00
	STA $1729,y

	LDA !EXTRA_BITS,x	; \ 
	AND #$04		;  |If the Extra Bit is set,	
	BEQ SlowFireSpeeds	; / use the alternate FireXSpeed table

	LDY $02
	LDA FireXSpeeds2,y
	LDY $00
	STA $1747,y
	BRA RetainYSpeeds

SlowFireSpeeds:
	LDY $02
	LDA FireXSpeeds,y
	LDY $00
	STA $1747,y
RetainYSpeeds:
	LDY $02
	LDA FireYSpeeds,y
	LDY $00
	STA $173D,y

	LDA #$FF
	STA $176F,y

	LDA #$06
	STA $1DFC

	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Face Player
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FacePlayer:			;
	%SubVertPos()		;
	TYA			;
	ASL			;
	STA $157C,x		;

	%SubHorzPos()		;
	TYA			; make it face the player
	ORA $157C,x		;
	STA $157C,x		;

	RTS			;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_GFX:	%GetDrawInfo()

		LDA $14                 ;\ 
                LSR A                   ; |
                LSR A                   ; |
                LSR A                   ; |
                CLC                     ; |Change animation's rate
                ADC $15E9               ; |
                AND #$01                ; |
                ASL A                   ; |
                ASL A  			; |
                STA $1602,x		;/


		LDA $1602,x
                    STA $03                 ; | $03 = index to frame start (0 or 4)                   
                   
                    LDA $157C,x             ; \ $02 = sprite direction
                    STA $02                 ; /
                    BNE NO_ADD
                    LDA $03
                    CLC
                    ADC #$08
                    STA $03
NO_ADD:              PHX                     ; push sprite index

                    LDX #$03                ; loop counter = (number of tiles per frame) - 1
LOOP_START_2:        PHX                     ; push current tile number
                    TXA                     ; \ X = index to horizontal displacement
                    ORA $03                 ; / get index of tile (index to first tile of frame + current tile number)
FACING_LEFT:         TAX                     ; \ 
                    
                    LDA $00                 ; \ tile x position = sprite x location ($00)
                    CLC                     ;  |
                    ADC HORIZ_DISP,x        ;  |
                    STA $0300,y             ; /
                    
                    LDA $01                 ; \ tile y position = sprite y location ($01) + tile displacement
                    CLC                     ;  |
                    ADC VERT_DISP,x         ;  |
                    STA $0301,y             ; /
                    
                    LDA TILEMAP,x           ; \ store tile
                    STA $0302,y             ; / 
        
                    LDX $02                 ; \
                    LDA PROPERTIES,x        ;  | get tile properties using sprite direction
                    LDX $15E9               ;  |
                    ORA $15F6,x             ;  | get palette info
                    ORA $64                 ;  | put in level properties
                    STA $0303,y             ; / store tile properties
                    
                    PLX                     ; \ pull, X = current tile of the frame we're drawing
                    INY                     ;  | increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 1 16x16 tile...
                    INY                     ;  |    ...sprite OAM is 8x8...
                    INY                     ;  |    ...so increment 4 times
                    DEX                     ;  | go to next tile of frame and loop
                    BPL LOOP_START_2        ; / 

                    PLX                     ; pull, X = sprite index
                    LDY #$02                ; \ 02, because we didn't write to 460 yet
                    LDA #$03                ;  | A = number of tiles drawn - 1
                    JSL $81B7B3             ; / don't draw if offscreen
                    RTS                     ; return