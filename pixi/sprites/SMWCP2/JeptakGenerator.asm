;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Jeptak Generator
;
;Uses extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    print "INIT ",pc             
                    print "MAIN ",pc                               
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITE_CODE_START:
		
		LDA $1A
		STA $E4,x
		LDA $1B,x
		STA $14E0,x
		LDA $9D
		BNE END

		PHX
		LDX #$FF
Loop:
		INX
		CPX #$0D
		BEQ None
		LDA $7FAB9E,x
		CMP #$31
		BNE Loop
		LDA $14C8,x
		CMP #$08
		BEQ END2
		CMP #$01
		BEQ END2
		BRA Loop

None:
		PLX
		LDA $1504,x
		CMP #$FF
		BEQ SpawnSprite
		LDA $14
		AND #$03
		BNE No
		INC $1504,x
		No:
		RTS
SpawnSprite:
		JSR SpawnCust
		STZ $1504,x

END:
		    RTS

END2:
		PLX
		RTS


;=====================================================;
;Spawn a custom sprite at Mario's position	      ;
;=====================================================;

!Sprite = $31	; Sprite to spawn
!State  = $01	; State of sprite ($14C8,x)

SpawnCust:
JSL $02A9DE
BMI Return
PHX
TYX
LDA #!State
STA $14C8,x
LDA #!Sprite
STA $7FAB9E,x
JSL $07F7D2
JSL $0187A7
LDA #$08
STA $7FAB10,x

;Store it to Mario's position.
LDA $94
STA $E4,x
LDA $95
STA $14E0,x
LDA $96
STA $D8,x
LDA $97
STA $14D4,x
PLX
Return:
RTS