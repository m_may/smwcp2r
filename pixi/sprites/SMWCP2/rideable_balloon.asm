!Timer   = $18	; how long until the sprite pops while mario stands on it
!HowSlow = $03	; how slow the timer will increase, only these values apply;
;; (Fastest) $00 $01 $03 $07 $0F $1F $3F $7F $FF (slowest)
;rideable_balloon
;by smkdan

;DOESN'T USE EXTRA BIT

;balloon that you can step on for a ride

;Tilemaps for SP4

!DIR = $03
!PROPRAM = $04
!PROPINDEX = $05
!XDISPINDEX = $06
!YDISPINDEX = $07
!FRAMEINDEX = $08
!TEMP = $09

XSPD:	db $00,$02,$04,$03,$00,$FE,$FC,$FD
XMAR1:	db $00,$00,$01,$01,$00,$00,$FF,$FF
XMAR2:	db $00,$00,$00,$00,$00,$00,$FF,$FF

COUNT:	db $40,$10,$40,$10
PAL:	db $04,$06,$08,$0A

SPDCMP:	db $FA,$FD
INCDEC:	db $FF,$01

;C2: riding bit


HEIGHTTBL: dw $FFE6,$FFD6,$FFD6
CLIPTBL:	dw $0016,$0026,$0026

PRINT "INIT ",pc
	LDA $E4,x	; save
	STA $1510,x	; initial
	LDA $14E0,x	; positions
	STA $151C,x	; to
	LDA $D8,x	; some
	STA $1570,x	; sprite
	LDA $14D4,x	; ram so it
	STA $157C,x	; can RESPAWN


	LDA $7FAB10,x		;only use custom color if made from generator
	AND #$04
	BEQ NORMPAL

	JSL $81ACF9		;get random
	AND #$03		;only low 2 bits
	TAY			;to be used as index

	LDA $15F6,x		;load properties
	AND #$F1		;all but pal bits

	PHB
	PHK
	PLB
	ORA PAL,y		;set palette bits
	PLB

	STA $15F6,x		;new palette

NORMPAL:
	LDA #$FA		;fixed Yspd
	STA $AA,x		;initial Yspd

	RTL

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL
RESPAWN:
	STZ $1540,x
	LDA $1510,x
	STA $E4,x
	LDA $151C,x
	STA $14E0,x
	LDA $1570,x
	STA $D8,x
	LDA $157C,x
	STA $14D4,x
	STZ $187B,x
	STZ $1528,x

	JSR SUB_IS_OFF_SCREEN
	BNE RETURN_L
	JSR SUB_SMOKE
	LDA #$1E
	STA $1DF9
RETURN_L:
	RTS
Run:	LDA #$00
	%SubOffScreen()
	LDA $1540,x
	BEQ NORMAL
	CMP #$02
	BCC RESPAWN
	RTS
NORMAL:
	lda $1528,x
	cmp.b #$10
	bcc +

	lda $14
	and #$01
	bne gothere

+
	JSR GFX			;draw sprite

gothere:
	STZ $15EA,x
	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_L           
	LDA $9D			;locked sprites?
	BNE RETURN_L
	LDA $15D0,x		;yoshi eating?
	BNE RETURN_L
	JSL $818022
	JSL $81801A

	LDA $7FAB10,x
	AND #$04
	BEQ NOCON
	LDA #$09
	STA $1686,x
	JSL $819138
	LDA $185F		; 18A7 wasn't quite right
	CMP #$30		;;;;;;;;;; this is the type of type it checks for!!
	BEQ pop
NOCON:	LDA $1528,x
	CMP #!Timer
	BCC NOPOP
	JSR NOPOP
pop:	LDA #$19
	STA $1DFC
SPOP:	STZ $185F
	JSR SUB_SMOKE
	LDA #$20
	STA $1540,x
	RTS
NOPOP:
	STZ $C2,x		;reset riding sprite, may get set later

	JSL $81A7DC		;mario interact
	BCC NOINTERACTION	;do nothing if there's no contact
	LDA $1558,x
	BNE NOINTERACTION

	%SubVertPos()
	LDA $0F
	CMP #$E8
	BMI NOINTERACTION
        LDA $7D			;don't interact on rising speed
        BMI NOINTERACTION

;interacting, set positions
	LDA $187B,x
	AND #!HowSlow
	BNE NOINC
	INC $1528,x
NOINC:
	LDA #$01
	STA $C2,x		;set riding sprite for GFX routine

	LDA #$01
	STA $1471		;'riding sprite'
	STZ $7D			;no Y speed for mario

	LDA $77
	AND #$08
	BEQ NORMINT
	LDA #$10
	STA $1558,x

NORMINT:	LDA #$E8
	LDY $187A
	BEQ NOYOSHI
	LDA #$E8-$10
NOYOSHI:	CLC
	ADC $D8,x
	STA $96
	LDA $14D4,x
	ADC #$FF
	STA $97
NOINTERACTION:
;apply speed
	INC $187B,x

NOCNT:	LDA $187B,x
	LSR : LSR : LSR : LSR
	AND #$07
	TAY
	LDA XSPD,y
	STA $B6,x
	LDA $C2,x
	BEQ NOMARMOV

	LDA $187B,x
	AND #$03
	BNE NOMARMOV
	LDA $94
	CLC
	ADC XMAR1,y
	STA $94
	LDA $95
	ADC XMAR2,y
	STA $95

NOMARMOV:
	LDY $C2,x		;load ride bit as index
	LDA SPDCMP,y
	STA $AA,x

RETURN:
	RTS        

SUB_SMOKE:
	JSR SUB_IS_OFF_SCREEN
	BNE NOSMOKE
	LDY #$03
LOOPSMK:	LDA $17C0,y
	BEQ FOUND
	DEY
	BPL LOOPSMK
	RTS
FOUND:	LDA #$01
	STA $17C0,y
	LDA $E4,x
	CLC
	ADC #$08
	STA $17C8,y
	LDA $D8,x
	STA $17C4,y
	LDA #$1B
	STA $17CC,y
NOSMOKE:	RTS
			
;=====

TILEMAP:	db $E0,$E2,$C4,$C6

XDISP:	db $00,$10,$00,$10

YDISP:	db $FE,$FE,$0E,$0E
	db $00,$00,$0E,$0E

GFX:	%GetDrawInfo()
	LDA $C2,x	;stepped on?
	ASL A		;x4
	ASL A
	STA !FRAMEINDEX

	LDA $157C,x	;direction...
	STA !DIR

	LDA $15F6,x	;properties...
	STA !PROPRAM

	LDX #$00	;loop index

OAM_LOOP:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300,y	;xpos

	TXA	
	CLC
	ADC !FRAMEINDEX	;variable YDISP
	PHX
	TAX
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301,y	;ypos
	PLX

	LDA TILEMAP,x
	STA $0302,y	;chr

	LDA !PROPRAM
	ORA $64
	STA $0303,y	;properties

	INY
	INY
	INY
	INY
	INX
	CPX #$04	;4 tiles
	BNE OAM_LOOP

	LDX $15E9		;restore sprite index

	LDY #$02		;16x16 tiles
	LDA #$03		;4 tiles
	JSL $81B7B3

	RTS			;RETURN

SUB_IS_OFF_SCREEN:  LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / RETURN