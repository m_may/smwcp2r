;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Score, by cstutor89
;; Displays the score (points you have obtained from minigames)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TILEMAP: db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5,$4D,$67

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA $7F9A82
BEQ Init
CLC
ADC $7F9A81
STA $7F9A81
LDA #$00
STA $7F9A82
Init:	RTL      

print "MAIN ",pc
Main:	PHB
	PHK
	PLB		
	JSR Run
	PLB
	RTL      				;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Run:	LDA $7FAB10,x
 	AND #$04
	BNE SKIP
 	JSR GFX
	RTS

SKIP:	JSR GFXTAR
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the bp score
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!GamesBits = $7F9A83
!BP = $7F9A81

!YPosBP = $C0 
!YPosGames = !YPosBP+$10

Nums8x8:
db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5	;0-9

Tilemap:
db $EC,$EE		;BP:
db $D8,$DA,$DC,$DE	;GAMES:/5

XPos:
db $08,$18		;BP:
db $08,$18,$28,$48	;GAMES:/5

YPos:
db !YPosBP,!YPosBP
db !YPosGames,!YPosGames,!YPosGames,!YPosGames

CxBPXPos:
db $28,$30		;X-pos for amount of BP

!CxGamesXPos = $40	;X-pos for amount of games played

GFX:
JSR Drawlolo

	LDA !BP
	LDY #$00
LoopX:
	CMP #$0A ; <- If A != 10 ..
	BCC Return
	SBC #$0A ; A - 10.
	INY	; Y + 1.
	BRA LoopX
Return:
	STA !1528,x
	TYA
	STA !1510,x

	PHX
	LDX #$04
	LDY #$00
-	LDA $7F9A83
	AND DoorsWhich,x
	BEQ +
	INY
+	DEX
	BPL -

	TYA
	PLX
	STA !C2,x
	CMP #$03
	BCC ThreeGames
	BEQ ThreeGames
	CMP #$04
	BEQ FourGames

FiveGames:
	LDA $7F9A81
	CMP #$0B
	BCC DoorToHeaven	;survived all 5 games? go on ahead
	BRA NoGo

FourGames:
	LDA $7F9A81
	CMP #$06		;lost 5 or less points after 4 games? go ahead
	BCC DoorToHeaven
	BRA NoGo

ThreeGames:
	LDA $7F9A81
	BEQ DoorToHeaven	;no loss after 3 games? good to go

NoGo:
LDA #$3E
BRA +

DoorToHeaven:
LDA #$3C

+
STA !1FD6,x
RTS

Drawlolo:
LDY !15EA,x

STZ $00			;reset number of tiles drawn

LDA !1FD6,x		;load properties according to bp and games
STA $01			;store to scratch RAM.

LDA !1510,x
STA $02			;ones
LDA !1528,x
STA $03			;tens

LDA !C2,x
STA $05			;games played

PHX

LDX #$05

.loop
LDA XPos,x
STA $0300,y
LDA YPos,x
STA $0301,y
LDA Tilemap,x
STA $0302,y
LDA #$3D
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$02
STA $0460,y
PLY
INY #4
INC $00
DEX
BPL .loop

LDX #$01

.bploop
LDA CxBPXPos,x
STA $0300,y
LDA #!YPosBP+8
STA $0301,y

PHX
LDA $02,x
TAX
LDA Nums8x8,x
STA $0302,y
PLX
LDA $01
ORA $64
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$00
STA $0460,y
PLY
INY #4
INC $00
DEX
BPL .bploop

LDA #!CxGamesXPos
STA $0300,y
LDA #!YPosGames
STA $0301,y
PHX
LDX $05
LDA Nums8x8,x
STA $0302,y
PLX
LDA #$3C
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$00
STA $0460,y
PLY
PLX
RTS

DoorsWhich:	db $01,$02,$04,$08,$10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the target shooting hud
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GFXTAR:
	STZ $0F00	;NO STATUS BAR kthxbai

	LDY !15EA,x

	LDA #$C0
	STA $0300,y	;xpos TARGET
	CLC
	ADC #$0E
	STA $0304,y	;xpos NUM1 TARGET
	CLC
	ADC #$08
	STA $0308,y	;xpos NUM2 TARGET
	CLC
	ADC #$08
	STA $030C,y	;xpos '/'
	CLC
	ADC #$08
	STA $0310,y	;xpos '2'
	CLC
	ADC #$08
	STA $0314,y	;xpos '0'
	LDA #$C0
	STA $0318,y	;xpos BULLET
	CLC
	ADC #$0E
	STA $031C,y	;xpos NUM1 BULLET
	CLC
	ADC #$08
	STA $0320,y	;xpos NUM2 BULLET
	CLC
	ADC #$08
	STA $0324,y	;xpos '/'
	CLC
	ADC #$08
	STA $0328,y	;xpos '2'
	CLC
	ADC #$08
	STA $032C,y	;xpos '5'

	LDA #$28
	STA $0301,y	;ypos TARGET
	STA $0305,y	;ypos NUM1 TARGET
	STA $0309,y	;ypos NUM2 TARGET
	STA $030D,y	;ypos '/'
	STA $0311,y	;ypos '2'
	STA $0315,y	;ypos '0'
	LDA #$30
	STA $0319,y	;ypos BULLET
	STA $031D,y	;ypos NUM1 BULLET
	STA $0321,y	;ypos NUM2 BULLET
	STA $0325,y	;ypos '/'
	STA $0329,y	;ypos '2'
	STA $032D,y	;ypos '5'

	LDA #$CA
	STA $0302,y
	LDA #$CB
	STA $030E,y
	LDA #$46
	STA $0312,y
	LDA #$76
	STA $0316,y
	LDA #$DA
	STA $031A,y
	LDA #$CB
	STA $0326,y
	LDA #$46
	STA $032A,y
	LDA #$C3
	STA $032E,y

	PHX
	LDA $7F9A90
LOOP3:	TAX
	CMP #$0A
	BCC E_LOOP3
	SEC
	SBC #$0A
	BRA LOOP3
E_LOOP3: LDA TILEMAP,x
	STA $030A,y
	LDX #$00
	LDA $7F9A90
LOOP4:	CMP #$0A
	BCC E_LP4
	SEC
	SBC #$0A
	INX
	BRA LOOP4
E_LP4:	LDA TILEMAP,x
	STA $0306,y
	PLX
	PHX
	LDA $7F9A70
LOOP5:	TAX
	CMP #$0A
	BCC E_LOOP5
	SEC
	SBC #$0A
	BRA LOOP5
E_LOOP5: LDA TILEMAP,x
	STA $0322,y
	LDX #$00
	LDA $7F9A70
LOOP6:	CMP #$0A
	BCC E_LP6
	SEC
	SBC #$0A
	INX
	BRA LOOP6
E_LP6:	LDA TILEMAP,x
	STA $031E,y
	PLX

	LDA #$38
	STA $0303,y	;prop
	STA $0307,y	;prop
	STA $030B,y	;prop
	STA $030F,y	;prop
	STA $0313,y	;prop
	STA $0317,y	;prop
	LDA #$33
	STA $031B,y	;prop
	LDA #$38
	STA $031F,y	;prop
	STA $0323,y	;prop
	STA $0327,y	;prop
	STA $032B,y	;prop
	STA $032F,y	;prop

	tya
	lsr #2
	tay
	lda #$00
	ldx #$0B
-
	sta $0460,y
	iny
	dex
	bpl -

	LDX $15E9	;restore sprite index
	RTS
