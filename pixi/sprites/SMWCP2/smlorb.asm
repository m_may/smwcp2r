;pakkun/yurarin boo projectile by smkdan (optimized by Blind Devil)

!Tilemap = $C7

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
print "INIT ",pc
	RTL

Run:
	LDA #$00
	%SubOffScreen()
	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE Return           
	LDA $9D			;locked sprites?
	BNE Return

	LDA !7FAB10,x		;load extra bits
	AND #$04		;check if first extra bit is set
	BNE NoGravity		;if yes, update speeds without gravity

	JSL $01802A|!BankB	;regular speed update
	BRA +

NoGravity:
	JSL $01801A|!BankB	;update ypos no gravity
	JSL $018022|!BankB	;update xpos no gravity

+
	JSL $01A7DC|!BankB	;mario interact
Return:
	RTS        
			
;=====

XDISP:	db $00,$08,$00,$08
YDISP:	db $00,$00,$08,$08
PROP:	db $00,$40,$80,$C0

GFX:
	%GetDrawInfo()
	LDA !15F6,x		;set $04 base
	STA $04

	LDX #$00
OAM_Loop:
	LDA $00			;dead simple GFX routine
	CLC
	ADC XDISP,x
	STA $0300|!Base2,y
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301|!Base2,y
	LDA #!Tilemap
	STA $0302|!Base2,y
	LDA $04
	ORA $64
	ORA PROP,x
	STA $0303|!Base2,y

	INY
	INY
	INY
	INY

	INX
	CPX #$04	;4 tiles
	BNE OAM_Loop

	LDX $15E9|!Base2	;restore sprite index
	LDY #$00		;8x8 tiles
        LDA #$03		;4 tile
        JSL $01B7B3|!BankB	;reserve
	RTS