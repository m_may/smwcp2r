;Asteroid Antics collectible by Blind Devil
;Acts very similarly to a goal sphere in Asteroid Antics, while in Opulent Oasis it has a cinematic role,
;by raising in the air, glowing and morphing into a gear that then goes into the machinery and starts moving.

;Uses extra bit.
;clear: cinematic (Opulent Oasis)
;set: collectible (Asteroid Antics)

;Uses extra property byte.
;$00 - Normal
;Otherwise - Fake

incsrc ../../../CommonDefines.asm

!event = $62
!TriesAmount = $03
!TNKLevel = $01BD
!RealSpriteNumber = $4D
!GoalSong = $04
!MaskTile = $EE

Tilemap:
db $80,$A7,$A9,$AB
db $C2,$C4
db $C6,$E6

print "INIT ",pc
LDA !7FAB10,x		;load extra bits
AND #$04		;check if first extra bit is set
BEQ .cinematic		;if not, skip ahead.

LDA !extra_prop_1,x
BEQ .realCollectible

.fakeCollectible
LDA !Collectibles	;load collectible address
AND #$02		;check if already collected
BEQ .notcollected

STZ !14C8,x		;erase sprite.
RTL

.cinematic
LDA #$40		;load value
STA !15AC,x		;store to sprite timer.

LDA #$58		;load value
STA !E4,x		;store to sprite's X-pos, low byte.
LDA #$0D		;load value
STA !14E0,x		;store to sprite's X-pos, high byte.
.notcollected
RTL			;return.

.realCollectible
LDA !Collectibles	;load collectible address
AND #$01		;check if already collected
BEQ .notcollected	;if not collected, process the sprite normally.

STZ !14C8,x		;erase sprite.
INC $1B96|!Base2	;enable side exit

LDY $13BF|!Base2	;load translevel number into Y
LDA $1EA2|!Base2,y	;load OW level setting flags
AND #$BF		;preserve all bits except midway point one
STA $1EA2|!Base2,y	;store result back.

PHX			;preserve sprite index
TYX			;transfer Y to X
%SaveSMWCCoins()
PLX			;restore sprite index
RTL			;return.

%PopCountTable()

print "MAIN ",pc
PHB			;preserve data bank
PHK			;push program bank into stack
PLB			;pull it back as new data bank
JSR SpriteCode		;call sprite code
PLB			;restore original data bank
RTL			;return.

SpriteCode:
LDA !C2,x
CMP #$04  ;if shattered...
BEQ .skipDrawing  ;...yeah, don't draw
JSR Graphics		;call graphics drawing routine
.skipDrawing

LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE Return		;if not equal, return.
LDA $9D			;load sprites/animations locked flag
BNE Return		;if set, return.

LDA !7FAB10,x		;load extra bits
AND #$04		;check if first extra bit is set
BNE Collectible		;if set, run collectible code.

LDA $187A|!Base2	;load riding yoshi flag
BNE +			;if equal, skip ahead.

LDA #$03		;load pose number
STA $13E0|!Base2	;store to player pose address.

+
LDA !C2,x		;load sprite state
JSL $0086DF|!BankB	;call 2-byte pointer subroutine
dw Upwards		;$00: move up
dw Shapeshift		;$01: morph
dw Downwards		;$02: move down
dw Activate		;$03: spin and teleport
dw Shattered		;$04: if fake - shattered and ready to kill

Return:
RTS			;return.

Collectible:
JSR SET_SPARKLE_POS	;display sparkle effect

JSR RegularAnimation	;call routine for regular animation of diamond sprite
JSL $01A7DC|!BankB	;call player/sprite interaction routine
BCC Return		;if there's no contact, return.

STZ !14C8,x		;erase sprite.

LDA !extra_prop_1,x
BNE CollectFake

LDA #$11
TSB !Collectibles	;make permamently collected

LDA #$0B		;load value (set goal animation for normal exit - boss)
STA $1696|!Base2	;store to screen barrier flag/goal flag.

	STZ $1411|!Base2
	STZ $7B
	LDA #$01
	STA $76

	LDA #$07
	STA $0F00|!Base2		;move down all status counters
	LDA #$FF
	STA $0F01|!Base2
	STA $0F02|!Base2		;all status bar timers constantly this value so they are displayed
	STA $0F03|!Base2

	LDA #!GoalSong
	STA $1DFB|!Base2
	LDA #$01
	STA !1534,x
RTS			;return.

CollectFake:
LDA #$22
TSB !Collectibles  ;make permamently collected
LDA #$1C
STA $1DFC|!addr  ;dragon coin sfx
LDA #$01
STA !1534,x
RTS

RegularAnimation:
LDA $14			;load effective frame counter
LSR #3			;divide by 2 three times
AND #$03		;preserve bits 0 and 1
STA !1528,x		;store to our tilemap index.
RTS			;return.

Upwards:
JSR SET_SPARKLE_POS	;display sparkle effect
JSR RegularAnimation	;call routine for regular animation of diamond sprite

LDA $14			;load effective frame counter
AND #$01		;preserve bit 0 only
BEQ +			;if not set, skip ahead.

REP #$20		;16-bit A
LDA $1464|!addr		;load layer 1 Y-pos next frame
CMP #$001D		;compare to value
BCC .nomorecamerarise	;if lower, don't rise camera anymore.
DEC $1464|!addr		;decrement layer 1 Y-pos by one
.nomorecamerarise
SEP #$20		;8-bit A

+
LDA !D8,x		;load sprite's Y-pos, low byte
CMP #$80		;compare to value
BCS +			;if higher or equal, skip ahead.

LDA !extra_prop_1,x
BNE .shatterFake 	;shatter if fake

LDA.w !EventsUnlocked+(!event/8)|!addr	;load events unlocked flags
AND #($80>>(!event%8))		;check if event was unlocked
BEQ .shatterReal		;shatter if not unlocked.

INC !C2,x		;increment state by one
LDA #$90		;load amount of frames
STA !15AC,x		;store to sprite timer.
LDA #$01		;load value
STA $7FC0F8		;store to one shot trigger address 1.
RTS			;return.

.shatterFake
LDA #$02
BRA .shatter
.shatterReal
LDA #$01
.shatter
TRB !Collectibles	;remove from player's inventory
LDA !D8,x		;load sprite's Y-pos, low byte
STA $98			;shatter particles' Y-pos, low byte
LDA !14D4,x		;load sprite's Y-pos, high byte
STA $99			;shatter particles' Y-pos, high byte
LDA !E4,x		;load sprite's X-pos, low byte
STA $9A			;shatter particles' X-pos, low byte
LDA !14E0,x		;load sprite's X-pos, high byte
STA $9B			;shatter particles' X-pos, high byte
PHB
LDA #$02
PHA
PLB
;LDA #$01		;make them colorful
JSL $028663|!bank	;spawn shatter particles
PLB
LDA #$04  ;shattered sprite state
STA !C2,x
LDA #$30     ;\
STA !15AC,x  ;/ wait this much until killing the player
RTS

+
LDA #$F8		;load value
STA !AA,x		;store to sprite's Y speed.

updpos:
JSL $01801A|!BankB	;update Y-pos based on speed (no gravity and object interaction)
RTS			;return.

Shapeshift:
LDA !15AC,x		;load sprite timer
BNE +			;if not zero, keep state running.

INC !C2,x		;increment state by one
LDA #$48		;load amount of frames
STA !15AC,x		;store to sprite timer.
RTS			;return.

+
CMP #$50		;compare to value
BCS +			;if higher or equal, skip ahead.

LDA #$06		;load tilemap index value
BRA .storetile		;branch to store it

+
CMP #$58		;compare to value
BCS +			;if higher or equal, skip ahead.

LDA #$05		;load tilemap index value
BRA .storetile		;branch to store it

+
CMP #$60		;compare to value
BCC .skipRegularAnimation
JMP RegularAnimation	;if higher or equal, use regular animation.
.skipRegularAnimation

LDA #$04		;load tilemap index value

.storetile
STA !1528,x		;store to our tilemap index.
RTS			;return.

Downwards:
LDA !D8,x		;load sprite's X-pos, low byte
CMP #$BC		;compare to value
BCC .moveit		;if lower, keep moving down.

LDA !15AC,x		;load sprite timer
BNE .nochange		;if not zero, don't change state.

LDA #$FF		;load amount of frames
STA !15AC,x		;store to sprite timer.
INC !C2,x		;increment state by one
RTS			;return.

.moveit
LDA #$10		;load value
STA !AA,x		;store to sprite's Y speed.
BRA updpos		;update positions

.nochange
RTS			;return.

Activate:
LDA $14			;load effective frame counter
LSR			;divide by 2
STA $02			;store to scratch RAM.
LSR			;divide by 2
STA $01			;store to scratch RAM.
LSR			;divide by 2
STA $00			;store to scratch RAM.

LDA !15AC,x		;load sprite timer
BEQ Finale		;if equal, do the teleport.
CMP #$60		;compare to value
BCS +			;if higher or equal, branch.

LDA $1887|!Base2	;load shake ground timer
BNE .noshake		;if not zero, skip ahead.

LDA #$21		;load SFX number
STA $1DF9|!Base2	;store to address to play it.
LDA #$02		;load value
STA $7FC0F8		;store to one shot trigger address 1.
LDA #$FF		;load amount of frames
STA $1887|!Base2	;store to shake ground timer.
STA $1DFB|!Base2	;store to music address to fade it out.

.noshake
LDY #$02		;load value into Y
BRA .settile		;branch to set tile

+
CMP #$90		;compare to value
BCS +			;if higher or equal, branch.

LDY #$01		;load value into Y
BRA .settile		;branch to set tile

+
CMP #$C0		;compare to value
BCS +			;if higher or equal, branch.

LDY #$00		;load value into Y

.settile
PHX			;preserve sprite index
TYX			;transfer Y to X
LDA $00,x		;load value from scratch RAM according to index
AND #$01		;preserve bit 0 only
CLC			;clear carry
ADC #$06		;add value
PLX			;restore sprite index
STA !1528,x		;store to our tilemap index.

+
RTS			;return.

Finale:
STZ $0DC1|!Base2

LDA #$06
STA $71
STZ $89
STZ $88

LDA #$04
STA $212C
STA $212D
RTS

Shattered:
LDA !15AC,x
BNE +
LDA !Collectibles
AND #$01  ;if you happen to have a real one
BNE SavingGrace  ;redo the cinematic with that one
LDA !TNKTries
INC
STA !TNKTries
CMP #!TriesAmount
BCS TNKTNKTNK
JSL $00F606|!bank  ;kill the player
+
RTS

TNKTNKTNK:
LDA #$2A
STA $1DFC|!addr		;when you hear this you know you have messed up

PHX
.teleport
	STZ $88
	STZ $89

	SEP #$20

	if !EXLEVEL
		JSL $03BCDC|!bank
	else
		LDX $95
		PHA
		LDA $5B
		LSR
		PLA
		BCC +
		LDX $97
	+
	endif
	LDA.b #!TNKLevel
	STA $19B8|!addr,x
	LDA.b #(!TNKLevel>>8)|$04
	STA $19D8|!addr,x

	LDA #$06
	STA $71
PLX
RTS

SavingGrace:
;change into the real one
LDA #!RealSpriteNumber
STA !9E,x
STA !7FAB9E,x
JSL $07F722|!bank
JSL $0187A7|!bank
LDA #$08
STA !7FAB10,x
LDA #$01
STA !14C8,x
;move to player
	LDA $96
	CLC
	ADC #$10
	STA !D8,x
	LDA $97
	ADC #$00
	STA !14D4,x
	LDA $94
	STA !E4,x
	LDA $95
	STA !14E0,x
RTS


SET_SPARKLE_POS:
		    LDA $13                 ; \ if the frame is set...
		    AND #$1F                ; /
		    ORA !186C,x             ; or the vertical sprite off screen flag is set...
		    BNE RETURN2             ; return
		    JSL $01ACF9|!BankB      ; \ set random routine
		    AND #$0F                ; /
		    CLC                     ;
		    LDY #$00                ; \ set x location (low and high byte)
		    ADC #$FC                ;  |
		    BPL SKIP                ;  |
		    DEY                     ;  |
SKIP:		    CLC                     ;  |
		    ADC !E4,x               ;  |
		    STA $02                 ;  |
		    TYA                     ;  |
		    ADC !14E0,x             ; /
		    PHA                     ; 
		    LDA $02                 ; \ set position in the screen
		    CMP $1A                 ;  |
		    PLA                     ;  |
		    SBC $1B                 ; /
		    BNE RETURN2             ; 
		    LDA $148E|!Base2        ; \ set y location (low byte) 
		    AND #$0F                ;  |
		    CLC                     ;  |
		    ADC #$FE                ;  |
		    ADC !D8,x               ;  |
		    STA $00                 ; /
		    LDA !14D4,x             ; \ set y location (high byte)
		    ADC #$00                ;  |
		    STA $01                 ; /
                    JSR DISPLAY_SPARKLE     ; display sparkle routine
RETURN2:	    RTS                     ; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; display sparkle routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
DISPLAY_SPARKLE:    LDY #$0B                ; \ find a free slot to display effect
FINDFREE:           LDA $17F0|!Base2,y      ;  |
                    BEQ FOUNDONE            ;  |
                    DEY                     ;  |
                    BPL FINDFREE            ;  |
                    RTS                     ; / return if no slots open

FOUNDONE:           LDA #$05                ; \ set effect graphic to sparkle graphic
                    STA $17F0|!Base2,y      ; /
                    LDA #$00                ; \ set time to show sparkle
                    STA $1820|!Base2,y      ; /
                    LDA $00                 ; \ sparkle y position
                    STA $17FC|!Base2,y      ; /
                    LDA $02                 ; \ sparkle x position
                    STA $1808|!Base2,y      ; /
                    LDA #$17                ; \ load generator x position and store it for later
                    STA $1850|!Base2,y      ; /
                    RTS                     ; return

Graphics:

%GetDrawInfo()		;get OAM slot index and sprite coordinates on screen

STZ $02			;reset scratch RAM - tiles drawn

LDA !C2,x		;load sprite state
CMP #$02		;compare to value
BCC +			;if lower, skip ahead.

JSR DrawMask		;draw mask tile

+
REP #$20		;16-bit A
LDA $00			;load XY-pos within the screen
STA $0300|!Base2,y	;store to OAM.
SEP #$20		;8-bit A

PHX			;preserve sprite index
LDA !1528,x		;load tilemap index
TAX			;transfer to X
LDA Tilemap,x		;load tile number from table according to index
STA $0302|!Base2,y	;store to OAM.
PLX			;restore sprite index

LDA !15F6,x		;load palette/properties from CFG
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

INC $02			;increment number of tiles drawn

LDY #$02		;load tile size (16x16)
LDA $02			;load amount of tiles drawn
DEC			;minus one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.

DrawMask:
LDA $00			;load X-pos within the screen
STA $0300|!Base2,y	;store to OAM.
LDA #$A3		;load Y-pos
CLC			;clear carry
ADC $1888|!Base2	;add layer 1 shaking index
STA $0301|!Base2,y	;store to OAM.
LDA #!MaskTile		;load mask tile number
STA $0302|!Base2,y	;store to OAM.
LDA #$01		;load palette/properties value
STA $0303|!Base2,y	;store to OAM
INC $02			;increment number of tiles drawn
INY #4			;increment Y four times
RTS			;return.