;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; jumping piraniha plant plus                                    by EternityLarva
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!max_speed = $40        ;00 - max_speed - 7F

!extended_sprite = $0B  ;extended sprite number "fireball_plus.asm"

!reverse_flag = $60     ;00 -> no patch
                        ;?? or ?????? -> reverse flag

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; extra_byte_1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; bit 0 - 2 -> direction
;  00 - up
;  01 - left
;  02 - down
;  03 - right
;
; bit 3 clr -> green color
;       set -> red color
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; extra_byte_2 - initial timer
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 00 - first jump timer - FF
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; extra_bits - fire flag
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; clr - no fireball
; set - spit fireball
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


tile:
db $AE,$AC,$C4,$C5  ;vertical
db $AA,$A8,$EB,$FB  ;horizontal

xpos:
db $00,$00,$08 ;up
db $00,$10,$10 ;left
db $00,$00,$08 ;down
db $00,$F8,$F8 ;right

ypos:
db $00,$10,$10 ;up
db $00,$00,$08 ;left
db $00,$F8,$F8 ;down
db $00,$00,$08 ;right

prop:
db $08,$0A,$4A ;up
db $48,$4A,$CA ;left
db $88,$8A,$CA ;down
db $08,$0A,$8A ;right

!mir_low    = !1504
!mir_high   = !1510

!rev_max = $FF-!max_speed

speed:
db $100-!max_speed,$100-!max_speed,!max_speed,!max_speed

speed2:
db $F0,$F0,$10,$10

tabletable:
db $00,$03,$06,$09

size:
db $02,$00,$00

coltable:
db $0A,$08

;init_x:
;dw $0008,$FFFF,$0008,$0001
;init_y:
;dw $FFFF,$0008,$0001,$0008

xspd:
db $10,$F0,$C0,$C0,$10,$F0,$40,$40
yspd:
db $C0,$C0,$10,$F0,$10,$10,$10,$F0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; init
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

	LDA #$00
	STA !sprite_misc_1540,x

	LDA #$02
	AND #$03
	BEQ init_up
	CMP #$01
	BEQ init_left
	CMP #$02
	BEQ init_down

init_right:
	LDA !sprite_y_low,x
	CLC
	ADC #$08
	STA !sprite_y_low,x
	LDA !sprite_y_high,x
	ADC #$00
	STA !sprite_y_high,x
	LDA !sprite_x_low,x
	SEC
	SBC #$01
	STA !sprite_x_low,x
	LDA !sprite_x_high,x
	SBC #$00
	STA !sprite_x_high,x
	BRA label19

init_down:
	LDA !sprite_y_low,x
	SEC
	SBC #$01
	STA !sprite_y_low,x
	LDA !sprite_y_high,x
	SBC #$00
	STA !sprite_y_high,x
	LDA !sprite_x_low,x
	CLC
	ADC #$08
	STA !sprite_x_low,x
	LDA !sprite_x_high,x
	ADC #$00
	STA !sprite_x_high,x
	BRA label20

init_left:
	LDA !sprite_y_low,x
	CLC
	ADC #$08
	STA !sprite_y_low,x
	LDA !sprite_y_high,x
	ADC #$00
	STA !sprite_y_high,x
	LDA !sprite_x_low,x
	CLC
	ADC #$01
	STA !sprite_x_low,x
	LDA !sprite_x_high,x
	ADC #$00
	STA !sprite_x_high,x
label19:
	LDA !sprite_x_high,x
	STA !mir_high,x
	LDA !sprite_x_low,x
	STA !mir_low,x
	RTL

init_up:
	LDA !sprite_y_low,x
	CLC
	ADC #$01
	STA !sprite_y_low,x
	LDA !sprite_y_high,x
	ADC #$00
	STA !sprite_y_high,x
	LDA !sprite_x_low,x
	CLC
	ADC #$08
	STA !sprite_x_low,x
	LDA !sprite_x_high,x
	ADC #$00
	STA !sprite_x_high,x
label20:
	LDA !sprite_y_high,x
	STA !mir_high,x
	LDA !sprite_y_low,x
	STA !mir_low,x
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR JumpingPiranhaMain
	PLB
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

JumpingPiranhaMain:
	LDA $64
	PHA
	LDA #$10
	STA $64
	JSR gfx
	PLA
	STA $64
	LDA #$00
	%SubOffScreen()
	LDA $9D
	BNE return

	LDA !1686,x
	ORA #$80
	STA !1686,x

	LDA #!reverse_flag
	BEQ p
	LDA !reverse_flag
	BNE q
p:
	JSL $01803A|!BankB
	BRA r
q:
	JSR reverse_contact
	JSL $019138|!BankB
r:
	LDA #$02
	AND #$01
	BNE +
	JSL $01801A|!BankB
	BRA ++
+
	JSL $018022|!BankB
++
	LDA !sprite_misc_c2,x
	JSL $0086DF|!BankB

	dw State00
	dw State01
	dw State02

return:
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; code for sprite state 00
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

State00:

	STZ !sprite_speed_y,x
	STZ !sprite_speed_x,x
	LDA !sprite_misc_1540,x
	BNE +

	%SubHorzPos()	;
	LDA #$02
	AND #$04
	BNE If_red
	LDA $0E
	CLC	
	ADC #$1B
	CMP #$37
	BCC +
If_red:
	PHY
	LDA #$02
	AND #$03
	TAY
	LDA speed,y
	STA !sprite_speed_y,x
	STA !sprite_speed_x,x
	PLY
	INC !sprite_misc_c2,x
	STZ !sprite_misc_1602,x
+
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; code for sprite state 01
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

State01:

	LDA #$02
	AND #$02
	BNE label21

	LDA !sprite_speed_y,x
	BEQ +
	BMI +
	CMP #!max_speed
	BCS ++
+
	CLC
	ADC #$02
	STA !sprite_speed_y,x
	STA !sprite_speed_x,x
++
	INC !sprite_misc_1570,x
	LDA !sprite_speed_y,x
	CMP #$F1
	BMI +
	LDA #$50
	STA !sprite_misc_1540,x
	INC !sprite_misc_c2,x
+
	RTS

label21:

	LDA !sprite_speed_y,x
	BPL +
	CMP #!rev_max
	BCC ++
+
	SEC
	SBC #$02
	STA !sprite_speed_y,x
	STA !sprite_speed_x,x

++

	INC !sprite_misc_1570,x
	LDA !sprite_speed_y,x
	CMP #$10
	BPL +
	LDA #$50
	STA !sprite_misc_1540,x
	INC !sprite_misc_c2,x
+
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; code for sprite state 02
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

State02:

	INC !sprite_misc_151c,x
	LDA !sprite_misc_1540,x;
	BNE MaybeSpitFire

NoFire:

	INC !sprite_misc_1570,x
	LDA $14
	AND #$03
	BNE label22

	LDA #$02
	AND #$02
	BNE +

	LDA !sprite_speed_y,x
	CMP #$08
	BPL ++
	INC		;
	BRA ++
+
	LDA !sprite_speed_y,x
	CMP #$F9
	BMI ++
	DEC		;
++
	STA !sprite_speed_y,x
	STA !sprite_speed_x,x

label22:

	JSL $019138|!BankB	; interact with objects

	LDA #$02
	AND #$01
	BEQ +

	LDA !sprite_x_high,x
	STA $00
	LDA !sprite_x_low,x
	STA $01
	BRA ++
+
	LDA !sprite_y_high,x
	STA $00
	LDA !sprite_y_low,x
	STA $01
++
	LDA $00
	CMP !mir_high,x
	BNE +
	LDA $01
	CMP !mir_low,x
	BNE +
	
	STZ !sprite_misc_c2,x
	LDA #$40
	STA !sprite_misc_1540,x

+
	RTS

MaybeSpitFire:

	LDA !extra_bits,x	;
	AND #$04
	BEQ NoFire

	STZ !sprite_misc_1570,x

	LDA !sprite_misc_1540,x
	CMP #$40
	BNE NoFire
	LDA !15A0,x
	ORA !186C,x
	BNE NoFire

	STZ $04
	JSR fireball
	LDA #$01
	STA $04

fireball:
	LDA #$04
	STA $00
	STZ $01
	LDA #$02
	AND #$03
	ASL
	ORA $04
	TAY
	LDA xspd,y
	STA $02
	LDA yspd,y
	STA $03
	LDA #!extended_sprite
	%SpawnExtended()
	LDA #$02
	AND #$03
	STA !extended_behind,y
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Reverse Contact Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
reverse_contact:
	
	JSR ProcessInteract
	BCC return_rev
	LDA $1490|!Base2
	BNE kill_sprite
	LDA $140D|!Base2
	ORA $187A|!Base2
	BEQ hurt
	LDA $7D
	CMP #$10
	BMI hurt
	REP #$20
	LDA $96
	PHA
	SEC
	SBC #$0010
	STA $96
	SEP #$20
	JSL $01AB99|!BankB
	REP #$20
	PLA
	STA $96
	SEP #$20
	JSL $01AA33|!BankB
	LDA #$02
	STA $1DF9|!Base2
	LDA #$10
	STA !sprite_misc_1558,x
return_rev:
	RTS
hurt:
	LDA !sprite_misc_1558,x
	BNE +
	JSL $00F5B7|!BankB
+
	RTS
kill_sprite:
	%Star()
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Process Interactt Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ProcessInteract:
	%SubHorzPos()
	REP #$20
	LDA $0E
	CLC
	ADC #$000C
	CMP #$0018
	BCS return_clr2
	SEP #$20
	%SubVertPos()
	LDA $0F
	CLC
	ADC #$0C
	CMP #$18
	BCS return_clr
	SEC
	RTS
return_clr2:
	SEP #$20
return_clr:
	CLC
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphic routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
gfx:
	%GetDrawInfo()

	PHY
	LDA #$02
	AND #$03
	TAY
	LDA tabletable,y
	STA $03
	PLY

	LDA #$02
	AND #$03
	ASL #2
	AND #$04
	STA $02

	LDA !sprite_misc_151c,x
	AND #$04
	LSR #2
	ORA $02
	STA $04

	LDA !sprite_misc_1570,x
	AND #$08
	LSR #3
	ORA $02
	STA $05
	
	PHY
	LDA #$02
	AND #$04
	LSR #2
	TAY
	LDA coltable,y
	STA $06
	PLY

	PHX
	LDX #$02
-
	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA $00
	CLC
	ADC xpos,x
	STA $0300|!Base2,y
	PLX

	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA $01
	CLC
	ADC ypos,x
	STA $0301|!Base2,y
	PLX

	PHX
	TXA
	BEQ +
	LDA #$02
	ORA $04
	BRA ++
+
	ORA $05
++
	TAX
	LDA tile,x
	STA $0302|!Base2,y
	PLX

	PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA prop,x
	PLX
	
	CPX #$00
	BEQ +
	AND #$F1
	ORA $06
+
	ORA $64
	STA $0303|!Base2,y

	PHX
	LDA size,x
	PHA
	TYA
	LSR #2
	TAX
	PLA
	STA $0460|!Base2,x
	PLX

	INY #4
	DEX
	BPL -
	PLX
	LDY #$FF
	LDA #$02
	JSL $01B7B3|!BankB
	RTS