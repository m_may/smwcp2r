
;;----------------------------------------------------------------------------------------
;; Customizable Constants
;;----------------------------------------------------------------------------------------

                !HIT_POINTS = $03        ; Customizable Hit Point Bar
                !SPIT_TIME  = $00        ; Spit When Dryad's below this HP
		!BLINKING_TIME = $70
		!FOOTBALL_NUM = $1B

;;----------------------------------------------------------------------------------------

PRINT "INIT ",pc
		LDA #!HIT_POINTS 
		STA !1528,x
		LDA #$E0
		STA !AA,x
		LDA #$01
		STA !1FD6,x
		LDA #$FF
		STA !1510,x
		STZ !1564,x
		RTL

PRINT "MAIN ",pc
		PHB         	; \
		PHK         	;  | main sprite function, just calls local subroutine
		PLB        	;  |
		JSR S_START   	;  |
		PLB        	;  |
		RTL    		; /


S_START:	JSR SUB_GFX
		LDA $9D                 ; \ if sprites not locked, RETURN
		BNE NOT_NEXT

		JSR CHECK_HIT
		JSR PIRAN_COLOR
		JSR APPLE_COLOR
		JSR FALL_SPIKES
		JSR SLOW_BERRIES

GO_STATE:	JSR CHOOSE_STATE
		LDA !C2,x
		CMP #$03
		BEQ NOT_NEXT
		LDA !1564,x
		BNE NOT_NEXT
		JSR NEXT_STATE
NOT_NEXT:	RTS

;;--------------------------------------------------------------------------------
;; Next State Routine
;;--------------------------------------------------------------------------------

STATE_MACH:	db $00,$02,$00,$03,$00,$07	;2,3,1,7
STATE_TIMER:	db $81,$41,$81,$FF,$FF,$81,$FF,$21

NEXT_STATE:	LDA !1510,x
		inc
		cmp.b #$06
		bcc +
		lda.b #$00
		+
		STA !1510,x
		TAY
		LDA STATE_MACH,y
		STA !C2,x
		TAY
		LDA STATE_TIMER,y
		STA !1564,x
		RTS

CHOOSE_STATE:	LDA !C2,x
		BNE + : JMP BOBBING : +
		DEC #2				;pillar action was unused since ever so it's effectively removed
		BNE + : JMP FALL_STUFF : +
		DEC
		BNE + : JMP SMASH_WALL : +
		DEC
		BNE + : JMP KILL_DRYAD : +
		DEC
		BNE + : JMP THROW_STUFF : +
		DEC
		BNE + : JMP LESS_THROW : +
		DEC
		BNE + : JMP DELETE_FRUITS : +
RETURN:		RTS

PIRAN_COLOR:	LDY #$0B
COLOR_LOOP:	LDA !9E,y		; \ if the sprite number is
		CMP #$B2		;  | key
		BNE NEXT_LOOP		; /
		LDA #$3F
		STA !15F6,y	; / make it red (if it's gold or red)
NEXT_LOOP:	DEY		;
		BPL COLOR_LOOP	; ...otherwise, loop
		RTS

APPLE_COLOR:	LDY #$0B
COLOR_LOOP_2:	LDA !9E,y		; \ if the sprite number is
		CMP #$1B		;  | key
		BNE NEXT_LOOP_2		; /
		LDA #$3D
		STA !15F6,y	; / make it red (if it's gold or red)
NEXT_LOOP_2:	DEY		;
		BPL COLOR_LOOP_2	; ...otherwise, loop
		RTS

FALL_SPIKES:	PHX
		LDX #$0B 
FALL_LOOP:	LDA !9E,x	
		CMP #$B2
		BNE NO_SPIKE
		STZ !1540,x 
NO_SPIKE:	DEX
		BPL FALL_LOOP
		PLX
		RTS

SLOW_BERRIES:	PHX
		LDX #$0B 
SLOW_LOOP:	LDA !9E,x	
		CMP #$1B
		BNE NO_BERRY		
		LDA !AA,x
		CMP #$80
		BCS NO_BERRY
		CMP #$40
		BCC NO_BERRY
		LSR
		STA !AA,x 
NO_BERRY:	DEX
		BPL SLOW_LOOP
		PLX
		RTS

;;--------------------------------------------------------------------------------
;; Change Route (State 6)
;;--------------------------------------------------------------------------------

LESS_THROW:	LDA !1528,x
		CMP #!SPIT_TIME
		BCC MUNCH
		LDA #$03
		BRA STORE_LESS
MUNCH:		LDA #$02
STORE_LESS:	STA !C2,x
		TAY
		LDA STATE_TIMER,y
		STA !1564,x
		RTS

;;--------------------------------------------------------------------------------
;; Delete Fruits (State 7)
;;--------------------------------------------------------------------------------

DELETE_FRUITS:	LDA !1564,x
		CMP #$20
		BEQ CHARGE
		CMP #$1B
		BEQ REMOVE_APPLES
		RTS

CHARGE:		LDA #$0C
		STA $1DF9|!Base2

inc !1534,x			;"divebomb" flag by blind devil
lda #$01
sta !1FD6,x

		RTS

REMOVE_APPLES:	PHX
		LDX #$07
LOOP_REMOVE:	TXA
		BEQ ELIM_REMOVE
		LDA !14C8,x
		BEQ DEC_X
		LDA !9E,x
		CMP #!FOOTBALL_NUM
		BNE DEC_X
		LDA #$04
		STA !14C8,x
		LDA #$1F
		STA !1540,x

DEC_X:		DEX
		BRA LOOP_REMOVE
ELIM_REMOVE:	PLX
		RTS

;;--------------------------------------------------------------------------------
;; Hit Detection Routine
;;--------------------------------------------------------------------------------

CHECK_HIT:
		JSL $018032|!BankB       ; interact with other sprites               
		JSL $01A7DC|!BankB
		BCC NO_CONTACT
		LDA !154C,x
		ORA !15AC,x
		BNE NO_CONTACT
		JSL $03B69F|!BankB

	%SubVertPos()
	CPY #$00		;compare Y to value (check if player is below sprite)
	BEQ SPRITE_WINS		;if yes, hurt him.
	LDA $0F			;else load low byte difference
	CMP #$F4		;compare to value
	BCS SPRITE_WINS		;if equal or higher, hurt the player.

		LDA $7D                 ; \  if Mario's y speed < 10 ...
		CMP #$08                ;  }   ... sprite will hurt Mario
		BMI SPRITE_WINS         ; /  
		JSL $01AA33|!BankB
		JSL $01AB99|!BankB
		LDA #!BLINKING_TIME
		STA !15AC,x
		LDA #$28
		STA $1DFC
		DEC !1528,x
		LDA !1528,x
		BEQ MAKE_DEAD
NO_CONTACT:	RTS
SPRITE_WINS:	JSL $00F5B7|!BankB
		RTS
MAKE_DEAD:	LDA #$04
		STA !C2,x
		LDA #$FF
		STA !1564,x
		RTS

KILL_DRYAD:	LDA #$10
		STA !154C,x

		LDA !1564,x
		CMP #$FE
		BEQ ELIMALL
		CMP #$80
		BNE ANIMATE_DRYAD
		LDA #$04
		STA !14C8,x             ; Destroy Sprite 
		LDA #$1F
		STA !1540,x
		LDA #$08
		STA $1DF9|!Base2

		LDA #$0B
		STA $1696|!Base2	;uberasm victory flag by blind devil

ANIMATE_DRYAD:	RTS

ELIMALL:	PHX 
		LDX #$07
LOOPALL:	TXA
		BEQ ELIMALLDONE
		LDA !14C8,x 
		BEQ DEC_X4
		LDA #$04
		STA !14C8,x  
		LDA #$1F
		STA !1540,x		;
DEC_X4:		;STZ $170B,x
		DEX
		BRA LOOPALL
ELIMALLDONE:	PLX
		RTS

;;--------------------------------------------------------------------------------
;; BOBBING/WEAVING Motion
;;--------------------------------------------------------------------------------

!MAX_YVELOCITY = $11	;$19
!MIN_YVELOCITY = $F0	;$E8

!DiveDn = $20
!DiveUp = $E0

XSPEED: 	db $E8,$18		; Speeds of Sprite's X Speed

BOBBING:
	LDA #!MAX_YVELOCITY
	STA $00
	LDA #!MIN_YVELOCITY
	STA $01

	LDA !1534,x
	BEQ +

	LDA #!DiveDn
	STA $00
	LDA #!DiveUp
	STA $01

+
		LDA !AA,x
		CLC
		ADC !1FD6,x
		STA !AA,x
		CMP #$80
		BCS DOWN_BOB
		CMP $00
		BCC END_BOB
		LDA #$FF
		STA !1FD6,x
		BRA END_BOB
DOWN_BOB:	CMP $01
		BCS END_BOB
		LDA #$01
		STA !1FD6,x
END_BOB:	JSL $01801A|!BankB	; Apply speed.
WEAVING:	LDA !14E0,x		; Load the current MAX_HIGH into A.
		BNE DONTSTOP		; If not met yet, keep walking.
		LDY !157C,x		
		TYA
		BNE RIGHT_WALK
		LDA !E4,x		; Compare to its current position.
		CMP #$25
		BCC STOP_WALK		; If not met yet, keep walking.
		BRA DONTSTOP
RIGHT_WALK:	LDA !E4,x		; Compare to its current position.
		CMP #$CB
		BCS STOP_WALK
		BRA DONTSTOP
STOP_WALK:	LDA !157C,x		;
		EOR #$01		; Flip sprite's direction to turn around.
		STA !157C,x		;
		LDA #$0B
		STA !C2,x

stz !1534,x	;"divebomb" flag by blind devil

		RTS
DONTSTOP:	LDA XSPEED,y		; Load X speed indexed by Y (done before).
		STA !B6,x		; Store Sprite'x X Speed from XSPEED.
		JSL $018022|!BankB	; Apply speed.
MOVERET:	RTS			; RETURN

;;--------------------------------------------------------------------------------
;; Throw Routine
;;--------------------------------------------------------------------------------

THROW_STUFF:	LDA !1564,x
		CMP #$80
		BNE NO_BERR
		JSR SMALLBERRIES
		JSR SMALLBERRIES
		LDA !1528,x
		CMP #$04
		BCS NO_BERR
		JSR SMALLBERRIES
		JSR SMALLBERRIES
NO_BERR:	RTS

;;--------------------------------------------------------------------------------
;; Throw Small Berries
;;--------------------------------------------------------------------------------

SMALLBERRIES:	JSL $02A9DE|!BankB      ; \ get an index to an unused sprite slot, RETURN if all slots full
                BMI NOROOMSBER          ; / after: Y has index of sprite being generated
		LDA #$08                ; \ set sprite status for new sprite
                STA !14C8,y             ; /
                LDA #$1B                ; \ set sprite number for new sprite
                STA !9E,y		; /

		JSL $01ACF9|!BankB
		LDA $148D
		AND #$7F
		CLC
		ADC #$40
		STA !E4,y
		LDA #$00
		STA !14E0,y            

             	LDA #$40                ; \ set y position for new sprite
             	STA !D8,y               ;  |
            	LDA #$00
		STA !14D4,y             ; /
	
		LDA #$10
		STA !B6,y
		
		LDA #$88
		STA !7FAB10,x
	
            	PHX                     ; \ before: X must have index of sprite being generated
            	TYX                     ;  | routine clears *all* old sprite values...
              	JSL $07F7D2|!BankB      ;  | ...and loads in new values for the 6 main sprite tables
             	PLX                     ; / 
                LDA !157C,x
        	STA !C2,y
NOROOMSBER:	RTS

;;--------------------------------------------------------------------------------
;; Smash Routine
;;--------------------------------------------------------------------------------

SMASH_WALL:	LDA #$1C
		STA $1DF9|!Base2
		LDA #$05
		STA !C2,x
		LDA #$D0
		STA $1887|!Base2
		LDY #$05
		LDA STATE_TIMER,y
		STA !1564,x
SKIP_FREEZE:	RTS

;;--------------------------------------------------------------------------------
;; Fall Routine
;;--------------------------------------------------------------------------------

FALL_STUFF:	LDA !1564,x
		AND #$3F
		BNE SKIP_FREEZE

;;--------------------------------------------------------------------------------
;; Throw Falling PIRANHA
;;--------------------------------------------------------------------------------

PIRANHA:	JSL $02A9DE|!BankB      ; \ get an index to an unused sprite slot, RETURN if all slots full
                BMI NOROOMPIRA          ; / after: Y has index of sprite being generated
		LDA #$08                ; \ set sprite status for new sprite
                STA !14C8,y             ; /
                LDA #$B2                ; \ set sprite number for new sprite
                STA !9E,y		; /

		;LDA $7E
		;JSL $01ACF9
		;LDA $148D
		LDA $7E
		STA !E4,y
		LDA #$00
		STA !14E0,y            

             	LDA #$B0                ; \ set y position for new sprite
             	STA !D8,y               ;  |
            	LDA #$00
		STA !14D4,y             ; /
	
		LDA !15F6,x
		STA !15F6,y
		
		LDA #$10
		STA !AA,y
	
		LDA #$88
		STA !7FAB10,x
	
            	PHX                     ; \ before: X must have index of sprite being generated
            	TYX                     ;  | routine clears *all* old sprite values...
              	JSL $07F7D2|!BankB      ;  | ...and loads in new values for the 6 main sprite tables
             	PLX                     ; / 
NOROOMPIRA:	RTS

;;--------------------------------------------------------------------------------
;; Graphics Routine
;;--------------------------------------------------------------------------------

NORM_TILE:	db $44,$46,$64,$66,$00,$02
		db $C2,$C4,$E2,$E4,$08,$0A
		db $88,$8E,$A8,$AE,$24,$26
		db $82,$84,$A2,$A4,$08,$0A

SUMM_TILE:	db $0C,$0E,$2C,$2E,$04,$06
PIRA_TILE:	db $48,$4A,$68,$6A,$24,$26
DIED_TILE:	db $4C,$4E,$6C,$6E,$08,$0A

ALL_XPOS:	db $F8,$08,$F8,$08,$F8,$08
ALL_YPOS:	db $00,$00,$10,$10,$20,$20
PROP_TILE:	db $37,$37,$37,$37,$37,$37
TWIRL_NUM:	db $00,$06,$0C,$12

SUB_GFX:	LDA !C2,x
		STA $06
		LDA !15AC,x
		BEQ GET_DRAW
		AND #$03
		BNE NOROOMPIRA
GET_DRAW:	%GetDrawInfo()       ; sets y = OAM offset
GET_BOTTOM:	PHX
		LDX #$05
SUB_LOOP:	LDA $00
		CLC
		ADC ALL_XPOS,x
		STA $0300|!Base2,y	

		LDA $01
		CLC
		ADC ALL_YPOS,x
		STA $0301|!Base2,y

		JSR DRAW_TILES

		LDA $06
		CMP #$04
		BEQ DEAD_COLOR1
		LDA PROP_TILE,x
		BRA STORE1_Y
DEAD_COLOR1:	LDA #$39	
STORE1_Y:	STA $0303|!Base2,y
		
		INY
		INY
		INY
		INY
		DEX

		BPL SUB_LOOP
		PLX
		LDY #$02
		LDA #$05
		JSL $01B7B3		; Call the routine that draws the sprite.
		RTS			; Never forget this!

DRAW_TILES:	LDA $06
		BEQ TWIRL_TILES
		DEC
		BEQ BRAM_TILES
		DEC
		BEQ FALL_TILES	
		DEC #2
		BEQ DEAD_TILES
		DEC
		BEQ THROW_TILES

IDLE_TILES:	LDA NORM_TILE,x
		BRA +

TWIRL_TILES:	PHX
		TXA
		STA $09
		LDA $14
		LSR : LSR : LSR
		AND #$03
		TAX
		LDA TWIRL_NUM,x
		CLC
		ADC $09
		TAX
		LDA NORM_TILE,x
		PLX
		BRA +
		
BRAM_TILES:	LDA SUMM_TILE,x
		BRA +

FALL_TILES:	LDA PIRA_TILE,x
		BRA +

DEAD_TILES:	LDA DIED_TILE,x
		BRA +

THROW_TILES:	PHX
		TXA
		STA $09
		LDA $14
		LSR
		AND #$03
		TAX
		LDA TWIRL_NUM,x
		CLC
		ADC $09
		TAX
		LDA NORM_TILE,x
		PLX
+
		STA $0302|!Base2,y
		RTS