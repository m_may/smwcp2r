;Air mushroom, by Blind Devil...
;...because the other guys FAILED to make a real simple collectible

!TILE = $24
!AirRAM = $0F08|!Base2

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
print "INIT ",pc
RTL

SpriteCode:
JSR Graphics

LDA !14C8,x
CMP #$08
BNE Return
LDA $9D
BNE Return

LDA #$00
%SubOffScreen()

JSL $01A7DC|!BankB
BCC Return

STZ $00
STZ $01
LDA #$1B
STA $02
LDA #$05
%SpawnSmoke()

LDA #$FF
STA !AirRAM

LDA #$15
STA $1DFC|!Base2
STZ !14C8,x

Return:
RTS

Graphics:
%GetDrawInfo()

REP #$20
LDA $00
STA $0300|!Base2,y
SEP #$20

LDA #!TILE
STA $0302|!Base2,y

LDA !15F6,x
ORA $64
STA $0303|!Base2,y

LDY #$02
LDA #$00
JSL $01B7B3|!BankB
RTS