;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fire Chomp
;; Coded by SMWEdit
;; based on mikeyk's boo disassembly
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!FLAMESPRNUM		= $93	; set this to the sprite you inserted the flame at (YOU MUST CHANGE THIS!)

		!FIRESFX		= $27	; sound effect for fire throwing
		!EXPLODESFX		= $09	; sound effect for explosion

		!TIMEBETWEENSPITS 	= $C0	; time before fire chomp spits a flame
		!EXPLODEWAIT		= $40	; time after all flames thrown to flash before explosion
		!EXPLOSIONDURATION	= $30	; how long the explosion lasts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; don't change these:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!SPITTIME		= $20

		!FreeRAM		= $7F9000
		!FreeRAM_SA1		= $40C400

		!SPRITE_Y_SPEED	= !AA
		!SPRITE_X_SPEED	= !B6
		!SPRITE_STATUS	= !14C8
		!SPRITE_STATE		= !C2
		!SPRITE_OAM		= !15EA
		!SPRITE_DIR		= !157C

		!GFX_INDEX		= !1504
		!SPIT_COUNT		= !1528
		!SPIT_TIMER		= !163E
		!EXPLODEWAITFLAG	= !1534
		!DEATHINDEX		= !1570

		!TMP1			= $00
		!TMP2			= $01
		!FC_TEMP1		= $00	;\
		!FC_TEMP2		= $02	; | will use 2 bytes
		!FC_TEMP3		= $04	; |
		!FC_TEMP4		= $06	;/
		!XFLIP			= $02
		!YFLIP			= $03
		!TILESDRAWN		= $04
		!GFXTMP1		= $05
		!GFXTMP2		= $06

		if !SA1
			!FreeRAM := !FreeRAM_SA1
		endif

		macro DefTable(num)
			!XLO_<num> = !FreeRAM+(!SprSize*(<num>+32))
			!YLO_<num> = !FreeRAM+(!SprSize*(<num>-1))
		endmacro

		!i	= 1
		while !i < 33
			%DefTable(!i)
			!i #= !i+1
		endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		print "INIT ",pc
		JSL $01ACF9|!BankB
		STA !DEATHINDEX,x
		LDA #!TIMEBETWEENSPITS	;\  start
		STA !SPIT_TIMER,x		;/  timer
		LDA !D8,x
		STA !YLO_1,x
		STA !YLO_2,x
		STA !YLO_3,x
		STA !YLO_4,x
		STA !YLO_5,x
		STA !YLO_6,x
		STA !YLO_7,x
		STA !YLO_8,x
		STA !YLO_9,x
		STA !YLO_10,x
		STA !YLO_11,x
		STA !YLO_12,x
		STA !YLO_13,x
		STA !YLO_14,x
		STA !YLO_15,x
		STA !YLO_16,x
		STA !YLO_17,x
		STA !YLO_18,x
		STA !YLO_19,x
		STA !YLO_20,x
		STA !YLO_21,x
		STA !YLO_22,x
		STA !YLO_23,x
		STA !YLO_24,x
		STA !YLO_25,x
		STA !YLO_26,x
		STA !YLO_27,x
		STA !YLO_28,x
		STA !YLO_29,x
		STA !YLO_30,x
		STA !YLO_31,x
		STA !YLO_32,x
		LDA !E4,x
		STA !XLO_1,x
		STA !XLO_2,x
		STA !XLO_3,x
		STA !XLO_4,x
		STA !XLO_5,x
		STA !XLO_6,x
		STA !XLO_7,x
		STA !XLO_8,x
		STA !XLO_9,x
		STA !XLO_10,x
		STA !XLO_11,x
		STA !XLO_12,x
		STA !XLO_13,x
		STA !XLO_14,x
		STA !XLO_15,x
		STA !XLO_16,x
		STA !XLO_17,x
		STA !XLO_18,x
		STA !XLO_19,x
		STA !XLO_20,x
		STA !XLO_21,x
		STA !XLO_22,x
		STA !XLO_23,x
		STA !XLO_24,x
		STA !XLO_25,x
		STA !XLO_26,x
		STA !XLO_27,x
		STA !XLO_28,x
		STA !XLO_29,x
		STA !XLO_30,x
		STA !XLO_31,x
		STA !XLO_32,x
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		print "MAIN ",pc
		PHB
		PHK
		PLB
		JSR SPRITE_CODE_START
		PLB
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPITGFXINDEXES:
	db $01,$01,$01,$01,$01,$01,$01,$01
	db $02,$02,$02,$02,$02,$02,$02,$02
	db $02,$02,$02,$02,$02,$02,$02,$02
	db $01,$01,$01,$01,$01,$01,$01,$01

FLASHPALS:
	db $02,$08				;   (palette - 8, * 2)

DEATHX:
	db $08,$10,$18,$18,$10,$08
	db $F8,$F0,$E8,$E8,$F8,$F8

T_EBB4:
	db $02,$FE				;   rate of speed change
T_F8CF:
	db $10,$F0				;   max speed

ISDYING:
	LDA #$18		;\ speed of
	STA !AA,x		;/ moving down
	INC !DEATHINDEX,x	; next index for speed
	LDA !DEATHINDEX,x	;\
	CMP #$0C		; | wrap at 12
	BCC GETSPD		; | and load A
	LDA #$00		; | with index
	STA !DEATHINDEX,x	;/
GETSPD:
	TAY			;   index -> Y
	LDA DEATHX,y		;   get X speed
	STA !B6,x		;   set X speed
	LDA #$02		;\  set head frame
	STA !GFX_INDEX,x	;/  for death
	LDA !15F6,x		;\
	AND #$F1		; | prevent death with
	ORA #$02		; | the wrong palette
	STA !15F6,x		;/
	JMP LOGPOSITION	;   make fireballs follow
RETURN1:
	RTS

SPRITE_CODE_START:
	JSR SUB_GFX
	LDA $9D		;\  return if
	BNE RETURN1		;/  sprites locked
	LDA !14C8,x		;   get sprite status
	CMP #$03		;\  less than 3
	BCC ISDYING		;/  means dying
	CMP #$08		;\  else != 8 means
	BNE RETURN1		;/  don't process
	LDA #$03
	%SubOffScreen()	;   only process sprite while on screen

	LDA !SPRITE_STATE,x
	BEQ NORMALSTATE
	JMP SPITSTATE		;   else go to "spitting"
NORMALSTATE:
	STZ !GFX_INDEX,x
	PEI ($96)
	LDA $96
	CLC
	ADC #$10
	STA $96
	LDA $97
	ADC #$00
	STA $97
	%BEC(NoAngleChase)

AngleChase:
	LDA $14		;\  only process
	AND #$03		; | speed once every
	BNE ENDSPEEDCHG	;/  four frames

	JSR CALCFRAME		;   get angle
	TAY			;   -> Y
	LDA FIRE_X,y		;\  fire X speeds
	STA !TMP1		;/  used as chase
	LDA FIRE_Y,y		;\  fire Y speeds
	STA !TMP2		;/  used as chase

	%SubHorzPos()		;\
	CPY #$00		; | flip
	BEQ +			; | X if
	LDA !TMP1		; | necessary
	EOR #$FF		; |
	INC			;/
	STA !TMP1
+	TYA			;\  set X
	STA !157C,x		;/  flip

	%SubVertPos()		;\
	CPY #$00		; |
	BEQ +			; | flip
	LDA !TMP2		; | Y if
	EOR #$FF		; | necessary
	INC			; |
	STA !TMP2		;/

+	LDA !B6,x		;\
	SEC			; | move X
	SBC !TMP1		; | speed
	BEQ ENDXCHG		; | towards
	BMI +			; | new X
	DEC !B6,x		; | speed
	BRA ENDXCHG		; |
+	INC !B6,x		;/
ENDXCHG:

	LDA !AA,x		;\
	SEC			; | move Y
	SBC !TMP2		; | speed
	BEQ ENDSPEEDCHG	; | towards
	BMI +			; | new Y
	DEC !AA,x		; | speed
	BRA ENDSPEEDCHG	; |
+	INC !AA,x		;/
ENDSPEEDCHG:
	BRA LBL_16

NoAngleChase:
	%SubHorzPos()
	LDA !1540,x
	BNE +
	LDA #$20
	STA !1540,x
+	LDA $0E
	CLC
	ADC #$0A
	CMP #$14
	BCC LBL_13
	LDA !15AC,x
	BNE +
	TYA
	CMP !SPRITE_DIR,x
	BEQ LBL_13
	LDA #$1F
	STA !15AC,x
+	CMP #$10
	BNE LBL_13

	LDA !SPRITE_DIR,x
	EOR #$01
	STA !SPRITE_DIR,x
LBL_13:
	STZ !1570,x
	LDA $14
	AND #$07
	BNE LBL_16
	
	%SubHorzPos()
	LDA !SPRITE_X_SPEED,x
	CMP T_F8CF,y
	BEQ +
	CLC
	ADC T_EBB4,y
	STA !SPRITE_X_SPEED,x
+	%SubVertPos()
	
	LDA !SPRITE_Y_SPEED,x
	CMP T_F8CF,y
	BEQ LBL_16
	CLC
	ADC T_EBB4,y
	STA !SPRITE_Y_SPEED,x

LBL_16:
	PLA
	STA $96
	PLA
	STA $97

	JSL $018022|!BankB
	JSL $01801A|!BankB
	JSR LOGPOSITION		;   keep logs of where it's been (for fireballs)
	JSL $01A7DC|!BankB
	LDA !EXPLODEWAITFLAG,x	;\  if waiting for explosion
	BNE EXPLODEWAITING		;/  then branch to that code
	LDA !SPIT_TIMER,x		;\  if timer hasn't run out,
	BNE RETURN			;/  then skip next code
	LDA !SPIT_COUNT,x		;\  if all fireballs already
	CMP #$04			; | thrown, then branch to code
	BCS BEGINEXPLODE		;/  to begin explosion wait
	INC !SPRITE_STATE,x		;   sprite state = spitting fireball
	LDA #!SPITTIME			;\   set spit timer (this timer is also
	STA !SPIT_TIMER,x		;/   used as an index, so don't change it)
RETURN:
	RTS

BEGINEXPLODE:
	INC !EXPLODEWAITFLAG,x
	LDA.b #!EXPLODEWAIT		;\ set explosion wait
	STA !SPIT_TIMER,x		;/ time to "spit timer"
	RTS

EXPLODEWAITING:
	LDA !SPIT_TIMER,x	;\  timer ran out"
	BEQ EXPLODE		;/  then explode
	LSR #2			;\
	AND #$01		; |
	TAY			; | cycle between palettes
	LDA !15F6,x		; | when getting
	AND #$F1		; | ready to explode
	ORA FLASHPALS,y	; |
	STA !15F6,x		;/
	RTS

EXPLODE:
	LDA #$0D			;\  turn sprite
	STA !9E,x			;/  into bob-omb
	JSL $07F7D2|!BankB		;   reset sprite tables
	;LDA #$08			;\  sprite status:
	;STA !$14C8,x			;/  normal
	LDA #$01			;\  make it
	STA !1534,x			;/  explode
	LDA #!EXPLOSIONDURATION	;\  set time for
	STA !1540,x			;/  explosion
	LDA #!EXPLODESFX		;\  play sound
	STA $1DFC|!Base2		;/  effect
	RTS

DONESPITTING:
	STZ !SPRITE_STATE,x		;   back to chasing
	LDA #!TIMEBETWEENSPITS	;\ reset
	STA !SPIT_TIMER,x		;/ timer
ENDSPIT1:
	RTS

SPITSTATE:
	INC !1540,x		;   "fight" timer used in normal code
	JSL $01A7DC|!BankB	;   interact w/ mario
	LDY !SPIT_TIMER,x	;\
	LDA SPITGFXINDEXES,y	; | set head frame
	STA !GFX_INDEX,x	;/
	LDA !SPIT_TIMER,x	; \ timer expired?
	BEQ DONESPITTING	; / then done spitting
	CMP #$10		; \ not $10? then skip
	BNE ENDSPIT1		; / fireball generation
	INC !SPIT_COUNT,x	; another fireball thrown
	LDA #!FIRESFX		; \ play
	STA $1DFC|!Base2		; / SFX

	LDA #!FLAMESPRNUM
	SEC
	STZ $00
	STZ $01
	%SpawnSprite()
	CPY #$FF
	BEQ ENDSPIT
	PEI ($96)
	LDA $96
	CLC
	ADC #$10
	STA $96
	LDA $97
	ADC #$00
	STA $97
	PHY			; back up Y
	JSR CALCFRAME		; call "pseudo-arctangent" routine to get angle
	TAY			; -> Y
	LDA FIRE_X,y		; \ set X speed
	STA !TMP1		; / to scratch RAM
	LDA FIRE_Y,y		; \ set Y speed
	STA !TMP2		; / to scratch RAM
	%SubHorzPos()		; \
	CPY #$00		;  |
	BEQ CHKY		;  | flip
	LDA !TMP1		;  | X if
	EOR #$FF		;  | necessary
	INC A			;  |
	STA !TMP1		; /
CHKY:
	%SubVertPos()		;\
	CPY #$00		; |
	BEQ ENDSIGNCHG	; | flip
	LDA !TMP2		; | Y if
	EOR #$FF		; | necessary
	INC A			; |
	STA !TMP2		;/
ENDSIGNCHG:
	PLX			;   load backed up Y
	LDA !TMP1		;\  set
	STA !B6,x		;/  X
	LDA !TMP2		;\  set
	STA !AA,x		;/  Y
	PLA			;\
	STA $96		; | load backed up
	PLA			; | Y position
	STA $97		;/
	LDX $15E9|!Base2
ENDSPIT:
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FIRE_X:
	db $10,$10,$10,$0F
	db $0F,$0E,$0D,$0C
	db $0B,$09,$08,$07
	db $05,$03,$02,$00

FIRE_Y:
	db $00,$02,$03,$05
	db $07,$08,$09,$0B
	db $0C,$0D,$0E,$0F
	db $0F,$10,$10,$10

CALCFRAME:
	LDA !D8,x
	SEC
	SBC $96
	STA !FC_TEMP2
	LDA !14D4,x
	SBC $97
	STA !FC_TEMP2+1
	BNE HORZDIST
	LDA !FC_TEMP2
	BNE HORZDIST
	LDA #$00
	RTS

HORZDIST:
	LDA !E4,x
	SEC
	SBC $94
	STA !FC_TEMP1
	LDA !14E0,x
	SBC $95
	STA !FC_TEMP1+1
	BNE BEGINMATH
	LDA !FC_TEMP1
	BNE BEGINMATH
	LDA #$0F
	RTS

BEGINMATH:
	REP #$20
	LDA !FC_TEMP1
	BPL CHKYDIST
	EOR #$FFFF
	INC
	STA !FC_TEMP1
CHKYDIST:
	LDA !FC_TEMP2
	BPL MULT
	EOR #$FFFF
	INC
	STA !FC_TEMP2
MULT:
	ASL #4
	STA !FC_TEMP3
	LDA !FC_TEMP1
	CLC
	ADC !FC_TEMP2
	STA !FC_TEMP4
	LDY #$00
	LDA !FC_TEMP4
	DEC
DIVLOOP:
	CMP !FC_TEMP3
	BCS END_DIVIDE
	INY
	CLC
	ADC !FC_TEMP4
	BRA DIVLOOP
END_DIVIDE:
	TYA
	SEP #$20
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HEADTILES:	db $46,$48,$4A

FBTILES:		db $24,$26,$24,$26
FBPROPS:		db $00,$00,$C0,$C0

SUB_GFX:
	LDY #$00
	LDA !157C,x		;\  set X
	BEQ +			; |
	LDY #$40		; |
+	STY !XFLIP		;/  flip
	LDY #$00		;\
	LDA !14C8,x		; | Y flip
	CMP #$03		; | depends
	BCS +			; | on death
	LDY #$80		; |
+	STY !YFLIP		;/
	%GetDrawInfo()
	STZ !TILESDRAWN	;   zero tiles drawn so far
	LDA $00		;\  set
	STA $0300|!Base2,y	;/  X
	LDA $01		;\  set
	STA $0301|!Base2,y	;/  Y
	PHY			;\
	LDY !GFX_INDEX,x	; | get tile
	LDA HEADTILES,y	; | number
	PLY			;/
	STA $0302|!Base2,y	;   set tile number
	LDA !15F6,x		;   get palette and GFX page
	ORA !XFLIP		;\  flip
	ORA !YFLIP		;/
	ORA $64		; add in priority bits
	STA $0303|!Base2,y	; set properties
	LDA !SPIT_COUNT,x	;\  if # fireballs thrown
	CMP #$04		; | is less than four, then
	BCC BEGINFIRE		;/  begin drawing fireballs
	JMP FINISHGFX		;   else finish GFX
BEGINFIRE:
	INY #4
	INC !TILESDRAWN	;   a tile was drawn
	LDA $15E9|!Base2	;\
	ASL #2			; | get fireball master tile
	CLC			; | index to
	ADC $14		; | scratch RAM
	STA !GFXTMP1		; /
FIREBALL1:
	LSR #2			;\
	AND #$03		; | index
	STA !GFXTMP2		;/
	LDA $00		;\
	CLC			; | set X
	ADC !XLO_8,x		; | position
	SEC			; |
	SBC !E4,x		; |
	STA $0300|!Base2,y	;/
	LDA $01		;\
	CLC			;  | set Y
	ADC !YLO_8,x		;  | position
	SEC			;  |
	SBC !D8,x		;  |
	STA $0301|!Base2,y	; /
	PHY			; \
	LDY !GFXTMP2		;  | get fireball
	LDA FBTILES,y	;  | tile number
	PLY			; /
	STA $0302|!Base2,y	; set tile number
	LDA #$05		; get palette and GFX page
	PHY			; \
	LDY !GFXTMP2		;  | add in tile
	ORA FBPROPS,y		;  | properties
	PLY			; /
	ORA $64		;   add in priority bits
	STA $0303|!Base2,y	;   set properties
	LDA !SPIT_COUNT,x	;\  if # fireballs thrown
	CMP #$03		; | is less than three, then
	BCC FIREBALL2		;/  draw another fireball
	JMP FINISHGFX		;   else finish GFX
FIREBALL2:
	INY #4			;   next OAM index
	INC !TILESDRAWN	;   a tile was drawn
	LDA !GFXTMP1		;\
	CLC			; | get second
	ADC #$08		; | fireball
	LSR #2			; |
	AND #$03		; |
	STA !GFXTMP2		;/
	LDA $00		;\
	CLC			; | set X
	ADC !XLO_16,x		; | position
	SEC			; |
	SBC !E4,x		; |
	STA $0300|!Base2,y	;/
	LDA $01		;\
	CLC			; | set Y  
	ADC !YLO_16,x		; | position
	SEC			; |
	SBC !D8,x		; |
	STA $0301|!Base2,y	;/
	PHY			;\
	LDY !GFXTMP2		; | get fireball
	LDA FBTILES,y		; | tile number
	PLY			;/
	STA $0302|!Base2,y	;   set tile number
	LDA #$05		;   get palette and GFX page
	PHY			;\
	LDY !GFXTMP2		; | add in tile
	ORA FBPROPS,y		; | properties
	PLY			;/
	ORA $64		;   add in priority bits
	STA $0303|!Base2,y	;   set properties
	LDA !SPIT_COUNT,x	;\  if # fireballs thrown
	CMP #$02		; | is less than two, then
	BCC FIREBALL3		;/  draw another fireball
	JMP FINISHGFX		;   else finish GFX
FIREBALL3:
	INY #4
	INC !TILESDRAWN
	LDA !GFXTMP1		;\
	CLC			; | get third
	ADC #$10		; | fireball
	LSR #2			; |
	AND #$03		; |
	STA !GFXTMP2		;/
	LDA $00		;\
	CLC			; | set X
	ADC !XLO_24,x		; | position
	SEC			; |
	SBC !E4,x		; |
	STA $0300|!Base2,y	;/
	LDA $01		;\
	CLC			; | set Y
	ADC !YLO_24,x		; | position
	SEC			; |
	SBC !D8,x		; |
	STA $0301|!Base2,y	;/
	PHY			;\
	LDY !GFXTMP2		; | get fireball
	LDA FBTILES,y	; | tile number
	PLY			;/
	STA $0302|!Base2,y	;   set tile number
	LDA #$05		;   get palette and GFX page
	PHY			;\
	LDY !GFXTMP2		; | add in tile
	ORA FBPROPS,y		; | properties
	PLY			;/
	ORA $64		;   add in priority bits
	STA $0303|!Base2,y	;   set properties
	LDA !SPIT_COUNT,x	;\  if # fireballs
	CMP #$01		; | thrown >= 1,
	BCS FINISHGFX		;/  then end GFX
FIREBALL4:
	INY #4
	INC !TILESDRAWN
	LDA !GFXTMP1		;\
	CLC			; | get fourth
	ADC #$18		; | fireball
	LSR #2			; |
	AND #%00000011	; |
	STA !GFXTMP2		;/
	LDA $00		;\
	CLC			; | set X
	ADC !XLO_32,x		; | position
	SEC			; |
	SBC !E4,x		; |
	STA $0300|!Base2,y	;/
	LDA $01		;\
	CLC			; | set Y
	ADC !YLO_32,x		; | position
	SEC			; |
	SBC !D8,x		; |
	STA $0301|!Base2,y	; /
	PHY			; \
	LDY !GFXTMP2		;  | get fireball
	LDA FBTILES,y		;  | tile number
	PLY			; /
	STA $0302|!Base2,y		;   set tile number
	LDA #$05		;   get palette and GFX page
	PHY			;\
	LDY !GFXTMP2		; | add in tile
	ORA FBPROPS,y		; | properties
	PLY			;/
	ORA $64		;   add in priority bits
	STA $0303|!Base2,y		;   set properties
FINISHGFX:
	LDY #$02		;   tiles are 16x16
	LDA !TILESDRAWN	;\  A must be # of
	JSL $01B7B3|!BankB	;   don't draw if offscreen, set sizes
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   LOGPOSITION:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LDA !YLO_31,x
	STA !YLO_32,x
	LDA !YLO_30,x
	STA !YLO_31,x
	LDA !YLO_29,x
	STA !YLO_30,x
	LDA !YLO_28,x
	STA !YLO_29,x
	LDA !YLO_27,x
	STA !YLO_28,x
	LDA !YLO_26,x
	STA !YLO_27,x
	LDA !YLO_25,x
	STA !YLO_26,x
	LDA !YLO_24,x
	STA !YLO_25,x
	LDA !YLO_23,x
	STA !YLO_24,x
	LDA !YLO_22,x
	STA !YLO_23,x
	LDA !YLO_21,x
	STA !YLO_22,x
	LDA !YLO_20,x
	STA !YLO_21,x
	LDA !YLO_19,x
	STA !YLO_20,x
	LDA !YLO_18,x
	STA !YLO_19,x
	LDA !YLO_17,x
	STA !YLO_18,x
	LDA !YLO_16,x
	STA !YLO_17,x
	LDA !YLO_15,x
	STA !YLO_16,x
	LDA !YLO_14,x
	STA !YLO_15,x
	LDA !YLO_13,x
	STA !YLO_14,x
	LDA !YLO_12,x
	STA !YLO_13,x
	LDA !YLO_11,x
	STA !YLO_12,x
	LDA !YLO_10,x
	STA !YLO_11,x
	LDA !YLO_9,x
	STA !YLO_10,x
	LDA !YLO_8,x
	STA !YLO_9,x
	LDA !YLO_7,x
	STA !YLO_8,x
	LDA !YLO_6,x
	STA !YLO_7,x
	LDA !YLO_5,x
	STA !YLO_6,x
	LDA !YLO_4,x
	STA !YLO_5,x
	LDA !YLO_3,x
	STA !YLO_4,x
	LDA !YLO_2,x
	STA !YLO_3,x
	LDA !YLO_1,x
	STA !YLO_2,x
	LDA !D8,x
	STA !YLO_1,x
	LDA !XLO_31,x
	STA !XLO_32,x
	LDA !XLO_30,x
	STA !XLO_31,x
	LDA !XLO_29,x
	STA !XLO_30,x
	LDA !XLO_28,x
	STA !XLO_29,x
	LDA !XLO_27,x
	STA !XLO_28,x
	LDA !XLO_26,x
	STA !XLO_27,x
	LDA !XLO_25,x
	STA !XLO_26,x
	LDA !XLO_24,x
	STA !XLO_25,x
	LDA !XLO_23,x
	STA !XLO_24,x
	LDA !XLO_22,x
	STA !XLO_23,x
	LDA !XLO_21,x
	STA !XLO_22,x
	LDA !XLO_20,x
	STA !XLO_21,x
	LDA !XLO_19,x
	STA !XLO_20,x
	LDA !XLO_18,x
	STA !XLO_19,x
	LDA !XLO_17,x
	STA !XLO_18,x
	LDA !XLO_16,x
	STA !XLO_17,x
	LDA !XLO_15,x
	STA !XLO_16,x
	LDA !XLO_14,x
	STA !XLO_15,x
	LDA !XLO_13,x
	STA !XLO_14,x
	LDA !XLO_12,x
	STA !XLO_13,x
	LDA !XLO_11,x
	STA !XLO_12,x
	LDA !XLO_10,x
	STA !XLO_11,x
	LDA !XLO_9,x
	STA !XLO_10,x
	LDA !XLO_8,x
	STA !XLO_9,x
	LDA !XLO_7,x
	STA !XLO_8,x
	LDA !XLO_6,x
	STA !XLO_7,x
	LDA !XLO_5,x
	STA !XLO_6,x
	LDA !XLO_4,x
	STA !XLO_5,x
	LDA !XLO_3,x
	STA !XLO_4,x
	LDA !XLO_2,x
	STA !XLO_3,x
	LDA !XLO_1,x
	STA !XLO_2,x
	LDA !E4,x
	STA !XLO_1,x
	RTS

