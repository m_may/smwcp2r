!tile = $EC

XDisp:
db $70,$80
db $60,$90
db $50,$A0
db $40,$B0
db $30,$C0
db $20,$D0
db $10,$E0
db $00,$F0

print "INIT ",pc
LDA !E4,x
STA !151C,x
LDA !14E0,x
STA !1528,x

LDA !7FAB10,x
AND #$04
BEQ +

LDA #$01
BRA ++

+
LDA #$10
++
STA !C2,x
RTL

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
RTL

SpriteCode:
LDA #$00
%SubOffScreen()

LDA $1A
LSR #4
ASL #4
CLC
ADC !151C,x
STA !E4,x
LDA $1B
ADC #$00
STA !14E0,x

LDA $9D
ORA $13D4
BEQ +
RTS

+
LDA !7FAB10,x
AND #$04
BEQ +

LDA $18BB
CMP #$02
BNE lol
BRA kill

+
LDA $18BB
CMP #$01
BNE +

LDA !C2,x
BEQ +

LDA $13
AND #$0F
BNE +

DEC !C2,x

LDA !C2,x
BEQ +

DEC !C2,x

+
lol:
JSR graphics
RTS

graphics:
%GetDrawInfo()

LDA !C2,x
BNE +

kill:
STZ !14C8,x
RTS

+
DEC
STA $02
STZ $03

PHX
LDX $02
GFXLoop:
LDA $00
CLC
ADC XDisp,x
STA $0300|!Base2,y

LDA $01
STA $0301|!Base2,y

LDA #!tile
STA $0302|!Base2,y

LDA #$0C			;palette/properties are set here
ORA $64
STA $0303|!Base2,y
INY #4
INC $03
DEX
BPL GFXLoop

PLX
LDY #$02
LDA $03
DEC
JSL $01B7B3|!BankB
RTS