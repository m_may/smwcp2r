;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FOR USE WITH DESERT BOSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!RemainingTimesToBounce = !1504  ; MUST BE THE SAME ADDRESS AS IN desertboss.asm

HorzBoostSpeed:
db $38,$E4,$1C,$C8

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR SpriteCode
			PLB
			print "INIT ",pc
			RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
			JSR GraphicsRoutine

			LDA !14C8,x
			CMP #$08
			BNE .Return

			LDA $9D
			BNE .Return

			LDA !RemainingTimesToBounce,x
			BPL .DontDestroy
			JSR PuffOfSmoke
			RTS

.DontDestroy
			JSL $01802A|!bank
			JSL $01A7DC|!bank
			BCC .NoContact

.PlayerContact
			LDA !163E,x
			BNE .Return
			LDA #$10
			STA !163E,x
			LDA $0E
			CMP #$E6
			BMI .BoostUp
			CMP #$0C
			BPL .EndHorzBoost
			%SubHorzPos()
			LDA !157C,x
			BEQ .EndAddFlip
.AddFlip
			INY
			INY
.EndAddFlip
.HorzBoost
			LDA HorzBoostSpeed,y
			STA $7B
.EndHorzBoost
			LDA $0E      ; is mario above the sprite?
			CMP #$E8     ; cause if he is.
			BMI .BoostUp ; sprite should not push upwards
			CMP #$00
			BMI .MakeSound
			;BPL .BoostDown
			;BRA .MakeSound
.BoostDown
			LDA #$09
			STA $7D
			BRA .MakeSound
.BoostUp
			LDA #$B0
			STA $7D
			LDA $15
			BPL .MakeSound
.ExtraUpBoost
			LDA #$A3
			STA $7D
.MakeSound
			LDA #$08
			STA $1DFC|!addr  ; sfx
			RTS

.NoContact
			LDA !1588,x
			BIT #$04
			BNE .HitGround
			BIT #$03
			BNE .HitWall

.Return
			RTS

.HitGround
			LDA !AA,x
			EOR #$FF
			STA !AA,x
			DEC !RemainingTimesToBounce,x
			RTS

.HitWall
			LDA !B6,x
			EOR #$FF
			STA !B6,x
			DEC !RemainingTimesToBounce,x
			RTS

PuffOfSmoke:
			LDA #$04
			STA !14C8,x
			LDA #$20
			STA !1540,x
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YarnTiles:
			db $8A,$8C

GraphicsRoutine:
			LDA $14
			LSR #3
			AND #$01
			STA $02

			%GetDrawInfo()

			LDA $00
			STA $0300|!addr,y
			LDA $01
			STA $0301|!addr,y

			PHY
			LDY $02
			LDA YarnTiles,y
			PLY
			STA $0302|!addr,y

			LDA !15F6,x
			ORA #$30
			STA $0303|!addr,y

			LDY #$02
			LDA #$00
			JSL $01B7B3|!bank
			RTS