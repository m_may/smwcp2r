;Baron Von Zeppelin
;by smkdan

;USES EXTRA BIT
;If set, it will come back after it's dropped it's payload.  Else, it won't come back.

;An enemy from SMW2 that holds a sprite of your choice and drops it when Mario gets near.

;Extra prop 1: How close Mario gets until the payload is dropped

;Tilemaps for SP3

!DIR = $03
!PROPRAM = $04
!PROPINDEX = $05
!XDISPINDEX = $06
!YDISPINDEX = $07
!FRAMEINDEX = $08
!TEMP = $09

SINE:	db $00,$01,$02,$03,$04,$03,$02,$01 
	db $00,$FF,$FE,$FD,$FC,$FD,$FE,$FF

SPEED:	db $04,$FC

;===

;Data to use for the sprite to be generated.  It takes a 16x16 sprite for it's dummy graphics.

;Currently set for a Goomba

!SPRITE_NUMBER = $03	;Set this to the sprite you want it to hold
!SPRITE_GFX = $E0	;Set this to the GFX of the sprite you want it to hold
!SPRITE_PROP = $14 ;palette, GFX page etc.

;=====

;C2: YSpd
;1570: Drop / fly flag
;1602: SINE index

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	LDA #$FF
	STA $161A,x
	JSR Run
	PLB
PRINT "INIT ",pc
	RTL	;also nothing


Run:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()
	JSR GFX			;draw sprite
	JSR GFXITEM		;draw optional item it's holding

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN           
	LDA $9D			;locked sprites?
	BNE RETURN
	LDA $15D0,x		;yoshi eating?
	BNE RETURN

	LDY $157C,x
	LDA SPEED,y		;SPEED depending on direction
	STA $B6,x

	LDA $1570,x		;if already generated, fly away
	BEQ CHECKPROX

FLYAWAY:
	LDA $14
	AND #$01
	BNE SKIPFLY		;slow ascent

	LDA $C2,x		;Yspd
	DEC A
	STA $C2,x
	STA $AA,x
	CMP #$F0
	BCS SKIPFLY

	INC $C2,x		;compensate
	STA $C2,x
	STA $AA,x

	BRA SKIPFLY

CHECKPROX:
	JSR PROXIMITY		;my common PROXIMITY routine
	BEQ NOGEN
	JSR GENERATE		;if in range, GENERATE it

	LDA $1570,x		;this is not zero if generation was successful
	BEQ NOGEN		;just continue until time comes

	LDA #$15		;play kicking shell sound
	STA $1DF9

	LDA $7FAB10,x		;extra bit
	AND #$04
	BNE NOGEN		;if bit is clear, don't come back

	LDA #$FF		;erase sprite permanentley
	STA $161A,x

NOGEN:
	JSR SINEMOTION		;wavy motion, Xspd
	LDA $AA,x		;keep storing to storage
	STA $C2,x
	
	%SubHorzPos()
	TYA
	STA $157C,x		;!TEMP: Always face Mario

SKIPFLY:
	JSL $81802A		;SPEED update

RETURN:
	RTS     

GENERATE:	
	JSL $82A9E4		;grab free sprite slot
	BMI RETURNG		;RETURN if none left...

	PHX			;preserve sprite index

	LDA #$01
	STA $14C8,y		;normal status, run init routine?
	LDA #!SPRITE_NUMBER	;load sprite to GENERATE...
	STA $009E,y		;into sprite type table
	
	LDA $E4,x		;same Xpos
	STA $00E4,y
	LDA $14E0,x		;share same screen
	STA $14E0,y

	LDA $D8,x
	CLC
	ADC #$19		;below it
	STA $00D8,y		;Ypos
	LDA $14D4,x
	ADC #$00		;transfer carry
	STA $14D4,y		;same screen

	TYX			;new sprite slot into X
	JSL $87F7D2		;create

	PLX			;restore sprite index
	
	INC $1570,x		;only does this once
		
RETURNG:
	RTS     
		
;=====

TILEMAP:	db $0C,$17,$17		;balloon, chain, chain

XDISP:	db $00,$04,$04

YDISP:	db $00,$0E,$12

XBIAS:	db $00,$08,$08
SIZE:	db $02,$00,$00

EORF:	db $40,$00

GFX:
	LDA $15F6,x	;load properties
	STA !PROPRAM	;into !TEMP

	LDA $157C,x	;direction...
	STA !DIR

	LDX #$00	;loop index

OAM_LOOP:
	TYA
	LSR A
	LSR A
	PHY
	TAY
	LDA SIZE,x
	STA $0460,y		;SIZE table
	PLY			;restore

	LDA !DIR
	BNE XLEFT

	LDA $00
	SEC
	SBC XDISP,x
	CLC
	ADC XBIAS,x		;add bias since it throws it on the other side
	STA $0300,y
	BRA XNEXT
XLEFT:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300,y
XNEXT:
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301,y

	LDA TILEMAP,x
	STA $0302,y

	LDA $64
	ORA !PROPRAM
	PHX
	LDX !DIR
	EOR EORF,x
	PLX
	STA $0303,y

	INY
	INY
	INY
	INY
	INX
	CPX #$03	;3 tiles for balloon and chain
	BNE OAM_LOOP

	LDX $15E9

	RTS	
	
GFXITEM:
	LDA $1570,x
	BNE GFXI4_RETURN	;RETURN if GFX shouldn't be drawn

	PHY
	TYA
	LSR A
	LSR A
	TAY
	LDA #$02
	STA $0460,y	;it's a 16x16 sprite being held
	PLY

	LDA $00
	STA $0300,y	;same x pos

	LDA $01
	CLC
	ADC #$19	;below it
	STA $0301,y

	LDA #!SPRITE_GFX
	STA $0302,y	;sprite to gen GFX

	LDA #!SPRITE_PROP	;sprite properties to GENERATE
	ORA $64
	PHX
	LDX !DIR
	EOR EORF,x
	PLX
	STA $0303,y

GFXI3_RETURN:
	LDY #$FF
	LDA #$03	;4 tiles
	BRA LOLOL

GFXI4_RETURN:
	LDY #$FF
	LDA #$02	;only 3 tiles
LOLOL:
	JSL $81B7B3	;bookkeeping
	RTS

;=====
;a routine to calculate whether or not Mario is within range of wiggler.  Based off fang.asm
;Z = 0 when in range, 1 when out of range

EORTBLI:	db $FF,$00
EORTBL:	db $00,$FF
		
PROXIMITY:
	LDA $14E0,x		;sprite x high
	XBA
	LDA $E4,x		;sprite x low
	REP #$20		;16bitA
	SEC
	SBC $94			;sub mario x
	SEP #$20		;8bitA
	PHA			;preserve for routine jump
	%SubHorzPos()		;horizontal distance
	PLA			;restore
	EOR EORTBLI,y		;invert if needed
	CMP $7FAB28,x		;range define by Extra Prop 1*
	BCS PRANGE_OUT		;RETURN not within range
	LDA #$01		;Z = 0
	RTS

PRANGE_OUT:
	LDA #$00		;Z = 1
	RTS

SINEMOTION:
	LDA $1602,x		;dedicated SINE index
	INC A			;advance
	STA $1602,x		;and store

	LSR A
	LSR A
	LSR A
	AND #$0F		;only 16 entries
	TAY
	LDA SINE,y		;grab entry
	STA $AA,x		;new Yspd
	RTS