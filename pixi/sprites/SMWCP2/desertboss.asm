;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Desert boss for SMWCP2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!SandSpriteNumber = $B6    ;sand block sprite number
!YarnSpriteNumber = $B5    ;ball of yarn sprite number
!PyramidSpriteNumber = $B7 ;head pyramid sprite number
!SandDropHeight = $0220    ;sand block drop height.
!HP = $03                  ;was 5, then 4, so now it's 3
!PyramidThrowingHeight = $14
!IdleAnimFrameRate = $08
!InitialIdleTime = $70
!BurrowTime = $04  ;must be $04
!BurrowFreezeTime = $20
!DropSandWaitBeforeAttackTimeP1 = $10
!DropSandWaitBeforeAttackTimeP2 = $0E
!DropSandWaitAfterAttackTime = $11
!HurtTime = $50
!BallWaitBeforeThrownTime = $10
!BallWaitAfterThrownTime = $10
!MagicTime = $1E
!PyramidWaitAfterThrownTime = $F0
!PyramidWaitAfterCaughtTime = $06

Phases:;phase table
db $08,$08,$00,$00 ;each entry corresponds to an HP value
               ;00 = first, 08 = second phase

IdleTime:          ;amount of frames to idle before attacking
db $00,$10,$30,$50 ;each entry corresponds to each amount of HP

IdleTimeAfterBall:
db $00,$02,$08,$20

PopOutFreezeTime:
db $FF,$0B,$13,$21

RandomAttacks:	;the first 8 entries are used with the RNG
db $01,$01,$04,$04;to determine what attack to use
db $05,$04,$05,$05;
RandomAttacksPHASE2:		;these 8 entries are for the second phase
db $04,$04,$07,$07;
db $05,$01,$01,$05;

TimerSet:
db !BurrowTime,!BurrowTime,!DropSandWaitBeforeAttackTimeP1,!DropSandWaitBeforeAttackTimeP1
db !BallWaitBeforeThrownTime,!DropSandWaitBeforeAttackTimeP1,!BallWaitBeforeThrownTime,!BallWaitBeforeThrownTime
TimerSetPHASE2:
db !DropSandWaitBeforeAttackTimeP2,!DropSandWaitBeforeAttackTimeP2,!MagicTime,!MagicTime;
db !BallWaitBeforeThrownTime,!BurrowTime,!BurrowTime,!BallWaitBeforeThrownTime;

RepeatSet:          ;as the name says, this is the number of times to repeat the attack
db $01,$01,$02,$03  ;it's fine to change this
db $00,$04,$00,$00  ;
RepeatSetPHASE2:    ;
db $06,$08,$01,$02  ;
db $00,$01,$01,$00  ;

SandTimerSet:  ;sand block timer table
db $FF,$16,$20,$30 ;each entry corresponds to a certain HP
               ;it is the number of frames before another block is dropped

SandPopOutTime:
db $FF,$10,$18,$30
SandPopOutSpeed:
db $FF,$06,$04,$02

UndergroundRightSpeed:
db $FF,$38,$28,$18
UndergroundLeftSpeed:
db $FF,$C8,$D8,$E8

HurtIdleTime:      ;how long the boss is supposed to idle after getting hurt, indexed by HP
db $FF,$20,$40,$60 ;this should be longer than the values in IdleTime

BallTimesToBounce:
db $FF,$0F,$0B,$09

ConsecutiveStatesBeforePyramid:
db $FF,$06,$04,$03

PyramidWaitBeforeThrownTime:
db $FF,$08,$18,$28

PyramidVariantTable:
db $00,$01,$00,$00

PyramidRepeats:
db $00,$01,$00,$00

PyramidTimeBeforeReturn: ;indexed by variant
db $30,$18

MagicSpeeds: ;indexed by repeat count
db $50,$30,$30

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!GfxFrame = !1504
!Phase = !1534
!StateRepeats = !151C
!StateTimer = !154C
!StateMisc = !1594
!MiscStateTimer = !15AC
!MiscStateTimer2 = !163E
!MiscStateTimer3 = !1540
!CurrentHP = !1626
!GroundHeightLowByte = !1602
!GroundHeightHighByte = !160E
!ConsecutiveStates = !187B

!SandPopOutTimer = !15AC  ; MUST BE THE SAME ADDRESS AS IN sandblock.asm

!BallRemainingTimesToBounce = !1504  ; MUST BE THE SAME ADDRESS AS IN balloyarn.asm

!PyramidIsReturning = !C2          ; \
!PyramidVariant = !151C            ; | MUST BE THE SAME ADDRESSES AS IN pyramid.asm
!PyramidTimerBeforeReturn = !15AC  ; /

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA #!InitialIdleTime
STA !StateTimer,x
LDA !14D4,x
STA !GroundHeightHighByte,x
LDA !D8,x
STA !GroundHeightLowByte,x
LDA #$08
STA !MiscStateTimer,x
LDA #!HP
STA !CurrentHP,x
RTL

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
LDY !CurrentHP,x
LDA Phases,y
STA !Phase,x

REP #$20
LDA $96
CMP #$02A8
BCC +
SEP #$20
STZ $1412|!addr

+
SEP #$20

LDA !C2,x  ; if state is throwing pyramid, don't face the player anymore
CMP #$08
BEQ +
%SubHorzPos()
TYA
STA !157C,x
+

JSR GraphicsRoutine

LDA $9D
BNE .Return

LDA !14C8,x
CMP #$08
BEQ .NotDying

.DyingFalling
LDA !14D4,x
XBA
LDA !D8,x
REP #$20
SEC : SBC $1C
CMP #$00F0
SEP #$20
BCC .OnScreen

.LevelEnd
STZ !14C8,x
LDA #$0B
STA $1696|!Base2	;uberasm victory flag by blind devil
.OnScreen
RTS

.NotDying
JSR CheckMarioContact
JSL $019138|!bank ;sprite<->object interaction

LDA !C2,x
JSL $0086DF|!bank

.States
dw Idle
dw Burrowing
dw Underground
dw PoppingOut
dw DroppingSand
dw ThrowingBall
dw Hurt
dw Magic
dw ThrowingPyramid

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Properties:
db $40,$00

Tilemap:
dw TilemapIdle1
dw TilemapIdle2
dw TilemapIdle3
dw TilemapIdle2
dw TilemapStaff1
dw TilemapStaff2
dw TilemapThrow1
dw TilemapThrow2
dw TilemapSandMound1
dw TilemapSandMound1 ;Hurt
dw TilemapSandMound2
dw TilemapPyramidless
dw TilemapDropSand1
dw TilemapDropSand2

XDisps:
dw XDispsIdle
dw XDispsIdle
dw XDispsIdle
dw XDispsIdle
dw XDispsIdle ;staff1
dw XDispsStaff2 ;staff2
dw XDispsIdle ;throw1
dw XDispsIdle ;throw2
dw XDispsSandMound
dw XDispsSandMound
dw XDispsSandMound
dw XDispsPyramidless
dw XDispsIdle ;dropsand1
dw XDispsStaff2 ;dropsand2

YDisps:
dw YDispsIdle1
dw YDispsIdle2
dw YDispsIdle1
dw YDispsIdle2
dw YDispsIdle1 ;staff1
dw YDispsStaff2 ;staff2
dw YDispsThrow ;throw1
dw YDispsThrow ;throw2
dw YDispsSandMound
dw YDispsSandMound
dw YDispsSandMound
dw YDispsPyramidless
dw YDispsDropSand1
dw YDispsDropSand2


TilemapIdle1:
db $EE,$E2,$A2,$82,$AA,$FF
TilemapIdle2:
db $EE,$E0,$A2,$82,$AA,$FF
TilemapIdle3:
db $EE,$E2,$A0,$82,$AA,$FF
TilemapStaff1:
db $CE,$E2,$A2,$82,$AA,$FF
TilemapStaff2:
db $DC,$CC,$E2,$A2,$82,$AA,$FF
TilemapThrow1:
db $CA,$E6,$A2,$82,$AA,$FF
TilemapThrow2:
db $CA,$E8,$A2,$82,$AA,$FF
TilemapSandMound1:
db $AC,$FF
TilemapSandMound2:
db $AE,$FF
TilemapPyramidless:
db $CA,$E8,$C2,$AA,$FF
TilemapDropSand1:
db $CE,$E4,$A2,$82,$AA,$FF
TilemapDropSand2:
db $DC,$CC,$E4,$A2,$82,$AA,$FF

XDispsIdle:
db $00,$00,$00,$00,$0E
XDispsStaff2:
db $00,$00,$00,$00,$00,$0E
XDispsSandMound:
db $00
XDispsPyramidless:
db $00,$00,$00,$0E

YDispsIdle1:
db $FC,$00,$F1,$E8,$00
YDispsIdle2:
db $FB,$00,$F0,$E7,$FF
YDispsStaff2:
db $FC,$F4,$00,$F1,$E8,$00
YDispsThrow:
db $01,$00,$F1,$E8,$00
YDispsSandMound:
db $00
YDispsPyramidless:
db $01,$00,$F1,$00
YDispsDropSand1:
db $00,$00,$F2,$E9,$00
YDispsDropSand2:
db $00,$F8,$00,$F2,$E9,$00

GraphicsRoutine:
LDA !157C,x
STA $03
LDA !15F6,x
STA $04
LDA !GfxFrame,x
ASL
STA $05

%GetDrawInfo()

LDX #$00

.GFXLoop
PHY
LDY $05
REP #$20
LDA Tilemap,y
STA $06
SEP #$20
TXY
LDA ($06),y
PLY
CMP #$FF
BEQ .EndGFXLoop
STA $0302|!addr,y

PHY
LDY $05
REP #$20
LDA XDisps,y
STA $06
SEP #$20
TXY
LDA ($06),y
LDY $03  ; if flipped
BNE +
EOR #$FF
INC
+
CLC : ADC $00
PLY
STA $0300|!addr,y

PHY
LDY $05
REP #$20
LDA YDisps,y
STA $06
SEP #$20
LDA $01
TXY
CLC : ADC ($06),y
PLY
STA $0301|!addr,y

PHY
LDY $03
LDA Properties,y
ORA #$30
ORA $04
PLY
STA $0303|!addr,y

INY #4
INX
BRA .GFXLoop
.EndGFXLoop

TXA
LDX $15E9|!addr
LDY #$02
JSL $01B7B3|!bank

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite state codes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Idle:
LDA !MiscStateTimer,x
BNE .Skip

INC !GfxFrame,x
LDA !GfxFrame,x
CMP #$04
BNE .SkipZero

STZ !GfxFrame,x

.SkipZero
LDA #!IdleAnimFrameRate
STA !MiscStateTimer,x

.Skip
LDA !StateTimer,x
BNE .DontSwitchState

LDY !CurrentHP,x
LDA !ConsecutiveStates,x
CMP ConsecutiveStatesBeforePyramid,y
BCS .SwitchToThrowPyramidState

JSL $01ACF9|!bank ;GetRand
LDA $148D|!addr
EOR $94
EOR $96
AND #$07
CLC : ADC !Phase,x
TAY

LDA RandomAttacks,y
STA !C2,x
LDA TimerSet,y
STA !StateTimer,x
LDA RepeatSet,y
STA !StateRepeats,x
STZ !MiscStateTimer,x
INC !ConsecutiveStates,x

.DontSwitchState
RTS

.SwitchToThrowPyramidState
LDA #$08
STA !C2,x
LDY !CurrentHP,x
LDA PyramidWaitBeforeThrownTime,y
STA !StateTimer,x
LDY !CurrentHP,x
LDA PyramidRepeats,y
STA !StateRepeats,x
STZ !MiscStateTimer,x
STZ !ConsecutiveStates,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Burrowing:
LDA !StateTimer,x
BEQ .StopMovingDown

LDA !D8,x
CLC
ADC #$03
STA !D8,x

LDA !14D4,x
ADC #$00
STA !14D4,x
RTS

.StopMovingDown
JSR ShatterEffect
LDA #$10
STA $1887|!addr

LDA #$08
STA !GfxFrame,x
LDA #!BurrowFreezeTime
STA !StateTimer,x
INC !C2,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Imgs:
db $08,$0A

StartPoppingOut:
LDA #$D0
STA !AA,x
STZ !B6,x
STZ !GfxFrame,x
INC !C2,x
JSR ShatterEffect
LDA #$10
STA $1887|!addr
RTS

Underground:
LDA !MiscStateTimer,x  ; time before popping out after player is above the mound
BEQ .NotGettingReadyToPopOut
CMP #$01
BEQ StartPoppingOut

.GettingReadyToPopOut
LDA $14
LSR
LSR
AND #$01
TAY
LDA Imgs,y
STA !GfxFrame,x
RTS

.NotGettingReadyToPopOut
LDA !B6,x
BEQ +

LDA $14
LSR
LSR
LSR
AND #$01
TAY
LDA Imgs,y
STA !GfxFrame,x

+
LDA !StateTimer,x
BNE .NoXSpeeds

LDA !14E0,x
XBA
LDA !E4,x
REP #$20
SEC
SBC $94
CLC
ADC #$000C
CMP #$0018
SEP #$20
BCS .SkipGettingReadyToPopOut

LDY !CurrentHP,x
LDA PopOutFreezeTime,y
STA !MiscStateTimer,x

.NoXSpeeds
RTS

.SkipGettingReadyToPopOut
JSL $018022|!bank
LDA !157C,x
BNE Left

LDA !B6,x
LDY !CurrentHP,x
CMP UndergroundRightSpeed,y
BCS .Skip

INC !B6,x
INC !B6,x

.Skip
RTS

Left:
LDA !B6,x
BEQ .Go
LDY !CurrentHP,x
CMP UndergroundLeftSpeed,y
BEQ .Skip
BCC .Skip

.Go
DEC !B6,x
DEC !B6,x

.Skip
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PoppingOut:
LDA !AA,x
BMI .SkipCheck

LDA !14D4,x
CMP !GroundHeightHighByte,x
BNE .SkipCheck

LDA !D8,x
CMP !GroundHeightLowByte,x
BNE .SkipCheck

LDA !StateRepeats,x
BNE .Repeat

JSR StartIdling
RTS

.Repeat
LDA #$01
STA !C2,x
LDA #!BurrowTime
STA !StateTimer,x
DEC !StateRepeats,x
STZ !B6,x

.SkipCheck
JSL $01802A|!bank
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DroppingSand:
LDA !StateTimer,x
BEQ .DropSand

LDA #$0C
STA !GfxFrame,x

LDA !StateRepeats,x
BNE .SkipEnd
LDA !StateTimer,x
CMP #$01
BNE .SkipEnd

JSR StartIdling

.SkipEnd
RTS

.DropSand
LDA $1887|!addr
BNE +

LDA #$10
STA $1887|!addr

+
LDA #$0D
STA !GfxFrame,x

LDA !StateRepeats,x
BNE .Skip

LDA #!DropSandWaitAfterAttackTime
STA !StateTimer,x

.Return
RTS

.Skip
LDA !MiscStateTimer,x
BNE .Return

JSL $02A9DE|!bank
BMI .Return
PHX
TYX
LDA #$08
STA !14C8,x
LDA #!SandSpriteNumber
STA !7FAB9E,x
JSL $07F7D2|!bank
JSL $0187A7|!bank
LDA #$08
STA !7FAB10,x
PLX

LDA $94
STA !E4,y
LDA $95
STA !14E0,y
LDA.b #!SandDropHeight
STA !D8,y
LDA.b #!SandDropHeight>>8
STA !14D4,y

PHX
LDA !CurrentHP,x
TAX
LDA SandPopOutTime,x
STA !SandPopOutTimer,y
LDA SandPopOutSpeed,x
STA !sprite_speed_y,y
PLX

DEC !StateRepeats,x

LDA #$21
STA $1DF9|!addr
LDY !CurrentHP,x
LDA SandTimerSet,y
STA !MiscStateTimer,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YarnXSpeeds:
db $28,$D8

ThrowingBall:
LDA !StateTimer,x
BEQ .ThrowSprite

LDA #$06
STA !GfxFrame,x
RTS

.ThrowSprite
LDA #$07
STA !GfxFrame,x

LDA !MiscStateTimer,x
BEQ .Skip
CMP #$01
BNE .Return

JSR StartIdlingAfterBall

.Return
RTS

.Skip
JSL $02A9DE|!bank
BMI .Return
PHX
TYX
LDA #$08
STA !14C8,x
LDA #!YarnSpriteNumber
STA !7FAB9E,x
JSL $07F7D2|!bank
JSL $0187A7|!bank
LDA #$08
STA !7FAB10,x
PLX

LDA !157C,x
STA !157C,y
PHY
TAY
LDA YarnXSpeeds,y
PLY
STA !B6|!dp,y

LDA !14E0,x
STA !14E0,y
LDA !E4,x
STA !E4|!dp,y

LDA !D8,x
STA !D8|!dp,y
LDA !14D4,x
STA !14D4,y

LDA #$C0
STA !AA|!dp,y

PHY
LDY !CurrentHP,x
LDA BallTimesToBounce,y
PLY
STA !BallRemainingTimesToBounce,y

LDA #!BallWaitAfterThrownTime
STA !MiscStateTimer,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Hurt:
STZ !B6,x
JSL $01802A|!bank

LDA !StateTimer,x
BNE .SkipReset

JSR StartIdling
LDY !CurrentHP,x
LDA HurtIdleTime,y
STA !StateTimer,x

.SkipReset
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedMagic:
db $10,$F0

Magic:
LDA #$04
STA !GfxFrame,x

LDA !StateTimer,x
BEQ .DontRaise
CMP #$14
BCS .Return

STZ !AA,x
LDA !D8,x
SEC
SBC #$04
STA !D8,x
LDA !14D4,x
SBC #$00
STA !14D4,x
LDA #$20
STA !MiscStateTimer2,x

.Return
RTS

.DontRaise
JSR Float

LDA !MiscStateTimer2,x
CMP #$3D
BCS .StaffGlowing
CMP #$08
BCC .StaffGlowing

.StaffNotGlowing
LDA #$04
BRA +
.StaffGlowing
LDA #$05
+
STA !GfxFrame,x

LDA !StateRepeats,x
BMI .End
LDA !MiscStateTimer2,x
BNE .Return

JSR SpawnMagic
LDA #$40
STA !MiscStateTimer2,x
LDA !StateRepeats,x
DEC
STA !StateRepeats,x
BPL .Skip

LDA #$E0
STA !AA,x

.Skip
LDA #$10
STA $1DF9|!addr
RTS

.End
LDA #$20
CMP !AA,x
BPL +
STA !AA,x

+
JSL $01802A|!bank
LDA !1588,x
AND #$04
BEQ .NotYet

JSR StartIdling
STZ !StateMisc,x
STZ !MiscStateTimer3,x
STZ !AA,x

.NotYet
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PyramidXSpeeds:
db $20,$E0,$40,$C0

ThrowingPyramid:
LDA !StateTimer,x
BEQ .Acting

LDA #$06
STA !GfxFrame,x
RTS

.Acting
LDA !StateRepeats,x
BMI .SwitchStateToIdle
LDA !MiscStateTimer,x
BEQ .DoThrowPyramid
CMP #$01
BNE .CheckPyramidContact

JSR StartBurrowing
RTS

.SwitchStateToIdle
JSR StartIdling
RTS

.CheckPyramidContact
LDY #!SprSize-1

.ContactLoop
LDA !14C8,y
CMP #$08
BCC .Next
PHX
TYX
LDA !7FAB9E,x
PLX
CMP #!PyramidSpriteNumber
BNE .Next
LDA !PyramidIsReturning,y ; if isn't returning don't check contact (so it doesn't check right after throwing)
BEQ .Next

JSL $03B69F|!bank
LDA $05
SEC : SBC #$08
STA $05
LDA $0B
SBC #$00
STA $0B
PHX
TYX
JSL $03B6E5|!bank
PLX
JSL $03B72B|!bank
BCC .Next

LDA #$00
STA !14C8,y
DEC !StateRepeats,x
LDA #!PyramidWaitAfterCaughtTime
STA !StateTimer,x
STZ !MiscStateTimer,x
RTS

.Next
DEY
BPL .ContactLoop

.Return
RTS

.DoThrowPyramid
LDA #$0B
STA !GfxFrame,x

JSL $02A9DE|!bank
BMI .Return
PHX
TYX
LDA #$08
STA !14C8,x
LDA #!PyramidSpriteNumber
STA !7FAB9E,x
JSL $07F7D2|!bank
JSL $0187A7|!bank
LDA #$08
STA !7FAB10,x
PLX

PHY
LDY !CurrentHP,x
LDA PyramidVariantTable,y
PLY
STA !PyramidVariant,y
PHX
TAX
LDA PyramidTimeBeforeReturn,x
STA !PyramidTimerBeforeReturn,y
PLX

LDA !157C,x
STA !157C,y
PHX
TAX
LDA !PyramidVariant,y
BEQ +
INX
INX
+
LDA PyramidXSpeeds,x
PLX
STA !B6|!dp,y

LDA !14E0,x
STA !14E0,y
LDA !E4,x
STA !E4|!dp,y

LDA !D8,x
SEC : SBC #!PyramidThrowingHeight
STA !D8|!dp,y
LDA !14D4,x
SBC #$00
STA !14D4,y

LDA #!PyramidWaitAfterThrownTime
STA !MiscStateTimer,x

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ShatterEffect:
LDA !D8,x
CLC
ADC #$08
STA $98
LDA !14D4,x
ADC #$00
STA $99
LDA !E4,x
STA $9A
LDA !14E0,x
STA $9B

PHB
LDA #$02
PHA
PLB
LDA #$00
JSL $028663|!bank
PLB
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Float:
JSL $01801A|!bank

LDA !MiscStateTimer3,x
BNE .DontSwitch

LDA !StateMisc,x
EOR #$01
STA !StateMisc,x

LDA #$18
STA !MiscStateTimer3,x

.DontSwitch
LDA !StateMisc,x
BEQ .Down

LDA !AA,x
BPL .Set1
CMP #$F8
BCC .NoSet1

.Set1
DEC !AA,x

.NoSet1
RTS

.Down
LDA !AA,x
BMI .Set2
CMP #$09
BCS .NoSet2

.Set2
INC !AA,x

.NoSet2
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

StartIdling:
LDY !CurrentHP,x
LDA IdleTime,y

StartIdlingRest:
STA !StateTimer,x
STZ !B6,x
STZ !C2,x
STZ !GfxFrame,x
LDA #!IdleAnimFrameRate
STA !MiscStateTimer,x
RTS

StartIdlingAfterBall:
LDY !CurrentHP,x
LDA IdleTimeAfterBall,y
BRA StartIdlingRest

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

StartBurrowing:
STZ !B6,x
LDA #$01
STA !C2,x
LDA #!BurrowTime
STA !StateTimer,x
STZ !MiscStateTimer,x
STZ !StateRepeats,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

KillBoss:
LDA #$02
STA !14C8,x
RTS

HurtBoss:
LDA #$28
STA $1DFC|!addr
DEC !CurrentHP,x
BEQ KillBoss
LDA #$06
STA !C2,x
LDA #$08
STA !GfxFrame,x
LDA #!HurtTime
STA !StateTimer,x
STZ !ConsecutiveStates
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CheckMarioContact:
LDA !C2,x
CMP #$02
BEQ .NoContact
CMP #$06
BEQ .NoContact

JSL $01A7DC|!bank
BCC .NoContact

LDA $0E
CMP #$E6
BPL .Hurt

LDA !GfxFrame,x
CMP #$0B
BNE .WithPyramid

.WithoutPyramid
JSL $01AA33|!bank  ; boost player
BRA HurtBoss

.WithPyramid
LDA $140D|!addr  ; spinjump?
BEQ .Hurt  ; if not spinjumping, hurt player

JSL $01AA33|!bank  ; boost player
JSL $01AB99|!bank  ; contact gfx
LDA #$02
STA $1DF9|!addr  ; sfx
RTS

.Hurt
JSL $00F5B7|!bank

.NoContact
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpawnMagic:
JSL $02A9DE|!bank
BMI .Return
PHX
TYX
LDA #$08
STA !14C8,x
LDA #$20
STA !9E,x
JSL $07F7D2|!bank
PLX
LDA !E4,x
STA !E4|!dp,y
LDA !14E0,x
STA !14E0,y
LDA !D8,x
STA !D8|!dp,y
LDA !14D4,x
STA !14D4,y
JSR TargetPlayer

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TargetPlayer:
LDA !14E0,x
XBA
LDA !E4,x
REP #$20
SEC : SBC $94
STA $00
SEP #$20
LDA !14D4,x
XBA
LDA !D8,x
REP #$20
SEC : SBC $96
SEC : SBC #$000F
STA $02
SEP #$20

PHY
LDY !StateRepeats,x
LDA MagicSpeeds,y
PLY
%Aiming()
LDA $00
STA !B6|!dp,y
LDA $02
STA !AA|!dp,y
RTS