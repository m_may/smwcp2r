;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Brown Moving Platform, by imamelia
;;
;; This platform will move back and forth in a specified range,
;; either vertically or horizontally.
;;
;; Uses first extra bit: YES
;;
;; If the first extra bit is set, the platform will move vertically.
;; If not, the platform will move horizontally.
;;
;; Based on the following sprite:
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Brown Platform that falls, by Mirumo
;;  A brown platform from SMB3 that falls when stepped.
;;
;;  Extra Bit: YES
;;  Clear: it will not move.
;;  Set	:  it will always move to the left.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !EXTRA_PROP_1 = $7FAB28
;Xspeed		    db $00,$00,$F8,$F8

X_SPEEDS: db $0C,$F4
Y_SPEEDS: db $0C,$F4

!TimerValue = $7F
!TurnTimer = $C2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    PHY
                    %SubHorzPos()
                    TYA
                    STA $157C,x
                    PLY
                    LDA $1588,x             ; if on the ground, reset the turn counter
                    ORA #$04
                    STA $1588,x             ; if on the ground, reset the turn counter
	LDA #!TimerValue
	STA !TurnTimer,x		; set turn timer
	STZ $157C,x		; set HORIZONTAL direction to right
	STZ $151C,x		; and VERTICAL direction to down
                    RTL                 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc                                    
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    JSR SPRITE_CODE_START   ;  |
                    PLB                     ;  |
                    RTL                     ; /


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:              RTS
SPRITE_CODE_START:   JSR SPRITE_GRAPHICS     ; graphics routine
                    LDA $14C8,x             ; \ 
                    CMP #$08                ;  | if status != 8, RETURN
                    BNE RETURN              ; /
                    LDA $9D                 ; \ if sprites locked, RETURN
                    BNE RETURN              ; /

			LDA #$03
			%SubOffScreen()

	LDA $7FAB10,x	;
	AND #$04	; if the extra bit is set...
	BNE VERTICAL	; move vertically

	HORIZONTAL:
	LDY $157C,x	;
	LDA X_SPEEDS,y	; set X speed based on direction
	STA $B6,x

	JSR UPDATEXPOSITION	; update X position
	JSR INTERACTION	; INTERACTION routine
	RTS

	VERTICAL:
	LDY $151C,x	;
	LDA Y_SPEEDS,y	; set Y speed based on direction
	STA $AA,x

	JSR UPDATEYPOSITION	; update Y position
	JSR INTERACTION	; INTERACTION routine
	RTS

UPDATEXPOSITION:

JSR WAVYMOTION
JSL $818022             ; Update X position without gravity

STA $1528,x             ; prevent Mario from sliding horizontally

DEC !TurnTimer,x	; decrement turn timer
LDA !TurnTimer,x	; if the turn timer is 00...
BNE DONTFLIP	; flip the sprite's direction
JSR FLIPDIRECTION	;
DONTFLIP:
RTS

UPDATEYPOSITION:

JSR WAVYMOTION2
JSL $01801A             ; Update Y position without gravity

DEC !TurnTimer,x	; decrement turn timer
LDA !TurnTimer,x	; if the turn timer is 00...
BNE DONTFLIP	; flip the sprite's direction
JSR FLIPDIRECTION	;
DONTFLIP_TWO:
RTS

INTERACTION:

JSL $01B44F	; invisible solid block routine
RTS

FLIPDIRECTION:

LDA $157C,x
EOR #$01		; flip HORIZONTAL direction
STA $157C,x

LDA $151C,x
EOR #$01		; flip VERTICAL direction
STA $151C,x

LDA #!TimerValue
STA !TurnTimer,x	; reset turn timer

RTS

;==================================================================
;Wavy Motion Subroutine.	
;==================================================================

WAVYMOTION:
	PHY ; Push Y in case something messes it up.
	LDA $14 ; Get the sprite frame counter ..
	LSR A ; 7F
	LSR A ; 3F ; Tip: LSR #3.
	LSR A ; 1F
	AND #$07 ; Loop through these bytes during H-Blank. (WHAT?)
	TAY ; Into Y.
	LDA WAVYSPD,y ; Load Y speeds ..
	STA $AA,x
	JSL $81801A ; Update, with no gravity.
	PLY ; Pull Y.
	RTS

WAVYMOTION2:
	PHY ; Push Y in case something messes it up.
	LDA $14 ; Get the sprite frame counter ..
	LSR A ; 7F
	LSR A ; 3F ; Tip: LSR #3.
	LSR A ; 1F
	AND #$07 ; Loop through these bytes during H-Blank. (WHAT?)
	TAY ; Into Y.
	LDA WAVYSPD2,y ; Load Y speeds ..
	STA $B6,x
	JSL $818022 ; Update, with no gravity.
	STA $1528,x
	PLY ; Pull Y.
	RTS

WAVYSPD: db $00,$F8,$F2,$F8,$00,$08,$0E,$08
WAVYSPD2: db $00,$F8,$F2,$F8,$00,$08,$0E,$08

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


FALLINGPLATDISPX:                 db $00,$10,$20,$30
FALLINGPLATDISPY:		 db $00,$00,$F8,$F8

FALLINGPLATTILES:                 db $88,$8A,$8C,$8E

SPRITE_GRAPHICS:	    %GetDrawInfo()
STY $3F
		    PHX                       
	            LDX #$03                
CODE_038498:  	    LDA $00                   
		    CLC                       
		    ADC FALLINGPLATDISPX,x  
		    STA $0300,Y         
		    LDA $01                   
		    CLC
		    ADC FALLINGPLATDISPY,x
		    STA $0301,Y         
		    LDA FALLINGPLATTILES,x  
		    STA $0302,Y          
		    LDA #$37             ;Sprite PAL/GFX Page                
		    ORA $64                   
		    STA $0303,Y          
		    INY                       
		    INY                       
		    INY                       
		    INY                       
	            DEX                       
	            BPL CODE_038498           
		    PLX                       
		    LDY.b #$02                
		    LDA.b #$03                
		    JSL $81B7B3     
		    RTS                       ; RETURN