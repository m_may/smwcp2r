;NOTE: The tile/property bytes for the "last-ditch effort" vial is not included in this table; it is coded directly into the graphics routine.

Tilemap:            db $86,$82,$82,$86
PROPERTIES:         db $21,$21,$61,$61		;Note: This MUST exclude the palette part of the byte!  These just contain priority and X/Y flip, as well as graphics page.

FreeRAM:		;FreeRAM to use for the tilemap if the KooPhD's bit is set.  It must be a 24-bit address!
db $7F,$88,$09

!FreeRAM2 = $7F880D	;FreeRAM to use for the last-ditch potion.  It must be FreeRAM + 4, so in this case it would be $7F880D.
;BlindEdit: this free ram workaround is dumb honestly

Vibrate:
db $00,$01,$00,$FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

LDA #$40
STA $1510,X

LDA $D8,x
STA $00
LDA $14D4,x
STA $01
REP #$20
LDA $00
SEC
SBC #$0010
STA $00
SEP #$20
LDA $00
STA $D8,x
LDA $01
STA $14D4,x
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR ThrownVialMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DoNothing:
JMP Graphics

SuicideAttack:

STZ $B6,x
;LDA $1588,x
;AND #$04
;CMP #$04
;BNE DoNothing2

;DoNothing2:
;JSL $81802A

LDA $1510,X
CMP #$20
BCS SkipVibrate
LDA $13
AND #$03
PHX
TAX
LDA Vibrate,X
PLX
STA $157C,X
SkipVibrate:
LDA $1510,X

BNE DontExplode
LDA #$06
STA $1528,x
LDA #$40
STA $1510,X
Explode: 
JMP ExplodeLong
DontExplode:

JMP Graphics









ThrownVialMain:
lda !1528,x
CMP #$05
BEQ .Explode
CMP #$04
BEQ .Mushroom
CMP #$06
BEQ .Explode

.SpitFire
LDA #$04
BRA THECODEISTERRIBLEBUTBDISLAZYTORECODETHIS

.Explode
LDA #$06
BRA THECODEISTERRIBLEBUTBDISLAZYTORECODETHIS

.Mushroom
LDA #$08
BRA THECODEISTERRIBLEBUTBDISLAZYTORECODETHIS

THECODEISTERRIBLEBUTBDISLAZYTORECODETHIS:
STA !C2,x

LDA $9D
BEQ SpritesArentLockedNothingToSeeHere
JMP Graphics
SpritesArentLockedNothingToSeeHere:
LDA #$00
%SubOffScreen()
LDA $1FD6,x
BNE DontUpdatePosition
JSL $81802A
DontUpdatePosition:
LDA $1588,X
AND #$04
CMP #$04
BEQ ContinueThing
LDA $1588,X
AND #$02
CMP #$02
BEQ ContinueThing
LDA $1588,X
AND #$01
CMP #$01
BEQ ContinueThing
LDA $1588,X
AND #$08
CMP #$08
BEQ ContinueThing

JMP Graphics

ContinueThing:
DEC $1510,x

LDA $1528,X	;Misc. sprite table used to index palette.
CMP #$05
BNE +
JMP SuicideAttack
+
CMP #$02
BEQ SpitFire
CMP #$04
BEQ Mushroom
CMP #$06
BEQ Explode


SpitFire:
LDA #$00
STA $00
JSR Fireball
LDA #$10
STA $00
JSR Fireball
LDA #$F0
STA $00
JSR Fireball

JSR ShatterVial

JSL $81ACF9	;Random sound effect for fireball
LDA $148D
EOR $148E
CLC
ADC $80
CLC
ADC $7E
AND #$07
CLC
ADC #$2D
STA $1DFC

STZ $14C8,x
JMP Graphics



Mushroom:

JSL $82A9DE
BMI Return

LDA $E4,x
STA $0C
LDA $D8,x
STA $0D
LDA $14E0,X
STA $0E
LDA $14D4,x
STA $0F

PHX
TYX
LDA #$01
STA $14C8,x
LDA #$74
STA $9E,x
JSL $87F7D2

;Store it to Mario's position.
LDA $0C
STA $E4,x
LDA $0E
STA $14E0,x
LDA $0D
STA $D8,x
LDA $0F
STA $14D4,x
LDA #$F0
STA $AA,x
PLX
STZ $14C8,x

JSR ShatterVial
LDA #$02
STA $1DFC

Return:
JMP Graphics



ExplodeLong:
LDA $1FD6,x
BNE DontPlaySoundEffect
LDA #$09
STA $1DFC
STA $1FD6,x
DontPlaySoundEffect:


LDA $1510,X
;BNE DontVanishSprite
;STZ $14C8,x
;DontVanishSprite:
STZ $B6,x
;LDA #$FF
STA $1540,x
LDA #$11
STA $1662,x

PHB
LDA #$02                ;  | 
PHA                       ;  | 
PLB                       ;  | 
JSL $828086       	;  | 
PLB                       ;  |
JSL $81A7DC
RTS







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; bone-throwing subroutine, ripped from $03C44E
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Fireball:

LDA $15A0,x	; if the sprite is offscreen horizontally...
ORA $186C,x	; or vertically...
BNE NoSpawn	; don't throw a fireball

LDY #$07	; 8 extended sprite slots to loop through

ExSprSlotLoop:	;

LDA $170B,y	; if the slot is free...
BEQ SpawnBone	; then spawn a fireball sprite
DEY		; if not...
BPL ExSprSlotLoop	; decrement the index and try the next slot

NoSpawn:		;
RTS		;

SpawnBone:	;

LDA #$0B	; extended sprite number
STA $170B,y	; 06 = bone

LDA $D8,x	; sprite Y position

STA $1715,y	;
LDA $14D4,x	;
SBC #$00	; handle overflow
STA $1729,y	;

LDA $E4,x	; sprite X position
STA $171F,y	; no X offset at all
LDA $14E0,x	;
STA $1733,y	;

LDA $00
STA $1747,y	; extended sprite X speed table
LDA #$D0
STA $173D,y
RTS		;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

QuitGraphics2:
JMP QuitGraphics


Graphics:
LDA $1540,x
BNE QuitGraphics2

		
        %GetDrawInfo()      ;A:87BF X:0007 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1102 VC:066 00 FL:665

	LDA $7FAB10,x
	AND #$04
	BNE Dynamic

	LDA.b #Tilemap
	STA $0B
	LDA.b #Tilemap>>8
	STA $0C
	LDA.b #Tilemap>>16
	STA $0D
	BRA DoneWithDrawingType
Dynamic:
	LDA FreeRAM
	STA $0D
	LDA FreeRAM+1
	STA $0C
	LDA FreeRAM+2
	STA $0B

DoneWithDrawingType:


		LDA $1528,x
		CLC				;ASDF CARRY YOU ARE THE BANE OF MY EXISTANCE POPPING UP IN PLACES WHERE I LEAST EXPECT YOU (read: before I added CLC carry was causing graphical glitchiness)
		ROL A
		STA $0E

LDA $157C,X
STA $04
		    LDA $14
		    AND #$03
                    PHX                     ; /
                    TAX

                    PHY                     ; set tile to be 16x16
                    TYA                     ;
                    LSR A                   ;
                    LSR A                   ;
                    TAY                     ;
                    LDA #$02	            ; 
                    STA $0460,y             ;
                    PLY                     ;
                    
                    LDA $00                 ; \ tile x position = sprite x location ($00) + tile displacement
			CLC
			ADC $04
                    STA $0300,y             ; /
                    
                    LDA $01                 ; |
                    STA $0301,y             ; /

			LDA $0E
			CMP #$0A
			BEQ DoSpecialTileMap
			PHY
			TXY
                    LDA [$0B],y           ; \ store tile
			PLY
                    STA $0302,y             ; /
			BRA DontDoSpecialTileMap
			DoSpecialTileMap:
			
			STX $00
			PLX
			LDA $7FAB10,x
			PHX
			LDX $00
			AND #$04
			BNE Dynamic2
			
			LDA #$82			;TILE FOR THE "FINAL ATTACK" VIAL
			STA $0302,y
			BRA DontDoSpecialTileMap
			Dynamic2:
			LDA !FreeRAM2
			STA $0302,y




			DontDoSpecialTileMap:

			LDA $0E
			CMP #$0A
			BEQ DoSpecialProperty
                    LDA PROPERTIES,x          ; flip tile if necessary
			BRA DontDoSpecialProperty
			DoSpecialProperty:
			LDA #$21			;PROPERTY FOR THE "FINAL ATTACK" VIAL
			DontDoSpecialProperty:


                    ORA $64                 ; add in tile priority of level
		    
phx : ldx $15e9 : ora !C2,x : plx

                    STA $0303,y             ; store tile properties

                    PLX                     ; pull, X = sprite index
                    LDY #$FF                ; \ we've already set 460 so use FF
                    LDA #$00                ; | A = number of tiles drawn - 1
                    JSL $81B7B3             ; / don't draw if offscreen
QuitGraphics:       RTS                     ; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Shatter Vial Routine.  This is ripped directly from the Yoshi's Egg sprite.  Yay for no documentation! =D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ShatterVial:

CODE_01F7C8:	JSR.w IsSprOffScreen      
CODE_01F7CB:	BNE Return01F82C          
CODE_01F7CD:	LDA $E4,x
CODE_01F7CF:	STA $00                   
CODE_01F7D1:	LDA $D8,x   
CODE_01F7D3:	STA $02                   
CODE_01F7D5:	LDA.w $14D4,x
CODE_01F7D8:	STA $03                   
CODE_01F7DA:	PHX                       
CODE_01F7DB:	LDY.b #$03                
CODE_01F7DD:	LDX.b #$0B                
CODE_01F7DF:	LDA.w $17F0,X             
CODE_01F7E2:	BEQ CODE_01F7F4           
CODE_01F7E4:	DEX                       
CODE_01F7E5:	BPL CODE_01F7DF           
CODE_01F7E7:	DEC.w $185D               
CODE_01F7EA:	BPL CODE_01F7F1           
CODE_01F7EC:	LDA.b #$0B                
CODE_01F7EE:	STA.w $185D               
CODE_01F7F1:	LDX.w $185D               
CODE_01F7F4:	LDA.b #$03                
CODE_01F7F6:	STA.w $17F0,X             
CODE_01F7F9:	LDA $00                   
CODE_01F7FB:	CLC                       
CODE_01F7FC:	ADC.w DATA_01F831,Y       
CODE_01F7FF:	STA.w $1808,X             
CODE_01F802:	LDA $02                   
CODE_01F804:	CLC                       
CODE_01F805:	ADC.w DATA_01F82D,Y       
CODE_01F808:	STA.w $17FC,X             
CODE_01F80B:	LDA $03                   
CODE_01F80D:	STA.w $1814,X             
CODE_01F810:	LDA.w DATA_01F835,Y       
CODE_01F813:	STA.w $1820,X             
CODE_01F816:	LDA.w DATA_01F839,Y       
CODE_01F819:	STA.w $182C,X             
CODE_01F81C:	TYA                       
CODE_01F81D:	ASL                       
CODE_01F81E:	ASL                       
CODE_01F81F:	ASL                       
CODE_01F820:	ASL                       
CODE_01F821:	ASL                       
CODE_01F822:	ASL                       
CODE_01F823:	ORA.b #$28                
CODE_01F825:	STA.w $1850,X             
CODE_01F828:	DEY                       
CODE_01F829:	BPL CODE_01F7E4           
CODE_01F82B:	PLX                       
Return01F82C:	RTS                       ; Return 

IsSprOffScreen:	LDA.w $15A0,X ; \ A = Current sprite is offscreen 
CODE_0180CE:	ORA.w $186C,X ; /  
Return0180D1:	RTS                       ; Return 

DATA_01F82D:                      db $00,$00,$08,$08

DATA_01F831:                      db $00,$08,$00,$08

DATA_01F835:                      db $E8,$E8,$F4,$F4

DATA_01F839:                      db $FA,$06,$FD,$03