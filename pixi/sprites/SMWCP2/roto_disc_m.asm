
; Roto Disc

; Uses first extra bit: YES
; Set the first extra bit to make the roto disc go counter clockwise

!FREESPACE = $7F8900

          !YLO_1 = !FREESPACE+0
          !YLO_2 = !FREESPACE+12
          !YLO_3 = !FREESPACE+24
          !YLO_4 = !FREESPACE+36
          !YLO_5 = !FREESPACE+48
          !YLO_6 = !FREESPACE+60
          !YLO_7 = !FREESPACE+72
          !YLO_8 = !FREESPACE+84
          !YLO_9 = !FREESPACE+96
          !YLO_10 = !FREESPACE+108
          !YLO_11 = !FREESPACE+120
          !YLO_12 = !FREESPACE+132
          !YLO_13 = !FREESPACE+144
          !YLO_14 = !FREESPACE+156
          !YLO_15 = !FREESPACE+168
          !YLO_16 = !FREESPACE+180
          !YLO_17 = !FREESPACE+192
          !YLO_18 = !FREESPACE+204
          !YLO_19 = !FREESPACE+216
          !YLO_20 = !FREESPACE+228
          !YLO_21 = !FREESPACE+240
          !YLO_22 = !FREESPACE+252
          !YLO_23 = !FREESPACE+264
          !YLO_24 = !FREESPACE+276
          !YLO_25 = !FREESPACE+288
          !YLO_26 = !FREESPACE+300
          !YLO_27 = !FREESPACE+312
          !YLO_28 = !FREESPACE+324
          !YLO_29 = !FREESPACE+336
          !YLO_30 = !FREESPACE+348
          !YLO_31 = !FREESPACE+360
          !YLO_32 = !FREESPACE+372

          !XLO_1 = !FREESPACE+384
          !XLO_2 = !FREESPACE+396
          !XLO_3 = !FREESPACE+408
          !XLO_4 = !FREESPACE+420
          !XLO_5 = !FREESPACE+432
          !XLO_6 = !FREESPACE+444
          !XLO_7 = !FREESPACE+456
          !XLO_8 = !FREESPACE+468
          !XLO_9 = !FREESPACE+480
          !XLO_10 = !FREESPACE+492
          !XLO_11 = !FREESPACE+504
          !XLO_12 = !FREESPACE+516
          !XLO_13 = !FREESPACE+528
          !XLO_14 = !FREESPACE+540
          !XLO_15 = !FREESPACE+552
          !XLO_16 = !FREESPACE+564
          !XLO_17 = !FREESPACE+576
          !XLO_18 = !FREESPACE+588
          !XLO_19 = !FREESPACE+600
          !XLO_20 = !FREESPACE+612
          !XLO_21 = !FREESPACE+624
          !XLO_22 = !FREESPACE+636
          !XLO_23 = !FREESPACE+648
          !XLO_24 = !FREESPACE+660
          !XLO_25 = !FREESPACE+672
          !XLO_26 = !FREESPACE+684
          !XLO_27 = !FREESPACE+696
          !XLO_28 = !FREESPACE+708
          !XLO_29 = !FREESPACE+720
          !XLO_30 = !FREESPACE+732
          !XLO_31 = !FREESPACE+744
          !XLO_32 = !FREESPACE+756


TILE:  db $8A,$8C
!TILEFOLLOWER = $A2


                    !RADIUS = $38
                    !CLOCK_SPEED = $03
                    !COUNTER_CLOCK_SPEED = $FD
                    !CIRCLE_COORDS = $87F7DB
                    
                    !EXTRA_BITS = $7FAB10
                    !EXTRA_PROP_2 = $7FAB34


; sprite initialization JSL


                    PRINT "INIT ",pc
            
                    LDA #!RADIUS
                    ;LDA !EXTRA_PROP_2,x                 
                    STA $187B,x
                    
                    LDA #$80                ;set initial clock position
                    STA $1602,x
                    TXA
                    AND #$01
                    STA $151C,x         

		LDA $D8,x
		STA !YLO_1,x
		STA !YLO_2,x
		STA !YLO_3,x
		STA !YLO_4,x
		STA !YLO_5,x
		STA !YLO_6,x
		STA !YLO_7,x
		STA !YLO_8,x
		STA !YLO_9,x
		STA !YLO_10,x
		STA !YLO_11,x
		STA !YLO_12,x
		STA !YLO_13,x
		STA !YLO_14,x
		STA !YLO_15,x
		STA !YLO_16,x
		STA !YLO_17,x
		STA !YLO_18,x
		STA !YLO_19,x
		STA !YLO_20,x
		STA !YLO_21,x
		STA !YLO_22,x
		STA !YLO_23,x
		STA !YLO_24,x
		STA !YLO_25,x
		STA !YLO_26,x
		STA !YLO_27,x
		STA !YLO_28,x
		STA !YLO_29,x
		STA !YLO_30,x
		STA !YLO_31,x
		STA !YLO_32,x
		LDA $E4,x
		STA !XLO_1,x
		STA !XLO_2,x
		STA !XLO_3,x
		STA !XLO_4,x
		STA !XLO_5,x
		STA !XLO_6,x
		STA !XLO_7,x
		STA !XLO_8,x
		STA !XLO_9,x
		STA !XLO_10,x
		STA !XLO_11,x
		STA !XLO_12,x
		STA !XLO_13,x
		STA !XLO_14,x
		STA !XLO_15,x
		STA !XLO_16,x
		STA !XLO_17,x
		STA !XLO_18,x
		STA !XLO_19,x
		STA !XLO_20,x
		STA !XLO_21,x
		STA !XLO_22,x
		STA !XLO_23,x
		STA !XLO_24,x
		STA !XLO_25,x
		STA !XLO_26,x
		STA !XLO_27,x
		STA !XLO_28,x
		STA !XLO_29,x
		STA !XLO_30,x
		STA !XLO_31,x
		STA !XLO_32,x
		RTL
                    
                    

; sprite main JSL

            
                    PRINT "MAIN ",pc                        
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    JSR START_SPRITE_CODE   ;  |
                    PLB                     ;  |
                    RTL                     ; /



; main sprite sprite code


START_SPRITE_CODE:   LDA #$01 : %SubOffScreen()
		    LDA $9D
                    BNE LABEL92
                    LDA !EXTRA_BITS,x
                    LDY #!CLOCK_SPEED
                    AND #$04
                    BNE LABEL90
                    LDY #!COUNTER_CLOCK_SPEED
LABEL90:             TYA
                    LDY #$00
                    CMP #$00
                    BPL LABEL91
                    DEY
LABEL91:             CLC
                    ADC $1602,x
                    STA $1602,x
                    TYA
                    ADC $151C,x
                    AND #$01
                    STA $151C,x
LABEL92:             LDA $151C,x
                    STA $01
                    LDA $1602,x
                    STA $00
                    REP #$30
                    LDA $00
                    CLC
                    ADC.w #$0080
                    AND.w #$01FF
                    STA $02
                    LDA $00
                    AND.w #$00FF
                    ASL A
                    TAX
                    LDA $07F7DB,x
                    STA $04
                    LDA $02
                    AND.w #$00FF
                    ASL A
                    TAX
                    LDA $07F7DB,x
                    STA $06
                    SEP #$30
                    LDX $15E9
                    LDA $04
                    STA $4202
                    LDA $187B,x
                    LDY $05
                    BNE LABEL93
                    STA $4203
                    ASL $4216
                    LDA $4217
                    ADC #$00
LABEL93:             LSR $01
                    BCC LABEL94
                    EOR #$FF                ; \ reverse direction of rotation
                    INC A                   ; /
LABEL94:             STA $04
                    LDA $06
                    STA $4202
                    LDA $187B,x
                    LDY $07
                    BNE LABEL95
                    STA $4203
                    ASL $4216
                    LDA $4217
                    ADC #$00
LABEL95:             LSR $03
                    BCC LABEL96
                    EOR #$FF
                    INC A
LABEL96:             STA $06
                    LDA $E4,x
                    PHA
                    LDA $14E0,x
                    PHA
                    LDA $D8,x
                    PHA
                    LDA $14D4,x
                    PHA
                    LDY $0F86,x
                    STZ $00
                    LDA $04
                    BPL LABEL97
                    DEC $00
LABEL97:             CLC
                    ADC $E4,x
                    STA $E4,x
                    PHP
                    PHA
                    SEC
                    SBC $1534,x
                    STA $1528,x
                    PLA
                    STA $1534,x
                    PLP
                    LDA $14E0,x
                    ADC $00
                    STA $14E0,x
                    STZ $01
                    LDA $06
                    BPL LABEL98
                    DEC $01
LABEL98:             CLC
                    ADC $D8,x
                    STA $D8,x
                    LDA $14D4,x
                    ADC $01
                    STA $14D4,x
            
LDA $9D
BNE SKIP
		    JSR LOGPOSITION	
SKIP:     
                    JSL $01A7DC             ; check for mario/sprite contact
                    BCC RETURN_EXTRA          ; (carry set = mario/sprite contact)
                    LDA $1490               ; \ if mario star timer > 0 ...
                    BNE HAS_STAR            ; /    ... goto HAS_STAR

SPRITE_WINS:         LDA $1497               ; \ if mario is invincible...
                    ORA $187A               ;  }  ... or mario on yoshi...
                    BNE RETURN_EXTRA          ; /   ... return
                    JSL $00F5B7             ; hurt mario

RETURN_EXTRA:        JSR SUB_GFX
                    
                    LDA $14C8,x             ; \ if sprite status != 8...
                    CMP #$08
                    BEQ ALIVE
                    
                    PLA
                    PLA
                    PLA
                    PLA
                    BRA DONE
                    
ALIVE:               PLA     
                    STA $14D4,x
                    PLA        
                    STA $D8,x  
                    PLA        
                    STA $14E0,x
                    PLA        
                    STA $E4,x                   
                    
DONE:                LDY #$02                ; \ 02 because we haven't written to $0460
                    LDA #$00                ; | A = number of tiles drawn - 1
                    JSL $01B7B3             ; / don't draw if offscreen
               
                    RTS

HAS_STAR:            LDA #$02                ; \ sprite status = 2 (being killed by star)
                    STA $14C8,x             ; /
                    LDA #$D0                ; \ set y speed
                    STA $AA,x               ; /
                    %SubHorzPos()         ; get new sprite direction
                    LDA KILLED_X_SPEED,y    ; \ set x speed based on sprite direction
                    STA $B6,x               ; /
                    INC $18D2               ; increment number consecutive enemies killed
                    LDA $18D2               ; \
                    CMP #$08                ; | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET2           ; |
                    LDA #$08                ; |
                    STA $18D2               ; /   
NO_RESET2:           JSL $02ACE5             ; give mario points
                    LDY $18D2               ; \ 
                    CPY #$08                ; | if consecutive enemies stomped < 8 ...
                    BCS NO_SOUND2           ; |
                    LDA STAR_SOUNDS,y       ; |    ... play sound effect
                    STA $1DF9               ; /
NO_SOUND2:           BRA RETURN_EXTRA        ; final return



; graphics routine - specific to sprite




SUB_GFX:             %GetDrawInfo()       ; after: Y = index to sprite TILE map ($300)
                                            ;      $00 = sprite x position relative to screen boarder 
                                            ;      $01 = sprite y position relative to screen boarder  

        LDA $14                     
        AND #$02                                    
        STA $03 

                    LDA $00                 ; TILE x position
                    STA $0300,y             ; 

                    LDA $01                 ; TILE y position
                    STA $0301,y             ; 


		    LDA $03
		    CLC
                    ADC TILE               ; store TILE
                    STA $0302,y             ;  
                    PHX
                    
                    ;LDX $15E9
                    ;LDA $15F6,x            ; load TILE pallette
                    
                    LDA $14
                    ;LSR A
                    AND #$07
                    ASL A

                    ORA #$01
                    ;TAX
                    ;LDA PALS,x
                    ORA $64                    
                    STA $0303,y             ; store TILE properties
                    
                    PLX
                   
                    INY                     ; | increase index to sprite TILE map ($300)...
                    INY                     ; |    ...we wrote 4 bytes of data...
                    INY                     ; |    
                    INY                     ; |    ...so increment 4 times

LDA $14
AND #$02
BNE FLASH
   		LDA $00			; \
		CLC			;  | set X
		ADC !XLO_6,x		;  | position
		SEC			;  |
		SBC $E4,x		;  |
		STA $0300,y		; /
		LDA $01			; \
		CLC			;  | set Y
		ADC !YLO_6,x		;  | position
		SEC			;  |
		SBC $D8,x		;  |
		STA $0301,y		; /
		LDA #!TILEFOLLOWER	
		STA $0302,y		; set TILE number



                LDA $14
                AND #$07
                ASL A
                ORA #$01
                ORA $64 
		STA $0303,y             ; set properties
		INY			; \
		INY			;  | next OAM
		INY			;  | index
		INY			; /

LDA $14
LSR
AND #$04
BNE FLASH
   		LDA $00			; \
		CLC			;  | set X
		ADC !XLO_10,x		;  | position
		SEC			;  |
		SBC $E4,x		;  |
		STA $0300,y		; /
		LDA $01			; \
		CLC			;  | set Y
		ADC !YLO_10,x		;  | position
		SEC			;  |
		SBC $D8,x		;  |
		STA $0301,y		; /
		LDA #!TILEFOLLOWER	
		STA $0302,y		; set TILE number

                LDA $14
                AND #$07
                ASL A
                ORA #$01
                ORA $64 
		STA $0303,y             ; set properties
		INY			; \
		INY			;  | next OAM
		INY			;  | index
		INY			; 
FLASH:
		LDY #$02		
		LDA #$02		
                JSL $01B7B3             ; / don't draw if offscreen


                    RTS                     ; return



; points routine - unknown


KILLED_X_SPEED:      db $F0,$10
STAR_SOUNDS:         db $00,$13,$14,$15,$16,$17,$18,$19

SUB_STOMP_PTS:       PHY                     ; 
                    LDA $1697               ; \
                    CLC                     ;  } 
                    ADC $1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697               ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y       ; \ play sound effect
                    STA $1DF9               ; /   
NO_SOUND:            TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:            JSL $02ACE5             ; give mario points
                    PLY                     ;
                    RTS                     ; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LOGPOSITION:
		LDA !YLO_31,x
		STA !YLO_32,x
		LDA !YLO_30,x
		STA !YLO_31,x
		LDA !YLO_29,x
		STA !YLO_30,x
		LDA !YLO_28,x
		STA !YLO_29,x
		LDA !YLO_27,x
		STA !YLO_28,x
		LDA !YLO_26,x
		STA !YLO_27,x
		LDA !YLO_25,x
		STA !YLO_26,x
		LDA !YLO_24,x
		STA !YLO_25,x
		LDA !YLO_23,x
		STA !YLO_24,x
		LDA !YLO_22,x
		STA !YLO_23,x
		LDA !YLO_21,x
		STA !YLO_22,x
		LDA !YLO_20,x
		STA !YLO_21,x
		LDA !YLO_19,x
		STA !YLO_20,x
		LDA !YLO_18,x
		STA !YLO_19,x
		LDA !YLO_17,x
		STA !YLO_18,x
		LDA !YLO_16,x
		STA !YLO_17,x
		LDA !YLO_15,x
		STA !YLO_16,x
		LDA !YLO_14,x
		STA !YLO_15,x
		LDA !YLO_13,x
		STA !YLO_14,x
		LDA !YLO_12,x
		STA !YLO_13,x
		LDA !YLO_11,x
		STA !YLO_12,x
		LDA !YLO_10,x
		STA !YLO_11,x
		LDA !YLO_9,x
		STA !YLO_10,x
		LDA !YLO_8,x
		STA !YLO_9,x
		LDA !YLO_7,x
		STA !YLO_8,x
		LDA !YLO_6,x
		STA !YLO_7,x
		LDA !YLO_5,x
		STA !YLO_6,x
		LDA !YLO_4,x
		STA !YLO_5,x
		LDA !YLO_3,x
		STA !YLO_4,x
		LDA !YLO_2,x
		STA !YLO_3,x
		LDA !YLO_1,x
		STA !YLO_2,x
		LDA $D8,x
		STA !YLO_1,x
		LDA !XLO_31,x
		STA !XLO_32,x
		LDA !XLO_30,x
		STA !XLO_31,x
		LDA !XLO_29,x
		STA !XLO_30,x
		LDA !XLO_28,x
		STA !XLO_29,x
		LDA !XLO_27,x
		STA !XLO_28,x
		LDA !XLO_26,x
		STA !XLO_27,x
		LDA !XLO_25,x
		STA !XLO_26,x
		LDA !XLO_24,x
		STA !XLO_25,x
		LDA !XLO_23,x
		STA !XLO_24,x
		LDA !XLO_22,x
		STA !XLO_23,x
		LDA !XLO_21,x
		STA !XLO_22,x
		LDA !XLO_20,x
		STA !XLO_21,x
		LDA !XLO_19,x
		STA !XLO_20,x
		LDA !XLO_18,x
		STA !XLO_19,x
		LDA !XLO_17,x
		STA !XLO_18,x
		LDA !XLO_16,x
		STA !XLO_17,x
		LDA !XLO_15,x
		STA !XLO_16,x
		LDA !XLO_14,x
		STA !XLO_15,x
		LDA !XLO_13,x
		STA !XLO_14,x
		LDA !XLO_12,x
		STA !XLO_13,x
		LDA !XLO_11,x
		STA !XLO_12,x
		LDA !XLO_10,x
		STA !XLO_11,x
		LDA !XLO_9,x
		STA !XLO_10,x
		LDA !XLO_8,x
		STA !XLO_9,x
		LDA !XLO_7,x
		STA !XLO_8,x
		LDA !XLO_6,x
		STA !XLO_7,x
		LDA !XLO_5,x
		STA !XLO_6,x
		LDA !XLO_4,x
		STA !XLO_5,x
		LDA !XLO_3,x
		STA !XLO_4,x
		LDA !XLO_2,x
		STA !XLO_3,x
		LDA !XLO_1,x
		STA !XLO_2,x
		LDA $E4,x
		STA !XLO_1,x

		RTS