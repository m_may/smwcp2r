;Bonus Magician (a.k.a. dumb stopwatch figure), recoded by Blind Devil

;the original sprite code was awfully and unnecessarily big so I had to
;kill myself twice and code a new one from scratch, yeah ehhh

;BTW now the extension byte 1 determines the amount of seconds the clock will have on init. much more flexible, yep

;also yeah, um... okay, the sine movement isn't smooth like the original one, but I wanted a reduced sprite code so I chose a different path

;if it makes you happy, original was supposed to have hands... and didn't

!SprXPos = $C8
!SprYPos = $28

Tiles:		db $AC,$AC		;Cape
		db $AA,$AA		;Feet
		db $AE,$AE,$AE,$AE	;Body
		db $8C,$8D		;Hat
		db $8A,$8A		;Hands

		!Mouth1 = $EC		;smiling
		!Mouth2 = $EE		;frowning

TimerTiles:
		db $C0,$C2,$C4,$C6,$C8,$CA,$CC,$CE,$E0,$E2	;numbers 0-9

XOffsets:
		db $00,$10
		db $00,$10
		db $00,$10,$00,$10
		db $FF,$07
		db $FA,$16
		db $08
		db $04,$0C

YOffsets:
		db $18,$18
		db $1E,$1E
		db $00,$00,$10,$10
		db $F8,$F8
		db $16,$16
		db $18
		db $08,$08

Properties:
		db $37,$77
		db $37,$77
		db $33,$73,$B3,$F3
		db $37,$37
		db $73,$33
		db $33
		db $3B,$3B

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA !7FAB40,x
STA !1528,x
LDA #$3C
STA !154C,x		;initialize frame count for a second of the timer

LDA #$14		;load amount of frames
STA !1540,x		;store to sine timer 1.
LDA #$1C		;load amount of frames
STA !1558,x		;store to sine timer 2.
LDA #$24		;load amount of frames
STA !15AC,x		;store to sine timer 3.

STZ !1FD6,x		;reset sine inverting flags.
STZ $0F5E|!Base2	;reset amount of seconds to add to stopwatch.

RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Main:
JSR Graphics		;draw graphics

REP #$20
LDA $1A
CLC
ADC #$00B8
STA $00
LDA $1C
CLC
ADC #$0018
STA $02
SEP #$20

LDA $00
STA !E4,x
LDA $01
STA !14E0,x
LDA $02
STA !D8,x
LDA $03
STA !14D4,x

LDA $9D			;load sprites/animation locked flag
BNE Quit		;if set, return.

LDA $0F5E|!Base2	;load second adder (from custom block)
BEQ +			;if there are no coins to add, skip ahead.

INC !1528,x		;increment one second every frame there are coins
DEC $0F5E|!Base2	;decrement second adder by one.
+

JSR HandleMoves		;handle GFX moving

LDA !154C,x		;load timer's timer value
BNE Quit		;if not zero, return.

LDA !1528,x		;load timer value
BNE +			;if not equal zero, just decrement it by one and reset the timer's timer

LDA $71			;load player animation pointer
CMP #$09		;check if player is dying
BEQ Quit		;if yes, then don't teleport.

LDA #$2A		;load incorrect SFX
STA $1DFC|!Base2	;play it lol

LDA #$06
STA $71
STZ $88
STZ $89			;do le warp, ya know
RTS

+
LDA #$1A		;load grinder click SFX
STA $1DF9|!Base2	;play it lol

LDA #$3C		;load value
STA !154C,x		;store to timer's timer.
DEC !1528,x		;decrement timer by one

Quit:
RTS

HandleMoves:
LDA $14			;load effective frame counter
AND #$07		;preserve bits 0, 1 and 2
BNE leret		;if set, return.

JSR counter1		;handle counter 1
JSR counter2		;handle counter 2
BRA counter3		;branch to handle counter 3

endinvert:
STA !1FD6,x		;store result back.

leret:
RTS			;return.

macro counterh(sinetimer,sineindex,sinebitflag)		;never liked macros but it won't hurt to use it here... laziness beat me, yeah
LDA <sinetimer>,x	;load sine timer X
BNE ?incordec		;if not equal zero, check if sine timer index is either incrementable or decrementable.

LDA #$24		;load amount of frames
STA <sinetimer>,x	;store to sine timer X.

LDA !1FD6,x		;load sine inverter flags
EOR #<sinebitflag>	;invert bit X
BRA endinvert		;branch to finish inverting respective bit

?incordec:
LDA !1FD6,x		;load sine inverter flags X
AND #<sinebitflag>	;preserve bit X
BEQ ?positive		;if not set, increase sine index.

LDA <sineindex>,x	;load sine index X
BEQ leret		;if equal the min amount (that is zero), return.

DEC <sineindex>,x	;decrement sine index X by one
RTS			;return.

?positive:
LDA <sineindex>,x	;load sine index X
CMP #$05		;check if equal the max amount
BEQ leret		;if equal, return.

INC <sineindex>,x	;decrement sine index X by one
RTS			;return.
endmacro

counter3:
%counterh(!15AC,!1594,$04)
counter2:
%counterh(!1558,!1534,$02)
counter1:
%counterh(!1540,!C2,$01)

Graphics:
LDY #!Mouth1		;load smiling mouth tile number

LDA !1528,x		;load timer value
CMP #$0A		;compare to value
BCS +			;if higher or equal, don't change tilemap value in A.

LDY #!Mouth2		;load frowning mouth tile number

+
TYA			;transfer to A
STA $02			;store to scratch RAM.

%GetDrawInfo()		;get OAM index and sprite positions within the screen (though they're not gonna be used here lol)

LDA !C2,x		;load sine index 1
STA $04			;store to scratch RAM.
LDA !1534,x		;load sine index 2
STA $05			;store to scratch RAM.
LDA !1594,x		;load sine index 3
STA $06			;store to scratch RAM.

LDA !1528,x		;load timer value
STA $09

PHX			;preserve sprite index

LDA $09
;HEX->DEC converter (A holds ones, X holds tens)
                     LDX #$00                ;> Set X to 0
StartCompare:
                     CMP #$0A                ;\
                     BCC +	             ; |While A >= 10:
                     SBC #$0A                ; |Decrease A by 10
                     INX                     ; |Increase X by 1
                     BRA StartCompare        ;/
+
STX $07			;store tens to scratch RAM.
STA $08			;store ones to scratch RAM.

;OAM draw order:
;> numbers
;> mouth
;> hands
;> hat
;> head
;> feet
;> cape


LDX #$0E		;loop count

.MainGFXLoop
LDA #!SprXPos		;load X offset
CLC			;clear carry
ADC XOffsets,x		;add X displacement from table according to index
STA $0300|!Base2,y	;store to OAM.

LDA Properties,x	;load palette/properties from table according to index
STA $0303|!Base2,y	;store to OAM.

CPX #$0D		;compare X to value
BCS .DrawNumbers	;if higher or equal, draw numbers.
CPX #$0C		;compare X to value
BEQ .DrawMouth		;if equal, draw the mouth.
CPX #$0A		;compare X to value
BCS .DrawHands		;if equal, draw hands.
CPX #$08		;compare X to value
BCS .DrawHat		;if equal, draw the hat.
CPX #$04		;compare X to value
BCS .DrawHead		;if equal, draw the head.
CPX #$02		;compare X to value
BCS .DrawFeet		;if equal, draw feet. else, draw the cape.

LDA Tiles,x		;load tile number from table according to index
STA $0302|!Base2,y	;store to OAM.

LDA #!SprYPos		;load Y offset
CLC			;clear carry
ADC YOffsets,x		;add Y displacement from table according to index
ADC $06			;add sine index 3 from scratch RAM
STA $0301|!Base2,y	;store to OAM.

.FinishLoop
INC $03			;increment tiles drawn count
INY #4			;increment Y four times
DEX			;decrement X by one
BPL .MainGFXLoop	;loop while it's positive.

.ender
PLX			;restore sprite index

LDY #$02		;make all tiles 16x16
LDA #$0E		;load number of tiles drawn, minus one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;a return at last.

.DrawMouth
LDA $02			;load tile number from scratch RAM
BRA .disps		;branch to set stuff

.DrawNumbers
PHX			;preserve loop count
;CPX #$0D		;compare X to value
BEQ .tens		;if equal, draw tens value.

LDX $08			;load ones value from scratch RAM into X
BRA .lolnum		;take tile number from index

.tens
LDX $07			;load tens value from scratch RAM into X

.lolnum
LDA TimerTiles,x	;load tile number from table according to index
PLX			;restore loop count
BRA .disps		;branch to set stuff

.DrawHat
.DrawHead
.DrawFeet
LDA Tiles,x		;load tile number from table according to index

.disps
STA $0302|!Base2,y	;store to OAM.

LDA #!SprYPos		;load Y offset
CLC			;clear carry
ADC $05			;add sine index 2 from scratch RAM
ADC YOffsets,x		;add Y displacement from table according to index
STA $0301|!Base2,y	;store to OAM.
BRA .FinishLoop		;branch to decrement loop count

.DrawHands
LDA Tiles,x		;load tile number from table according to index
STA $0302|!Base2,y	;store to OAM.

LDA #!SprYPos		;load Y offset
CLC			;clear carry
ADC $04			;add sine index 1 from scratch RAM
ADC YOffsets,x		;add Y displacement from table according to index
STA $0301|!Base2,y	;store to OAM.
BRA .FinishLoop		;branch to decrement loop count