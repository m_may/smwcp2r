;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Bone Pile
; By Sonikku
;
; Description: A sprite that stays wherever you put it, and if
; Mario gets close, turns into a Dry Bones that is getting up
; (rather than being a completely new sprite altogether).
; It turns into a normal Dry Bones or a Dry Bones that throws
; bones depending on its X position.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
    LDA !sprite_x_low,x
    LSR #4
    AND #$01
    STA !C2,x
    RTL

print "MAIN ",pc
    PHB : PHK : PLB
    JSR SpriteCode
    PLB
    RTL

Sprite:    db $30,$32
SpriteCode:
    JSR Graphics            ; load graphics
    LDA #$00                ; only process if on screen
    %SubOffScreen()
    LDA $9D                 ; if sprites locked..
    BNE Return              ; return
    LDA #$08                ; if sprites status..
    CMP !14C8,x             ; isn't 8..
    BNE Return              ; return
    JSL $01802A|!BankB      ; update position based on speed
    LDA !1588,x             ; if sprite is in the air
    AND #$04                ; it has y speed
    BEQ NoGround
    STZ !sprite_speed_y,x   ; if on ground, no y speed
NoGround:
    %SubHorzPos()           ; determine if Mario is close to this sprite
    REP #$20                ; Accum (16-bit)
    LDA $0E
    CLC : ADC #$0024
    CMP #$0050
    BCS Return              ; if he isn't, return
    SEP #$20                ; Accum (8-bit)

    LDY !C2,x

    LDA Sprite,y            ; load sprite to transform into
    STA !9E,x               ; and store it.

    JSL $07F7D2|!BankB      ; reset init

    LDA #$08                ; sprite's status is 8
    STA !14C8,x

    LDA #$01                ; sprites stomp status is 1 now
    STA !1534,x
    LDA #$30                ; set timer before getting up
    STA !1540,x
    LDA #$07                ; play sound effect
    STA $1DF9|!Base2
    LDA #$01                ; make it face the same direction as the original sprite
    STA !157C,x
Return:
    SEP #$20                ; Accum (8-bit)
    RTS

Tilemap:    db $2D,$2E
X_Disp:     db $F8,$00

Graphics:
    %GetDrawInfo()

    LDA !15F6,x
    STA $02

    LDX #$00
-   LDA $00
    CLC : ADC X_Disp,x
    STA $0300|!Base2,y

    LDA $01
    STA $0301|!Base2,y
    LDA Tilemap,x
    STA $0302|!Base2,y

    LDA $02
    ORA $64
    STA $0303|!Base2,y

    INY #4
    INX
    CPX #$02
    BNE -

    LDX $15E9|!Base2
    LDY #$02
    LDA #$01
    JSL $01B7B3|!BankB
    RTS
