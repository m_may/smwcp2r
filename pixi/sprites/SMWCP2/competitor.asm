;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!DIR = $03
!PROPRAM = $04

PRINT "INIT ",pc
	JSR SUBSETXPOS	; this actually jumps to the init routine of the exploding block
	STY $C2,x	; the Y register determines which statue it will be
	;LDA WALKTIME,y
	;STA $1540,x
ENDINIT:
	RTL

SUBSETXPOS:	; this determines which kind of statue it will be based on its X position
	LDA $E4,x	; sprite X position (low byte)
	LSR
	LSR
	LSR
	LSR		; divide by 16 to get it in terms of tiles
	AND #$03	; we want only 4 possible values
	TAY		; transfer this into Y (to determine the sprite that the exploding block would spawn)
	RTS		; but since this isn't an exploding block, we can just end right here

PRINT "MAIN ",pc
	PHB
	PHK
	PLB		
	JSR Run
	PLB
	RTL

RETURN:
	RTS

Run:

	LDA $7FAB10,x
 	AND #$04
	BEQ SKIP
 	JSR GFX_COIN
	RTS
SKIP:	JSR GFX

	LDA $14C8,x	; \ 
	CMP #$08	;  | if status != 8, RETURN
	BNE RETURN	; /
	LDA $9D		; \ if sprites locked, RETURN
	BNE RETURN	; /

	LDA $1602,x
	BEQ KEEPRUNNING

	LDA $1588,x	;sprite blocked status table xxxxUDLR
	AND #$04
	BEQ NOJUMP
	LDA #$E0
	STA $AA,x

NOJUMP:

	LDA $B6,x
	SEC
	SBC #$04
	BMI SETZERO
	STA $B6,x
	BRA UPDATE
	
SETZERO:
	STZ $B6,x
	BRA UPDATE
KEEPRUNNING:

	LDA $1DEF
	CMP #$01
	BEQ GOTIME
	JSR SUBSETXPOS	; this actually jumps to the init routine of the exploding block
	CPY #$03
	BEQ DONEWALK
	LDA #$08
	STA $B6,x
	BRA UPDATE

DONEWALK:
	LDA $1DEF
	CMP #$01
	BEQ GOTIME
	STZ $B6,x
	BRA UPDATE
	

GOTIME:
	LDA $C2,x
	TAY
	LDA RUNSPEED,y
	;LDA #$70
	STA $B6,x

UPDATE:
	JSL $01802A	;UPDATE position based on speed values
	
	RTS

LEVELASM:

	LDA $13C8
	BNE DECRAM
	LDA $16
	AND #$01
	BNE MOVE
	BRA DONE
DECRAM:
	DEC $13C8
	LDA $16
	AND #$01
	BNE BOOST
	BRA DONE
BOOST:
	LDA $7B
	CLC
	ADC #$18
	CLC
	CMP #$7F
	BCC NOSLOW
	LDA #$7F
NOSLOW:
	STA $7B
MOVE:
	LDA #$0A
	STA $13C8

DONE:
	RTS

RUNSPEED:
	db $6F,$58,$40,$00
WALKTIME:
	db $28,$28,$28,$28
;;;;;;;;;;;;;;;;;;;;;;;
;GFX routine
;;;;;;;;;;;;;;;;;;;;;;;

RACETURTLECOLOR:
		db $03,$0B,$0D,$0F
TILEMAP:	db $A0,$4E
	db $A2,$82

;XDISP:	db $00,$00
;	db $00,$10

YDISP:	db $00,$F0
	db $10,$10

GFX:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()

	LDA $157C,x	;direction...
	STA !DIR

	LDA $15F6,x	;properties...
	ORA #$64	;flip the X direction
	STA !PROPRAM

	LDX #$00

OAM_LOOP:

	LDA $00
	STA $0300,y	;xpos

	LDA $01
	CLC
	ADC YDISP,x
	STA $0301,y	;ypos

	PHX
	LDX $15E9
	LDA $B6,x	;general purpose timer
	PLX
	CMP #$00
	BEQ NORMAL	;don't animate if not moving horizontal
	CMP #$3A
	BCS RUNFAST	;animate quickly if running fast
	
	LDA $14		;animate medium if moving, but not quickly
	AND #$08
	BNE NORMAL
	

FASTER:
	INX
	INX
	LDA TILEMAP,x
	STA $0302,y
	LDA $0301,y
	INC
	STA $0301,y
	DEX
	DEX
	BRA DUNN

RUNFAST:
	LDA $14
	AND #$02
	BNE FASTER


NORMAL:
	LDA TILEMAP,x
	STA $0302,y	;chr

DUNN:
	PHX
	LDX $15E9
	LDA $C2,x
	TAX
	LDA $04
	;AND #$0E
	ORA RACETURTLECOLOR,x
	;LDA !PROPRAM
	ORA $64
	STA $0303,y	;prop
	PLX

	INY
	INY
	INY
	INY
	INX

	CPX #$02
	BNE OAM_LOOP

	LDX $15E9	;restore sprite index
	LDY #$02	;16x16
	LDA #$01	;2 tiles
	JSL $01B7B3	;reserve

	RTS

TILEMAP_NUM: db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5,$4D,$67

GFX_COIN:

	LDA #$B0
	STA $0200,y	;xpos COIN
	LDA #$C0
	STA $0204,y	;xpos NUM1
	LDA #$C8
	STA $0208,y	;xpos NUM2
	LDA #$D0
	STA $020C,y	;xpos NUM3

	LDA #$28
	STA $0201,y	;ypos COIN
	STA $0205,y	;ypos NUM1
	STA $0209,y	;ypos NUM2
	STA $020D,y	;ypos NUM3

	LDA #$DB
	STA $0202,y

	LDA $140C
	STA $7F8888
	CMP #$64
	BCC .not100
	SEC : SBC #$64
	STA $7F8888
	LDA TILEMAP_NUM+1
	STA $0206,y
	BRA +
.not100
	LDA TILEMAP_NUM
	STA $0206,y
+
	PHX

	; ones digit
	LDA $7F8888
-	TAX
	CMP #$0A
	BCC +
	SEC : SBC #$0A
	BRA -
+	LDA TILEMAP_NUM,x
	STA $020E,y

	; tens digit
	LDX #$00
	LDA $7F8888
-	CMP #$0A
	BCC +
	SEC : SBC #$0A
	INX
	BRA -
+	LDA TILEMAP_NUM,x
	STA $020A,y

	PLX

	LDA #$36
	STA $0203,y	;prop
	STA $0207,y	;prop
	STA $020B,y	;prop
	STA $020F,y	;prop

	LDX $15E9	;restore sprite index
	PHY
	;LDY #$00	;8X8
	;LDA #$03	;4 tiles
	;JSL $01B7B3	;reserve

	; what the fuck, do it manually
	PLY
	TYA
	LSR
	LSR
	TAX
	STZ $0420,x
	STZ $0421,x
	STZ $0422,x
	STZ $0423,x

	LDX $15E9	;restore sprite index

	RTS