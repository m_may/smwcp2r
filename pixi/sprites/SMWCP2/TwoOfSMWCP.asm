;Number Two + (c) 201x-20xx text (titlescreen sprites)
;by Blind Devil

;leod originally did the number sprite but it needed serious optimization so I redid it.
;The copyright thingy is spawned by setting the extra bit.

!ram = $62		;determines displaying the 2
!ram2 = $0F5E|!Base2	;determines displaying the copyright text

!Num2Prop = $03		;palette/properties/flip for number 2, YXPPCCCT format
!CProp = $00		;palette/properties/flip for copyright thingy, YXPPCCCT format

!giepy = 0		;0 for pixi, 1 for giepy


print "INIT ",pc
LDA !7FAB10,x
AND #$04
BNE NotThe2

LDA !D8,x
SEC
SBC #$08
STA !D8,x
LDA !14D4,x
SBC #$00
STA !14D4,x

LDA !E4,x
CLC
ADC #$04
STA !E4,x

NotThe2:
RTL

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
RTL

SpriteCode:
LDA !7FAB10,x		;load sprite extra bits
AND #$04		;check if first extra bit is set
BNE CPtext		;if yes, then it's a copyright text.

LDA !ram		;load RAM flag 1 (used on titlescreen code)
BEQ Return1		;if equal zero, don't display the sprite.

JSR TwoGFX		;call graphics drawing routine

LDA !C2,x		;load sprite (timer) flag
BNE +			;if not equal zero, skip ahead.

LDA #$08		;load amount of frames
STA !15AC,x		;store to sprite timer.
INC !C2,x		;increment flag
RTS			;return.

+
LDA $13
AND #$01
BNE Return1

LDA !15AC,x		;load sprite timer
BNE Return1		;if not equal zero, return.

LDA !1528,x		;load tilemap index of sprite
CMP #$0D		;compare with max number of frames
BEQ ResetTimerAndAni	;if equal, set index back to zero and timer to #$FF

INC !1528,x		;increment tilemap index by one

Return1:
RTS			;return.

ResetTimerAndAni:
LDA #$FF		;load amount of frames
STA !15AC,x		;store to sprite timer.
STZ !1528,x		;reset tilemap index.
RTS			;return.

CPtext:
LDA !ram2		;load RAM flag 2 (used on titlescreen code)
BEQ Return2		;if equal zero, don't display the sprite.

JSR CPGFX		;call graphics drawing routine

Return2:
RTS			;return.

Tilemap2:
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44		;index 0
;db $00,$20,$40 : db $06,$22,$42 : db $08,$24,$44		;index 1
;db $00,$20,$40 : db $0A,$22,$42 : db $0C,$24,$44		;index 2
;db $0E,$20,$40 : db $26,$22,$42 : db $28,$24,$44		;index 3
;db $00,$20,$40 : db $02,$2A,$42 : db $04,$2C,$44		;index 4
;db $2E,$4E,$6E : db $60,$64,$46 : db $62,$66,$48		;index 5
;db $00,$4A,$6A : db $02,$4C,$6C : db $04,$24,$44		;index 6
;db $00,$20,$68 : db $02,$22,$42 : db $04,$24,$44		;index 7
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index 8
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index 9
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index A
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index B
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index C
;db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;index D

;more highlighted animation
db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44		;index 0
db $00,$20,$40 : db $06,$22,$42 : db $08,$24,$44	;1, index 1
db $00,$20,$40 : db $06,$22,$42 : db $08,$24,$44	;2, same as 1
db $00,$20,$40 : db $0A,$22,$42 : db $0C,$24,$44	;3, index 2
db $00,$20,$40 : db $0A,$22,$42 : db $0C,$24,$44	;4, same as 3
db $0E,$20,$40 : db $26,$22,$42 : db $28,$24,$44	;5, index 3
db $0E,$20,$40 : db $26,$22,$42 : db $28,$24,$44	;6, same as 5
db $00,$20,$40 : db $02,$2A,$42 : db $04,$2C,$44	;7, index 4
db $2E,$4E,$6E : db $60,$64,$46 : db $62,$66,$48	;8, index 5
db $2E,$4E,$6E : db $60,$64,$46 : db $62,$66,$48	;9, same as 8
db $00,$4A,$6A : db $02,$4C,$6C : db $04,$24,$44	;A, index 6
db $00,$4A,$6A : db $02,$4C,$6C : db $04,$24,$44	;B, same as A
db $00,$20,$68 : db $02,$22,$42 : db $04,$24,$44	;C, index 7
db $00,$20,$40 : db $02,$22,$42 : db $04,$24,$44	;D, same as C

XDisp2:
db $F8,$F8,$F8,$08,$08,$08,$18,$18,$18

YDisp2:
db $05,$15,$25,$05,$15,$25,$05,$15,$25

TwoGFX:
if !giepy
	JSL GetDrawInfo
else
	%GetDrawInfo()
endif

STZ $02			;reset scratch RAM.

PHY			;preserve OAM index
LDA !1528,x		;load tilemap table index
TAY			;transfer to Y
-
CPY #$00		;compare Y to value
BEQ +			;if equal, branch out of the loop.

LDA $02			;load scratch RAM containing true tilemap table index
CLC			;clear carry
ADC #$09		;add value
STA $02			;store result back.

DEY			;decrement Y by one
BRA -			;branch back

+
PLY			;restore OAM index

PHX			;preserve sprite index
LDX #$08		;loop count
GFX2Loop:
LDA $00			;load sprite's X-pos within the screen
CLC			;clear carry
ADC XDisp2,x		;add displacement value from table according to index
STA $0300|!Base2,y	;store to OAM.
LDA $01			;load sprite's Y-pos within the screen
CLC			;clear carry
ADC YDisp2,x		;add displacement value from table according to index
STA $0301|!Base2,y	;store to OAM.
PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $02			;add tilemap table index
TAX			;transfer back to X
LDA Tilemap2,x		;load tilemap from table according to index
STA $0302|!Base2,y	;store to OAM.
LDA #!Num2Prop		;load palette/properties
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.
PLX			;restore loop count
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL GFX2Loop		;loop while it's positive.
PLX			;restore sprite index

LDA #$08		;load number of tiles drawn, minus one

FinishOAMWrite:
LDY #$02		;load value into Y (all tiles are 16x16)
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.

CTilemap:
db $C0,$C2,$C4,$C6,$C8

CxDisp:
db $F8,$08,$18,$28,$38

;BELOW: GFX ROUTINE FOR (C) THINGY
CPGFX:
if !giepy
	JSL GetDrawInfo
else
	%GetDrawInfo()
endif

PHX			;preserve sprite index
LDX #$04		;loop count
GFXCLoop:
LDA $00			;load sprite's X-pos within the screen
CLC			;clear carry
ADC CxDisp,x		;add displacement value from table according to index
STA $0300|!Base2,y	;store to OAM.
LDA $01			;load sprite's Y-pos within the screen
CLC			;clear carry
ADC #$08		;add value
STA $0301|!Base2,y	;store to OAM.
LDA CTilemap,x		;load tilemap from table according to index
STA $0302|!Base2,y	;store to OAM.
LDA #!CProp		;load palette/properties
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL GFXCLoop		;loop while it's positive.
PLX			;restore sprite index

LDA #$04		;load number of tiles drawn, minus one
BRA FinishOAMWrite	;finish OAM write and return