;Custom Platform that falls after some time, by Blind Devil
;A platform that stands in place, until the player stands on it. Then it starts moving.
;After some time, it will shake, and fall straight down.

;Extra byte (extension) 1 defines the moving direction.
;> 00: up
;> 01: down
;> 02: left
;> 03: right

;Tilemap tables/defines:
;leftmost and rightmost tiles:
PlatTiles:
db $60,$62

;middle tile (up, down, left, right):
MidTile:
db $61,$61,$61,$61

;Speed tables:
XSpeedPlat:
db $00,$00,$F0,$10	;up, down, left, right
YSpeedPlat:
db $F0,$10,$00,$00	;up, down, left, right

;shaking offset (in pixels):
ShakeOffset:
dw $FFFE,$0002

;Time before shaking:
!RideTime = $90

;Time to shake (platform keeps moving):
!ShakeTime = $30

;Code starts below.

print "MAIN ",pc
PHB			;preserve data bank
PHK			;push program bank into stack
PLB			;pull back as new data bank
JSR SpriteCode		;call main sprite code
PLB			;restore previous data bank
print "INIT ",pc
RTL			;return.

Return:
RTS			;return.

SpriteCode:
LDA !7FAB40,x		;load extra byte 1 value
AND #$03		;preserve bits 0 and 1 only
STA !1510,x		;store to sprite RAM.

JSR Graphics		;call graphics drawing routine

LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE Return		;if not, return.
LDA $9D			;load sprites/animation locked flag
BNE Return		;if set, return.

LDA #$03		;load value
%SubOffScreen()		;call offscreen handling routine

LDA !C2,x		;load sprite state
JSL $0086DF|!BankB	;call 2-byte pointer subroutine
dw Idle			;00: on place
dw Moving		;01: moving
dw MoveShake		;02: moving (but shaking)
dw Falling		;03: falling

Idle:
STZ !B6,x		;reset sprite's X speed.
STZ !AA,x		;reset sprite's Y speed.
BRA Falling		;update sprite positions and run platform interaction routine

Moving:
INC !1528,x		;increment address by one (our platform timer)
LDA !1528,x		;load platform timer
CMP #!RideTime		;compare to value
BCS ChangeToShake	;if higher or equal, change sprite state.

ActuallyMove:
LDY !1510,x		;load platform direction into Y
LDA XSpeedPlat,y	;load speed value from table according to index
STA !B6,x		;store to sprite's X speed.
LDA YSpeedPlat,y	;load speed value from table according to index
STA !AA,x		;store to sprite's Y speed.

Falling:
JSL $01802A|!BankB	;update sprite positions based on speeds (apply gravity and object interaction)
JMP PlatformInteraction	;run platform interaction

ChangeToFall:
STZ !B6,x		;reset sprite's X speed.
STZ !AA,x		;reset sprite's Y speed.

ChangeToShake:
STZ !1528,x		;reset platform timer.
INC !C2,x		;increment state by one
JMP PlatformInteraction	;keep running platform interaction

MoveShake:
INC !1528,x		;increment address by one (our platform timer)
LDA !1528,x		;load platform timer
CMP #!ShakeTime		;compare to value
BCS ChangeToFall	;if higher or equal, change sprite state.

LDA !1528,x		;load platform timer
AND #$01		;preserve bit 0 only
ASL			;multiply by 2 to get correct index
TAY			;transfer to Y

LDA !14D4,x		;load sprite's Y-pos, high byte
XBA			;flip high/low bytes of A
LDA !D8,x		;load sprite's Y-pos, low byte
REP #$20		;16-bit A
CLC			;clear carry
ADC ShakeOffset,y	;add shaking offset from table according to index
SEP #$20		;8-bit A
STA !D8,x		;store result back.
XBA			;flip high/low bytes of A
STA !14D4,x		;store result back.
BRA ActuallyMove	;keep the platform moving and process interactions

PlatformInteraction:
JSL $01A7DC|!BankB	;call player/sprite interaction routine
BCC .noride		;if there's no contact, return.
LDA !154C,x		;load timer to disable sprite interaction with the player
BNE .noride		;if set, return.

%SubVertPos()
LDA $0F			;load sprite's vertical distance relative to player
CMP #$E8		;compare to value
BPL .noride		;if positive, don't ride sprite.

LDA $7D			;load player's Y speed
BMI .noride		;if negative, don't ride sprite.

LDA !C2,x		;load sprite state
BNE +			;if non-zero, don't incrmeent to make it move.

INC !C2,x		;increment state by one

+
LDA $77			;load player blocked status flags
AND #$08		;check if blocked on top
BEQ +			;if not blocked, skip ahead.

LDA #$04		;load amount of frames
STA !154C,x		;store to timer to disable sprite interaction with the player.
RTS			;return. don't ride sprite.

+
LDA #$01		;load value
STA $1471|!Base2	;store to type of platform player is riding address.
STZ $7D			;reset player's Y speed.
INC $7D			;also increment it by one (for when falling the player can jump out of the platform)
LDA #$E2		;load Y offset for player into A
LDY $187A|!Base2	;load riding Yoshi flag
BEQ +			;if not riding, don't change Y offset.
LDA #$D2		;else load new value for Y offset
+
CLC			;clear carry
ADC !D8,x		;add sprite's Y-pos, low byte, to it
STA $96			;store to player's Y-pos within the level, next frame, low byte.
LDA !14D4,x		;load sprite's Y-pos, high byte
ADC #$FF		;add value, with carry
STA $97			;store to player's Y-pos within the level, next frame, high byte.

LDA $77			;load player blocked status flags
AND #$03		;check if blocked on top or sides
BNE .noride		;if yes, don't update X-pos.

LDY #$00		;load value into Y
LDA $1491|!Base2	;load amount of pixels the sprite has moved horizontally
BPL +			;if positive, branch ahead
DEY			;else decrement Y
+
CLC			;clear carry
ADC $94			;add player's X-pos within the level, next frame, low byte, to it
STA $94			;store result to player's X-pos within the level, next frame, low byte.
TYA			;transfer Y to A
ADC $95			;add player's X-pos within the level, next frame, high byte, to it... with carry
STA $95			;store result to player's X-pos within the level, next frame, high byte.

.noride
RTS			;return.

XDisp:
db $F0,$10,$00		;left tile, right tile, middle tile

Graphics:
%GetDrawInfo()		;get OAM slot and sprite coordinates within the screen

PHX			;preserve sprite index
LDA !1510,x		;load platform direction
TAX			;transfer to X
LDA MidTile,x		;load middle tile number from table according to index
STA $02			;store to scratch RAM.

LDX #$02		;loop count

GFXLoop:
CPX #$02		;compare X to value
BEQ DrawMiddle		;if equal, draw the middle tile.

LDA PlatTiles,x		;load tile number from table according to index
BRA StoreTile		;branch to store tile

DrawMiddle:
LDA $02			;load tile number from scratch RAM
StoreTile:
STA $0302|!Base2,y	;store to OAM.

LDA $00			;load sprite's X-pos within the screen
CLC			;clear carry
ADC XDisp,x		;add displacement value from table according to index
STA $0300|!Base2,y	;store to OAM.

LDA $01			;load sprite's Y-pos within the screen
STA $0301|!Base2,y	;store to OAM.

PHX			;preserve loop count
LDX $15E9|!Base2	;load index of current sprite being processed
LDA !15F6,x		;load palette/properties from CFG
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

INY #4			;increment Y four times
PLX			;restore loop count
DEX			;decrement X by one
BPL GFXLoop		;loop while it's positive.

PLX			;restore sprite index
LDY #$02		;load tile size for all tiles (#$02 = all 16x16)
LDA #$02		;load amount of tiles drawn, minus one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.