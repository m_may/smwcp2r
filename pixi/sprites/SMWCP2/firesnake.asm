;SMB3 - Fire snake
;by smkdan
;uses 768 bytes worth of sprite tables as in the fire chomp

!FREESPACE = $7F8900

    !YLO_1 = !FREESPACE+0
    !YLO_2 = !FREESPACE+12
    !YLO_3 = !FREESPACE+24
    !YLO_4 = !FREESPACE+36
    !YLO_5 = !FREESPACE+48
    !YLO_6 = !FREESPACE+60
    !YLO_7 = !FREESPACE+72
    !YLO_8 = !FREESPACE+84
    !YLO_9 = !FREESPACE+96
    !YLO_10 = !FREESPACE+108
    !YLO_11 = !FREESPACE+120
    !YLO_12 = !FREESPACE+132
    !YLO_13 = !FREESPACE+144
    !YLO_14 = !FREESPACE+156
    !YLO_15 = !FREESPACE+168
    !YLO_16 = !FREESPACE+180
    !YLO_17 = !FREESPACE+192
    !YLO_18 = !FREESPACE+204
    !YLO_19 = !FREESPACE+216
    !YLO_20 = !FREESPACE+228
    !YLO_21 = !FREESPACE+240
    !YLO_22 = !FREESPACE+252
    !YLO_23 = !FREESPACE+264
    !YLO_24 = !FREESPACE+276
    !YLO_25 = !FREESPACE+288
    !YLO_26 = !FREESPACE+300
    !YLO_27 = !FREESPACE+312
    !YLO_28 = !FREESPACE+324
    !YLO_29 = !FREESPACE+336
    !YLO_30 = !FREESPACE+348
    !YLO_31 = !FREESPACE+360
    !YLO_32 = !FREESPACE+372

    !XLO_1 = !FREESPACE+384
    !XLO_2 = !FREESPACE+396
    !XLO_3 = !FREESPACE+408
    !XLO_4 = !FREESPACE+420
    !XLO_5 = !FREESPACE+432
    !XLO_6 = !FREESPACE+444
    !XLO_7 = !FREESPACE+456
    !XLO_8 = !FREESPACE+468
    !XLO_9 = !FREESPACE+480
    !XLO_10 = !FREESPACE+492
    !XLO_11 = !FREESPACE+504
    !XLO_12 = !FREESPACE+516
    !XLO_13 = !FREESPACE+528
    !XLO_14 = !FREESPACE+540
    !XLO_15 = !FREESPACE+552
    !XLO_16 = !FREESPACE+564
    !XLO_17 = !FREESPACE+576
    !XLO_18 = !FREESPACE+588
    !XLO_19 = !FREESPACE+600
    !XLO_20 = !FREESPACE+612
    !XLO_21 = !FREESPACE+624
    !XLO_22 = !FREESPACE+636
    !XLO_23 = !FREESPACE+648
    !XLO_24 = !FREESPACE+660
    !XLO_25 = !FREESPACE+672
    !XLO_26 = !FREESPACE+684
    !XLO_27 = !FREESPACE+696
    !XLO_28 = !FREESPACE+708
    !XLO_29 = !FREESPACE+720
    !XLO_30 = !FREESPACE+732
    !XLO_31 = !FREESPACE+744
    !XLO_32 = !FREESPACE+756

!DIR = $03
!PROPRAM = $04
!TEMP = $09
!IND = $0A
!INDINDEX = $0C

;general def
!JUMPCNT = $20		;32 frames between jumps
!JUMPCNT2 = $28
!JUMPYSPD = $D8		;what to set yspd to on jumps (originally $E0)
!JUMPYSPD2 = $F4
JUMPXSPD: db $09,$F7	;what to set xspd on jumps (originally $06, $FA)
JUMPXSPD2: db $16,$EA

;table def
!JUMPRAM = $1570	;jumping delay counter

PRINT "INIT ",pc
	JSR INITTABLE	;init extra sprite tables
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE
	LDA #!JUMPCNT	;set delay count for jumping
	BRA STORE_JUMPCOUNT
LOW_TYPE:
	LDA #!JUMPCNT2
STORE_JUMPCOUNT:
	STA !JUMPRAM,x
      	RTL

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

RETURN_I:
	RTS

Run:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()
	JSR GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_I           
	LDA $9D		;locked sprites?
	BNE RETURN_I
	LDA $15D0,x	;yoshi eating?
	BNE RETURN_I

	JSL $81802A	;updated speed
	JSL $81A7DC	;mario interact

	JSR LOGPOSITIONS

	LDA $1588,x
	AND #$04	;on ground?
	BNE ONGROUND

;inair
	LDA $1588,x		; \ check if sprite is touching object side
	AND #%00000011		; /
	BEQ DONT_CHANGE_DIR	; don't change direction if not touching side
	LDA $157C,x		; \
	EOR #$01		;  | flip sprite direction
	STA $157C,x		; /
	
	LDY $157C,x	;index for xspd
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE5
	LDA JUMPXSPD,y
	BRA STORE_XSPEED2
LOW_TYPE5:
	LDA JUMPXSPD2,y
STORE_XSPEED2:
	STA $B6,x	;new xspd
DONT_CHANGE_DIR:


	DEC $AA,x	;reduce regular gravity by decreasing speed
	DEC $AA,x	;...twice

	BRA SKIPJUMP
	
ONGROUND:
	STZ $B6,x	;no xspd when on ground

	DEC !JUMPRAM,x	;decrement jumping counter
	BNE SKIPJUMP	;ready to jump?

;jump
	%SubHorzPos()	;face mario
	TYA
	STA $157C,x		;new direction

	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE2
	LDA #!JUMPCNT	;set jump counter back
	BRA STORE_JUMPCOUNT2
LOW_TYPE2:
	LDA #!JUMPCNT2
STORE_JUMPCOUNT2:
	STA !JUMPRAM,x

	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE3
	LDA #!JUMPYSPD	;set jump yspeed
	BRA STORE_YSPEED
LOW_TYPE3:
	LDA #!JUMPYSPD2
STORE_YSPEED:
	STA $AA,x

	LDY $157C,x	;index for xspd
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE4
	LDA JUMPXSPD,y
	BRA STORE_XSPEED
LOW_TYPE4:
	LDA JUMPXSPD2,y
STORE_XSPEED:
	STA $B6,x	;new xspd

SKIPJUMP:
;interaction
	LDA $E4,x	;xlow
	PHA
	LDA $14E0,x	;xhigh
	PHA
	LDA $D8,x	;ylow
	PHA
	LDA $14D4,x	;yhigh, all preserved
	PHA

	LDA #$00
	PHA
INTLOOP:
	PLA
	TAY
	INC A
	INC A
	PHA
	
	REP #$30	;16bit AXY
	TYA
	AND.w #$00FF	;wipe high
	TAY
	LDA $15E9	;load index
	AND.w #$00FF
	CLC
	ADC INDEXES,y	;load index again, ADD 15E9!
	TAX
	SEP #$20	;8bit A
	LDA $7F0000,x	;load byte from table
	SEP #$10	;8bitXY
	LDX $15E9	;sprite index
	STA $E4,x	;storex

	PLA
	TAY
	INC A
	INC A
	PHA
	REP #$30	;16bit AXY
	TYA
	AND.w #$00FF	;wipe high
	TAY
	LDA $15E9	;load index
	AND.w #$00FF
	CLC
	ADC INDEXES,y	;load index again, ADD 15E9!
	TAX
	SEP #$20	;8bit A
	LDA $7F0000,x	;load byte
	SEP #$10	;8bit XY
	LDX $15E9	;sprite index
	STA $D8,x	;storey 

	JSL $01A7DC	;mario interact, CAN ONLY RUN ONCE ARGH

	PLA
	PHA
	CMP #$0C	;done yet?
	BNE INTLOOP	;interaction again
	PLA

	PLA
	STA $14D4,x	;yhigh, all restored
	PLA
	STA $D8,x
	PLA
	STA $14E0,x
	PLA
	STA $E4,x
	
	RTS        
			
;=====

TILEMAP:	db $A8,$AA	;head tiles
TILEMAPB:	db $C8,$CA	;body tiles

INDEXES:
	dw !XLO_10&$FFFF,!YLO_10&$FFFF	;INDEXES to use out of the sprite tables
	dw !XLO_21&$FFFF,!YLO_21&$FFFF
	dw !XLO_32&$FFFF,!YLO_32&$FFFF

GFX:
	LDA $14		;frame counter
	LSR A
	LSR A
	AND #$01	;flicker every 4th frame
	STA !TEMP

	LDA $15F6,x	;properties...
	STA !PROPRAM

;some sort of loop
	LDA $00
	STA $0300,y	;xpos
	LDA $01
	STA $0301,y	;ypos
	LDX !TEMP
	LDA TILEMAP,x
	STA $0302,y	;chr
	LDA !PROPRAM
	ORA $64
	STA $0303,y	;prop

	INY		;next tile
	INY
	INY
	INY	

;draw body parts, what a mess this is 8D
	STZ !INDINDEX	;reset indirect index
	
OAM_BODY:
	LDX !INDINDEX	;load index
	INC !INDINDEX	;advance index to next word
	INC !INDINDEX
	LDA $15E9	;load sprite index
	REP #$30	;AXY
	AND.w #$00FF	;wipe high
	CLC
	ADC INDEXES,x	;add sprite table index
	TAX		;into X
	SEP #$20	;8bit A again

	LDA $00		;????
	CLC
	ADC $7F0000,x	;load from table	
	LDX.w #$0000	;wipe x
	SEP #$10	;8bit XY
	LDX $15E9
	SEC
	SBC $E4,x	;sub xpos
	STA $0300,y	;xpos

	LDX !INDINDEX	;load index
	INC !INDINDEX	;advance index to next word
	INC !INDINDEX
	LDA $15E9	;load sprite index
	REP #$30	;AXY
	AND.w #$00FF	;wipehigh
	CLC
	ADC INDEXES,x	;add sprite table index
	TAX		;into X
	SEP #$20	;8bit A again	

	LDA $01		;???
	CLC
	ADC $7F0000,x	;load from table
	LDX.w #$0000	;wipe x
	SEP #$10	;8bit XY again
	LDX $15E9
	SEC
	SBC $D8,x	;sub ypos
	STA $0301,y	;ypos

	LDX !TEMP	;frame index
	LDA TILEMAPB,x
	STA $0302,y	;chr

	LDA !PROPRAM
	ORA $64
	STA $0303,y	;prop
	
	INY
	INY
	INY
	INY		;next tile
	LDA !INDINDEX	;test ind index
	CMP #$0C	;3*4
	BNE OAM_BODY

	LDX $15E9	;restore sprite index

	LDY #$02	;16x16
	LDA #$03	;4 tiles
	JSL $01B7B3	;reserve

	RTS

;these two are from the firechomp
INITTABLE:
	LDA $D8,x	;y;set positions to be all behind the head
      	STA !YLO_1,x
      	STA !YLO_2,x
      	STA !YLO_3,x
      	STA !YLO_4,x
      	STA !YLO_5,x
      	STA !YLO_6,x
      	STA !YLO_7,x
      	STA !YLO_8,x
      	STA !YLO_9,x
      	STA !YLO_10,x
      	STA !YLO_11,x
      	STA !YLO_12,x
      	STA !YLO_13,x
      	STA !YLO_14,x
      	STA !YLO_15,x
      	STA !YLO_16,x
      	STA !YLO_17,x
      	STA !YLO_18,x
      	STA !YLO_19,x
      	STA !YLO_20,x
      	STA !YLO_21,x
      	STA !YLO_22,x
      	STA !YLO_23,x
      	STA !YLO_24,x
      	STA !YLO_25,x
      	STA !YLO_26,x
      	STA !YLO_27,x
      	STA !YLO_28,x
      	STA !YLO_29,x
      	STA !YLO_30,x
      	STA !YLO_31,x
      	STA !YLO_32,x
      	LDA $E4,x	;x
      	STA !XLO_1,x
      	STA !XLO_2,x
      	STA !XLO_3,x
      	STA !XLO_4,x
      	STA !XLO_5,x
      	STA !XLO_6,x
      	STA !XLO_7,x
      	STA !XLO_8,x
      	STA !XLO_9,x
      	STA !XLO_10,x
      	STA !XLO_11,x
      	STA !XLO_12,x
      	STA !XLO_13,x
      	STA !XLO_14,x
      	STA !XLO_15,x
      	STA !XLO_16,x
      	STA !XLO_17,x
      	STA !XLO_18,x
      	STA !XLO_19,x
      	STA !XLO_20,x
      	STA !XLO_21,x
      	STA !XLO_22,x
      	STA !XLO_23,x
      	STA !XLO_24,x
      	STA !XLO_25,x
      	STA !XLO_26,x
      	STA !XLO_27,x
      	STA !XLO_28,x
      	STA !XLO_29,x
      	STA !XLO_30,x
      	STA !XLO_31,x
      	STA !XLO_32,x

	RTS

LOGPOSITIONS:
      	LDA !YLO_31,x
      	STA !YLO_32,x
      	LDA !YLO_30,x
      	STA !YLO_31,x
      	LDA !YLO_29,x
      	STA !YLO_30,x
      	LDA !YLO_28,x
      	STA !YLO_29,x
      	LDA !YLO_27,x
      	STA !YLO_28,x
      	LDA !YLO_26,x
      	STA !YLO_27,x
      	LDA !YLO_25,x
      	STA !YLO_26,x
      	LDA !YLO_24,x
      	STA !YLO_25,x
      	LDA !YLO_23,x
      	STA !YLO_24,x
      	LDA !YLO_22,x
      	STA !YLO_23,x
      	LDA !YLO_21,x
      	STA !YLO_22,x
      	LDA !YLO_20,x
      	STA !YLO_21,x
      	LDA !YLO_19,x
      	STA !YLO_20,x
      	LDA !YLO_18,x
      	STA !YLO_19,x
      	LDA !YLO_17,x
      	STA !YLO_18,x
      	LDA !YLO_16,x
      	STA !YLO_17,x
      	LDA !YLO_15,x
      	STA !YLO_16,x
      	LDA !YLO_14,x
      	STA !YLO_15,x
      	LDA !YLO_13,x
      	STA !YLO_14,x
      	LDA !YLO_12,x
      	STA !YLO_13,x
      	LDA !YLO_11,x
      	STA !YLO_12,x
      	LDA !YLO_10,x
      	STA !YLO_11,x
      	LDA !YLO_9,x
      	STA !YLO_10,x
      	LDA !YLO_8,x
      	STA !YLO_9,x
      	LDA !YLO_7,x
      	STA !YLO_8,x
      	LDA !YLO_6,x
      	STA !YLO_7,x
      	LDA !YLO_5,x
      	STA !YLO_6,x
      	LDA !YLO_4,x
      	STA !YLO_5,x
      	LDA !YLO_3,x
      	STA !YLO_4,x
      	LDA !YLO_2,x
      	STA !YLO_3,x
      	LDA !YLO_1,x
      	STA !YLO_2,x
      	LDA $D8,x
      	STA !YLO_1,x
      	LDA !XLO_31,x
      	STA !XLO_32,x
      	LDA !XLO_30,x
      	STA !XLO_31,x
      	LDA !XLO_29,x
      	STA !XLO_30,x
      	LDA !XLO_28,x
      	STA !XLO_29,x
      	LDA !XLO_27,x
      	STA !XLO_28,x
      	LDA !XLO_26,x
      	STA !XLO_27,x
      	LDA !XLO_25,x
      	STA !XLO_26,x
      	LDA !XLO_24,x
      	STA !XLO_25,x
      	LDA !XLO_23,x
      	STA !XLO_24,x
      	LDA !XLO_22,x
      	STA !XLO_23,x
      	LDA !XLO_21,x
      	STA !XLO_22,x
      	LDA !XLO_20,x
      	STA !XLO_21,x
      	LDA !XLO_19,x
      	STA !XLO_20,x
      	LDA !XLO_18,x
      	STA !XLO_19,x
      	LDA !XLO_17,x
      	STA !XLO_18,x
      	LDA !XLO_16,x
      	STA !XLO_17,x
      	LDA !XLO_15,x
      	STA !XLO_16,x
      	LDA !XLO_14,x
      	STA !XLO_15,x
      	LDA !XLO_13,x
      	STA !XLO_14,x
      	LDA !XLO_12,x
      	STA !XLO_13,x
      	LDA !XLO_11,x
      	STA !XLO_12,x
      	LDA !XLO_10,x
      	STA !XLO_11,x
      	LDA !XLO_9,x
      	STA !XLO_10,x
      	LDA !XLO_8,x
      	STA !XLO_9,x
      	LDA !XLO_7,x
      	STA !XLO_8,x
      	LDA !XLO_6,x
      	STA !XLO_7,x
      	LDA !XLO_5,x
      	STA !XLO_6,x
      	LDA !XLO_4,x
      	STA !XLO_5,x
      	LDA !XLO_3,x
      	STA !XLO_4,x
      	LDA !XLO_2,x
      	STA !XLO_3,x
      	LDA !XLO_1,x
      	STA !XLO_2,x
      	LDA $E4,x
      	STA !XLO_1,x

	RTS