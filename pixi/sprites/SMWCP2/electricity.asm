;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Electricity, by mikeyk
;;
;; Description: This sprite bounces between two surfaces and can only be killed with a
;; star.
;;
;; Uses first extra bit: YES
;; If the first extra bit is set, the sprite will bounce vertically between two surfaces.
;; Otherwise the sprite will bounce horizontally between two surfaces.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !EXTRA_BITS = !7FAB10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
lda $13BF
cmp.b #$11B-$DC
bne +

lda !15F6,x
and #$F1
ora #$0C
sta !15F6,x

+
                    %SubHorzPos()
                    TYA
                    STA !157C,x
                    RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc                                    
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
                    JSR SPRITE_CODE_START   ;  |
                    PLB                     ;  |
                    RTL                     ; /


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:              RTS
SPRITE_CODE_START:   JSR SPRITE_GRAPHICS       ; graphics routine
                    LDA !14C8,x             ; \ 
                    CMP #$08                ;  | if status != 8, RETURN
                    BNE RETURN              ; /
                    LDA #$00
		%SubOffScreen()   ; handle off screen situation
                    LDA $9D                 ; \ if sprites locked, RETURN
                    BNE RETURN              ; /

                    LDA !EXTRA_BITS,x
                    AND #$04
                    BEQ HORIZ_MOVEMENT
                    
                    JSR HANDLE_VERT
                    BRA CHECK_CONTACT
                    
HORIZ_MOVEMENT:      JSR HANDLE_HORIZ
   
CHECK_CONTACT:      JSL $01A7DC|!BankB             ; check for mario/sprite contact (carry set = contact)
                    BCC RETURN_24           ; RETURN if no contact

SPRITE_WINS:         LDA !15D0,x             ;  |   ...or sprite being eaten...
                    BNE RETURN_24           ; /   ...RETURN
                    LDA $1490|!Base2        ; \ if mario star timer > 0, goto HAS_STAR 
                    BNE HAS_STAR            ; / NOTE: branch to RETURN_24 to disable star killing                  
                    LDA $187A|!Base2        ; \ if riding sprite...
                    BNE RETURN_24 
                    JSL $00F5B7|!BankB       ; hurt mario
RETURN_24:           RTS                     ; final RETURN

HAS_STAR:
		%Star()
NO_SOUND2:           RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_SPEED:             db $18,$E8

HANDLE_HORIZ:       LDY !157C,x             ; \ set x speed based on direction
                    LDA X_SPEED,y           ;  |
                    STA !B6,x               ; /
                    
                    STZ !AA,x
                    JSL $01802A|!BankB      ; update position based on speed values
         
                    LDA !1588,x             ; \ if sprite is in contact with an object...
                    AND #$03                ;  |
                    BEQ NO_CONTACT          ;  |
leflipdir:
                    LDA !157C,x             ;  | flip the direction status
                    EOR #$01                ;  |
                    STA !157C,x             ; /
NO_CONTACT:         RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Y_SPEED:             db $18,$E8

HANDLE_VERT:        LDY !157C,x             ; \ set x speed based on direction
                    LDA Y_SPEED,y           ;  |
                    STA !AA,x               ; /
                    
                    STZ !B6,x
                    JSL $01802A|!BankB       ; update position based on speed values
         
                    LDA !1588,x             ; \ if sprite is in contact with an object...
                    AND #$0C                ;  |
                    BEQ NO_CONTACT          ;  |
                    BRA leflipdir

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TILEMAP:             db $E6,$C8

SPRITE_GRAPHICS:     %GetDrawInfo()       ; sets y = OAM offset
                    LDA !157C,x             ; \ $02 = direction
                    STA $02                 ; / 
                    LDA $14                 ; \ 
                    LSR A                   ;  |
                    ;LSR A                   ;  |
                    LSR A                   ;  |
                    CLC                     ;  |
                    ADC $15E9|!Base2        ;  |
                    AND #$01                ;  |
                    STA $03                 ;  | $03 = index to frame start (0 or 1)
                    PHX                     ; /
                    
                    LDA !14C8,x
                    CMP #$02
                    BNE LOOP_START_2
                    STZ $03
                    LDA !15F6,x
                    ORA #$80
                    STA !15F6,x

LOOP_START_2:       
		REP #$20
		    LDA $00                 ; \ tile x position = sprite x location ($00)
                    STA $0300|!Base2,y             ; /
		SEP #$20

                    LDA !15F6,x             ; tile properties xyppccct, format
                    ;LDX $02                 ; \ if direction == 0...
                    ;BNE NO_FLIP             ;  |
                    ;ORA #$40                ; /    ...flip tile
NO_FLIP:             ORA $64                 ; add in tile priority of level
                    STA $0303|!Base2,y     ; store tile properties

                    LDX $03                 ; \ store tile
                    LDA TILEMAP,x           ;  |
                    STA $0302|!Base2,y      ; /

                    PLX                     ; pull, X = sprite index
                    LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
                    LDA #$00                ;  | A = (number of tiles drawn - 1)
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; RETURN