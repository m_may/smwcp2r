;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Boomerang (for Boomerang Brother), by mikeyk
;;
;; Description: This is the boomerang that the Boomerang Brother throws.
;;
;; BIG FAT NOTE: This sprite depends on the boomerang brother sprite.  Make sure you
;; insert the boomerang brother right before this sprite.  (ex. If this is sprite 1C,
;; make the boomerang brother 1B)
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !EXTRA_BITS = !7FAB10

                    !SLOW_DOWN_TIMER = !1504
                    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !SLOWDOWN_TIME = $20
                    !TIME_UNTIL_CHANGE = $38
                    !TILES_PER_FRAME = $00

TILEMAP:             db $4B,$4E,$4B,$4E
PROPERTIES:          db $C9,$09,$09,$C9

SPEED_TABLE:         db $20,$E0,$18,$E8     ; speed of sprite, right, left
Y_SPEED:             db $F8,$F8,$09,$09
                    db $00,$00,$08,$08



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    STZ !SLOW_DOWN_TIMER,x
                    RTL
                    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc
HAMMER_BRO_JSL:      PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
	            JSR DECREMENTTIMERS
                    JSR START_SPRITE_CODE   ;  |
                    PLB                     ;  |
                    RTL                     ; /
DECREMENTTIMERS:
	LDA !14C8,x
	CMP #$08
	BNE DONEDEC
	LDA $9D
	BNE DONEDEC
	LDA !SLOW_DOWN_TIMER,x
	BEQ DONEDEC
	DEC !SLOW_DOWN_TIMER,x
DONEDEC:
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:              RTS                    
START_SPRITE_CODE:   JSR SUB_GFX             ; draw sprite gfx
                    LDA !14C8,x             ; \ if sprite status != 8...
                    CMP #$08                ;  }   
                    BNE RETURN              ; /    ... RETURN
                    LDA $9D                 ; \ if sprites locked...
                    BNE RETURN              ; /    ... RETURN

                    LDA #$00
			%SubOffScreen()   ; only process sprite while on screen
                    INC !1570,x             ; increment number of frames sprite has been on screen

                    ;STZ !AA,x              ; no y speed
                    
                    LDA !157C,x             
                    TAY                     
                    LDA !SLOW_DOWN_TIMER,x             ; \ if the slow down timer is set, 
                    BNE SLOW_DOWN           ; / then slow down the sprite
                    LDA !1540,x             ; \ also slow down the sprite if its about
                    BEQ SET_SPEED           ;  | to change direction.
                    CMP #!SLOWDOWN_TIME      ;  | ( 0 < timer < !SLOWDOWN_TIME )
                    BCS SET_SPEED           ; /
SLOW_DOWN:           INY                     ; \ increment index to use slower speeds
                    INY                     ; /
SET_SPEED:           LDA SPEED_TABLE,y       ; \ set the x speed
                    STA !B6,x               ; / 
                    
                    LDA !C2,x   
                    BEQ NO_INCS
                    INY
                    INY
                    INY
                    INY
NO_INCS:             LDA Y_SPEED,y
                    STA !AA,x  
                    
                    JSL $01802A|!BankB           ; update position based on speed values

                    LDA !1540,x             ; \ check if its time to change directions
                    CMP #$01                ;  | ($1540 about to expire)
                    BNE LABEL17             ; /
                    LDA !157C,x             ; \ change direction
                    EOR #$01                ;  |
                    STA !157C,x             ; /
                    LDA #!SLOWDOWN_TIME      ; \ set time to slow down sprite
                    STA !SLOW_DOWN_TIMER,x             ; /
                    INC !C2,x               ; increment turn count
LABEL17:             

                    JSL $01A7DC|!BankB     ; check for mario/sprite contact

                    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_GFX:             %GetDrawInfo()       ; after: Y = index to sprite tile map ($300)
                                            ;      $00 = sprite x position relative to screen boarder 
                                            ;      $01 = sprite y position relative to screen boarder  
                    
                    LDA !1570,x             ; \ calculate which frame to show
                    LSR A                   ;  |   based on how long sprite's been alive
                    LSR A                   ;  |   based on how long sprite's been alive
                    ;LSR A                   ;  |   based on how long sprite's been alive
                    AND #$03                ;  | 
                    STA $03                 ; /
                  
                    LDA !157C,x             ; \ $02 = sprite direction
                    PHY
                    LDY !1540,x 
                    BEQ NOT_TURNING
                    CPY #!SLOWDOWN_TIME
                    BCS NOT_TURNING
                    EOR #$01
NOT_TURNING:         PLY
                    STA $02                 ; /
            
                    PHX                     ; push sprite index

LOOP_START:         
                REP #$20
		LDA $00                 ; \ tile x position = sprite x location ($00)
		STA $0300|!Base2,y             ; /
		SEP #$20
                    
                    LDX $03
                    LDA TILEMAP,x           ; \
                    STA $0302|!Base2,y             ; / 
        
                    ;LDX $03                    ; \
                    LDA PROPERTIES,x        ;  | get sprite pallette
                    ;LDX $02                    ;  |
                    ;BNE NO_FLIP_TILE       ;  |
                    ;ORA #$40               ;  | put in sprite direction
NO_FLIP_TILE:        ORA $64                 ;  | put in level PROPERTIES
                    STA $0303|!Base2,y             ; / store tile PROPERTIES
                    
                    PLX                     ; pull, X = sprite index

                    LDY #$02                ; \ 02, because 16x16 tiles
                    LDA #!TILES_PER_FRAME    ;  | A = number of tiles drawn - 1
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; RETURN