print "MAIN ",pc
	PHB		;\
	PHK		; | Change the data bank to the one our code is running from.
	PLB		; | This is a good practice.
	JSR SpriteCode	; | Jump to the sprite's function.
	PLB		; | Restore old data bank.
print "INIT ",pc
RTL		;/ And return.

YSPEED: db $08,$F8
XSPEED: db $08,$F8

;===================================
;Sprite Function
;===================================

NOO:
LDA #$0F
STA $0DAE
NoContact1:
RTS

SpriteCode:
	JSR Graphics
	LDA #$00
	%SubOffScreen()
	
	LDA $9D
	BNE NoContact1
	
	LDA $71  		
	CMP #$05
	BCS NOO
	
	LDA !extra_prop_1,x
	BEQ SkipMoving
	
  	LDA $7FAB10,x        
	AND #$04                
	BNE GOVERTICAL
				
LDA $1558,x
BNE NotOnGround1
	
	LDA $157C,x			
	EOR #$01			
	STA $157C,x			
	LDA #$7F
	STA $1558,x
	BRA NotOnGround1
	RTS
NotOnGround1:
	LDY $157C,x			
	LDA XSPEED,y		
	STA $B6,x			
	JSL $019138 
	JSL $018022			
	JSR Adresses
		RTS
SkipMoving:

JSL $01A7DC
BCC NoContact1

DEC $0DAE
BPL +
INC $0DAE
+
BRA Check
		
GOVERTICAL:
LDA $1558,x
BNE NotOnGround2
	
	LDA $151C,x			
	EOR #$01			
	STA $151C,x			
	LDA #$7F
	STA $1558,x
	BRA NotOnGround2
	RTS
NotOnGround2:
	LDY $151C,x			
	LDA YSPEED,y		
	STA $AA,x			
	JSL $019138 
	JSL $01801A			
	BRA Adresses
	

Adresses:
	JSL $018032
JMP SkipMoving

;===================================
;
;===================================
Check:
	LDA $0E				;\
	CMP #$E6			; | Determine if Mario touches sprite from sides.
	BPL SpriteWins			;/ If he does, branch.

;===========================================
;Mario Hurts Sprite
;===========================================

	JSL $01AA33             ;\ Set mario speed.
	JSL $01AB99             ;/ Display contact graphic.

	LDA $140D		; IF spin-jumping ..
	BNE SpinKill		; Go to spin kill.
	
	JSR DrawSmoke
	
	LDA #$13		;\
	STA $1DF9		;/ Play sound.
	RTS

SpinKill:
	LDA #$04		;\
	STA $14C8,x		;/ Sprite status = killed by a spin-jump.
	LDA #$08		;\
	STA $1DF9		;/ Sound to play.
	JSL $07FC3B		; Show star animation effect.
	RTS
;===========================================
;Sprite Hurts Mario
;===========================================

SpriteWins:
JSR DrawSmoke    
RTS
NoContact:
JSR DrawSmoke	
RTS
HasStar:
JSR DrawSmoke
RTS

TILE: db $CC,$CC,$CC,$CC
TILEMAP: db $CC,$8A,$AC,$8A

Graphics:
	%GetDrawInfo()
		  		
	LDA $00			;\
	STA $0300,y		;/ Draw the X position of the sprite

	LDA $01			;\
	STA $0301,y		;/ Draw the Y position of the sprite

LDA $71  		
CMP #$09
BEQ NOO2


PHX
LDA $14
LSR 
LSR 
LSR 
AND #$03
TAX
LDA TILEMAP,x
STA $0302,y
PLX
KEEP2:
LDA #%00001001
ORA $64
STA $0303,y

	INY			;\
	INY			; | The OAM is 8x8, but our sprite is 16x16 ..
	INY			; | So increment it 4 times.
	INY			;/

	LDY #$02		; Y ends with the tile size .. 02 means it's 16x16
	LDA #$00		; A -> number of tiles drawn - 1.
				; I drew only 1 tile so I put in 1-1 = 00.

	JSL $01B7B3		; Call the routine that draws the sprite.
	RTS			; Never forget this!

NOO2:
PHX
LDA $14
LSR 
LSR 
LSR 
AND #$03
TAX
LDA TILE,x
STA $0302,y
PLX
BRA KEEP2

DrawSmoke:
	STZ $00 : STZ $01
	LDA #$1B : STA $02
	LDA #$01
	%SpawnSmoke()
	LDA #$08
	STA $1DF9
	STZ $14C8,x         
	RTS;