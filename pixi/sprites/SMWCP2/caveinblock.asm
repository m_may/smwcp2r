;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cave-In Block, by yoshicookiezeus
;;
;; A block that falls, bounces when it hits the ground, and then falls
;; offscreen. It is 16x16 if the extra bit is set, and 32x32 if it isn't.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; If you don't want the block to hurt Mario at all, use the CFG editor to
; enable "Don't use default interaction with Mario".
; If you do that, you can set !HurtAfterFall to 1, as it'll not matter (you'll save some cycles).

!AppearAboveGround = 0      ; Set to 1 if you want the block to appear above the ground
!HurtAfterFall = 1          ; Set to 1 if you want the block to hurt Mario even after hitting the ground

!FallSFX = $09              ; Sound effect to play when the sprite hits the ground
!FallBank = $1DFC

print "INIT ",pc
    %BEC(+)
        LDA #$01            ;\ mark block as small
        STA !C2,x           ;/
        STZ !1662,x         ; set sprite clipping value for small block
        STZ !1656,x         ; set object clipping value for small block
+   RTL

print "MAIN ",pc
    PHB : PHK : PLB
    JSR SpriteCode
    PLB
    RTL

SpriteCode:
    JSR Graphics
    LDA #$00
    %SubOffScreen()

    LDA $9D                 ;\ if sprites locked,
    BNE Return              ;/ return
    JSL $01802A|!BankB      ; update sprite position
    JSL $01A7DC|!BankB

    JSL $019138|!BankB      ; interact with objects

    LDA !1588,x             ;\ if sprite isn't touching the ground,
    AND #$04                ; |
    BEQ Return              ;/ branch

    if !HurtAfterFall == 0
        LDA !167A,x         ;\ disable sprite's interaction with Mario
        ORA #$80            ; |
        STA !167A,x         ;/
    endif

    LDA !1686,x             ;\ disable sprite's interaction with objects
    ORA #$80                ; |
    STA !1686,x             ;/

    LDA #$E0                ;\ set new sprite Y speed
    STA !AA,x               ;/

    LDA.b #!FallSFX         ;\ play sound effect
    STA.w !FallBank|!Base2  ;/
Return:
    RTS

X_Disp:         db $00,$10,$00,$10      ;\  just your generic 32x32 square
Y_Disp:         db $00,$00,$10,$10      ; |
TilemapBig:     db $44,$46,$64,$66      ;/

!TileSmall      = $0A

Graphics:
    %GetDrawInfo()

    LDA !C2,x           ;\ if block is small,
    BNE Small           ;/ branch

; graphics routine for big block
    PHX                     ; preserve sprite index
    LDX #$03                ; setup loop
-   LDA $00                 ;\ set tile x position
    CLC : ADC X_Disp,x      ; |
    STA $0300|!Base2,y      ;/

    LDA $01                 ;\ set tile y position
    CLC : ADC Y_Disp,x      ; |
    STA $0301|!Base2,y      ;/

    LDA TilemapBig,x        ;\ set tile number
    STA $0302|!Base2,y      ;/

    PHX                     ;\ set tile properties
    LDX $15E9|!Base2        ; |
    LDA !15F6,x             ; |
    ORA $64                 ; |

    if !AppearAboveGround == 0
        AND #$CF            ; | clear priority bytes
    endif

    STA $0303|!Base2,y      ; |
    PLX                     ;/

    INY #4                  ; increase OAM index

    DEX                     ;\ if tiles left to draw,
    BPL -                   ;/ go to start of loop

    PLX                     ; retrieve sprite index

    LDY #$02                ; the tiles drawn were 16x16
    LDA #$03                ; four tiles were drawn
    JSL $01B7B3|!BankB      ; finish OAM write
    RTS

; graphics routine for small block
Small:
    LDA $00                 ;\ set tile x position
    STA $0300|!Base2,y      ;/
    LDA $01                 ;\ set tile y position
    STA $0301|!Base2,y      ;/
    LDA.b #!TileSmall       ;\ set tile number
    STA $0302|!Base2,y      ;/

    PHX                     ;\ set tile properties
    LDX $15E9|!Base2        ; |
    LDA !15F6,x             ; |
    ORA $64                 ; |

    if !AppearAboveGround == 0
        AND #$CF            ; | clear priority bytes
    endif

    STA $0303|!Base2,y      ; |
    PLX                     ;/

    LDY #$02                ; finish OAM write
    LDA #$00
    JSL $01B7B3|!BankB
    RTS
