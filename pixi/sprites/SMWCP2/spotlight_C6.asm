;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Dark Room Spotlight (sprite C6), by imamelia
;;
;; This is a disassembly of sprite C6 in SMW, the dark room spotlight sprite.
;; (Also known informally as the "disco ball" light.)
;;
;; Uses first extra bit: NO
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BGColorLo:
db $10,$8C

BGColorHi:
db $42,$31

Data1:
db $01,$FF

Data2:
db $FF,$90

Tilemap:
db $80,$82,$84,$86,$E0,$E2,$E4,$E6 ;,$E6

TileProperties:
;db $31,$33,$35,$37,$31,$33,$35,$37,$39
db $31,$33,$35,$37,$39

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR SpotlightMain
PLB
print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpotlightMain:	; Warning: There are a lot of unknown RAM addresses here,
		; so the comments aren't very descriptive.
LDA $1534,x	; seems to be a flag for whether this spotlight is the first in the level
BNE Continue	; if this is the first spotlight sprite to appear, skip the erasure check
LDY #$09		; loop through 10 indexes
EraseExtraLoop:	;
CPY $15E9	; if the sprite is this one...
BEQ SkipErase	; don't erase it
LDA $14C8,y	; check the sprite status
CMP #$08		; if the second sprite is not in normal status...
BNE SkipErase	; there is no need to erase it
LDA $009E,y	; check the sprite number
CMP #$C6	; if it isn't another spotlight sprite...
BNE SkipErase	; there is no need to erase it
STZ $14C8,x	; erase this sprite if another spotlight already exists in the level

EndMain:
RTS

SkipErase:	;

DEY		;
BPL EraseExtraLoop	;

INC $1534,x	; if the code reaches here, this spotlight was the first to appear in the level

Continue:		;

JSR SpotlightGFX	; draw the sprite

LDA #$FF		; CGADSUB settings:
STA $40		; subtract colors, half-color math, enable all layers
LDA #$20		; prevent color math
STA $44		; on the inside color window
STA $43		; enable window 1 for BG2/BG4/single color (removed redundant LDA #$20)
LDA #$80		;
STA $0D9F	; enable HDMA channel 7
LDA $C2,x	; get the sprite's state
AND #$01	; one of two possibilities: on or off, affected by sprite C8 (the light switch)
TAY		; on/off status --> Y
LDA BGColorLo,y	; set the low byte of the background color
STA $0701	;
LDA BGColorHi,y	; set the high byte of the background color
STA $0702	;

LDA $9D		; check the sprite lock timer
BNE EndMain	; if sprites are locked, end the main routine here

LDA $1482	; don't know what this RAM address is...
BNE Label1	;
STZ $1476	; no need for LDA #$00 here...and I don't know what this RAM address is either
LDA #$90		;
STA $1478	; ...or this one...
LDA #$78		;
STA $1472	; ...yeah...
LDA #$87		;
STA $1474	; ...lorem ipsum and stuff...
LDA #$01		;
STA $1486	; Sorry, Atma, but I don't know what any of this stuff does.
STZ $1483	;
INC $1482	; Somebody document these RAM addresses, for Pete's sake!
Label1:		;
LDY $1483	;
LDA $1476	; Remember, flip only RAM address $1476.
CLC		;
ADC Data1,y	; While you are flipping, be sure you can see the value of $1476 and $1478.
STA $1476	;
LDA $1478	; If you flip quickly enough, the two RAM addresses
CLC		;
ADC Data1,y	; will start to look like one animated disco ball.
STA $1478	;
CMP Data2,y	; compare to blah
BNE Label2	; branch if not equal to yadda
LDA $1483	;
INC		;
AND #$01	; I should put a subliminal message here or something.
STA $1483	;
Label2:		;
LDA $13		; load the cotton-pickin' finger-lickin' chicken-pluckin' frame counter
AND #$03	; clear out all bits but the cotton-lickin' finger-pluckin' chicken-pickin' first two
BNE EndMain	; halt the cotton-pluckin' finger-pickin' chicken-lickin' main routine here
LDY #$00		;
LDA $1472	;
STA $147A	;
SEC		;
SBC $1476	; MY BUS IS PURPLE
BCS Label3	;
INY		;
EOR #$FF		;
INC		;
Label3:		;
STA $1480	; How come you're always such a fussy young man?
STY $1484	;
STZ $147E	; Don't want no Cap'n Crunch, don't want no raisin bran *
LDY #$00		;
LDA $1474	; Well, don't you know kids are starving in Japan?
STA $147C	;
SEC		; So eat it.  Just eat it.
SBC $1478	;
BCS Label4	;
INY		;
EOR #$FF		; Okay, I take it this code is analogous to the previous block...
INC		;
Label4:		;
STA $1481	;
STY $1485	;
STZ $147F	; **
LDA $C2,x	;
STA $0F		; ...why are you even still reading this?...
PHX		;
REP #$10		;
LDX.w #$0000	;
Label12:		;
CPX.w #$005F	; It's not like these comments are that interesting...
BCC Label5	;
LDA $147E	;
CLC		;
ADC $1480	; Seriously, I'm as lost in this code as you are.
STA $147E	;
BCS Label6	;
CMP #$CF		;
BCC Label7	; Surely you have something better to do...
Label6:		;
SBC #$CF		;
STA $147E	; Oh, forget it.
INC $147A	;
LDA $1484	;
BNE Label7	; But I really wish these RAM addresses were documented...
DEC $147A	;
DEC $147A	;
Label7:		;
LDA $147F	; Okay, this is getting tiresome.
CLC		;
ADC $1481	;
STA $147F	;
BCS Label8	; Well, I'm getting near the end of the code anyway, so...
CMP #$CF		;
BCC Label9	;
Label8:		;
SBC #$CF		; I'm gonna go make a stir-fry.
STA $147F	;
INC $147C	;
LDA $1485	;
BNE Label9	;
DEC $147C	;
DEC $147C	;
Label9:		;
LDA $0F		;
BNE Label10	;
Label5:		;
LDA #$01		;
STA $04A0,x	;
DEC		;
BRA Label11	;

Label10:		;
LDA $147A	;
STA $04A0,x	;
LDA $147C	;
Label11:		;
STA $04A1,x	; **
INX		;
INX		;
CPX #$01C0	;
BNE Label12	;
SEP #$10		;
PLX		;
RTS		;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpotlightGFX:

LDY $15EA,x	; no need for GetDrawInfo since the sprite is always on the screen

LDA #$78		;
STA $0300,y	; also has a fixed X position
LDA #$28		;
STA $0301,y	; and Y position

PHX		; preserve the sprite index
LDA $C2,x	; check the sprite state
;LDX #$08		; if the light is off...
LDX #$04		; if the light is off...
AND #$01	; ~use the ninth tile in the tilemap~ (now only fifth palette)

;new code here
BEQ +
PLX
INC !1504,x
LDA !1504,x
PHX
LSR
AND #$03
TAX
+
LDA TileProperties,x
STA $0303,y
PLX
LDA !1504,x
PHX
LSR
LSR
LSR
AND #$07
TAX
LDA Tilemap,x
STA $0302,y

;BEQ SkipAnimation	;
;LDA $13		; if the light is on...
;LSR		;
;AND #$07	; give the light a nice, fast, 8-frame animation
;TAX		;
;SkipAnimation:	;
;LDA Tilemap,x	; set the tilemap
;STA $0302,y	;
;LDA TileProperties,x; set the tile properties
;STA $0303,y	;

TYA		;
LSR		;
LSR		;
TAY		; divide the OAM index by 4

LDA #$02		;
STA $0460,y	; set the tile size to 16x16

PLX
RTS