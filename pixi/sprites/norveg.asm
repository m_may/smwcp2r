;@Norveg.bin

	; stuff you should actually mess with
		!BossNum		= $00
		!ProjectileNum		= $01
		!ParticleNum		= $04

		!phase1hp		= $07
		!phase2hp		= $07
		!phase3hp		= $1D

		!battlemusic		= $21

		!flowerground		= $0178


	; free RAM
		;these 2 must be the same as in the uberasm (uberasmtool/level/NorvegHDMA.asm)
		!TimerForVictoryAfterBossDeath = $0F5E|!addr
		!UploadMessages34Flag = $0F5F|!addr

		!RNG			= $60
		!rng			= !RNG		; synonym
		!chunkcooldown		= $1BA3|!addr	; 64 free bytes
		!DestroyTileBufferIndex	= $61


		!HDMA_table_size	= $0080		; size of HDMA table

		; these overwrite unused parts of the level data
	if !sa1 = 1
		!BG2X			= $40D580
		!BG2Y			= $40D582
		!BG2dir			= $40D584
		!HDMA_table		= $40D586
		!flowerspeed		= $40D686
		!DestroyTileBuffer	= $40D688
	else
		!BG2X			= $7ED580
		!BG2Y			= $7ED582
		!BG2dir			= $7ED584
		!HDMA_table		= $7ED586
		!flowerspeed		= $7ED686
		!DestroyTileBuffer	= $7ED688
	endif

	;format of !DestroyTileBuffer : x coordinate, y coordinate


	; routines
		!GetMarioClipping	= $03B664|!BankB
		!GetSpriteClipping04	= $03B69F|!BankB
		!GetSpriteClipping00	= $03B6E5|!BankB
		!CheckContact		= $03B72B|!BankB
		!ApplySpeed		= $01802A|!BankB
		!HurtMario		= $00F5B7|!BankB
		!SpinJumpStars		= $07FC3B|!BankB
		!ResetSprite		= $07F7D2|!BankB


	; RAM
		!OAM			= $0200|!addr
		!OAMhi			= $0420|!addr

		!spriteindex		= $15E9|!addr

		!levelheight		= $13D7|!addr

		!shakescreen		= $1887|!addr

		!msgtrigger		= $1426|!addr

		!HDMA			= $0D9F|!addr




	if !sa1 = 1
		!map16lo		= $40C800
		!map16hi		= $41C800
	else
		!map16lo		= $7EC800
		!map16hi		= $7FC800
	endif

		!SPC1			= $1DF9|!addr
		!SPC2			= $1DFA|!addr
		!SPC3			= $1DFB|!addr
		!SPC4			= $1DFC|!addr



	; mario RAM
		!marioanim		= $71

		!marioxspeed		= $7B
		!marioyspeed		= $7D

		!mariox			= $94
		!marioy			= $96

		!mariospinjump		= $140D|!addr


	; sprite regs
		!x_lo			= !sprite_x_low
		!x_hi			= !sprite_x_high
		!y_lo			= !sprite_y_low
		!y_hi			= !sprite_y_high
		!xlo			= !x_lo
		!xhi			= !x_hi
		!ylo			= !y_lo
		!yhi			= !y_hi
		!xsub			= !sprite_speed_x_frac
		!ysub			= !sprite_speed_y_frac
		!xspeed			= !sprite_speed_x
		!yspeed			= !sprite_speed_y
		!blocked		= !sprite_blocked_status

		!dir			= !sprite_misc_157c



		!iframes		= !sprite_misc_1558		; auto-decrements


		!dontinteract		= !sprite_misc_154c		; auto-decrements, prevents body hitbox interaction with player


		!animindex		= !sprite_misc_1510
		!animtimer		= !sprite_misc_151c
		!attack			= !sprite_misc_1528
		!prevattack1		= !sprite_misc_1534
		!prevattack2		= !sprite_slope			; this is fine, boss won't use slopes
		!attacktimer		= !sprite_misc_1540		; auto-decrements

		!phase			= !sprite_misc_1594
		!nobouncetimer		= !sprite_misc_15ac

		!hp			= !sprite_off_screen_horz	; 15A0


	; phase 1
		!glasshp		= !sprite_misc_1504
		!bobombindex		= !sprite_being_eaten
		!jetboostcooldown	= !sprite_cape_disable_time
		!legoffset		= !sprite_off_screen_vert	; 186C
		!countercounter		= !sprite_misc_1570
		!hurttimer		= !sprite_misc_1564		; auto-decrements

	; phase 2
		!shoothitbox		= !sprite_misc_1504
		!guntarget		= !sprite_being_eaten
		!jawoffset		= !sprite_off_screen_vert

	; phase 3





	; dynamic z def
		!ResourceIndex = $00
		%GFXTabDef(!ResourceIndex)
		%GFXDef(00)


	;	!FrameIndex		= !SpriteMiscTable1	; 0DF5	(which frame we're on, global (basically what !animindex usually means to me))
	;	!AnimationIndex		= !SpriteMiscTable2	; 0E0B	(which animation is playing)
	;	!AnimationFrameIndex	= !SpriteMiscTable3	; C2	(which frame in the animation we are on)
	;	!AnimationTimer		= !SpriteMiscTable7	; 1528	(counts down until the next frame of the animation should run)
	;	!LastFrameIndex		= !SpriteMiscTable6	; 151C	(written in MAIN's macro call, determines if GFX has to update)

		!FrameIndex		= !SpriteMiscTable1	; reg added by dyzen(?)
		!AnimationFrameIndex	= !SpriteMiscTable2	; reg added by dyzen(?)
		!AnimationIndex		= !animindex
		!AnimationTimer		= !animtimer
		!LastFrameIndex		= !sprite_misc_c2





; TODO:
;	- implement a "secret hard mode", similar to troupemaster grimm
;





macro def_anim(name)
	!anim_<name> := !Temp
	!Temp := !Temp+1
endmacro


	!Temp = 0
	%def_anim(idle)
	%def_anim(shoot)
	%def_anim(walk)
	%def_anim(jump)
	%def_anim(hurt)
	%def_anim(throwgrenade)
	%def_anim(idle2)
	%def_anim(walk2)
	%def_anim(claw)
	%def_anim(claw2)
	%def_anim(jump2)
	%def_anim(throw)



; output:
;	BEQ -> triggers if animation was not updated
;	BNE -> triggers if animation was updated
macro anim(name)
	LDA.b #!anim_<name>
	CMP !AnimationIndex,x : BEQ ?noupdate
	JSR UpdateAnim
	REP #$02				; clear z flag
	?noupdate
endmacro

macro checkanim(name)
	LDA.b #!anim_<name>
	CMP !AnimationIndex,x
endmacro




macro decreg(reg)
	LDA <reg>,x : BEQ ?End
	DEC <reg>,x
	?End:
endmacro



	print "INIT ", pc
	INIT:
		PHB : PHK : PLB

		LDX !spriteindex				; restore X

		LDA #$00
		STA !extra_prop_1,x
		STA !extra_byte_2,x

		STZ !chunkcooldown+0
		STZ !chunkcooldown+1
		STZ !chunkcooldown+2
		STZ !chunkcooldown+3
		STZ !chunkcooldown+4
		STZ !chunkcooldown+5
		STZ !chunkcooldown+6
		STZ !chunkcooldown+7


	; debug
;	LDA #$03
;	STA !phase,x
;	LDA #$03 : STA $19

		; dz code
		LDA #$C0 : STA !extra_byte_1,x			; 60 fps mode
		LDA #$00 : JSR UpdateAnim
		LDA #$FF : STA !LastFrameIndex,x
		%CheckSlotNormalSprite(#$09, $00)

		PLB
		RTL



	DATA:
	incsrc "norveg_data.asm"
	incsrc "norveg_anim.asm"
	incsrc "norveg_tank_anim.asm"



	print "MAIN ", pc
	MAIN:
		PHB : PHK : PLB

		; mode check
		LDA !extra_prop_1,x : BNE .Debris
		JMP .Boss

		.Debris
		STZ !legoffset,x
		LDA #$00 : STA !extra_prop_2,x
		LDA !yhi,x : XBA
		LDA !ylo,x
		REP #$20
		SEC : SBC $1C
		CMP #$0140
		SEP #$20
		BCC ..process
		..despawn
		LDA !extra_prop_1,x
		CMP #$0B : BNE ..normaldespawn
		..normaldespawn
		STZ !sprite_status,x
		PLB
		RTL

		..process
		LDA $9D : BNE ..draw

		LDA !extra_prop_1,x
		CMP #$0B : BNE ..nocollision
		..collision
		LDX.b #!SprSize-1
	-	LDA !sprite_status,x
		CMP #$08 : BNE +
		LDA !extra_bits,x
		AND #$08 : BEQ +
		LDA !new_sprite_num,x
		CMP.b #!BossNum : BNE +
		LDA !extra_prop_1,x : BNE +
		%checkanim(throw) : BNE +
		LDA !phase,x
		AND #$7F
		CMP #$04 : BNE +
		JSR .ThrowAnim
		BRA ..nocollision
	+	DEX : BPL -
		LDX !spriteindex
		..nocollision

		JSL !ApplySpeed-$10
		JSL !ApplySpeed-$08
		LDA !yspeed,x : BMI ..gravity
		CMP #$40 : BCS ..nogravity
		..gravity
		INC !yspeed,x
		INC !yspeed,x
		INC !yspeed,x
		..nogravity

		..draw
		LDA !dir,x
		BEQ $02 : LDA #$40
		; EOR #$40						; > the GFX already face right
		STA $06
		STZ $0A
		STZ $0C
		LDX #$44
		..loop
		LDA !OAM+$001,x
		CMP #$F0 : BEQ ..thisone
		INX #4 : BNE ..loop
		..thisone
		STX $0E
		STZ $0F
		LDX !spriteindex
		LDA !extra_prop_1,x
		DEC A
		ASL A : TAY
		REP #$20
		LDA Tank_Ptr,y : JSR DRAWTANK_Draw

		..return
		PLB
		RTL


		.ThrowAnim
		TXY
		LDX !spriteindex
		LDA !AnimationFrameIndex,y : BEQ ..hold
		CMP #$02 : BEQ ..hold2
		CMP #$03 : BNE ..return
		..throw
		LDA !animtimer,y
		CMP #$FF : BNE ..return
		LDA #$25 : STA !SPC1				; roar sfx
		LDA #$C0 : STA !yspeed,x
		LDA !dir,y : BEQ ..right
		..left
		LDA #$C0 : STA !xspeed,x
		RTS
		..right
		LDA #$40 : STA !xspeed,x
		RTS

		..hold
		LDA.w !ylo,y
		CLC : ADC #$40
		STA !ylo,x
		LDA.w !yhi,y
		ADC #$00
		STA !yhi,x
		STZ !xspeed,x
		LDA #$B0 : STA !yspeed,x
		..return
		RTS

		..hold2
		LDA !animtimer,y
		CMP #$10 : BCS ..return
		STZ !yspeed,x
		LDA #$20 : TSB !shakescreen
		RTS


		.Boss

	; flower controller (yes it needs !spriteindex)
		PHX
		LDX #!SprSize-1
	-	LDA !sprite_status,x : BNE ..c : JMP +
	..c	LDA !extra_bits,x
		AND #$08 : BNE +
		LDA !blocked,x
		AND #$04 : BNE +
		LDA !sprite_num,x
		CMP #$75 : BNE +
		%SubHorzPos()
		LDA #$01
		BIT !yspeed,x
		BMI $02 : LDA #$03
		STA $01
		ASL A : STA $02
		LDA DATA_FlowerXSpeed,y : STA $00
		SEC : SBC !flowerspeed
		CLC : ADC $01
		CMP $02 : BCC ..t
		LDA !flowerspeed
		BIT $00 : BMI ..l
	..r	CMP #$80 : BCS ..inc
		CMP $00 : BCC ..inc
	..dec	SEC : SBC $01
		BRA ..w
	..l	CMP #$80 : BCC ..dec
		CMP $00 : BCS ..dec
	..inc	CLC : ADC $01
		BRA ..w
	..t	LDA $00
	..w	STA !flowerspeed
		STA !xspeed,x
		STX !spriteindex
		JSL !ApplySpeed-$08

		LDA !y_hi,x
		XBA
		LDA !y_lo,x
		REP #$20
		CMP.w #!flowerground : SEP #$20 : BCC +
		LDA.b #!flowerground>>8 : STA !y_hi,x
		LDA.b #!flowerground : STA !y_lo,x

	+	DEX : BMI ..end : JMP -
	..end	PLX
		STX !spriteindex



	; BG3 controller
	;	REP #$20
	;	STZ $24
	;	LDA $1A
	;	LSR A
	;	STA $22
	;	SEP #$20


	; RNG code
		LDA !RNG : %Random()
		ADC $15
		ADC $16
		ADC $17
		ADC $18
		ADC $19
		ADC $94
		ADC $95
		ADC $96
		ADC $97
		ADC $7B
		ADC $7D
		ADC $77
		ADC $13DA
		ADC $13DC
		%Random()
		ADC !xlo,x
		ADC !xhi,x
		ADC !ylo,x
		ADC !yhi,x
		ADC !xspeed,x
		ADC !yspeed,x
		ADC !blocked,x
		ADC !sprite_speed_x_frac,x
		ADC !sprite_speed_y_frac,x
		STA !RNG

	; level loop code
		REP #$20				;\
		LDA $1A					; |
		CMP #$02E0				; | check loop threshold
		SEP #$20				; |
		BCC +					;/
		XBA					;\
		SBC #$02				; |
		STA $1B					; | loop camera
		DEC $1463|!addr				; |
		DEC $1463|!addr				;/
		DEC $D2					;\
		DEC $D2					; | loop mario
		DEC !mariox+1				; |
		DEC !mariox+1				;/
		LDY.b #!SprSize-1			;\
	-	LDA !xhi,y				; |
		DEC #2					; | loop sprites (LDA/DEC#2/STA saves 1 cycle over double DEC addr,x)
		STA !xhi,y				; |
		DEY : BPL -				;/
		LDY #$09				;\
	-	LDA $1733|!addr,y			; |
		DEC #2					; | loop extended sprites
		STA $1733|!addr,y			; |
		DEY : BPL -				;/
		LDY #$05				;\
	-	LDA $16F3|!addr,y			; |
		DEC #2					; | loop score sprites
		STA $16F3|!addr,y			; |
		DEY : BPL -				;/
		LDY #$0B				;\
	-	LDA $18EA|!addr,y			; |
		DEC #2					; | loop minor extended sprites
		STA $18EA|!addr,y			; |
		DEY : BPL -				;/
		+					; done


		; dz code
		LDY !FrameIndex,x
		LDA ResourceLastRow,y : STA $52
		%EasyNormalSpriteDynamicRoutineFixedGFX("!FrameIndex,x", "!LastFrameIndex,x", !GFX00, "#ResourceOffset", "#ResourceSize", $52)
		LDA DZ_DS_Loc_US_Normal,x : TAX
		LDA DZ_DS_Loc_IsValid,x : BNE .Valid
		LDX !spriteindex
		PLB
		RTL

		.Valid
		; sprite main code
		LDX !spriteindex
		LDA $9D : BEQ .Process
		JMP GRAPHICS
		.Process





	TIMERS:
		%decreg(!nobouncetimer)




	PHASE:
		PEA.w HANDLE_ANIMATION-1
		LDA !phase,x
		ASL A
		CMP.b #.Ptr_end-.Ptr
		BCC $02 : LDA #$00
		.Loop
		TAX
		PHX
		JSR (.Ptr,x)
		PLA : STA $00
		LDA !phase,x
		ASL A
		CMP $00 : BNE .Loop		; prevent 1 frame gfx bugs
		RTS


		.Ptr
		dw INTRO			; 0
		dw P1_ATTACK			; 1
		dw TransitionToPhase2		; 2
		dw P2_ATTACK			; 3
		dw TransitionToPhase3		; 4
		dw P3_ATTACK			; 5
		dw DEATH			; 6
		..end



	INTRO:
		LDX !spriteindex
		LDA !phase,x : BMI .Main
		.Init
		ORA #$80 : STA !phase,x
		%anim(jump)
		LDA #$80 : STA !ylo,x
		STZ !yhi,x
		LDA #$26 : STA !SPC1				; fall sfx
		STZ !hp,x
		%SubHorzPos()
		TYA : STA !dir,x
		INC !glasshp,x

		.Main
		LDA !blocked,x
		AND #$04 : PHA
		JSL !ApplySpeed
		PLA : BNE ..noland
		LDA !blocked,x
		AND #$04 : BEQ ..noland
		LDA !hp,x : BEQ ..noland
		..land
		LDA #$0F : STA !shakescreen
		LDA #$09 : STA !SPC4				; boom sfx
		JSR .LandDust
		..noland

		LDA !blocked,x
		AND #$04 : BNE ..ground

		..air
		LDA !attacktimer,x
		CMP #$80 : BNE ..nowarp
		..warp
		LDA #$2F : STA !xlo,x
		STZ !xhi,x
		LDA #$80 : STA !ylo,x
		STZ !yhi,x
		INC !hp,x
		STZ !yspeed,x
		STZ !dir,x
		LDA #$26 : STA !SPC1				; fall sfx

	;-	BIT $4212 : BVC -				;\
	;	LDA #$13					; |
	;	STA $212C					; |
	;	STA $212E					; | wait for h-blank to update main/sub screen designation
	;-	BIT $4212 : BVC -				; | BG1 + BG2 + sprites on main
	;	LDA #$04					; | BG3 on sub
	;	STA $212D					; |
	;	STA $212F					;/

		RTS

		..nowarp
		LDA !hp,x : BNE .Return
		LDA !yspeed,x : BPL ..timer
		LDA #$80 : STA !yspeed,x
		RTS

		..timer
		LDA #$FF : STA !attacktimer,x
		RTS

		..ground
		%anim(idle)
		LDA !attacktimer,x : BEQ .Attack
		CMP #$E0 : BEQ ..msg1
		CMP #$10 : BEQ ..msg2
		CMP #$D0 : BNE .Return

		..jump
		%anim(jump)
		LDA #$80 : STA !yspeed,x
		LDA #$80 : STA !SPC3				; fade music
		JMP Pounce_JumpDust
		..msg1
		LDA #$01 : STA !msgtrigger
		RTS
		..msg2
		LDA #$02 : STA !msgtrigger
		RTS

		.Attack
		LDA #$01 : STA !phase,x
		LDA.b #!battlemusic : STA !SPC3			; battle music

		.Return
		RTS


		.LandDust
		LDA !xlo,x : PHA
		SEC : SBC #$20
		STA !xlo,x
		JSR Pounce_JumpDust
		LDA !xlo,x
		CLC : ADC #$30
		STA !xlo,x
		JSR Pounce_JumpDust
		PLA : STA !xlo,x
		RTS




	DEATH:
		LDX !spriteindex
		LDA !phase,x : BMI .Main
		.Init
		ORA #$80 : STA !phase,x
		%anim(hurt)
		%SubHorzPos()
		TYA : STA !dir,x
		LDA #$50 : STA !attacktimer,x
		LDA #$80 : STA !SPC3				; fade music
		LDA #$40 : STA !iframes,x
		.Main

		LDA !attacktimer,x : BEQ .Die
		CMP #$10 : BNE .Return
		LDA #$02 : STA !msgtrigger

		.Return
		RTS

		.Die
		LDA #$A0 : STA !TimerForVictoryAfterBossDeath
		LDA #$04 : STA !sprite_status,x
		LDA #$1F : STA !sprite_misc_1540,x
		LDA #$08 : STA !SPC1				; spin jump crush sfx
		JSL !SpinJumpStars
		RTS



	P1_ATTACK:
		LDX !spriteindex
		PEA.w P1_WALK-1
		STZ !dir,x

		.Speed
		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		SEC : SBC $1A
		STA $00
		SEP #$20
		CMP #$2E : BCC ..8

		..0
		LDA #$80 : STA !jetboostcooldown,x
		STZ !xspeed,x
		JSR ExhaustSmoke
		LDA !attack,x
		AND #$7F
		CMP #$02 : BNE ..done
		LDA !extra_byte_2,x
		CMP #$0C : BEQ ..done
		INC A : STA !extra_byte_2,x
		BRA ..done

		..8
		LDA !extra_byte_2,x : BEQ +
		DEC A : STA !extra_byte_2,x
	+	REP #$20
		LDA $1A
		CLC : ADC #$002D
		SEP #$20
		STA !xlo,x
		XBA : STA !xhi,x
		XBA
		AND #$3F
		CMP #$2E : BEQ ..step1
		CMP #$0E : BNE ..nostep
		..step2
		JSR Foot1Smoke
		BRA ..shake
		..step1
		JSR Foot2Smoke
		..shake
		LDA #$05 : STA !shakescreen
		LDA #$09 : STA !SPC4				; boom sfx
		..nostep
		..done


		.Hurt
		LDA !hurttimer,x : BEQ ..done
		%anim(hurt)
		RTS
		..done


		.Revenge
		%checkanim(claw2) : BNE ..done
		LDA !AnimationFrameIndex,x
		CMP #$01 : BEQ ..attack
		CMP #$02 : BNE ..return
		LDA !animtimer,x : BEQ ..done
		RTS
		..attack
		REP #$10
		LDY.w #DATA_CounterHitbox1 : JSR HITBOX
		JSL !GetMarioClipping
		JSL !CheckContact : BCC ..return
		JSL !HurtMario
		LDA #$30 : STA !marioxspeed
		LDA #$09 : STA !SPC4				; boom sfx
		..return
		RTS
		..done



		LDA !attacktimer,x : BNE .Process
		LDA !prevattack1,x : STA !prevattack2,x			;\
		LDA !attack,x						; | rotate attacks into memory for ai
		AND #$7F : STA !prevattack1,x				;/


		.Roll
		%anim(idle)
		LDA !RNG
		AND #$0F
		TAY
		LDA .RandomAttack,y
		CMP !prevattack1,x : BNE .Fine
		CMP !prevattack2,x : BNE .Fine
		INC A

		.Fine
		CMP #$02 : BNE ..notjetboost
		LDY !jetboostcooldown,x : BEQ ..notjetboost
		LDA #$01
		..notjetboost
		CMP.b #(.Ptr_end-.Ptr)/2
		BCC $02 : LDA #$00
		STA !attack,x

		.Process
		LDA !attack,x
		ASL A
		TAX
		JMP (.Ptr,x)


		.RandomAttack
		db $00,$00,$00,$00
		db $00,$00,$01,$01
		db $01,$01,$01,$01
		db $02,$02,$02,$02


		.Ptr
		dw ShootBulletBill	; 00
		dw TossBobomb		; 01
		dw JetBoost		; 02
		dw MechWait		; 03
		..end




	P1_WALK:
		LDX !spriteindex
		LDA !phase,x : BMI .Main
		.Init
		ORA #$80 : STA !phase,x
		LDA.b #!phase1hp : STA !hp,x
		LDA.b #$08 : STA !glasshp,x
		JSR StartScroll
		INC !UploadMessages34Flag

		.Main


		.XSpeed
		LDA !xspeed,x : BEQ ..done
		ASL #4
		CLC : ADC !xsub,x
		STA !xsub,x
		PHP
		LDY #$00
		LDA !xspeed,x
		LSR #4
		CMP #$08
		BCC $03 : ORA #$F0 : DEY
		PLP
		ADC !xlo,x
		STA !xlo,x
		TYA
		ADC !xhi,x
		STA !xhi,x
		LDA !xlo,x
		AND #$3F
		CMP #$2E : BEQ ..step1
		CMP #$2F : BEQ ..step1
		CMP #$0E : BEQ ..step2
		CMP #$0F : BNE ..nostep
		..step2
		JSR Foot1Smoke
		BRA ..shake
		..step1
		JSR Foot2Smoke
		..shake
		LDA #$05 : STA !shakescreen
		LDA #$09 : STA !SPC4				; boom sfx
		..nostep
		..done

		.YSpeed
		LDA !yspeed,x : BEQ ..done
		ASL #4
		CLC : ADC !ysub,x
		STA !ysub,x
		PHP
		LDY #$00
		LDA !yspeed,x
		LSR #4
		CMP #$08
		BCC $03 : ORA #$F0 : DEY
		PLP
		ADC !ylo,x
		STA !ylo,x
		TYA
		ADC !yhi,x
		STA !yhi,x
		..done

		LDA !yspeed,x : BMI ..gravity
		CMP #$10 : BCC ..gravity
		..gravity
		INC #3
		..nogravity
		CMP #$80 : BCS ..gravitydone
		CMP #$10 : BCC ..gravitydone
		LDA #$10
		..gravitydone
		STA !yspeed,x

	P1_Collision:
	; reset stuff
		STZ !blocked,x
		LDA !xlo,x : STA $0C
		LDA !xhi,x : STA $0D
		LDA !ylo,x : STA $0E
		LDA !yhi,x : STA $0F

	; down collision point
		LDA !yspeed,x : BMI .Up1

		.Down
		REP #$20
		LDA #$0008 : CLC : ADC $0C : STA $9A
		LDA #$0010 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16 : JSR GetActsLikeOfMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		LDA !blocked,x
		ORA #$04
		STA !blocked,x
		LDA !ylo,x
		AND #$F0
		STA !ylo,x
		..done
		JMP .Front1


	; up collision points
		.Up1
		REP #$20
		LDA #$FFF8 : CLC : ADC $0C : STA $9A
		LDA #$FFD0 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Up2
		REP #$20
		LDA #$0008 : CLC : ADC $0C : STA $9A
		LDA #$FFD0 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Up3
		REP #$20
		LDA #$0018 : CLC : ADC $0C : STA $9A
		LDA #$FFD0 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Up4
		REP #$20
		LDA #$0028 : CLC : ADC $0C : STA $9A
		LDA #$FFD0 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done




	; front collision points
		.Front1
		REP #$20
		LDA #$0028 : CLC : ADC $0C : STA $9A
		LDA #$FFD8 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Front2
		REP #$20
		LDA #$0028 : CLC : ADC $0C : STA $9A
		LDA #$FFE8 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Front3
		REP #$20
		LDA #$0010 : CLC : ADC $0C : STA $9A
		LDA #$FFF8 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ .Corner			; skip front 4
		..collision
		JSR AddToDestroyTileBuffer
		..done

		.Front4
		REP #$20
		LDA #$0010 : CLC : ADC $0C : STA $9A
		LDA #$0008 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		JSR AddToDestroyTileBuffer
		..done
		BRA .Corner_done




	; corner collision points
		.Corner
		REP #$20
		LDA #$0010 : CLC : ADC $0C : STA $9A
		LDA #$0008 : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16 : JSR GetActsLikeOfMap16
		CMP #$25 : BNE ..collision
		XBA : BNE ..collision
		..corner2
		REP #$20
		LDA #$000C : CLC : ADC $0C : STA $9A
		LDA #$000B : CLC : ADC $0E : STA $98
		JSR GetMap16Index : JSR GetMap16 : JSR GetActsLikeOfMap16
		CMP #$25 : BNE ..collision
		XBA : BEQ ..done
		..collision
		LDA #$F0 : STA !yspeed,x
		LDA !attack,x
		AND #$7F
		CMP #$02 : BNE ..done
		LDA !xspeed,x : BNE ..done
		INC !xlo,x : BNE ..done
		INC !xhi,x
		..done






		.YAdjust
		LDA !yhi,x : BEQ ..done
		CMP #$02 : BCS ..adjust
		LDA !ylo,x
		CMP #$90 : BCC ..done
		..adjust
		LDA #$90 : STA !ylo,x
		LDA #$01 : STA !yhi,x
		..done


	P1_DestroyTiles:
		LDA !DestroyTileBufferIndex
		BEQ .done
		TAX
		REP #$20
		LDA !DestroyTileBuffer-4,x
		STA $9A
		LDA !DestroyTileBuffer-2,x
		STA $98
		SEP #$20
		TXA : SEC : SBC #$04 : STA !DestroyTileBufferIndex
		REP #$20
		LDA #$0025 : JSR ChangeMap16Graphics  ;return with 8-bit A
		JSR SpawnBrickPiece
		.done


	P1_Chunks:
	.chunks
	LDA $14
	LSR A : BCS +
	ASL A
	AND #$1F
	TAY
		REP #$20
		LDA DATA_CleanerTable,y : BEQ +
		STA $04
		LDA $1A
		SEC : SBC #$0010
		BPL $03 : LDA #$0000
		STA $00
		LDA #$00C0 : STA $02
		JSR GenerateChunk
	+
		SEP #$20



	LDA $14
	AND #$7F : BNE ..nochunk

		LDX.b #((DATA_ChunkTable_end-DATA_ChunkTable)/2)-1
	-	LDA !chunkcooldown,x : BEQ +
		DEC !chunkcooldown,x
	+	DEX : BPL -

		LDA !RNG
	-	AND #$07 : TAX
		LDA !chunkcooldown,x : BEQ ..validchunk
		..newchunk
		TXA
		INC A
		BRA -
		..validchunk
		TXA
		ASL A
		TAY
		LDA #$03 : STA !chunkcooldown,x
		REP #$20
		LDA DATA_ChunkTable,y : STA $04

		LDA $1A
		CLC : ADC #$01A0
		STA $00
		LDA $1C : STA $02
		JSR GenerateChunk
		..nochunk



	P1_INTERACTION:
		LDX !spriteindex

		STZ $01
		LDA !legoffset,x
		CLC : ADC !extra_byte_2,x
		STA $00
		BPL $02 : DEC $01
		LDA !ylo,x : PHA
		CLC : ADC $00
		STA !ylo,x
		LDA !yhi,x : PHA
		ADC $01
		STA !yhi,x


	; check mario clipping
		JSL !GetMarioClipping


	; optimization: only load 1 spike on any 1 frame to save cycles (based on which side mario is on)
		%SubHorzPos()
		TYA
		CMP !dir,x : BNE .TopSpike
		.FrontSpike
		REP #$10
		LDY.w #DATA_FrontSpike : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		JSL !HurtMario
		..nocontact
		BRA .Foot

		.TopSpike
		REP #$10
		LDY.w #DATA_TopSpike : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		JSL !HurtMario
		..nocontact


		.Foot
		REP #$10
		LDY.w #DATA_Foot : JSR HITBOX
		JSL !CheckContact : BCC ..nomariocontact
		JSL !HurtMario
		..nomariocontact
		LDY !bobombindex,x : BMI ..nocontact
		LDA !1534,y : BNE ..nocontact
		LDA !sprite_status,y
		CMP #$08 : BCC ..nocontact
		TYX
		LDA !extra_bits,x
		AND.b #!CustomBit : BNE ..nobobombcontact
		LDA !sprite_num,x
		CMP #$0D : BNE ..nobobombcontact
		JSL !GetSpriteClipping00
		JSL !CheckContact : BCC ..nobobombcontact
		LDA #$02 : STA !sprite_status,x
		STZ !xspeed,x
		..nobobombcontact
		LDX !spriteindex
		JSL !GetMarioClipping
		..nocontact


	; handle revenge attack
		.CounterCounter
		LDA $77
		AND #$04 : BEQ ..noreset
		STZ !countercounter,x
		..noreset
		LDA !countercounter,x
		CMP #$02 : BCC ..done
		JSR RevengeSmoke
		JSL !GetMarioClipping				; reload this
		LDA !marioyspeed : BMI ..done
		REP #$10
		LDY.w #DATA_CounterSightbox1 : JSR HITBOX
		JSL !CheckContact : BCC ..done
		%anim(claw2)
		LDA #$25 : STA !SPC1				; roar sfx
		..done


	; boss hurtbox
		.Hurtbox
		LDA !nobouncetimer,x : BEQ ..process
		..fail
		JMP ..nocontact
		..process
		REP #$10
		LDA !glasshp,x : BNE ..glass
		..pilot
		LDY.w #DATA_Pilot : BRA ..shared
		..glass
		LDY.w #DATA_Glass
		..shared
		JSR HITBOX
		LDA !dontinteract,x : BNE ..fail
		JSL !CheckContact : BCC ..nocontact
		LDY #$03
		..loop
		LDA $17C0|!addr,y : BEQ ..thisone
		DEY : BPL ..loop
		LDY #$00
		..thisone
		LDA #$02 : STA $17C0|!addr,y
		LDA #$08 : STA $17CC|!addr,y
		LDA !mariox : STA $17C8|!addr,y
		LDA !marioy
		CLC : ADC #$18
		STA $17C4|!addr,y
		LDA #$02 : STA !SPC1
		LDA #$C8
		BIT $15
		BPL $02 : LDA #$A0
		STA !marioyspeed
		LDA #$08 : STA !nobouncetimer,x
		LDA !glasshp,x : BNE ..damageglass
		..damagepilot
		LDA !iframes,x : BNE ..nocontact
		JSR HurtBoss
		LDA #$21 : STA !hurttimer,x
		BRA ..nocontact
		..damageglass
		LDA #$0F : STA !dontinteract,x
		LDA !mariospinjump : BEQ ..1dmg
		..4dmg
		LDA #$1F : STA !shakescreen
		LDA #$18 : STA !SPC4			; crash sfx
		LDA !glasshp,x
		SEC : SBC #$04
		STA !glasshp,x
		BRA ..checkglass
		..1dmg
		DEC !glasshp,x
		..checkglass
		LDA !glasshp,x
		BEQ ..falloff
		BPL ..nocontact
		STZ !glasshp,x
		..falloff
		JSR DropGlass
		..nocontact



; 0 -> fireball 1
; 1 -> fireball 2
; 2 -> bobomb
; 3 -> bobomb

	; optimization: only check 1 fireball per frame
		.PlayerFireball
		LDA $14
		AND #$03
		CMP #$02 : BCS ..nocontact
		ORA #$08
		TAY
		LDA $170B|!addr,y
		CMP #$05 : BNE ..nocontact
		LDA $171F|!addr,y : STA $00		; xlo
		LDA $1733|!addr,y : STA $08		; xhi
		LDA $1715|!addr,y : STA $01		; ylo
		LDA $1729|!addr,y : STA $09		; yhi
		LDA #$08
		STA $02
		STA $03
		JSL !CheckContact : BCC ..nocontact
		LDA $14
		AND #$01
		ORA #$08
		TAY
		LDA #$01 : STA $170B|!addr,y
		LDA #$0F : STA $176F|!addr,y
		LDA #$01 : STA !SPC1
		LDA !glasshp,x : BNE ..nocontact
		LDA #$40 : STA !iframes,x
		DEC !hp,x
		JSR HurtBoss_CheckHP
		..nocontact


	; optimization: only check bob-omb explosion every other frame
		.Bobomb
		LDA !iframes,x : BNE ..nocontact
		LDA $14
		AND #$02 : BCC ..nocontact
		LDA !bobombindex,x : TAX
		LDA !1564,x : BNE ..done
		LDA !extra_bits,x
		AND #$08 : BNE ..done
		LDA !sprite_status,x
		CMP #$08 : BCC ..done
		CMP #$0B : BEQ ..done
		LDA !sprite_num,x
		CMP #$0D : BNE ..done
		JSR GetBombHitbox
		JSL !CheckContact : BCC ..done
		LDA !1534,x : BNE ..damage
		LDA !sprite_status,x
		CMP #$09 : BNE ..done
		JSR ExplodeBomb
		BRA ..done
		..damage
		LDX !spriteindex
		LDA !glasshp,x : BEQ ..hurt
		STZ !glasshp,x
		JSR DropGlass
		LDA #$40 : STA !iframes,x
		BRA ..nocontact
		..hurt
		JSR HurtBoss
		LDA #$21 : STA !hurttimer,x
		..done
		LDX !spriteindex
		..nocontact

		PLA : STA !yhi,x
		PLA : STA !ylo,x
		RTS


	DropGlass:
		LDA #$19 : STA !SPC4			; bubble pop sfx
		STZ $00
		STZ $01
		LDA #$20 : STA $02
		LDA #$C0 : STA $03
		SEC : LDA.b #!BossNum
		%SpawnSprite()
		BCS .Return
		TYX
		LDA #$04 : STA !extra_prop_1,x
		LDA #$08 : STA !sprite_status,x
		LDX !spriteindex
		.Return
		RTS



	TossBobomb:
		LDX !spriteindex
		.OffScreen
		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		SEC : SBC $1A : BMI ..checkleft
		CMP #$00F0 : BCC ..ok
		..checkright
		SEP #$20
		LDA !dir,x : BEQ ..end
		..wait
		INC !attacktimer,x
		RTS
		..checkleft
		SEP #$20
		LDA !dir,x : BEQ ..wait
		..end
		STZ !attacktimer,x
		RTS
		..ok
		SEP #$20


		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		LDA #$3C : STA !attacktimer,x
	;	LDA !glasshp,x : BNE ..fail
		LDA !phase,x
		AND #$7F
		CMP #$01 : BNE ..ok
		LDY !bobombindex,x				;\
		LDA !sprite_status,y : BEQ ..ok			; |
		TYX						; |
		LDA !extra_bits,x				; | check if a bobomb already exists
		LDX !spriteindex				; |
		AND.b #!CustomBit : BNE ..ok			; |
		LDA !sprite_num,y				; |
		CMP #$0D : BNE ..ok				;/
		..fail
		STZ !attack,x
		BRA ShootBulletBill

		..ok
		%anim(throwgrenade)

		.Main
		LDA !attacktimer,x
		CMP #$2C : BNE .Return

		..throw
		LDY !dir,x
		LDA .XDisp,y : STA $00
		LDA !phase,x
		AND #$7F
		CMP #$01
		BEQ $02 : INY #2
		LDA .XSpeed,y : STA $02
		LDA #$C0
		CLC : ADC !legoffset,x
		CLC : ADC !extra_byte_2,x
		STA $01
		LDA #$E0 : STA $03
		CLC : LDA #$0D
		%SpawnSprite()
		TYA : STA !bobombindex,x		; save bobomb index
		LDA #$10 : STA !SPC1			; throw sfx
		BCS .Return
		LDA #$10
		STA !154C,y
		STA !1564,y


		.Return
		RTS

		.XDisp
		db $0E,$F6
		.XSpeed
		db $30,$D0
		db $20,$E0


	ShootBulletBill:
		LDX !spriteindex
		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		%anim(idle)
		LDA #$3C : STA !attacktimer,x
		..spawn
		LDA #$F2
		LDY !dir,x
		BNE $02 : LDA.b #-$F2
		STA $00
		PHA
		LDA #$E6
		CLC : ADC !legoffset,x
		CLC : ADC !extra_byte_2,x
		STA $01
		PHA
		CLC : LDA #$1C
		%SpawnSprite()
		BCS ..fail
		LDA #$08 : STA !sprite_status,y
		..fail
		PLA : STA $01
		PLA : STA $00
		STZ $02
		STZ $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Main
		LDA #$02 : STA !sprite_misc_1528,y
		LDA #$0F : STA !sprite_misc_1540,y

		.Main
		RTS


	JetBoost:
		LDX !spriteindex
		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		LDA #$7C : STA !attacktimer,x
		%anim(jump)
		.Main
		LDA !attacktimer,x
		CMP #$48 : BCS .Return
		INC !attacktimer,x
		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		SEC : SBC $1A
		STA $00
		CMP #$00B0
		SEP #$20
		BCS .Stop

		.Forward
		LDA #$20 : STA !xspeed,x
		RTS

		.Stop
		LDA !xlo,x
		AND #$3F
		CMP #$0E : BEQ ..stop
		CMP #$0F : BEQ ..stop
		CMP #$2E : BEQ ..stop
		CMP #$2F : BNE .Forward
		..stop
		LDA #$E0 : STA !attacktimer,x
		INC !attack,x
		.Return
		RTS


	MechWait:
		LDX !spriteindex
		%anim(idle)
		LDA $14
		AND #$0F : BNE .Return
		LDA !extra_byte_2,x : BEQ .Return
		DEC A : STA !extra_byte_2,x

		.Return
		RTS




	TransitionToPhase2:
		LDX !spriteindex
		LDA !phase,x : BPL .Init
		JMP .Main

		.XDisp
		db $10,$10,$10,$00,$00,$00
		.YDisp
		db $00,$00,$00,$00,$00,$00
		.XSpeed
		db $12,$14,$18,$F8,$FC,$FE
		.YSpeed
		db $E8,$E4,$E0,$E0,$E4,$E8
		.Mode
		db $08,$07,$06,$06,$07,$08




		.Init
		ORA #$80 : STA !phase,x
		LDA #$80 : STA !attack,x

		JSR DropFlower

		LDA !bobombindex,x : BMI ..nobobomb
		TAX
		LDA !sprite_status,x
		CMP #$08 : BCC ..nobobomb
		LDA !extra_bits,x
		AND #$08 : BNE ..nobobomb
		LDA !sprite_num,x
		CMP #$0D : BNE ..nobobomb
		LDA #$02 : STA !sprite_status,x
		..nobobomb
		LDX !spriteindex
		%anim(hurt)

		STZ $00
		LDA !extra_byte_2,x
		CLC : ADC !legoffset,x
		BPL $02 : DEC $00
		CLC : ADC !ylo,x
		STA !ylo,x
		LDA $00
		ADC !yhi,x
		STA !yhi,x


		LDA #$00 : STA !extra_byte_2,x
		STZ !legoffset,x


	; explode legs
		LDA #$07 : STA !SPC1			; collapse sfx
		LDY.b #.YDisp-.XDisp-1
		..loop
		LDA .XDisp,y : STA $00
		LDA .YDisp,y : STA $01
		LDA .XSpeed,y : STA $02
		LDA .YSpeed,y : STA $03
		PHY
		SEC : LDA.b #!BossNum
		%SpawnSprite()
		PLA : STA $0F
		BCS .Main
		TYX
		LDY $0F
		LDA .Mode,y
		INC A : STA !extra_prop_1,x
		LDA #$08 : STA !sprite_status,x
		LDX !spriteindex
		DEY : BPL ..loop

		.Main

		.LimitPlayer
		LDA !attack,x
		CMP #$C0 : BCC ..done
		REP #$20
		LDA #$0160-1
		CMP !marioy : BCS +
		INC A
		STA !marioy
		INC $1471|!addr
	+	SEP #$20
		..done

		LDA $143E|!addr : BEQ .Process
		LDA $1A
		AND #$0F : BNE .Return
		STZ $143E|!addr			; scroll command

		.Process
		LDA !attack,x
		ASL #4
		CLC : ADC $1A
		STA $9A
		LDA $1B
		ADC #$00
		STA $9B

		LDA !attack,x
		AND #$F0
		CLC : ADC $1C
		STA $98
		LDA $1D
		ADC #$00
		STA $99
		JSR GetMap16Index
		REP #$30
		LDA !attack,x
		AND #$00FF
		ASL A
		TAY
		LDA.w DATA_Phase2Arena-$100,y
		JSR SetMap16 : JSR ChangeMap16Graphics

		INC !attack,x
		LDA !attack,x
		CMP #$F0 : BCC .Return
		LDA !phase,x
		INC A
		AND #$7F
		STA !phase,x

		.Return


	P2_Smoke:
		LDA $14
		AND #$07 : BNE .Return
		LDY !dir,x
		LDA .XDisp,y : STA $00
		LDA #$E0 : STA $01
		LDA !RNG
		AND #$07
		SBC #$04
		STA $02
		LDA #$30 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$02 : STA !sprite_misc_1528,y
		LDA #$0F : STA !sprite_misc_1540,y
		.Return
		RTS

		.XDisp
		db $EC,$14



	P2_ATTACK:
		LDX !spriteindex
		LDA !phase,x : BMI .Main
		.Init
		ORA #$80 : STA !phase,x
		LDA.b #!phase2hp : STA !hp,x
		STZ !attack,x
		STZ !attacktimer,x
		STZ !xspeed,x
		STZ !yspeed,x

		.Main
		PEA.w P2_PHYSICS-1

		; speed
		LDY !dir,x
		LDA .XSpeed,y : STA !xspeed,x
		LDA $14
		LSR #3
		AND #$1F
		SBC #$10
		BPL $03 : EOR #$FF : INC A
		SEC : SBC #$08
		STA !yspeed,x

		LDA !attacktimer,x : BNE .Process
		.Roll
		%SubHorzPos()
		TYA
		CMP !dir,x : BEQ ..nochange
		..changeside
		LDA #$06 : BRA ..setattack
		..nochange
		LDA !prevattack1,x : STA !prevattack2,x			;\
		LDA !attack,x						; | rotate attacks into memory for ai
		AND #$7F : TAY						; |
		LDA .AttackID,y : STA !prevattack1,x			;/
		LDA !RNG
		..loop
		CMP.b #(.Ptr_rollthreshold-.Ptr)/2 : BCC ..thisone
		SBC.b #(.Ptr_rollthreshold-.Ptr)/2 : BRA ..loop
		..thisone
		STA $00
		TAY
		LDA .AttackID,y
		CMP !prevattack1,x : BNE ..validattack
		CMP !prevattack2,x : BNE ..validattack
		LDA $00
		INC A : BRA ..loop
		..validattack
		LDA $00
		..setattack
		STA !attack,x
		%anim(idle)


		.Process
		LDA !attack,x
		ASL A
		CMP.b #.Ptr_end-.Ptr
		BCC $02 : LDA #$00
		TAX
		JMP (.Ptr,x)

		.Ptr
		dw Shoot	; 00
		dw TossBobomb	; 01
		dw Shoot	; 02
		dw TossBobomb	; 03
		..rollthreshold
		dw Stunned	; 04
		dw CounterPunch	; 05
		dw ChangeSide	; 06
		..end


		.AttackID
		db $00
		db $01
		db $00
		db $01
		db $FF
		db $FF
		db $FF


		.XSpeed
		db $08,$F8


	Shoot:
		LDX !spriteindex
		.OffScreen
		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		SEC : SBC $1A : BMI ..checkleft
		CMP #$00F0 : BCC ..ok
		..checkright
		SEP #$20
		LDA !dir,x : BEQ ..end
		..wait
		INC !attacktimer,x
		RTS
		..checkleft
		SEP #$20
		LDA !dir,x : BEQ ..wait
		..end
		STZ !attacktimer,x
		RTS
		..ok
		SEP #$20

		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		LDA #$80 : STA !attacktimer,x
		%anim(shoot)
		LDA !mariox
		SEC : SBC $1A
		STA !guntarget,x


		.Main

		.UpdateTarget
		LDA !xlo,x
		SEC : SBC $1A
		LDY !dir,x
		CLC : ADC .XLimit,y
		CPY #$00 : BNE ..leftlimit
		..rightlimit
		CMP !guntarget,x : BCC ..withinbounds
		BRA ..bind
		..leftlimit
		CMP !guntarget,x : BCS ..withinbounds
		..bind
		STA !guntarget,x
		..withinbounds
		LDA !mariox
		SEC : SBC $1A
		CMP !guntarget,x
		BEQ ..done
		BCS ..inc
		..dec
		DEC !guntarget,x : BRA ..done
		..inc
		INC !guntarget,x
		..done



		LDA !xlo,x : STA $00
		LDA !xhi,x : STA $01
		LDA !yhi,x : XBA
		LDA !ylo,x
		REP #$20
		SEC : SBC $1C
		SEC : SBC #$00C0
		STA $02
		LDA $00
		SEC : SBC !mariox
		STA $00
		SEP #$20

		.AnimCheck
		LDA !AnimationFrameIndex,x : BNE ..shoot
		LDA !animtimer,x : BEQ ..takeaim
		..wait
		RTS
		..takeaim
		INC !FrameIndex,x
		INC !AnimationFrameIndex,x
		LDA #$20 : STA !animtimer,x
		..shoot
		LDA !animtimer,x
		CMP #$04 : BCS ..wait


		.FireSmoke
		LDA !attacktimer,x
		CMP #$30 : BCC ..done
		LDA $14
		AND #$03 : BNE ..done
		LDA #$38 : STA !SPC4			;custom sfx
		LDA !RNG
		AND #$07
		SBC #$04
		STA $02
		LDA !RNG
		LSR #3
		AND #$07
		SBC #$04
		STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS ..done
		LDA #$02 : STA !sprite_misc_1528,y
		LDA #$0F : STA !sprite_misc_1540,y
		LDA !guntarget,x
		CLC : ADC $1A
		STA.w !xlo,y
		LDA $1B
		ADC #$00
		STA.w !xhi,y
		LDA #$74 : STA.w !ylo,y
		LDA #$01 : STA.w !yhi,y
		..done


		LDA !attacktimer,x
		CMP #$50 : BCS .AnimCheck_wait
		CMP #$30 : BCS .Shoot
		CMP #$08 : BNE +
		LDA.b #!anim_shoot : JSR UpdateAnim
		LDA #$20 : STA !animtimer,x
	+	INC !animtimer,x
		RTS


		.Shoot
		JSL !GetMarioClipping
		LDA #$13 : STA $06
		LDA #$07 : STA $07
		LDA !xlo,x : STA $04
		LDA !xhi,x : STA $0A
		LDA !ylo,x
		SEC : SBC #$38
		STA $05
		LDA !yhi,x
		SBC #$00
		STA $0B
		LDA #$70
		SEC : SBC $05
		STA $4204
		STZ $4205
		LDA $04
		SEC : SBC $1A
		SEC : SBC !guntarget,x
		LDY !dir,x : BNE +
		EOR #$FF : INC A
	+	LSR #4
		STA $4206
		NOP #8
		LDA $4214
		.Hitbox
		PHA
		JSL !CheckContact : BCC .Next
		PLA
		JSL !HurtMario
		.Return
		RTS

		.Next
		PLA : STA $0E
		LDA !guntarget,x
		CLC : ADC $1A
		STA $0C
		LDA #$00
		ADC $1B
		STA $0D

		LDY !dir,x
		LDA $04
		CLC : ADC .XDisp,y
		STA $04
		LDA $0A
		ADC .XDisp+2,y
		STA $0A
		LDA !dir,x : BNE ..leftlimit
		..rightlimit
		LDA $04
		CMP $0C
		LDA $0A
		SBC $0D : BCS .Return
		BRA ..nolimit
		..leftlimit
		LDA $04
		CMP $0C
		LDA $0A
		SBC $0D
		BCC .Return
		BMI .Return
		..nolimit
		LDA $05
		CLC : ADC $0E
		STA $05
		LDA $0B
		ADC #$00
		STA $0B

		LDA $0E : BRA .Hitbox


		.XLimit
		db $40,$C0

		.XDisp
		db $10,$F0
		db $00,$FF


	ChangeSide:
		LDX !spriteindex
		LDA !attack,x :  BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		LDA #$FF : STA !attacktimer,x
		STZ !yspeed,x
		%anim(jump)


		.Main
		LDY !dir,x
		LDA .XSpeed,y : STA !xspeed,x
		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		SEC : SBC $1A
		BPL .GoRight
		.GoLeft
		CMP #$FFC0 : BCS .Return
		SEP #$20
		LDA #$00 : BRA .Warp
		.GoRight
		CMP #$0140 : BCC .Return
		SEP #$20
		LDA #$01
		.Warp
		STA !dir,x
		STZ !xspeed,x
		STZ !attacktimer,x
		LDA #$70 : STA !ylo,x
		LDA #$01 : STA !yhi,x
		RTS

		.Return
		SEP #$20
		RTS

		.XSpeed
		db $20,$E0




	P2_PHYSICS:
		LDA $14
		LSR #2
		AND #$0F
		SEC : SBC #$08
		BPL $03 : EOR #$FF : INC A
		SEC : SBC #$04
		STA !jawoffset,x


		JSR P2_Smoke


		.Speed
		JSL !ApplySpeed-$10
		JSL !ApplySpeed-$08




	P2_INTERACTION:
		JSL !GetMarioClipping


		.CounterCounter
		LDA $77
		AND #$04 : BEQ ..noreset
		STZ !countercounter,x
		..noreset
		LDA !countercounter,x
		CMP #$02 : BCC ..done
		JSR RevengeSmoke
		JSL !GetMarioClipping				; reload this
		LDA !marioyspeed : BMI ..done
		REP #$10
		LDY.w #DATA_CounterSightbox1 : JSR HITBOX
		JSL !CheckContact : BCC ..done
		LDA #$05 : STA !attack,x
		INC !attacktimer,x
		RTS
		..done



	; optimization: only load 1 spike on any 1 frame to save cycles (based on which side mario is on)
		%SubHorzPos()
		TYA
		CMP !dir,x : BNE .TopSpike
		.FrontSpike
		REP #$10
		LDY.w #DATA_FrontSpike : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		JSL !HurtMario
		..nocontact
		BRA .Jaw

		.TopSpike
		REP #$10
		LDY.w #DATA_TopSpike : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		JSL !HurtMario
		..nocontact

		.Jaw
		REP #$10
		LDY.w #DATA_Jaw : JSR HITBOX
		STZ $0F
		LDA !jawoffset,x
		BPL $02 : DEC $0F
		CLC : ADC $05
		STA $05
		LDA $0B
		ADC $0F
		STA $0B
		JSL !CheckContact : BCC ..nocontact
		JSL !HurtMario
		..nocontact

		.Pilot
		REP #$10
		LDY.w #DATA_Pilot : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		LDY #$03
		..loop
		LDA $17C0|!addr,y : BEQ ..thisone
		DEY : BPL ..loop
		LDY #$00
		..thisone
		LDA #$02 : STA $17C0|!addr,y
		LDA #$08 : STA $17CC|!addr,y
		LDA !mariox : STA $17C8|!addr,y
		LDA !marioy
		CLC : ADC #$18
		STA $17C4|!addr,y
		LDA #$02 : STA !SPC1
		LDA #$C8
		BIT $15
		BPL $02 : LDA #$A0
		STA !marioyspeed
		LDA #$08 : STA !nobouncetimer,x
		LDA !iframes,x : BNE ..nocontact
		JSR HurtBoss
		LDA #$04 : STA !attack,x
		INC !attacktimer,x
		..nocontact


	; optimization: only check 1 fireball per frame
		.PlayerFireball
		LDA $14
		AND #$03
		CMP #$02 : BCS ..nocontact
		ORA #$08
		TAY
		LDA $170B|!addr,y
		CMP #$05 : BNE ..nocontact
		LDA $171F|!addr,y : STA $00		; xlo
		LDA $1733|!addr,y : STA $08		; xhi
		LDA $1715|!addr,y : STA $01		; ylo
		LDA $1729|!addr,y : STA $09		; yhi
		LDA #$08
		STA $02
		STA $03
		JSL !CheckContact : BCC ..nocontact
		LDA $14
		AND #$01
		ORA #$08
		TAY
		LDA #$01 : STA $170B|!addr,y
		LDA #$0F : STA $176F|!addr,y
		LDA #$01 : STA !SPC1
		LDA !iframes,x : BNE ..nocontact
		LDA #$40 : STA !iframes,x
		DEC !hp,x
		JSR HurtBoss_CheckHP
		..nocontact


	; optimization: only check bob-omb explosion every other frame
		.Bobomb
		LDA !iframes,x : BNE ..nocontact
		LDA $14
		AND #$02 : BCC ..nocontact
		LDX.b #!SprSize-1
		..loop
		STX $0C
		LDA $14
		EOR $0C
		AND #$01 : BNE ..next
		LDA !1564,x : BNE ..next
		LDA !extra_bits,x
		AND #$08 : BNE ..next
		LDA !sprite_status,x
		CMP #$08 : BCC ..next
		CMP #$0B : BEQ ..next
		LDA !sprite_num,x
		CMP #$0D : BNE ..next
		JSR GetBombHitbox
		JSL !CheckContact : BCC ..next
		LDA !1534,x : BNE ..damage
		JSR ExplodeBomb
		BRA ..next
		..damage
		LDX !spriteindex
		JSR HurtBoss
		LDA #$21 : STA !hurttimer,x
		LDA #$04 : STA !attack,x
		INC !attacktimer,x
		BRA ..nocontact
		..next
		DEX : BPL ..loop
		LDX !spriteindex
		..nocontact


		.Return
		RTS




	TransitionToPhase3:
		LDX !spriteindex
		LDA !phase,x : BPL .Init
		JMP .Main

		.Init
		ORA #$80 : STA !phase,x
		JSR DropFlower
		LDA #$07 : STA !SPC1			; collapse sfx

		LDX.b #!SprSize-1			;\
	-	LDA !sprite_status,x : BEQ +		; |
		LDA !extra_bits,x			; |
		AND #$08 : BNE +			; |
		LDA !sprite_num,x			; |
		CMP #$0D : BNE +			; | kill all bobombs
		LDA !sprite_misc_1534,x			; |
		CMP #$01 : BEQ +			; |
		LDA #$02 : STA !sprite_status,x		; |
	+	DEX : BPL -				; |
		LDX !spriteindex			;/

		; drop mech
		STZ $00
		STZ $01
		LDY !dir,x
		LDA .XSpeed,y : STA $02
		LDA #$F0 : STA $03
		SEC : LDA.b #!BossNum
		%SpawnSprite()
		BCS ..fail
		LDA !dir,x : STA !dir,y
		TYX
		LDA #$0B : STA !extra_prop_1,x
		LDA #$08 : STA !sprite_status,x
		LDX !spriteindex
		..fail

		STZ !iframes,x
		LDY !dir,x
		LDA .XSpeed,y : STA !xspeed,x
		LDA #$E0 : STA !yspeed,x
		LDA !ylo,x
		SEC : SBC #$2E
		STA !ylo,x
		LDA !yhi,x
		SBC #$00
		STA !yhi,x

		%anim(jump)


		.Main
		JSL !ApplySpeed

		LDA !xhi,x : XBA
		LDA !xlo,x
		REP #$20
		CMP $1A
		SEP #$20 : BCS ..onscreen
		LDA $1A : STA !xlo,x
		LDA $1B : STA !xhi,x
		..onscreen

		LDA !blocked,x
		AND #$04 : BNE ..ground
		..air
		LDA #$C0 : STA !attacktimer,x
		RTS
		..ground
		%SubHorzPos()
		TYA : STA !dir,x
		STZ !xspeed,x
		%checkanim(idle2) : BEQ ..animdone
		%anim(throw) : BEQ ..noupdate
		LDA #$20 : STA !animtimer,x
		LDA #$18 : STA !SPC4			; crash sfx
		LDA #$0F : STA !shakescreen
		..noupdate
		LDA !AnimationFrameIndex,x : BEQ ..shake
		CMP #$01 : BNE ..animdone
		LDA !animtimer,x : BNE ..animdone
		INC !AnimationFrameIndex,x
		INC !FrameIndex,x
		LDA #$30 : STA !animtimer,x
		BRA ..animdone
		..shake
		STZ $00
		LDA $14
		AND #$02
		DEC A
		BPL $02 : DEC $00
		CLC : ADC !xlo,x
		STA !xlo,x
		LDA $00
		ADC !xhi,x
		STA !xhi,x
		..animdone
		LDA !attacktimer,x : BEQ ..end
		CMP #$30 : BNE ..return
		%anim(idle2)
		BRA ..return

		..end
		LDA !phase,x
		AND #$7F
		INC A : STA !phase,x
		LDA #$01 : STA !msgtrigger


		..return

		; TODO: figure out how he falls through the floor and remove this bandaid tier solution
		; he falls through the floor if phase 1 has been run
		; so... something wacky tobacky must be going on there
		LDA !yhi,x : BEQ +
		LDA #$70
		CMP !ylo,x : BCS +
		STA !ylo,x
		+

		RTS


		.XSpeed
		db $F0,$10

		.XSpeedJaw
		db $20,$E0


	P3_ATTACK:
		LDX !spriteindex
		LDA !phase,x : BMI .Main
		.Init
		ORA #$80 : STA !phase,x
		LDA.b #!phase3hp : STA !hp,x
		STZ !countercounter,x
		LDA #$01 : STA !attack,x
		LDA #$3C+1 : STA !attacktimer,x
		RTS

		.Main

	; revenge smoke
		JSR RevengeSmoke

	; update attack
		.UpdateAttack
		LDA !attack,x						;\
		AND #$7F : BNE ..norock					; |
		JSR .CheckRock : BCC ..norock				; | cancel bum rush into rock throw
		..rock							; |
		LDA #$02 : BRA ..thisone				; |
		..norock						;/
		LDA !attacktimer,x : BNE ..done				; check timer
		JSR .CheckRock : BCS ..rock				; check rock distance
		LDA !attack,x						;\
		AND #$7F						; | don't move unrollable attacks into attack mem
		CMP.b #(.Ptr_rollthreshold-.Ptr)/2 : BCS ..rollattack	;/
		LDA !prevattack1,x : STA !prevattack2,x			;\
		LDA !attack,x						; | rotate attacks into memory for ai
		AND #$7F : STA !prevattack1,x				;/
		..rollattack						;\
		LDA !RNG						; |
		AND #$01						; |
		CMP !prevattack1,x : BNE ..thisone			; | roll a random attack, but it can't be the same one more than twice in a row
		CMP !prevattack2,x : BNE ..thisone			; |
		INC A							; |
	-	CMP.b #(.Ptr_rollthreshold-.Ptr)/2 : BCC ..thisone	; |
		SBC.b #(.Ptr_rollthreshold-.Ptr)/2 : BRA -		;/
		..thisone						;\
		CMP.b #(.Ptr_end-.Ptr)/2				; |
		BCC $02 : LDA #$00					; | store attack + timer (1 second)
		STA !attack,x						; |
		LDA #$3C : STA !attacktimer,x				;/
		..done

		PEA.w P3_PHYSICS-1					;\
		LDA !attack,x						; | push address for physics, then go to attack code
		ASL A							; |
		TAX							; |
		JMP (.Ptr,x)						;/



		.Ptr
		dw BumRush		; 00
		dw Pounce		; 01
		..rollthreshold
		dw ThrowRock		; 02
		dw CounterPunch		; 03
		dw Stunned		; 04
		..end



	.CheckRock
		LDA !x_hi,x : XBA					;\
		LDA !x_lo,x						; |
		REP #$20						; |
		SEC : SBC !mariox					; | check distance to mario
		BPL $03 : EOR #$FFFF					; |
		CMP #$0070						; |
		SEP #$20						; |
		RTS							;/


	BumRush:
		LDX !spriteindex			;\
		LDA !attack,x : BMI .Main		; |
		.Init					; |
		LDA !marioyspeed : BPL ..process	; > have init wait for mario to be moving down
	-	%anim(idle2)				; |
		JMP Friction				; |
		..process				; | init code
		LDA #$4C : STA !attacktimer,x		; |
		LDA !attack,x				; |
		ORA #$80 : STA !attack,x		; |
		.Main					;/
		LDA !attacktimer,x
		CMP #$3C : BCS -
		%checkanim(claw) : BEQ +
		%anim(walk2)				;
	+	LDA !blocked,x
		AND #$04 : BEQ .Air

		.Ground
		LDA !xspeed,x				;\
		ASL A					; |
		ROL A					; |
		AND #$01				; |
		TAY					; |
		LDA !xlo,x				; | end attack if running into screen border
		SEC : SBC $1A				; |
		CMP .XThreshold,y : BNE ..nolimit	; |
		STZ !attacktimer,x			; |
		RTS					; |
		..nolimit				;/

		%checkanim(claw) : BEQ +
		%SubHorzPos()				;\ face mario
		TYA : STA !dir,x			;/
		+

		LDY !dir,x				;\
		%checkanim(claw)
		BNE $02 : INY #2
		LDA .XSpeed,y				; |
		SEC : SBC !xspeed,x			; | snap to target speed when close enough to it
		INC #2					; |
		CMP #$04 : BCC ..targetspeed		;/
		LDA .XSpeed,y : BMI ..left		; check target speed dir
		..right					;\
		BIT !xspeed,x : BMI ..inc		; |
		CMP !xspeed,x : BEQ .Shared		; |
		BCC ..dec				; |
		..inc					; | accelerate right
		INC !xspeed,x				; |
		INC !xspeed,x				; |
		INC !xspeed,x				; |
		BRA .Shared				;/
		..left					;\
		BIT !xspeed,x : BPL ..dec		; |
		CMP !xspeed,x : BEQ .Shared		; |
		BCS ..inc				; |
		..dec					; | accelerate left
		DEC !xspeed,x				; |
		DEC !xspeed,x				; |
		DEC !xspeed,x				; |
		BRA .Shared				;/
		..targetspeed				;\
		LDA .XSpeed,y : STA !xspeed,x		; | snap to target speed
		BRA .Shared				;/

		.Air
		STZ !xspeed,x

		.Shared
		LDA !marioanim : BNE .NoContact


		JSL !GetMarioClipping
		.Sightbox
		REP #$10
		LDY.w #DATA_MeleeSightbox : JSR HITBOX
		JSL !CheckContact : BCC ..done
		LDA !animindex,x
		CMP.b #!anim_claw : BEQ ..noanimupdate
		LDA #$25 : STA !SPC1				; roar sfx
		LDA !animtimer,x : PHA
		LDA !AnimationFrameIndex,x : PHA
		LDA.b #!anim_claw : JSR UpdateAnim
		PLA : STA !AnimationFrameIndex,x
		PLA : STA !animtimer,x
		RTS
		..noanimupdate
		INC !attacktimer,x
		..done

		.Attackbox
		%checkanim(claw) : BNE .NoContact
		LDA !AnimationFrameIndex,x
		AND #$01 : BEQ .NoContact
		REP #$10
		LDY.w #DATA_AttackHitbox : JSR HITBOX
		JSL !CheckContact : BCC .NoContact
		JSL !HurtMario

		.NoContact
		RTS


		.XSpeed
		db $12,$EE
		db $22,$DE

		.XThreshold
		db $F0,$00



	Pounce:
		LDX !spriteindex			;\
		LDA !attack,x : BMI .Main		; |
		.Init					; |
		ORA #$80 : STA !attack,x		; | init code
		LDA.b #!anim_throw : JSR UpdateAnim	; |
		LDA #$40 : STA !animtimer,x		; |
		%SubHorzPos()
		TYA : STA !dir,x
		.Main					;/

		LDA !blocked,x
		AND #$04 : BNE ..checkjump
		LDA #$02 : STA !attacktimer,x
		RTS

		..checkjump
		STZ $00					;\
		LDA $14					; |
		AND #$02				; |
		DEC A					; |
		BPL $02 : DEC $00			; | shake
		CLC : ADC !xlo,x			; |
		STA !xlo,x				; |
		LDA $00					; |
		ADC !xhi,x				; |
		STA !xhi,x				;/
		LDA !attacktimer,x			;\ wait for jump timer
		CMP #$02 : BNE ..nojump			;/

		%SubHorzPos()
		TYA : STA !dir,x
		LDA !xhi,x : XBA			;\
		LDA !xlo,x				; |
		REP #$20				; |
		SEC : SBC !mariox			; |
		BPL $04 : EOR #$FFFF : INC A		; |
		LDY #$00				; |
		CMP #$0020 : BCC + : INY		; |
		CMP #$0030 : BCC + : INY		; | get x speed adjustment index
		CMP #$0040 : BCC + : INY		; |
		CMP #$0050 : BCC + : INY		; |
		CMP #$0060 : BCC + : INY		; |
		CMP #$0070 : BCC + : INY		; |
		CMP #$0080 : BCC + : INY		; |
		CMP #$0090 : BCC + : INY		; |
	+	SEP #$20				;/
		LDA .YBoost,y : STA $00			; get y speed mod
		LDA .XBoost,y				;\
		LDY !dir,x				; |
		EOR .XFlip,y				; | calculate x speed
		CLC : ADC .XSpeed,y			; |
		STA !xspeed,x				;/
		LDA $00					;\
		CLC : ADC #$C0				; | set y speed
		STA !yspeed,x				;/
		%anim(jump2)				; set anim
		LDA #$25 : STA !SPC1				; roar sfx

		BRA .JumpDust

		..nojump
		BCC ..landing
		BRA ..friction

		..landing
		%anim(idle2)
		LDA !xspeed,x
		ASL A
		ROR !xspeed,x

		..friction
		JSR Friction

		..jumpdone

		.Return
		RTS


		.JumpDust
		LDA #$10 : STA $00
		LDA #$08 : STA $01
		LDA #$10 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS ..return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		LDA #$F0 : STA $00
		LDA #$08 : STA $01
		LDA #$F0 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS ..return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		..return
		RTS


		.XSpeed
		db $20,$E0

		.XFlip
		db $00,$FF

		.XBoost
		db $00,$04,$08,$0C,$10,$14,$18,$1C,$20

		.YBoost
		db $30,$28,$20,$18,$10,$0C,$08,$04,$00


	ThrowRock:
		LDX !spriteindex			;\
		LDA !attack,x : BMI .Main		; |
		.Init					; | init code
		ORA #$80 : STA !attack,x		; |
		LDA.b #!anim_throw : JSR UpdateAnim	; |
		.Main					;/


		%SubHorzPos()
		TYA : STA !dir,x
		JSR Friction


		LDA !attacktimer,x
		CMP #$34 : BNE .NoThrow

		.Throw
		LDY !dir,x
		LDA .XDisp,y : STA $00
		STZ $01
		STZ $02
		LDA #$C0 : STA $03
		SEC : LDA #!ProjectileNum
		%SpawnSprite()
		BCS .NoThrow
		LDA #$21 : STA !attacktimer,y
		LDA !dir,x : STA !dir,y
		.NoThrow

		RTS

		.XDisp
		db $06,$FA



	CounterPunch:
		LDX !spriteindex
		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		STZ !iframes,x
		LDA #$20 : STA !attacktimer,x
		%anim(claw2)
		STZ !countercounter,x				; clear revenge count
		LDA #$40 : STA !dontinteract,x
		LDA !phase,x
		AND #$7F
		CMP #$04 : BCC +
		%SubHorzPos()
		TYA : STA !dir,x
	+	LDA #$25 : STA !SPC1				; roar sfx
		.Main

		JSR Friction
		LDA !AnimationFrameIndex,x
		CMP #$01 : BNE .NoContact

		REP #$10
		LDA !phase,x
		AND #$7F
		CMP #$05 : BEQ ..3
		..2
		LDY.w #DATA_CounterHitbox1 : BRA +
		..3
		LDY.w #DATA_CounterHitbox3
	+	JSR HITBOX
		JSL !GetMarioClipping
		JSL !CheckContact : BCC .NoContact
		JSL !HurtMario
		LDY !dir,x
		LDA .Pushback,y : STA !marioxspeed
		LDA #$09 : STA !SPC4				; boom sfx

		.NoContact
		RTS

		.Pushback
		db $30,$D0




	Stunned:
		LDX !spriteindex
		LDA !attack,x : BMI .Main
		.Init
		ORA #$80 : STA !attack,x
		LDA #$20 : STA !attacktimer,x
		STZ !yspeed,x
		%anim(hurt)
		.Main
		LDA !blocked,x
		AND #$04 : BEQ .Return
		JSR Friction
		.Return
		RTS





	P3_PHYSICS:
		JSL !ApplySpeed				; update position based on speed

		.ScreenBorder				;\
		LDA !xhi,x : XBA			; |
		LDA !xlo,x				; |
		REP #$20				; | check on-screen position
		SEC : SBC $1A				; |
		BMI ..left				; |
		CMP #$00F1 : BCC ..done			;/
		..right					;\
		LDA $1A					; | limit right
		CLC : ADC #$00F0			; |
		BRA ..updatex				;/
		..left					;\ limit left
		LDA $1A					;/
		..updatex				;\
		SEP #$20				; |
		STA !xlo,x				; |
		XBA : STA !xhi,x			; | update xpos and xspeed
		STZ !xspeed,x				; |
		..done					; |
		SEP #$20				;/


		; TODO: figure out how he falls through the floor and remove this bandaid tier solution
		; he falls through the floor if phase 1 has been run
		; so... something wacky tobacky must be going on there
		LDA !yhi,x : BEQ +
		LDA #$70
		CMP !ylo,x : BCS +
		STA !ylo,x
		+

		LDA !RNG
		AND #$03
		CMP #$03
		BNE $02 : LDA #$00
		DEC A
		STA $00

		LDY #$03				;\
	-	LDA $17C0|!addr,y			; |
		CMP #$01 : BNE +			; | smoke puffs move up
		LDA $17C4|!addr,y			; |
		DEC A : STA $17C4|!addr,y		; |
		LDA $17C4|!addr,y			; |
		CLC : ADC $00				; > random x movement
		STA $17C4|!addr,y			; |
	+	DEY : BPL -				;/




		LDA !blocked,x
		AND #$04 : BEQ .Air

		.Ground
		LDA #$10 : STA !yspeed,x

		.Air


		.Done



	P3_INTERACTION:

	; optimization: only check 1 fireball per frame
		.PlayerFireball
		LDA $14
		AND #$03
		CMP #$02 : BCS ..nocontact
		ORA #$08
		TAY
		LDA $170B|!addr,y
		CMP #$05 : BNE ..nocontact
		LDA $171F|!addr,y : STA $00		; xlo
		LDA $1733|!addr,y : STA $08		; xhi
		LDA $1715|!addr,y : STA $01		; ylo
		LDA $1729|!addr,y : STA $09		; yhi
		LDA #$08
		STA $02
		STA $03
		REP #$10
		LDY.w #DATA_BodyHitboxFull : JSR HITBOX
		JSL !CheckContact : BCC ..nocontact
		LDA $14
		AND #$01
		ORA #$08
		TAY
		LDA #$01 : STA $170B|!addr,y
		LDA #$0F : STA $176F|!addr,y
		LDA #$01 : STA !SPC1
		LDA !iframes,x : BNE ..nocontact
		LDA #$40 : STA !iframes,x
		DEC !hp,x
		JSR HurtBoss_CheckHP
		..nocontact

	; get mario hitbox
		JSL !GetMarioClipping


	; handle revenge attack
		.CounterCounter
		LDA $77
		AND #$04 : BEQ ..noreset
		STZ !countercounter,x
		..noreset
		LDA !marioyspeed : BMI ..done
		LDA !countercounter,x
		CMP #$02 : BCC ..done
		REP #$10
		LDY.w #DATA_CounterSightbox3 : JSR HITBOX
		JSL !CheckContact : BCC ..done
		LDA #$03 : STA !attack,x
		INC !attacktimer,x
		BRA .NoContact
		..done

		LDA !dontinteract,x : BNE .NoContact
		LDA !marioanim : BNE .NoContact
		LDA !nobouncetimer,x : BNE .NoContact
		REP #$10
		LDY.w #DATA_BodyHitbox : JSR HITBOX
		JSL !CheckContact : BCC .NoContact

		LDA $0B : XBA
		LDA $05
		REP #$20
		SEC : SBC #$0010
		CMP !marioy
		SEP #$20
		BCS .Bounce
		JSL !HurtMario
		BRA .NoContact


	; bounce code here
		.Bounce
		JSR ContactGFX
		LDA #$02 : STA !SPC1
		LDA #$C8
		BIT $15
		BPL $02 : LDA #$A0
		STA !marioyspeed
		LDA #$08 : STA !nobouncetimer,x
		LDA !iframes,x : BNE .NoContact
		LDA #$04 : STA !attack,x
		JSR HurtBoss

		.NoContact

		RTS









	; optimized
	HANDLE_ANIMATION:
		LDA !AnimationTimer,x : BEQ .NextAnim
		.SameAnim
		DEC !AnimationTimer,x
		BRA .Done
		.NextAnim
		REP #$30
		LDA !AnimationIndex,x
		AND #$00FF
		ASL A : TAY
		INC !AnimationFrameIndex,x
		LDA !AnimationFrameIndex,x
		AND #$00FF
		CMP Animation_Length,y : BCC ..keepindex
		..updateindex
		SEP #$20
		LDA Animation_LastTransition,y : STA !AnimationFrameIndex,x
		REP #$20
		..keepindex
		CLC : ADC Animation_Index,y
		TAY
		SEP #$20
		LDA Animation_Data,y : STA !FrameIndex,x
		LDA Animation_Time,y : STA !AnimationTimer,x
		SEP #$10


		; STZ $01
		; LDA !AnimationIndex,x
		; STA $00				;$00 = Animation index in 16 bits
		; STZ $03
		; LDA !AnimationFrameIndex,x
		; STA $02				;$02 = Animation Frame index in 16 bits
		; STZ $05
		; STX $04				;$04 = sprite index in 16 bits
		; REP #$30				;A7X/Y of 16 bits
		; LDX $04				;X = sprite index in 16 bits
		; LDA $00
		; ASL
		; TAY					;Y = 2*Animation index
		; INC $02				;New Animation Frame Index = Animation Frame Index + 1
		; LDA $02				;if Animation Frame index < Animation Length then Animation Frame index++
		; CMP AnimationLength,y			;else go to the frame where start the loop.
		; BCC +							
		; LDA AnimationLastTransition,y
		; STA $02				;New Animation Frame Index = first frame of the loop.
	; +	LDA $02
		; CLC
		; ADC AnimationIndexer,y
		; TAY					;Y = Position of the first frame of the animation + animation frame index
		; SEP #$20				;A of 8 bits
		; LDA Frames,y
		; STA !FrameIndex,x			;New Frame = Frames[New Animation Frame Index]
		; LDA Times,y
		; STA !AnimationTimer,x			;Time = Times[New Animation Frame Index]
		; LDA $02
		; STA !AnimationFrameIndex,x
		; SEP #$10				;X/Y of 8 bits
		; LDX $04				;X = sprite index in 8 bits


		.Done


; scratch RAM use for tilemap loader
;
; $00 sprite x
; $02 sprite y
; $04 y cull threshold (used for removing legs in pilot mode)
; $06 xflip flag
; $08 length value
; $0A end position value
; $0C OAM index, later holds tile X
; $0E tile remap value

	GRAPHICS:
		STZ $00
		LDA !phase,x
		AND #$7F : BNE ++
		LDA !hp,x : BEQ +
	++	CMP #$04 : BCS +
		JSR DRAWTANK
		%checkanim(idle) : BNE +
		PLB
		RTL
		+


		.Cull
		LDA !phase,x
		AND #$7F : BNE ..notintro
		LDA !hp,x : BEQ ..0000
		BRA ..FFF8
		..notintro
		CMP #$01 : BEQ ..FFF8
		CMP #$04 : BCS ..0000
		..FFFF
		LDA #$FF
		STA $04
		STA $05
		BRA ..done
		..FFF8
		LDA #$F8 : STA $04
		LDA #$FF : STA $05
		BRA ..done
		..0000
		STZ $05
		..done


		LDA !ylo,x : PHA
		SEC : SBC $00
		STA !ylo,x
		LDA !yhi,x : PHA
		SBC #$00
		STA !yhi,x

		.Blink
		%checkanim(claw2) : BEQ ..done
		LDA !iframes,x : BEQ ..done
		CMP #$20 : BCC ..rapid
		..slow
		AND #$06 : BNE ..done
		BRA .Return
		..rapid
		AND #$02 : BNE .Return
		..done

		.Draw
		LDA !x_lo,x : STA $00
		LDA !x_hi,x : STA $01
		LDA !y_hi,x : XBA
		LDA !y_lo,x
		REP #$20
		SEC : SBC $1C
		STA $02
		LDA $00
		SEC : SBC $1A
		STA $00
		SEP #$20

		JSR .LoadTilemap


		.Return
		PLA : STA !yhi,x
		PLA : STA !ylo,x
		PLB
		RTL




	; optimized
	.LoadTilemap
		%GetVramDisp(DZ_DS_Loc_US_Normal) : STA $0E		; tile remap value (probably a pointer?)

		REP #$30						;\
		LDA !sprite_oam_index,x					; | $0C = OAM index
		AND #$00FF : STA $0C					;/

		LDA !dir,x						;\
		AND #$0001						; | $06 = xflip flag (0x40)
		BEQ $03 : LDA #$0040					; |
		; EOR #$0040						; > the GFX already face right
		STA $06							;/

		LDA !LastFrameIndex,x					;\
		AND #$00FF						; |
		ASL A : TAX						; | get length
		LDA FramesLength,x					; |
		CMP #$FFFF : BNE .Proceed				;/
		SEP #$30						;\
		LDX !spriteindex					; | return if length is invalid
		RTS							;/

		.Proceed
		STA $08							; $08 = length value
		LDA FramesEndPosition,x : STA $0A			; $0A = end position value
		LDA FramesStartPosition,x : TAX				; X = start position value
		LDY $0C							; Y = OAM index
		JMP .Loop_end						; go into loop

		.Loop
		SEP #$20						; A 8-bit
		LDA #$2F						;\
		EOR $06							; | get prop
		STA !OAM+$103,y						;/
		LDA Sizes,x : BEQ ..8x8					;\
		..16x16							; |
		LDA #$80						; | $0C = size (n-flag trigger)
		..8x8							; |
		STA $0C							;/

		REP #$20						;\
		LDA XDisplacements,x					; |
		AND #$00FF						; |
		CMP #$0080						; |
		BCC $03 : ORA #$FF00					; |
		BIT $06-1 : BVC ..noflip				; > invert x offset if sprite is flipped
		EOR #$FFFF : INC A					; |
		BIT $0C-1 : BMI ..noflip				; > add 8 to x offset if an 8x8 tile is flipped
		CLC : ADC #$0008					; |
		..noflip						; |
		CLC : ADC $00						; | $0C = tile x position
		CMP #$0100 : BCC ..goodx				; |
		CMP #$FFF0 : BCC ..next					; |
		..goodx							; |
		STA $0C							;/
		LDA YDisplacements,x					;\
		AND #$00FF						; |
		CMP #$0080						; |
		BCC $03 : ORA #$FF00					; | get y position
		..checkcull						; |
		BIT $04 : BPL ..nocull					; |
		BIT #$8000 : BEQ ..next					; |
		CMP $04 : BCS ..next					; > cull
		..nocull						; |
		CLC : ADC $02						; |
		CMP #$00E0 : BCC ..goody				; |
		CMP #$FFF0 : BCC ..next					; |
		..goody							;/
		STA !OAM+$101,y						;\
		SEP #$20						; | store OAM coords
		LDA $0C : STA !OAM+$100,y				;/
		%RemapOamTile("Tiles,x", $0E) : STA !OAM+$102,y		; store tile num
		REP #$20						;\
		TYA							; |
		LSR #2							; |
		TAY							; |
		SEP #$20						; | write x pos hi + size bit
		LDA $0D							; |
		AND #$01						; |
		ORA Sizes,x						; |
		STA !OAMhi+$40,y					;/
		REP #$20						;\
		TYA							; |
		INC A							; | OAM index +4
		ASL #2							; |
		TAY							;/
		..next							;\
		DEX : BMI ..return					; | return if all tiles have been written
		CPX $0A : BCC ..return					;/
		..end							;\ return if OAM is full
		CPY #$00FC+1 : BCS ..return				;/
		JMP .Loop						; > loop
		..return						;\
		SEP #$30						; | return
		LDX !spriteindex					; |
		RTS							;/









		; %GetDrawInfo()			;Calls GetDrawInfo to get the free slot and the XDisp and YDisp
		; %GetVramDisp(DZ_DS_Loc_US_Normal)
		; STA $0E
		; STZ $03				;$02 = Free Slot but in 16bits
		; STY $02
		; STZ $05
		; LDA !GlobalFlip,x   
		; STA $0F
		; ASL
		; STA $04				;$04 = Global Flip but in 16bits
		; LDA $0F
		; CLC
		; ROR A
		; ROR A 
		; ROR A
		; STA $0F
		; PHX					;Preserve X
		; STZ $07
		; LDA !LastFrameIndex,x
		; STA $06				;$06 = Frame Index but in 16bits
		; REP #$30				;A/X/Y 16bits mode
		; LDY $04				;Y = Global Flip
		; LDA $06
		; ASL
		; CLC
		; ADC FramesFlippers,y
		; TAX					;X = Frame Index
		; LDA FramesLength,x
		; CMP #$FFFF
		; BNE +
		; SEP #$30
		; PLX
		; RTS
		; +
		; STA $08
		; LDA FramesEndPosition,x
		; STA $04				;$04 = End Position + A value used to select a frame version that is flipped
		; LDA FramesStartPosition,x   
		; TAX					;X = Start Position
		; SEP #$20				;A 8bits mode
		; LDY $02				;Y = Free Slot
		; CPY #$00FD
		; BCS .return				;Y can't be more than #$00FD
		; -
		; %RemapOamTile("Tiles,x", $0E)
		; STA !TileCode,y			;Set the Tile code of the tile Y
		; LDA #$2D
		; EOR $0F
		; STA !TileProperty,y			;Set the Tile property of the tile Y
		; LDA $00
		; CLC
		; ADC XDisplacements,x
		; STA !TileXPosition,y			;Set the Tile x pos of the tile Y
		; LDA $01
		; CLC
		; ADC YDisplacements,x
		; STA !TileYPosition,y			;Set the Tile y pos of the tile Y
		; PHY
		; REP #$20 
		; TYA
		; LSR
		; LSR
		; TAY					;Y = Y/4 because size directions are not continuous to map 200 and 300
		; SEP #$20
		; LDA Sizes,x
		; STA !TileSize460,y			;Set the Tile size of the tile Y
		; PLY
		; INY
		; INY
		; INY
		; INY					;Next OAM Slot
		; CPY #$00FD
		; BCS .return 				;Y can't be more than #$00FD
		; DEX
		; BMI .return
		; CPX $04 				;if X < start position or is negative then return
		; BCS -   				;else loop
		; .return
		; SEP #$10
		; PLX					;Restore X
		; LDY #$FF				;Allows mode of 8 or 16 bits
		; LDA $08   				;Load the number of tiles used by the frame
		; JSL $01B7B3|!rom  			;This insert the new tiles into the oam, 
							; ;A = #$00 => only tiles of 8x8, A = #$02 = only tiles of 16x16, A = #$04 = tiles of 8x8 or 16x16
							; ;if you select A = #$04 then you must put the sizes of the tiles in !TileSize
		; RTS






; input:
;	void
; output:
;	$00 = y position displacement for main body

; scratch RAM use for tilemap loader
;
; $00 - sprite X pos (16-bit)
; $02 - sprite Y pos (16-bit)
; $04 - tilemap pointer
; $06 - xflip flag
; $08 - number of bytes to read from tilemap
; $0A - tilemap Y offset
; $0C - tile X position
; $0E - tile Y position


	DRAWTANK:
		LDA !dir,x
		BEQ $02 : LDA #$40
		; EOR #$40						; > the GFX already face right
		STA $06
		STZ $0A
		STZ $0C
		LDA !sprite_oam_index,x : STA $0E			;\ OAM index
		LDA #$01 : STA $0F					;/

		LDA !phase,x
		AND #$7F
		CMP #$02 : BCC .Phase1

		.Phase2
		REP #$20						;\ draw shoulder pad
		LDA Tank_Ptr+18 : JSR .Draw				;/
		LDA !jawoffset,x : STA $0C				;\
		REP #$20						; | draw jaw
		LDA Tank_Ptr+10 : JSR .Draw				;/
		JSR .Pilot						; draw pilot + tank
		LDA #$2E : STA $00
		RTS


		.Phase1
		STZ !legoffset,x
		LDA !xlo,x
		AND #$3F : TAY
		LDA Tank_Foot1OffsetY,y
		CMP Tank_Foot2OffsetY,y
		BPL $03 : LDA Tank_Foot2OffsetY,y
		EOR #$FF : INC A
		STA !legoffset,x


		.ShoulderPad
		PHY
		LDA !extra_byte_2,x : STA $0C
		REP #$20
		LDA Tank_Ptr+18 : JSR .Draw
		PLY

		.Foot1
		LDA Tank_Foot1OffsetX,y : STA $0A
		PHA
		LDA Tank_Foot1OffsetY,y : STA $0C
		PHA
		LDA !xlo,x
		AND #$3F : TAY
		REP #$20
		LDA Tank_Ptr+12 : JSR .Draw
		..knee
		LDA $01,s
		CLC
		BPL $01 : SEC
		ROR A
		CLC : ADC !extra_byte_2,x
		STA $0C
		LDA $02,s
		CLC
		BPL $01 : SEC
		ROR A
		STA $0A
		REP #$20
		LDA Tank_Ptr+14 : JSR .Draw
		..hip
		PLA
		CLC
		BPL $01 : SEC
		ROR A
		CLC
		BPL $01 : SEC
		ROR A
		CLC : ADC !extra_byte_2,x
		STA $0C
		PLA
		CLC
		BPL $01 : SEC
		ROR A
		CLC
		BPL $01 : SEC
		ROR A
		STA $0A
		REP #$20
		LDA Tank_Ptr+16 : JSR .Draw
		..done


		.Jaw
		LDA !xlo,x
		AND #$3F : TAY
		LDA Tank_Foot1OffsetY,y
		EOR #$FF : INC A
		SEC : SBC #$12
		CLC
		BPL $01 : SEC
		ROR A
		CLC : ADC !extra_byte_2,x
		STA $0C
		REP #$20
		LDA Tank_Ptr+10 : JSR .Draw
		..done


		.Glass
		LDA !glasshp,x : BEQ ..done
		LDA !extra_byte_2,x : STA $0C
		REP #$20
		LDA Tank_Ptr+6 : JSR .Draw
		..done

		.Foot2
		LDA !xlo,x
		AND #$3F : TAY
		; hip not visible
		..knee
		LDA Tank_Foot2OffsetX,y
		CLC : ADC #$10
		PHA
		CLC
		BPL $01 : SEC
		ROR A
		STA $0A
		LDA Tank_Foot2OffsetY,y : PHA
		CLC
		BPL $01 : SEC
		ROR A
		STA $0C
		LDA !extra_byte_2,x					;\ skip knee 2 when it's invisible to mitigate sprite limit
		CMP #$0C : BEQ ..foot					;/
		REP #$20
		LDA Tank_Ptr+24 : JSR .Draw
		..foot
		PLA : STA $0C
		PLA : STA $0A
		REP #$20
		LDA Tank_Ptr+22 : JSR .Draw
		..done

		.Pilot
		LDA !iframes,x : BEQ ..draw
		CMP #$20 : BCC ..rapid
		..slow
		AND #$06 : BNE ..draw
		BRA ..done
		..rapid
		AND #$02 : BNE ..done
		..draw
		%checkanim(idle) : BNE ..done
		LDA $14
		AND #$08
		BEQ $02 : LDA #$02
		TAY
		LDA !extra_byte_2,x : STA $0C
		REP #$20
		LDA Tank_Ptr+2,y : JSR .Draw
		..done

		.Tank
		LDA $0E : STA !sprite_oam_index,x			; update OAM index
		LDA !extra_byte_2,x : STA $0C
		REP #$20
		LDA Tank_Ptr+0 : JSR .Draw
		..done


		LDA #$28
		SEC : SBC !legoffset,x
		SEC : SBC !extra_byte_2,x
		STA $00
		RTS




		.BG2
		SEP #$20
		STZ $0B
		LDA $0A
		BPL $02 : DEC $0B
		STZ $0D

		LDA !phase,x
		AND #$7F
		CMP #$02 : BCC +
		LDA $0C : BRA ++
	+	LDA $0C
		CLC : ADC !legoffset,x
		STA $0C
	++	BPL $02 : DEC $0D

		LDA !x_lo,x
		CLC : ADC $0A
		STA $00
		LDA !x_hi,x
		ADC $0B
		STA $01
		LDA !y_lo,x
		CLC : ADC $0C
		XBA
		LDA !y_hi,x
		ADC $0D
		XBA
		REP #$20
		SEC : SBC $1C
		SEC : SBC #$0068-1
		EOR #$FFFF : INC A
		STA !BG2Y
		LDA $00
		SEC : SBC $1A
		SEC : SBC #$0032
		EOR #$FFFF : INC A
		STA !BG2X
		LDA !dir,x
		AND #$00FF
		BEQ $03 : LDA #$0060
		STA !BG2dir
		SEP #$30
		JMP HandleHDMA_Main



; input:
;	A = pointer to tilemap
;	$06 = xflip flag (0x40)
;	$08 = number of bytes to load from pointer
;	$0A = X offset (8-bit signed)
;	$0C = Y offset (8-bit signed)
; output:
;	$0E = OAM index

		.Draw
		CMP.w #Tank_TM : BEQ .BG2				; tank: just BG2
		STA $04
		CMP.w #Tank_FullJunk : BNE ..proceed			; full junk: BG2, then draw support tiles
		JSR .BG2
		REP #$20

		..proceed
		LDA ($04)
		AND #$00FF : STA $08
		INC $04
		SEP #$30

		STZ $0B
		LDA $0A
		BPL $02 : DEC $0B
		STZ $0D

		LDA !phase,x
		AND #$7F
		CMP #$02 : BCC +
		LDA $0C : BRA ++
	+	LDA $0C
		CLC : ADC !legoffset,x
		STA $0C
	++	BPL $02 : DEC $0D

		LDA !x_lo,x
		CLC : ADC $0A
		STA $00
		LDA !x_hi,x
		ADC $0B
		STA $01
		LDA !y_lo,x
		CLC : ADC $0C
		STA $02
		LDA !y_hi,x
		ADC $0D
		STA $03
		REP #$30
		LDX $0E
		LDY #$0000

	; X = OAM index
	; Y = tilemap table index
	; loop starts here
		.Loop
		REP #$30
		LDA ($04),y
		INY
		AND #$00FF
		CMP #$0080
		BCC $03 : ORA #$FF00
		BIT $06-1
		BVC $04 : EOR #$FFFF : INC A
		CLC : ADC $00
		SEC : SBC $1A
		CMP #$0100 : BCC ..goodx
		CMP #$FFF0 : BCS ..goodx

		..badcoord
		INY #3
		SEP #$20
		JMP .LoopCheck

		..goodx
		STA $0C
		LDA ($04),y
		AND #$00FF
		CMP #$0080
		BCC $03 : ORA #$FF00
		CLC : ADC $02
		SEC : SBC $1C
		CMP #$00E0 : BCC ..goody
		CMP #$FFF0 : BCC ..badcoord
		..goody
		INY
		STA $0E

		.DrawTile
		SEP #$20
		LDA ($04),y : STA !OAM+$002,x
		INY
		LDA ($04),y
		INY
		EOR $06
		STA !OAM+$003,x
		LDA $0C : STA !OAM+$000,x
		LDA $0E : STA !OAM+$001,x
		REP #$20
		TXA
		LSR #2
		TAX
		LDA $0D
		AND #$0001
		ORA #$0002
		STA !OAMhi+$00,x
		INX
		TXA
		ASL #2
		TAX

		.LoopCheck
		CPY $08 : BCS .Return
		JMP .Loop

		.Return
		STZ $0A
		STZ $0C
		STX $0E
		SEP #$30
		LDX !spriteindex
		RTS



;=========================;
; JSR-ABLE ROUTINES BELOW ;
;=========================;



	HandleHDMA:
	.Main
		PHB
		PHP
		REP #$20
		SEP #$10
		LDY.b #!HDMA_table>>16
		PHY : PLB
		REP #$10



		LDY #$0000
		LDA $1FFE
		CMP.w #!HDMA_table
		BNE $03 : LDY.w #!HDMA_table_size
		PHY

		LDA.w !BG2Y : BMI ..displaynormal

		..crop
		CMP #$0060 : BCS ..finishcrop
		CLC : ADC.w !BG2dir
		STA.w !HDMA_table+3,y
		LDA #$0060
		SEC : SBC.w !BG2Y
		BEQ ..finishuncropped
		STA.w !HDMA_table+0,y
		LDA.w !BG2dir
		BEQ $03 : LDA #-$0014
		CLC : ADC.w !BG2X
		STA.w !HDMA_table+1,y
		TYA
		CLC : ADC #$0005
		TAY

		..finishcrop
		LDA #$0001 : STA.w !HDMA_table+0,y
		LDA #$0100 : STA.w !HDMA_table+1,y
		LDA #$0000 : STA.w !HDMA_table+5,y
		BRA ..end

		..displaynormal
		EOR #$FFFF : INC A
		BEQ ..finishuncropped
		CMP #$0080 : BCC ..singlesegment

		..doublesegment
		LSR A
		STA.w !HDMA_table+0,y
		BCC $01 : INC A
		STA.w !HDMA_table+5,y
		LDA #$0100
		STA.w !HDMA_table+1,y
		STA.w !HDMA_table+6,y
		TYA
		CLC : ADC #$000A
		TAY
		BRA ..finishuncropped

		..singlesegment
		STA.w !HDMA_table+0,y
		LDA #$0100 : STA.w !HDMA_table+1,y
		TYA
		CLC : ADC #$0005
		TAY

		..finishuncropped
		LDA #$0060 : STA.w !HDMA_table+0,y
		LDA.w !BG2dir
		BEQ $03 : LDA #-$0014
		CLC : ADC.w !BG2X
		STA.w !HDMA_table+1,y
		LDA.w !BG2Y
		CLC : ADC.w !BG2dir
		STA.w !HDMA_table+3,y
		LDA #$0001 : STA.w !HDMA_table+5,y
		LDA #$0100 : STA.w !HDMA_table+6,y
		LDA #$0000 : STA.w !HDMA_table+10,y

		..end
		PLA
		CLC : ADC.w #!HDMA_table
		STA $1FFE

		PLP
		PLB
		RTS




	ExplodeBomb:
		JSL !ResetSprite
		LDA #$08 : STA !sprite_status,x
		LDA #$01 : STA !1534,x
		LDA #$40 : STA !1540,x
		LDA #$1B : STA !167A,x
		INC !1FD6,x
		LDA #$09 : STA !SPC4			; boom sfx
		RTS


	GetBombHitbox:
		LDA !1534,x
		CMP #$01 : BEQ .Exploding
		.NotExploding
		LDA !xlo,x : STA $00
		LDA !xhi,x : STA $08
		LDA !ylo,x : STA $01
		LDA !yhi,x : STA $09
		LDA #$10
		STA $02
		STA $03
		RTS

		.Exploding
		LDA !xlo,x
		SEC : SBC #$10
		STA $00
		LDA !xhi,x
		SBC #$00
		STA $08
		LDA !ylo,x
		SEC : SBC #$10
		STA $01
		LDA !yhi,x
		SBC #$00
		STA $09
		LDA #$30
		STA $02
		STA $03
		RTS

	DropFlower:
		STZ $00
		LDA #$E0 : STA $01
		LDA #$C0 : STA $03
		CLC : LDA #$75
		%SpawnSprite()
		BCS .Return
		LDA #$00 : STA !flowerspeed
		LDA #$10 : STA !SPC1			; drop mushroom sfx
		.Return
		RTS


	RevengeSmoke:
		LDA !countercounter,x
		CMP #$02 : BCC .Return
		LDA $14
		AND #$03 : BNE .Return
		LDA !RNG
		AND #$0F
		SBC #$08
		STA $00
		LDA #$E8 : STA $01
		LDA !phase,x
		AND #$7F
		CMP #$04 : BCS +
		LDA !legoffset,x
		CLC : ADC !extra_byte_2,x
		SEC : SBC #$40
		CLC : ADC $01
		STA $01
	+	STZ $02
		LDA #$F0 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$02 : STA !sprite_misc_1528,y
		LDA #$0F : STA !sprite_misc_1540,y
		.Return
		RTS


	ExhaustSmoke:
		LDA $14
		AND #$03 : BNE .Return
		LDA #$C8 : STA $00
		LDA #$C4
		CLC : ADC !legoffset,x
		CLC : ADC !extra_byte_2,x
		STA $01
		LDA !RNG
		AND #$0F
		ORA #$F0
		SBC #$04
		STA $02
		LDA #$F0 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$02 : STA !sprite_misc_1528,y
		LDA #$0F : STA !sprite_misc_1540,y
		.Return
		RTS


	Foot1Smoke:
		LDA !xlo,x
		AND #$3F : TAY
		LDA #$18
		SEC : SBC Tank_Foot1OffsetX,y
		STA $00
		LDA Tank_Foot1OffsetY,y
		CLC : ADC #$1A
		STA $01
		LDA #$10 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		LDA !xlo,x
		AND #$3F : TAY
		LDA #$F8
		SEC : SBC Tank_Foot1OffsetX,y
		STA $00
		LDA Tank_Foot1OffsetY,y
		CLC : ADC #$1A
		STA $01
		LDA #$F0 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		.Return
		RTS


	Foot2Smoke:
		LDA !xlo,x
		AND #$3F : TAY
		LDA #$28
		SEC : SBC Tank_Foot2OffsetX,y
		STA $00
		LDA Tank_Foot2OffsetY,y
		CLC : ADC #$1A
		STA $01
		LDA #$10 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		LDA !xlo,x
		AND #$3F : TAY
		LDA #$08
		SEC : SBC Tank_Foot2OffsetX,y
		STA $00
		LDA Tank_Foot2OffsetY,y
		CLC : ADC #$1A
		STA $01
		LDA #$F0 : STA $02
		LDA #$F8 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		BCS .Return
		LDA #$01 : STA !sprite_misc_1528,y
		LDA #$0B : STA !sprite_misc_1540,y
		.Return
		RTS










; secondary hitbox
; $00 - X lo
; $01 - Y lo
; $02 - W
; $03 - H
; $08 - X hi
; $09 - Y hi

; sprite hitbox
; $04 - X lo
; $05 - Y lo
; $06 - W
; $07 - H
; $0A - X hi
; $0B - Y hi


; $03B664 - player clipping -> $00
; $03B69F - sprite clipping X -> $04
; $03B6E5 - sprite clipping X -> $00

; $03B72B - check for contact

	HITBOX:
		LDA !x_lo,x : STA $0C		;\ $0C = 16-bit X position
		LDA !x_hi,x : STA $0D		;/
		LDA !y_hi,x : XBA		;\
		LDA !y_lo,x			; | get Y position
		REP #$20			; |
		CLC : ADC $0002,y		;/
		BMI .OutOfBounds		; if negative, this is out of bounds
		STA $05				; lo byte Y -> $05
		STA $0A				; hi byte Y -> $0B
		LDA $0004,y : STA $06		; W + H -> $06 + $07
		AND #$00FF : STA $0E		; $0E = 16-bit W
		LDA !dir,x			;\ different codes for left/right facing
		AND #$00FF : BNE .Left		;/

		.Right				;\
		LDA $0000,y			; |
		EOR #$FFFF			; | X offset = 0x10 - xdisp - W
		SEC : SBC $0E			; |
		CLC : ADC #$0010		; |
		BRA .WriteX			;/

		.Left				;\ X offset = xdisp
		LDA $0000,y			;/

		.WriteX				;\
		CLC : ADC $0C			; | add sprite X
		BPL .Valid			;/
		CLC : ADC $0E			;\
		BEQ .OutOfBounds		; | if negative X position + W <= 0, this is out of bounds
		BMI .OutOfBounds		;/
		SEP #$20			;\
		STA $06				; | otherwise, W = W + X and X = 0
		REP #$20			; |
		LDA #$0000			;/

		.Valid
		SEP #$30
		STA $04				; lo byte X -> $04
		XBA : STA $0A			; hi byte X -> $0A
		RTS

		.OutOfBounds
		STZ $04				;\ x = 0
		STZ $0A				;/ y = 0
		LDA #$0101 : STA $06		; size = 1
		SEP #$30			; this should make it never interact with anything
		RTS



	Friction:
		LDA !xspeed,x
		INC #2
		CMP #$04 : BCC .Stop
		LDA !xspeed,x : BMI .inc
		.dec
		DEC !xspeed,x
		DEC !xspeed,x
		DEC !xspeed,x
		RTS
		.inc
		INC !xspeed,x
		INC !xspeed,x
		INC !xspeed,x
		RTS
		.Stop
		STZ !xspeed,x
		RTS



	StartScroll:
		LDA #$0C : STA $143E|!addr	; scroll command
		LDA #$80 : STA $1446|!addr	;\ camera X speed
		STZ $1447|!addr			;/
		STZ $1440|!addr			; starting Y position (determines speed, 0 = slow)
		LDA #$0D : STA $1441|!addr	; ??? related to speed
		STZ $1456|!addr			; layer (0 = layer 1)
		RTS






	HurtBoss:
		LDA !iframes,x : BNE .Return
		LDA #$28 : STA !SPC4

		INC !countercounter,x

		LDA !phase,x
		AND #$7F : TAY			; Y = phase index

		LDA #$40 : STA !iframes,x
		LDA !hp,x
		SEC : SBC .JumpDamage,y
		STA !hp,x
		.CheckHP
		BEQ .NextPhase
		BPL .Return

		.NextPhase
		LDA !phase,x
		INC A
		AND #$7F
		STA !phase,x

		.Return
		RTS


		.JumpDamage
		db $00,$01,$00,$01,$00,$03




;
; terrain generation here
;
;
; chunk header: 1 bytes, number of bytes in chunk (3 bytes per block)
; for each block in the chunk:
;	- 8-bit index offset
;	- 16-bit map16 number




; input:
;	store X/Y coords to $00-$03
;	store pointer to chunk in $04


; $00 - X pos
; $02 - Y pos
; $04 - pointer to chunk data
; $06 - number of bytes to read
;
; $08 - lowest nybble of index to map16 table
; $0A - highest 3 nybbles of index to map16 table
;
; $0C - height x 2
; $0E - height x 4


	GenerateChunk:
		REP #$30
		LDA $00 : BPL .GoodX
		.Return
		SEP #$30
		RTS
		.GoodX
		LDA $02 : BMI .Return
		CMP !levelheight : BCS .Return
		.GoodY

		LDA !levelheight
		ASL A
		STA $0C
		ASL A
		STA $0E


		LDA ($04)
		AND #$00FF
		STA $06
		INC $04


		LDA $00
		AND #$00F0
		LSR #4
		STA $08				; lowest nybble of index
		LDA $01
		AND #$00FF
		TAY
		LDA #$0000
		DEY : BMI +
	-	CLC : ADC !levelheight
		DEY : BPL -
	+	STA $0A
		LDA $02
		AND #$FFF0
		CLC : ADC $0A
		STA $0A				; highest 3 nybbles of index

		LDY #$0000
		CPY $06 : BCS .Done



		.Loop
		LDA ($04),y
		AND #$00F0
		CLC : ADC $0A
		STA $02
		LDA ($04),y
		AND #$000F
		CLC : ADC $08
		CMP #$0010 : BCC ..samescreen
		AND #$000F
		CLC : ADC !levelheight
		..samescreen
		ADC $02				; A = index for current block
		CMP $0E				; make sure to not to to screen 4+
		BCC $02 : SBC $0E		; (wrap 03->00)

		TAX
		INY
		LDA ($04),y
		SEP #$20
		STA !map16lo,x
		XBA : STA !map16hi,x
		REP #$20
		PHX
		TXA
		CMP $0C : BCC ..lotohi
		..hitolo
		SBC $0C
		BRA ..writemirror
		..lotohi
		ADC $0C
		..writemirror
		CMP $0E				; make sure to not go to screen 4+
		BCC $02 : SBC $0E		; (wrap 03->00)
		TAX
		LDA ($04),y
		SEP #$20
		STA !map16lo,x
		XBA : STA !map16hi,x
		REP #$20
		PLX

		INY #2
		CPY $06 : BCC .Loop


		.Done
		SEP #$30
		RTS






; input: void
; outut: void
	ContactGFX:
		LDY #$03
		.Loop
		LDA $17C0|!addr,y : BEQ .ThisOne
		DEY : BPL .Loop
		RTS
		.ThisOne
		LDA #$02 : STA $17C0|!addr,y
		LDA #$07 : STA $17CC|!addr,y
		LDA !mariox : STA $17C8|!addr,y
		LDA !marioy
		CLC : ADC #$10
		STA $17C4|!addr,y
		RTS



; input:
;	$9A = x coordinate of point (16-bit)
;	$98 = y coordinate of point (16-bit)
; output:
;	$08 = map16 index (16-bit)
;	$9A = x coordinate of point (16-bit)
;	$98 = y coordinate of point (16-bit)
;	(returns 16-bit A)
	GetMap16Index:
	.Main
		SEP #$10
		REP #$20
		LDA $98
		AND #$FFF0
		BPL $03 : LDA #$0000
		CMP !levelheight : BCC ..withinboundsy
		LDA !levelheight
		SEC : SBC #$0010
		..withinboundsy
		STA $08
		LDA #$0000
		LDY $9B
		BEQ ..screendone
		BMI ..xdone
		..calcscreen
		CLC : ADC !levelheight
		DEY : BNE ..calcscreen
		..screendone
		CLC : ADC $08
		STA $08
		LDA $9A
		LSR #4
		AND #$000F
		TSB $08
		..xdone
		RTS

; input:
;	$08 = map16 index (16-bit)
; output:
;	A = map16 number (16-bit, but A is 8-bit)
;	X = sprite num
;	(returns all regs 8-bit)
	GetMap16:
		REP #$10
		SEP #$20
		LDX $08
		LDA !map16hi,x
		XBA
		LDA !map16lo,x
		SEP #$30
		LDX !spriteindex
		RTS

; input:
;	$08 = map16 index (16-bit)
;	A = map16 number (16-bit, but A is 8-bit)
; output:
;	X = sprite num
;	A = map16 number (16-bit, but A is 8-bit)
;	(returns all regs 8-bit)
	SetMap16:
		REP #$10
		SEP #$20
		LDX $08
		XBA
		STA !map16hi,x
		XBA
		STA !map16lo,x
		SEP #$30
		LDX !spriteindex
		RTS

	;optimization - doesn't loop to find the acts like of acts like of ... / checks only for $0000-$3FFF
	GetActsLikeOfMap16:
		PHA
		LDA.l $06F626|!bank
		STA $5A
		PLA
		REP #$20
		CMP #$0200
		BCC .return
		ASL
		ADC.l $06F624|!bank
		STA $58
		LDA [$58]
		.return
		SEP #$20
		RTS

	;optimization - only supports layer 1
; input:
;	A = map16 number (16-bit)
;	$98 = y coordinate of point
;	$9A = x coordinate of point
	ChangeMap16Graphics:
		REP #$20
		PEI ($0C)
		PEI ($0E)
		ASL A
		STA $00
		;REP #$10
		;TAX
		;SEP #$20
		PHB
		;STX $00
		;LDY $98 : STY $0E
		;LDY $9A : STY $0C
		LDA $98 : STA $0E
		LDA $9A : STA $0C
		SEP #$30
		;LDX $1933|!addr
		;LDA $5B : STA $0A

		.Horz
		REP #$20
		LDA !levelheight
		ASL A
		STA $04
		LDA $98
		CMP !levelheight
		SEP #$20
		BCC .Process
		.Return
		PLB
		REP #$20
		PLA : STA $0E
		PLA : STA $0C
		SEP #$30
		LDX !spriteindex
		RTS


		.Process
		LDA $9B : STA $0B
		ASL A
		ADC $0B
		TAY
		REP #$20
		LDA $98
		;AND #$FFF0
		;STA $08
		AND #$00F0
		ASL #2 : XBA
		STA $06
		;TXA
		SEP #$20
		;ASL A
		;TAX

		LDA $0D
		LSR A
		LDA $0F
		AND #$01
		ROL A
		ASL #2
		ORA #$20
		;CPX #$00 : BEQ ..noadd
		;ORA #$10
		;..noadd
		TSB $06
		LDA $9A
		AND #$F0
		LSR #3
		TSB $07
		;LSR A
		;TSB $08
		;LDA $1925|!addr
		;ASL A
		;REP #$31
		;ADC $00BEA8|!BankB,x
		;TAX
		;TYA
		;ADC $00,x
		;TAX
		;LDA $08
		;ADC $00,x
		;CMP $04 : BCC ..lotohi			; mirror!
		;..hitolo
		;TAX
		;SBC $04
		;BRA ..shared
		;..lotohi
		;TAX
		;ADC $04
		;..shared
		;STA $04
		;LDA $00					; A = block num
		;ASL A
		;TAY
		;LSR A
		;SEP #$20
		;STA.l !map16lo&$FF0000,x
		;XBA : STA.l !map16hi&$FF0000,x
		;LDX $04
		;STA.l !map16hi&$FF0000,x
		;XBA : STA.l !map16lo&$FF0000,x
		;LSR $0A

		.HorzL1
		STZ $1933|!addr
		REP #$30
		LDY $00
		LDA $1A					;\
		SBC #$007F				; |$08 : Layer1X - 0x80
		STA $08					;/
		LDA $1C : STA $0A			;  $0A : Layer1Y
		PHK
		PEA.w .Return-1
		PEA.w $804D-1
		JML $00C0FB|!BankB



	SpawnBrickPiece:
		LDA $9A
		SEC : SBC !xlo,x
		STA $00
		LDA $98
		SEC : SBC !ylo,x
		STA $01
		LDA !xspeed,x
		BNE $02 : LDA #$10
		STA $02
		LDA #$D0 : STA $03
		SEC : LDA.b #!ParticleNum
		%SpawnSprite()
		LDA #$07 : STA !SPC4			; shatter block sfx
		RTS



	AddToDestroyTileBuffer:
		REP #$20
		LDX !DestroyTileBufferIndex
		LDA $9A : STA !DestroyTileBuffer,x
		LDA $98 : STA !DestroyTileBuffer+2,x
		SEP #$20
		TXA : CLC : ADC #$04 : STA !DestroyTileBufferIndex
		REP #$20
		LDA #$0025 : JSR SetMap16 ;returns with 8-bit A
		RTS



	; optimized + alt name
	ChangeAnimationFromStart:
	UpdateAnim:
		STA !AnimationIndex,x
		REP #$30
		AND #$00FF
		ASL A : TAY
		LDA Animation_Index,y : TAY
		SEP #$20
		LDA Animation_Data,y : STA !FrameIndex,x
		LDA Animation_Time,y : STA !AnimationTimer,x
		SEP #$10
		STZ !AnimationFrameIndex,x
		RTS




		; STZ $01
		; LDA !AnimationIndex,x : STA $00		;$00 = Animation index in 16 bits
		; STZ $03
		; LDA !AnimationFrameIndex,x
		; STA $02					;$02 = Animation Frame index in 16 bits
		; STZ $05
		; STX $04					;$04 = sprite index in 16 bits
		; REP #$30				;A7X/Y of 16 bits
		; LDX $04					;X = sprite index in 16 bits
		; LDA $00
		; ASL
		; TAY					;Y = 2*Animation index
		; LDA $02
		; CLC
		; ADC AnimationIndexer,y
		; TAY					;Y = Position of the first frame of the animation + animation frame index
		; SEP #$20				;A of 8 bits
		; LDA Frames,y
		; STA !FrameIndex,x			;New Frame = Frames[New Animation Frame Index]
		; LDA Times,y
		; STA !AnimationTimer,x			;Time = Times[New Animation Frame Index]
		; LDA $02 : STA !AnimationFrameIndex,x
		; SEP #$10				;X/Y of 8 bits
		; LDX $04					;X = sprite index in 8 bits
		; RTS
