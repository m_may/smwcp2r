; RAM
!SubState = !C2
!HandleInteraction = !1504
!CurrentAction = !151C
!HealthPoints = !1528
!StunTimer = !1534
!WaitTimer = !1540
!DeltaY = !157C
!DeltaX = !1594
!GraphicsFrame = !1602

; Intro
!FinalPosition = $0110
!AttackMode = $04

; General defines
!HurtTimer = 90
!HurtState = $0C

!CannonYOffset = $20

!BulletFireID = $08	; Cluster bullet

!ProjectileID = $AA ; Croc Misc.

!GlassID = $02		; Broken glass ID

!ProjectileFrame = !1602

; Graphical defines
!DefaultFrame = $04

; Aiming

; Spread defines

; Bomb defines
!BombID = $00
!BombFrame = $00

; Vial defines
!VialThrowSpeed = $08
!VialID = $04
!VialFrame = $02

; Aiming defines
!BulletAimingTimer = $30

; Drone defines
!DroneID = $06
!DroneFrame = $06

; Death cutscene
!DeathState = $12
!DeathFrame = $0F
!DeathTimer = 60

!ExplosionTimer = 71

!InitialRejection = $E0

!CrocID = $08
!GlassBrokenSfx = $07
!GlassBrokenBank = $1DFC

!LevelEndMusic = $0B

CrocGraphics:
.Normal:
dw $04,$04,$04,$04,$07,$07,$0B,$0B

.Pointing
dw $03,$03,$03,$03,$08,$08,$0C,$0C

.Hurt1:
dw $05,$05,$05,$05,$09,$09,$0D,$0D

.Hurt2:
dw $06,$06,$06,$06,$0A,$0A,$0E,$0E

; Pointer to the graphics : Numbers of tiles - 1

GfxPointers:
dw .Frame0 : db $09		; Cannon hidden
dw .Frame1 : db $09		; Cannon hidden, pointing
dw .Frame2 : db $09		; Cannon semihidden, pointing
dw .Frame3 : db $09		; Cannon unhidden, pointing
dw .Frame4 : db $09		; Default
dw .Frame5 : db $09		; Hurt #1
dw .Frame6 : db $09		; Hurt #2
dw .Frame7 : db $09		; Default, slightly broken glass
dw .Frame8 : db $09		; Pointing, slightly broken glass
dw .Frame9 : db $09		; Hurt #1, slightly broken glass
dw .FrameA : db $09		; Hurt #2, slightly broken glass
dw .FrameB : db $09		; Default, severely broken glass
dw .FrameC : db $09		; Pointing, severely broken glass
dw .FrameD : db $09		; Hurt #1, severely broken glass
dw .FrameE : db $09		; Hurt #2, severely broken glass
dw .FrameF : db $07		; Glass broken

; X offset, Y offset, tile, properties*
; *Properties are YX-SCCCT with S as the size bit instead of priority (which is controlled by $64).

.Frame0:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FA2
db $00 : db $20 : dw $1FA4
db $10 : db $20 : dw $5FA2
db $F8 : db $00 : dw $1D08
db $08 : db $00 : dw $1D0A

.Frame1:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FA2
db $00 : db $20 : dw $1FA4
db $10 : db $20 : dw $5FA2
db $F8 : db $00 : dw $1D24
db $08 : db $00 : dw $1D26

.Frame2:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FA6
db $00 : db $20 : dw $1FA8
db $10 : db $20 : dw $5FA6
db $F8 : db $00 : dw $1D24
db $08 : db $00 : dw $1D26

.Frame3:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D24
db $08 : db $00 : dw $1D26

.Frame4:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D08
db $08 : db $00 : dw $1D0A

.Frame5:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D28
db $08 : db $00 : dw $1D2A

.Frame6:
db $F8 : db $00 : dw $1F00
db $08 : db $00 : dw $1F02
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D48
db $08 : db $00 : dw $1D4A

.Frame7:
db $F8 : db $00 : dw $1F04
db $08 : db $00 : dw $1F06
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D08
db $08 : db $00 : dw $1D0A

.Frame8:
db $F8 : db $00 : dw $1F04
db $08 : db $00 : dw $1F06
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D24
db $08 : db $00 : dw $1D26

.Frame9:
db $F8 : db $00 : dw $1F04
db $08 : db $00 : dw $1F06
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D28
db $08 : db $00 : dw $1D2A

.FrameA:
db $F8 : db $00 : dw $1F04
db $08 : db $00 : dw $1F06
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D48
db $08 : db $00 : dw $1D4A

.FrameB:
db $F8 : db $00 : dw $1F20
db $08 : db $00 : dw $1F22
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D08
db $08 : db $00 : dw $1D0A

.FrameC:
db $F8 : db $00 : dw $1F20
db $08 : db $00 : dw $1F22
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D24
db $08 : db $00 : dw $1D26

.FrameD:
db $F8 : db $00 : dw $1F20
db $08 : db $00 : dw $1F22
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D28
db $08 : db $00 : dw $1D2A

.FrameE:
db $F8 : db $00 : dw $1F20
db $08 : db $00 : dw $1F22
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D48
db $08 : db $00 : dw $1D4A

.FrameF:
db $F8 : db $00 : dw $1F40
db $08 : db $00 : dw $1F42
db $F0 : db $10 : dw $1F8A
db $00 : db $10 : dw $1F8C
db $10 : db $10 : dw $5F8A
db $F0 : db $20 : dw $1FAA
db $00 : db $20 : dw $1FCE
db $10 : db $20 : dw $5FAA
db $F8 : db $00 : dw $1D68
db $08 : db $00 : dw $1D6A

XPositions:
db $44,$4C,$54,$5C,$64,$6C,$74,$7C
db $84,$8C,$94,$9C,$A4,$AC,$B4,$BC

YPositions:
db $02,$04,$06,$08,$0A,$0C,$0E,$10
db $12,$14,$16,$18,$1A,$1C,$1E,$20

print "INIT ",pc
	LDA #$10
	STA !AA,x
	LDA #$0B
	STA $71
	STZ !GraphicsFrame,x
	LDA #$01
	STA $18B8|!addr
RTL

print "MAIN ",pc
	PHB : PHK : PLB
	JSR CrocMain
	PLB
RTL

CrocMain:
	LDA !GraphicsFrame,x	; Don't draw Doc if GFX index > $7F
	BMI +
	JSR Graphics

+	LDA #$00
	%SubOffScreen()

	TXY
	LDX !CurrentAction,y	; Get the current action.
	JSR (.Actions,x)
	LDX $15E9|!addr			; Restore sprite index
	LDA !HandleInteraction,x
	BEQ +
	STZ !HandleInteraction,x
	JSR MarioInteraction
	JSR SpriteInteraction
+	
RTS

.Actions
dw MoveDown					; 0
dw OpenMouth				; 2
dw PrepareAttack			; 4
dw Attack					; 6
dw BobOmbSpring				; 8
dw Wait						; A
dw TeleportOut				; C
dw Teleport					; E
dw TeleportIn				; 10
dw Death					; 12
dw DeathExplosion			; 14
dw DeathFalling				; 16

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Cutscene
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MoveDown:
	TYX
	LDA #$01
	STA $9D
	JSL $01801A|!bank		; Apply speed
	LDA !14D4,x
	XBA
	LDA !D8,x
	REP #$20
	CMP #!FinalPosition
	SEP #$20
	BNE .Return
	STZ !AA,x
	TYX
	INC !CurrentAction,x	; Next action
	INC !CurrentAction,x
	LDA #45
	STA !WaitTimer,x
.Return:
RTS

OpenMouth:
	TYX
	LDA #$01
	STA $9D
	LDY #$00
	DEC !WaitTimer,x		; Have to do this manually
	LDA !WaitTimer,x
	BEQ .Finished
	CMP #30
	BCS .Store
	INY
	CMP #15
	BCS .Store
	INY
.Store:
	TYA
	STA !GraphicsFrame,x
RTS

.Finished:
	LDA #!DefaultFrame
	STA !GraphicsFrame,x
	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #60
	STA !WaitTimer,x
	STZ $9D
	STZ $71
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Movement
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Wait:
	JSR IsFrozen			; Is the game running?

	LDA #$01
	STA !HandleInteraction,y
	LDA !WaitTimer,y
	BNE .Return

	TYX
	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #20
	STA !WaitTimer,x
	STZ !SubState,x
.Return:
RTS

TeleportOut:
	JSR IsFrozen

	LDA !WaitTimer,y
	PHA
	LSR #2
	TAX
	LDA #$FF
	BCC .StoreGraphics
	LDA.w !SubState,y
	BEQ .NotHurt
	TXA
	LSR #2
	LDX !HealthPoints,y
	LDA CrocGraphics_Hurt1,x
	BCC .StoreGraphics
	LDA CrocGraphics_Hurt2,x
BRA .StoreGraphics

.NotHurt:
	LDX !HealthPoints,y
	LDA CrocGraphics_Pointing,x
.StoreGraphics:
	STA !GraphicsFrame,y

	PLA
	BNE +

	TYX

	JSR RNG
	PHA
	AND #$0F
	TAY
	LDA !D8,x
	SEC : SBC YPositions,y
	STA !DeltaY,x			; Get the distance of the next position.
	PLA
	LSR #4
	TAY
	LDA !E4,x
	SEC : SBC XPositions,y
	STA !DeltaX,x
	LDA #40
	STA !WaitTimer,x
	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #$03
	STA !GraphicsFrame,x
+
RTS

Teleport:
	JSR IsFrozen
	LDX !HealthPoints,y
	LDA.w !SubState,y
	BEQ +
	LDA CrocGraphics_Normal,x
	BRA .StoreGraphics

+	LDA CrocGraphics_Pointing,x

.StoreGraphics:
	STA !GraphicsFrame,y
	LDA !WaitTimer,y
	BNE .NotNext
	TYX
	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #20
	STA !WaitTimer,x
RTS

.NotNext:
	AND #$01				; Alternate between old and new position.
	ASL
	TAX
	JSR (.Actions,x)
RTS

.Actions
dw .First
dw .Second

.First:
	LDA.w !D8,y
	CLC : ADC !DeltaY,y
	STA.w !D8,y
	LDA.w !E4,y
	CLC : ADC !DeltaX,y
	STA.w !E4,y
RTS

.Second:
	LDA.w !D8,y
	SEC : SBC !DeltaY,y
	STA.w !D8,y
	LDA.w !E4,y
	SEC : SBC !DeltaX,y
	STA.w !E4,y
RTS

TeleportIn:
	JSR IsFrozen
	LDA !WaitTimer,y
	BEQ .Finish
	LSR #2
	LDA #$FF
	BCC .StoreGraphics
	LDX !HealthPoints,y
	LDA !SubState,y
	BEQ +
	LDA CrocGraphics_Normal,x
	BRA .StoreGraphics
+	LDA CrocGraphics_Pointing,x
.StoreGraphics
	STA !GraphicsFrame,y
RTS

.Finish:
	LDX !HealthPoints,y
	LDA CrocGraphics_Normal,x
	STA !GraphicsFrame,y
	LDA #!AttackMode
	STA !CurrentAction,y
	LDA #120
	STA !WaitTimer,y
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Handles Croc's death
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Death:
	LDX !WaitTimer,y
	BNE .Return
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
	LDA #!ExplosionTimer
	STA !WaitTimer,y
.Return
	TXA
	LSR #2
	AND #$01
	CLC : ADC #$0D
	STA !GraphicsFrame,y
RTS

DeathExplosion:
	LDX !WaitTimer,y
	BNE .NotNextState
BRA .SpawnSprites

.NotNextState
	TXA
	LSR #2
	AND #$01
	CLC : ADC #$0D
	STA !GraphicsFrame,y
	TXA
	AND #$07
	BNE .NoSmoke
	TXA
	LSR #3
	TAX
	LDA #$09
	STA $1DFC|!addr
	LDA .SmokePosition_X,x
	STA $00
	LDA .SmokePosition_Y,x
	STA $01
	LDA #$1B
	STA $02
	TYX
	LDA #$01
	%SpawnSmoke()
.NoSmoke:
RTS

.SmokePosition:
..X
db $10,$F8,$F6,$02,$FB,$1A,$F5,$01

..Y
db $00,$1E,$12,$07,$15,$0E,$13,$05

.SpawnSprites:
	JSL $02A9E4|!bank
	BPL ..DontReturn
RTS

..DontReturn:
	TYX
	LDA #$08
	STA !14C8,x
	JSL $07F7D2|!bank
	LDA #!ProjectileID
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY

	LDX $15E9|!addr
	LDA !D8,x
	STA.w !D8,y
	LDA !14D4,x
	STA !14D4,y
	LDA !E4,x
	STA.w !E4,y
	LDA !14E0,x
	STA !14E0,y
	LDA #!CrocID
	STA !1528,y		; "Projectile" type (Doc Croc)
	LDA #$B0
	STA.w !AA,y
	LDA #!GlassBrokenSfx
	STA !GlassBrokenBank|!addr

	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #!DeathFrame
	STA !GraphicsFrame,x

	LDA #$04
	STA $00
	STZ $01

	LDA #$F0
	STA $02
	LDA #$F8
	STA $03
	LDA #!GlassID+!ExtendedOffset
	%SpawnExtended()
	BCS +
	LDA #$00
	STA $1765|!addr,y
+

	LDA #$10
	STA $02
	LDA #$F8
	STA $03
	LDA #!GlassID+!ExtendedOffset
	%SpawnExtended()
	BCS +
	LDA #$01
	STA $1765|!addr,y
+

	LDA #$F8
	STA $02
	LDA #$F0
	STA $03
	LDA #!GlassID+!ExtendedOffset
	%SpawnExtended()
	BCS +
	LDA #$02
	STA $1765|!addr,y
+

	LDA #$08
	STA $02
	LDA #$F0
	STA $03
	LDA #!GlassID+!ExtendedOffset
	%SpawnExtended()
	BCS +
	LDA #$03
	STA $1765|!addr,y
+
RTS

DeathFalling:
	TYX
	LDA !AA,x
	BPL .Falling
	CMP #$E8
	BCS .Falling
	LDA #$E8
	STA !AA,x
.Falling:
	CLC : ADC #$03
	STA !AA,x
	BMI .NotTerminalSpeed
	CMP #$40
	BCC .NotTerminalSpeed
	LDA #$40
	STA !AA,x
.NotTerminalSpeed

	JSL $01801A|!bank

	LDA !14D4,x
	XBA
	LDA !D8,x
	REP #$20
	SEC : SBC $1C
	CMP #$00F0
	SEP #$20
	BCC .OnScreen

	STZ !14C8,x
	LDA #$0B
	STA $1696|!Base2	;uberasm victory flag by blind devil
.OnScreen
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Spawns a Bob-Omb or a springboard depending on Croc's HP
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BobOmbSpring:
	LDA !HealthPoints,y
	CMP #$0E			; Skip on the last health point.
	BNE .Okay			; Player has to jump onto the drone to kill Croc.
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
RTS

.Okay
	JSR IsFrozen

	LDA #$01
	STA !HandleInteraction,y

	LDA !HealthPoints,y
	LSR
	TAX
	LDA .SpriteNumbers,x
	BMI .FoundOne		; Failsafe
	STA $00

	LDX #!SprSize-1
.Loop:
	LDA !14C8,x
	CMP #$08
	BCC .NextSprite
	LDA !9E,x			; Found a Bob-Omb or Spring? Skip.
	CMP $00
	BEQ .FoundOne

.NextSprite:
	DEX
	BPL .Loop

	LDA !WaitTimer,y
	BNE .Return
	
	PHY
	JSL $02A9E4|!bank
	TYX
	PLY
	LDA $00
	STA !9E,x
	LDA #$01
	STA !14C8,x
	JSL $07F7D2|!bank
	LDA !D8,y
	CLC : ADC #!CannonYOffset
	STA !D8,x
	LDA !14D4,y
	ADC #$00
	STA !14D4,x
	LDA !E4,y
	STA !E4,x
	LDA !14E0,y
	STA !14E0,x
	
	LDA #$06
	STA $1DFC|!addr

.FoundOne:
	LDA #$20
	STA !WaitTimer,y
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
.Return:
RTS

.SpriteNumbers:
db $0D,$2F,$0D,$2F,$FF,$0D,$FF,$2F

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Prepare Attack
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrepareAttack:
	JSR IsFrozen
	LDA #$01
	STA !HandleInteraction,y
	LDA !WaitTimer,y
	BNE .Return
	LDA !HealthPoints,y
	TAX
	JSR (.Attacks,x)
	TYX
	INC !CurrentAction,x
	INC !CurrentAction,x
.Return
RTS

.Attacks
dw .Normal			; Spread
dw .Normal			; Bomb
dw .Normal			; Aiming
dw .Normal			; Vial
dw .Normal			; Mushroom
dw .RNG
dw .Drone
dw .RNG

.Normal
	STX !SubState,y
	LDA ..WaitTimer,x
	STA !WaitTimer,y
RTS

..WaitTimer:
dw 50, 40, 30, 45, 15

.RNG:
	JSR RNG
	AND #$06
	STA !SubState,y
	
	CPX #$0E
	BNE ..Slower
	CLC : ADC #$08
..Slower:
	TAX
	LDA ..WaitTimer,x
	STA !WaitTimer,y
RTS

..WaitTimer:
dw 30, 35, 30, 35	; Slow (2 HP)
dw 25, 30, 25, 25	; Fast (1 HP)

.Drone:
	LDA #$30
	STA !WaitTimer,y
	LDA #$0A
	STA !SubState,y
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Attack state
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Attack:
	JSR IsFrozen
	LDA #$01
	STA !HandleInteraction,y
	LDX !SubState,y
	JMP (.Attacks,x)

.Attacks
dw .Spread
dw .Bomb
dw .Aiming
dw .Vial
dw .Mushroom
dw .Drone

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Spread attack
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Spread:
	LDX #$04
	LDA !WaitTimer,y
	BEQ ..FireEnd
	DEX #2
	CMP #09
	BEQ ..Fire
	DEX #2
	CMP #19
	BEQ ..Fire
RTS

..FireEnd:
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
	LDA #$20
	STA !WaitTimer,y
..Fire:
	STX $00
	LDA ..SpeedX,x
	STA $01
	LDA ..SpeedY,x
	STA $02
	JSR FindClusterSlot
	JSR ..FireBullet
	LDA #$27
	STA $1DFC|!addr

	LDY $00
	INY
	LDA ..SpeedX,y
	STA $01
	LDA ..SpeedY,y
	STA $02
	LDY $15E9|!addr

	JSR FindClusterSlot_yield

..FireBullet:
	LDA #!BulletFireID+!ClusterOffset
	STA $1892|!addr,x
	LDA !D8,y
	CLC : ADC #!CannonYOffset
	STA $1E02|!addr,x
	LDA !14D4,y
	ADC #$00
	STA $1E2A|!addr,x
	LDA !E4,y
	STA $1E16|!addr,x
	LDA !14E0,y
	STA $1E3E|!addr,x
	LDA $02
	STA $1E52|!addr,x
	LDA $01
	STA $1E66|!addr,x
	LDA #$00
	STA $0F4A|!addr,x
RTS

..SpeedX:
db $04,$FC,$12,$EE,$18,$E8

..SpeedY:
db $18,$18,$0F,$0F,$00,$00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Exploding Bomb
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Bomb:
	LDA !WaitTimer,y
	BEQ ..Return
	TYX
	JSL $02A9E4|!bank
	BMI ..Return
	
	TYX
	LDA #$08
	STA !14C8,y
	JSL $07F7D2|!bank
	LDA #!ProjectileID
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY

	LDX $15E9|!addr
	LDA !D8,x
	CLC : ADC #!CannonYOffset
	STA !D8,y
	LDA !14D4,x
	ADC #$00
	STA !14D4,y
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y
	LDA #!BombID
	STA !1528,y		; Projectile type (bomb)
	LDA #!BombFrame
	STA !ProjectileFrame,y

	LDA #$20
	STA !WaitTimer,x

	INC !CurrentAction,x
	INC !CurrentAction,x
	LDA #$06
	STA $1DFC|!addr
..Return:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Homing bullets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Aiming:
	LDX #$03
	LDA !WaitTimer,y
	BEQ ..FireEnd
	DEX #3
	CMP #09
	BEQ ..Fire
	INX #3
	CMP #19
	BEQ ..Fire
RTS

..FireEnd:
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
	LDA #$20
	STA !WaitTimer,y
..Fire:
	STX $00
	LDA ..SpeedX,x
	STA $01
	LDA ..SpeedY,x
	STA $02
	JSR FindClusterSlot
	JSR ..FireBullet
	LDA #$27
	STA $1DFC|!addr

	LDY $00
	LDA ..SpeedX+1,y
	STA $01
	LDA ..SpeedY+1,y
	STA $02
	LDY $15E9|!addr

	JSR FindClusterSlot_yield
	JSR ..FireBullet
	LDY $00
	LDA ..SpeedX+2,y
	STA $01
	LDA ..SpeedY+2,y
	STA $02
	LDY $15E9|!addr

	JSR FindClusterSlot_yield

..FireBullet:
	LDA #!BulletFireID+!ClusterOffset
	STA $1892|!addr,x
	LDA !D8,y
	CLC : ADC #!CannonYOffset
	STA $1E02|!addr,x
	LDA !14D4,y
	ADC #$00
	STA $1E2A|!addr,x
	LDA !E4,y
	STA $1E16|!addr,x
	LDA !14E0,y
	STA $1E3E|!addr,x
	LDA $02
	STA $1E52|!addr,x
	LDA $01
	STA $1E66|!addr,x
	LDA #$00
	STA $0F4A|!addr,x
	LDA #!BulletAimingTimer
	STA $0F9A|!addr,x
RTS

..SpeedX:
db $F6,$F1,$EE,$0A,$0F,$12

..SpeedY:
db $EE,$F1,$F6,$EE,$F1,$F6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Fire vials
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Vial:
	LDX #$00
	LDA !WaitTimer,y
	BEQ ..FireEnd
	INX
	CMP #30
	BEQ ..Fire
	INX
	CMP #15
	BEQ ..Fire
RTS

..FireEnd:
	LDA !CurrentAction,y
	INC #2
	STA !CurrentAction,y
	LDA #$20
	STA !WaitTimer,y
..Fire:
	LDA ..XSpeed,x
	STA $0D

	JSL $02A9E4|!bank
	BMI ..Return
	TYX
	LDA #$08
	STA !14C8,y
	JSL $07F7D2|!bank
	LDA #!ProjectileID
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY

	LDX $15E9|!addr
	LDA !D8,x
	CLC : ADC #!CannonYOffset
	STA !D8,y
	LDA !14D4,x
	ADC #$00
	STA !14D4,y
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y

	LDA #!VialThrowSpeed
	STA !AA,y
	LDA $0D
	STA !B6,y

	LDA #!VialID
	STA !1528,y		; Projectile type (vial)
	LDA #!VialFrame
	STA !ProjectileFrame,y

	LDA #$06
	STA $1DFC|!addr
..Return:
RTS

..XSpeed:
db $20,$E0,$00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mushroom vial
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Mushroom:
	LDX #$00
	LDA !WaitTimer,y
	BEQ ..Fire
RTS

..Fire
	JSL $02A9E4|!bank
	BMI ..Return
	TYX
	LDA #$08
	STA !14C8,y
	JSL $07F7D2|!bank
	LDA #!ProjectileID
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY

	LDX $15E9|!addr
	LDA !D8,x
	STA !D8,y
	LDA !14D4,x
	STA !14D4,y
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y

	LDA #!VialThrowSpeed
	STA !AA,y
	LDA #$00
	STA !B6,y

	LDA #!VialID
	STA !1528,y		; Projectile type (vial)
	LDA #!VialFrame
	STA !ProjectileFrame,y
	LDA #$01
	STA !C2,y		; Vial type (mushroom)

	LDA #$06
	STA $1DFC|!addr

	LDA !CurrentAction,x
	CLC : ADC #$04	; Skip Bob-Omb
	STA !CurrentAction,x
	INC !HealthPoints,x
	INC !HealthPoints,x
..Return:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Homing drone
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.Drone:
	LDA !WaitTimer,y
	BEQ ..Return
	TYX
	JSL $02A9E4|!bank
	BMI ..Return
	
	TYX
	LDA #$08
	STA !14C8,y
	JSL $07F7D2|!bank
	LDA #!ProjectileID
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY

	LDX $15E9|!addr
	LDA !D8,x
	CLC : ADC #!CannonYOffset
	STA !D8,y
	LDA !14D4,x
	ADC #$00
	STA !14D4,y
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y
	LDA #!DroneID
	STA !1528,y		; Projectile type (drone)

	LDA #!DroneFrame
	STA !ProjectileFrame,y

	LDA #$20
	STA !WaitTimer,x

	LDA !CurrentAction,x
	CLC : ADC #$04
	STA !CurrentAction,x
	INC !HealthPoints,x
	INC !HealthPoints,x
	LDA #$06
	STA $1DFC|!addr
..Return:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Subroutines
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Graphics
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:
	%GetDrawInfo()

	LDA !GraphicsFrame,x
	STA $0E
	STZ $0F
	REP #$20
	AND #$00FF
	ASL
	CLC : ADC $0E
	ADC.w #GfxPointers
	STA $0E
	LDA ($0E)
	PHA
	SEP #$20
	LDY #$02
	LDA ($0E),y
	STA $0E
	STA $0F
	LDA !15EA,x
	LSR #2
	CLC : ADC #$60
	STA $0C
	LDA #$04
	STA $0D

	LDY !15EA,x
	REP #$10
	PLX 
.GfxLoop:
	LDA $0000,x
	CLC : ADC $00
	STA $0300|!addr,y
	LDA $0001,x
	CLC : ADC $01
	STA $0301|!addr,y
	LDA $0002,x
	STA $0302|!addr,y
	LDA $0003,x
	AND #$10
	LSR #3
	STA ($0C)
	LDA $0003,x
	AND #$CF
	ORA $64
	STA $0303|!addr,y

	INC $0C
	INX #4
	INY #4
	DEC $0E
	BPL .GfxLoop
	SEP #$10

	LDX $15E9|!addr
	LDY #$FF
	LDA $0F
	JSL $01B7B3|!bank
.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Kills sprite if sprite doesn't live or game is frozen.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

IsFrozen:
	LDA !14C8,y
	EOR #$08
	CMP $9D
	BEQ .Return
	PLA : PLA	; Terminates itself
.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Finds an empty cluster slot, you can continue as well.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FindClusterSlot:
	LDX #$14
.yield:
	DEX
-	LDA $1892|!addr,x
	BEQ .Okay
	DEX
	BPL -
	PLA : PLA	; Terminates itself
.Okay
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Finds an empty cluster slot, you can continue as well.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetCrocClipping:
	LDA !D8,x
	CLC : ADC #$02
	STA $05
	LDA !14D4,x
	ADC #$00
	STA $0B
	
	LDA !E4,x
	SEC : SBC #$08
	STA $04
	LDA !14E0,x
	SBC #$00
	STA $0A
	
	LDA #$20
	STA $06
	LDA #$20
	STA $07
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Interaction routines, one for Mario and one for sprites
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MarioInteraction:
	JSR GetCrocClipping

	JSL $03B664|!bank
	JSL $03B72B|!bank
	BCS .Interacting
RTS

.Interacting:
	LDA $0B
	XBA
	LDA $05
	REP #$20
	SEC
	SBC #$0014
	CMP $D3
	SEP #$20
	BMI .SpriteWins
	JSL $01AA33|!bank	; Boost Mario upwards

.HurtCroc:
	; Kill all springboards
	LDX #!SprSize-1
.Loop:
	LDA !14C8,x
	CMP #$08
	BCC .Nope
	LDA !9E,x			; Found a Spring? Kill it.
	CMP #$2F
	BEQ .Okay
	LDA $1337
	LDA !7FAB10,x		; Is it a custom sprite? Good.
	AND #$08
	BEQ .Nope
	LDA !7FAB9E,x		; Is it the Croc Misc sprite? Good.
	CMP #!ProjectileID
	BNE .Nope
	LDA !1528,x			; Is it a drone? Kill it!
	CMP #!DroneID
	BNE .Nope

.Okay:
	LDA #$04
	STA !14C8,x
	LDA #$1F
	STA !1540,x

.Nope:
	DEX
	BPL .Loop

	LDX $15E9|!addr
	INC !SubState,x		; Mark Doc as hurt (alternate animation)
	LDA #$28
	STA $1DFC|!addr
	LDA #!HurtTimer
	STA !WaitTimer,x
	INC !HealthPoints,x
	INC !HealthPoints,x
	LDA !HealthPoints,x
	CMP #$10
	BNE .Survived
	LDA #!DeathState
	STA !CurrentAction,x
	LDA #$FF
	STA $1DFB|!addr		; Disable music
	LDA #!DeathTimer
	STA !WaitTimer
RTS

.Survived:
	LDA #!HurtState
	STA !CurrentAction,x
RTS

.SpriteWins:
	JSL $00F5B7|!bank
RTS

SpriteInteraction:
	LDY #!SprSize-1
.Loop:
	LDA.w !9E,y
	CMP #$0D
	BNE .NextSprite
	LDA !14C8,y
	CMP #$08
	BEQ .HasExploded
	CMP #$09
	BNE .NextSprite

	; Stunned/Kicked
	LDA !D8,x
	CLC : ADC #$22
	STA $05
	LDA !14D4,x
	ADC #$00
	STA $0B
	
	LDA !E4,x
	CLC : ADC #$02
	STA $04
	LDA !14E0,x
	ADC #$00
	STA $0A
	
	LDA #$0C
	STA $06
	LDA #$0C
	STA $07
	BRA .Shared

.NextSprite:
	DEY
	BPL .Loop
RTS

.HasExploded:
	; Self-explainatory
	LDA !1534,y
	BEQ .NextSprite
	JSR GetCrocClipping

.Shared
	PHX
	TYX
	JSL $03B6E5|!bank
	PLX
	JSL $03B72B|!bank
	BCC .NextSprite

	PHY
	JSR MarioInteraction_HurtCroc
	PLY

	; Only explode thrown Bob-Ombs, ignore their explosions.
	LDA !14C8,y
	CMP #$08
	BEQ .NotThrown
	LDA #$08
	STA !14C8,y
	LDA #$40
	STA !1540,y
	LDA #$01
	STA !1534,y
.NotThrown
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Get the RNG depending on the player position.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RNG:
	JSL $01ACF9|!bank		; RNG
	LDA $148D|!addr			; Modify with player position.
	EOR $94
	EOR $96
RTS

