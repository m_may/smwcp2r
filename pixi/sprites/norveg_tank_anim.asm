

macro tile()
	db !currentx+!basex
	db !currenty+!basey
	db !tile
	db $3D

	!currentx	:= !currentx+$10
	!tile		:= !tile+2
endmacro

macro row(row, x, tile)
	!currentx = <x>
	!currenty = <row>*$10
	!tile = <tile>
endmacro



Tank:

	.Ptr
	dw .TM		; 0
	dw .Pilot0	; 1
	dw .Pilot1	; 2
	dw .Glass	; 3
	dw .Glove	; 4
	dw .Jaw		; 5
	dw .Foot1	; 6
	dw .Knee1	; 7
	dw .Hip		; 8
	dw .ShoulderPad	; 9
	dw .FullJunk	; A
	dw .Foot2	; B
	dw .Knee2	; C


	!basex	= -$32
	!basey	= -$68

	.TM
	db ..end-..start
	..start
	; %row(0, $18, $03)
	; %tile()
	; %tile()
	; %row(1, $10, $22)
	; %tile()
	; %tile()
	; %tile()
	; %row(2, $00, $40)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(3, $00, $60)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(4, $08, $81)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(5, $30, $A6)
	; %tile()
	; %tile()
	; %tile()
	..end


	.FullJunk
	db ..end-..start
	..start
	; %row(0, $18, $03)
	; %tile()
	; %tile()
	; %row(1, $10, $22)
	; %tile()
	; %tile()
	; %tile()
	; %row(2, $00, $40)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(3, $00, $60)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(4, $08, $81)
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %tile()
	; %row(5, $30, $A6)
	; %tile()
	; %tile()
	; %tile()
	; shoulder pad
	db $10+!basex,$38+!basey,$3C,$3D
	db $20+!basex,$38+!basey,$3E,$3D
	; jaw
	db $1B+!basex,$45+!basey,$A0,$2D
	db $1B+!basex,$4D+!basey,$B0,$2D
	db $2B+!basex,$4D+!basey,$B2,$2D
	db $2B+!basex,$55+!basey,$C2,$2D
	db $3B+!basex,$55+!basey,$C4,$2D
	..end

	.ShoulderPad
	db ..end-..start
	..start
	db $10+!basex,$38+!basey,$3C,$3D
	db $20+!basex,$38+!basey,$3E,$3D
	..end


	!basex	= $00
	!basey	= -$4A

	.Pilot0
	db ..end-..start
	..start
	db $F8+!basex,$00+!basey,$20,$2D
	db $08+!basex,$00+!basey,$22,$2D
	db $F8+!basex,$10+!basey,$40,$2D
	db $08+!basex,$10+!basey,$42,$2D
	..end

	.Pilot1
	db ..end-..start
	..start
	db $F8+!basex,$00+!basey,$24,$2D
	db $08+!basex,$00+!basey,$26,$2D
	db $F8+!basex,$10+!basey,$44,$2D
	db $08+!basex,$10+!basey,$46,$2D
	..end

	!basex = $06
	!basey = -$47

	.Glass
	db ..end-..start
	..start
	db $00+!basex,$00+!basey,$08,$2D
	db $08+!basex,$00+!basey,$09,$2D
	db $00+!basex,$08+!basey,$18,$2D
	db $08+!basex,$08+!basey,$19,$2D
	..end

	!basex = -$1D
	!basey = -$23

	.Glove
	db ..end-..start
	..start
	db $00+!basex,$00+!basey,$5C,$3D
	db $10+!basex,$00+!basey,$5E,$3D
	db $00+!basex,$08+!basey,$6C,$3D
	db $10+!basex,$08+!basey,$6E,$3D
	..end

	!basex = -$17
	!basey = -$23

	.Jaw
	db ..end-..start
	..start
	db $00+!basex,$00+!basey,$A0,$2D
	db $00+!basex,$08+!basey,$B0,$2D
	db $10+!basex,$08+!basey,$B2,$2D
	db $10+!basex,$10+!basey,$C2,$2D
	db $20+!basex,$10+!basey,$C4,$2D
	..end

	!basex = -$24
	!basey = -$06

	.Foot1
	db ..end-..start
	..start
	db $05+!basex,$F9+!basey,$38,$3D
	db $00+!basex,$00+!basey,$0B,$3D
	db $10+!basex,$00+!basey,$0D,$3D
	db $18+!basex,$00+!basey,$0E,$3D
	db $00+!basex,$08+!basey,$1B,$3D
	db $10+!basex,$08+!basey,$1D,$3D
	db $18+!basex,$08+!basey,$1E,$3D
	..end

	.Foot2
	db ..end-..start
	..start
	db $05+!basex,$F9+!basey,$38,$2D
	db $00+!basex,$00+!basey,$0B,$2D
	db $10+!basex,$00+!basey,$0D,$2D
	db $18+!basex,$00+!basey,$0E,$2D
	db $00+!basex,$08+!basey,$1B,$2D
	db $10+!basex,$08+!basey,$1D,$2D
	db $18+!basex,$08+!basey,$1E,$2D
	..end


	.Knee1
	db ..end-..start
	..start
	db $01+!basex,$E9+!basey,$3A,$3D
	..end

	.Knee2
	db ..end-..start
	..start
	db $01+!basex,$E9+!basey,$3A,$2D
	..end

	.Hip
	db ..end-..start
	..start
	db $0A+!basex,$E3+!basey,$3A,$3D
	..end

	.Foot1OffsetY
	db $EA,$EA,$EA,$EB,$EB,$EB,$EC,$EC,$ED,$ED,$EE,$EE,$EF,$EF,$F0,$F0
	db $F1,$F2,$F3,$F4,$F5,$F6,$F7,$F8,$F9,$FA,$FB,$FC,$FD,$FE,$FF,$00
	.Foot2OffsetY
	db $00,$FF,$FE,$FD,$FC,$FB,$FA,$F9,$F8,$F7,$F6,$F5,$F4,$F3,$F2,$F1
	db $F0,$F0,$EF,$EF,$EE,$EE,$ED,$ED,$EC,$EC,$EB,$EB,$EB,$EA,$EA,$EA
	; complement
	db $EA,$EA,$EA,$EB,$EB,$EB,$EC,$EC,$ED,$ED,$EE,$EE,$EF,$EF,$F0,$F0
	db $F1,$F2,$F3,$F4,$F5,$F6,$F7,$F8,$F9,$FA,$FB,$FC,$FD,$FE,$FF,$00



	.Foot1OffsetX
	db $F8,$FA,$FB,$FD,$FE,$00,$01,$03,$04,$06,$07,$09,$0A,$0C,$0D,$0F
	db $10,$0F,$0E,$0D,$0C,$0B,$0A,$09,$08,$07,$06,$05,$04,$03,$02,$01
	.Foot2OffsetX
	db $00,$FF,$FE,$FD,$FC,$FB,$FA,$F9,$F8,$F7,$F6,$F5,$F4,$F3,$F2,$F1
	db $F1,$F1,$F2,$F2,$F3,$F3,$F4,$F4,$F5,$F5,$F6,$F6,$F6,$F7,$F7,$F8
	; complement
	db $F8,$FA,$FB,$FD,$FE,$00,$01,$03,$04,$06,$07,$09,$0A,$0C,$0D,$0F
	db $10,$0F,$0E,$0D,$0C,$0B,$0A,$09,$08,$07,$06,$05,$04,$03,$02,$01




