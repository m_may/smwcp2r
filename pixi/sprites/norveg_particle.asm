

	; stuff you should actually mess with
		!BossNum		= $00
		!ProjectileNum		= $01




	; routines
		!GetMarioClipping	= $03B664|!BankB
		!GetSpriteClipping04	= $03B69F|!BankB
		!GetSpriteClipping00	= $03B6E5|!BankB
		!CheckContact		= $03B72B|!BankB

		!ApplySpeed		= $01802A|!BankB

		!HurtMario		= $00F5B7|!BankB

	; RAM
		!OAM			= $0200|!addr
		!OAMhi			= $0420|!addr

		!spriteindex		= $15E9|!addr


		!SPC1			= $1DF9|!addr
		!SPC2			= $1DFA|!addr
		!SPC3			= $1DFB|!addr
		!SPC4			= $1DFC|!addr



	; mario RAM
		!marioanim		= $71

		!marioxspeed		= $7B
		!marioyspeed		= $7D

		!mariox			= $94
		!marioy			= $96

		!mariospinjump		= $140D|!addr


	; sprite regs
		!x_lo			= !sprite_x_low
		!x_hi			= !sprite_x_high
		!y_lo			= !sprite_y_low
		!y_hi			= !sprite_y_high
		!xlo			= !x_lo
		!xhi			= !x_hi
		!ylo			= !y_lo
		!yhi			= !y_hi

		!xspeed			= !sprite_speed_x
		!yspeed			= !sprite_speed_y

		!dir			= !sprite_misc_157c

		!animtimer		= !sprite_misc_1570



	; sprite_misc_1528:	type
	; sprite_misc_1540:	life timer (if applicable)



	print "INIT ", pc
	INIT:
		RTL


	DESPAWN:
		SEP #$20
		STZ !sprite_status,x
		PLB
		RTL


	print "MAIN ", pc
	MAIN:
		PHB : PHK : PLB
		LDA $9D : BNE .Graphics

		.Physics
		JSL !ApplySpeed-$10
		JSL !ApplySpeed-$08

		LDA !sprite_misc_1528,x : BEQ ..gravity

		..waitfortimer
		LDA !sprite_misc_1540,x : BEQ DESPAWN
		BRA .Graphics

		..gravity
		INC !yspeed,x
		INC !yspeed,x
		INC !yspeed,x
		INC !animtimer,x


		.Graphics
		LDA !xlo,x : STA $00
		LDA !xhi,x : STA $01
		LDA !yhi,x : XBA
		LDA !ylo,x
		REP #$20
		SEC : SBC $1C
		STA $02				; optimization: write tile ypos here
		CMP #$00E0 : BCC ..goody
		CMP #$FFF0 : BCC DESPAWN
		..goody
		LDA $00
		SEC : SBC $1A
		STA $00				; optimization: write tile xpos here
		CMP #$0100 : BCC ..goodx
		CMP #$FFF0 : BCC DESPAWN
		..goodx
		SEP #$20


		.Draw
		LDA !sprite_misc_1528,x : BEQ ..loprio

		..hiprio
		LDY #$50
		..loop
		LDA !OAM+$001,y
		CMP #$F0 : BEQ ..thisone
		INY #4 : BNE ..loop
		..thisone
		LDA $00 : STA !OAM+$000,y
		LDA $02 : STA !OAM+$001,y
		LDA !sprite_misc_1528,x
		CMP #$01 : BEQ ..dust
		..smoke
		LDA !sprite_misc_1540,x
		LSR #2
		AND #$03
		TAX
		LDA.l $019A4E,x : BRA +

		..dust
		LDA !sprite_misc_1540,x : TAX
		LDA .DustTile,x
	+	STA !OAM+$002,y
		LDA #$30 : STA !OAM+$003,y
		TYA
		LSR #2
		TAY
		LDA $01
		AND #$01 : STA !OAMhi+$00,y
		LDX !spriteindex
		LDA !sprite_misc_1528,x
		CMP #$02 : BNE ..return
		LDA !OAMhi+$00,y
		ORA #$02 : STA !OAMhi+$00,y
		..return
		PLB
		RTL

		..loprio
		LDY !sprite_oam_index,x
		LDA $00 : STA !OAM+$100,y
		LDA $02 : STA !OAM+$101,y
		LDA !animtimer,x
		LSR #3
		TDC
		ADC #$3C
		STA !OAM+$102,y
		LDA #$30 : STA !OAM+$103,y
		TYA
		LSR #2
		TAY
		LDA $01
		AND #$01 : STA !OAMhi+$40,y
		LDX !spriteindex
		PLB
		RTL


	.DustTile
	db $66,$66,$66,$66,$64,$64,$64,$64,$62,$62,$62,$62








