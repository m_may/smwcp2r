
	; tank hitboxes
		.FrontSpike
		dw $FFEC,$FFDA : db $02,$0C

		.TopSpike
		dw $0012,$FFA2 : db $18,$2C

		.Glass
		dw $FFF6,$FFC0 : db $20,$1C

		.Pilot
		dw $FFF6,$FFC4 : db $20,$18

		; phase 1 only
		.Foot
		dw $FFF8,$FFE8 : db $2A,$28

		; phase 2 only
		.Jaw
		dw $FFF6,$FFEC : db $2A,$0C




	; norveg hitboxes
		.BodyHitbox
		dw $0002,$FFF2 : db $0C,$1E

		.BodyHitboxFull
		dw $0002,$FFEA : db $0C,$26

		.MeleeSightbox
		dw $FFD0,$FFF4 : db $30,$40

		.AttackHitbox
		dw $FFF0,$FFF0 : db $10,$20

		.CounterSightbox1
		dw $FFD0,$FF90 : db $40,$20

		.CounterSightbox3
		dw $FFD0,$FFC0 : db $40,$40

		.CounterHitbox1
		dw $FFEE,$FFA8 : db $28,$38

		.CounterHitbox3
		dw $FFE6,$FFC0 : db $40,$40

		.GunfireHitbox
		dw $FF80,$FFC0 : db $7F,$20



	; phase 2 arena block numbers
		.Phase2Arena
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
	;	dw $025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025,$025
		dw $025,$025,$025,$025,$025,$025,$025,$3EB6,$3EB7,$025,$025,$025,$025,$025,$025,$025
		dw $025,$025,$025,$025,$025,$025,$025,$3EB8,$3EB9,$025,$025,$025,$025,$3E44,$3E45,$025
		dw $025,$025,$3E56,$025,$3E58,$3E59,$3E5A,$3EBA,$3EBB,$3E58,$3E59,$3E5A,$025,$3E54,$3E55,$025
		dw $025,$3E66,$3E67,$025,$3E68,$3E69,$3E6A,$3EBC,$3EBD,$3E68,$3E69,$3E6A,$025,$3E64,$3E65,$025
		dw $3E01,$3E02,$3E01,$3E02,$3E01,$3E2B,$3E01,$3E02,$3E01,$3E02,$3E0A,$3E02,$3E01,$3E02,$3E01,$3E02
		dw $3E60,$3E61,$3E73,$3E74,$3E75,$3E1A,$3E60,$3E61,$3E62,$3E60,$3E1A,$3E76,$3E77,$3E78,$3E62,$3E60
		dw $3E70,$3E71,$3E83,$3E84,$3E85,$3E2A,$3E60,$3E61,$3E62,$3E60,$3E2A,$3E86,$3E87,$3E88,$3E62,$3E60





	; terrain chunk data
	.ChunkTable
	dw .RampRight
	dw .RampLeft
	dw .Flat1
	dw .Flat2
	dw .Flat3
	dw .PlatformType1
	dw .PlatformType2
	dw .PitType1
	..end

	.RampRight
	db ..end-..start
	..start
	db $A0 : dw $3E00
	db $B0 : dw $3E20
	db $C0 : dw $3E10
	db $D0 : dw $3E20
	db $A1 : dw $3E03
	db $B1 : dw $3E33
	db $C1 : dw $3E60
	db $D1 : dw $3E70
	db $B2 : dw $3E02
	db $C2 : dw $3E61
	db $D2 : dw $3E71
	db $B3 : dw $3E03
	db $C3 : dw $3E23
	db $D3 : dw $3E13
	..end

	.RampLeft
	db ..end-..start
	..start
	db $B0 : dw $3E00
	db $C0 : dw $3E20
	db $D0 : dw $3E10
	db $B1 : dw $3E02
	db $C1 : dw $3E60
	db $D1 : dw $3E70
	db $A2 : dw $3E00
	db $B2 : dw $3E30
	db $C2 : dw $3E61
	db $D2 : dw $3E71
	db $A3 : dw $3E03
	db $B3 : dw $3E23
	db $C3 : dw $3E13
	db $D3 : dw $3E23
	..end

	.Flat1
	db ..end-..start
	..start
	db $C0 : dw $025
	db $D0 : dw $3E00
	db $A1 : dw $3E44
	db $B1 : dw $3E54
	db $C1 : dw $3E64
	db $D1 : dw $3E01
	db $A2 : dw $3E45
	db $B2 : dw $3E55
	db $C2 : dw $3E65
	db $D2 : dw $3E02
	db $C3 : dw $025
	db $D3 : dw $3E03
	..end

	.Flat2
	db ..end-..start
	..start
	db $90 : dw $3E09
	db $A0 : dw $3E39
	db $B0 : dw $3E29
	db $C0 : dw $3E00
	db $D0 : dw $3E20
	db $A1 : dw $3E58
	db $B1 : dw $3E68
	db $C1 : dw $3E01
	db $D1 : dw $3E60
	db $A2 : dw $3E59
	db $B2 : dw $3E69
	db $C2 : dw $3E02
	db $D2 : dw $3E61
	db $A3 : dw $3E5A
	db $B3 : dw $3E6A
	db $C3 : dw $3E03
	db $D3 : dw $3E23
	..end

	.Flat3
	db ..end-..start
	..start
	db $B0 : dw $3E00
	db $C0 : dw $3E20
	db $D0 : dw $3E10
	db $A1 : dw $3E66
	db $B1 : dw $3E01
	db $C1 : dw $3E60
	db $D1 : dw $3E70
	db $A2 : dw $3E67
	db $B2 : dw $3E02
	db $C2 : dw $3E61
	db $D2 : dw $3E71
	db $B3 : dw $3E03
	db $C3 : dw $3E23
	db $D3 : dw $3E13
	..end

	.PlatformType1
	db ..end-..start
	..start
	db $C0 : dw $3E00
	db $D0 : dw $3E20
	db $81 : dw $24A
	db $C1 : dw $3E01
	db $D1 : dw $3E60
	db $82 : dw $24A
	db $C2 : dw $3E02
	db $D2 : dw $3E61
	db $C3 : dw $3E03
	db $D3 : dw $3E23
	..end

	.PlatformType2
	db ..end-..start
	..start
	db $80 : dw $24A
	db $C0 : dw $3E8F
	db $D0 : dw $3E00
	db $81 : dw $24A
	db $C1 : dw $025
	db $D1 : dw $3E01
	db $82 : dw $24A
	db $C2 : dw $025
	db $D2 : dw $3E02
	db $83 : dw $24A
	db $C3 : dw $3E8F
	db $D3 : dw $3E03
	..end

	.PitType1
	db ..end-..start
	..start
	db $B0 : dw $3E8F
	db $C0 : dw $3E8F
	db $D0 : dw $3E8F
	db $C1 : dw $025
	db $D1 : dw $025
	db $E1 : dw $025
	db $C2 : dw $025
	db $D2 : dw $025
	db $E2 : dw $025
	db $B3 : dw $3E8F
	db $C3 : dw $3E8F
	db $D3 : dw $3E8F
	..end

	.CleanerTable
	dw .CleanerChunk0
	dw .CleanerChunk1
	dw .CleanerChunk2
	dw .CleanerChunk3
	dw .CleanerChunk4
	dw $0000
	dw .CleanerChunk5
	dw .CleanerChunk6
	dw .CleanerChunk7
	dw .CleanerChunk8
	dw $0000
	dw .CleanerChunk9
	dw .CleanerChunkA
	dw .CleanerChunkB
	dw .CleanerChunkC
	dw $0000

	.CleanerChunk0
	db ..end-..start
	..start
	db $00 : dw $0025
	..end
	.CleanerChunk1
	db ..end-..start
	..start
	db $10 : dw $0025
	..end
	.CleanerChunk2
	db ..end-..start
	..start
	db $20 : dw $0025
	..end
	.CleanerChunk3
	db ..end-..start
	..start
	db $30 : dw $0025
	..end
	.CleanerChunk4
	db ..end-..start
	..start
	db $40 : dw $0025
	..end
	.CleanerChunk5
	db ..end-..start
	..start
	db $50 : dw $0025
	..end
	.CleanerChunk6
	db ..end-..start
	..start
	db $60 : dw $0025
	..end
	.CleanerChunk7
	db ..end-..start
	..start
	db $70 : dw $0025
	..end
	.CleanerChunk8
	db ..end-..start
	..start
	db $80 : dw $0025
	..end
	.CleanerChunk9
	db ..end-..start
	..start
	db $90 : dw $0025
	..end
	.CleanerChunkA
	db ..end-..start
	..start
	db $A0 : dw $0025
	..end
	.CleanerChunkB
	db ..end-..start
	..start
	db $B0 : dw $0025
	..end
	.CleanerChunkC
	db ..end-..start
	..start
	db $C0 : dw $0025
	..end




	.FlowerXSpeed
	db $20,$E0

