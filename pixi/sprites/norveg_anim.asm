
ResourceOffset:
	.Frame0
	dw $0000,$0400
	.Frame1
	dw $0400,$0800
	.Frame2
	dw $0800,$0C00
	.Frame3
	dw $0C00,$0DE0
	.Frame4
	dw $0FA0,$13A0
	.Frame5
	dw $13A0,$1760
	.Frame6
	dw $1760,$1900
	.Frame7
	dw $1A80,$1C40
	.Frame8
	dw $1E00,$1F80
	.Frame9
	dw $2100,$22E0
	.Frame10
	dw $24A0,$2860
	.Frame11
	dw $2860,$2A20
	.Frame12
	dw $2BE0,$3000
	.Frame13
	dw $3000,$3400
	.Frame14
	dw $3400,$37C0
	.Frame15
	dw $37C0,$39A0
	.Frame16
	dw $3B60,$3F20
	.Frame17
	dw $3F20,$40C0
	.Frame18
	dw $4240,$4400
	.Frame19
	dw $45C0,$4740
	.Frame20
	dw $48C0,$4AA0
	.Frame21
	dw $4C60,$5020
	.Frame22
	dw $5020,$51E0
	.Frame23
	dw $53A0,$5520
	.Frame24
	dw $56A0,$5AE0
	.Frame25
	dw $5B20,$5CC0
	.Frame26
	dw $5E40,$6020
	.Frame27
	dw $61E0,$6380
	.Frame28
	dw $6500,$6920
	.Frame29
	dw $6920,$6D40
	.Frame30
	dw $6D40,$6F20
ResourceSize:
	.Frame0
	db $20,$00
	.Frame1
	db $20,$00
	.Frame2
	db $20,$00
	.Frame3
	db $0F,$0E
	.Frame4
	db $20,$00
	.Frame5
	db $1E,$00
	.Frame6
	db $0D,$0C
	.Frame7
	db $0E,$0E
	.Frame8
	db $0C,$0C
	.Frame9
	db $0F,$0E
	.Frame10
	db $1E,$00
	.Frame11
	db $0E,$0E
	.Frame12
	db $21,$00
	.Frame13
	db $20,$00
	.Frame14
	db $1E,$00
	.Frame15
	db $0F,$0E
	.Frame16
	db $1E,$00
	.Frame17
	db $0D,$0C
	.Frame18
	db $0E,$0E
	.Frame19
	db $0C,$0C
	.Frame20
	db $0F,$0E
	.Frame21
	db $1E,$00
	.Frame22
	db $0E,$0E
	.Frame23
	db $0C,$0C
	.Frame24
	db $22,$02
	.Frame25
	db $0D,$0C
	.Frame26
	db $0F,$0E
	.Frame27
	db $0D,$0C
	.Frame28
	db $21,$00
	.Frame29
	db $21,$00
	.Frame30
	db $0F,$0E
ResourceLastRow:
	.Frame0
	db $30
	.Frame1
	db $30
	.Frame2
	db $30
	.Frame3
	db $10
	.Frame4
	db $30
	.Frame5
	db $30
	.Frame6
	db $10
	.Frame7
	db $10
	.Frame8
	db $10
	.Frame9
	db $10
	.Frame10
	db $30
	.Frame11
	db $10
	.Frame12
	db $40
	.Frame13
	db $30
	.Frame14
	db $30
	.Frame15
	db $10
	.Frame16
	db $30
	.Frame17
	db $10
	.Frame18
	db $10
	.Frame19
	db $10
	.Frame20
	db $10
	.Frame21
	db $30
	.Frame22
	db $10
	.Frame23
	db $10
	.Frame24
	db $30
	.Frame25
	db $10
	.Frame26
	db $10
	.Frame27
	db $10
	.Frame28
	db $40
	.Frame29
	db $40
	.Frame30
	db $10


macro FramesLength(id, next)
	dw Tiles_Frame<next>-Tiles_Frame<id>-1
endmacro

macro FramesStartPosition(id, next)
	dw Tiles_Frame<next>-Tiles-1
endmacro

macro FramesEndPosition(id)
	dw Tiles_Frame<id>-Tiles
endmacro


FramesLength:
	%FramesLength(0, 1)
	%FramesLength(1, 2)
	%FramesLength(2, 3)
	%FramesLength(3, 4)
	%FramesLength(4, 5)
	%FramesLength(5, 6)
	%FramesLength(6, 7)
	%FramesLength(7, 8)
	%FramesLength(8, 9)
	%FramesLength(9, 10)
	%FramesLength(10, 11)
	%FramesLength(11, 12)
	%FramesLength(12, 13)
	%FramesLength(13, 14)
	%FramesLength(14, 15)
	%FramesLength(15, 16)
	%FramesLength(16, 17)
	%FramesLength(17, 18)
	%FramesLength(18, 19)
	%FramesLength(19, 20)
	%FramesLength(20, 21)
	%FramesLength(21, 22)
	%FramesLength(22, 23)
	%FramesLength(23, 24)
	%FramesLength(24, 25)
	%FramesLength(25, 26)
	%FramesLength(26, 27)
	%FramesLength(27, 28)
	%FramesLength(28, 29)
	%FramesLength(29, 30)
	%FramesLength(30, 31)

FramesStartPosition:
	%FramesStartPosition(0, 1)
	%FramesStartPosition(1, 2)
	%FramesStartPosition(2, 3)
	%FramesStartPosition(3, 4)
	%FramesStartPosition(4, 5)
	%FramesStartPosition(5, 6)
	%FramesStartPosition(6, 7)
	%FramesStartPosition(7, 8)
	%FramesStartPosition(8, 9)
	%FramesStartPosition(9, 10)
	%FramesStartPosition(10, 11)
	%FramesStartPosition(11, 12)
	%FramesStartPosition(12, 13)
	%FramesStartPosition(13, 14)
	%FramesStartPosition(14, 15)
	%FramesStartPosition(15, 16)
	%FramesStartPosition(16, 17)
	%FramesStartPosition(17, 18)
	%FramesStartPosition(18, 19)
	%FramesStartPosition(19, 20)
	%FramesStartPosition(20, 21)
	%FramesStartPosition(21, 22)
	%FramesStartPosition(22, 23)
	%FramesStartPosition(23, 24)
	%FramesStartPosition(24, 25)
	%FramesStartPosition(25, 26)
	%FramesStartPosition(26, 27)
	%FramesStartPosition(27, 28)
	%FramesStartPosition(28, 29)
	%FramesStartPosition(29, 30)
	%FramesStartPosition(30, 31)

FramesEndPosition:
	%FramesEndPosition(0)
	%FramesEndPosition(1)
	%FramesEndPosition(2)
	%FramesEndPosition(3)
	%FramesEndPosition(4)
	%FramesEndPosition(5)
	%FramesEndPosition(6)
	%FramesEndPosition(7)
	%FramesEndPosition(8)
	%FramesEndPosition(9)
	%FramesEndPosition(10)
	%FramesEndPosition(11)
	%FramesEndPosition(12)
	%FramesEndPosition(13)
	%FramesEndPosition(14)
	%FramesEndPosition(15)
	%FramesEndPosition(16)
	%FramesEndPosition(17)
	%FramesEndPosition(18)
	%FramesEndPosition(19)
	%FramesEndPosition(20)
	%FramesEndPosition(21)
	%FramesEndPosition(22)
	%FramesEndPosition(23)
	%FramesEndPosition(24)
	%FramesEndPosition(25)
	%FramesEndPosition(26)
	%FramesEndPosition(27)
	%FramesEndPosition(28)
	%FramesEndPosition(29)
	%FramesEndPosition(30)


Tiles:
	.Frame0
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame1
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame2
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame3
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame4
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame5
	db $0C,$0A,$08,$06,$0F,$04,$02,$00,$0E
	.Frame6
	db $0C,$0A,$08,$06,$04,$02,$00
	.Frame7
	db $0C,$0A,$08,$06,$04,$02,$00
	.Frame8
	db $0A,$08,$06,$04,$02,$00
	.Frame9
	db $0C,$0E,$0A,$08,$06,$04,$02,$00
	.Frame10
	db $0C,$0A,$08,$0F,$06,$04,$02,$00,$0E
	.Frame11
	; cut: 2, 5
	db $0C,$0A,$08,$06,$04,$02,$00
;	db $0C,$0A,$06,$04,$00
	.Frame12
	; cut: 2, 5, 6
	db $0E,$0C,$0A,$08,$06,$20,$04,$02,$00
;	db $0E,$0C,$08,$06,$02,$00
	.Frame13
	; cut: 2, 5
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
;	db $0E,$0C,$08,$06,$02,$00
	.Frame14
	db $0C,$0F,$0A,$08,$06,$04,$02,$00,$0E
	.Frame15
	db $0C,$0E,$0A,$08,$06,$04,$02,$00
	.Frame16
	db $0C,$0F,$0E,$0A,$08,$06,$04,$02,$00
	.Frame17
	db $0C,$0A,$08,$06,$04,$02,$00
	.Frame18
	db $0C,$0A,$08,$06,$04,$02,$00
	.Frame19
	db $0A,$08,$06,$04,$02,$00
	.Frame20
	db $0C,$0A,$0E,$08,$06,$04,$02,$00
	.Frame21
	db $0F,$0C,$0A,$08,$06,$04,$02,$00,$0E
	.Frame22
	db $0C,$0A,$08,$06,$04,$02,$00
	.Frame23
	db $0A,$08,$06,$04,$02,$00
	.Frame24
	db $20,$0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame25
	db $0A,$08,$06,$04,$02,$00,$0C
	.Frame26
	db $0C,$0E,$0A,$08,$06,$04,$02,$00
	.Frame27
	db $0A,$08,$06,$04,$02,$00,$0C
	.Frame28
	db $0E,$20,$0C,$0A,$08,$06,$04,$02,$00
	.Frame29
	db $20,$0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame30
	db $0E,$0C,$0A,$08,$06,$04,$02,$00
	.Frame31	; used by macro, don't touch

XDisplacements:
	.Frame0
	db $EE,$F1,$FA,$FE,$FE,$0A,$0E,$0E
	.Frame1
	db $ED,$F1,$FA,$FD,$FD,$0A,$0D,$0D
	.Frame2
	db $ED,$EF,$F9,$FD,$FE,$09,$0D,$0E
	.Frame3
	db $EF,$F2,$F7,$F9,$01,$07,$09,$11
	.Frame4
	db $EF,$F2,$F9,$FF,$FF,$09,$0F,$0F
	.Frame5
	db $F1,$F3,$F9,$FA,$03,$04,$09,$09,$19
	.Frame6
	db $F2,$F7,$F8,$FF,$03,$07,$08
	.Frame7
	db $F1,$F6,$F6,$01,$06,$06,$11
	.Frame8
	db $F6,$F9,$FA,$04,$06,$09
	.Frame9
	db $EE,$F1,$F4,$F9,$FE,$04,$09,$0E
	.Frame10
	db $F2,$F2,$F4,$02,$02,$04,$0A,$0E,$14
	.Frame11
	; cut: 2, 5
	db $F2,$F7,$F9,$01,$07,$09,$11
;	db $F2,$F7,$01,$07,$11
	.Frame12
	; cut: 2, 5, 6
	db $EF,$F0,$F5,$FF,$00,$05,$0C,$0F,$10
;	db $EF,$F0,$FF,$00,$0F,$10
	.Frame13
	; cut: 2, 5
	db $EE,$F0,$FA,$FE,$FF,$0A,$0E,$0E
;	db $EE,$F0,$FE,$FF,$0E,$0E
	.Frame14
	db $EF,$F4,$F7,$FB,$FF,$03,$0B,$0F,$13
	.Frame15
	db $EF,$F4,$F8,$F9,$FB,$08,$09,$0B
	.Frame16
	db $EF,$F4,$F7,$FA,$FD,$FF,$0A,$0A,$0D
	.Frame17
	db $F2,$F7,$F8,$FF,$03,$07,$08
	.Frame18
	db $F1,$F6,$F6,$01,$06,$06,$11
	.Frame19
	db $F6,$F9,$FA,$04,$06,$09
	.Frame20
	db $F6,$F9,$FA,$FF,$02,$09,$0F,$12
	.Frame21
	db $F0,$F1,$F6,$F8,$01,$06,$08,$11,$16
	.Frame22
	db $F1,$F9,$FA,$01,$09,$0A,$11
	.Frame23
	db $F7,$F8,$F9,$03,$07,$08
	.Frame24
	db $EF,$F0,$F6,$FF,$00,$06,$0F,$10,$16
	.Frame25
	db $EE,$FA,$FE,$00,$0A,$0E,$10
	.Frame26
	db $F1,$F3,$F7,$FB,$01,$07,$0B,$11
	.Frame27
	db $EE,$FA,$FE,$00,$0A,$0E,$10
	.Frame28
	db $EF,$F7,$F9,$FF,$FF,$09,$0F,$0F,$11
	.Frame29
	db $EF,$F3,$F6,$FB,$FB,$03,$0B,$0B,$13
	.Frame30
	db $EF,$F3,$FC,$00,$03,$0C,$10,$13

YDisplacements:
	.Frame0
	db $EC,$F9,$01,$E1,$F1,$01,$E4,$F4
	.Frame1
	db $ED,$FC,$01,$E1,$F1,$01,$E4,$F4
	.Frame2
	db $FA,$ED,$01,$F2,$E2,$01,$F2,$E5
	.Frame3
	db $F4,$F9,$F1,$01,$E1,$F1,$01,$E4
	.Frame4
	db $ED,$FD,$01,$E1,$F1,$01,$E4,$F4
	.Frame5
	db $F6,$01,$F4,$E4,$04,$E1,$F1,$01,$F7
	.Frame6
	db $F6,$FF,$F0,$E1,$01,$F3,$E4
	.Frame7
	db $F1,$E2,$01,$F2,$E2,$01,$F2
	.Frame8
	db $F5,$01,$E5,$E2,$F2,$02
	.Frame9
	db $EA,$FA,$01,$F1,$E2,$FE,$F0,$E4
	.Frame10
	db $F2,$02,$E4,$F2,$F9,$E2,$F2,$02,$E8
	.Frame11
	; cut: 2, 5
	db $F9,$F1,$01,$E1,$F1,$01,$E4
;	db $F9,$F1,$E1,$F1,$E4
	.Frame12
	; cut: 2, 5, 6
	db $F5,$E5,$01,$F1,$E1,$01,$01,$F1,$E5
;	db $F5,$E5,$F1,$E1,$F1,$E5
	.Frame13
	; cut: 2, 5
	db $FA,$EA,$01,$F1,$E2,$01,$E5,$F5
;	db $FA,$EA,$F1,$E2,$E5,$F5
	.Frame14
	db $F2,$02,$E2,$01,$F1,$E1,$01,$F1,$EF
	.Frame15
	db $F3,$03,$F1,$E1,$01,$F1,$E1,$01
	.Frame16
	db $F4,$04,$EC,$01,$E2,$F1,$F2,$02,$E5
	.Frame17
	db $F6,$FF,$F0,$E1,$01,$F4,$E4
	.Frame18
	db $F1,$E2,$01,$F2,$E2,$01,$F1
	.Frame19
	db $F5,$01,$E5,$E2,$F2,$02
	.Frame20
	db $F7,$01,$EF,$E2,$F1,$01,$E5,$F5
	.Frame21
	db $FF,$F1,$E2,$01,$F1,$E2,$01,$F2,$ED
	.Frame22
	db $F0,$FF,$E0,$F0,$FF,$E0,$F0
	.Frame23
	db $E1,$FD,$ED,$01,$E1,$F1
	.Frame24
	db $01,$F1,$E1,$01,$F1,$E1,$01,$F1,$E5
	.Frame25
	db $F1,$01,$F1,$E8,$01,$F1,$EB
	.Frame26
	db $E9,$F9,$01,$F2,$E2,$FE,$F0,$E4
	.Frame27
	db $F1,$01,$F1,$E8,$01,$F1,$EB
	.Frame28
	db $F9,$09,$E3,$F1,$01,$E3,$F1,$01,$E2
	.Frame29
	db $F9,$01,$E1,$E2,$F2,$01,$E1,$F1,$01
	.Frame30
	db $F9,$FE,$F0,$E2,$FA,$F2,$E5,$02

Sizes:
	.Frame0
	db $02,$02,$02,$02,$02,$02,$02,$02
	.Frame1
	db $02,$02,$02,$02,$02,$02,$02,$02
	.Frame2
	db $02,$02,$02,$02,$02,$02,$02,$02
	.Frame3
	db $00,$02,$02,$02,$02,$02,$02,$02
	.Frame4
	db $02,$02,$02,$02,$02,$02,$02,$02
	.Frame5
	db $02,$02,$02,$02,$00,$02,$02,$02,$00
	.Frame6
	db $00,$02,$02,$02,$02,$02,$02
	.Frame7
	db $02,$02,$02,$02,$02,$02,$02
	.Frame8
	db $02,$02,$02,$02,$02,$02
	.Frame9
	db $02,$00,$02,$02,$02,$02,$02,$02
	.Frame10
	db $02,$02,$02,$00,$02,$02,$02,$02,$00
	.Frame11
	; cut: 2, 5
	db $02,$02,$02,$02,$02,$02,$02
;	db $02,$02,$02,$02,$02
	.Frame12
	; cut: 2, 5, 6
	db $02,$02,$02,$02,$02,$00,$02,$02,$02
;	db $02,$02,$02,$02,$02,$02
	.Frame13
	; cut: 2, 5
	db $02,$02,$02,$02,$02,$02,$02,$02
;	db $02,$02,$02,$02,$02,$02
	.Frame14
	db $02,$00,$02,$02,$02,$02,$02,$02,$00
	.Frame15
	db $02,$00,$02,$02,$02,$02,$02,$02
	.Frame16
	db $02,$00,$00,$02,$02,$02,$02,$02,$02
	.Frame17
	db $00,$02,$02,$02,$02,$02,$02
	.Frame18
	db $02,$02,$02,$02,$02,$02,$02
	.Frame19
	db $02,$02,$02,$02,$02,$02
	.Frame20
	db $02,$02,$00,$02,$02,$02,$02,$02
	.Frame21
	db $00,$02,$02,$02,$02,$02,$02,$02,$00
	.Frame22
	db $02,$02,$02,$02,$02,$02,$02
	.Frame23
	db $02,$02,$02,$02,$02,$02
	.Frame24
	db $02,$02,$02,$02,$02,$02,$02,$02,$02
	.Frame25
	db $02,$02,$02,$02,$02,$02,$00
	.Frame26
	db $02,$00,$02,$02,$02,$02,$02,$02
	.Frame27
	db $02,$02,$02,$02,$02,$02,$00
	.Frame28
	db $02,$00,$02,$02,$02,$02,$02,$02,$02
	.Frame29
	db $00,$02,$02,$02,$02,$02,$02,$02,$02
	.Frame30
	db $00,$02,$02,$02,$02,$02,$02,$02



macro animlength(name)
	dw .<name>_end-.<name>
endmacro

macro animindex(name)
	dw .<name>-.Data
endmacro

Animation:

	.Index
	%animindex(Idle)
	%animindex(Shoot)
	%animindex(Walk)
	%animindex(Jump)
	%animindex(Hurt)
	%animindex(ThrowGrenade)
	%animindex(Idle2)
	%animindex(Walk2)
	%animindex(Claw)
	%animindex(Claw2)
	%animindex(Jump2)
	%animindex(Throw)

	.Length
	%animlength(Idle)
	%animlength(Shoot)
	%animlength(Walk)
	%animlength(Jump)
	%animlength(Hurt)
	%animlength(ThrowGrenade)
	%animlength(Idle2)
	%animlength(Walk2)
	%animlength(Claw)
	%animlength(Claw2)
	%animlength(Jump2)
	%animlength(Throw)


	.LastTransition
	..idle
	dw $0000
	..shoot
	dw $0001
	..walk
	dw $0000
	..jump
	dw $0000
	..hurt
	dw $0000
	..throwgrenade
	dw $0002
	..idle2
	dw $0000
	..walk2
	dw $0000
	..claw
	dw $0000
	..claw2
	dw $0002
	..jump2
	dw $0000
	..throw
	dw $0003


	.Data
	.Idle
	db $00,$01,$02,$01
	..end
	.Shoot
	db $03,$04,$05
	..end
	.Walk
	db $06,$07,$06,$08
	..end
	.Jump
	db $09
	..end
	.Hurt
	db $0A
	..end
	.ThrowGrenade
	db $0B,$0C,$0D
	..end
	.Idle2
	db $0E,$0F,$10,$0F
	..end
	.Walk2
	db $11,$12,$11,$13
	..end
	.Claw
	db $16,$14,$16,$15
	..end
	.Claw2
	db $17,$18,$19
	..end
	.Jump2
	db $1A
	..end
	.Throw
	db $1B,$1C,$1D,$1E
	..end


	.Time
	..idle
	db $08,$08,$08,$08
	..shoot
	db $08,$02,$02
	..walk
	db $08,$08,$08,$08
	..jump
	db $FF
	..hurt
	db $FF
	..throwgrenade
	db $08,$04,$08
	..idle2
	db $04,$04,$04,$04
	..walk2
	db $08,$08,$08,$08
	..claw
	db $06,$06,$06,$06
	..claw2
	db $08,$08,$08
	..jump2
	db $FF
	..throw
	db $08,$04,$18,$FF

