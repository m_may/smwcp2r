

	; stuff you should actually mess with
		!BossNum		= $00
		!ProjectileNum		= $01
		!ParticleNum		= $04



	; routines
		!GetMarioClipping	= $03B664|!BankB
		!GetSpriteClipping04	= $03B69F|!BankB
		!GetSpriteClipping00	= $03B6E5|!BankB
		!CheckContact		= $03B72B|!BankB

		!ApplySpeed		= $01802A|!BankB

		!HurtMario		= $00F5B7|!BankB

	; RAM
		!OAM			= $0200|!addr
		!OAMhi			= $0420|!addr

		!spriteindex		= $15E9|!addr


		!SPC1			= $1DF9|!addr
		!SPC2			= $1DFA|!addr
		!SPC3			= $1DFB|!addr
		!SPC4			= $1DFC|!addr



	; mario RAM
		!marioanim		= $71

		!marioxspeed		= $7B
		!marioyspeed		= $7D

		!mariox			= $94
		!marioy			= $96

		!mariospinjump		= $140D|!addr


	; sprite regs
		!x_lo			= !sprite_x_low
		!x_hi			= !sprite_x_high
		!y_lo			= !sprite_y_low
		!y_hi			= !sprite_y_high
		!xlo			= !x_lo
		!xhi			= !x_hi
		!ylo			= !y_lo
		!yhi			= !y_hi


		!xspeed			= !sprite_speed_x
		!yspeed			= !sprite_speed_y

		!dir			= !sprite_misc_157c

		!nobouncetimer		= !sprite_misc_1504

		!animindex		= !sprite_misc_1510
		!animtimer		= !sprite_misc_151c
		!attack			= !sprite_misc_1528
		!attacktimer		= !sprite_misc_1540


		!blocked		= !sprite_blocked_status





	print "INIT ", pc
	INIT:
		RTL





	print "MAIN ", pc
	MAIN:
		PHB : PHK : PLB
		LDA $9D : BEQ .Process
		JMP GRAPHICS
		.Process

		.OffscreenCheck
		LDA !xlo,x : STA $00
		LDA !xhi,x : STA $01
		LDA !yhi,x : XBA
		LDA !ylo,x
		REP #$20
		SEC : SBC $1C
		CMP #$00E0 : BCC ..goody
		CMP #$FFF0 : BCS ..goody
		..kill
		SEP #$20
		STZ !sprite_status,x
		PLB
		RTL
		..goody
		LDA $00
		SEC : SBC $1A
		CMP #$0100 : BCC ..goodx
		CMP #$FFF0 : BCC ..kill
		..goodx
		SEP #$20



	PHYSICS:
		.GetThrown
		LDA !attacktimer,x
		CMP #$01 : BNE .NoThrow
		LDA !x_lo,x : STA $00
		LDA !x_hi,x : STA $01
		LDA !y_hi,x : XBA
		LDA !y_lo,x
		REP #$20
		SEC : SBC !marioy
		SEC : SBC #$0010
		STA $02
		LDA $00
		SEC : SBC !mariox
		STA $00
		SEP #$20
		LDA #$60 : %Aiming()
		LDA #$0F : %Random()
		SBC #$08
		ADC $00
		STA !xspeed,x
		LDA #$0F : %Random()
		SBC #$08
		ADC $02
		STA !yspeed,x
		INC !sprite_misc_1528,x
		LDA #$10 : STA !SPC1			; throw sfx
		.NoThrow


		; skip gravity
		LDA !sprite_misc_1528,x : BEQ +
		DEC !yspeed,x
		DEC !yspeed,x
		DEC !yspeed,x
		+
		JSL !ApplySpeed


		LDA !blocked,x
		AND #$0F : BEQ .NoTerrainContact
		LDA #$04 : STA !sprite_status,x
		LDA #$1F : STA !sprite_misc_1540,x
		LDA #$09 : STA !SPC4			; boom sfx
		.NoTerrainContact




	INTERACTION:
		LDA !marioanim : BNE .NoContact
		LDA !x_lo,x : STA $04
		LDA !x_hi,x : STA $0A
		LDA !y_lo,x : STA $05
		LDA !y_hi,x : STA $0B
		LDA #$10
		STA $06
		STA $07
		JSL !GetMarioClipping
		JSL !CheckContact : BCC .NoContact
		LDA !mariospinjump : BEQ .HurtMario
		LDA $0B : XBA
		LDA $05
		REP #$20
		SEC : SBC #$0010
		CMP !marioy
		SEP #$20
		BCC .HurtMario
		.Bounce
		LDA #$C8
		BIT $15
		BPL $02 : LDA #$A0
		STA !marioyspeed
		STZ !xspeed,x
		STZ !yspeed,x
		STZ !sprite_misc_1528,x
		LDA #$02 : STA !SPC1			; spin jump contact sfx
		JSR ContactGFX
		BRA .NoContact
		.HurtMario
		JSL !HurtMario
		.NoContact






; scratch RAM use for tilemap loader
;
; $00 - sprite X pos (16-bit)
; $02 - sprite Y pos (16-bit)
; $04 - tilemap pointer
; $06 - xflip flag
; $08 - number of bytes to read from tilemap
; $0A
; $0C - tile X position
; $0E - tile Y position

	GRAPHICS:
		REP #$20
		LDA.w #.Tilemap : STA $04
		LDA ($04) : STA $08
		INC $04
		LDA $14
		AND #$000C
		CLC : ADC $04
		STA $04
		SEP #$30

		LDA !dir,x
		BEQ $02 : LDA #$40
		EOR #$40
		STA $06

		LDA !x_lo,x : STA $00
		LDA !x_hi,x : STA $01
		LDA !y_lo,x : STA $02
		LDA !y_hi,x : STA $03



	; X = OAM index
	; Y = tilemap table index

		.FindSlot
		LDX #$00
		..loop
		LDA !OAM+$001,x
		CMP #$F0 : BEQ ..thisone
		INX #4 : BNE ..loop
		LDX #$00
		..thisone
		LDY #$00


	; loop starts here

		.Loop
		REP #$20
		LDA ($04),y
		INY
		AND #$00FF
		CMP #$0080
		BCC $03 : ORA #$FF00
		BIT $06-1
		BVC $04 : EOR #$FFFF : INC A
		CLC : ADC $00
		SEC : SBC $1A
		CMP #$0100 : BCC ..goodx
		CMP #$FFF0 : BCS ..goodx

		..badcoord
		INY #3
		SEP #$20
		JMP .LoopCheck

		..goodx
		STA $0C
		LDA ($04),y
		AND #$00FF
		CMP #$0080
		BCC $03 : ORA #$FF00
		CLC : ADC $02
		SEC : SBC $1C
		CMP #$00E0 : BCC ..goody
		CMP #$FFF0 : BCC ..badcoord
		..goody
		INY
		STA $0E

		.DrawTile
		SEP #$20
		LDA ($04),y : STA !OAM+$002,x
		INY
		LDA ($04),y
		INY
		EOR $06
		STA !OAM+$003,x
		LDA $0C : STA !OAM+$000,x
		LDA $0E : STA !OAM+$001,x
		TXA
		LSR #2
		TAX
		LDA $0D
		AND #$01
		ORA #$02
		STA !OAMhi+$00,x
		INX
		TXA
		ASL #2
		TAX

		.LoopCheck
		CPY $08 : BCS .Return
		JMP .Loop

		.Return
		LDX !spriteindex
		PLB
		RTL

	.Tilemap:
	db ..end-..start
	..start
	db $00,$00,$02,$75
	..end
	db $00,$00,$02,$77
	db $00,$00,$04,$75
	db $00,$00,$04,$77


; input: void
; outut: void
	ContactGFX:
		LDY #$03
		.Loop
		LDA $17C0|!Base2,y : BEQ .ThisOne
		DEY : BPL .Loop
		RTS
		.ThisOne
		LDA #$02 : STA $17C0|!Base2,y
		LDA #$07 : STA $17CC|!Base2,y
		LDA !mariox : STA $17C8|!Base2,y
		LDA !marioy
		CLC : ADC #$10
		STA $17C4|!Base2,y
		RTS


