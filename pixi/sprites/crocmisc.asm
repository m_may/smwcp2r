!SubType = !C2
!Type = !1528
!Graphics = !1602
!GfxFlip = !160E

!BombExplosionSfx = $16
!BombExplosionPort = $1DFC

!ShockwaveInitSpeed = $40
!ShockwaveFinalSpeed = $30
!ShockwaveTopGfx = $A0
!ShockwaveBottomGfx = $C0
!ShockwaveProperties = $4F

!VialBrokenSfx = $06
!VialBrokenPort = $1DF9
!VialShroomSfx = $02
!VialShroomPort = $1DFC

!CrocProperties = $0D

SingleTiles:
dw $0F80,$098E,$09AE,$4F82,$4F84

CrocTiles:
db $44,$46,$64,$66
db $46,$44,$66,$64
db $64,$66,$44,$46
db $66,$64,$46,$44

print "INIT ",pc
RTL

print "MAIN ",pc
	PHB : PHK : PLB
	JSR CrocMisc
	PLB
RTL

CrocMisc:
	JSR Graphics

	LDA !14C8,x
	EOR #$08
	ORA $9D
	BNE .Return

	%SubOffScreen()

	JSL $01801A|!bank
	JSL $018022|!bank

	TXY
	LDX !Type,y
	JSR (.Projectiles,x)

	LDX $15E9|!addr
.Return:
RTS

.Projectiles
dw Bomb
dw Shockwave
dw Vial
dw Drone
dw DocCroc

Bomb:
	TYX
	JSR HandleGravity
	JSL $019138|!bank
	LDA !1588,x		; On ground?
	AND #$04
	BEQ .Return

	; Explode the bomb
	STX $0C			; I hate you
	LDA !7FAB9E,x	; I hate you even more
	STA $0D
	LDA #$0D
	STA !9E,x		; Bob-Omb
	LDA #$08
	STA !14C8,x		; Alive
	JSL $07F7D2|!bank
	LDA #!BombExplosionSfx
	STA !BombExplosionPort|!addr
	LDA #$40
	STA !1540,x
	LDA #$01
	STA !1534,x
	LDA !1686,x
	AND #$F7
	STA !1686,x

	; Shockwave right
	JSR .SpawnShockwave
	BCC .Return
	LDA #!ShockwaveInitSpeed
	STA !B6,y		; Initial speed
	LDA #$00
	STA !157C,y		; Direction (right)

	; Shockwave left
	JSR .SpawnShockwave
	BCC .Return
	LDA #-!ShockwaveInitSpeed
	STA !B6,y		; Initial speed
	LDA #$01
	STA !157C,y		; Direction (left)
.Return
RTS

.SpawnShockwave
	JSL $02A9E4|!bank
	BMI ..Return
	TYX
	LDA #$08
	STA !14C8,x
	JSL $07F7D2|!bank
	LDA $0D
	STA !7FAB9E,x
	LDA #$08
	STA !7FAB10,x
	JSL $0187A7|!bank
	TXY
	LDX $0C
	LDA !D8,x
	AND #$F0		; Fixate on ground height.
	STA !D8,y
	LDA !14D4,x
	STA !14D4,y
	LDA !E4,x
	STA !E4,y
	LDA !14E0,x
	STA !14E0,y
	LDA #$02
	STA !Type,y

	LDA #$00
	STA !1656,y		; Object clipping
	LDA #$00
	STA !1662,y		; Sprite clipping
	SEC
RTS

..Return
	CLC
RTS

Shockwave:
	TYX
	LDY !157C,x
	TYA
	STA !GfxFlip,x
	LDA !B6,x
	SEC : SBC .MaxSpeed,y
	EOR .MaxSpeed,y
	BMI .FinalSpeed
	LDA !B6,x
	CLC : ADC .Accell,y
	BRA .SetSpeed
.FinalSpeed
	LDA .MaxSpeed,y
.SetSpeed
	STA !B6,x

	LDA #$40
	STA !AA,x

	JSL $01A7DC|!bank	; Interaction with player
	JSL $019138|!bank	; Interaction with blocks

	LDA !1588,x
	AND #$24
	BNE .OnGround
	LDA !1504,x		; Is 1 when in air
	BEQ .JustFellFromGround
.StillInAir
	DEC !1534,x 
	BNE .Return
	STZ !14C8,x			; Don't exist anymore.
.Return
RTS
.JustFellFromGround
	INC !1504,x
	LDA #$04
	STA !1534,x
RTS
.OnGround
	STZ !1504,x
RTS

.MaxSpeed
db !ShockwaveFinalSpeed,-!ShockwaveFinalSpeed

.Accell
db $FF,$01

Vial:
	TYX
	JSR HandleGravity
	JSL $019138|!bank

	LDA $14				; Rotate the vial
	LSR
	AND #$03
	TAY
	LDA .VialFrame,y
	STA !Graphics,x
	LDA .VialFlip,y
	STA !GfxFlip,x

	LDA !SubType,x
	BNE +
	JSL $01A7DC|!bank	; Interaction
	BCC +
	JSL $00F5B7|!bank
+	LDA !1588,x			; On ground?
	BEQ .Return
	STZ !14C8,x
	LDY !SubType,x
	BEQ .Fire
	
	JSL $02A9E4|!bank
	BMI .Return
	
	TYX
	LDA #$08
	STA !14C8,x
	LDA #$74
	STA !9E,x
	JSL $07F7D2|!bank
	LDX $15E9|!addr
	LDA !D8,x
	STA !D8,y
	LDA !E4,x
	STA !E4,y
	LDA !14D4,x
	STA !14D4,y
	LDA !14E0,x
	STA !14E0,y
	LDA #$10
	STA !AA,y

	LDA #!VialShroomSfx
	STA !VialShroomPort
.Return:
RTS

.Fire
	LDY #$00
	LSR					; Right wall
	BCS +
	INY #2
	LSR					; Left wall
	BCS +
	INY #2
	LSR					; Ground
	BCS +
	INY #2
+	CPY #$06
	BEQ .NotBroken
	LDA .YSpeed,y
	STA $0C
	LDA .YSpeed+1,y
	STA $0D
	LDA .XSpeed,y
	STA $0E
	LDA .XSpeed+1,y
	STA $0F

	JSR .SpawnFire
	BCC .NotBroken
	LDA $0C
	STA $173D|!addr,y	; Vertical speed
	LDA $0E
	STA $1747|!addr,y	; Horizontal speed

	LDA #!VialBrokenSfx
	STA !VialBrokenPort|!addr

	JSR .SpawnFire
	BCC .NotBroken
	LDA $0D
	STA $173D|!addr,y	; Vertical speed
	LDA $0F
	STA $1747|!addr,y	; Horizontal speed

.NotBroken:
RTS

.SpawnFire:
	LDY #$07
-	LDA $170B|!addr,y
	BNE .NextSlot
	LDA #$0B
	STA $170B|!addr,y	; Jumping Piranha Fire

	; Spawn fire at centre
	LDA !D8,x
	CLC : ADC #$08
	STA $1715|!addr,y
	LDA !E4,x
	CLC : ADC #$08
	STA $171F|!addr,y
	LDA !14D4,x
	ADC #$00
	STA $1729|!addr,y
	LDA !14E0,x
	ADC #$00
	STA $1733|!addr,y
	LDA #$06
	STA $1DF9|!addr
	SEC
RTS

.NextSlot:
	DEY
	BPL -
	CLC
RTS

.YSpeed:
db $00,$F0,$00,$F0,$D0,$D0

.XSpeed:
db $E0,$F0,$20,$10,$18,$E8

.VialFrame
db $02,$04,$02,$04

.VialFlip:
db $00,$00,$01,$01

Drone:
	TYX
	
	LDA $14
	LSR
	BCC +
	LDA !Graphics,x
	EOR #$0E		; Flips between 6 and 8.
	STA !Graphics,x
+

	JSL $01A7DC|!bank	; Interaction

	; Get full speed
	LDA !14E0,x
	XBA
	LDA !E4,x
	REP #$20
	SEC : SBC $D1
	PHA
	STA $00

	SEP #$20
	LDA !14D4,x
	XBA
	LDA !D8,x
	REP #$20
	SEC : SBC $D3
	PHA
	STA $02
	SEP #$20
	
	LDA #$10
	%Aiming()
	LDA $00
	STA $8A
	LDA $02
	STA $8B
	
	; Get accelleration
	REP #$20
	PLA
	STA $02
	PLA
	STA $00
	SEP #$20
	
	LDA #$02
	%Aiming()

	LDA $8A
	CLC : ADC $00
	SEC : SBC $8A
	EOR $8A
	BMI .TooFastHorizontal
	EOR $8A
	CLC : ADC $8A
	STA !B6,x
.TooFastHorizontal:

	LDA $8B
	CLC : ADC $02
	SEC : SBC $8B
	EOR $8B
	BMI .TooFastVertical
	EOR $8B
	CLC : ADC $8B
	STA !AA,x
.TooFastVertical:

	%SubHorzPos()
	TYA
	STA !157C,x
	STA !GfxFlip,x
RTS

DocCroc:
	TYX
	LDA !AA,x		; No speed limit.
	JSR HandleGravity_Falling
	LDA !AA,x
	BMI +
	LDA #$02
	STA !GfxFlip,x
+
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Graphics
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:
	%GetDrawInfo()

	LDA !GfxFlip,x
	LSR
	ROR #2
	STA $05
	STZ $04

	LDA !Type,x
	CMP #$02
	BEQ .Shockwave
	CMP #$08
	BEQ .DocCroc

	LDA !Graphics,x
	TAX

	REP #$20
	LDA $00
	STA $0300|!addr,y
	LDA SingleTiles,x
	EOR $04
	STA $0302|!addr,y
	SEP #$20

	LDA #$00
.Shared:
	LDX $15E9|!addr
	LDY #$02
	JSL $01B7B3|!bank
RTS

.Shockwave:
	LDA $00
	STA $0300|!addr,y
	STA $0304|!addr,y
	LDA $01
	STA $0305|!addr,y
	CLC : ADC #$F0
	STA $0301|!addr,y
	LDA #!ShockwaveTopGfx
	STA $0302|!addr,y
	LDA #!ShockwaveBottomGfx
	STA $0306|!addr,y
	LDA $05
	EOR #!ShockwaveProperties
	STA $0303|!addr,y
	STA $0307|!addr,y
	SEP #$20
	LDA #$01
BRA .Shared

.Flip:
db $5F,$1F

.DocCroc:
	LDA #$F8
	CLC : ADC $00
	STA $0300|!addr,y
	STA $0308|!addr,y
	CLC : ADC #$10
	STA $0304|!addr,y
	STA $030C|!addr,y

	LDA #$F8
	CLC : ADC $01
	STA $0301|!addr,y
	STA $0305|!addr,y
	CLC : ADC #$10
	STA $0309|!addr,y
	STA $030D|!addr,y

	LDA !GfxFlip,x
	ASL #2
	TAX
	LDA CrocTiles+0,x
	STA $0302|!addr,y
	LDA CrocTiles+1,x
	STA $0306|!addr,y
	LDA CrocTiles+2,x
	STA $030A|!addr,y
	LDA CrocTiles+3,x
	STA $030E|!addr,y

	LDA $05
	EOR #!CrocProperties
	STA $0303|!addr,y
	STA $0307|!addr,y
	STA $030B|!addr,y
	STA $030F|!addr,y

	LDA #$03
JMP .Shared

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; HandleGravity
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HandleGravity:
	LDA !AA,x
	BPL .Falling
	CMP #$E8
	BCS .Falling
	LDA #$E8
	STA !AA,x
.Falling:
	CLC : ADC #$03
	STA !AA,x
	BMI .NotTerminalSpeed
	CMP #$40
	BCC .NotTerminalSpeed
	LDA #$40
	STA !AA,x
.NotTerminalSpeed:
RTS
