;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom Launcher, based on XM's code, further adapted into Sprite Tool by Davros
;;
;; Description: This will generate a custom sprite.
;; Specify the actual sprite that is generated below.
;;
;; NOTE: Trying to generate a sprite that doesn't exist will crash your game.
;;
;; Uses first extra bit: NO  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !CUST_SPRITE_TO_GEN = $12
                    !NEW_SPRITE_NUM = !7FAB9E
		    !EXTRA_BITS = !7FAB10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    PRINT "INIT ",pc              
                    PRINT "MAIN ",pc                                    
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main torpedo ted launcher code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SPRITE_CODE_START:  LDA $17AB,x             ; \ RETURN if it's not time to generate
                    BNE RETURN              ; /
                    LDA #$50                ; \ set time till next generation = 50
                    STA $17AB,x             ; /
                    LDA $178B,x             ; \ don't generate if off screen vertically
                    CMP $1C                 ;  |
                    LDA $1793,x             ;  |
                    SBC $1D                 ;  |
                    BNE RETURN              ; /
                    LDA $179B,x             ; \ don't generate if off screen horizontally
                    CMP $1A                 ;  |
                    LDA $17A3,x             ;  |
                    SBC $1B                 ;  |
                    BNE RETURN              ; / 
                    LDA $179B,x             ; \ ?? something else related to x position of generator??
                    SEC                     ;  | 
                    SBC $1A                 ;  | 
                    CLC                     ;  | 
                    ADC #$10                ;  | 
                    CMP #$20                ;  | 
                    BCC RETURN              ; /
                    JSL $02A9DE|!BankB      ; \ get an index to an unused sprite slot, RETURN if all slots full
                    BMI RETURN              ; / after: Y has index of sprite being generated

                    PHX                     ; \ before: X must have index of sprite being generated
                    TYX                     ; / routine clears *all* old sprite values...

GENERATE_SPRITE: 

		    LDA #$08                ; \ set sprite status for new sprite
                    STA !14C8,y             ; /
                    LDA #!CUST_SPRITE_TO_GEN ; \ set sprite number for new sprite
                    STA !NEW_SPRITE_NUM,x    ; /
		    JSL $07F7D2|!BankB		; reset sprite properties 
                    JSL $0187A7|!BankB      ; get table values for custom sprite       
                    LDA #$88                ; \ mark as initialized
                    STA !EXTRA_BITS,x        ; /
		PLX
LDA $7FFFFF
                    LDA $179B,x             ; \ set x position for new sprite
                    STA !E4,y             ;  |
                    LDA $17A3,x             ;  |
                    STA !14E0,y             ; /
                    LDA $178B,x             ; \ set y position for new sprite
                    STA !D8,y             ;  |
                    LDA $1793,x             ;  |
                    STA !14D4,y             ; /


		LDA #$30
		STA !1540,y

		PHY
		PHX
		TYX
    		%SubHorzPos()
		PLX
		TYA
		PLY
		STA $157C,y
		
                    JSR SUB_HAND            ; display hand graphic
RETURN:              RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; display hand for torpedo ted launcher
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HAND:           JSL $02A9DE|!BankB             ; \ get an index to an unused sprite slot, RETURN if all slots full
                    BMI SUBRETURN              ; / after: Y has index of sprite being generated

                    PHX                     ; \ before: X must have index of sprite being generated
                    TYX                     ; / routine clears *all* old sprite values...

GENERATE_SPRITE_TWO:
		    LDA #$08                ; \ set sprite status for new sprite
                    STA !14C8,y             ; /
                    LDA #!CUST_SPRITE_TO_GEN ; \ set sprite number for new sprite
                    STA !NEW_SPRITE_NUM,x    ; /
		    JSL $07F7D2|!BankB    ; reset sprite properties 
                    JSL $0187A7|!BankB      ; get table values for custom sprite       
                    LDA #$8C               ; \ mark as initialized
                    STA !EXTRA_BITS,x        ; /
                    PLX                     ; call init routine on sprite

                    LDA $179B,x             ; \ set x position for new sprite
		CLC
		ADC #$08
                    STA !E4,y             ;  |
                    LDA $17A3,x             ;  |
		ADC #$00
                    STA !14E0,y             ; /
                    LDA $178B,x             ; \ set y position for new sprite
		CLC
		ADC #$09
                    STA !D8,y             ;  |
                    LDA $1793,x             ;  |
		ADC #$00
                    STA !14D4,y             ; /
		LDA #$90
		STA !1558,y
		LDA #$00
		STA !AA,y
		STA !B6,y

SUBRETURN: RTS