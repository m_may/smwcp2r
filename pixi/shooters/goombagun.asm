;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Normal Launcher 2, by Davros
;;
;; Description: This will generate a normal sprite without showing the hand.
;; Specify the actual sprite that is generated below.
;;
;; NOTE: Trying to generate a sprite that doesn't exist will crash your game.
;;
;; Uses first extra bit: NO
;;  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	!SHOTS_LEFT = $7F9A70
                    !SPRITE_TO_GEN = $0F

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    PRINT "MAIN ",pc                                    
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                 
                    PRINT "INIT ",pc       
                    RTL      
XOFFSET:
	db $F0,$10

XOFFSET2:
	db $FF,$00
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main torpedo ted launcher code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN2:
	RTS

NEXTSPRITE:
	DEX		;next sprite
	BPL LOOP
	PLX
	BRA RETURN2

SPRITE_CODE_START:

CHECK:
	PHX
	LDX #$0B	;12 sprites to LOOP through
LOOP:
	LDA $14C8,x	;test sprite status
	CMP #$0B	;must be being carried
	BNE NEXTSPRITE
	LDA $9E,x	;test sprite #
	CMP #$80
	BNE NEXTSPRITE
	
	PLX

	LDA $18
	AND #$20            ; \ RETURN if l not pressed
                    BEQ RETURN              ; /

	LDA !SHOTS_LEFT
	BEQ RETURN

                    JSL $02A9DE             ; \ get an index to an unused sprite slot, RETURN if all slots full
                    BMI RETURN              ; / after: Y has index of sprite being generated

GENERATE_SPRITE:    

	LDA #$08                ; \ set sprite status for new sprite
                    STA $14C8,y             ; /
                    LDA #!SPRITE_TO_GEN      ; \ set sprite number for new sprite
                    STA $009E,y             ; /
                   
	LDX $76	; \ set x position for new sprite
             LDA $94
	CLC
	ADC XOFFSET,x 	;(** this can be either $F0 or $10 depending on the direction faced **)
	STA $00E4,y             ;  |
			;  |
	LDA $95      
	ADC XOFFSET2,x
	STA $14E0,y             ; /
                    
	LDA $96
	CLC
	ADC #$10	; \ set y position for new sprite
        STA $00D8,y             ;  |
        LDA $97  
        ADC #$00
        STA $14D4,y             ; /

	LDA #$09		;status = stationary
	STA $14C8,y		;normal status, run init routine?
	LDA #$FF		;set stun timer
	STA $1540,y



                  ; \ before: X must have index of sprite being generated
                    TYX                     ;  | routine clears *all* old sprite values...
                    JSL $87F7D2             ;  | ...and loads in new values for the 6 main sprite tables
                    JSR SUB_GET_DIR         ;  |
                    TYA                     ;  |
                    STA $157C,x             ;  |
                    STA $00                 ;  |
                    LDA #$FF                ;  |
                    STA $1540,x             ;  |
	LDA #$A0
	STA $AA,x
	STA $154C,x	;disable interaction
	STA $1564,x	;disable interaction
	STA $15DC,x	;disable interaction
	LDA !15F6,x
	AND #$F1
	ORA #$02
	STA !15F6,x
	LDA #$09
	STA $1DFC
	LDA !SHOTS_LEFT
	DEC
	STA !SHOTS_LEFT
	LDA #$06
	STA $1887	;shake skreen
                    ;  |
RETURN:		    RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; horizontal mario/sprite contact - shared
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_GET_DIR:         LDY #$00                ;  |
                    LDA $94                 ;  |
                    SEC                     ;  |
                    SBC $E4,x               ;  |
                    STA $0F                 ;  |
                    LDA $95                 ;  |
                    SBC $14E0,x             ;  |
                    BPL RETURN_2            ;  |
                    INY                     ;  |
RETURN_2:            RTS                     ; RETURN

