;rideable baloon shooter
;by smkdan

;This will generate a stream of rideable balloons

;CHANGE THIS TO SPRITE NUMBER OF RIDEABLE BALLOON
!SPRITE_NUM = $26

;CHANGE THIS TO FREQUENCY OF GENERATION	
GEN_INTERVAL:
db $C0,$80

;SET TO 01 TO USE RANDOM PALETTES OR 00 TO STICK WITH DEFAULT
!CUSTOM_PAL = $00

;17AB: counter for shooter

;179B: Xpos low
;17A3: Xpos high

;178B: Ypos low
;1793: Ypos high

CP:	db $88,$8C

	print "INIT ",pc 
	print "MAIN ",pc                                    
	PHB                     
	PHK                     
	PLB                     
	JSR Run
	PLB                     
	RTL      

Run:
	LDA $17AB,x	;load counter
	BNE RETURN	;RETURN if not yet reached

	JSL $02A9DE|!BankB	;grab free sprite slot
	BMI RETURN		;RETURN if none lef
	
	LDA #$01
	STA !14C8,y		;normal status, run init routine
	
	PHY
	LDA $1783,x
	AND #$40
	ROL
	ROL
	ROL
	TAY
	LDA GEN_INTERVAL,y
	STA $17AB,x	;restore counter
	PLY
	
	PHX
	TYX
	LDA #!SPRITE_NUM
	STA !7FAB9E,x		;into sprite type table for custom sprites
	PLX

	LDA $179B,x		;load Xlow
	STA !E4,y		;same Xpos
	LDA $17A3,x		;Xhigh
	STA !14E0,y

	REP #$20		;16bit math
	LDA $1C			;scroll Y
	CLC
	ADC.w #$00E0		;add 224
	SEP #$20		;8bit A for stores...

	STA $00D8,y		;Ypos low
	XBA
	STA !14D4,y		;Ypos high

	TYX			;new sprite slot into X
	JSL $07F7D2|!BankB	;create sprite
	JSL $0187A7|!BankB	;tweaker tabless

	LDY #!CUSTOM_PAL		;load user supplied custom palette value
	LDA CP,y
	STA !7FAB10,X

RETURN:
	RTS