!GlassType = $1765|!addr
!GlassProperties = $0F

Tiles:
db $0C,$0E,$7C,$7D

TileSize:
db $02,$02,$00,$00

TileOffset:
dw $00,$00,$08,$08

print "MAIN ",pc
	PHB : PHK : PLB
	JSR GlassMain
	PLB
RTL

GlassMain:
	%ExtendedGetDrawInfo()

	LDA !GlassType,x
	TAX
	LDA Tiles,x
	STA $03
	LDA TileOffset,x
	STA $04
	LDA TileSize,x
	STA $05

	LDA $01
	CLC : ADC $04
	STA $0200|!addr,y
	LDA $02
	CLC : ADC $04
	STA $0201|!addr,y
	LDA $03
	STA $0202|!addr,y
	LDA #!GlassProperties
	ORA $64
	STA $0203|!addr,y

	TYA
	LSR #2
	TAY
	LDA $05
	STA $0420|!addr,y

	LDX $15E9|!addr

	LDA #$00
	%ExtendedSpeed()
RTS
