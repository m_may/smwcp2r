;-----------------------------------------------------------------------;
; Cluster Flower Petals - by Ladida					;
; Can be anything, like petals, leaves, snow; just change the graphics. ;
; Edit of Roy's original Spike Hell sprite.				;
;-----------------------------------------------------------------------;

;!ParticleTile = $67	;Tile # of the flower petal. was $67

;!ParticleSize = $00	;Size of flower petal. 8x8 by default

;!ParticlePalPage = $7A	;YXPPCCCT of tile. XY flip handled by Properties table.;was 35, 3F

!ParticleXPos = $1E16
!ParticleYPos = $1E02
!ParticleXSpeed1 = $1E66
!ParticleXSpeed2 = $0F72
!ParticleXSpeedFraction = $1E8E
!ParticleYSpeed = $1E52
!ParticleYSpeedFraction = $1E7A

!ParticleProps = $0F86
!ParticleSize = $0F5E
!ParticleTile = $1E3E
!ParticleShow = $1E2A
!ParticleTimeToLive = $0F9A

OAMStuff:
db $B0,$B4,$B8,$BC,$C0,$C4,$C8,$CC,$D0,$D4,$D8,$DC,$80,$84,$88,$8C,$40,$44,$48,$4C ; These are all in $02xx

Properties:
db $00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0 ; Properties table, per sprite. YXPPCCCT.

PropertiesTwo:
db $80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40 ; Properties table, per sprite. YXPPCCCT.

do_movement:
	stz $01
	clc
	adc $00
	bmi +
	jmp add_speed
	+
	jmp subtract_speed

subtract_speed:
	beq +
	cmp #$F1
	bcs +
		dec $01
		clc
		adc #$10
		bra subtract_speed
	+
	sta $00
	rts
add_speed:
	beq +
	cmp #$10
	bcc +
		inc $01
		sec
		sbc #$10
		bra add_speed
	+
	sta $00
	rts

print "MAIN ",pc
Main:				;The code always starts at this label in all sprites.
	lda $9D
	bne .dont_move
		lda !ParticleXSpeedFraction,y
		sta $00
		LDA $14
		LSR #5
		AND #$01
		BEQ +
		LDA !ParticleXSpeed1,y
		BRA ++
		+
		LDA !ParticleXSpeed2,y
		++
		jsr do_movement
		lda $00
		sta !ParticleXSpeedFraction,y
		lda !ParticleXPos,y
		clc
		adc $01
		sta !ParticleXPos,y

		lda !ParticleYSpeedFraction,y
		sta $00
		lda !ParticleYSpeed,y
		jsr do_movement
		lda $00
		sta !ParticleYSpeedFraction,y

		lda !ParticleYPos,y
		clc
		adc $01
		sta !ParticleYPos,y
		sec                             ;  | Check Y position relative to screen border Y position.
		sbc $1C
		cmp #$F0
		bcc +
			lda !ParticleShow,y
			bne +
			lda #$01
			sta !ParticleShow,y
		+

	.dont_move

	lda !ParticleShow,y
	beq .dont_show
		jsr Draw
		lda !ParticleTimeToLive,y
		beq .dont_kill
			cmp #$01
			bne +
				lda #$00
				sta $1892,y
				inc
			+
			dec
			sta !ParticleTimeToLive,y
		.dont_kill
	.dont_show

	rts

Draw:                       ; OAM routine starts here.
LDX.w OAMStuff,y 		; Get OAM index.
LDA !ParticleYPos,y			; \ Copy Y position relative to screen Y to OAM Y.
SEC                             ;  |
SBC $1C				;  |
STA $0201,x			; /
LDA !ParticleXPos,y			; \ Copy X position relative to screen X to OAM X.
SEC				;  |
SBC $1A				;  |
STA $0200,x			; /
LDA !ParticleTile,y		; \ Tile
STA $0202,x                     ; /
LDA $14
LSR #2
AND #$01
BEQ +
LDA Properties,y
ORA !ParticleProps,y
BRA ++
+
LDA PropertiesTwo,y
ORA !ParticleProps,y
++
STA $0203,x
PHX
TXA
LSR
LSR
TAX
LDA !ParticleSize,y
STA $0420,x
PLX
LDA $18BF
ORA $1493
BEQ ReturnToTheChocolateWhatever            	; Change BEQ to BRA if you don't want it to disappear at generator 2, sprite D2.
LDA $0201,x
CMP #$F0                                    	; As soon as the sprite is off-screen...
BCC ReturnToTheChocolateWhatever
LDA #$00					; Kill it.
STA $1892,y					;

ReturnToTheChocolateWhatever:
RTS
