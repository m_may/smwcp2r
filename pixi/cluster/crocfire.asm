!YSpeed = $1E52|!addr
!XSpeed = $1E66|!addr
!YPosFrac = $1E7A|!addr
!XPosFrac = $1E8E|!addr
!HomingState = $0F4A|!addr
!HomingTimer = $0F9A|!addr

!BulletClippingXOff = $02
!BulletClippingYOff = $02
!BulletClippingWidth = $0C
!BulletClippingHeight = $0C
!BulletHomingSpeed = $18

!MarioClippingXOff = read1($03B669)
MarioClippingYOff = $03B65C
!MarioClippingWidth = read1($03B673)
MarioClippingHeight = $03B660
CircleCoords = $07F7DB

Tilemap:		db $2C,$2E,$4C,$4E
Properties:		db $25,$27,$2B,$2B

print "MAIN ",pc
	PHB : PHK : PLB
	JSR Main
	PLB
RTL

Main:
	JSR Graphics
	LDA $9D
	BNE .Return
	JSR SubOffScreen
	LDA !HomingTimer,x
	BEQ .NotHoming
	DEC
	STA !HomingTimer,x
	BNE .NotHoming
	JSR TargetMario
	INC !HomingState,x
.NotHoming:
	JSR Movement
	JSR Interaction
.Return:
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Subroutines
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Graphics
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OAMIndeces:
db $40,$44,$48,$4C,$D0,$D4,$D8,$DC,$80,$84
db $88,$8C,$B0,$B4,$B8,$BC,$C0,$C4,$C8,$CC

Graphics:
	LDY OAMIndeces,x

	LDA !cluster_x_low,x
	SEC : SBC $1A
	STA $0200|!addr,y
	LDA !cluster_y_low,x
	SEC : SBC $1C
	STA $0201|!addr,y

	PHY
	LDY !HomingState,x
	LDA !HomingTimer,x
	BEQ +
	INY #2
+	LDA Properties,y
	XBA
	LDA $14
	LSR #2
	AND #$03
	TAY
	LDA Tilemap,y
	PLY
	REP #$20
	STA $0202|!addr,y
	SEP #$20

.Shared:
	TYA
	LSR #2
	TAY
	LDA #$02
	STA $0420|!addr,y
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; SubOffScreen
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SubOffScreen:
	LDA !cluster_x_low,x
	CMP $1A
	LDA !cluster_x_high,x
	SBC $1B
	BNE .OffScreen

	LDA !cluster_y_high,x
	XBA
	LDA !cluster_y_low,x
	REP #$20
	SEC : SBC $1C
	CMP #$00EF
	SEP #$20
	BCC .Return
.OffScreen:
	STZ $1892|!addr,x
.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; TargetMario
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TargetMario:
	LDA !cluster_x_high,x
	XBA
	LDA !cluster_x_low,x
	REP #$20
	SEC : SBC $94
	STA $00
	SEP #$20
	LDA !cluster_y_high,x
	XBA
	LDA !cluster_y_low,x
	REP #$20
	SEC : SBC $96
	SEC : SBC #$000F
	STA $02
	SEP #$20

	LDA #$20
	%Aiming()
	LDA $00
	STA !XSpeed,x
	LDA $02
	STA !YSpeed,x
RTS

AtanTable:
db $00,$06,$0D,$13,$19,$1F,$25,$2C
db $32,$38,$3E,$44,$49,$4F,$55,$5A
db $60,$65,$6A,$70,$75,$7A,$7F,$84
db $88,$8D,$91,$96,$9A,$9E,$A2,$A7
db $AA,$AE,$B2,$B6,$B9,$BD,$C0,$C3
db $C7,$CA,$CD,$D0,$D3,$D6,$D9,$DB
db $DE,$E1,$E3,$E6,$E8,$EA,$ED,$EF
db $F1,$F3,$F5,$F7,$F9,$FB,$FD,$FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Movement
;
; Adopted from YCZ's original bullet because... too lazy.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Movement:
	LDA !YSpeed,x
	ASL
	ASL
	ASL
	ASL
	CLC
	ADC !YPosFrac,x
	STA !YPosFrac,x
	PHP
	LDA !YSpeed,x
	LSR
	LSR
	LSR
	LSR
	CMP #$08
	LDY #$00
	BCC .Label1
	ORA #$F0
	DEY

.Label1:
	PLP
	ADC $1E02|!addr,x
	STA $1E02|!addr,x
	TYA
	ADC $1E2A|!addr,x
	STA $1E2A|!addr,x

	LDA !XSpeed,x
	ASL
	ASL
	ASL
	ASL
	CLC
	ADC !XPosFrac,x
	STA !XPosFrac,x
	PHP
	LDA !XSpeed,x
	LSR
	LSR
	LSR
	LSR
	CMP #$08
	LDY #$00
	BCC .Label2
	ORA #$F0
	DEY

.Label2:
	PLP
	ADC $1E16|!addr,x
	STA $1E16|!addr,x
	TYA
	ADC $1E3E|!addr,x
	STA $1E3E|!addr,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Interaction
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Interaction:
	LDA !cluster_x_low,x
	CLC : ADC #!BulletClippingXOff
	STA $00
	LDA !cluster_x_high,x
	ADC #$00
	STA $01
	LDA !cluster_y_low,x
	CLC : ADC #!BulletClippingYOff
	STA $02
	LDA !cluster_y_high,x
	ADC #$00
	STA $03
	TXY
	LDX #$00
	LDA $19
	BNE +
	INX
+	LDA $187A|!addr
	BEQ +
	INX #2
+	LDA MarioClippingYOff,x
	STA $04
	STZ $05
	LDA MarioClippingHeight,x
	TYX
	REP #$20
	AND #$00FF
	STA $06
	CLC : ADC.w #!BulletClippingHeight
	STA $08
	LDA $94
	CLC : ADC.w #!MarioClippingXOff
	SEC : SBC $00
	CLC : ADC.w #!MarioClippingWidth
	CMP.w #!BulletClippingWidth+!MarioClippingWidth
	BCS .Return
	LDA $96
	CLC : ADC $04
	SEC : SBC $02
	CLC : ADC $06
	CMP $08
	BCS .Return
	SEP #$20
	JSL $00F5B7|!bank
.Return:
	SEP #$20
RTS
