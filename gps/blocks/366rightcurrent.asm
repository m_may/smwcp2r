;Customizable Conveyor Block
;Made on request for Pureblade by Kaijyuu.  I just put the coding into the ASM format.
;Set to act like tile 130.

db $42
JMP Return : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Main:

LDA $94		;mario's low x position 
CLC 
ADC #$08	;add 1 (customizable in hex, speed of the conveyor belt)
STA $94 	;store back to mario's low x position 
LDA $95 	; mario's high x position 
ADC #$00	; add 1 if overflow (if $94 went from FF->00) 
STA $95 	; store back to mario's high x position
RTL

Return:

RTL		