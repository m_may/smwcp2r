db $42

JMP Mario : JMP Mario : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

!FakeSpriteNumber = $4C
!RealSpriteNumber = $4D

incsrc ../../CommonDefines.asm

if !sa1
	!SprSize = $16
else
	!SprSize = $0C
endif

Mario:
LDA $94
CMP #$54
BCC Return
CMP #$5D
BCS Return

LDA $71
BNE Return		;return if running some animation

LDA $16
AND #$08
BEQ Return		;return if not pressing up

LDX #!FakeSpriteNumber
LDA !Collectibles		;load collectibles address
AND #$02		;check if bit 1 is set (fake diamond collected)
BNE .spawnit
LDA !Collectibles		;load collectibles address
AND #$01		;check if bit 0 is set (real diamond collected)
BEQ Return			;if not, skip ahead.

LDX #!RealSpriteNumber
.spawnit:
TXA
;kill all sprites
LDX #!SprSize-1
-
STZ !14C8,x
DEX
BPL -
;now spawn it at the first slot
LDX #$00
STA !9E,x
STA !7FAB9E,x
JSL $07F722|!bank
JSL $0187A7|!bank
LDA #$08
STA !7FAB10,x
LDA #$01
STA !14C8,x
TXA

%move_spawn_to_player()

.beginCutscene:
STZ $7B			;reset player's X speed.
STZ $13D4|!addr
STZ $1411|!addr
STZ $1412|!addr
LDA #$0B
STA $71

Return:
RTL

print "The mysterious machine of Opulent Oasis that spawns a diamond sprite which triggers a cutscene event before warping the player to another sublevel when Up is pressed in front of it, if a real diamond was collected. Does nothing otherwise... or, you're free to try some combinations for a different outcome..."