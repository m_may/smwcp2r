;Customizable Conveyor Block
;Made on request for Pureblade by Kaijyuu.  I just put the coding into the ASM format.
;Set to act like tile 130.
db $42
JMP Return : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Main:

LDA $96		;mario's low y position 
CLC 
SBC #$07	;add 1 (customizable in hex, speed of the conveyor belt)
STA $96 	;store back to mario's low y position 
LDA $97 	; mario's high y position 
SBC #$00	; add 1 if overflow (if $94 went from FF->00) 
STA $97 	; store back to mario's high y position
RTL

Return:

RTL		