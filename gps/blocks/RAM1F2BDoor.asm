incsrc ../../CommonDefines.asm

db $42

JMP Mario : JMP Mario : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

Mario:
	LDA $72
	BNE Return	;return if not on ground

	LDA $16
	AND #$08
	BEQ Return	;return if not pressing up

	LDA !PerilousPathsCleared
	CMP #$0F
	BEQ warp	;only warp if bits 0, 1, 2 and 3 are set

	LDA #$2A
	STA $1DFC|!addr
	RTL

warp:
	STZ $13E0|!addr

	LDA #$0F
	STA $1DFC|!addr

	LDA #$06
	STA $71
	STZ $89
	STZ $88

Return:
	RTL

print "A door that's only enterable if the player has gone through all 4 paths of Perilous Paths successfully."