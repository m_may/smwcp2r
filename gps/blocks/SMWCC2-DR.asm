db $37
JMP Collect : JMP Collect : JMP Collect : JMP Return : JMP Return : JMP Return : JMP Return
JMP Collect : JMP Collect : JMP Collect
JMP Collect : JMP Collect

Collect:
PHY

PHB
PHK
PLB

LDY.b #$06

MakingLoop:
REP #$10
LDX TileNumber,y
%change_map16()
SEP #$10
REP #$20
LDA $98
SEC
SBC TilePositionY-$02,y
STA $98
LDA $9A
SEC
SBC TilePositionX-$02,y
STA $9A

DEY
DEY
BPL MakingLoop
SEP #$20

PLB
PLY

LDA #$1C
STA $1DF9|!addr

LDA #$60
STA $0F02|!addr		;this is the SMWC coin counter timer (status bar).

LDA $7FC060
ORA #$02
STA $7FC060

Return:
RTL

TilePositionX:
dw $0010,$FFF0,$0010

TilePositionY:
dw $0000,$0010,$0000

TileNumber:
dw $4102,$4103,$4112,$4113

print "Bottom-right part of SMWC Coin #2. Make sure you configure Conditional Direct Map16 options for it (use flag 1, and mark 'always show objects/add 0x100 tiles')."