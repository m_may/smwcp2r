db $42
JMP End : JMP End : JMP End : JMP Sprite : JMP Sprite : JMP End : JMP End : JMP End : JMP End : JMP End

Sprite:
	LDA $0A		;\ 
	AND #$F0	; |
	STA $9A		; |
	LDA $0B		; | Update block position
	STA $9B		; |
	LDA $0C		; |
	AND #$F0	; |
	STA $98		; |
	LDA $0D		; |
	STA $99		;/

	PHB		;\
	LDA #$02	; |
	PHA		; | Shatter Effect
	PLB		; |
	LDA #$00	; |
	JSL $028663	; |
	PLB		;/
	LDA #$02	;\ Replace Block
	STA $9C		; | with tile 25
	JSL $00BEB0	;/
	RTL

End:
	RTL