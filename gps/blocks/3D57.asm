db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball
JMP TopCorner : JMP HeadInside : JMP BodyInside

!Frame = $00
!Length = $0f;Only use $00, $01, $03, $07, $0F, $1F, $3F, $7F or $FF (note that $00 is idiotic).
Solidity:
db $01,$01,$01,$01,$01,$01,$01,$01
db $00,$00,$00,$00,$00,$00,$00,$00

MarioBelow:
MarioAbove:
MarioSide:

TopCorner:
HeadInside:
BodyInside:

SpriteV:
SpriteH:

MarioCape:
MarioFireball:

LDA.l $7FC080+!Frame
AND.b #!Length
TAX
LDA.l Solidity,x
BEQ Return
LDA #$30
STA $1693
LDY #$01
Return:
RTL