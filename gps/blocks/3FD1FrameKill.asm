
!Gl = $00		; $00 if it should be based on level exAnim, and $20 if it should be based on global exAnim
!Slot = $00 		; exAnimation slot the killing should be based on

!Max = $00 		; Last frame the block should kill on

db $42
JMP M : JMP M : JMP M : JMP S : JMP S : JMP R : JMP R
JMP M : JMP M : JMP M

M:
LDA $7FC080+!Gl+!Slot	;\
CMP.b #!Max+$01		;| Check if it is a correct frame
BCS R			;/

JSL $00F606		; If yes, kill the player

S:
LDA $7FC080+!Gl+!Slot	;\
CMP.b #!Max+$01		;| Check if it is a correct frame
BCS R			;/
	LDA #$04
	STA $14C8,x

	LDA #$08	;spinjump death sound
	STA $1DF9

R:
RTL

