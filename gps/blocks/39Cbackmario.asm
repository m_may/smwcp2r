; This is the code for the Door to change Mario's position relative to the FG.
; He can go behind and in front of it as he pleases.
db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball : JMP RETURN : JMP RETURN : JMP RETURN

MarioSide:
SpriteV:
SpriteH:
MarioCape:
MarioFireball:	LDA $15		; Button To Press
		AND #$08	; Compare with going up.
		BNE RETURN	; If Mario is pressing up, do nothing.
		STZ $0DD4	; Allow Mario to go through the door again.
		RTL	

MarioBelow:
MarioAbove:	LDA $15		; Button To Press
		AND #$08	; Compare with going up.
		BEQ RETURN	; If Mario is not pressing up, do nothing.
		LDA $0DD4	; Free memory used to determine if you're holding down the Up button.
		BNE RETURN	; If you are, don't do door actions.
		LDA #$0F	; Load the door sound
		STA $1DFC	; Store and load the I/O Port
		LDA $64		; Load Mario's Priority
		EOR #$30	; Flip Mario's Priority
		STA $64		; Store Mario's Priority in the layer.
		LDA #$01	; Lock Mario so he doesn't go in & out of doors.
		STA $0DD4	; Store to free memory.
RETURN:		RTL