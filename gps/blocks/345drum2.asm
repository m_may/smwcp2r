db $42
!Slot = $00
JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP MarioSide : JMP MarioSide : JMP MarioSide

MarioAbove:
LDA $7FC080+!Slot
CMP #08
BEQ R
CMP #09
BNE MarioFireBall

R:
LDA #$08		;"Boing" sound
STA $1DFC|!addr		;I/O Port played
LDA #$90
STA $7D			;Mario's Y Speed
LDA #$01
STA $0F5E|!addr
LDA #$80
STA $1406|!addr
STZ $1697|!addr        ;Clear consecutive jumps

MarioSide:
MarioBelow:
SpriteV:
SpriteH:
MarioCape:
MarioFireBall:
RTL              ;Return