; Made by Lightvayne [Hurt Mario/Sprite Block]
; A Block that will hurt the player and destory sprites when the On/Off Switch is Off. Based off of block made by Ramp202
; Act like 130 or 25 depending on how you need it

db $42

JMP Mario : JMP Mario : JMP Mario : JMP Death : JMP Death : JMP Return : JMP Return 
JMP Mario : JMP Mario : JMP Mario

Mario:
	LDA $14AF	;\ If On/Off Switch is On, Return
	BEQ Return	;/
	JSL $00F5B7	;  Change $00F5B7 to $00F606 to kill the player instead.
	LDY #$00
RTL

Death:
	LDA $14AF	;\ If On/Off Switch is On, Return
	BEQ Return	;/

	LDA #$04	;\ Calls the spinjumpcloud routine
	STA $14C8,x	;/

	LDA #$08	;\ Spinjump death sound
	STA $1DF9	;/
Return:
RTL