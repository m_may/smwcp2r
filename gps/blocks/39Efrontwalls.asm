; This wall can only be walked in front of if Mario is in front of the FG.
db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP Sprite : JMP Sprite : JMP MarioCape : JMP MarioFireball : JMP MarioSide : JMP MarioSide : JMP MarioSide

MarioSide:
MarioCape:
MarioFireball:	
MarioBelow:
MarioAbove:	LDA $64		; Load Mario's Priority
		AND #$20	; Check to see if Mario has priority or not.
		BEQ RETURN	; If Mario doesn't have priority over everything else, you can move.
Sprite:		LDY #$01	; Load the value of Block 130 (Cement Block) Hi Bit
		LDA #$30	; Load the value of Block 130 (Cement Block) Low Bit
		STA $1693	; Store value of tile.
RETURN:		RTL