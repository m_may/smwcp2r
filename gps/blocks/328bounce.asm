;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Bounce Block
;Made by Broozer
;
;What this block does:
;This block will simply bounce Mario, and play the original "boing" sound, played by the musical note blocks as
;well.
;
;Set it to act like tile 130
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42
JMP Return : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP MarioAbove : JMP Return : JMP Return

MarioAbove:
LDA #$80
STA $7D          ;Mario's Y Speed
LDA #$08         ;"Boing" sound
STA $1DFC        ;I/O Port played
LDA #$FF 	; screen follows Mario
STA $1404
Return:
RTL              ;Return