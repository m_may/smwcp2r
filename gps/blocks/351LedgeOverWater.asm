;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;This block will act like water that is capped by a walkable ledge,
;basically a combination of tiles 002 and 100.
;Must act like Tile 002.
;						-Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return : JMP Ledge : JMP Return : JMP Ledge : JMP Return : JMP Return : JMP Ledge : JMP Ledge : JMP Return : JMP Return

Ledge:
	LDY #$01
	LDA #$00	;Acts like Water
	STA $1693

Return:
	RTL