; This code is for inside ledges that Mario can jump on only when he's behind the FG.
db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP Sprite : JMP Sprite : JMP MarioCape : JMP MarioFireball : JMP MarioSide : JMP MarioSide : JMP MarioSide

MarioSide:
MarioCape:
MarioFireball:	
MarioBelow:
MarioAbove:	LDA $64		; Load Mario's Priority
		AND #$20	; Check to see if Mario has priority or not.
		BNE RETURN	; If Mario does have priority over everything else, you can move.
Sprite:		LDY #$01	; Load the value of Block 100 (Ledge) Hi Bit
		LDA #$00	; Load the value of Block 100 (Ledge) Low Bit
		STA $1693	; Store value of tile.
RETURN:		RTL