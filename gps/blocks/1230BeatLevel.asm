incsrc ../../CommonDefines.asm

db $42
JMP Main : JMP Main : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Main:
	LDA #$05
	STA $1DF9|!addr		;Give it a "Midway point/Goal" Sound Effect.
	LDA #$08
	TSB !Cheats
	TSB !Cheats+1
Blank:
	LDA #$02
	STA $9C  ;Store it to Map16 Tile Generator.
	JSL $00BEB0|!bank  ;Generate the Blank Tile.
	LDY #$00  ;Leave the Y Index with Zero.
Return:
	RTL
