;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;This is an appearing/disappearing block that will act like tile 100 on frames 00-0F
;and tile 002 on frames 10-1F. Must act like tile 02.
;						-Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return : JMP Block : JMP Return : JMP Block : JMP Return : JMP Return : JMP Block : JMP Block : JMP Return : JMP Return

Animate:
db $01,$01,$01,$01,$00,$00,$00,$00

Block:
	LDA $7FC080
	LSR
	LSR
	AND #$07
	TAX
	LDA Animate,x
	BEQ Return
	LDY #$01
	LDA #$00
	STA $1693
Return:
	RTL