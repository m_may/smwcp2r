db $42
JMP Mario : JMP Mario : JMP Mario : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

Mario:
	LDA #$A8	;5F original value
	STA $7B
	RTL

Sprite:
	LDA !14C8,x
	CMP #$02
	BEQ Return
	LDA #$B8
	STA !B6,x

Return:
	RTL

print "This block launches both player and sprites leftwards with high speed."