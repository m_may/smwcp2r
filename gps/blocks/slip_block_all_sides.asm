;To be only used as slopes. see image here: SlopeTilemaps.png
;Act as: slope pieces.

	!Freeram_MarioSlip = $1461|!addr
;^[1 byte] freeram determines slippery or not. Needed so that
;when reading the slippery flag is true and not always assume its
;0 every frame (a clear-itself and then do something RAM address)

	!Freeram_SpriteSlip = $7FACC1
;^[12 (normal) or 22 (SA-1) bytes] same as !Freeram_MarioSlip but for
;sprites.

	!Freeram_SpriteSlipFlag = $7FACCD
;^[12 (normal) or 22 (SA-1) bytes] the actual slippery flag for each
;sprite (determines if the sprite should treat the surface slippery;or not).

	!slipperyness = $FF
;^How much slippery, #$80 = half slippery and #$FF is full slippery.

db $37
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball
JMP TopCorner : JMP BodyInside : JMP HeadInside
JMP WallFeet : JMP WallBody ; if using db $37

MarioBelow:
MarioAbove:
MarioSide:
TopCorner:
BodyInside:
HeadInside:
WallFeet:
WallBody:
	LDA $1471		;\In case if the player stands on a sprite platform close
	BNE done		;/to the slope as possible, then do nothing.
	LDA #$01		;\set freeram slippery flag
	STA !Freeram_MarioSlip	;/
				;^Note that this routine is for slopes only.
done:
SpriteH:
MarioCape:
MarioFireball:
	RTL

SpriteV:
	LDA #$01
	STA !Freeram_SpriteSlip,x
	RTL

print "An icy slope."