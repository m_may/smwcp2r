db $37

JMP Kill : JMP KillAbove : JMP Kill : JMP return : JMP return : JMP return : JMP return
JMP return : JMP Kill : JMP Kill
JMP KillIN : JMP KillIN

KillAbove:
LDA $98
AND #$0F
CMP #$08
BCS KillIN
RTL

Kill:
LDA $75			;load player is in water flag
BEQ return		;if not, return.

KillIN:
PHY			;preserve Y
JSL $00F606|!bank	;kill the player
PLY			;restore Y

return:
RTL			;return.

print "This is SMW's regular lava surface, however it has custom block code implemented so it properly kills Mario when touched."