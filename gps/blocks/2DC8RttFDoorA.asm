;Mario teleports to the level set in this .asm
;by smkdan
;Act like tile 25
db $42
JMP Code : JMP Code : JMP Code : JMP Return : JMP Return : JMP Return : JMP Return : JMP Code : JMP Code : JMP Code
;don't touch those

!LEVEL = $0C0		;level dest
!PROPERTIES = $00	;See 7E:19B8 RAM map, minus the high bit
	
MarioAbove: 
MarioSide: 
MarioBelow:
Code:

	LDA $16
	AND #$08
	BNE Check
	RTL
Check:
	LDA #$29
	STA $1DFC

	LDA #$03
	STA $7F9A82	;points missed in last game
	LDA $7F9A9F
	TAX
	LDA #$03
	STA $7F9A91,x
	LDA $5B
	BEQ Horz

	LDX $97
	BRA Setup
Horz:
	LDX $95		;mario X pos high

Setup:
	PEA !LEVEL	;push teleport destination
	PLA		;low byte
	STA $19B8,x	;write exit table
	PLA		;high byte
	ORA #$04	;exit present
	ORA #!PROPERTIES	
	STA $19D8,x	;write high bit and properties

Teleport:
	LDA #$06	;teleport
	STA $71
	STZ $89
	STZ $88
	
SpriteV: 
SpriteH:
MarioCape: 
MarioFireBall:
Return:
	RTL