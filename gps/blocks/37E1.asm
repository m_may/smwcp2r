;Break when hit by shell
;Made by ImJake9
;Requested by Knight of Time
;
;Acts like tile 130
db $42
JMP Return : JMP Return : JMP Return : JMP Return : JMP ShellH : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

ShellH:
LDA $9E,x	;\
CMP #$04	;|Is sprite a shell?
BEQ CheckState	;|If not, return.
CMP #$05	;|
BEQ CheckState	;|
CMP #$06	;|
BEQ CheckState	;|
CMP #$07	;|
BEQ CheckState	;/
RTL

CheckState:
LDA $14C8,x		;\
CMP #$09		;|If the shell is kicked,
BEQ ShatterBlock	;|Destroy the block.
CMP #$0A		;|
BEQ ShatterBlock	;/
RTL

ShatterBlock:
LDA $0A		;\
AND #$F0	;|
STA $9A		;|
		;|Store values to the block postition
LDA $0B		;|so the block doesn't shatter under Mario.
STA $9B		;|
		;|
LDA $0C		;|
AND #$F0	;|
STA $98		;|
		;|
LDA $0D		;|
STA $99		;/

LDA #$02	;\
STA $9C		;|
JSL $00BEB0	;|
PHB		;|Shatter block.
LDA #$02	;|
PHA		;|
PLB		;|
LDA #$00	;|
JSL $028663	;|
PLB		;/

REP #$20
LDA $98
SEC
SBC #$0010
STA $98
SEP #$20
JSL $00BEB0
REP #$20
LDA $98
SEC
SBC #$0010
STA $98
SEP #$20
JSL $00BEB0



Return:
RTL