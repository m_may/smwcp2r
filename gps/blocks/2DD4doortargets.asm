;pass on a defined # of coins
;by smkdan
;act like tile 25
db $42
JMP Code : JMP Code : JMP Code : JMP Return : JMP Return : JMP Return : JMP Return : JMP Code : JMP Code : JMP Code
;don't touch those

;amount of coins the player can pass on
!Terrible = 5
!Poor = 10
!Good = 15

!RAM_TargetsBroken = $7F9A90

MarioAbove:
MarioBelow:
MarioSide:
Code:

	LDA $16
	AND #$08
	BNE Check
	RTL

Check:

	LDA !RAM_TargetsBroken	;load current coins
	CMP #!Terrible	;compare to required coins
	BCC Terrible		;return (act like 25) if carry clear
	CMP #!Poor	;compare to required coins
	BCC Poor		;return (act like 25) if carry clear
	CMP #!Good	;compare to required coins
	BCC Good		;return (act like 25) if carry clear

	LDA #$00
	STA $7F9A82
	LDA $7F9A9F
	TAX
	LDA #$01
	STA $7F9A91,x

	LDA #$29
	STA $1DFC

	LDA #$05
	STA $71
	RTL

Terrible:
	LDA #$05
	STA $7F9A82
	LDA $7F9A9F
	TAX
	LDA #$04
	STA $7F9A91,x
	LDA #$2A
	STA $1DFC
	LDA #$05
	STA $71
	RTL
Poor:
	LDA #$03
	STA $7F9A82
	LDA $7F9A9F
	TAX
	LDA #$03
	STA $7F9A91,x
	LDA #$29
	STA $1DFC
	LDA #$05
	STA $71
	RTL
Good:
	LDA #$01
	STA $7F9A82
	LDA $7F9A9F
	TAX
	LDA #$02
	STA $7F9A91,x
	LDA #$29
	STA $1DFC
	LDA #$05
	STA $71
Return:
	RTL