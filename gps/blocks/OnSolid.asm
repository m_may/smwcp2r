;This block is solid when the on/off switch is set to 'on'.
;Acts like tile 25.

db $37
JMP Block : JMP Block : JMP Block : JMP Block : JMP Block : JMP Return : JMP Block : JMP Block : JMP Block : JMP Block
JMP Block : JMP Block

Block:
	LDA $14AF|!addr
	BNE Return
	LDY #$01	;act like tile 130
	LDA #$30
	STA $1693|!addr
Return:
	RTL

print "This block is solid when the on/off switch is set to 'on'."