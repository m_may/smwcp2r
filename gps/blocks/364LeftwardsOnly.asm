;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 1-Way Block (Right -> Left), based on a block by Ramp202
; 
; This block will be solid to Mario, Sprites and Fireballs
; on the Left side, but will be passable from all other sides.
; Must act like Tile 130.
; 						-Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return1 : JMP Return1 : JMP MarioSide : JMP Return1 : JMP SpriteSide : JMP Return : JMP Fireballs : JMP Return1 : JMP Return1 : JMP Return1

MarioSide:
	LDA $9A		;\
	AND #$08	; | Check if Mario is on the Left Side
	BNE Return1	;/
	LDA $7B		;\ If Mario's Speed is in the range
	BMI Return1	;/ of 80-FF (Going Left), Return
	RTL
Fireballs:
	LDX $18FC	; Load Extended Sprite into x
	LDA $171F,x	;\
	AND #$08	; | Check if the Fireball is on the Left Side
	BNE Return1	;/
	LDA $1747,x	;\ If Fireball's Speed is in the range
	BMI Return1	;/ of 80-FF (Going Left), Return
	RTL
SpriteSide:
	LDA $E4,x	;\
	AND #$08	; | Check if any Sprites are on the Left Side
	BNE Return1	;/
	LDA $B6,x	;\ If Sprite's Speed is in the range
	BPL Return	;/ of 80-FF (Going Left), Return
Return1:
	LDA #$25	;\
	STA $1693	; | Act like Tile 25
	LDY #$00	;/
Return:
	RTL