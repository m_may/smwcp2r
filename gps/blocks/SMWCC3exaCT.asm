db $37
JMP Collect : JMP Collect : JMP Collect : JMP Return : JMP Return : JMP Return : JMP Return
JMP Collect : JMP Collect : JMP Collect
JMP Collect : JMP Collect

Collect:
LDA $7FC0FD		;load custom trigger exanimation address 2
AND #$20		;check if custom trigger D is active
BNE Return		;if it is, return.

LDA $7FC0FD		;load custom trigger exanimation address 2
ORA #$20		;set bit for custom trigger D
STA $7FC0FD		;store result back.

LDA #$1C
STA $1DF9|!addr

LDA #$60
STA $0F02|!addr		;this is the SMWC coin counter timer (status bar).

LDA $7FC060
ORA #$04
STA $7FC060		;CDM16 flag

Return:
RTL

print "SMWC Coin #3, meant to be used in levels with HDMA since original coins can mess up the effects for a frame. Requires Custom Trigger D ExAnimation."