db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP TopCorner : JMP HeadInside : JMP BodyInside

MarioCape:
MarioFireBall:
RTL

TopCorner:
HeadInside:
BodyInside:
MarioAbove:
MarioBelow:
MarioSide:
LDA $18C7
BNE End
INC $18C7
JMP Prep2


SpriteV:
SpriteH:
LDA $18C7
BNE End
INC $18C7
;JMP Prep1

Prep1:
LDA $0A		;\ 
AND #$F0	;| Update the position
STA $9A		;| of the block
LDA $0B		;| so it doesn't shatter
STA $9B		;| where the players at
LDA $0C		;|
AND #$F0	;|
STA $98		;|
LDA $0D		;|
STA $99		;/

Prep2:
REP #$20
LDA $98
STA $13E6
LDA $9A
STA $1869
SEP #$20
LDA #$01
STA $1864

End:
RTL