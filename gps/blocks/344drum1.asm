db $42
!Slot = $00 ; exAnimation slot the bouncing should be based on
JMP R : JMP MarioAbove : JMP R : JMP R : JMP R : JMP R : JMP R : JMP R : JMP R : JMP R

MarioAbove:
LDA.l $7FC080+!Slot
CMP #02
BCS R
LDA #$08		;"Boing" sound
STA $1DFC|!addr		;I/O Port played
LDA #$90
STA $7D			;Mario's Y Speed
LDA #$01
STA $0F5E|!addr
LDA #$80
STA $1406|!addr
STZ $1697|!addr        ;Clear consecutive jumps

R: 
RTL