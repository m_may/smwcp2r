;pass on a defined # of coins
;by smkdan
;act like tile 25
db $42
JMP Code : JMP Code : JMP Code : JMP Return : JMP Return : JMP Return : JMP Return : JMP Code : JMP Code : JMP Code
;don't touch those

;amount of coins the player can pass on

MarioAbove:
MarioBelow:
MarioSide:
Code:

	LDA $16
	AND #$08
	BNE Check
	RTL

Check:

	LDX $95		;load Xposition high byte as index
	CPX #$06	;if door is on screen 6, it is the prize door
	BEQ BigDoor
	LDA $7F9A83	;byte containing which events have been entered
	AND WhichDoor,x	;compare to required coins
	BNE NoGo	;return (act like 25) if carry clear
DoorToHeaven:
	LDA #$0F	;door sound effect
	STA $1DFC

	LDA InitTime,x
	STA $7F9A84 ;initial time
	LDA $7F9A83

SkipTime:

	LDA #$05
	STA $71
	RTL

NoGo:
	LDA #$2A
	STA $1DFC
Return:
	RTL

BigDoor:
	LDX #$04
	STZ $00
Loop:
	LDA $7F9A83
	AND DoorsWhich,x
	BEQ NoAdd
	INC $00
NoAdd:
	DEX
	BPL Loop
	LDA $00
	CMP #$03
	BCC NoGo
	BEQ ThreeGames
	CMP #$04
	BEQ FourGames
FiveGames:
	BRA DoorToHeaven	;survived all 5 games? go on ahead
	

FourGames:
	LDA $7F9A81
	CMP #$06	;lost 5 or less points after 4 games? go ahead
	BCC DoorToHeaven
	BRA NoGo

ThreeGames:
	LDA $7F9A81
	BEQ DoorToHeaven		;no loss after 3 games? good to go
	BRA NoGo


WhichDoor:	db $01,$02,$04,$00,$08,$10
DoorsWhich:	db $01,$02,$04,$08,$10
InitTime:		db $00,$00,$78,$00,$28,$78