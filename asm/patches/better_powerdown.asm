!DisableDropping	= !False	; If true then the reserve item will NOT drop if you get hurt
!DroppingIfOnlyBig	= !True		; If the above is false, this one is unused.

;From now on, please do not change
!True			= 1
!False			= 0

lorom

org $00F5F3
autoclean JSL PowerDown
RTL

freecode : print pc
print "Patch location: $",pc
PowerDown:
PHX
if !DisableDropping|!DroppingIfOnlyBig == !False
	JSL $028008
endif
LDA $19
ASL : TAX
.notBig
JSR (Actions,x)
PLX
RTL

; Feel free to add more actions if you use more power ups
; (e.g. through LX5's custom power up patch).
Actions:
dw $FFFF	; Unused
dw .Mushroom
dw .Cape
dw .FireFlower

.Mushroom
if !DisableDropping == !False
	if !DroppingIfOnlyBig == !True
		JSL $028008
	endif
endif
LDA #$04
STA $1DF9
LDA #$2F
STA $1496
STA $9D
LDA #$01
STA $71
STZ $19
JSL CUSTOMSHIT
RTS

.Cape
LDA #$0C
STA $1DF9
JSL $01C5AE
INC $9D
LDA #$01
STA $19
JSL CUSTOMSHIT
RTS

.FireFlower
LDA #$20
STA $149B
STA $9D
LDA #$04
STA $71
LDA #$04
STA $1DF9
STZ $1407
LDA #$7F
STA $1497
LDA #$01
STA $19
JSL CUSTOMSHIT
RTS

;CUSTOM SHIT
CUSTOMSHIT:
LDA $147D	;If for some reason the player managed to activate stuff during an animation,
BEQ Return2	;this will force address to become zero again.

LDA $1497	;If flashing, 
ORA $1490	;have star, 
ORA $1493	;or level ending, 
BNE Return2	;return.

STZ $147D	;Else, force reset on power meter action flag. Dunno why I'd need the code above. Redundant but meh.

Return2:
RTL

print "Freespace used: 0x",freespaceuse," bytes."