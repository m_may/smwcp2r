;SA-1 detector
if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
else 
	!sa1	= 0
	!addr	= $0000
	!bank	= $800000
	lorom
endif


incsrc ../../CommonDefines.asm


org $00A270  ; allow beating level via start+select
	autoclean JML BeatLevelCheat

org $04928E ;allow moving everywhere on overworld
	autoclean JML FreeOverworld


freecode : print pc

BeatLevelCheat:
LDA !Cheats
AND #$08
BEQ .dontBeat
LDA #$01
BIT $15
BPL .beat
.secondExit
LDA #$02
.beat
LDY #$0B
JML $00C9FE|!bank ;hijacked in asm/patches/BeatLevel.asm
.dontBeat
LDA #$80
JML $00A27E|!bank


FreeOverworld: ;REP #$30
AND #$00FF
PHA
LDA !Cheats
LSR
BCS + ;if cheat enabled
PLA
JML $049294|!bank
+
PLA
JML $0492AF|!bank ;allow walking on unrevealed paths
