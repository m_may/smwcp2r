;Modified Stripe Images for File Select Menus + Modified Exit Count Handler - authors?
;blindnote: mostly untouched from what I've copied, though some stuff deemed unnecessary was cut out.
;some hijacks considered trivial were moved to the hex edits patch.
;Disk Poppy - added percent calculation, detecting outdated/too new saves, service menu

;SA-1 detector (though I honestly don't think the hack should or would be converted at some point)
!sa1	= 0
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

incsrc ../../CommonDefines.asm
incsrc ../../Version.asm

org $009ADF
	autoclean JML checkForServiceMenu
org $009AE3
	fileSelectedNormally:
org $009AEA
	fileNotSelected:
org $009B16
	returnShort:

org $009D66
	autoclean jml new_exit_numbers
org $009DA8
	new_exit_numbers_jml_return:

;tweak this whenever the stripes are edited
org $009D55 : LDA #$8C
org $009DAB : SBC $AA
org $009D96 : LDY #$01

org $009D3A
autoclean JML stuff
org $009B7B
autoclean JML ultrastuff

freecode : print pc

checkForServiceMenu:
LDA $18
BPL +
JML fileSelectedNormally|!bank
+
AND #$30
BNE +
JML fileNotSelected|!bank
+
STX $010A|!addr
;go to service menu
LDA #$39  ;service menu fade in gamemode
STA $0100
LDA #$0F  ;door sfx
STA $1DFC
PLA                    ;
PLA                    ;
JML returnShort|!bank  ;jump to RTS (any RTS in bank $00) - it will return from the gamemode


stuff:
STZ $05
CPX #$CB
BEQ FileSelectCode
BRA EraseFileSelectCode

FileSelectCode:
PHB : PHK : PLB
STZ $0DDE|!addr
LDX #$00
REP #$10
LDY.w #$0000
-
LDA.l FileSelectStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #FileSelectStimEnd-FileSelectStimStart+$0001
BNE -
LDA #$32
STA $AA
PLB
JML $009D53|!bank

EraseFileSelectCode:
PHB : PHK : PLB
LDX #$00
REP #$10
LDY.w #$0000
-
LDA.l EraseFileSelectStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #EraseFileSelectStimEnd-EraseFileSelectStimStart+$0001
BNE -
SEP #$10
PLB
JML $009DB4|!bank

ultrastuff:
PHB : PHK : PLB
LDA $05
LDX #$00
LDA #$0A
STA $AA
REP #$10
LDY.w #$0000
-
LDA.l UltraStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #UltraStimEnd-UltraStimStart+$0001
BNE -
SEP #$10
LDA #$16
PLB
JML $009D57|!bank

FileSelectStimStart:
incbin "stims/title/fileselect4.stim"
FileSelectStimEnd:

EraseFileSelectStimStart:
incbin "stims/title/erasefileselect4.stim"
EraseFileSelectStimEnd:

UltraStimStart:
incbin "stims/title/ultra.stim"
UltraStimEnd:

table "../../uberasmtool/msgtable.txt"

new_exit_numbers: ;begins with 8-bit A and 16-bit XY
	lda.l $700000+!SRAMVersionComplementSRAMOffset,x
	eor #$FF
	cmp.l $700000+!SRAMVersionSRAMOffset,x
	bne .incompatibleFile
	cmp #!CurrentSRAMVersion
	beq .checkTNK
	.incompatibleFile
	sep #$10
	ldx $00
	lda.b #$3D
	jsr draw_special_tile
	inx #2
	jsr draw_special_tile
	inx #2
	jsr draw_special_tile
	bra .return
	.checkTNK
	lda.l $700000+!TNKModeOffset,x
	beq .checkProgress
	.TNK
	sep #$10
	ldx $00
	lda.b #'T'
	jsr draw_TNK_tile
	inx #2
	lda.b #'N'
	jsr draw_TNK_tile
	inx #2
	lda.b #'K'
	jsr draw_TNK_tile
	bra .return
	.checkProgress
	lda.l $700000+!EventsUnlockedTotalCountOffset,x
	rep #$21
	and #$00FF
	adc.l $700000+!SMWCCoinTotalCountOffset,x
	cmp.w #!MaxProgress
	sep #$30
	ldx $00
	bcc +
	lda.b #$38
	jsr draw_special_tile
	inx #2
	inc
	jsr draw_special_tile
	inx #2
	inc
	jsr draw_special_tile
	bra .return
	+
	sta $211B
	xba
	sta $211B
	lda.b #!ProgressMultiplier
	sta $211C
	lda $2135
	pha
	lda.b #$FD
	jsr draw_tile
	inx #2
	jsr draw_tile
	inx #2
	;percent sign
	lda.b #$0D
	jsr draw_tile
	sep #$20
	pla
	jsr hex_to_dec
	dex #2
	cpy #$00
	jsr draw_tile
	dex #2
	tya
	beq .return
	jsr draw_tile
	.return
	jml new_exit_numbers_jml_return|!bank

draw_TNK_tile:
	pha
if !sa1
	sta $40837F,x
else
	sta $7F837F,x
endif
	lda.b #$3D
if !sa1
	sta $408380,x
else
	sta $7F8380,x
endif
	pla
	rts

draw_special_tile:
	pha
if !sa1
	sta $40837F,x
else
	sta $7F837F,x
endif
	lda.b #$29
if !sa1
	sta $408380,x
else
	sta $7F8380,x
endif
	pla
	rts

draw_tile:
	pha
	clc
	adc #$22
if !sa1
	sta $40837F,x
else
	sta $7F837F,x
endif
	lda.b #$39
if !sa1
	sta $408380,x
else
	sta $7F8380,x
endif
	pla
	rts

;Easier to copy this routine than deal with jslrts
hex_to_dec:
	ldy.b #$00
-
	cmp.b #$0A
	bcc +
	sbc.b #$0A
	iny	
	bra -
	+
	rts

