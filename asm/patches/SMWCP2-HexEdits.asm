;A patch full of trivial hex edits.
;Formerly known as fixall.asm, though I took the liberty to remove/change unnecessary stuff,
;and move freecode : print pc hijacks to separate patches for better control and organization.

;blindedit: LM3 rendered the 'no yoshi' hex edit incompatible. said hex edit was removed.

org $0081D0 : nop #2			;stop game from disabling color math on layer 3
org $0082AA : nop #6			;stop game from jumping layer 3 on fadeout.
org $009436 : db $4C,$17,$94		;disable titlescreen windowing animation (part 1).
org $009AAD : db $4C,$C0,$9A		;disable titlescreen windowing animation (part 2).
org $009AD0 : NOP #3			;disable cursor blinking (titlescreen/ow menus).
org $009B20 : db $20			;don't enable half-color for erase data menu on titlescreen.
org $009C1F				;get rid of titlescreen movement data.
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
org $009C82 : NOP : LDA #$00		;remove titlescreen movement
org $009C87 : BRA $06			;titlescreen never loops
org $009C9F : db $EA,$EA,$EA,$EA	;prevent sprites from disappearing when the file select appears on titlescreen.
org $009CAD : db $EA,$EA,$EA		;prevent HDMA from being disabled on file select.
org $009CB1 : db $00			;disable intro level
org $009EBA : db $A5,$30		;repoint arrow tilemap (titlescreen/ow menus).
org $009EC1 : db $A6,$30		;repoint blank tilemap (titlescreen/ow menus).
org $009F2A : db $00			;speed of fadeins and fadeouts.
org $009F67 : db $07			;which layers are affected by the mosaic effect.
org $00A09C : db $01			;level C5 timer
;org $00A0F9 : db $EA,$EA		;always send player to titlescreen if running into a Game Over. (No need as there is a custom game over now)
org $00A439 : db $80			;fixes the Koopa Paratroopa wing glitch
org $00C5BF : db $11			;don't disable HDMA when player animation ($71) is equal #$0B.
org $00CB0C : db $0C			;prevent gradients from acting strangely at the end of the level (goal tape/keyhole).
org $00CBA3 : db $49			;Fixes a misplaced tile on the Keyhole's Iris In Effect.
org $00D0F1 : db $80			;disable 'TIME UP!' message.
org $00D109 : db $3C			;trigger death animation "jump" when $1496 reaches this value
org $00D26A : db $20,$04,$FA		;Piranha Plant Fix #2 (prevents Mario from being hurt while exiting a Piranha Plant's pipe)
org $00F61C : db $40			;death animation initial timer value
org $00FA04				;Piranha Plant Fix #2 (prevents Mario from being hurt while exiting a Piranha Plant's pipe)
db $9C,$F9,$13,$AD,$97,$14,$D0,$03,$EE,$97,$14,$60
org $00F619 : db $12,$14		;prevents screen from scrolling when Mario loses a life.
org $00FBA4				;remap smoke part 1
db $64,$64,$62,$60,$E8,$EA,$EC,$EA
org $018DDF : db $42,$02		;palette/Priority/GFX Flip of Para-Goomba's wings.
org $018E8D				;Piranha Plant Fix #1 (restores Classic Piranha Plants)
db $20,$C0,$FF,$EA,$EA,$EA,$EA,$EA,$EA,$EA
org $019B8C		 		;shelless koopa tilemap (Fourth byte unused?)
db $E0,$E2,$E2,$CE,$E4,$86,$4E
org $019B93				;blue shelless koopa tilemap (Fourth byte unused?)
db $E0,$E2,$E2,$CE,$E4,$E0,$E0
org $019BC1				;changes the blank tiles used by Jumpin' Piranha Plant from SP2 to SP1
db $69,$69,$C4,$C4,$69,$69
org $019C0D : db $67,$69,$88,$88	;flopping fish remap
org $019C25 : db $69,$69		;changes the blank tiles used by Springboard from SP2 to SP1
org $019C35				;change the Podoboo's tilemap
db $C8,$C8,$D8,$D8,$C9,$C9,$D9,$D9,$D8,$D8,$C8,$C8,$D9,$D9,$C9,$C9
org $019E20 : db $42,$42,$02,$02	;palette/Priority/GFX Flip of Parakoopa's wings.
org $01AA0E : db $03			;prevent Bob-omb, Goomba, Mechakoopa's stun timers from being reset when kicked.
org $01C34C : db $00			;prevents Fire Flower from flipping back and forth.
org $01C510				;prevents items from being sent to the item box.
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
org $01C616 : db $04,$06,$08,$0A	;palettes Star flash through.
org $01E19A : db $4C			;bypass podoboo's dynamic GFX routine
org $01E985 : db $64,$64,$62,$60	;remap smoke part 2
org $01EC36 : db $80			;disable Yoshi rescue message
org $01FFC0				;Piranha Plant Fix #1 (restores Classic Piranha Plants)
db $B5,$9E,$C9,$1A,$F0,$0B,$B9,$0B,$03,$29,$F1,$09,$0A,$99
db $0B,$03,$60,$B9,$07,$03,$29,$F1,$09,$0A,$99,$07,$03,$60
org $02878E : db $06			;On/Off switch's bounce YXPPCCCT format. (Palette B, Page 1)
org $028114 : db $EF			;Bob-omb's explosion GFX tile
org $02811E : db $18			;will Change GFX page used by explosion tile
org $02878A : db $02			;Bounce Block Note Block Palette/GFX (YXPPCCCT)
org $028C6A : db $64,$64,$62,$60	;remap smoke part 3
org $028D42				;remap smoke part 4
db $68,$68,$6A,$6A,$6A,$62,$62,$62
db $64,$64,$64,$64,$64
org $0291F2 : db $0A			;Block Bounce Note Block sprite tiles
org $0291F6 : db $CA			;On/Off switch's bounce tile.
org $029347 : db $20,$E2,$FF		;fixes a glitch where placing a coin above a ?/turn block will cause the coin to leave behind an invisible solid block.
org $0296D8				;remap smoke part 5
db $64,$62,$64,$62,$60,$62,$60
org $029922 : db $64,$62,$64,$62,$62	;remap smoke part 6
org $029C33				;remap smoke part 7
db $64,$64,$62,$60,$60,$60,$60,$60
org $02A347 : db $64,$64,$60,$62	;remap smoke part 8
org $029C94 : db $6C			;remaps the Spin Jump Star tile
org $029D4B : db $E8			;repoints smiley coin to normal coin
org $02A2DA : db $03			;force dry bones' bone to use the second gfx page
org $02A2DF				;Hammer Tilemap
db $20,$22,$22,$20,$20,$22,$22,$20
org $02A2E7				;Hammer YXPPCCCT
db $46,$46,$06,$06,$86,$86,$C6,$C6
org $02AC18 : db $80			;fixes the glitch that occurs when a variable/custom sprite is carried from one level to another
org $02AD4D				;remap score sprites to a blank tile, part 1
db $69,$69,$69,$69,$69,$69,$69
db $69,$69,$69,$69,$69
org $02AD63				;remap score sprites to a blank tile, part 2
db $69,$69,$69,$69,$69,$69,$69
db $69,$69,$69,$69,$69
org $02BB0B				;yoshi wings fix
db $02,$FA,$06,$06,$00,$FF,$00,$00
db $10,$08,$10,$08,$5D,$C6,$5D,$C6
db $42,$42,$02,$02,$00,$02,$00,$02
org $02BDA7 : NOP #12			;fix spike top
org $02CAFA : db $4B,$0B		;makes Chargin' Chuck's arm use the correct palette
org $02CB91 : db $0C			;fix diggin' chuck's shoulder when facing left
org $02CB9B : db $CA,$E2,$A0		;diggin' chuck's hand/shovel tilemaps
org $02CD51 : db $35,$35,$75,$75	;fixes the bottoms of the Flattened Big Switches
org $038E0A : db $03			;palette/properties of timed lift numbers
org $03B90C : db $00,$28		;Magikoopa Palette Correction part 1
org $03B91C : db $40,$34		;Magikoopa Palette Correction part 2
org $03B92C : db $A3,$40		;Magikoopa Palette Correction part 3
org $03B93C : db $06,$4D		;Magikoopa Palette Correction part 4
org $03B94C : db $69,$59		;Magikoopa Palette Correction part 5
org $03B95C : db $CC,$65		;Magikoopa Palette Correction part 6
org $03B96C : db $2F,$72		;Magikoopa Palette Correction part 7
org $03B97C : db $92,$7E		;Magikoopa Palette Correction part 8
org $0491E0 : db $EA,$EA,$EA,$EA,$EA	;disable music fading when entering a level from the overworld
org $04E5B1 : db $60,$61		;tiles revealed by the switch palace level tiles when destroyed
org $04EB42 : db $80			;makes Forest of Illusion submap reveal its event tiles at the same speed as other submaps.
org $04EBAE				;recolor OW smokes
db $F4,$B4,$74,$34,$F4,$B4,$74,$34
db $34,$74,$B4,$F4,$34,$74,$B4,$F4
db $34,$34,$34,$34,$34,$34,$34,$34
db $34,$34,$34,$34,$34,$34,$34,$34
db $34,$34,$34,$34,$34,$34,$34,$34
org $04F709 : db $FF			;no thunders on the ow
org $04F754 : db $80			;disables lightning in Valley of Bowser submap.
org $04FB44 : db $EA,$EA,$EA		;remove overworld clouds (they mess up with indicators)
org $04FDAD : db $74			;palette/properties/flip of overworld ghost when moving right.
org $0584BE : db $20,$20		;enable Priority for Level Mode 07 & 08.
org $0584C1 : db $20			;enable Priority for Level Mode 0A.
org $0584C3 : db $20,$20,$20		;enable Priority for Level Mode 0C, 0D & 0E.
org $0584C8 : db $20			;enable Priority for Level Mode 11.
org $0584D5 : db $20,$20		;enable Priority for Level Mode 1E & 1F.
org $05B129 : db $EA,$EA,$EA		;prevent hdma from being disabled after a message box is triggered.
org $05B296 : db $0C			;prevent hdma from being disabled while a message box is triggered.
org $05CDD8 : db $80			;disable score tallying sfx
org $07F431 : db $34			;make the podoboo use the first GFX page
org $07F43E : db $17			;edit Parabomb's palette
org $07F69C : db $25			;fix dolphin tails when vertically offscreen

;EXPERIMENTAL HEX EDITS
!ThinkThisIsCool = 0			;if not, the edits are reverted to normal.
;blindnote: while I thought it was cool, it has issues so I've reverted them back.

if !ThinkThisIsCool
	org $00D109 : db $E5			;trigger death animation "jump" when $1496 reaches this value
	org $00F61C : db $FF			;death animation initial timer value
	org $00F620 : NOP #2			;allow sprites/animation to keep running even after dying
else
	org $00D109 : db $2D
	org $00F61C : db $30
	org $00F620 : STA $9D
endif

;UNSURE HEX EDITS
!IAmSure = 0				;if not, below edits aren't applied.

if !IAmSure
	org $048E3A : BRA + : NOP #11 : +	;fixes an issue when using AMK
endif