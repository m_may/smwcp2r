;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Additional stuff to completely eradicate IRQ since we
;  don't need it, in order to fix weird Layer 3 HDMA bugs.
;  Added in by edit1754
;
; Note: Almost certainly causes problems with stock SMW
;  boss battles, but we're not using those in this, so it
;  doesn't really matter.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

org $008275
	BRA +
	NOP #2
+

org $0082A4
autoclean JML LetsGoAheadAndSetTheseRegistersCorrectly

freecode

AllowedModes:
	db $00,$00,$00,$00,$00,$00,$00,$00
	db $00,$00,$00,$00,$00,$00,$00,$00
	db $00,$01,$01,$01,$01,$00,$00,$00
	db $00,$00,$00,$00,$00

LetsGoAheadAndSetTheseRegistersCorrectly:
	LDX $0100|!addr
	LDA.l AllowedModes,x
	BEQ +
	LDA $22			; \ Adjust scroll settings for layer 3 
	STA.w $2111		;  | ; BG 3 Horizontal Scroll Offset
	LDA $23			;  | 
	STA.w $2111		;  | ; BG 3 Horizontal Scroll Offset
	LDA $24			;  | 
	STA.w $2112		;  | ; BG 3 Vertical Scroll Offset
	LDA $25			;  | 
	STA.w $2112		; /  ; BG 3 Vertical Scroll Offset
	LDA $3E			; \Set the layer BG sizes, L3 priority, and BG mode 
	STA.w $2105		; /(Effectively, this is the screen mode) ; BG Mode and Tile Size Setting
	LDA $40			; \Write CGADSUB 
	STA.w $2131		; / ; Add/Subtract Select and Enable
	JML $0082B0|!bank

+	
	;STZ $2111 ;Get rid of these, they break layer 3 backgrounds. Moved this clear to OW init.
	;STZ $2111
	JML $0082AA|!bank