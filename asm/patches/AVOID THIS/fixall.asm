;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Super Mario World Fix-All Patch;;
;;Version 1.0			 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;The Super Mario World Perfection Patch is an xkas patch that:
;	a) fixes several of the little glitches and bugs that were left in SMW upon its 1990 release
;	b) installs hacks that optimize and improve the ROM for better editing
;	c) allows for quick and easy editing of some of the game's most commonly edited offsets

header
lorom

; nuke old hijack
;org $00C956
;	LDA $13D2
;	BNE $07


org $00A268		; star-select everywhere
	db $00

org $049291 : db $4C, $AF, $92 ; REMOVE THIS LINE BEFORE FINAL RELEASE: ACTIVATES ALL PATHS ON OVERWORLD was B1 D1 C1
;org $049291 : nop #3

org $02E6D3			; Fixes Fishing Lakitu and 1-up glitch from here V
JML fix_lakitu

org $029C94 : db $6C	; remaps the Spin Jump Star tile

org $048E3A : BRA + : NOP #11 : +		;fixes an issue when using AMK



org $00DA58 ;Fixes Tides in Stone Sea Sanctuary and Frostblow Freezer
db $80		;
org $00DA6F ;same
db $80		;

org $05B93F : dw $0680  ;Another title screen fix

org $0093C6 : db $B8  ;change the length of the Nintendo Presents screen for the intro song

org $0084E5 : dl $04819F ; Overworld pause fix

org $02CD51 ;Fixes the bottoms of the Flattened Big Switches
db $35,$35,$75,$75		;

org $00A514 ; Overworld color Stuff
db $7F

org $00A51F
db $7E	    ; the previous two bytes fix the overworld's animated colors.

;org $0096C7 ; title music
;db $15	    ; track 15

org $00AF35 ; Disables fadeout when a level is beaten. Doesn't apply to bosses.
db $4C,$91,$B0,$EA 

org $00B091 ; Disables fadeout when a level is beaten. Doesn't apply to bosses.
;db $AD,$C6,$13,$F0,$FA,$A5,$13,$29,$03,$4C,$39,$AF
;db $EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA
lda #$40
sta $1495
lda #$80
sta $1494
rts

org $05CC16 ; Eliminates Course Clear Scorecard. 
NOP #80 ; 

org $05CC42 ; Hide Timer/Score Tally in Scorecard 
NOP #31 ;

; Mo tally card disable
org $05CC66
rts

org $009C82	; Disables title screen movement.  Title screen will not loop.
db $EA,$A9,$00

org $01C510 ; Prevents items from being sent to the item box. 
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

org $00F619 ; Prevents screen from scrolling when Mario loses a life. 
db $12,$14

org $029D4B ; repoints smiley coin to normal coin
db $E8

org $019C0D
db $67,$69,$88,$88	;flopping fish remap

org $02878E ; On/Off switch's bounce YXPPCCCT format.
db $06      ; Palette B, Page 1

org $0291F6 ; On/Off switch's bounce tile.
db $CA

org $05DAE6	; Disables Level 24 (Chocolate Island 2)'s gimmick. Useful for when
db $80		; OW transferring when porting ROMs goes wrong.

org $00CBA3	; Fixes a misplaced tile on the Keyhole's Iris In Effect
db $49

org $00CB0C	; HDMA Flicker Fix
db $0C

org $01C34C	; Prevents Fire Flower from flipping back and forth.
db $00

org $01AA0E	; Prevents various sprites' stun timers from being reset when kicked.
db $03

org $05DA19	; Disables all No Yoshi intros, saving you the three seconds of
db $4C,$D7,$DA	; having to manually do it yourself.

org $028114	;Bob-omb's explosion GFX tile
db $EF		;3C

!Sprite166EVals = $07f3fe

; Edit Parabomb's palette
org !Sprite166EVals+$40
db $17

org $02811E	;Will Change GFX page used by explosion tile
db $18		;38

org $02878A	;Bounce Block Note Block Palette/GFX (YXPPCCCT)
db $02		;03

org $029347	; Fixes a glitch where placing a coin above a ? block or
db $20,$E2,$FF	; turn block will cause the coin to leave behind an invisible
		; solid block if the block below is hit.

org $04F754	; Disables lightning in Valley of Bowser submap. (Flashing)
db $80		; In SMWCP2, this submap is used for World 5.

org $04EB42	; Makes Forest of Illusion submap reveal its event tiles at the same
db $80		; speed as other tilemaps. (World 6 in SMWCP2).

org $0291F2	;Block Bounce Note Block sprite tiles
db $0A		;6B

org $02A2DF	;Hammer Tilemap
db $20,$22,$22,$20,$20,$22,$22,$20
		;08 6D 6D 08 08 6D 6D 08

org $02A2E7	;Hammer YXPPCCCT
db $46,$46,$06,$06,$86,$86,$C6,$C6
		;47 47 07 07 87 87 C7 C7

org $00913F	;x133F, fixes the "S" in "Mario/Luigi Start"
db $34		;30

org $009170	;x1370, fixes the "S" in "Mario/Luigi Start"
db $F4		;F0
db $80

org $009D30	;x1F30, title screen "Erase Game" palette darken fix
db $EA,$EA,$EA,$EA,$EA
		;8D 01 07 84 40

org $00A439	;x2639, fixes the Koopa Paratroopa wing glitch
db $80		;F0

org $00AF4C	;x314C, Fade Fix
db $FE,$00,$A9,$01
		;EE 00 A9 07

org $00af4e
	fadeout_loop:

org $00AF6A	;x316A, Fade Fix
;db $C9,$3E,$00,$F0,$05,$C9,$1E,$00,$D0,$04,$38,$E9,$10,$00,$AA,$10,$D3,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA,$EA
;CMP #$003E 
;BEQ .arLabel0A
;CMP #$001E
;BNE .arLabel0E
;.arLabel0A
;SEC
;SBC #$0010
;.arLabel0E
TAX
BPL fadeout_loop
jmp $af88
;NOP #13

; Fix that the bob-omb explosions can kill things they shouldn't
org $828168
	autoclean jml fix_explode_kill_spr
org $828177
	fix_explode_kill_spr_return:

; Stop game from disabling color math on layer 3
org $0081D0
	nop #2 ;and.b #$FB

; Stop game from jumping layer 3 on fadeout.
org $0082aa
	nop #6

;Stop the game from overwriting the last part of the first 2 palettes in the title screen.
org $009A9E
	nop #6
	;was:
	;JSR CODE_00ADA6
	;JSR CODE_00922F

org $00CBA3	;x4DA3, fixes a misplaced tile on the keyhole "iris in" effect
db $49		;4B

org $00D26A	;x546A, Piranha Plant Fix #2 (prevents Mario from being hurt while exiting a Piranha Plant's pipe)
db $20,$04,$FA
		;9C F9 13

org $00FA04	;x7C00, Piranha Plant Fix #2 (prevents Mario from being hurt while exiting a Piranha Plant's pipe)
db $9C,$F9,$13,$AD,$97,$14,$D0,$03,$EE,$97,$14,$60

org $018E8D	;x908D, Piranha Plant Fix #1 (restores Classic Piranha Plants)
db $20,$C0,$FF,$EA,$EA,$EA,$EA,$EA,$EA,$EA
   ;$B9 $0B $03 $29 $F1 $09 $0B $99 $0B $03

org $01FFC0	;x101C0, Piranha Plant Fix #1 (restores Classic Piranha Plants)
db $B5,$9E,$C9,$1A,$F0,$0B,$B9,$0B,$03,$29,$F1,$09,$0B,$99,$0B,$03,$60,$B9,$07,$03,$29,$F1,$09,$0B,$99,$07,$03,$60

org $019BC1	;x9DC1, changes the blank tiles used by Jumpin' Piranha Plant from SP2 to SP1
db $69,$69,$C4,$C4,$69,$69
		;83 83 C4 C4 83 83 

org $019C25	;x9E25, changes the blank tiles used by Springboard from SP2 to SP1
db $69,$69
		;83 83

;org $01CFCD	;xD1CD, Morton/Ludwig/Roy HP
;db $05		;03

org $01DEE3	;xE0E3, changes the blank tiles used by Bonus Roulette from SP2 to SP1
db $58,$59,$69,$69,$48,$49,$58,$59,$69,$69,$48,$49,$34,$35,$69,$69,$24,$25,$34,$35,$69,$69,$24,$25,$36,$37,$69,$69,$26,$27,$36,$37,$69,$69,$26,$27
		;58 59 83 83 48 49 58 59 83 83 48 49 34 35 83 83 24 25 34 35 83 83 24 25 36 37 83 83 26 27 36 37 83 83 26 27

;org $01E733	;xE933, changes the tile used by a squished Goomba
;db $38		;C7

org $01FFD7	;x101D7, makes Classic Piranha Plants use the first graphics page
db $0A		;0B

org $01FFCC	;x101CC, makes Upside-Down Piranha Plants use the first graphics page
db $0A		;0B

;org $0288B0	;x10AB0, sprite that comes out of Block 127/128
;db $85		;04

;org $0288C1	;x10AC1, sprite that comes out of Block 127/128 when Yoshi is present
;db $85		;04

;org $0291F7	;x113F7, Turn Block Bounce Sprite
;db $40		;40

org $02AC18	;x12E18, fixes the glitch that occurs when a variable/custom sprite is carried from one level to another
db $80		;30

org $02AD4D	;x12F4D, changes the blank tile used by 10-Point Score from SP2 to SP1
db $69,$69,$69,$69
		;83 83 83 83

org $02CAFA	;x14CFA, makes Chargin' Chuck's arm use the correct palette
db $4B,$0B
		;47 07

org $02FDB8	;x17FB8, Death Swooper tilemap fix
db $C0		;EB

;org $0381A2	;x183A2, Big Boo Boss HP
;db $05		;03

;org $03CE1A	;x1D01A, Lemmy/Wendy HP
;db $05		;03

;org $03CED4	;x1D0D4, how many HP can Lemmy/Wendy lose before other sprites in the room vanish
;db $05		;03

;org $03CFAF	;x1D1AF, fixes Wendy's bow
;db $08		;06

;org $03CFB5	;x1D1B5, fixes Wendy's bow
;db $08		;02

;org $03D1D7	;x1D3D7, fixes Wendy's bow
;db $1F,$1E	;1E 1F

;org $03D1DD	;x1D3DD, fixes Wendy's bow
;db $1E,$1F	;1F 1E

org $048E2E	;x2102E, allows Overworld music to play properly after a boss is defeated
db $80		;F0

org $0584E1	;x286E1, music played during SMW Koopaling Boss Battles
db $05		;05

;org $07F27B	;x3F47B, makes Goombas die when jumped on
;db $30		;10

;org $07F27C	;x3F47C, makes Winged Goombas "die" when jumped on
;db $30		;10

org $07F47C	;x3F67C, makes Flying Red Coins immune to fireballs
db $38		;28

;;;;;;;;;;;;;;MESSAGE BOX FIX;;;;;;;;;;;;;;

org $05B129

db $EA,$EA,$EA

org $05B296

db $0C


;;;;;;;;;LAYER PRIORITY STUFF;;;;;;;;;;;;;;;


org $0584BE		;\ Enable Priority for Level Mode 07 & 08
db $20,$20		;/

org $0584C1		;\ Enable Priority for Level Mode 0A
db $20			;/

org $0584C3		;\ Enable Priority for Level Mode 0C, 0D & 0E
db $20,$20,$20		;/

org $0584C8		;\ Enable Priority for Level Mode 11
db $20			;/

org $0584D5		;\ Enable Priority for Level Mode 1E & 1F
db $20,$20		;/

;;;;;;;KOOPA REMAP;;;;;;;;;;;;;;;;;;;;;;;;;;;;

org $019B8C	; Shelless koopa tilemap (Fourth byte unused?)
db $E0,$E2,$E2,$CE,$E4,$86,$4E

org $019B93	; Blue shelless koopa tilemap (Fourth byte unused?)
db $E0,$E2,$E2,$CE,$E4,$E0,$E0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Magikoopa Palette Correction Patch ;;
;; by andy_k_250		      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The lightest blue color in Magikoopa's	  ;;
;; fade-in/fade-out palette table was		  ;;
;; inverted in the original game for some reason. ;;
;; This patch corrects that problem.		  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

org $03B90C	;x1BB0C
db $00,$28	;92 7E

org $03B91C	;x1BB1C
db $40,$34	;2F 72

org $03B92C	;x1BB2C
db $A3,$40	;CC 65

org $03B93C	;x1BB3C
db $06,$4D	;69 59

org $03B94C	;x1BB4C
db $69,$59	;06 4D

org $03B95C	;x1BB5C
db $CC,$65	;A3 40

org $03B96C	;x1BB6C
db $2F,$72	;40 34

org $03B97C	;x1BB7C
db $92,$7E	;00 28


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Sprite Block Fix - Moves all Sprite Blocks up by 1 pixel
;
;All I've done is add Sprites 4C, 59 and 5A,
;the Exploding Block Sprite and Turn Block Bridges,
;to this patch. This is still Edit1754's work.
;                                           -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

org $018215				;Sprite 4C, Exploding Block w/ Fish/Goomba.Koopa
		dw Fix
org $01822F				;Sprite 59, Turn Block Bridge (H+V)
		dw BlkSprInit
org $018231				;Sprite 5A, Turn Block Bridge (H only)
		dw BlkSprInit
org $0182EF				;Sprite B9, Info Box
		dw BlkSprInit
org $01830D				;Sprite C8, Light Switch Block
		dw BlkSprInit

org $01CD1E
Fix:		JSR $83A4
		JMP BlkSprInit

org $01FFF0
BlkSprInit:	LDA.b $D8,x
		SEC
		SBC.b #$01
		STA.b $D8,x
		LDA.w $14D4,x
		SBC.b #$00
		STA.w $14D4,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Non-Dynamic Podoboo Patch, by imamelia
;;
;; This patch changes the way the Podoboo uses graphics.  Instead of using the
;; tiles normally used by Yoshi, it instead uses graphics from SP3 or SP4 and gets
;; them the same way any other sprite would.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; change the Podoboo's tilemap
org $019C35
db $C8,$C8,$D8,$D8,$C9,$C9,$D9,$D9,$D8,$D8,$C8,$C8,$D9,$D9,$C9,$C9
; 06 06 16 16 07 07 17 17 16 16 06 06 17 17 07 07

; change a JSR to a JMP, bypassing the dynamic GFX routine
org $01E19A
db $4C

; make the sprite use the second GFX page
org $07F431
db $34



;;;;;COUNTER BREAK Y;;;;;;;;;;;;;;;;;

ORG $A0D5
;NOP #8				;Instead of wasting 16 cycles we'll just skip those NOPs

		BRA ORLY	;2 bytes long - 3 cycles
		NOP #6		;We skipped 18 cycles meaning we saved 15 cycles :O		
ORLY:		STZ $0DBA,x	;No Yoshi color (of Player 1 and Player 2)

ORG $0491C9

		BRA YARLY	;\
		NOP		;/Saved 3 cycles
YARLY:		STZ $0DC1	;i have no idea i dont even
		STZ $13C7	;No Yoshi color
		STZ $187A	;No riding Yoshi flag

;;;;;CLASSIC FIREBALL;;;;;;;;;;;;;;;;;

org $02A129
LDA #$02    ; / Make sprite fall down...
STA $14C8,x ; \ ... or disappear in smoke (depends on its settings)
LDA #$05    ; 5 = 100. Changing it will change the amount of score you get (1=10,2=20,3=40,4=80,5=100,6=200,7=400,8=800,9=1000,A=2000,B=4000,C=8000,D=1up)
JSL $82ACEF ; Jump to the score routine handler.
NOP
NOP

;;;;;SPIKE TOP FIX;;;;;;;;;;;;;;;;;;;;

org $02BDA7
NOP #12

;;;;;1 Player Only;;;;;;;;;;;;;;
ORG $009DFA
	INC $0100
	BRA +
	
ORG $009E0B
	+
	LDX #$00	;00 = 1 player, 01 = 2 player
	
ORG $05B872
	db $FF

; Reznor fireball remap for Frank
org $02A187
and.b #$03
phx
tax

org $02A1E5
	jml remap_reznor_fireballs
remap_reznor_fireballs_return_no_remap:

org $02A1F9
remap_reznor_fireballs_return_remap:

	



	
;;;other fixes added by MDP on 6-7-14;;;;;
; org $008DFF	; Palettes Star in item box flash through. Removed because it breaks the status bar
; db $02,$04,$08,$0A
org $01C616	; Palettes Star flash through.
db $04,$06,$08,$0A
org $019E20	; Palette/Priority/GFX Flip of Parakoopa's wings.
db $42,$42,$02,$02
org $018DDF	; Palette/Priority/GFX Flip of Para-Goomba's wings.
db $42,$02
; org $02BB1B	; Yoshi Wings YXPPCCCT. 
; db $42,$42,$02,$02 

org $01FA37		; Boo Block Tilemap
db $88,$C8,$CA
org $01FA2F		; Makes all three tiles of the Boo Block use the same palette.
db $09,$03,$EA

org $038E08		; Tilemap properties of timed lift.
db $0B,$4B,$03

; yoshi wings fix by lx5
org $02BB0B
	db $02,$FA,$06,$06
	db $00,$FF,$00,$00
	db $10,$08,$10,$08
	db $5D,$C6,$5D,$C6
	db $42,$42,$02,$02	; but this was also edited by MDP (was 46,06)
	db $00,$02,$00,$02
org $02BB23
	STA $02
	
org $04F709	; no thunders on the ow
	db $FF

org $00A09C ;level C5 timer
	db $01

; This fixes the problem where yoshi doesn't animate on the goal walk
; ora.w $13C6
org $01EA92
    bra yoshi_cutscene_fix

org $01EA95
	yoshi_cutscene_fix:

org $80DAA9
	jml fix_mario_power_down_key_underwater_glitch
	fix_mario_power_down_key_underwater_glitch_return:
	rts

; Make game over always send you back to the title screen.
org $80A0F9
	nop #2

org $849149
	jml allow_switch_palace_reenter

org $849150
	allow_switch_palace_reenter__not_enter:

org $84919F
	allow_switch_palace_reenter__enter:


;This should make falling spikes invincible to stars, to remove glitched graphics
org $07F4C7+$B2
db read1($07F4C7+$B2)|%00000010
;Undo
;db read1($07F4C7+$B2)&%11111101

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Code goes here
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

freecode
	fix_explode_kill_spr:
		lda $009E,y
		cmp.b #$9E ; Don't kill ball n' chain
		beq .dont_kill
			LDA.b #$02
			STA.w $14C8,y
			LDA.b #$C0
			STA.w $00AA,y
			LDA.b #$00
			STA.w $00B6,y
		.dont_kill
			jml fix_explode_kill_spr_return
        fire_tiles:
		db $EA,$EB,$EA,$EB

	remap_reznor_fireballs:
		lsr
		and.b #$03
		tax
		rep #$20
		lda $010b
		cmp.w #$00D6
		sep #$20
		beq +
		jml remap_reznor_fireballs_return_no_remap
		+
		lda.l fire_tiles,x
		sta.w $0202,y
		lda.w $02A15F,x
		eor $00
		ora $64
		ora #$01
		sta $0203,y
		jml remap_reznor_fireballs_return_remap

	fix_lakitu:
		CMP #$18
		BCS nope
		LDA $9D
		BNE nope
		JML $82E6D7
		nope:
		JML $82E6EB			; ^ to here

	fix_mario_power_down_key_underwater_glitch:
		lda.b $71
		cmp.b #$01
		beq .skip
			LDA.b #$0E
			STA.w $1DF9				;$00DAAB	|
			LDA.w $1496				;$00DAAE	|
			ORA.b #$10				;$00DAB1	|
			STA.w $1496				;$00DAB3	|
		.skip
		jml fix_mario_power_down_key_underwater_glitch_return

	allow_switch_palace_reenter:
		lda.w $13C1
		cmp.b #$81
		beq +
		cmp.b #$85
		beq +
		cmp.b #$86
		beq +
		jml allow_switch_palace_reenter__not_enter
		+
		jml allow_switch_palace_reenter__enter
