;Modified Stripe Images for File Select Menus + Modified Exit Count Handler - authors?
;blindnote: mostly untouched from what I've copied, though some stuff deemed unnecessary was cut out.
;some hijacks considered trivial were moved to the hex edits patch.

!exit_count = $6A

;SA-1 detector (though I honestly don't think the hack should or would be converted at some point)
!sa1	= 0
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

org $009D6C
	autoclean jml new_exit_numbers
org $009DA6
	new_exit_numbers_jml_return:

;tweak this whenever the stripes are edited
org $009D55 : LDA #$8C
org $009DAB : SBC $AA
org $009D96 : LDY #$01

org $009D3A
autoclean JML stuff
org $009B7B
autoclean JML ultrastuff

freecode

stuff:
STZ $05
CPX #$CB
BEQ FileSelectCode
BRA EraseFileSelectCode

FileSelectCode:
PHB : PHK : PLB
STZ $0DDE|!addr
LDX #$00
REP #$10
LDY.w #$0000
-
LDA.l FileSelectStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #FileSelectStimEnd-FileSelectStimStart+$0001
BNE -
LDA #$32
STA $AA
PLB
JML $009D53|!bank

EraseFileSelectCode:
PHB : PHK : PLB
LDX #$00
REP #$10
LDY.w #$0000
-
LDA.l EraseFileSelectStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #EraseFileSelectStimEnd-EraseFileSelectStimStart+$0001
BNE -
SEP #$10
PLB
JML $009DB4|!bank

ultrastuff:
PHB : PHK : PLB
LDA $05
LDX #$00
LDA #$0A
STA $AA
REP #$10
LDY.w #$0000
-
LDA.l UltraStimStart,x
PHX : TYX
if !sa1
STA $40837D,x
else
STA $7F837D,x
endif
PLX : INX : INY
CPY.w #UltraStimEnd-UltraStimStart+$0001
BNE -
SEP #$10
LDA #$16
PLB
JML $009D57|!bank

FileSelectStimStart:
incbin "stims/title/fileselect4.stim"
FileSelectStimEnd:

EraseFileSelectStimStart:
incbin "stims/title/erasefileselect4.stim"
EraseFileSelectStimEnd:

UltraStimStart:
incbin "stims/title/ultra.stim"
UltraStimEnd:

new_exit_numbers:
	ldx $00
	cmp.b #!exit_count
	bcc +
	lda.b #$16
	jsr draw_tile
	inx #2
	lda.b #$17
	jsr draw_tile
	inx #2
	lda.b #$18
	jsr draw_tile
	bra .return
	+
	pha
	lda.b #$FD
	jsr draw_tile
	inx #2
	jsr draw_tile
	inx #2
	jsr draw_tile
	pla
	jsr hex_to_dec
	cpy #$00
	bne +
		dex #2 ; Move the tile back one space if we have a 1 digit number here
	+
	jsr draw_tile
	dex #2
	tya
	beq .return
	jsr hex_to_dec
	jsr draw_tile
	dex #2
	tya
	beq .return
	jsr draw_tile
	.return
	jml new_exit_numbers_jml_return

draw_tile:
	pha
	clc
	adc #$22
if !sa1
	sta $40837F,x
else
	sta $7F837F,x
endif
	lda.b #$39
if !sa1
	sta $408380,x
else
	sta $7F8380,x
endif
	pla
	rts

;Easier to copy this routine than deal with jslrts
hex_to_dec:
	ldy.b #$00
-
	cmp.b #$0A
	bcc +
	sbc.b #$0A
	iny	
	bra -
	+
	rts