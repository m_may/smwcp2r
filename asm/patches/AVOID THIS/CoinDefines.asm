@include

;this file isn't meant to be applied directly to the ROM.

!CheckpointRAM = $7FC700	; 96 bytes of ram for the midway patch $7FC700

!ramspace = $7FC770		; needs a 41 ($29) bytes string of RAM.$7FC770
!TempCounter = !ramspace+37	; 4 bytes of regular RAM.
!Bytes	= #$28			; amount of bytes in total - 1

!hibyte = #$13			; High byte(map16page) of the blocks
!lobyte1 = #$0C			; low byte(map16spot) of the first block in the first row of 4 blocks
!lobyte2 = #$1C			; low byte(map16spot) of the first block in the second row of 4 blocks
!lobyte3 = #$2C			; low byte(map16spot) of the first block in the third row of 4 blocks
!lobyteb = #$3C			; low byte(map16spot) of the first blank coin tile
!lobytem = #$4C			; low byte(map16spot) of the first midway point block

; !SSM16space = $218000 		;Freespace for the shared SubSetMap16 subroutine(was 20cb00)

;Coinsounds
!SFX = #$1C			; Which sound to play upon collecting coins
!IObank = $1DF9			; Currently set to Yoshi coin sound