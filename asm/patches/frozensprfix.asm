;Freeze fix, by GreenHammerBro.
;This patch fixes sprite glitches not obeying ram address $7E009D, for
;example, shelless koopas and sumo brother's lightning continue to fall.
;This also fixes a bug where if you freeze while red or blue koopas
;(turns around on edges) are on the very edge of the platform, will fall
;off.

;*11/27/2015 UPDATE: fix the crash bug due to a bank changing error.
;*1/2/2016 UPDATE: fix a problem that the shooter timer doesn't use $14
; (it does use $9D, but can decrement twice if freezes and unfreezes).

!UsingCusSprites = 1		;>If you are using custom sprites, then set this to 1.
!SMA2ChangingItm = 0		;>Change this to 1 if you want changing item to play sfx.

;^Do note that patching while enabled before disabling and patching again
;DOES NOT remove that feature. Therefore, backup your rom!

!ChangingItemSfxNumb	= $23	;\Sma2 styled changing item.
!ChangingItemSfxRam	= $1DFC	;/>This is before being added by sa-1 base, so use original ram map.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Do not change anything below here.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if read1($00FFD5) == $23
	!SA1 = 1
	sa1rom
else
	!SA1 = 0
endif

; Example usage
if !SA1
	; SA-1 base addresses	;Give thanks to absentCrowned for this:
				;http://www.smwcentral.net/?p=viewthread&t=71953
	!Base1 = $3000		;>$0000-$00FF -> $3000-$30FF
	!Base2 = $6000		;>$0100-$0FFF -> $6100-$6FFF and $1000-$1FFF -> $7000-$7FFF

	!SprTbl_1540	= $32C6
	!SprTbl_7FAB10	= $6040
	!SprTbl_9E	= $3200
	!SprTbl_D8	= $3216
	!SprTbl_14D4	= $3258
	!SprTbl_AA	= $309E
	!SprTbl_1588	= $334A
	!SprTbl_187B	= $3410
	!SprTbl_B6	= $30B6
else
	; Non SA-1 base addresses
	!Base1 = $0000
	!Base2 = $0000

	!SprTbl_1540	= $1540
	!SprTbl_7FAB10	= $7FAB10
	!SprTbl_9E	= $9E
	!SprTbl_D8	= $D8
	!SprTbl_14D4	= $14D4
	!SprTbl_AA	= $AA
	!SprTbl_1588	= $1588
	!SprTbl_187B	= $187B
	!SprTbl_B6	= $B6
endif


org $02DEB0			;\Fix sumo's lightning
autoclean JML SumoLightning	;|
nop #1				;/

org $018922					;\Freeze shelless koopas' position ($7E00D8/$7E14D4) from changing.
autoclean JSL FallingShellessKoopasVelocity	;|As well as conveyors still carrying them.
nop #1						;/

org $019049					;\Fix gravational acceleration for the shelless koopas
autoclean JSL FallingShellessKoopasGravAccel	;|(without this, they will freeze as normal, but after unfreezing
nop #4						;/they will "boost down" due to $7E00AA "charging up" or increasing while not moving).

org $01C321			;\Hey, Mr. changing item, obey the law of $7E009D!
autoclean JML ChangingItem	;|
nop #2				;/

if !SMA2ChangingItm != 0
	org $01C330			;\Also add a sfx like Super Mario Advance 2 remake.
	autoclean JSL ChangingItemSfx	;|
	nop #1				;/
endif

org $01C6A6			;\hey, star, don't use ram address $7E0013! Use $7E0014 instead.
db $14				;/(this makes the star palette flash various colors).

org $03C4BC			;\You too, dark room spotlight.
db $14				;/

org $02B39C			;\And shooter timer (it does check $9D in the first place, but can
db $14				;/double-decrement if it freezes AND unfreezes), for $17AB.

freecode : print pc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SumoLightning:
	LDA $9D
	BNE .SumoLightningFrozen
	LDA !SprTbl_1540,x
	BNE +
	JML $02DEB5
+	JML $02DEFC
	
.SumoLightningFrozen
	JML $02DEEA

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FallingShellessKoopasVelocity:
	LDA $9D			;\If freeze flag set, then don't update position.
	BNE +			;/

;	PHK			;\Thanks K3fka for the JSL-RTS trick!
;	PEA.w .jslrtsreturn-1	;|
;	PEA.w  $0180CA-1	;|
;	JML $019032		;/>Sprite movement and block interaction routine.
;.jslrtsreturn

	JSL $01802A		;>Even shorter, thanks LX5!


	STZ !SprTbl_B6,x	;>[restore code] Set sprite x speed to 0.
+
	RTL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FallingShellessKoopasGravAccel:
	LDA $9D			;\time not frozen,...
	BEQ .Normal		;/
if !UsingCusSprites != 0
	LDA !SprTbl_7FAB10,x 	;\...or if its a custom sprite,...
	AND #$08		;|
	BNE .Normal		;/
endif
	LDA !SprTbl_9E,x	;\...or other than the 4 shelless koopas
	CMP #$04		;|then don't freeze gravity.
	BCS .Normal		;/
	RTL

.Normal
	LDA !SprTbl_AA,x	;\restore code, not for shelless koopas.
	CLC			;|
	ADC.w $019030,Y		;|
	STA !SprTbl_AA,x	;|
	RTL			;/
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ChangingItem:
	LDA $9D			;\time frozen, then don't increment $187B.
	BNE +			;/

	INC !SprTbl_187B,x	;\Restore code.
	LDA !SprTbl_187B,x	;|
	JML $01C327		;/

+	LDA !SprTbl_187B,X	;>restore code for frozen.
	JML $01C327
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if !SMA2ChangingItm != 0
ChangingItemSfx:
	LDA.w $01C313,y			;\Restore changing item's changing routine
	STA !SprTbl_9E,x		;/
	LDA !SprTbl_187B,X		;\play sfx at the start of the frame period of each item.
	AND.b #%00111111		;|
	BNE +				;|
	LDA #!ChangingItemSfxNumb	;|
	STA !ChangingItemSfxRam+!Base2	;/
+
	RTL
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;