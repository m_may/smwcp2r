@includefrom "MMP16.asm"

incsrc ../../CommonDefines.asm
!total_midpoints	= $0003

level_mid_tables:
;levels 0-F
dw $0000 : dw $0000 : dw $0000				; 
dw $0001 : dw $0001 : dw $0001	; Hub			; place the level number which you want to use here
dw $0002 : dw $0002 : dw $0002	; Girder Grasslands	; 
dw $0003 : dw $0003 : dw $0003	; Plateau Pumps		; IE: $0140 will use level 140's midway entrance
dw $0004 : dw $0004 : dw $0004	; Hiker's Hollow	; $00FF will use level 0FF's midway entrance
dw $0005 : dw $0005 : dw $0005	; Smoggy Steppe		; $0100 will use level 100's
dw $0006 : dw $0006 : dw $0006	; Switch Scamper	; ect
dw $0007 : dw $0007 : dw $0007	; Velocity Valves	; setting the highest nybble (making it 1000+ )
dw $00B1 : dw $0008 : dw $0008	; Countdown Chamber
dw $005C : dw $100B : dw $100C	; Reclaimed Refinery
dw $000A : dw $000A : dw $000A	; Lily Swamp Romp	; will use a secondary entrance instead
dw $006A : dw $1190 : dw $006D	; Blazing Brush		; IE: $1003 will use secondary entrance 003
dw $000C : dw $000C : dw $000C	; Floral Fears		; $1138 will use secondary entrance 138
dw $01AA : dw $000D : dw $000D	; Manic Mine		; $1000 will use secondary entrance 000
dw $000E : dw $000E : dw $000E	; Fringe Forest		; ect
dw $0030 : dw $000F : dw $000F	; Treetop Toss-Up
;levels 10-1F
dw $0010 : dw $0010 : dw $0000	; Arboreal Ascent
dw $0047 : dw $0048 : dw $0000	; Briar Mire
dw $0012 : dw $0012 : dw $0000	; Akarui Avenue
dw $0013 : dw $0013 : dw $0000	; Pon-Pon Palace
dw $0034 : dw $0014 : dw $0000	; Onsen Overhang
dw $0015 : dw $0015 : dw $0000	; Frantic Fugumanen
dw $0016 : dw $0016 : dw $0000	; Koura Crevice
dw $00EA : dw $10EC : dw $0000	; Shenmi Frica-Sea
dw $0018 : dw $00F1 : dw $101C	; Tourou Temple
dw $009B : dw $0019 : dw $0000	; Tropical Turnout
dw $001A : dw $1195 : dw $0000	; Beach Ball Brawl
dw $0058 : dw $001B : dw $0000	; Hydrostatic Halt
dw $001C : dw $001C : dw $0000	; Coral Corridor
dw $003F : dw $001D : dw $0000	; Crocodile Crossing
dw $0053 : dw $001E : dw $0000	; Seaside Spikes
dw $00EF : dw $00EE : dw $0000	; Stone Sea Sanctuary
;levels 20-24
dw $0020 : dw $0039 : dw $0000	; Sunset Split
dw $0021 : dw $0021 : dw $0000	; Gunky Grotto
dw $0022 : dw $0022 : dw $0000	; Scorching Sepulcher
dw $00B4 : dw $0023 : dw $0000	; Emerald Escapade
dw $0024 : dw $0024 : dw $0000	; Frivolous Fires
;levels 101-10F
dw $0089 : dw $0101 : dw $0000 	; Crumbling Catacombs
dw $019E : dw $01A0 : dw $01A2 	; Digsite Dangers
dw $0103 : dw $0146 : dw $0000 	; Opulent Oasis
dw $0104 : dw $0174 : dw $0000	; Jewel Jubilation
dw $0105 : dw $0105 : dw $0000 	; Ice-Floe Inferno
dw $0106 : dw $0106 : dw $0000	;Warp Pipes from Grass <-> Oriental
dw $0107 : dw $0107 : dw $0000	;Warp Pipes from Forest <-> Grass
dw $0108 : dw $101D : dw $101E 	;Warp Pipes from Oriental <-> Reef 1 and Oriental <-> Reef 2
dw $0109 : dw $0109 : dw $0109	;Warp Pipes from Desert <-> Carnival
dw $010A : dw $010A : dw $0000	;Warp Pipes from Desert <-> Sky
dw $010B : dw $0155 : dw $1155 	;Warp Pipes from Carnival <-> Fire-Ice (ice portion) and Factory 3 <-> Asteroid
dw $010C : dw $010C : dw $0000	;Warp Pipes from Carnival <-> Sky
dw $010D : dw $010D : dw $0000  ;Warp Pipes from Sky <-> Fire-Ice (fire portion)
dw $010E : dw $110D : dw $110E 	;Warp Pipes from Fire-Ice <-> Factory, Factory <-> Factory 1
dw $00AA : dw $010F : dw $0000 	; Terrifying Timbers
;levels 110-11F
dw $0090 : dw $0110 : dw $0000	; Carnival Caper
dw $0111 : dw $0111 : dw $0000	; Sideshow Showdown
dw $00FF : dw $00FC : dw $0000	; Coaster Commotion
dw $00E8 : dw $0113 : dw $0000	; Balloon Bonanza
dw $0114 : dw $0114 : dw $0000	; Playground Bound
dw $017C : dw $0186 : dw $0000	; Fantastic Elastic
dw $0116 : dw $0116 : dw $0000	; Tropopause Trail
dw $00C9 : dw $00C9 : dw $0000	; Dynamic Disarray
dw $0031 : dw $0031 : dw $0031	; Aerotastic Assault
dw $007D : dw $0119 : dw $0000	; Radiatus Ruins
dw $011A : dw $011A : dw $0000	;Warp Pipes from Desert <-> Desert
dw $0179 : dw $011B : dw $0000	; Cumulus Chapel
dw $011C : dw $011C : dw $0000	; Artillery Auberge
dw $0055 : dw $011D : dw $0000	; Dangling Danger
dw $00B9 : dw $011E : dw $0167	; Gliding Garrison
dw $011F : dw $0189 : dw $0000	; Stratus Skyworks
;levels 120-12F
dw $0120 : dw $0120 : dw $0000	; Callous Cliffs
dw $0164 : dw $0165 : dw $0167	; Stormy Stronghold
dw $0122 : dw $0122 : dw $0000	; Lava Lift Lair
dw $10A6 : dw $0123 : dw $0000	; Crystalline Citadel
dw $0124 : dw $0124 : dw $0000	; Shivering Cinders
dw $002A : dw $002A : dw $0000	; Frigid-Fried Frenzy
dw $01E6 : dw $0126 : dw $0000	; Frostflow Freezer
dw $0127 : dw $0127 : dw $0000	; Volcanic Panic
dw $0159 : dw $0128 : dw $0000	; Smoldering Shrine
dw $0129 : dw $0129 : dw $0000	; Chilly Colors
dw $012A : dw $112C : dw $0098	; Pyro-Cryo Castle
dw $012B : dw $01DD : dw $0000	; Rusted Retribution
dw $012C : dw $012C : dw $0000	; Incineration Station
dw $012D : dw $012D : dw $0000	; Petroleum Passage
dw $012E : dw $012E : dw $0000	; Musical Mechanisms
dw $005F : dw $012F : dw $0000	; Synchronized Sector
;levels 130-13B
dw $0162 : dw $0130 : dw $0000	; Myrrky Mainline
dw $0184 : dw $0184 : dw $0185	; Perilous Paths
dw $0132 : dw $1144 : dw $0143	; Nexus Norvegicus
dw $00E0 : dw $0133 : dw $0000	; Reverse Universe
dw $0134 : dw $0134 : dw $0000	; Candy Calamity
dw $0096 : dw $0135 : dw $0000	; Monotone Monochrome
dw $0082 : dw $0083 : dw $119F	; EREHPS EHT RAEF
dw $0137 : dw $0137 : dw $0000	; Crystal Conception
dw $0138 : dw $0138 : dw $0000	; Honeydew Hills
dw $01B1 : dw $0139 : dw $0000	; Bedazzling Bastion
dw $0153 : dw $0150 : dw $0000	; Asteroid Antics
dw $0144 : dw $014F : dw $014B	; awakening