!TwoPlayers = 0
!ValuesFromSRAM = 0

;SA-1 detector
if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
else 
	!sa1	= 0
	!addr	= $0000
	!bank	= $800000
	lorom
endif

incsrc ../../CommonDefines.asm
incsrc ../../Version.asm

if !ValuesFromSRAM

;Preventing the game to reset values to defaults - by Blind Devil
;hint - take a look at $009E1C in SMWDisC

org $009E1C
BRA + : NOP   ;branch ahead.
org $009E5C
+             ;we skipped resetting several values (lives, coins, Yoshi color, powerup, reserve, score) to defaults.

else

;By Disk Poppy

org $009E13
autoclean JML ResetMidpointsAndSetLivesAlsoOW
org $009E2C
HijackBack:

freecode : print pc
TNK:
LDA #$3D  ;TNK fade in gamemode
STA $0100
LDA #$09  ;bullet bill sfx
STA $1DFC
JML $009B16|$800000  ;rts in bank $00

ResetMidpointsAndSetLivesAlsoOW:
.checkTNK
LDA !TNKMode
BNE TNK
.checkOverworldReset
LDA !ResetOW
BEQ .OWDone2
DEC  ;CMP #$01
BNE +
PHK
PEA.w .OWDone-1
PEA $84CE
JML $009F1D|!bank
+
DEC  ;CMP #$02
BNE +
LDA #!CurrentOWVersion
STA !OWVersionRAM
EOR #$FF
STA !OWVersionComplementRAM
LDA !PerilousPathsCleared : PHA
LDA !Collectibles : PHA
PHK
PEA.w .resetDone-1
PEA $84CE
JML $009F06|!bank
.resetDone
PLA : STA !Collectibles
PLA : STA !PerilousPathsCleared
+
.OWDone
LDA #$00
STA !ResetOW
.OWDone2
JSL $04DAAD|!bank  ;load overworld and events

.checkCheats
LDA !Cheats
AND #$02  ;check for bit 1
BNE .noReset
LDX #$5F
.resetMidpointsAndTempSMWCCoinsLoop
LDA !CheckpointsSMWCCoins,x
AND #$07
STA !CheckpointsSMWCCoins,x  ;reset temporary SMWC Coins and Multi Midway Points
LDA $1EA2|!addr,x
AND #$BF  ;complement of #$40
STA $1EA2|!addr,x  ;reset midpoints
DEX
BPL .resetMidpointsAndTempSMWCCoinsLoop
.noReset
;calucate number of lives based on SMWC Coins (temp)
.setLives
LDA !SMWCCoinTotalCount
LSR #3
CLC : ADC #$04
if !TwoPlayers
	LDX $0DB2|!addr
	-
	STA $0DB4|!addr,x 
	BPL -
endif
JML HijackBack|!bank

endif
