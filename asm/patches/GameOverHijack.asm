;As a side effect of this "hijack" TIME UPs are treated like game over,
;it would need to be turned into a real hijack and at a different location
;(at $00D0E2 I think).

!CustomGameOverFadeGamemode = $35  ; refer to uberasmtool/list.txt

if read1($00FFD5) == $23
	sa1rom
	!addr = $6000
else
	lorom
	!addr = $0000
endif

org $00D0F8
LDA #$00
STA $143C|!addr
LDA #$03
STA $143D|!addr
LDY #!CustomGameOverFadeGamemode
