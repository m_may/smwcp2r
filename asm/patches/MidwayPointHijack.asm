;Midway Point block/object code hijack by Blind Devil
;Just a little patch that makes SMW's midway point trigger Kaijyuu's midway flag 0,
;and changes the object routine so it doesn't display if it's been triggered.

!sa1	= 0			; 0 if LoROM, 1 if SA-1 ROM.
!dp	= $0000			; $0000 if LoROM, $3000 if SA-1 ROM.
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.
!bank8	= $80			; $80 if LoROM, $00 if SA-1 ROM.

if read1($00ffd5) == $23
	!sa1	= 1
	!dp	= $3000
	!addr	= $6000
	!bank	= $000000
	!bank8	= $00
	sa1rom
endif

incsrc ../../CommonDefines.asm
!OWLevelNum = $13BF|!addr

;Checkpoint to trigger value.
!CheckpointNumber = $00

org $00F2D8  ;WARNING - LUNAR MAGIC HIJACKS THIS CODE TOO, IF THERE IS A CRASH ON TOUCHING THE MIDPOINT IT'S LIKELY THIS...
autoclean JSL MidpointBlock	;call our code.
NOP #4				;no operation 4 times - erase leftovers.

org $0DA699
autoclean JML MidpointObject	;jump to our code.
NOP				;no operation once - erase leftovers.

freecode : print pc
MidpointBlock:
PHX				;preserve whatever's in X
LDA $00 : PHA			;preserve $00 (just in case)

LDA $7FC060			;load CDM16 flags address 1
AND #$07				;preserve bits 0, 1 and 2
ASL #3				;shift bits to the left 3 times
STA $00				;should be free to use, if not - push it

LDX !OWLevelNum			;load OW number on X
LDA !CheckpointsSMWCCoins,x
AND #$3F
if !CheckpointNumber != $00
	ORA #!CheckpointNumber<<6
endif
ORA $00
STA !CheckpointsSMWCCoins,x	;store to checkpoint table, indexed.
LDA #$01			;load value
STA $13CE|!addr			;store to "midway point collected" flag.

PLA : STA $00			;restore $00
PLX				;restore X
RTL				;return.

MidpointObject:
LDA $13CE			;load "midway point collected" flag.
BEQ .draw			;if not set, always draw bar.

LDX !OWLevelNum			;load OW number on X
LDA !CheckpointsSMWCCoins,x	;load checkpoint table, indexed
AND #$C0
if !CheckpointNumber != $00
	CMP #!CheckpointNumber<<6		;compare to checkpoint value
endif
BNE .draw			;if not equal, bar can be drawn.

JML $8DA6B0			;jump back to original code - return, don't draw bar.

.draw
JML $8DA69E			;jump back to original code - draw midway point bar.