;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Cloud Timer Warning 
;This patch modifies lakitu's stolen cloud to indicate when it's use is about to end by adding blinking and sound effect, like in SMA2.
;By RussianMan. Credit is optional.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if read1($00FFD5) == $23	;sa-1 compatibility
  sa1rom
  !addr = $6000
  !1594 = $3360
  !15AC = $338C
  !186C = $7642
else
  !addr = $0000
  !1594 = $1594
  !15AC = $15AC
  !186C = $186C
endif

!WarningTime = $2E		;when start blinking. do note that timer used decrements every 4 frames.

!WarningSound = $00		;0 - disable sound effect, if you want to
!WarningBank = $1DFC|!addr	;

!BlinkTiming = $02		;every x frames it'll blink and do sound effect

org $01E7B0
autoclean JSL FlagOrNot

org $01E8CD
autoclean JML GfxOrNot
NOP

freecode : print pc

FlagOrNot:
LDA $18E0|!addr			;if it's not time to blink just yet
CMP #!WarningTime		;
BCS .NoMess			;do only original's timing

LDA !15AC,x			;do blinking with custom timing
BNE .NoMess			;

INC !1594,x			;flag - blink, blink, show, hide, you know the drill.

LDA #!BlinkTiming
STA !15AC,x

if !WarningSound		;if sound effect is disabled, this is useless
  LDA #!WarningSound		;
  STA !WarningBank		;
endif				;

.NoMess
LDA $14				;\[restored code] (cloud's timer timing)
AND #$03			;/
RTL

GfxOrNot:
LDA !186C,x			;\[restored code] (check if vertically offscreen)
BNE .Re				;/

LDA $18E0|!addr			;always show if it's timer isn't coming to a end
CMP #!WarningTime		;
BCS .Show			;

LDA !1594,x			;
AND #$01			;
BEQ .Show			;

.Re
JML $01E897

.Show
JML $01E8D2

;some notes used during creation of this patch.
;!1594 - flag for showing cloud's GFX (previously unused misc. ram)
;!15AC - used for blinking timing, because using frame counter and AND command is limiting
;$18E0 - Cloud's timer