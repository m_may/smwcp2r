;A small hijack I made to the switch palace switch so it generates SFX when pressed.
;Added permament "switches unlocked" flag handling

;SA-1 detector (though I honestly don't think the hack should or would be converted at some point)
!sa1	= 0
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

incsrc ../../CommonDefines.asm


org $00EEB3
autoclean JML Switch
NOP : NOP : NOP
SwitchHijackBack:

freecode
Switch:
LDA.l ColorOnOW,x
STA $13D2|!addr  ;overworld switch palace color
LDA #$0B
STA $1DF9|!addr  ;play sfx
LDA #$01
STA $1F27,x
STA !SwitchesUnlocked,x
JML SwitchHijackBack|!bank

ColorOnOW:
db $04,$01,$02,$03
