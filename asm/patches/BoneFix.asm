;dry bones' bone got messed up for apparently no reason so I fix that manually, kinda
;NOTE: This isn't patched through the batch by default. Do it manually in case it's needed (that is, whenever the bone glitches).

!Tile1 = $80
!Tile2 = $82

lorom

org $02A2C6
autoclean JML FixDatBone

freedata

FixDatBone:
LDA $0202,y
CMP #$20
BEQ +

LDA #!Tile1
BRA ++

+
LDA #!Tile2
++
STA $0202,y

LDA $0203,y
AND #$F1
ORA #$03
STA $0203,y

JML $82A2DE