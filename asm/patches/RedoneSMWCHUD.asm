;New SMWCP2 Status Bar by Blind Devil
;Thanks to Erik's NSMBWii Status Bar patch, I was able to make a superior status bar for SMWC,
;based off of the old one, but taking a patch of today to make it.
;YAY!

;Note that the status bar relies on CDM16 flags 0, 1 and 2 for coins collected now.
;No need to use other RAM addresses for it.

; RAM addreses (you can change them if you want, however
; it's not really needed.)

       !LivesCounterXPos     = #$08
       !LivesCounterYPos     = $0EFA
       !CoinCounterXPos      = #$E8
       !CoinCounterYPos      = $0EFC
       !SCCounterXPos        = #$60
       !SCCounterYPos        = $0EFE
       !StatusBarDisable     = $0EFF

;other RAM addresses added by blind devil
	!MoveBitflags = $0F00			;----LSC (whenever a bit is set, the respective counter moves down/is displayed)
	!LivesCounterTimer = $0F01		;timer that controls for how long the lives counter should be displayed
	!SCCounterTimer = $0F02			;timer that controls for how long the SMWC coin counter should be displayed
	!CoinCounterTimer = $0F03		;timer that controls for how long the coin counter should be displayed

; Customizable values

       !CoinsFor1Up    = 100       ; How many coins are needed for a 1-Up
                                   ; Decimal, maximum of 100.

	!TimersInit = $60	;amount of frames (plus the moving time) to display timers upon level init/status update
	!StopYPos = $08		;at which Y position should the counters stop moving when moving down to be displayed

; Tilemaps (GFX00/01)

       !PlayerHeadTile  = $24
       !FullStatusCoin  = $80
       !Number0         = $76
       !Number1         = $77
       !Number2         = $46
       !Number3         = $47
       !Number4         = $C2
       !Number5         = $C3
       !Number6         = $D2
       !Number7         = $D3
       !Number8         = $D4
       !Number9         = $D5
       !BlankStatusCoin = $44

; Nuke that old ugly bar
; (hijacks by Lui37)

org $0081F4
       BRA + : NOP : +
org $0082E8
       BRA + : NOP : +
org $008C81
       pad $009045
org $009051
       pad $0090D1
org $00985A
       BRA $01 : NOP
org $00A2D5
       BRA $01 : NOP
org $00A5A8
       BRA $01 : NOP
org $00A5D5
       BRA $01 : NOP
if read1($00F5F3) != $22    ; powerdown check
org $00F5F8
       BRA $02 : NOP #2
endif
org $01C540
       BRA + : NOP #11 : +

; Remap some tiles

org $00FBA4
       db $64,$64,$62,$60,$E8,$EA,$EC,$EA
org $01E985
       db $64,$64,$62,$60
org $028C6A
       db $64,$64,$62,$60
org $028D42
       db $68,$68,$6A,$6A,$6A,$62,$62,$62
       db $64,$64,$64,$64,$64
org $0296D8
       db $64,$62,$64,$62,$60,$62,$60
org $029922
       db $64,$62,$64,$62,$62
org $029922
       db $64,$62,$64,$62,$62
org $029C33
       db $64,$64,$62,$60,$60,$60,$60,$60
org $02A347
       db $64,$64,$60,$62
org $028ECC
       db $69
;============================================

; SA-1 check

	!dp = $0000
	!addr = $0000
	!bank = $800000
	!sa1 = 0
	!gsu = 0

if read1($00FFD5) == $23
	sa1rom
	!dp = $3000
	!addr = $6000
	!bank = $000000
	!sa1 = 1
endif

;============================================

; Hijacks

org $008292
       autoclean JML IRQHack

org $00A5AB
       autoclean JML StatusBarInit

org $00A2E6
       autoclean JML StatusBarMain

ORG $00A1DA
	autoclean JML run_pause

org $028AC3
	autoclean JML LivesHandlerHijack

freecode : print pc

print "Inserted at $",pc

;============================================

; Actual code

;=================================
; IRQ Hack
;=================================

IRQHack:
       PHB : PHK : PLB
       LDX $0100|!addr
       LDA .allowedModes,x
       BNE .disableIRQ

.defaultIRQ
       PLB
       LDY #$00
       LDA $4211
       JML $008297|!bank

.disableIRQ
       PLB
       LDY #$E0
       LDA $4211
       STY $4209
       STZ $420A
       LDA $0DAE|!addr
       STA $2100
       LDA $0D9F|!addr
       STA $420C
       JML $008394|!bank

.allowedModes
       db $00,$00,$00,$00,$00,$00,$00,$00
       db $00,$00,$00,$01,$00,$00,$00,$01
       db $00,$00,$01,$01,$01,$01,$00,$00
       db $00,$00,$00,$00,$00

;=================================
; INIT Code
;=================================

StatusBarInit:
        JSL $05809E|!bank                 ; restore hijacked code
        PHA                               ; preserve accumulator

; Set the position of every timer.

       LDA #$F0				;they all start offscreen
       STA !LivesCounterYPos|!addr
       STA !CoinCounterYPos|!addr
       STA !SCCounterYPos|!addr

	LDA #$07			;but all move down on level init
	STA !MoveBitflags|!addr

	LDA #$03
	STA $0EF9|!addr
	STA $0EFB|!addr
	STA $0EFD|!addr			;speeders (when asked to move down, move it fast)

; init displaying timers
	LDA #!TimersInit
	STA !LivesCounterTimer|!addr
	STA !SCCounterTimer|!addr
	STA !CoinCounterTimer|!addr

       PLA                               ; restore accumulator
       JML $00A5AF|!bank                 ; return

;=================================
; MAIN Code
;=================================

StatusBarMain:
	JSL $028AB1|!bank	; restore hijacked code
	JSL StatusProc		;<< called via JSL because the pause code also calls it
	JML $00A2EA|!bank	; return

StatusProc:
       PHP : SEP #$30              ;\ Preserve p.flags/8-bit AXY
       PHB : PHK : PLB             ;/ set data bank

; Rewrite the functions of the counters which were removed by hijacks above.

; coins
	LDA !StatusBarDisable|!addr
	BNE .handlecoinsNOW

	LDA $13CC|!addr
	BEQ +

	LDA #!TimersInit
	STA !CoinCounterTimer|!addr
	LDA #$01
	TSB !MoveBitflags|!addr

+
	LDA !CoinCounterYPos|!addr	;load coin counter's Y-pos
	CMP #!StopYPos			;compare to value
	BNE .NoCoinIncrease		;if not equal, skip coin handling.

.handlecoinsNOW	
       LDA $13CC|!addr
       BEQ .NoCoinIncrease
       DEC $13CC|!addr
       INC $0DBF|!addr
       LDA $0DBF|!addr
       CMP #!CoinsFor1Up
       BCC .NoCoinIncrease
       SEC : SBC #!CoinsFor1Up
       STA $0DBF|!addr
       INC $18E4|!addr

; This is the actual start of the status bar.
.NoCoinIncrease
       LDA !StatusBarDisable|!addr ;\ if status bar disabled, return
       BNE .return                 ;/
       LDA $0100|!addr             ;\ if on the title screen, return
       CMP #$07                    ; |
       BEQ .return                 ;/

	JSR HandleTimersAndYPos		;handle timers and counters position updating

	LDA !LivesCounterYPos|!addr	;load lives counter's Y-pos
	CMP #$F0			;compare to value
	BEQ +				;if equal, do not draw.
       JSR DrawLives
+
	LDA !CoinCounterYPos|!addr	;load coin counter's Y-pos
	CMP #$F0			;compare to value
	BEQ +				;if equal, do not draw.
       JSR DrawCoins
+
	LDA !SCCounterYPos|!addr	;load SMWC coin counter's Y-pos
	CMP #$F0			;compare to value
	BEQ .return			;if equal, do not draw.
       JSR DrawYCoins

.return
       PLB                         ;\ restore
       PLP                         ;/
       RTL

numberTable:
        db !Number0,!Number1,!Number2,!Number3,!Number4
        db !Number5,!Number6,!Number7,!Number8,!Number9

;=======================================
; Draw the lives counter
;=======================================

; this is the only counter I'll comment. Most of the others work the same way, anyway.
; the rest is similar.
; mario's head
DrawLives:
LDX #$20			;starting oam index

       LDA !LivesCounterXPos
       STA $0200|!addr,x
       LDA !LivesCounterYPos|!addr
       STA $0201|!addr,x
       LDA #!PlayerHeadTile
       STA $0202|!addr,x
       LDA #$7A
       STA $0203|!addr,x
       LDA #$02                    ;\  Drew one 16x16 tile, so $00 = 02
       STA $00                     ; |
       JSR setOAMInfo              ;/

       LDA $0DBE|!addr             ;\
       BMI +                       ; |
       CMP #98                     ; |
       BCC +                       ; | hotfix:
       LDA #98                     ; | prevent the player from getting more than 99 lives
       STA $0DBE|!addr             ;/

+      INC                         ;\
       CMP #$0A                    ; | Determine number of digits to show
       BCS .twoDigits              ;/

.oneDigit
       TAY
	LDX #$24			;starting oam index
       LDA !LivesCounterXPos
       CLC : ADC #20
       STA $0200|!addr,x
       LDA !LivesCounterYPos|!addr
       CLC : ADC #$06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo
       JMP +

.twoDigits
       JSR hexToDec
LDX #$24				;starting oam index
       LDA !LivesCounterXPos
       CLC : ADC #20
       STA $0200|!addr,x
       LDA !LivesCounterYPos|!addr
       CLC : ADC #$06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo

       LDA $0DBE|!addr : INC
       JSR hexToDec
       TAY
LDX #$28				;starting oam index
       LDA !LivesCounterXPos
       CLC : ADC #28
       STA $0200|!addr,x
       LDA !LivesCounterYPos|!addr
       CLC : ADC #06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo

+      SEP #$10                    ; 8-bit XY
       RTS                         ; return

;======================================
; Draw the coin counter
;======================================

DrawCoins:
LDX #$2C				;starting oam index
       LDA !CoinCounterXPos
       STA $0200|!addr,x
       LDA !CoinCounterYPos|!addr
       STA $0201|!addr,x
       LDA #$E8
       STA $0202|!addr,x
       LDA #$34
       STA $0203|!addr,x
       LDA #$02
       STA $00
       JSR setOAMInfo

       LDA $0DBF|!addr
       CMP #$0A
       BCS .twoDigits
.oneDigit
       TAY
LDX #$30				;starting oam index
       LDA !CoinCounterXPos
       SEC : SBC #10
       STA $0200|!addr,x
       LDA !CoinCounterYPos|!addr
       CLC : ADC #06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo
       JMP +

.twoDigits
       JSR hexToDec
LDX #$30				;starting oam index
       LDA !CoinCounterXPos
       SEC : SBC #18
       STA $0200|!addr,x
       LDA !CoinCounterYPos|!addr
       CLC : ADC #06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo

       LDA $0DBF|!addr
       JSR hexToDec
       TAY
LDX #$34				;starting oam index
       LDA !CoinCounterXPos
       SEC : SBC #10
       STA $0200|!addr,x
       LDA !CoinCounterYPos|!addr
       CLC : ADC #06
       STA $0201|!addr,x
       LDA numberTable,y
       STA $0202|!addr,x
       LDA #$3C
       STA $0203|!addr,x
       STZ $00
       JSR setOAMInfo

+
       RTS

;============================================
; Draw the yoshi coin counter
;============================================

DrawYCoins:
       LDY #$02
LDX #$38				;starting oam index

.LoopCoins
       LDA $7FC060
       AND .MaskBits,y
       BEQ .NotCollected

       LDA !SCCounterXPos
       CLC : ADC .collectedPositions,y
       STA $0200|!addr,x
       LDA !SCCounterYPos|!addr
       STA $0201|!addr,x
       LDA #!FullStatusCoin
       STA $0202|!addr,x
       LDA #$34
       STA $0203|!addr,x
       LDA #$02
       STA $00
       REP #$10
       JSR setOAMInfo
       SEP #$10
       JMP .finishDraw

.NotCollected
       LDA !SCCounterXPos
       CLC : ADC .collectedPositions,y
       STA $0200|!addr,x
       LDA !SCCounterYPos|!addr
       STA $0201|!addr,x
       LDA #!BlankStatusCoin
       STA $0202|!addr,x
       LDA #$34
       STA $0203|!addr,x
       LDA #$02
       STA $00
       REP #$10
       JSR setOAMInfo
       SEP #$10

.finishDraw
INX #4
       DEY
       BMI +
       JMP .LoopCoins
+      SEP #$10
       RTS

.MaskBits
       db $01,$02,$04
.collectedPositions
       db $00,$18,$30

;=======================================
; Other misc. routines
;=======================================

; Sets OAM info in $0420+
; Entry: $00 with the size (#$00 is 8x8, #$02 is 16x16)

setOAMInfo:
       PHX
       LDA $0200|!addr,x
       REP #$20
       TXA : LSR #2 : TAX
       SEP #$20
       LDA $00
.store
       STA $0420|!addr,x
       PLX
       RTS

; Converts Hex to decimal (16 bit indexes)
; Entry: A with the number

hexToDec:
	REP #$10
       LDY #$0000
.loop
       CMP #$0A
       BCC .break
       SBC #$0A
       INY
       BRA .loop
.break
	SEP #$10
       RTS

; Handle moving counters and keeping them on screen
HandleTimersAndYPos:
LDA !LivesCounterTimer|!addr
BEQ +
DEC !LivesCounterTimer|!addr
BRA ++
+
LDA #$04
TRB !MoveBitflags|!addr
++
LDA !SCCounterTimer|!addr
BEQ +
LDA #$02
TSB !MoveBitflags|!addr
DEC !SCCounterTimer|!addr
BRA ++
+
LDA #$02
TRB !MoveBitflags|!addr
++
LDA !CoinCounterTimer|!addr
BEQ +
DEC !CoinCounterTimer|!addr
BRA ++
+
LDA #$01
TRB !MoveBitflags|!addr
++

JSR .yposhandle

.ret
RTS

.yposhandle
LDA !CoinCounterYPos|!addr
PHA
LDA !SCCounterYPos|!addr
PHA
LDA !LivesCounterYPos|!addr
PHA

LDA !MoveBitflags|!addr
AND #$04
BEQ .livesUP
PLA
CMP #!StopYPos		;if at stop pos (down)
BNE .movedownlives

STZ $0EF9|!addr		;zero speeder
BRA .SC			;move on to next counter

.movedownlives
CMP #$04		;if not below slowdown pos (down)
BMI +			;don't decrement speeder
LDA $0EF9|!addr
DEC
BEQ +			;if equal #$01 skip ahead
DEC $0EF9|!addr		;decrement speeder

+
LDA $0EF9|!addr
BEQ .SC			;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
INC !LivesCounterYPos|!addr	;move down
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE .SC

..one1frame
INC !LivesCounterYPos|!addr	;move down
BRA .SC

.livesUP
PLA
CMP #$08		;if at lowest position
BEQ ..force1		;force speeding up
CMP #$F0		;if at stop pos (up)
BMI .SC			;move on to next counter
CMP #$06		;if at slowdown position
BCS +			;skip ahead, no accel

LDA $0EF9|!addr
CMP #$03
BEQ +
..force1
INC $0EF9|!addr

+
LDA $0EF9|!addr
BEQ .SC			;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
DEC !LivesCounterYPos|!addr	;move up
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE .SC

..one1frame
DEC !LivesCounterYPos|!addr	;move up

.SC
LDA !MoveBitflags|!addr
AND #$02
BEQ .SCUP
PLA
CMP #!StopYPos		;if at stop pos (down)
BNE .movedownsc

STZ $0EFB|!addr		;zero speeder
BRA .coins		;move on to next counter

.movedownsc
CMP #$04		;if not below slowdown pos (down)
BMI +			;don't decrement speeder
LDA $0EFB|!addr
DEC
BEQ +			;if equal #$01 skip ahead
DEC $0EFB|!addr		;decrement speeder

+
LDA $0EFB|!addr
BEQ .coins		;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
INC !SCCounterYPos|!addr	;move down
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE .coins

..one1frame
INC !SCCounterYPos|!addr	;move down
BRA .coins

.SCUP
PLA
CMP #$08		;if at lowest position
BEQ ..force1		;force speeding up
CMP #$F0		;if at stop pos (up)
BMI .coins		;move on to next counter
CMP #$06		;if at slowdown position
BCS +			;skip ahead, no accel

LDA $0EFB|!addr
CMP #$03
BEQ +
..force1
INC $0EFB|!addr

+
LDA $0EFB|!addr
BEQ .coins		;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
DEC !SCCounterYPos|!addr	;move up
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE .coins

..one1frame
DEC !SCCounterYPos|!addr	;move up

.coins
LDA !MoveBitflags|!addr
AND #$01
BEQ .coinsUP
PLA
CMP #!StopYPos		;if at stop pos (down)
BNE .movedowncoins

STZ $0EFD|!addr		;zero speeder
BRA ++			;move on to next counter

.movedowncoins
CMP #$04		;if not below slowdown pos (down)
BMI +			;don't decrement speeder
LDA $0EFD|!addr
DEC
BEQ +			;if equal #$01 skip ahead
DEC $0EFD|!addr		;decrement speeder

+
LDA $0EFD|!addr
BEQ ++			;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
INC !CoinCounterYPos|!addr	;move down
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE ++

..one1frame
INC !CoinCounterYPos|!addr	;move down
BRA ++

.coinsUP
PLA
CMP #$08		;if at lowest position
BEQ ..force1		;force speeding up
CMP #$F0		;if at stop pos (up)
BMI ++			;move on to next counter
CMP #$06		;if at slowdown position
BCS +			;skip ahead, no accel

LDA $0EFD|!addr
CMP #$03
BEQ +
..force1
INC $0EFD|!addr

+
LDA $0EFD|!addr
BEQ ++			;= #$00, don't move
DEC
BEQ ..one2frames	;= #$01, move one pixel every two frames
DEC
BEQ ..one1frame		;= #$02, move one pixel
;else move two pixels
DEC !CoinCounterYPos|!addr	;move up
BRA ..one1frame

..one2frames
LDA $13
AND #$01
BNE ++

..one1frame
DEC !CoinCounterYPos|!addr	;move up

++
RTS

run_pause:
LDA $13D4|!addr
BEQ .nopause

LDA #$07
STA !MoveBitflags|!addr
LDA #!TimersInit
STA !LivesCounterTimer|!addr
STA !SCCounterTimer|!addr
STA !CoinCounterTimer|!addr

JSL StatusProc

LDA #$82
STA $0402|!addr		;lives/coin tiles are forcefully 16x16
LDA #$A0
STA $0403|!addr		;smwc coin 2 tile is forcefully 16x16
LDA #$0E
STA $0404|!addr		;smwc coin 1 is forcefully 16x16; send spinning coin tile offscreen as well

.nopause
LDA $1426|!addr		;restored code below
BEQ +

JML $00A1DF|!bank

+
JML $00A1E4|!bank

;=============================================================
; Give player lives only when the counter is onscreen
;=============================================================

LivesHandlerHijack:
	LDA !StatusBarDisable|!addr
	BNE .handlelivesNOW

	LDA #!TimersInit
	STA !LivesCounterTimer|!addr
	LDA #$04
	TSB !MoveBitflags|!addr

	LDA !LivesCounterYPos|!addr	;load coin counter's Y-pos
	CMP #!StopYPos			;compare to value
	BNE NoLivesYet			;if not equal, skip lives handling.

.handlelivesNOW
DEC $18E4|!addr		;restored code below
BEQ +

JML $028AC8|!bank

+
JML $028ACD|!bank

NoLivesYet:
JML $028AD5|!bank

print bytes