;Message Box Hijack by Blind Devil
;I gave up trying to make the sprite message box work with my SMWCP2 builds.
;Something felt really off, so I went for this change after countless weeks
;of mysel... I mean failure.

;Welp what this does: it changes the windowing effect so the bottom of the
;screen goes black for text to appear. It also triggers layer 3 parallax
;HDMA for the bottom scanlines so the text displays correctly in there if set
;to do so. If not set (for levels that already mess with layer 3 through HDMA),
;then this should be done manually through uberASM: setting up the whole effect
;table again, taking in account the bottom scanlines for the message... and after
;the message is disposed, restore the previous effect.

;Due to the way this is coded (and it's pretty improvised), messages must be
;inserted as layer 3 tilemaps, or written to VRAM by some means. But VRAM writes
;can be painful to work with (involves DMA and tons of checks, plus the already
;complex manual parallax steps if used), wasting cycles, vblank time and so on.

!WindowTable = $7F8900
!L3ParallaxTable = $7F890B	;HDMA table is automatically set up here with defaults unless a bit in 'ManualL3Parallax' table is set for the sublevel.
!L3MsgYPos = $0080		;Y-pos of layer 3 HDMA entry in order to display message

incsrc ../../CommonDefines.asm

;Default defines not to be changed.
!sa1	= 0			; 0 if LoROM, 1 if SA-1 ROM.
!dp	= $0000			; $0000 if LoROM, $3000 if SA-1 ROM.
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.
!bank8	= $80			; $80 if LoROM, $00 if SA-1 ROM.

if read1($00ffd5) == $23
	!sa1	= 1
	!dp	= $3000
	!addr	= $6000
	!bank	= $000000
	!bank8	= $00
	sa1rom
endif

;org $00A0B9			;Hijack spot: beginning of OW loading routine (gamemode C)
;autoclean JML DoRestoreLe7	;jump to code to restore original values for HDMA on channel 7 (used by various vanilla windowing effects)

org $00A1DF			;Hijack spot: message box triggering
autoclean JSL Messages		;call custom code

freecode : print pc

Messages:
LDA $1426|!addr		;load message box flag
BNE PointerHandler	;if there's a message active, branch.
RTL			;return.

PointerHandler:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
JSL $0086DF|!bank	;call 2-byte pointer subroutine
dw DoInc		;$00 - set message mode on
dw writewin		;$01 - write window and start hdma
dw lowerbsc		;$02 - lower blank scanline count
dw waitinput		;$03 - wait for input/timer
dw raisebsc		;$04 - raise blank scanline count

waitinput:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;compare to value
BNE ++			;if not equal, the player can't close the message.

LDA $13D2|!addr		;load switch palace switch that is pressed
BNE .switchend		;if a switch has just been pressed, branch.

LDA $1DF5|!addr		;load timer to dispose the intro/switch palace message
BEQ +			;if equal zero, just dispose message as the button is pressed.
DEC			;decrement value by one
BNE .dectim		;if not equal, decrement it every fourth frame.

LDA $18			;load controller data 2, first frame only
AND #$80		;check if A is pressed
ORA $16			;OR with controller data 1, first frame only
AND #$90		;check if B or Start are pressed
BEQ ++			;if none of these buttons are pressed, return.

STZ $1DF5|!addr		;reset timer to dispose the intro/switch palace message.
JSL $05B160|!bank	;side exit
RTL			;return.

.switchend
JSL .dectim		;call label as subroutine to decrement timer

LDA $1DF5|!addr		;load timer to dispose the intro/switch palace message
BNE ++			;if not equal zero, return.

LDY #$0B
LDA #$01
PEA $84CE		;so that the RTS takes us to RTL
JML $00C9FE|!bank	;ends in RTS

.dectim
LDA $13			;load frame counter
AND #$03		;preserve bits 0 and 1
BNE ++			;if any of them are set, return.

DEC $1DF5|!addr		;decrement timer to dispose the intro/switch palace message
RTL			;return.

+
LDA $18			;load controller data 2, first frame only
AND #$80		;check if A is pressed
ORA $16			;OR with controller data 1, first frame only
AND #$90		;check if B or Start are pressed
BEQ ++			;if none of these buttons are pressed, return.

JSR UndoMask
JSR GetLevelBit		;get option bit for respective level
CMP #$00		;compare to value (yeah we need this line this time because zero flag was modified)
BNE .SkipDisable	;if set, skip disabling HDMA. it's all done manually (or at least should be lol).

LDA #$40		;load bit value
TRB $0D9F|!addr		;clear it from HDMA channels to be processed.

.SkipDisable
INC $1B89|!addr		;increment message box pointer by one

++
RTL			;return.

writewin:
LDX #$09		;loop count
-
LDA.l windowbase,x	;load value from table according to index
STA.l !WindowTable,x	;store to RAM table.
DEX			;decrement X by one
BPL -			;loop while it's positive

JSR UndoMask

   REP #$20                  ;\  Get into 16 bit mode
   LDA #$2601                ; | Register $2126 using mode 1
   STA $4370                 ; | 4370 = transfer mode, 4371 = register
   LDA.w #!WindowTable	     ; | High byte and low byte of table addresse.
   STA $4372                 ; | 4372 = low byte, 4373 = high byte
   SEP #$20                  ; | Back to 8 bit mode
   LDA.b #!WindowTable>>16   ; | Bank byte of table addresse.
   STA $4374                 ;/  = bank byte
   LDA #$80                  ;\  
   TSB $0D9F|!addr           ;/  enable HDMA channel 7

DoInc:
INC $1B89|!addr		;increment message box pointer by one

return:
RTL			;return.

lowerbsc:
LDA.l !WindowTable+3	;load specific scanline count from RAM table
CMP #$27		;compare to value
BEQ +			;if equal, stop decrementing.

SEC			;set carry
SBC #$04		;subtract value
STA.l !WindowTable+3	;store result back.
RTL			;return.

+
JSR ParallaxPerSublevel	;determine if parallax hdma should be used
INC $1B89|!addr		;increment message box pointer by one
RTL			;return.

raisebsc:
LDA.l !WindowTable+3	;load specific scanline count from RAM table
CMP #$5F		;compare to value
BEQ +			;if equal, stop decrementing.

CLC			;clear carry
ADC #$04		;add value
STA.l !WindowTable+3	;store result back.
RTL			;return.

+
STZ $1B89|!addr		;increment message box pointer by one
STZ $1426|!addr		;reset message box flag.

LDA #$80		;load bit value
TRB $0D9F|!addr		;clear it from HDMA channels to be processed.

STZ $41
STZ $42
STZ $43
LDA #$02
STA $44

;RestoreLe7:
;REP #$20
;LDA #$2641
;STA $4370
;LDA #$927C
;STA $4372
;SEP #$20
;STZ $4374		;these last lines restore the previous setup of channel 7, used by the circle HDMA on level end.

RTL			;return.

;DoRestoreLe7:
;JSL RestoreLe7
;STZ $0DDA|!addr
;LDX $0DB3|!addr		;restored code
;JML $00A0BF|!bank	;jump back to regular code

UndoMask:
LDA #$26
STA $42
LDA #$22
STA $41
STA $43
LDA #$A2
STA $44
RTS

GetLevelBit:
PHB			;preserve data bank
PHK			;push program bank into stack
PLB			;pull it as the new data bank

REP #$20		;16-bit A
LDA $010B|!addr		;load sublevel number
PHA			;preserve into stack
AND #$0007		;preserve bits 0, 1, 2 and 3
TAY			;transfer to Y
PLA			;restore sublevel number
LSR #3			;divide by 2 three times
TAX			;transfer to X
SEP #$20		;8-bit A

LDA.l ManualL3Parallax,x	;load respective byte of level
AND BitTable,y			;preserve bit according to value from the bit table
PLB				;restore previous data bank
RTS				;return.

ParallaxPerSublevel:
PHB
PHK
PLB

JSR GetLevelBit		;get option bit for respective level
CMP #$00		;compare to value - zero flag was modified so this is necessary
BNE .ManualSetup	;if set, default layer 3 parallax configs won't be enabled.

LDX #$0F		;table index
LDY #$03		;loop count
-
LDA.w parallaxbase,y	;load value from table according to index
STA.l !L3ParallaxTable,x;store to RAM table, indexed.
DEX #5			;decrement X five times
DEY			;decrement Y by one
BPL -			;loop while it's positive.

REP #$20
LDA $22				;load layer 3 X-pos
STA.l !L3ParallaxTable+1	;store to RAM table.
STA.l !L3ParallaxTable+6	;store to RAM table.
LDA $24				;load layer 3 X-pos
STA.l !L3ParallaxTable+3	;store to RAM table.
STA.l !L3ParallaxTable+8	;store to RAM table.

LDA #!L3MsgYPos			;load value
STA.l !L3ParallaxTable+13	;store to RAM table.

LDA #$0000			;load value

LDX $1426|!addr			;load message box flag into X
CPX #$02			;compare X to value
BNE .not2			;if not equal, keep X-pos entry untouched.

LDA #$0100			;load value

.not2
STA.l !L3ParallaxTable+11	;store to RAM table.

LDA #$1103
STA $4360
LDA.w #!L3ParallaxTable
STA $4362
SEP #$20

LDA.b #!L3ParallaxTable>>16
STA $4364
LDA #$40			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 6

.ManualSetup
LDA #$22
STA $41
STA $43
STA $44

LDA $40
AND #$04
BNE +

LDA #$02
STA $43

+
STZ $42
PLB
RTS

windowbase:
db $80,$FF,$00
db $5F,$FF,$00
db $01,$00,$FF
db $00

parallaxbase:
db $80,$27,$01,$00

BitTable:
db $80,$40,$20,$10,$08,$04,$02,$01

ManualL3Parallax:
db %00110010		;levels 000-007
db %00000000		;levels 008-00F
db %00000000		;levels 010-017
db %00100000		;levels 018-01F
db %00000000		;levels 020-027
db %00000000		;levels 028-02F
db %00000000		;levels 030-037
db %00000000		;levels 038-03F
db %00000000		;levels 040-047
db %00000000		;levels 048-04F
db %00000000		;levels 050-057
db %00000000		;levels 058-05F
db %00000000		;levels 060-067
db %00000000		;levels 068-06F
db %00000000		;levels 070-077
db %00000000		;levels 078-07F
db %00000000		;levels 080-017
db %00000000		;levels 088-01F
db %00000000		;levels 090-097
db %00000000		;levels 098-09F
db %00000000		;levels 0A0-0A7
db %00000000		;levels 0A8-0AF
db %00000000		;levels 0B0-0B7
db %00000000		;levels 0B8-0BF
db %00000000		;levels 0C0-0C7
db %00000000		;levels 0C8-0CF
db %00000000		;levels 0D0-0D7
db %00000000		;levels 0D8-0DF
db %00000000		;levels 0E0-0E7
db %00000000		;levels 0E8-0EF
db %00000000		;levels 0F0-0F7
db %00000000		;levels 0F8-0FF
db %00000000		;levels 100-107
db %00000000		;levels 108-10F
db %00000000		;levels 110-117
db %00010000		;levels 118-11F
db %00000000		;levels 120-127
db %00000000		;levels 128-12F
db %00000010		;levels 130-137
db %00000000		;levels 138-13F
db %00000000		;levels 140-147
db %00000000		;levels 148-14F
db %00000000		;levels 150-157
db %00000000		;levels 158-15F
db %00000000		;levels 160-167
db %00000000		;levels 168-16F
db %00000001		;levels 170-177
db %11100000		;levels 178-17F
db %00000000		;levels 180-117
db %00000000		;levels 188-11F
db %00000000		;levels 190-197
db %00000000		;levels 198-19F
db %00000000		;levels 1A0-1A7
db %00000000		;levels 1A8-1AF
db %00000000		;levels 1B0-1B7
db %00000000		;levels 1B8-1BF
db %00000000		;levels 1C0-1C7
db %00000000		;levels 1C8-1CF
db %00000000		;levels 1D0-1D7
db %00000000		;levels 1D8-1DF
db %00000000		;levels 1E0-1E7
db %00000000		;levels 1E8-1EF
db %00000000		;levels 1F0-1F7
db %00000000		;levels 1F8-1FF