;This patch allows you to disable the screen barrier (as in, allow the player to go offscreen)
;when a ram address is set.

!Freeram_DisableBarrier		= $1696|!addr
!HighestYPos			= $FF80		;>Highest player Y position relative to the border allowed.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ROM type detector
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	!dp = $0000
	!addr = $0000
	!sa1 = 0
	!gsu = 0

if read1($00FFD6) == $15
	sfxrom
	!dp = $6000
	!addr = !dp
	!gsu = 1
elseif read1($00FFD5) == $23
	sa1rom
	!dp = $3000
	!addr = $6000
	!sa1 = 1
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Hijacks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
org $00E98C
	autoclean JML DisableBarrier
	NOP #1

org $00F595
	autoclean JML DisableBottomAndTop
	NOP #1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Freespace code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
freecode : print pc
DisableBarrier:
	JSR CheckRam			;>Override the edgescreen check
	BNE .NoBarrier			;>If true, skip entire edgescreen code

	.SMWNormal
	LDA $1B96+!addr			;\Restore code
	BEQ ..SolidBarrier		;|

	..SideExit
	JML $00E991			;/

	..SolidBarrier
	JML $00E9A1			;>Solid border 

	.NoBarrier
	JML $00E9FB			;>Passable border (also, not to skip blocked status from blocks too!)

DisableBottomAndTop:
	JSR CheckRam
	BNE .NoBarrier

	.SMWNormal
	REP #$20			;\Restore code.
	LDA #!HighestYPos		;|
	JML $00F59A			;/

	.NoBarrier
	JML $00F5B6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Check if at least one RAM is set
;Zero flag: set if all ram is clear
;otherwise clear if at least 1 is set.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CheckRam:
	LDA !Freeram_DisableBarrier	;>Freeram to disable
	ORA $71				;>If animation trigger...
	;ORA $xxxx+!addr		;\Custom RAM here.
	;ORA $YYYY+!addr		;/
	RTS
;^Add as many RAM as you like. If using SA-1/superfx, change the following RAM:
;$0000-$00FF : Add "+!dp"
;$0100-$FFFF : add "+!addr"
;for 1 byte addressing or 3 byte, you don't need.