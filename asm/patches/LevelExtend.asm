;NOTE: This works by preventing mario's collision points
;from being outside the level itself, not limiting the
;actual player. So if anything is placed on the edge of
;the level, they will extend to infinity (not really,
;as long the position values don't overflow).

;The same thing applies to sprite's interaction,
;they too will interact with "off-level" blocks.

;This also includes layer 2; if moving layer 2 gets
;anywhere within mario's collision points at the edge
;of the level, mario will interact with it.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Don't touch these
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;LM v3.00 check

	assert read1($05d9a1) == $22, "Please patch Lunar Magic version 3.00."

;Sa-1 handling
	!dp = $0000
	!addr = $0000
	!sa1 = 0
	!gsu = 0

	if read1($00FFD6) == $15
		sfxrom
		!dp = $6000
		!addr = !dp
		!gsu = 1
	elseif read1($00FFD5) == $23
		sa1rom
		!dp = $3000
		!addr = $6000
		!sa1 = 1
	endif
	macro define_sprite_table(name, name2, addr, addr_sa1)
	if !sa1 == 0
	    !<name> = <addr>
	else
	    !<name> = <addr_sa1>
	endif
	    !<name2> = !<name>
	endmacro

	; Regular sprite tables
	%define_sprite_table(sprite_num, "9E", $9E, $3200)
	%define_sprite_table(sprite_speed_y, "AA", $AA, $9E)
	%define_sprite_table(sprite_speed_x, "B6", $B6, $B6)
	%define_sprite_table(sprite_misc_c2, "C2", $C2, $D8)
	%define_sprite_table(sprite_y_low, "D8", $D8, $3216)
	%define_sprite_table(sprite_x_low, "E4", $E4, $322C)
	%define_sprite_table(sprite_status, "14C8", $14C8, $3242)
	%define_sprite_table(sprite_y_high, "14D4", $14D4, $3258)
	%define_sprite_table(sprite_x_high, "14E0", $14E0, $326E)
	%define_sprite_table(sprite_speed_y_frac, "14EC", $14EC, $74C8)
	%define_sprite_table(sprite_speed_x_frac, "14F8", $14F8, $74DE)
	%define_sprite_table(sprite_misc_1504, "1504", $1504, $74F4)
	%define_sprite_table(sprite_misc_1510, "1510", $1510, $750A)
	%define_sprite_table(sprite_misc_151c, "151C", $151C, $3284)
	%define_sprite_table(sprite_misc_1528, "1528", $1528, $329A)
	%define_sprite_table(sprite_misc_1534, "1534", $1534, $32B0)
	%define_sprite_table(sprite_misc_1540, "1540", $1540, $32C6)
	%define_sprite_table(sprite_misc_154c, "154C", $154C, $32DC)
	%define_sprite_table(sprite_misc_1558, "1558", $1558, $32F2)
	%define_sprite_table(sprite_misc_1564, "1564", $1564, $3308)
	%define_sprite_table(sprite_misc_1570, "1570", $1570, $331E)
	%define_sprite_table(sprite_misc_157c, "157C", $157C, $3334)
	%define_sprite_table(sprite_blocked_status, "1588", $1588, $334A)
	%define_sprite_table(sprite_misc_1594, "1594", $1594, $3360)
	%define_sprite_table(sprite_off_screen_horz, "15A0", $15A0, $3376)
	%define_sprite_table(sprite_misc_15ac, "15AC", $15AC, $338C)
	%define_sprite_table(sprite_slope, "15B8", $15B8, $7520)
	%define_sprite_table(sprite_off_screen, "15C4", $15C4, $7536)
	%define_sprite_table(sprite_being_eaten, "15D0", $15D0, $754C)
	%define_sprite_table(sprite_obj_interact, "15DC", $15DC, $7562)
	%define_sprite_table(sprite_oam_index, "15EA", $15EA, $33A2)
	%define_sprite_table(sprite_oam_properties, "15F6", $15F6, $33B8)
	%define_sprite_table(sprite_misc_1602, "1602", $1602, $33CE)
	%define_sprite_table(sprite_misc_160e, "160E", $160E, $33E4)
	%define_sprite_table(sprite_index_in_level, "161A", $161A, $7578)
	%define_sprite_table(sprite_misc_1626, "1626", $1626, $758E)
	%define_sprite_table(sprite_behind_scenery, "1632", $1632, $75A4)
	%define_sprite_table(sprite_misc_163e, "163E", $163E, $33FA)
	%define_sprite_table(sprite_in_water, "164A", $164A, $75BA)
	%define_sprite_table(sprite_tweaker_1656, "1656", $1656, $75D0)
	%define_sprite_table(sprite_tweaker_1662, "1662", $1662, $75EA)
	%define_sprite_table(sprite_tweaker_166e, "166E", $166E, $7600)
	%define_sprite_table(sprite_tweaker_167a, "167A", $167A, $7616)
	%define_sprite_table(sprite_tweaker_1686, "1686", $1686, $762C)
	%define_sprite_table(sprite_off_screen_vert, "186C", $186C, $7642)
	%define_sprite_table(sprite_misc_187b, "187B", $187B, $3410)
	%define_sprite_table(sprite_tweaker_190f, "190F", $190F, $7658)
	%define_sprite_table(sprite_misc_1fd6, "1FD6", $1FD6, $766E)
	%define_sprite_table(sprite_cape_disable_time, "1FE2", $1FE2, $7FD6)
;hijacks
	org $00F451
	autoclean JML ConstrainMarioCollisionPoints

	org $0194D4
	autoclean JML Sprite_HorizLvl_blk_interYPos

	org $0194EE
	autoclean JML Sprite_HorizLvl_blk_interXPos

	org $019466				;>This is so that it gets the full Y pos info without branching out.
	nop #4

	org $01947B
	nop #4				;>Same thing as above

	org $01946C
	autoclean JML Sprite_VertLvl_Blk_interYPos

	org $019481
	autoclean JML Sprite_VertLvl_Blk_interXPos

	freecode : print pc

ConstrainMarioCollisionPoints:
;A is 16-bit
;X is currently the index of which collision point
	.XPositon
	LDA #$0000			;>Left boundary.
	CMP $94				;\Check if left boundary is right of mario
	BPL ..SetXPosCollisPoint	;/(mario is too far left)
	SEP #$20			;>8-bit A
	LDA $5B				;\Right edge of level varies based on
	LSR				;|if the level is vertical or horizontal
	BCC ..HorizontalLvl		;/
	
	..VerticalLvl
	REP #$20			;>16-bit A
	LDA #$01F0			;>Right edge of vertical level
	CMP $94				;\Check if right boundary is left of mario
	BMI ..SetXPosCollisPoint	;/(mario is too far right)
	BRA ..NormalXPosition
	
	..HorizontalLvl
	LDA $5E				;\Number of screens (horizontal)
	DEC				;/
	XBA				;>high byte position
	LDA #$F0			;>Low byte x position that touches right edge of horizontal level
	REP #$20			;>8-bit A
	CMP $94				;\check if right boundary is to the left of mario
	BMI ..SetXPosCollisPoint	;/(mario too far to the right).

	..NormalXPosition
	LDA $94

	..SetXPosCollisPoint
	CLC
	ADC $00E830,x
	STA $9A
	
	.YPosition
	SEP #$20
	LDA $187A|!addr			;>Riding yoshi (this check allows proper interacion if interacting blocks 1 block below the top).
	BEQ ..NotRidingYoshi
	
	..RidingYoshi
	REP #$20
	LDA #$FFD8
	BRA ..YoshiDone
	
	..NotRidingYoshi
	REP #$20
	LDA #$FFE8			;>Top boundary of level (would be when super mario is halfway of his body)

	..YoshiDone
	CMP $96				;\Check if top boundary is lower than mario (mario too far above.)
	BPL ..SetYPosCollisPoint	;/
	SEP #$20			;>8-bit A
	LDA $5B				;\Determine level type (vertical or horizontal)
	LSR				;|
	BCC ..HorizontalLvl		;/

	..VerticalLvl
	LDA $5F				;\Number of screens (vertical)
	DEC				;|
	XBA				;/
	LDA #$E0			;>Low byte Y position
	REP #$20			;>16-bit A
	CMP $96				;\check if bottom boundary is above mario (mario is too far below)
	BMI ..SetYPosCollisPoint	;/
	BRA ..NormalYPosition

	..HorizontalLvl
	REP #$20
	;LDA #$0190
	LDA $13D7|!addr
	SEC
	SBC #$0020
	CMP $96				;\Check if bottom boundary is is above mario
	BMI ..SetYPosCollisPoint	;/(mario is too far below)
	
	..NormalYPosition
	LDA $96

	..SetYPosCollisPoint
	CLC
	ADC $00E89C,x
	STA $98
	
	.Done
	JML $00F461
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Since vertical and horizontal level codes are seperate,
;I don't need to check if it is, since it's already done.

;Unlike limiting collision points position by checking the
;mario position altogether itself, this code checks and limits
;the individual collision points position, since each sprite
;have different clipping points relative to sprite position.

Sprite_HorizLvl_blk_interYPos:		;>$0194D4
	LDA #$0000			;>Top of level
	CMP $0C				;>Collision point Y position
	BPL .Exceed			;>If top of level below collision point (collision is above)
	;LDA #$01AF			;>Bottom of level
	LDA $13D7|!addr			;\LM v3's level height
	DEC				;/
	CMP $0C				;>Collision point Y position
	BMI .Exceed			;>If bottom of level is above collision point (collision is under)
	SEP #$20
	JML $0194DD			;>Continue on with code

	.Exceed
	STA $0C

	.Align16x16
	SEP #$20
	AND #$F0			;\So it doesn't glitch out with blocks ($0194C5)
	STA $00				;/that need 16x16 alignment
	JML $0194DD			;>Continue on with code

Sprite_HorizLvl_blk_interXPos:		;>$0194EE
	REP #$20
	LDA #$0000			;>Left edge of level
	CMP $0A				;>Collision point X position.
	BPL .Exceed			;>If left edge is right of mario (mario too far left)
	SEP #$20
	LDA $5E				;\Right edge of level
	DEC				;|
	XBA				;|
	LDA #$FF			;/
	REP #$20
	CMP $0A				;>Compare with collision point x position
	BMI .Exceed			;>If right edge is left of point (or point is to the right), limit it.
	SEP #$20
	JML $0194F4			;>Continue on with code


	.Exceed
	STA $0A				;\So edge blocks don't glitch out when hit off screen 
	SEP #$20
	STA $01				;/($0194E3)
	JML $0194F4			;>Continue on with code

Sprite_VertLvl_Blk_interYPos:		;>$01946C
	REP #$20
	LDA #$0000			;>Top border
	CMP $0C				;>Collision point Y Position
	BPL .Exceed			;>If top edge is below collision point (collision point above)
	SEP #$20
	LDA $5F				;\Bottom edge of level
	DEC				;|
	XBA				;|
	LDA #$FF			;/
	REP #$20
	CMP $0C				;>Collision point Y position
	BMI .Exceed			;>check if bottom edge is above collision point (collision point below)
	SEP #$20
	BRA .Restore

	.Exceed
	STA $0C
	SEP #$20
	AND #$F0
	STA $00				;>In case of weird glitches happen that aligns with grid ($01945F)

	.Restore
	LDA !E4,x
	CLC
	ADC.w $0190BA,y
	JML $019472

Sprite_VertLvl_Blk_interXPos:		;>$019481
	REP #$20
	LDA #$0000			;>Left edge
	CMP $0A				;>Collision point x position
	BPL .Exceed			;>if left edge is right of collision (collision is left of edge)
	LDA #$01FF			;>Right edge
	CMP $0A				;>Collision point x position
	BMI .Exceed			;>If right edge is left of collision (collision is right of it)
	SEP #$20
	BRA .Restore
	
	.Exceed
	STA $0A				;>Limit
	SEP #$20
	STA $01				;>Also limit the copied x position

	.Restore
	LDA $01				;\Restore code.
	LSR				;|
	LSR				;/
	JML $019485