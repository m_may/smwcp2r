;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Multiple Midway Points 1.6 patch.
;; By Kaijyuu
;; Fixed on Lunar Magic 2.4+ by LX5
;; Modified slightly by Disk Poppy for SMWCP2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!sa1	= 0			; 0 if LoROM, 1 if SA-1 ROM.
!dp	= $0000			; $0000 if LoROM, $3000 if SA-1 ROM.
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.
!bank8	= $80			; $80 if LoROM, $00 if SA-1 ROM.

if read1($00ffd5) == $23
	!sa1	= 1
	!dp	= $3000
	!addr	= $6000
	!bank	= $000000
	!bank8	= $00
	sa1rom
endif

ORG $05D9DE
	autoclean JML secondary_exits	; additional code for secondary exits

org $048F74
	autoclean JML reset_midpoint	; hijack the code that resets the midway point

org $05DAA3
	autoclean JML no_yoshi		; make secondary exits compatible with no yoshi intros

freecode : print pc

incsrc multi_midway_tables.asm

pushpc

org $05D842
mmp_main:
	STZ $0F
	LDY #$00
	LDA $141A|!addr		; skip if not the opening level
	BNE .return
	LDA $1B93|!addr		; prevent infinite loop if using a secondary exit
	BNE .return
	LDA $0109|!addr
	BNE .calculateSublevelNumber
	;JSR get_translevel	; moved to OWEnterLevelHijack.asm
	;TAY
	LDY $13BF|!addr		; get translevel number
	LDA $1EA2|!addr,y	; get level settings
	AND #$40		; check midway point flag
	BEQ .return		; return if not set
	LDA !CheckpointsSMWCCoins,y	;\ get midway point number to use
	AND #$C0		; |
	ASL		;/
	ROL
	ROL
	ASL
	TAX
	REP #$30
	TYA			; stick translevel number in A
	;AND #$00FF		; clear high byte of A
	ASL			; x 2
	STA $0E			; store to scratch
	LDY #!total_midpoints	; 
	TXA
	CLC
.loop
	ADC $0E			; multiply it by the number of midway points in use per level
	DEY
	BNE .loop
	TAX			; stick in x
	LDA.l level_mid_tables,x	; get new level number
	CMP #$1000		; check secondary exit flag
	BCS .secondary_exit	; use secondary exit
	;AND #$01FF		; failsafe
	STA $0E			; store level number
	JMP $D8B7

.secondary_exit
	AND #$0FFF		; clear secondary exit flag here
	ORA #$0600		; these bits have to be set
	SEP #$30		; 8 bit A X Y
	STA $19B8|!addr		; store secondary exit number (low)
	XBA			; flip high and low byte of A
	STA $19D8|!addr		; store secondary exit number (high and properties)
	STZ $95			; set these to 0
	STZ $97
	JMP $D7B3	; return and load level at a secondary exit position

.return
	LDY $0DB3|!addr
	LDA $13BF|!addr
.calculateSublevelNumber
	CMP #$25
	BCC +
	;SEC
	SBC #$24
	+
	NOP
	NOP
code_05D8AC:

print "code_05D8AC: ",pc

assert code_05D8AC >= $05D8AC
assert code_05D8AC <= $05D8AC

pullpc

secondary_exits:
	LDX $1B93|!addr		; check if using a secondary exit for this
	BNE .return		; if so, skip the code that sets mario's x position to the midway entrance
	STA $13CF|!addr		; restore old code
	LDA $02			; restore old code
	JML $05D9E3|!bank	; return
.return	
	JML $05D9EC|!bank	; return without setting mario's x to the midway entrance

reset_midpoint:  ; idk if it's needed now since we reset midpoints when saving SMWC coins
	STA $1EA2|!addr,x	; restore old code
	INC $13D9|!addr		;
	LDA !CheckpointsSMWCCoins,x
	AND #$FF3F		; reset midway point number too
	STA !CheckpointsSMWCCoins,x
	JML $048F7A|!bank	; return

no_yoshi:
	STZ $1B93|!addr		; reset this (prevents glitch with no yoshi intros and secondary entrances)
	LDA $05D78A|!bank,x	; restore old code
	JML $05DAA7|!bank	; return

print freespaceuse