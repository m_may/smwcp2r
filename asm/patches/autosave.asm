lorom

!sramplus = 1

org $049037 ; runs whenever you move to a new tile
PHX
PHY
PHP

if !sramplus == 0
PHB
REP #$30
LDX #$1EA2
LDY #$1F49
LDA.w #141-1
MVN $7E7E
PLB
endif
SEP #$30

JSL $009BC9|$800000
PLP
PLY
PLX
RTS
warnpc $049058
