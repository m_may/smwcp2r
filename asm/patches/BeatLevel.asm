;SA-1 detector
!sa1	= 0
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

incsrc ../../CommonDefines.asm


org $00C9FE
autoclean JML BeatLevel
NOP
NOP
BRA retshort

org $00CA30
retshort:
;RTS


freecode : print pc

;inputs
;A - exit number starting from 1
;Y - gamemode to change to
BeatLevel:
STY $0100|!addr		;store Y to game mode address, usually #$0B to fadeout to OW.
STA $0DD5|!addr		;store to OW action after the level (trigger normal/secret exit/etc.).
INC $1DE9|!addr		;what's this I don't even (if not zero it means an event should activated on OW)

DEC
LDX $13BF|!addr		;load translevel into X
CMP.l ExitsPerLevel,x
BCS GetEventFail

;Unlock Event
%GetEvent()  ;branches to GetEventFail in case it fails
LDA !EventsUnlocked,y
AND.l ReverseAND,x  ;check if unlocked before
BNE .alreadyUnlocked
INC !EventsUnlockedTotalCount
LDA !EventsUnlocked,y
ORA.l ReverseAND,x
STA !EventsUnlocked,y
.alreadyUnlocked

LDX $13BF|!addr		;load translevel into X
%SaveSMWCCoins()

LDA #$01
STA $13CE|!addr		;store to... what, midway flag?

.endSaveEventsAndCoins
LDA #$00		;load value
STA $7FB000		;store to song (AMK) number. properly allow other songs to play later on.
JML retshort|!bank

GetEventFail:
LDA #$2A
STA $1DFC|!addr  ;"incorrect" SFX
LDA #$80
STA $0DD5
BRA BeatLevel_endSaveEventsAndCoins

%PopCountTable()

ReverseAND:
%ReverseAND()

ExitsPerLevel:
%ExitsPerLevel()
