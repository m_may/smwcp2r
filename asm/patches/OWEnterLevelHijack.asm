;SA-1 detector
if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	!7ED000 = $40D000		; $7ED000 if LoROM, $40D000 if SA-1 ROM.
	sa1rom
else 
	!sa1	= 0
	!addr	= $0000
	!bank	= $800000
	!7ED000 = $7ED000		; $7ED000 if LoROM, $40D000 if SA-1 ROM.
	lorom
endif

incsrc ../../CommonDefines.asm

org $048F35
autoclean JML OnExitLevel
NOP
exitHijackBack:

org $04919F
autoclean JML OnEnterLevel
enterHijackBack:

freecode : print pc

OnExitLevel:
LDA $13CE|!addr
BEQ .exitWithoutEvents
BMI .exitWithEventsDontResetMidpoints
JML exitHijackBack|!bank
.exitWithoutEvents
JML $048F56|!bank
.exitWithEventsDontResetMidpoints
STZ $13CE|!addr
JML $048F77|!bank

OnEnterLevel:
JSR GetTranslevel
TAX
LDA.l ExitsPerLevel,x
BEQ LevelHasNoExits
DEC
PHA
.checkExitsLoop
LDX $13BF|!addr
%GetEvent()
;check if event unlocked but not active
LDA $1F02|!addr,y
EOR #$FF               ;event not active
AND !EventsUnlocked,y  ;and unlocked
AND.w $04E44B,x ;since DB is $05 | reverse AND table
BNE NoEnter
LDA $01,s
DEC
STA $01,s
BPL .checkExitsLoop
GetEventFail:
PLA
LevelHasNoExits:
LDA $0DD6|!addr  ;restored code
LSR
JML enterHijackBack|!bank

NoEnter:
PLA ;exit number
INC
STA $0DD5|!addr
INC $1DE9|!addr
LDA #$FF
STA $13CE|!addr ;new addition - when this address is negative beat the level but keep the midpoints

LDX $13BF|!addr
LDA.l $05D608,x  ;event per level table
STA $1DEA|!addr

;STZ $0DAE|!addr  ;\
LDA #$0E
STA $0DAE|!addr
STZ $0DAF|!addr  ;/ so that it fades in to the overworld properly
LDA #$0C
STA $0100|!addr
JML $0491E8|!bank  ;RTS

ExitsPerLevel:
%ExitsPerLevel()


GetTranslevel:  ;taken from MMP16.asm
	LDY $0DD6|!addr		;get current player*4
	LDA $1F17|!addr,y		;ow player X position low
	LSR #4
	STA $00
	LDA $1F19|!addr,y		;ow player y position low
	AND #$F0
	ORA $00
	STA $00
	LDA $1F1A|!addr,y		;ow player y position high
	ASL
	ORA $1F18|!addr,y		;ow player x position high
	LDY $0DB3|!addr		;get current player
	LDX $1F11|!addr,y		;get current map
	BEQ +
	CLC : ADC #$04		;if on submap, add $0400
+	STA $01
	REP #$10
	LDX $00
	LDA !7ED000,x		;load layer 1 tilemap, and thus current translevel number
	STA $13BF|!addr
	SEP #$10
	RTS
