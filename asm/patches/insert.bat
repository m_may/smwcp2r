set ASAR="../../asar.exe"
set ROM="../../SMWCP2R.smc"

@echo Before using this, make sure Asar is placed in the root folder.
@pause

cls
@echo Welcome to Batman.
@echo.
@echo.

%ASAR% "1player.asm" %ROM%
@echo.
%ASAR% "antisoftlock.asm" %ROM%
@echo.
%ASAR% "asarspritetiles.asm" %ROM%
@echo.
%ASAR% "autosave.asm" %ROM%
@echo.
%ASAR% "better_powerdown.asm" %ROM%
@echo.
%ASAR% "blockmask.asm" %ROM%
@echo.
%ASAR% "classicfire.asm" %ROM%
@echo.
%ASAR% "CloudTimerWarning.asm" %ROM%
@echo.
%ASAR% "Counter Break Yoshi.asm" %ROM%
@echo.
%ASAR% "DisableScreenBarrierViaRam.asm" %ROM%
@echo.
%ASAR% "easymode7.asm" %ROM%
@echo.
%ASAR% "FallingSpikeFix.asm" %ROM%
@echo.
%ASAR% "frozensprfix.asm" %ROM%
@echo.
%ASAR% "lakitu_limit.asm" %ROM%
@echo.
%ASAR% "LevelExtend.asm" %ROM%
@echo.
%ASAR% "MarioGFXDMA.asm" %ROM%
@echo.
%ASAR% "MidwayPointHijack.asm" %ROM%
@echo.
%ASAR% "MMP16.asm" %ROM%
@echo.
%ASAR% "NoMesBoxWin.asm" %ROM%
@echo.
%ASAR% "NoteWallFix.asm" %ROM%
@echo.
%ASAR% "OWMusic.asm" %ROM%
@echo.
%ASAR% "optimize_2132_store.asm" %ROM%
@echo.
%ASAR% "PalaceSwitchHijack.asm" %ROM%
@echo.
%ASAR% "quickrom.asm" %ROM%
@echo.
%ASAR% "RedoneSMWCHUD.asm" %ROM%
@echo.
%ASAR% "RolloverFix.asm" %ROM%
@echo.
%ASAR% "SMWCP2-HexEdits.asm" %ROM%
@echo.
%ASAR% "BoneFix.asm" %ROM%
@echo.
%ASAR% "sram_plus.asm" %ROM%
@echo.
%ASAR% "Title.asm" %ROM%
@echo.
%ASAR% "tweaker.asm" %ROM%
@echo.
%ASAR% "VScrollReprogrammed.asm" %ROM%
@echo.
%ASAR% "yoshi.asm" %ROM%
@echo.

@echo.
@echo - Check for any errors.
@echo.
@echo - btw remember to SHIFT+F8 your ROM in LM because SRAM Plus
@echo undoes the sprite 19 fix by default.
@echo.
@echo - If using LM v3.04, remember to apply tidefix.asm manually.
@echo.
@echo - STEP 1/2 COMPLETE -
@pause

cls
@echo Batman is about to insert 'ferrisgfxpatch.asm' into the ROM.
@echo If you've already inserted this patch previously, close this
@echo window. Else, press Enter to continue.
@pause
@echo.
%ASAR% "ferrisgfxpatch.asm" %ROM%
@echo.
@echo.
@echo - STEP 2/2 COMPLETE -
@pause