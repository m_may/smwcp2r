;Force new tweaker properties for Falling Spike by Blind Devil
;Done because Tweaker has no effect in them by default. Most of Tweaker
;properties have no effect because SMW doesn't set up these bytes at all
;from within sprite code.

org $039214
autoclean JML TweakFallingSpike

freecode : print pc
TweakFallingSpike:
JSL $8190B2		;restored code.

LDA #$80		;load value
STA $1656,x		;store to first tweaker byte (die in puff of smoke).

LDA #$04		;load value
STA $190F,x		;store to sixth tweaker byte (disable slide kill).

JML $839218		;jump back to actual code.