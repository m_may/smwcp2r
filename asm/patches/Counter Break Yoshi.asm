if read1($00FFD5) == $23
	sa1rom
	!Base2 = $6000		;>$0100-$0FFF -> $6100-$6FFF and $1000-$1FFF -> $7000-$7FFF
else
	lorom
	!Base2 = $0000
endif

ORG $A0D5
	;NOP #8		;Instead of wasting 16 cycles we'll just skip those NOPs
	BRA ORLY		;2 bytes long - 3 cycles
	NOP #6		;We skipped 18 cycles meaning we saved 15 cycles :O		
ORLY:
	STZ $0DBA+!Base2,x	;No Yoshi color (of Player 1 and Player 2)

ORG $0491C9

	BRA YARLY	;\
	NOP			;/Saved 3 cycles
YARLY:		
	STZ $0DC1+!Base2	;i have no idea i dont even
	STZ $13C7+!Base2	;No Yoshi color
	STZ $187A+!Base2	;No riding Yoshi flag