lorom

; tweaker byte = table
!1656 = $07F26C
!1662 = $07F335
!166E = $07F3FE
!167A = $07F4C7
!1686 = $07F590
!190F = $07F659

; ...
macro write(addr, value)
	org <addr>
	db <value>
endmacro

; use this to set and clear bits for a tweaker byte
macro editBits(tweakerbyte, sprite, setbits, clearbits)
	!offset = <tweakerbyte>+<sprite>
	!content = read1(!offset)
	!result = (!content|<setbits>)&~<clearbits>
	%write(!offset, !result)
endmacro

; use this if you want to directly modify the value of a tweaker byte
macro editByte(tweakerbyte, sprite, value)
	!offset = <tweakerbyte>+<sprite>
	%write(!offset, <value>)
endmacro

; use this if you want to edit the palette of a sprite (not hardcoded ones)
macro editPalette(sprite, palette)
	!palette = <palette>
	assert !palette <= $0F, "Invalid palette number. Either use values in the $08-$0F or $00-$07 range."
	!offset = !166E+<sprite>
	!content = read1(!offset)
	!bitmask = !palette<<1&$0E
	!result = (!content&~$0E)|!bitmask
	%write(!offset, !result)
endmacro

; do your edits here

%editBits(!190F, $B4, $04, 0)	; Disable slide killing for non-lineguided grinders
;%editBits(!190F, $B2, $04, 0)	; .. and falling spike (commented out - now handled via separate patch)
%editBits(!190F, $A8, $04, 0)	; .. and blargg
%editBits(!190F, $B6, $04, 0)	; .. and reflecting fireball
%editBits(!190F, $C3, $04, 0)	; .. porcupuffer

%editBits(!166E, $C3, $FD, 0)	; disable cape/fireball kill for porcupuffer

%editBits(!167A, $7E, $82, 0)	; make flying coin invincible

%editBits(!167A, $9E, $02, 0)	; make ball'n'chain invincible

;%editPalette($11, $0B)		; change palette of buzzy beetle
%editPalette($7F, $0A)		; .. and of that golden mushroom thing

;honestly, this patch sucks.
org !166E+$1B			;bouncing football edit
db $05
org !166E+$AA            	;fishbone edit
db $73
org $039784			;also fishbone (tail tiles properties)
db $43,$C3,$03,$83