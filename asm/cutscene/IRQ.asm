pushpc

org $008385
	autoclean JML IRQHijack
	NOP

pullpc


IRQHijackBack:
	LDA.b #$81
	LDY.w $0D9B
	JML $00838A

IRQHijack:
{
	LDA !inCutscene       ; \
	BEQ IRQHijackBack     ; / If we're not in a cutscene, get out of here.

	                ; We have two codes here.  The first deals with windowing and shows layer 1/2.  
	                ; The second turns on the text display.
	LDA $11
	CMP #$01
	BEQ .midScreen          ; Between the border and the text.
	CMP #$02
	BEQ .enableSecondScreen
	CMP #$03
	BEQ .enableSprites
-	BIT $4212           ; Wait until H-blank
	BVC -

	LDA #$27            ; Set the window.
	STA $2126           ; 
	LDA #$D8            ; 
	STA $2127           ; 

	LDA #$6D
	STA $4209           ; \ Set an IRQ at scanline #$70
	STZ $420A           ; /

	BRA .end
 .midScreen
-	BIT $4212           ; Wait until H-blank
	BVC -
	LDA #$FF
	STA $2126
	STZ $2127

	lda #$04
	sta $212c
	sta $212d

	lda #$13
	sta $212E

	LDA #$74
	STA $4209           ; \ Set an IRQ at scanline #$74 (it will switch from "picture" to "text" mode there)
	STZ $420A           ; /
	BRA .end
 .enableSprites  
	LDA #$14            ; \ Layer 3 and OBJ on the main screen
	STA $212C           ; /

	LDA #$03            ; \ Layers 1, 2, and OBJ on the sub screen.
	;STA $212D           ; /
	STA $212F           ; Clip them, too.
	stz $212E
	stz $2131
	BRA .end
 .enableSecondScreen

-	BIT $4212           ; Wait until H-blank
	BVC -

	LDA !cutsceneFadeStatus
	BMI .noBrightnessFoolery

	LDA !fadeTextOut
	BEQ +
	DEC
	STA $2100
	BRA ++
	+
	LDA #$0F
	STA $2100
	++

 .noBrightnessFoolery

	LDA #$FF
	STA $2126
	STZ $2127

	STZ $2112
	LDA #$89
	CLC
	ADC !textYPos
	STA $2112
	STZ $2112
	STZ $2111
	STZ $2111

	LDA #$A0
	STA $4209           ; \ Set an IRQ at scanline #$A0
	STZ $420A           ; / It will turn off sprite clipping.

 .end
	INC $11

	JML $0083B2
}
