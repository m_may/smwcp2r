!inCutscene = $0F6E     ; True if we're in a cutscene.
!toLoad = $0F6F ; Set to 0 to load the cutscene.
                ; Set to non-zero to load the overworld.
!cutsceneFadeStatus = $0F70
                ; Bit 7 indicates whether or not we're fading (0 = not fading)
                ; Bit 0 indicates the direction (0 = fading in, 1 = fading out)

!textYPos = $16B5           ; Current position of the layer 3 text
!fadeTextOut = $16B7        ; Serves two purposes.  If >= 1, sets the text darkness to this - 1.  If 1, clear out all text graphics.