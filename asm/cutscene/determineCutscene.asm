pushpc
; Modify gamemode $11
org $0096D5
	autoclean JSL LevelStartHijack
	NOP
	NOP
	LDA.W $141A
	BNE $09
	LDA.W $141D
pullpc

LevelStartHijack:
{
.hijackPrepare
	STZ $4200
	; We need to do a JSL to NoButtons (F62D), but it's a JSR routine.
	PHK
	PEA.w .jslrtsreturn-1
	PEA $974E
	JML $00F62D
 .jslrtsreturn

	PHB
	PHK
	PLB

.check
	; Determine whether to finish loading the level after the cutscene.
	LDA !toLoad
	BEQ .determineLevelNumber
	JMP .finishLoadingLevel

.determineLevelNumber
	LDA $0109
	BEQ .hijackExitRestore

	CMP #$E9
	BNE .hijackExitRestore

	LDA $1F11
	BNE .hijackExitRestore

.yesCutscene
	SEP #$20
	LDA #2
	STA !toLoad

 .changeGamemode ; (to custom - see uberasmtool/gamemode/cutscene)
	LDA #$34
	STA $0100|!addr

.hijackExitSkip
	STZ $13D5
	STZ $13D9

	PLB
	; If we are going to run a cutscene, then we don't run the rest of the level loading code.
	; Instead we just set the new game mode and exit.
	; In order to exit from where we are now though we need some hackery.
	; First pull our return address off the stack.
	PLA : PLA : PLA
	; Now push the address of the instruction we want to jump to in bank one minus 1 onto the stack.
	; When we return from OUR RTL, we'll jump back to the end, thus ignoring the rest of the code that was going to run.
	LDA #$00
	PHA
	PEA $9748
	RTL

.finishLoadingLevel
	STZ !toLoad
	BRA .hijackExitRestore

.hijackExitRestore
	PLB
	RTL
}